<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed table-linked">
				<thead>
					<tr>
						<th>#</th>
						<th><a href="'.site_url().'payroll/payroll/branch.branch_name/'.$order_method.'/'.$page.'">Branch</a></th>
						<th><a href="'.site_url().'payroll/payroll/payroll.month_id/'.$order_method.'/'.$page.'">Month</a></th>
						<th><a href="'.site_url().'payroll/payroll/payroll.payroll_year/'.$order_method.'/'.$page.'">Year</a></th>
						<th><a href="'.site_url().'payroll/payroll/payroll.created/'.$order_method.'/'.$page.'">Created</a></th>
						<th><a href="'.site_url().'payroll/payroll/payroll.created_by/'.$order_method.'/'.$page.'">Created by</a></th>
						<th><a href="'.site_url().'payroll/payroll/payroll.payroll_status/'.$order_method.'/'.$page.'">Status</a></th>
						
						<th colspan="2" style="text-align:center">Bank</th>
						<th colspan="2"  style="text-align:center">PAYE</th>
						<th colspan="2"  style="text-align:center">NHIF</th>
						<th colspan="2"  style="text-align:center">NSSF</th>
						<th colspan="2"  style="text-align:center">HSE LEVY</th>
						<th colspan="2"  style="text-align:center">NITA</th>
						<th colspan="1"  style="text-align:center">PAYROLL</th>
						<th colspan="1"  style="text-align:center">PAYSLIPS</th>
						<th colspan="5">Actions</th>


					</tr>
				</thead>
				<tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			
			foreach ($query->result() as $row)
			{
				$branch_id = $row->branch_id;
				$branch_name = $row->branch_name;
				$payroll_id = $row->payroll_id;
				$payroll_year = $row->payroll_year;
				$month_name = $row->month_name;
				$created = date('jS M Y H:i a',strtotime($row->created));
				$created_by = $row->created_by;
				$filename = $row->file_data;
				$payroll_status = $row->payroll_status;
				$payroll_closed = $row->payroll_closed;
				$payroll_name = $month_name.' '.$payroll_year;
				
				$file_name = $filename.'.txt';
				
				//get branch
				if($administrators->num_rows() > 0)
				{
					foreach($administrators->result() as $res)
					{
						$personnel_id = $res->personnel_id;
						if($personnel_id == $created_by)
						{
							$personnel_fname = $res->personnel_fname;
							$personnel_onames = $res->personnel_onames;
							$created_by = $personnel_onames.' '.$personnel_fname;
						}
					}
				}
				
				//create deactivated status display
				if($payroll_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					//$button = '<a class="btn btn-info" href="'.site_url().'accounts/activate-payroll/'.$payroll_id.'" onclick="return confirm(\'Do you want to activate '.$payroll_name.'?\');" title="Activate '.$payroll_name.'"><i class="fa fa-thumbs-up"></i></a>';
					$button = '';
				}
				//create activated status display
				else if($payroll_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-xs btn-default" href="'.site_url().'payroll/deactivate-payroll/'.$payroll_id.'" onclick="return confirm(\'Do you want to close '.$payroll_name.'?\');" title="Close '.$payroll_name.'"><i class="fa fa-times"></i></a>';
				}
				if($payroll_closed==0)
				{
					$payroll_closed_button = '<a href="'.$this->payroll_file_location.''.$file_name.'" download="'.$file_name.'" target="_blank"class = "btn btn-xs btn-default" title = "Download backup" ><i class="fa fa-thumbs-down"></i></a>';
				}
				$count++;
					// <td><a href="'.site_url().'payroll/list-batches/'.$payroll_id.'/'.$branch_id.'" class="btn btn-sm btn-info" title="Recreate data file for '.$payroll_name.'" onclick="return confirm(\'Are you sure you would like to generate the payroll in batches\');">Batches</a></td>
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$branch_name.'</td>
						<td>'.$month_name.'</td>
						<td>'.$payroll_year.'</td>
						<td>'.$created.'</td>
						<td>'.$created_by.'</td>
						<td>'.$status.'</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/generate-bank-report/'.$payroll_id.'" class="btn btn-xs btn-default" title="Generate Bank '.$payroll_name.'" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
							
						</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/export-bank-report/'.$payroll_id.'" class="btn btn-xs btn-success" title="Generate Bank '.$payroll_name.'" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
						</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/print-paye-report/'.$payroll_id.'" class="btn btn-xs btn-default" title="Print '.$payroll_name.' PAYE report" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/print-paye-report/'.$payroll_id.'" class="btn btn-xs btn-success" title="Print '.$payroll_name.' PAYE report" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
						</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/print-nhif-report/'.$payroll_id.'" class="btn btn-xs btn-info" title="Print '.$payroll_name.' NHIF report" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/export-nhif-report/'.$payroll_id.'" class="btn btn-xs btn-info" title="Print '.$payroll_name.' NHIF report" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
						</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/print-nssf-report/'.$payroll_id.'" class="btn btn-xs btn-warning" title="Print '.$payroll_name.' NSSF report" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/export-nssf-report/'.$payroll_id.'" class="btn btn-xs btn-warning" title="Print '.$payroll_name.' NSSF report" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
						</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/print-hse-report/'.$payroll_id.'" class="btn btn-xs btn-primary" title="Print '.$payroll_name.' Housing Levy report" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/export-hse-report/'.$payroll_id.'" class="btn btn-xs btn-primary" title="Print '.$payroll_name.' Housing Levy report" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
						</td>

						<td style="text-align:center">
							<a href="'.site_url().'payroll/print-nita-report/'.$payroll_id.'" class="btn btn-xs btn-default" title="Print '.$payroll_name.' NITA report" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/export-nita-report/'.$payroll_id.'" class="btn btn-xs btn-default" title="Print '.$payroll_name.' NITA report" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
						</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/print-payroll/'.$payroll_id.'" class="btn btn-xs btn-success" title="Print '.$payroll_name.'" target="_blank">Payroll <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
						</td>
						<td style="text-align:center">
							<a href="'.site_url().'payroll/print-month-payslips/'.$payroll_id.'" class="btn btn-xs btn-primary" title="Print '.$payroll_name.'" target="_blank">Payslips</a>
						</td>
						<td><a href="'.site_url().'payroll/print-month-summary/'.$payroll_id.'/'.$branch_id.'" class="btn btn-xs btn-primary" title="Summary '.$payroll_name.'" target="_blank">Summary</a></td>
						<td><a href="'.site_url().'payroll/send-month-payslips/'.$payroll_id.'" class="btn btn-xs btn-success" title="Send Payslips for '.$payroll_name.'">Send Payslips</a></td>
						<!--<td><a href="'.site_url().'payroll/create-data-file/'.$payroll_id.'/'.$branch_id.'" class="btn btn-xs btn-default" title="Recreate data file for '.$payroll_name.'" onclick="return confirm(\'Are you sure you would like to regenerate the data file\');">Create Data File</a></td>-->
					
						<td>'.$button.'</td>
						<td>'.$payroll_closed_button.'</td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no payrolls";
		}
?>
						<div class="row">
                        	<div class="col-sm-3">
                            	<section class="panel">
                                    <header class="panel-heading">						
                                        <h2 class="panel-title">Search payroll</h2>
                                    </header>
                                    <div class="panel-body">
                                    	<?php 
										echo form_open('payroll/search-payroll');
										?>
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">Year: </label>
                                            
                                            <div class="col-lg-7">
                                                <input type="text" name="year" class="form-control" size="54" value="<?php echo date("Y");?>" />
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">Month: </label>
                                            
                                            <div class="col-lg-7">
                                                <select name="month" class="form-control">
                                                    <?php
                                                        if($month->num_rows() > 0){
                                                            foreach ($month->result() as $row):
                                                                $mth = $row->month_name;
                                                                $mth_id = $row->month_id;
                                                                if($mth == date("M")){
                                                                    echo "<option value=".$mth_id." selected>".$row->month_name."</option>";
                                                                }
                                                                else{
                                                                    echo "<option value=".$mth_id.">".$row->month_name."</option>";
                                                                }
                                                            endforeach;
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-top:10px;">
                                            <div class="col-lg-7 col-lg-offset-5">
                                                <div class="form-actions center-align">
                                                    <button class="submit btn btn-primary" type="submit">
                                                        <i class='fa fa-search'></i> Search
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <?php echo form_close();?>
                                    </div>
                                </section>
                            </div>
                            <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
							<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
							<style type="text/css">
								element {
										width: 58px !important;
										height: 25px !important;
									}
								.toggle.btn {
										width: 58px !important;
										height: 25px !important;
									}
									.toggle.btn {
										min-width: 59px;
										min-height: 20px !important;
									}
									.toggle {
										margin: 0px 0 0;
										position: relative;
										clear: both;
									}
									.toggle {
										display: inline-block;
										padding: 2px 12px !important;
									}
									.alert {
										padding: 1px;
										margin-bottom: 20px;
										border: 1px solid transparent;
										border-radius: 4px;
									}
							</style>
							<?php
							$housing_levy = $this->session->userdata('housing_levy');

							if($housing_levy)
								$housing_levy = 'checked';
							else
								$housing_levy = '';

							$nssf_rate = $this->session->userdata('nssf_rate');

							if($nssf_rate)
								$nssf_rate = 'checked';
							else
								$nssf_rate = '';


							$nhif_rate = $this->session->userdata('nhif_rate');

							if($nhif_rate)
								$nhif_rate = 'checked';
							else
								$nhif_rate = '';
							?>
                            
                        	<div class="col-sm-6">
                            	<section class="panel">
                                    <header class="panel-heading">						
                                        <h2 class="panel-title">Create payroll</h2>
                                       
                                    </header>
                                    <div class="panel-body">
                                    	<?php 
										echo form_open('payroll/create-payroll');
										?>
										<div class="col-lg-12">
										<div class="col-lg-6">
	                                        <div class="form-group">
	                                            <label class="col-lg-5 control-label">Year: </label>
	                                            
	                                            <div class="col-lg-7">
	                                                <input type="text" name="year" class="form-control" size="54" value="<?php echo date("Y");?>" />
	                                            </div>
	                                        </div>
	                                        
	                                        <div class="form-group">
	                                            <label class="col-lg-5 control-label">Month: </label>
	                                            
	                                            <div class="col-lg-7">
	                                                <select name="month" class="form-control">
	                                                    <?php
	                                                        if($month->num_rows() > 0){
	                                                            foreach ($month->result() as $row):
	                                                                $mth = $row->month_name;
	                                                                $mth_id = $row->month_id;
	                                                                if($mth == date("M")){
	                                                                    echo "<option value=".$mth_id." selected>".$row->month_name."</option>";
	                                                                }
	                                                                else{
	                                                                    echo "<option value=".$mth_id.">".$row->month_name."</option>";
	                                                                }
	                                                            endforeach;
	                                                        }
	                                                    ?>
	                                                </select>
	                                            </div>
	                                        </div>
	                                         <div class="row" style="margin-top:25px;">
                                            <div class="col-lg-12">
                                                <div class="form-actions center-align">
                                                    <button class="submit btn btn-xs btn-primary" type="submit" onclick="return confirm('Are you sure you want to create the payroll')">
                                                        Create Payroll
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
	                                    </div>
	                                    <div class="col-lg-6">
                                         <div class="form-group">
                                            <label class="col-lg-2 control-label">Housing Levy: </label>
                                            
                                            <div class="col-lg-2">
  												<input id="toggle-event" <?php echo $housing_levy;?> type="checkbox"  data-toggle="toggle" style="min-height:10px !important;" >
  												
                                            </div>
                                            <div class="col-lg-8">
                                            	<div class="alert alert-xs alert-info alert-warning"> 1.5 % of the gross pay as deduction</div>
                                            </div>
                                        </div>

                                       
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">NSSF: </label>
                                            
                                            <div class="col-lg-2">
  												<input id="toggle-nssf" <?php echo $nssf_rate;?> type="checkbox"  data-toggle="toggle" >
  												
                                            </div>
                                            <div class="col-lg-8">
                                            	<div class="alert alert-xs alert-info alert-warning"> Application of the 6% deduction for nssf</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">NHIF: </label>
                                            
                                            <div class="col-lg-2">
  												<input id="toggle-nhif" <?php echo $nhif_rate;?> type="checkbox"  data-toggle="toggle" style="min-height: 30px !important;height: 30px !important;" >
  												
                                            </div>
                                            <div class="col-lg-8">
                                            	<div class="alert alert-xs alert-info alert-warning"> 2.75% of the gross pay as deduction</div>
                                            </div>
                                        </div>

                                       
                                       </div>
                                       

                                    </div>
                                    <div class="col-lg-12" style="display: none;">
                                        <div class="row" style="margin-top:10px;">
                                            <div class="col-lg-12">
                                                <div class="form-actions center-align">
                                                    <button class="submit btn btn-xs btn-primary" type="submit" onclick="return confirm('Are you sure you want to create the payroll')">
                                                        Create Payroll
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <?php echo form_close();?>
                                    </div>
                                </section>
                            </div>
                            <div class="col-sm-3">
                            	<section class="panel">
                                    <header class="panel-heading">						
                                        <h2 class="panel-title">Change branch</h2>
                                    </header>
                                    <div class="panel-body">
                                    	<?php 
										$attributes = array('class' => 'form-horizontal');
										echo form_open('payroll/change-branch', $attributes);
										?>
                                        <div class="form-group">
                                            <label class="col-lg-5 control-label">Branch: </label>
                                            
                                            <div class="col-lg-7">
                                                <select name="branch_id" class="form-control">
                                                    <?php
                                                        if($branches->num_rows() > 0)
														{
                                                            foreach ($branches->result() as $row):
                                                                $branch_name = $row->branch_name;
                                                                $branch_id = $row->branch_id;
                                                                echo "<option value=".$branch_id.">".$branch_name."</option>";
                                                            endforeach;
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-top:10px;">
                                            <div class="col-lg-7 col-lg-offset-5">
                                                <div class="form-actions center-align">
                                                    <button class="submit  btn btn-xs btn-primary" type="submit">
                                                        Change
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <?php echo form_close();?>
                                    </div>
                                </section>
                            </div>
                        </div>
                        
						<section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?></h2>
								 <div class="widgets-panel pull-right" style="margin-top:-20px">
                                        	<!-- <a href="<?php echo site_url();?>payroll/all-payroll" class="btn btn-sm btn-warning ">All Branches Payrolls</a> -->
                                        <a href="<?php echo site_url();?>payroll/salary-data" class="btn btn-xs btn-info ">Edit Staff Statutories</a>
                                   </div>
							</header>
							<div class="panel-body">

                            	<div class="col-md-12" >
                            	<?php
                                $success = $this->session->userdata('success_message');
		
								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}
								
								$error = $this->session->userdata('error_message');
								
								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}
								?>
                                        <?php
										$search = $this->session->userdata('payroll_search');
		
										if(!empty($search))
										{
											?>
                                            <a href="<?php echo site_url();?>payroll/close-payroll-search" class="btn btn-sm btn-warning">Close search</a>
                                            <?php
										}
                                        
										?>
                                </div>

                            	<div class="col-md-12" >
									<div class="table-responsive">
	                                	
										<?php echo $result;?>
								
	                                </div>
	                            </div>
                                <div class="panel-footer">
	                            	<?php if(isset($links)){echo $links;}?>
	                            </div>
							</div>
                            
						</section>
						<script type="text/javascript">
							 $(function() {
							    $('#toggle-event').change(function() {
							      $('#console-event').html('Toggle: ' + $(this).prop('checked'))
							      var checked = $(this).prop('checked');

							      if(checked == true)
							      {
							      	var check = 1;
							      	// alert($(this).prop('checked'));
							      }
							      else
							      {
							      	var check = 0;
							      	// alert('dalse');
							      }


							     	var config_url = document.getElementById("config_url").value;
							        var data_url = config_url+"payroll/update_housing_levy/"+check;

									$.ajax({
								    type:'POST',
								    url: data_url,
								    data:{check: check},
								    dataType: 'text',
								    success:function(data){

								    	
								    },
								    error: function(xhr, status, error) {

								   		 alert(error);
								    }

								    });
							    
							    })



							    // nssf

							     $('#toggle-nssf').change(function() {
							      var checked = $(this).prop('checked');

							      if(checked == true)
							      {
							      	var check = 1;
							      	// alert($(this).prop('checked'));
							      }
							      else
							      {
							      	var check = 0;
							      	// alert('dalse');
							      }


							     	var config_url = document.getElementById("config_url").value;
							        var data_url = config_url+"payroll/update_nssf_rate/"+check;

									$.ajax({
								    type:'POST',
								    url: data_url,
								    data:{check: check},
								    dataType: 'text',
								    success:function(data){

								    	
								    },
								    error: function(xhr, status, error) {

								   		 alert(error);
								    }

								    });
							    
							    })



							     // nhif

							      $('#toggle-nhif').change(function() {
							      var checked = $(this).prop('checked');

							      if(checked == true)
							      {
							      	var check = 1;
							      	// alert($(this).prop('checked'));
							      }
							      else
							      {
							      	var check = 0;
							      	// alert('dalse');
							      }


							     	var config_url = document.getElementById("config_url").value;
							        var data_url = config_url+"payroll/update_nhif_rate/"+check;

									$.ajax({
								    type:'POST',
								    url: data_url,
								    data:{check: check},
								    dataType: 'text',
								    success:function(data){

								    	
								    },
								    error: function(xhr, status, error) {

								   		 alert(error);
								    }

								    });
							    
							    })
							})
						</script>