
<?php
$personnel_id = $this->session->userdata('personnel_id');
$prepared_by = $this->session->userdata('first_name');
$roll = $payroll->row();
$year = $roll->payroll_year;
$month = $roll->month_id;
$totals = array();
// $employer_contribution = 200;
	$total_number_of_payments = 6;
	$total_gross = 0;
	$total_paye = 0;
	$total_nssf = 0;
	$total_nhif = 0;
	$total_life_ins = 0;
	$total_allowances = 0;
	$total_savings = 0;
	$total_loans = 0;
	$total_schemes = 0;
	$total_net = 0;
	$all_loans_total = array();
	$total_personnel_overtime = $total_personnel_benefits = $total_personnel_payments = $total_personnel_allowances = $total_personnel_deductions = $total_personnel_other_deductions = $total_personnel_loans = $total_personnel_savings = array();
	
	$benefits_amount = $payroll_data->benefits;
	$total_benefits = $payroll_data->total_benefits;
	$payments_amount = $payroll_data->payments;
	$total_payments = $payroll_data->total_payments;
	$allowances_amount = $payroll_data->allowances;
	$total_allowances = $payroll_data->total_allowances;
	$deductions_amount = $payroll_data->deductions;
	$total_deductions = $payroll_data->total_deductions;
	$other_deductions_amount = $payroll_data->other_deductions;
	$total_other_deductions = $payroll_data->total_other_deductions;
	$nssf_amount = $payroll_data->nssf;
	$nhif_amount = $payroll_data->nhif;
	$life_ins_amount = $payroll_data->life_ins;
	$paye_amount = $payroll_data->paye;
	$monthly_relief_amount = $payroll_data->monthly_relief;
	$insurance_relief_amount = $payroll_data->insurance_relief;
	$insurance_amount_amount = $payroll_data->insurance;
	$scheme = $payroll_data->scheme;
	$total_scheme = $payroll_data->total_scheme;

	$housing_levy = $payroll_data->housing_levy;
	$savings = $payroll_data->savings;
	$total_overtime = $overtime_amount = $overtime_rate = $overtime_type = NULL;
	//if($payroll_data->total_overtime != NULL)
	//if(isset($payroll_data->total_overtime_amount))
	//{
	$total_overtime = $payroll_data->total_overtime;
	$overtime_amount = $payroll_data->overtime;
	$overtime_rate = $payroll_data->overtime_rate;
	$overtime_type = $payroll_data->overtime_type;

if($payments->num_rows() > 0)
{
	foreach($payments->result() as $res)
	{
		$payment_abbr = $res->payment_name;
		$payment_id = $res->payment_id;
		$total_payment_amount[$payment_id] = 0;
		if(isset($total_payments->$payment_id))
		{
			$total_payment_amount[$payment_id] = $total_payments->$payment_id;
		}
	}
}

//benefits
	if($benefits->num_rows() > 0)
	{
		foreach($benefits->result() as $res)
		{
			$benefit_abbr = $res->benefit_name;
			$benefit_id = $res->benefit_id;
				
			$total_benefit_amount[$benefit_id] = 0;
			if(isset($total_benefits->$benefit_id))
			{
				$total_benefit_amount[$benefit_id] = $total_benefits->$benefit_id;
			}
		}
	}
	
	//allowances
	if($allowances->num_rows() > 0)
	{
		foreach($allowances->result() as $res)
		{
			$allowance_abbr = $res->allowance_name;
			$allowance_id = $res->allowance_id;
				
			$total_allowance_amount[$allowance_id] = 0;
			if(isset($total_allowances->$allowance_id))
			{
				$total_allowance_amount[$allowance_id] = $total_allowances->$allowance_id;
			}
		}
	}
	
	//overtime
	if($overtime->num_rows() > 0)
	{
		foreach($overtime->result() as $res)
		{
			$overtime_name = $res->overtime_name;
			$overtime_id = $res->overtime_type;
				
			$total_overtime_amount[$overtime_id] = 0;
			if(isset($total_overtime->$overtime_id))
			{
				$total_overtime_amount[$overtime_id] = $total_overtime->$overtime_id;
			}
		}
	}

// echo "<pre>";
// print_r($total_payment_amount);
// echo "</pre>";die();

if ($query->num_rows() > 0)
{
	$nssf_amount  =  $payroll_data->nssf;
	$count = 0;
	
	$result = 
	'
	<table class="table table-bordered table-striped table-condensed" id ="testTable">
		<thead>
			<tr>
				<th>PAYROLL NUMBER</th>
				<th>LAST NAME</th>
				<th>OTHER NAMES</th>
				<th>ID NO</th>
				<th>KRA PIN</th>
				<th>NSSF NO</th>
				<th>GROSS PAY</th>
	';
	$total_nssf = $total_employer_contribution = $vol_amount = 0;
	
	$result .= '
			</tr>
		</thead>
		<tbody>
	';
	
	foreach ($query->result() as $row)
	{
		$personnel_id = $row->personnel_id;
		$personnel_number = $row->personnel_number;
		$personnel_fname = $row->personnel_fname;
		$personnel_onames = $row->personnel_onames;
		$personnel_nssf_number = $row->personnel_nssf_number;
		$personnel_national_id_number = $row->personnel_national_id_number;
		$personnel_kra_pin = $row->personnel_kra_pin;
		
		$total_employer_contribution += $employer_contribution;
		$gross = 0;

		$name_parts = explode(' ', $personnel_fname);
		$first_name = $name_parts[0];

		// Get the other names by removing the first element and joining the rest back into a string
		$other_names = implode(' ', array_slice($name_parts, 1));


		if($payments->num_rows() > 0)
		{
			foreach($payments->result() as $res)
			{
				$payment_id = $res->payment_id;
				$payment_abbr = $res->payment_name;
				$table_id = $payment_id;
				
				if($total_payment_amount[$payment_id] != 0)
				{
					$payment_amt = 0;
					if(isset($payments_amount->$personnel_id->$table_id))
					{
						$payment_amt = $payments_amount->$personnel_id->$table_id;
					}
					$gross += $payment_amt;
					if(!isset($total_personnel_payments[$payment_id]))
					{
						$total_personnel_payments[$payment_id] = 0;
					}
					$total_personnel_payments[$payment_id] += $payment_amt;
				}
			}
		}

		//benefits
		$total_benefits = 0;
		if($benefits->num_rows() > 0)
		{
			foreach($benefits->result() as $res)
			{
				$benefit_id = $res->benefit_id;
				$benefit_name = $res->benefit_name;
				$table_id = $benefit_id;
								
				if($total_benefit_amount[$benefit_id] != 0)
				{
					$benefit_amt = 0;
					if(isset($benefits_amount->$personnel_id->$table_id))
					{
						$benefit_amt = $benefits_amount->$personnel_id->$table_id;
					}
					$total_benefits += $benefit_amt;
					if(!isset($total_personnel_benefits[$benefit_id]))
					{
						$total_personnel_benefits[$benefit_id] = 0;
					}
					$total_personnel_benefits[$benefit_id] += $benefit_amt;
				}
			}
		}
		
		//allowances
		if($allowances->num_rows() > 0)
		{
			foreach($allowances->result() as $res)
			{
				$allowance_id = $res->allowance_id;
				$allowance_name = $res->allowance_name;
				$table_id = $allowance_id;
				
				if($total_allowance_amount[$allowance_id] != 0)
				{
					$allowance_amt = 0;
					if(isset( $allowances_amount->$personnel_id->$table_id))
					{
						$allowance_amt =  $allowances_amount->$personnel_id->$table_id;
					}
					$gross += $allowance_amt;
					if(!isset($total_personnel_allowances[$allowance_id]))
					{
						$total_personnel_allowances[$allowance_id] = 0;
					}
					$total_personnel_allowances[$allowance_id] += $allowance_amt;
				}
			}
		}
		
		//overtime
		if($overtime->num_rows() > 0)
		{
			foreach($overtime->result() as $res)
			{
				$overtime_name = $res->overtime_name;
				$overtime_id = $res->overtime_type;
				$table_id = $overtime_id;
				if($total_overtime_amount[$overtime_id] != 0)
				{
					$overtime_amt = 0;
					if(isset( $overtime_amount->$personnel_id->$table_id))
					{
						$overtime_amt =  $overtime_amount->$personnel_id->$table_id;
					}
					$gross += $overtime_amt;
					if(!isset($total_personnel_overtime[$overtime_id]))
					{
						$total_personnel_overtime[$overtime_id] = 0;
					}
					$total_personnel_overtime[$overtime_id] += $overtime_amt;
				}
			}
		}

		// var_dump($gross);die();
		
		//basic
		$table_id = 0;
		$basic_pay = $this->payroll_model->get_payroll_amount($personnel_id, $payroll_items, $basic_pay_table, $table_id);
		//$total_basic_pay += $basic_pay;
		//$gross += $basic_pay;
		
		//nssf
		//$nssf = $this->payroll_model->get_payroll_amount($personnel_id, $payroll_items, $nssf_table, 1);
		$nssf = $nssf_amount->$personnel_id;
		$individual_nssf = $employer_contribution = $nssf;
		if($individual_nssf != 0)
		{
			$individual_nssf  = $individual_nssf + $employer_contribution;
		}
		$count++;
		$result .= 
		'
			<tr>
				<td style="text-align:left">'.$personnel_number.'</td>
				<td style="text-align:left">'.$other_names.'</td>
				<td style="text-align:left">'.$first_name.'</td>
				<td style="text-align:left">'.$personnel_national_id_number.'</td>
				<td style="text-align:left">'.$personnel_kra_pin.'</td>
				<td style="text-align:left">'.$personnel_nssf_number.'</td>
				<td style="text-align:left">'.number_format($gross,2).'</td>
			</tr>
		';
		$total_nssf+=$individual_nssf ;
		$total_vol_amount+=$vol_amount;
	}
	// $result .= '
	// 		<tr> 
	// 			<td colspan="4"></td>';
	// //gross
	// $result .= '
	// 		<th>'.number_format($total_nssf , 2, '.', ',').'</th>
	// 		<th>'.number_format($total_vol_amount, 2, '.', ',').'</th>
	// 		<th>'.number_format($total_nssf + $total_vol_amount, 2, '.', ',').'</th>
	// 		<th colspan="2"></th>
	// 	</tr> 
	// ';
	
	$result .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result = "There are no personnel";
}

?>

<!DOCTYPE html>
<html lang="en">
	<style type="text/css">
		.receipt_spacing{letter-spacing:0px; font-size: 12px;}
		.center-align{margin:0 auto; text-align:center;}
		
		.receipt_bottom_border{border-bottom: #888888 medium solid;}
		.row .col-md-12 table {
			border:solid #000 !important;
			border-width:1px 0 0 1px !important;
			font-size:10px;
		}
		.row .col-md-12 th, .row .col-md-12 td {
			border:solid #000 !important;
			border-width:0 1px 1px 0 !important;
		}
		
		.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
		.title-img{float:left; padding-left:30px;}
		img.logo{max-height:70px; margin:0 auto;}
	</style>
    <head>
        <title>NSSF Report</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css">
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>tableExport.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/sprintf.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jspdf.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/base64.js"></script>
         <script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script>

		<script src="<?php echo base_url()."assets/"?>table-resources/table2excel/jquery.table2excel.min.js"></script>

		 <script language="javascript">
		    function doc_keyUp(e) {
		        // this would test for whichever key is 40 and the ctrl key at the same time
		        if (e.keyCode === 69) {
		            alert("Your Excel Document is being prepared and should start downloading in a few. Don't hit E again until it does.");
		            // call your function to do the thing
		            downloadExcel();
		        }
		    }
		// register the handler 
		    document.addEventListener('keyup', doc_keyUp, false);
		    function downloadExcel() {
		    	// alert("sasa")
		        $("#testTable").table2excel({
		            exclude: ".noExl",
		            name: 'NSSF Report',
		            filename: "NSSF Report.xls",
		            fileext: ".xls",
		            exclude_img: true,
		            exclude_links: true,
		            exclude_inputs: true
		        });
		    }
		</script>
    </head>
    <body class="receipt_spacing">
    	<div class="row" >
        	<img src="<?php echo $branch_image_name;?>" alt="<?php echo $branch_name;?>" class="img-responsive logo"/>
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $branch_name;?><br/>
                    <?php echo $branch_address;?> <?php echo $branch_post_code;?> <?php echo $branch_city;?><br/>
                    E-mail: <?php echo $branch_email;?>. Tel : <?php echo $branch_phone;?><br/>
                    <?php echo $branch_location;?><br/>
                </strong>
            </div>
        </div>
        
      	<div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<h4><?php echo '<h3>NSSF for The month of '.date('M Y',strtotime($year.'-'.$month)).'</h3>';?></h4>
            </div>
        </div>
        
        <div class="col-md-12 receipt_bottom_border" >
        	<div class="col-md-12">
            	<?php echo $result;?>
            </div>
        	<div class="col-md-12 center-align">
            	<?php echo 'Prepared By: '.$prepared_by.' '.date('jS M Y H:i:s',strtotime(date('Y-m-d H:i:s')));?>
            </div>
        </div>
   <a href="#" onclick="javascript:xport.toCSV('testTable');">XLS</a>
<!--<a href="#" onClick ="$('#customers').tableExport({type:'csv',escape:'false'});">CSV</a>
<a href="#" onClick ="$('#customers').tableExport({type:'pdf',escape:'false'});">PDF</a>-->

    </body>
</html>
<script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>