
<?php
//$personnel_id = $this->session->userdata('personnel_id');
$created_by = $this->session->userdata('first_name');
$contacts = $this->site_model->get_contacts();

$engagement_date = '2024-07-07';//$engagement_date;


$current_month = date('m');
$current_year = date('Y');
$current_date = date('d');

$days_consumed = 0;

if($engagement_date != "0000-00-00" OR $engagement_date != null)
{
	// check if its in the current month

	$exploded_date = explode('-', $engagement_date);

	$explode_month = $exploded_date[1];
	$explode_year = $exploded_date[0];
	$explode_day = $exploded_date[2];

	if($explode_month == $current_month AND $explode_year == $current_year)
	{
		$days_consumed = (int)$explode_day;
	}

}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Payslip for <?php echo $personnel_name;?></title>
    <!-- For mobile content -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- IE Support -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css">
    <style type="text/css">
        .receipt_spacing {
            letter-spacing: 0px;
            font-size: 14px;
        }
        .center-align {
            margin: 0 auto;
            text-align: center;
        }
        .receipt_bottom_border {
            border-bottom: #888888 medium solid;
        }
        .row .col-md-12 table {
            border: solid #000 !important;
            border-width: 0px 0 0 1px !important;
            font-size: 12px;
        }
        .row .col-md-12 th, .row .col-md-12 td {
            border: solid #000 !important;
            border-width: 0 1px 1px 0 !important;
        }
        .row .col-md-12 .title-item {
            float: left;
            width: 130px;
            font-weight: bold;
            text-align: right;
            padding-right: 20px;
        }
        .title-img {
            float: left;
            padding-left: 30px;
        }
        img.logo {
            max-height: 70px;
            margin: 0 auto;
        }
        .col-xs-6 {
            text-align: right;
        }
        .col-xs-6#align-left {
            text-align: left !important;
            padding-left: 0px !important;
        }
        .row {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }
        /* Diagonal Overlay Styles */
        .overlay {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(255, 255, 255, 0.3); /* Semi-transparent background */
            color: #000;
            display: flex;
            justify-content: center;
            align-items: center;
            font-size: 34px;
            font-weight: bold;
            z-index: 10;
            pointer-events: none; /* Ensures clicks pass through */
            overflow: hidden;
        }
        .overlay::before {
            content: "Sample Payslip";
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%) rotate(-30deg); /* Center the text and rotate */
            font-size: 34px; /* Adjust font size */
            color: #000; /* Adjust text color */
            opacity: 0.5; /* Adjust text opacity */
            white-space: nowrap; /* Prevent text from wrapping */
        }
        .content {
            position: relative;
            z-index: 1; /* Ensure content is above the overlay */
        }
    </style>
</head>
<body class="receipt_spacing">
    <!-- Diagonal Overlay Element -->
    <div class="overlay"></div>

    <div class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                <div class="row">
                    <strong>
                        <h4><?php echo strtoupper($contacts['company_name']);?></h4>
                    </strong>
                    <h4><?php echo strtoupper($personnel_name) ?></br></h4>
                    <h5>STAFF NUMBER. <?php echo  $personnel_number ?></br>
                    NSSF NO.<?php  echo $nssf_number ?></br>
                    NHIF NO. <?php echo $nhif_number?></br>
                    KRA PIN: <?php echo  $kra_pin?></br></h5>
                </div>
                <div class="row">
                    <strong>EARNINGS</strong>
                </div>
                <div class="row">
                    <?php 
                    /*********** Payments ***********/
                    $total_payments = 0;
                                                                
                    if($payments->num_rows() > 0)
                    {
                        foreach ($payments->result() as $row2)
                        {
                            $payment_id = $row2->payment_id;
                            $payment_name = $row2->payment_name;
                            
                            if($personel_payments->num_rows() > 0){
                                foreach($personel_payments->result() as $allow){
                                    $id = $allow->id;
									
                                    
                                    if($id == $payment_id)
                                    {
                                        $amount = $allow->amount;

                                        if($days_consumed > 0 AND $amount > 0)
                						$amount = $this->payroll_model->calculate_gross_pay($days_consumed,$amount);


										if($amount != 0)
										{
											?>
											<div class="col-xs-6" id="align-left">
												<?php echo  strtoupper($payment_name);?>
											</div>
											
											<div class="col-xs-6">
												<?php echo number_format($amount, 2);?>
											</div>
											<?php
											break;
										}
                                    }
                            
                                    else{
                                        $amount = 0;
                                    }
                                }
                            }
                            
                            else{
                                $amount = 0;
                            }
                            $total_payments += $amount;
                        }
                    }


                  
        		

        			// var_dump($amount);die();
                    ?>
                </div>
                
                <!-- Allowances header -->
                <div class="row">
                        <strong>NON CASH BENEFITS</strong>
                </div>
                <!-- End allowances header -->
                
                <!-- Allowances -->
                <div class="row">
                    <?php
                    
                    /*********** Non cash benefits ***********/
                    $total_benefits = 0;
                                                                
                    if($benefits->num_rows() > 0)
                    {
                        foreach ($benefits->result() as $row2)
                        {
                            $benefit_id = $row2->benefit_id;
                            $benefit_name = $row2->benefit_name;
                            $benefit_taxable = $row2->benefit_taxable;
                            
                            if($personnel_benefits->num_rows() > 0)
                            {
                                foreach($personnel_benefits->result() as $allow)
                                {
                                    $id = $allow->id;
                                    
                                    if($id == $benefit_id){
                                        $amount = $allow->amount;
										if($amount > 0)
										{
											?>
											<div class="col-xs-6" id="align-left">
												<?php echo strtoupper($benefit_name);?>
											</div>
											
											<div class="col-xs-6">
												<?php echo number_format($amount, 2);?>
											</div>
											<?php
											break;
										}
                                    }
                            
                                    else{
                                        $amount = 0;
                                    }
                                }
                            }
                            
                            else{
                                $amount = 0;
                            }
							
							if($benefit_taxable == 1)
							{
                            	$total_benefits += $amount;
							}
                        }
                    }
                    ?>
                </div>
                
                <!-- Allowances header -->
                <div class="row">
                        <strong>ALLOWANCES</strong>
                </div>
                <!-- End allowances header -->
                
                <!-- Allowances -->
                <div class="row">
                    <?php
                    
                    /*********** Allowances ***********/
                    $total_allowances = 0;
                    $total_taxable_allowances = 0;
                                                                
                    if($allowances->num_rows() > 0)
                    {
                        foreach ($allowances->result() as $row2)
                        {
                            $allowance_id = $row2->allowance_id;
                            $allowance_name = $row2->allowance_name;
                            $allowance_taxable = $row2->allowance_taxable;
                            
                            if($personnel_allowances->num_rows() > 0){
                                foreach($personnel_allowances->result() as $allow){
                                    $id = $allow->id;
                                    
                                    if($id == $allowance_id){
                                        $amount = $allow->amount;
										if($amount != 0)
										{
											?>
											<div class="col-xs-6" id="align-left">
												<?php echo strtoupper($allowance_name);?>
											</div>
											
											<div class="col-xs-6">
												<?php echo number_format($amount, 2);?>
											</div>
											<?php
											break;
										}
                                    }
                            
                                    else{
                                        $amount = 0;
                                    }
                                }
                            }
                            
                            else{
                                $amount = 0;
                            }
							// var_dump($allowance_taxable);die();
							if($allowance_taxable == 2)
                            	$total_allowances += $amount;
							if($allowance_taxable == 1)
								$total_taxable_allowances += $amount;
                        }
                    }
					//overtime
					$total_overtime = 0;
                                                       
                    if($overtime->num_rows() > 0)
                    { 
                        foreach ($overtime->result() as $row2)
                        {
                            $overtime_id = $row2->overtime_type;
                            $overtime_name = $row2->overtime_name;
                           
                            if($personnel_overtime->num_rows() > 0)
							{ $overtime_amount =0;  
                                foreach($personnel_overtime->result() as $ot)
								{
									$id = $ot->id;
									$personnel_overtime_hours = $ot->amount;
									$overtime_type = $ot->overtime_type;
									 $overtime_type_rate = $ot->overtime_type_rate;
									
									//calculate overtime
									if($overtime_type_rate == 1)
									{
										if($payments->num_rows() > 0)
										{
											foreach ($payments->result() as $row2)
											{
												$payment_id = 1;
												$basic_pay =  $payments_amount->$personnel_id->$table_id;
												 foreach($personel_payments->result() as $allow)
												 {
                                   					 $basic_id = $allow->id;
                                    
													if($basic_id == $payment_id)
													{
														 $basic_pay = $allow->amount;
													}
												}
											}
										}
										if($overtime_type == 1)
										{
											$overtime_rate = $this->config->item('normal_overtime_rate');
											//var_dump($overtime_rate);die();
										}
										else if($overtime_type == 2)
										{
											$overtime_rate = $this->config->item('holiday_overtime_rate');
											
											
										}
										if ($branch_working_hours > 0)
										{
											 $overtime_amount = ($basic_pay * $overtime_rate * $personnel_overtime_hours) / $branch_working_hours;
										}
															
									}
									else
									{
										$overtime_amount = $personnel_overtime_hours;
										
									}
                                    if($id == $overtime_id)
									{
                                        //$amount = $total_overtime;
										//var_dump($overtime_amount);
										if($overtime_amount > 0)
										{
											
											?>
											<div class="col-xs-6" id="align-left">
												<?php echo strtoupper($overtime_name);?>
											</div>
											
											<div class="col-xs-6">
												<?php echo number_format($overtime_amount, 2);?>
											</div>
											<?php
											
										}
                                    }
                                }
								$total_overtime = $total_overtime + $overtime_amount;	
                            }
							 
                        }
                    }
					//var_dump($total_overtime);
                    /*********** Taxable ***********/
					$gross = ($total_payments + $total_allowances + $total_taxable_allowances + $total_overtime);
					// var_dump($gross);die();

					$gross_taxable = $total_payments  + $total_taxable_allowances + $total_benefits + $total_overtime;


					// $housing_levy = $gross_taxable * (1.5/100);
					$housing_levy = ($gross_taxable * (1.5/100));
					$housing_session = $this->session->userdata('housing_levy');

					if(empty($housing_session))
						$housing_levy = 0;




					$nssf = 0;
					$taxable = 0;
					$paye = 0;
					$paye_less_relief = 0;
					$monthly_relief = 0;
					$insurance_relief = 0;
					$insurance_amount = 0;
						
					/*********** NSSF ***********/
					// $nssf_query = $this->payroll_model->get_nssf();
					
					// if($nssf_query->num_rows() > 0)
					// {
					// 	foreach ($nssf_query->result() as $row2)
					// 	{
					// 		$nssf_id = $row2->nssf_id;
					// 		$nssf = $row2->amount;
					// 		$nssf_percentage = $row2->percentage;
					
					// 		if($nssf_percentage == 1)
					// 		{
					// 			$nssf_deduction_amount = $gross_taxable;
								
					// 			if($nssf_deduction_amount > 18000)
					// 			{
					// 				$nssf_deduction_amount = 18000;
					// 			}
					// 			$nssf = $nssf_deduction_amount * ($nssf/100);
					// 		}
					// 	}
					// }




					$nssf_session = $this->session->userdata('nssf_rate');
					$nssf_charges = $this->payroll_model->calculate_nssf($gross,$nssf_session);
					$tier_one = $nssf_charges['tier_one'];
					$tier_two = $nssf_charges['tier_two'];
					$nssf = $nssf_charges['nssf'];


					$taxable = $gross_taxable - $nssf;

					 /*********** NHIF ***********/
					// $nhif_query = $this->payroll_model->calculate_nhif($gross);
					// $nhif = 0;
					
					// if($nhif_query->num_rows() > 0)
					// {
					// 	foreach ($nhif_query->result() as $row2)
					// 	{
					// 		$nhif = $row2->amount;
					// 	}
					// }

					$nhif_session = $this->session->userdata('nhif_rate');
					$nhif = $this->payroll_model->nhif_view($gross,$nhif_session);
					

					
					if($taxable > 10164)
					{
						$monthly_relief = $this->payroll_model->get_monthly_relief();
						$insurance_res = $this->payroll_model->get_insurance_relief($personnel_id);
						$insurance_relief = $insurance_res['relief'];
						$insurance_amount = $insurance_res['amount'];
						
						/*********** PAYE ***********/
						$paye = $this->payroll_model->calculate_paye($taxable);

						$paye_less_relief = $paye - $monthly_relief - $insurance_relief;
						// $paye_less_relief = $paye;
						
						if($paye_less_relief < 0)
						{
							$paye_less_relief = 0;
						}

						// var_dump($insurance_relief);die();

						$nhif_relief = $this->payroll_model->get_nhif_relief($nhif);


						$housing_levy_relief = 0;
						if($housing_levy > 0)
							$housing_levy_relief = 0.15 * $housing_levy;

						$paye_less_relief = $paye_less_relief - $nhif_relief - $housing_levy_relief;




						// var_dump($nhif_relief);die();
					}
					
                    
                   
					if($paye_less_relief < 0)
						$paye_less_relief = 0;


					
					// Secondary Employees have no statutories involed
					// var_dump($nhif_relief );die();


					
                    ?>
                </div>
                
                <!-- Taxable -->
                <div class="row">
                	<div class="col-xs-6" id="align-left">
                        <strong>TOTAL EARNINGS</strong>
                    </div>
                    
                    <div class="col-xs-6">
                        <?php echo number_format(($gross), 2);?>
                    </div>
                </div>
				
				<div class="row">
                	 <strong>P.A.Y.E </strong>
                </div>
				
                <!-- End taxable -->
                <div class="row">
                	<div class="col-xs-6" id="align-left">
                       LIABLE PAY
                    </div>
                    
                    <div class="col-xs-6">
                       <?php echo number_format(($gross_taxable), 2);?>
                    </div>
                </div>
                <!-- NSSF -->
                <div class="row">
                	<div class="col-xs-6" id="align-left">
                        LESS PENSIONS/NSSF
                    </div>
                    
                    <div class="col-xs-6">
                       (<?php echo number_format(($nssf), 2);?>)
                    </div>
                </div>
				
                <!-- End NSSF -->
                <div class="row">
                	<div class="col-xs-6" id="align-left">
                        CHARGEABLE AMT KSHS
                    </div>
                    
                    <div class="col-xs-6">
                       <?php echo number_format(($taxable), 2);?>
                    </div>
                </div>
                
                 <div class="row">
                	<div class="col-xs-6" id="align-left">
                        TAX CHARGED
                    </div>
                    
                    <div class="col-xs-6">
                       <?php echo number_format(($paye), 2);?>
                    </div>
                </div>
				 <div class="row">
                	<div class="col-xs-6" id="align-left">
                        PERSONAL RELIEF
                    </div>
                    
                    <div class="col-xs-6">
                       <?php echo number_format(($monthly_relief), 2);?>
                    </div>
                </div>
                <!-- End allowances-->
                <!-- Insurance relief -->
                <?php if($insurance_relief > 0){?>
                <div class="row">
                	<div class="col-xs-6" id="align-left">
                        INSURANCE RELIEF
                    </div>
                    
                    <div class="col-xs-6">
                        <?php echo number_format(($insurance_relief), 2);?>
                    </div>
                </div>
                <?php }?>
                <!-- End insurance relief -->
                <div class="row">
                	 <strong>DEDUCTIONS </strong>
                </div>
                <!-- PAYE -->
                <div class="row">
                	<div class="col-xs-6" id="align-left">
                       PAYE
                    </div>
                    
                    <div class="col-xs-6">
                        <?php echo number_format(($paye_less_relief), 2);?>
                    </div>
                </div>
                <!-- End PAYE -->
                
                <!-- Life insurance -->
                <?php if($insurance_amount > 0){?>
                <div class="row">
                	<div class="col-xs-6" id="align-left">
                        LIFE INSURANCE
                    </div>
                    
                    <div class="col-xs-6">
                        <?php echo number_format(($insurance_amount), 2);?>
                    </div>
                </div>
                <?php }?>
                <!-- End life insurance -->
                
                <!-- Deductions -->
                <div class="row">
                    <div class="col-xs-6" id="align-left">
                        NSSF
                    </div>
                    
                    <div class="col-xs-6">
                        <?php echo number_format($nssf, 2);?>
                    </div>
                  
                    <div class="col-xs-6" id="align-left">
                        NHIF
                    </div>
                    
                    <div class="col-xs-6">
                        <?php echo number_format($nhif, 2);?>
                    </div>

                    <!-- PAYE -->
	                <div class="row">
	                	<div class="col-xs-6" id="align-left">
	                       HOUSING LEVY
	                    </div>
	                    
	                    <div class="col-xs-6">
	                        <?php echo number_format(($housing_levy), 2);?>
	                    </div>
	                </div>
	                <!-- End PAYE -->

	                
                    <?php
					
                    /*********** Deductions ***********/
					$total_deductions = 0;
											
					if($deductions->num_rows() > 0)
					{
						foreach ($deductions->result() as $row2)
						{
							$deduction_id = $row2->deduction_id;
							$deduction_name = $row2->deduction_name;
							$deduction_taxable = $row2->deduction_taxable;
							
							if($personnel_deductions->num_rows() > 0){
								foreach($personnel_deductions->result() as $allow){
									$id = $allow->id;
									
									if($id == $deduction_id){
										$amount = $allow->amount;
										if($amount > 0)
										{
											?>
											<div class="col-xs-6" id="align-left">
												<?php echo strtoupper($deduction_name);?>
											</div>
											
											<div class="col-xs-6">
												<?php echo number_format($amount, 2);?>
											</div>
											<?php
											break;
										}
									}
							
									else{
										$amount = 0;
									}
								}
							}
							
							else{
								$amount = 0;
							}
							$total_deductions += $amount;
						}
					}
					
                    /*********** Other deductions ***********/
					$total_other_deductions = 0;
											
					if($other_deductions->num_rows() > 0)
					{
						foreach ($other_deductions->result() as $row2)
						{
							$other_deduction_id = $row2->other_deduction_id;
							$other_deduction_name = $row2->other_deduction_name;
							$other_deduction_taxable = $row2->other_deduction_taxable;
							
							if($personnel_other_deductions->num_rows() > 0){
								foreach($personnel_other_deductions->result() as $allow){
									$id = $allow->id;
									
									if($id == $other_deduction_id){
										$amount = $allow->amount;
										if($amount > 0)
										{
											?>
											<div class="col-xs-6" id="align-left">
												<?php echo strtoupper($other_deduction_name);?>
											</div>
											
											<div class="col-xs-6">
												<?php echo number_format($amount, 2);?>
											</div>
											<?php
											break;
										}
									}
							
									else{
										$amount = 0;
									}
								}
							}
							
							else{
								$amount = 0;
							}
							$total_other_deductions += $amount;
						}
					} ?>
                </div>
                <!-- End deductions -->
                
                <!-- Savings header -->
                <div class="row">
                	<strong>SAVINGS </strong>
                </div>
                <!-- End savings header -->
                
                <!-- Savings -->
                <div class="row">
                    <?php
                    
                    $total_savings = 0;
											
					if($savings->num_rows() > 0)
					{
						foreach ($savings->result() as $row2)
						{
							$saving_id = $row2->savings_id;
							$saving_name = $row2->savings_name;
							
							if($personnel_savings->num_rows() > 0){
								foreach($personnel_savings->result() as $allow){
									$id = $allow->id;
									
									if($id == $saving_id){
										$amount = $allow->amount;
										$opening = $allow->personnel_savings_opening;
										?>
                                        <div class="col-xs-6" id="align-left">
                                        	<?php echo $saving_name;?>
                                        </div>
                                        
                                        <div class="col-xs-6">
                                            <?php echo number_format($amount, 2);?>
                                        </div>
                                        <?php
										break;
									}
							
									else{
										$amount = 0;
										$opening = 0;
									}
								}
							}
							
							else{
								$amount = 0;
								$opening = 0;
							}
							$total_savings += ($amount);
						}
					}
                    ?>
                </div>
                <!-- End savings -->
                
                <!-- Loans header -->
                <div class="row">
                    <strong> LOANS </strong>
                </div>
                <!-- End loans header -->
                
                <!-- Loans -->
				<?php
                
                $total_loan_schemes = 0;
                                        
                if($loan_schemes->num_rows() > 0)
                {
                    $amount = 0;
                    $monthly = 0;
                    $interest = 0;
                    $interest2 = 0;
                    $sdate = '';
                    $edate = '';
                    $today = date("y-m-d");
                    $prev_payments = "";
                    $prev_interest = "";
                    
                    foreach ($loan_schemes->result() as $row2)
                    {
                        $loan_scheme_id = $row2->loan_scheme_id;
                        $loan_scheme_name = $row2->loan_scheme_name;
                        
                        if($personnel_loan_schemes->num_rows() > 0)
                        {
                            foreach($personnel_loan_schemes->result() as $open)
                            {
                                $id = $open->id;
                                
                                if($id == $loan_scheme_id)
                                {
                                    $amount = $open->amount;
									
									if($amount > 0)
									{
										$monthly = $open->monthly;
										$interest = $open->interest;
										$interest2 = $open->interest2;
										$sdate = $open->sdate;
										$edate = $open->edate;
										$prev_payments = $monthly * $this->payroll_model->dateDiff($sdate.' 00:00', $today.' 00:00', 'month');
										$prev_interest = $interest * $this->payroll_model->dateDiff($sdate.' 00:00', $today.' 00:00', 'month');
										$loan_balance = ($amount-$prev_payments);
										$interest_balance = ($interest2-$prev_interest);
										?>
										<div class="row">
											<div class="col-xs-6" id="align-left">
												<?php echo $loan_scheme_name;?>
											</div>
											
											<div class="col-xs-6" style="text-align:left">
												<?php echo number_format($amount, 2);?>
											</div>
										</div>
										
										<div class="row">
											<div class="col-xs-6" id="align-left">
												<?php echo $loan_scheme_name;?> Int
											</div>
											
											<div class="col-xs-6" style="text-align:left">
												<?php echo number_format($interest2, 2);?>
											</div>
										</div>
										
										<div class="row">
											<div class="col-xs-6" id="align-left">
												<?php echo $loan_scheme_name;?> PM
											</div>
											
											<div class="col-xs-6">
												<?php echo number_format($monthly, 2);?>
											</div>
										</div>
										
										<div class="row">
											<div class="col-xs-6" id="align-left">
												<?php echo $loan_scheme_name;?> Int PM
											</div>
											
											<div class="col-xs-6" style="text-align:left">
												<?php echo number_format($interest, 2);?>
											</div>
										</div>
										
										<div class="row">
											<div class="col-xs-6" id="align-left">
												<?php echo $loan_scheme_name;?> Bal
											</div>
											
											<div class="col-xs-6" style="text-align:left">
												<?php echo number_format($loan_balance, 2);?>
											</div>
										</div>
										
										<div class="row">
											<div class="col-xs-6" id="align-left">
												<?php echo $loan_scheme_name;?> Int Bal
											</div>
											
											<div class="col-xs-6" style="text-align:left">
												<?php echo number_format($interest_balance, 2);?>
											</div>
										</div>
										<?php
										break;
									}
                                }
                            }
                        }
                        $total_loan_schemes += $monthly;
                    }
                }
				
				//$all_deductions = $paye_less_relief + $nssf + $nhif + $total_deductions + $total_other_deductions + $total_loan_schemes + $total_savings + $insurance_amount;
				$all_deductions = $paye_less_relief + $nssf + $nhif + $total_deductions + $total_other_deductions + $total_loan_schemes + $total_savings + $housing_levy;
				
				$net_pay = $gross - $all_deductions;
                ?>
                <!-- End loans -->
               
                <!-- End Gross -->
                
                <!-- Total deductions -->
                <div class="row">
                	<div class="col-xs-6" id="align-left">
                        <strong>TOTAL DEDUCTIONS</strong>
                    </div>
                    
                    <div class="col-xs-6">
                        <?php echo number_format(($all_deductions), 2);?>
                    </div>
                </div>
                <!-- End Total deductions -->
								
                <!-- Net pay -->		
                <div class="row">
                    <div class="col-xs-6" id="align-left">
                        <strong>NET PAY</strong>
                    </div>
                    
                    <div class="col-xs-6">
                        <?php 
						if($net_pay < 0)
						{	
							$net_pay = 0;
						}echo number_format(($net_pay), 2);?>
                    </div>
                </div>
                <!-- End Net -->
               
            </div>
        </div>
        
    </body>
</html>
