<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a href="'.site_url().'payroll/salary-data/personnel_fname/'.$order_method.'/'.$page.'">Name</a></th>
						<th><a href="'.site_url().'payroll/salary-data/personnel_fname/'.$order_method.'/'.$page.'">Phone</a></th>
						<th><a href="'.site_url().'payroll/salary-data/personnel_fname/'.$order_method.'/'.$page.'">Email</a></th>
						<th><a href="'.site_url().'payroll/salary-data/personnel_phone/'.$order_method.'/'.$page.'">Bank</a></th>
						<th><a href="'.site_url().'payroll/salary-data/personnel_fname/'.$order_method.'/'.$page.'">Bank Branch</a></th>
						<th><a href="'.site_url().'payroll/salary-data/personnel_phone/'.$order_method.'/'.$page.'">Account Number</a></th>
						<!--<th>Basic</th>
						<th>Allowances</th>-->
						<th>Gross</th>
						<th>PAYE</th>
						<th>NSSF</th>
						<th>NHIF</th>
						<th>Housing Levy</th>
						<th>Deductions</th>
						<th>Net pay</th>
						<th colspan="3">Actions</th>
					</tr>
				</thead>
				<tbody>
			';
			
			foreach ($query->result() as $row)
			{
				$personnel_id = $row->personnel_id;
				$branch_name = $row->branch_name;
				$personnel_fname = $row->personnel_fname;
				$personnel_onames = $row->personnel_onames;
				$personnel_username = $row->personnel_username;
				$personnel_phone = $row->personnel_phone;
				$personnel_email = $row->personnel_email;
				$personnel_status = $row->personnel_status;
				$bank_name = $row->bank_name;
				$bank_branch_name = $row->bank_branch_name;
				$bank_account_number = $row->bank_account_number;
				$bank_code = $row->bank_code;
				$bank_branch_code = $row->bank_branch_code;
				$personnel_email = $row->personnel_email;
				$engagement_date = $row->engagement_date;
				$personnel_name = $personnel_fname.' '.$personnel_onames;

				$current_month = date('m');
				$current_year = date('Y');
				$current_date = date('d');

				$days_consumed = 0;

				if($engagement_date != "0000-00-00" OR $engagement_date != null)
				{
					// check if its in the current month

					$exploded_date = explode('-', $engagement_date);

					$explode_month = $exploded_date[1];
					$explode_year = $exploded_date[0];
					$explode_day = $exploded_date[2];

					if($explode_month == $current_month AND $explode_year == $current_year)
					{
						$days_consumed = (int)$explode_day;
					}

				}
				
				//get salary details
				$payments = $this->payroll_model->payments_view($personnel_id);

				if($days_consumed > 0 AND $payments > 0)
				$payments = $this->payroll_model->calculate_gross_pay($days_consumed,$payments);

                					
				$benefits = $this->payroll_model->benefits_view($personnel_id);
				$allowances = $this->payroll_model->allowances_view_separated_by_type($personnel_id);
				$taxable_allowance = $allowances['taxable_allowance'];
				$allowance = $allowances['allowance'];
				$deductions = $this->payroll_model->deductions_view($personnel_id);
				$other_deductions = $this->payroll_model->other_deductions_view($personnel_id);
				$savings = $this->payroll_model->savings_view($personnel_id);
				$loan_schemes = $this->payroll_model->scheme_view($personnel_id);
                $monthly_relief = $this->payroll_model->get_monthly_relief();
				$insurance_res = $this->payroll_model->get_insurance_relief($personnel_id);
				$insurance_relief = $insurance_res['relief'];
				$insurance_amount = $insurance_res['amount'];
				
				$taxable = $payments + $benefits + $taxable_allowance;
				$gross = ($payments + $benefits + $taxable_allowance);

				$housing_levy = $taxable * (1.5/100);
				$housing_session = $this->session->userdata('housing_levy');
				if(empty($housing_session))
					$housing_levy = 0;


				$nssf_session = $this->session->userdata('nssf_rate');
				if(empty($nssf_session))
					$nssf_session = 0;

				// $nssf = $this->payroll_model->nssf_view($gross);
				$nssf_charges = $this->payroll_model->calculate_nssf($gross,$nssf_session);
				$tier_one = $nssf_charges['tier_one'];
				$tier_two = $nssf_charges['tier_two'];
				$nssf = $nssf_charges['nssf'];
				// $paye = $this->payroll_model->calculate_paye($taxable - $nssf);
				// var_dump($gross);die();
				// $nhif_query = $this->payroll_model->calculate_nhif($gross);
				// $nhif = 0;
				
				// if($nhif_query->num_rows() > 0)
				// {
				// 	foreach ($nhif_query->result() as $row2)
				// 	{
				// 		$nhif = $row2->amount;
				// 	}
				// }


				$nhif_session = $this->session->userdata('nhif_rate');
				if(empty($nhif_session))
					$nhif_session = 0;

				
				$nhif = $this->payroll_model->nhif_view($gross,$nhif_session);
				$taxable  = $taxable - $nssf;
				// $gross -= $nssf;
				$paye = 0;
				if($taxable > 10164)
				{
					$monthly_relief = $this->payroll_model->get_monthly_relief();
					$insurance_res = $this->payroll_model->get_insurance_relief($personnel_id);
					$insurance_relief = $insurance_res['relief'];
					$insurance_amount = $insurance_res['amount'];
					
					/*********** PAYE ***********/
					$paye = $this->payroll_model->calculate_paye($taxable);

					$paye = $paye - $monthly_relief - $insurance_relief;
					// $paye_less_relief = $paye;
					
				

					$nhif_relief = $this->payroll_model->get_nhif_relief($nhif);

					$housing_levy_relief = 0;
					if($housing_levy > 0)
						$housing_levy_relief = 0.15 * $housing_levy;

					$paye = $paye - $nhif_relief - $housing_levy_relief;

					// var_dump($paye);die();
				}


				if($paye < 0)
					$paye = 0;
				// $paye = $paye - ($insurance_relief + $monthly_relief);
				// $nhif = $this->payroll_model->nhif_view($gross);
				$total_deductions = $nssf + $nhif + $deductions + $other_deductions + $paye + $savings + $loan_schemes +$housing_levy;
				$net = $gross - $total_deductions;
				$count++;
				// <td><a href="'.site_url().'payroll/add-overtime-hours/'.$personnel_id.'" class="btn btn-xs btn-primary" title="Overtime for '.$personnel_name.'"><i class="fa fa-clock-o"></i> Overtime</a></td>
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$personnel_fname.'</td>
						<td>'.$personnel_phone.'</td>
						<td>'.$personnel_email.'</td>
						<td>'.$bank_name.' ('.$bank_code.')</td>
						<td>'.$bank_branch_name.' ('.$bank_branch_code.')</td>
						<td>'.$bank_account_number.'</td>
						<!--<td>'.number_format($payments, 2).'</td>
						<td>'.number_format($allowances, 2).'</td>-->
						<td>'.number_format($gross, 2).'</td>
						<td>'.number_format($paye, 2).'</td>
						<td>'.number_format($nssf, 2).'</td>
						<td>'.number_format($nhif, 2).'</td>
						<td>'.number_format($housing_levy, 2).'</td>
						<td>'.number_format($total_deductions, 2).'</td>
						<td>'.number_format($net, 2).'</td>
						<td><a href="'.site_url().'payroll/payment-details/'.$personnel_id.'" class="btn btn-xs btn-success" title="Edit '.$personnel_name.'"><i class="fa fa-money"></i> Allocations</a></td>
						
						<td><a href="'.site_url().'payroll/payroll/view-payslip/'.$personnel_id.'" class="btn btn-xs btn-info" title="Payslip for '.$personnel_name.'" target="_blank">Sample Payslip</td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no personnel";
		}
?>

 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search personnel</h2>
    </header>
    
    <!-- Widget content -->
   <div class="panel-body">
    	<div class="padd">
			<?php
            echo form_open("payroll/payroll/search_personnel", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Branch: </label>
                        
                        <div class="col-md-8">
                            <select class="form-control" name="branch_id">
                            	<option value="">---Select Branch---</option>
                                <?php
                                    if($branches->num_rows() > 0){
                                        foreach($branches->result() as $row):
                                            $branch_name = $row->branch_name;
                                            $branch_id= $row->branch_id;
                                            ?><option value="<?php echo $branch_id; ?>" ><?php echo $branch_name; ?></option>
                                        <?php	
                                        endforeach;
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                   
                </div>
                <div class="col-md-3">
                	 <div class="form-group">
                        <label class="col-md-4 control-label">Personnel Number: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="personnel_number" placeholder="Personnel number">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Name: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="personnel_fname" placeholder="Staff name">
                        </div>
                    </div>
                    
                    <div class="form-group" style="display:none">
                        <label class="col-md-4 control-label">Other names: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="personnel_onames" placeholder="Other names">
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                	<div class="col-md-8 col-md-offset-4">
                        	<div class="center-align">
                            	<button type="submit" class="btn btn-info btn-sm">Search</button>
                            </div>
                        </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
    </div>
</section>

						<section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?></h2>
								<div class="pull-right" style="margin-top:-20px;">
                                    <div class="col-sm-2 col-sm-offset-10">
                                        <a href="<?php echo site_url();?>payroll/payroll" class="btn btn-xs btn-warning pull-right">Back to payroll</a>
                                        
                                    </div>
                                </div>
							</header>
							<div class="panel-body">
								<div class="col-md-12">

									<?php
									$search = $this->session->userdata('personnel_search_title');
									
									if(!empty($search))
									{
										echo '<h6>Filtered by: '.$search.'</h6>';
										echo '<a href="'.site_url().'payroll/payroll/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
									}
	                                $success = $this->session->userdata('success_message');
			

									if(!empty($success))
									{
										echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
										$this->session->unset_userdata('success_message');
									}
									
									$error = $this->session->userdata('error_message');
									
									if(!empty($error))
									{
										echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
										$this->session->unset_userdata('error_message');
									}
									?>
									
								</div>
                            	
                            	
                            	
                                
								<div class="col-md-12">
									<div class="table-responsive">
	                                	
										<?php echo $result;?>
								
	                                </div>
	                            </div>
							</div>
                            <div class="panel-footer">
                           
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>