<?php
    $this->load->helper('cookie'); 
    $this->site_model->get_company();
    $active = get_cookie('active', TRUE);
    if(!isset($contacts))
    {
        $contacts = $this->site_model->get_contacts();
    }
    $data['contacts'] = $contacts = $contacts;

     

    $queue_activated = $contacts['queue_notification'];

    $personnel_id = $this->session->userdata('personnel_id');

    if($personnel_id > 0)
    {
        $department_id = $this->site_model->get_personnel_department_id($personnel_id);
    }
    else
    {
        $department_id = 0;
    }


?>
<!doctype html>
<html class="fixed sidebar-left-collapsed">
    <head>
        <?php echo $this->load->view('admin/includes/header', $contacts, TRUE); ?>
        <style>
        #cover-spin {
            position:fixed;
            width:100%;
            left:0;right:0;top:0;bottom:0;
            background-color: rgba(255,255,255,0.7);
            z-index:9999;
            display:none;
        }

        @-webkit-keyframes spin {
          from {-webkit-transform:rotate(0deg);}
          to {-webkit-transform:rotate(360deg);}
        }

        @keyframes spin {
          from {transform:rotate(0deg);}
          to {transform:rotate(360deg);}
        }

        #cover-spin::after {
            content:'';
            display:block;
            position:absolute;
            left:48%;top:40%;
            width:40px;height:40px;
            border-style:solid;
            border-color:black;
            border-top-color:transparent;
            border-width: 4px;
            border-radius:50%;
            -webkit-animation: spin .8s linear infinite;
            animation: spin .8s linear infinite;
        }
        .custom-overlay {
            position: fixed;
            top: 0;
            z-index: 10000;
            width: 100%;
            height: 100%;
            background-color: rgba(0,0,0,0.5);
            display: flex;
            justify-content: center;
            align-items: center;
            cursor: not-allowed;
        }

        .custom-overlay-content {
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0,0,0,0.5);
            display: flex;
        }

        /* image */
        .custom-overlay-content img {
            flex: 1;
        }

        .custom-overlay-info {
            width: 400px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            text-align: center;
            flex: 1;
        }

        .custom-overlay-info h3 {
            color: red;
            font-size: 24px;
            margin-bottom: 10px;
        }

        .custom-overlay-info p {
            font-size: 16px;
            margin-bottom: 10px;
        }

        .custom-overlay-info small {
            font-size: 14px;
            margin-bottom: 10px;
        }

        .custom-overlay-info .reasons {
            
            margin-top: 20px;
        }

        /* ul */
        .custom-overlay-info .reasons ul {
            list-style: none;
            padding: 0;
        }

        .custom-overlay-info .reasons ul li {
            font-size: 14px;
            margin-bottom: 5px;
            font-weight: lighter;
        }

        .custom-overlay-info .link-button {
            display: inline-block;
            padding: 10px 20px;
            background-color: #f00;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            margin-top: 20px;
            cursor: pointer;
        }

        </style>
    </head>

    <body>
        <div id="cover-spin"></div>
        <input type="hidden" id="base_url" value="<?php echo site_url();?>">
        <input type="hidden" id="config_url" value="<?php echo site_url();?>">
         <input type="hidden" id="department_id_one" value="<?php echo $department_id;?>">
         <input type="hidden" id="redirect_url" value="<?php echo $this->uri->uri_string();?>">
          <input type="hidden" id="queue_activated" value="<?php echo $queue_activated;?>">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
        <section class="body">
            <!-- Top Navigation -->

            <!-- if not active display overlay  -->
            <?php if($active == 0) { ?>
                <div class="custom-overlay">
                    <div class="custom-overlay-content">
                        <!-- image goes here -->
                        <img src="<?php echo base_url().'assets/img/7210975.jpg'?>" height="35" alt="<?php echo $company_name;?>" class="img-responsive" width="400" />
                        <div class="custom-overlay-info">
                            <h3>Account not active</h1>
                            <p>Your account is not active.</p>
                            <small>Please contact the administrator.</small>
                            <div class="reasons">
                                <p>Possible reasons:</p>
                                <ul>
                                   <!--  <li>Account payment due</li>
                                    <li>Account deactivated</li> -->
                                    <li>Account has been suspended</li>
                                    <li>Please contact us +254705465644</li>
                                </ul>
                            </div>
                            <small>Already paid? Logout and please  login in</small>
                            <li>
								<a  tabindex="-1" href="<?php echo site_url()."logout-admin";?>" class="link-button">Logout</a>
							</li>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php echo $this->load->view('admin/includes/top_navigation', $data, TRUE); ?>


            <div class="inner-wrapper">
                <?php echo $this->load->view('admin/includes/top_level_navigation', '', TRUE); ?>


                <section role="main" class="content-body">
                    

                    <?php echo $content;?>

                </section>
            </div>


            <aside id="sidebar-right" class="sidebar-right" style="display: none;">
                <div class="nano has-scrollbar">
                    <div class="nano-content" tabindex="0" style="right: -17px;">
                        <!-- <a href="#" class="mobile-close d-md-none">
                            Collapse <i class="fa fa-chevron-right"></i>
                        </a> -->

                        <div class="sidebar-right-wrapper">
                            <div id="current-sidebar-div"></div>
                            <div id="existing-sidebar-div"></div>
                            <div id="sidebar-div"></div>

                        </div>
                    </div>
                <div class="nano-pane" style="opacity: 1; visibility: visible;"><div class="nano-slider" style="height: 189px; transform: translate(0px);"></div></div></div>
            </aside>

        </section>

        <!-- Vendor -->

        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/pnotify/pnotify.custom.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-cookie/jquery.cookie.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/js/bootstrap.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/nanoscroller/nanoscroller.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/magnific-popup/magnific-popup.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>

        <!-- Specific Page Vendor -->
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-appear/jquery.appear.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.pie.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.categories.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/flot/jquery.flot.resize.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/raphael/raphael.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/morris/morris.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/gauge/gauge.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/snap-svg/snap.svg.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/liquid-meter/liquid.meter.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/jquery.vmap.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/select2/select2.js"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/assets/javascripts/";?>theme.js"></script>
        <script src="<?php echo base_url()."assets/themes/jasny/js/jasny-bootstrap.js";?>"></script>

        <!-- Theme Custom -->
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.custom.js"></script>

        <!-- Theme Initialization Files -->
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/theme.init.js"></script>

        <!-- Example -->
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/dashboard/examples.dashboard.js"></script>
        <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/javascripts/charts.js"></script>
        <!-- s -->
        <!-- Full Google Calendar - Calendar -->
        <!-- <script src="<?php echo base_url()."assets/bluish/"?>js/fullcalendar.min.js"></script>  -->
        <script src='<?php echo base_url()."assets/fullcalendar/";?>fullcalendar.min.js'></script>
        <script src='<?php echo base_url()."assets/fullcalendar/";?>scheduler.min.css'></script>
        <script src='<?php echo base_url()."assets/fullcalendar/";?>moment.min.js'></script>
        <script src='<?php echo base_url()."assets/fullcalendar/";?>scheduler.min.js'></script>
        <!-- jQuery Flot -->
        <script src="<?php echo base_url()."assets/bluish/"?>js/excanvas.min.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.resize.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.axislabels.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.pie.js"></script>
        <script src="<?php echo base_url()."assets/bluish/"?>js/jquery.flot.stack.js"></script>
        <!-- <script src="<?php echo base_url()."assets/"?>js/main.js"></script> -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/bluish";?>/style/jquery.cleditor.css">
        <script src="<?php echo base_url()."assets/themes/bluish";?>/js/jquery.cleditor.min.js"></script> <!-- CLEditor -->
        <script type="text/javascript" src="<?php echo base_url();?>assets/themes/tinymce/tinymce.min.js"></script>
         <script src='<?php echo base_url()."assets/bluish/"?>src/jquery-customselect.js'></script>
        <link href='<?php echo base_url()."assets/bluish/"?>src/jquery-customselect.css' rel='stylesheet' />
         <!-- <script src="<?php echo base_url()."assets/bluish/"?>src/owl.carousel.js"></script> -->
         <script src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/js/examples.notifications.js"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: ".cleditor",
                height: "10"
            });

            $(document).ready(function(){
              
               var department_id = document.getElementById("department_id_one").value;

               var queue_activated = document.getElementById("queue_activated").value;

               if(queue_activated == 1)
               {
                    setInterval(function(){check_new_patients_department(department_id)},2000);
               }

               // check_new_patients_department(4);

             });
            
            // function to show the cover-spin
            function show_loader() {
                $("#cover-spin").show(0);
            }

            // function to hide the cover-spin
            function hide_loader() {
                $("#cover-spin").hide(0);
            }
            function check_new_patients_department(module)
            {
                 var current_page = $('#current_page').val();
                var config_url = $('#config_url').val();
                var url = config_url+"site/check_queues/"+module;
                // alert(current_page);
                $.ajax({
                type:'POST',
                url: url,
                data:{branch_id: 1},
                dataType: 'text',
                // processData: false,
                // contentType: false,
                success:function(data){
                    // alert(data);
                  var data = jQuery.parseJSON(data);
                  // alert(json.length);
                  if(data.message == 'success')
                  {
                        var name = data.result['patient_surname']+' '+data.result['patient_othernames'];
                        var department_name = data.result['department_name'];
                        var doctor_name = data.result['personnel_fname'];

                        var page = data.page;

                        if(page == 1)
                        {
                            var text_message = name+' has been queued to '+department_name+' Department. Patient for Dr.'+doctor_name+' ';
                            var text_color = 'info';
                        }
                        else if(page == 2)
                        {
                            var text_message = name+' has been queued back to be cleared. Patient from Dr.'+doctor_name+' ';
                            var text_color = 'success';
                        }

                        // alert(data.result['patient_surname']);
                        var notice = new PNotify({
                                                title: 'Queue Notification',
                                                text: text_message,
                                                type: text_color,
                                                addclass: 'click-2-close',
                                                hide: false,
                                                buttons: {
                                                        closer: false,
                                                        sticker: false
                                                    }
                                                });

                                                notice.get().click(function() {
                                                    notice.remove();
                                                });

                  }
                  else
                  {

                  }

                   // for(let i = 0; i <= json.length; i++) {
                   //      let obj = json[i];
                   //      // alert(obj);
                   //      var visit_department_id = obj.visit_department_id;
                   //      var patient_name = obj.patient_surname;
                   //       var department_name = obj.department_name;
                   //       var department_name = obj.department_name;
                   //       // var notice = notice+'_'+i;
                        

                   //      console.log(obj.patient_surname);
                   //  }




                },
                error: function(xhr, status, error) {
                alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

                }
                });
            }
            function check_new_patients_department_old(module)
            {   
             var XMLHttpRequestObject = false;
            
                if (window.XMLHttpRequest) {
                
                    XMLHttpRequestObject = new XMLHttpRequest();
                } 
                    
                else if (window.ActiveXObject) {
                    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
                }
                
                var config_url = $('#config_url').val();
                var url = config_url+"site/check_queues/"+module;
                // alert(url);
                if(XMLHttpRequestObject) {
                            
                    XMLHttpRequestObject.open("GET", url);
                            
                    XMLHttpRequestObject.onreadystatechange = function(){
                        
                        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                            
                            var one = XMLHttpRequestObject.responseText;
                            // alert(one);
                            // let json = one;

                            let json = [{"visit_department_id":"42871","visit_id":"39910","department_id":"10","created":"2023-01-23 00:00:00","created_by":"88","last_modified":"2023-01-23 13:40:44","modified_by":"88","visit_department_status":"1","accounts":"1","picked":"0","picked_date":null,"left_status":"0","left_date":null,"marked_by":"0","picked_by":"0","notified":"0","visit_date":"2023-01-23","revisit":"2","doc_visit":"0","dental_visit":"0","lab_visit":"0","pharmarcy":"0","schedule_id":"0","color_code_id":"0","visit_symptoms":null,"visit_objective_findings":null,"visit_assessment":null,"visit_plan":null,"visit_time":"2023-01-23 13:40:44","visit_time_out":null,"appointment_id":"0","time_start":"13:40:44 PM","time_end":null,"consultation_type_id":null,"visit_type":"1","patient_id":"18673","close_card":"0","vitals_alert":null,"payment_insurance":null,"payment_cash":null,"payment_mobilemoney":null,"payment_cheque":null,"strath_no":null,"account_notes":"","personnel_id":"1","patient_insurance_id":null,"patient_insurance_number":"","visit_delete":"0","date_deleted":null,"deleted_by":null,"bill_to_id":null,"total_payments":"0","total_debit_notes":"0","total_credit_notes":"0","consultation":"0","counseling":"0","dental":"0","ecg":"0","laboratory":"0","nursing_fee":"0","paediatrics":"0","pharmacy":"0","physician":"0","physiotherapy":"0","procedures":"0","radiology":"0","ultrasound":"0","cash":null,"cheque":null,"mpesa":null,"clinic_meds":null,"branch_code":"AD","insurance_limit":"0","visit_number":null,"inpatient":"0","ward_id":null,"held_by":null,"payment_info":null,"insurance_description":"","invoice_number":"223123","invoice_number_doc":null,"room_id":"1","procedure_done":null,"rejected_amount":null,"rejected_reason":null,"rejected_status":null,"rejected_by":null,"rejected_date":null,"mcc":"0","hold_card":"0","sick_leave_note":null,"sick_leave_start_date":null,"sick_leave_days":null,"is_new":"0","appointment_date":null,"parent_visit":null,"insurance_number":"","prefix":"2","suffix":"23123","preauth":"0","approved_amount":"0","principal_member":"","branch_id":"2","sync_status":"0","visit_account":null,"visit_time_in":null,"nurse_id":"55","patient_number":"21069","patient_date":"2023-01-13 12:57:22","title_id":"0","patient_surname":"Lucy","patient_othernames":"Wanjiku","patient_date_of_birth":"1996-01-01","patient_picture":null,"patient_bloodgroup":null,"civil_status_id":null,"patient_address":null,"patient_postalcode":null,"patient_town":"","patient_phone1":"0719132716","patient_phone2":"","patient_email":"","patient_national_id":"0","religion_id":null,"gender_id":"2","patient_kin_othernames":"","patient_kin_sname":"","patient_kin_phonenumber1":"","patient_kin_phonenumber2":null,"relationship_id":"4","visit_type_id":"3","patient_status":"0","nurse_notes":null,"dependant_id":"0","strath_type_id":null,"last_visit":"2023-01-23 00:00:00","patient_delete":"0","department":null,"faculty":null,"insurance_company_id":"1","current_patient_number":"","patient_year":"2023","scheme_name":"","rip_status":"0","invoiced":"0","patient_type":"0","next_appointment_date":null,"category_id":"2","about_us":"32","about_us_view":"","chart_type":"0","patient_first_name":null},{"visit_department_id":"42872","visit_id":"39911","department_id":"10","created":"2023-01-23 00:00:00","created_by":"46","last_modified":"2023-01-23 15:26:43","modified_by":"46","visit_department_status":"1","accounts":"1","picked":"0","picked_date":null,"left_status":"0","left_date":null,"marked_by":"0","picked_by":"0","notified":"0","visit_date":"2023-01-23","revisit":"2","doc_visit":"0","dental_visit":"0","lab_visit":"0","pharmarcy":"0","schedule_id":"0","color_code_id":"0","visit_symptoms":null,"visit_objective_findings":null,"visit_assessment":null,"visit_plan":null,"visit_time":"2023-01-23 15:26:43","visit_time_out":null,"appointment_id":"0","time_start":"15:26:43 PM","time_end":null,"consultation_type_id":null,"visit_type":"1","patient_id":"18667","close_card":"0","vitals_alert":null,"payment_insurance":null,"payment_cash":null,"payment_mobilemoney":null,"payment_cheque":null,"strath_no":null,"account_notes":"","personnel_id":"27","patient_insurance_id":null,"patient_insurance_number":"","visit_delete":"0","date_deleted":null,"deleted_by":null,"bill_to_id":null,"total_payments":"0","total_debit_notes":"0","total_credit_notes":"0","consultation":"0","counseling":"0","dental":"0","ecg":"0","laboratory":"0","nursing_fee":"0","paediatrics":"0","pharmacy":"0","physician":"0","physiotherapy":"0","procedures":"0","radiology":"0","ultrasound":"0","cash":null,"cheque":null,"mpesa":null,"clinic_meds":null,"branch_code":"ADT","insurance_limit":"0","visit_number":null,"inpatient":"0","ward_id":null,"held_by":null,"payment_info":null,"insurance_description":"","invoice_number":"323123","invoice_number_doc":null,"room_id":"1","procedure_done":null,"rejected_amount":null,"rejected_reason":null,"rejected_status":null,"rejected_by":null,"rejected_date":null,"mcc":"0","hold_card":"0","sick_leave_note":null,"sick_leave_start_date":null,"sick_leave_days":null,"is_new":"0","appointment_date":null,"parent_visit":null,"insurance_number":"","prefix":"3","suffix":"23123","preauth":"0","approved_amount":"0","principal_member":"","branch_id":"3","sync_status":"0","visit_account":null,"visit_time_in":null,"nurse_id":"55","patient_number":"21063","patient_date":"2023-01-13 11:12:44","title_id":"0","patient_surname":"Lucy","patient_othernames":"Wangui Gikunju","patient_date_of_birth":"2023-01-13","patient_picture":null,"patient_bloodgroup":null,"civil_status_id":null,"patient_address":null,"patient_postalcode":null,"patient_town":"","patient_phone1":"0720438983","patient_phone2":"","patient_email":"","patient_national_id":"0","religion_id":null,"gender_id":"2","patient_kin_othernames":"","patient_kin_sname":"","patient_kin_phonenumber1":"","patient_kin_phonenumber2":null,"relationship_id":"4","visit_type_id":"3","patient_status":"0","nurse_notes":null,"dependant_id":"0","strath_type_id":null,"last_visit":"2023-01-23 00:00:00","patient_delete":"0","department":null,"faculty":null,"insurance_company_id":"1","current_patient_number":"","patient_year":"2023","scheme_name":"","rip_status":"0","invoiced":"0","patient_type":"0","next_appointment_date":null,"category_id":"2","about_us":"3","about_us_view":"","chart_type":"0","patient_first_name":null}];
                            // let json = [{
                            //         "id" : "1", 
                            //         "msg"   : "hi",
                            //         "tid" : "2013-05-05 23:35",
                            //         "fromWho": "hello1@email.se"
                            //     },
                            //     {
                            //         "id" : "2", 
                            //         "msg"   : "there",
                            //         "tid" : "2013-05-05 23:45",
                            //         "fromWho": "hello2@email.se"
                            //     }];

                            for(let i = 0; i < json.length; i++) {
                                let obj = json[i];
                                alert(obj);
                                console.log(obj.visit_department_id);
                            }

                            if(one == 1)
                            {

                                // notifyDoctor();
                                // document.getElementById("blinker-div").classList.add("blinker"); 
                            }
                            else
                            {
                                // document.getElementById("blinker-div").classList.remove("blinker"); 
                            }
                            
                 
                        }
                    }
                            
                    XMLHttpRequestObject.send(null);
                }
            }
        </script>
        <script>
            function close_side_bar()
            {
                // $('html').removeClass('sidebar-right-opened');
                document.getElementById("sidebar-right").style.display = "none";
                document.getElementById("current-sidebar-div").style.display = "none";
                // document.getElementById("existing-sidebar-div").style.display = "none";
                tinymce.remove();
            }

            function open_sidebar()
            {
                document.getElementById("sidebar-right").style.display = "block";
                document.getElementById("current-sidebar-div").style.display = "none";
            }

            function notifyDoctor()
            {
                var notice = new PNotify({
                title: 'Click to Close',
                text: 'Check me out! I\'m a sticky notice. You\'ll have to click me to close.',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                        closer: false,
                        sticker: false
                    }
                });

                notice.get().click(function() {

                    notice.remove();
                });
            }
          $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        })

        function change_branch(branch_id)
        {
            var current_page = $('#current_page').val();
            var config_url = $('#config_url').val();
            var url = config_url+"site/change_branch/"+branch_id;
            // alert(current_page);
            $.ajax({
            type:'POST',
            url: url,
            data:{branch_id: branch_id},
            dataType: 'text',
            // processData: false,
            // contentType: false,
            success:function(data){
              var data = jQuery.parseJSON(data);

              if(data.message == 'success')
              {
                window.location.href = current_page;
              }
              else
              {

              }


            },
            error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

            }
            });
        }

        function view_patient_statement(patient_id)
        {
            open_sidebar();
            show_loader();

            var config_url = $('#config_url').val();
            var url = config_url+"view-patient-statement/"+patient_id;
            alert(url);
            $.ajax({
                type:'POST',
                url: url,
                data:{query: null},
                dataType: 'json',
                success:function(data){
                //window.alert("You have successfully updated the symptoms");
                //obj.innerHTML = XMLHttpRequestObject.responseText;

                  hide_loader();
                 $("#sidebar-div").html(data);
                  // alert(data);
                },
                error: function(xhr, status, error) {
                //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                alert(error);
                }

            });


        }

        function change_report(status)
        {

            var config_url = $('#config_url').val();
            var redirect_url = $('#redirect_url').val();
            var url = config_url+"site/report_status/"+status;
            // alert(url);
            $.ajax({
                type:'POST',
                url: url,
                data:{query: null},
                dataType: 'text',
                success:function(data){

                    window.location.href = config_url+redirect_url;
                },
                error: function(xhr, status, error) {
                //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                alert(error);
                }

            });

        }

        function view_doctors_appointments(visit_id)
        {

            var config_url = $('#config_url').val();
            var datess=document.getElementById("next_appointment_date"+visit_id).value;
            var doctor=document.getElementById("personnel"+visit_id).value;
            // alert(doctor);
            var url= config_url+"reception/doc_schedule/"+doctor+"/"+datess;

              $('#doctors_schedule'+visit_id).load(url);
              $('#doctors_schedule'+visit_id).fadeIn(1000); return false;

        }

        </script>



    </body>
</html>