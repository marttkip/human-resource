<?php
$branch_session = $this->session->userdata('branch_id');

$branch_add = '';
$visit_branch_add = '';
if($branch_session > 0)
{
	$branch_add = ' AND branch_id = '.$branch_session;
	$visit_branch_add = ' AND visit.branch_id = '.$branch_session;
}
$i =0;
$where = 'patients.patient_delete = 0 AND category_id = 2 '.$branch_add;
$table = 'patients';
$uhdc_patient = $this->reception_model->count_items($table, $where);


$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND DATE(patients.patient_date)'.$visit_branch_add;
$table = 'patients,visit,appointments';
$new_bookings = $this->reception_model->count_items($table, $where);




$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0  AND appointments.appointment_date <= "'.date('Y-m-d').'"  '.$visit_branch_add;
$table = 'patients,visit,appointments';
$total_appointments = $this->reception_model->count_items($table, $where);



$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 4 ) AND appointments.appointment_date <= "'.date('Y-m-d').'" '.$visit_branch_add;
$table = 'patients,visit,appointments';
$honoured_appointments = $this->reception_model->count_items($table, $where);



$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 1 OR appointments.appointment_status = 5 OR appointments.appointment_status = 6 OR appointments.appointment_status = 2 OR appointments.appointment_status = 7) AND appointments.appointment_date <= "'.date('Y-m-d').'" '.$visit_branch_add;
$table = 'patients,visit,appointments';
$noshow_appointments = $this->reception_model->count_items($table, $where);


$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 3 ) AND appointments.appointment_date <= "'.date('Y-m-d').'" '.$visit_branch_add;
$table = 'patients,visit,appointments';
$cancelled_appointments = $this->reception_model->count_items($table, $where);


$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 1 AND appointments.appointment_date <= "'.date('Y-m-d').'" '.$visit_branch_add;
$table = 'patients,visit,appointments';
$rescheduled_appointments = $this->reception_model->count_items($table, $where);


$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status <> 3) AND appointments.appointment_date > "'.date('Y-m-d').'" '.$visit_branch_add;
$table = 'patients,visit,appointments';
$coming_appointments = $this->reception_model->count_items($table, $where);


$configuration_rs = $this->site_model->get_company_configuration();

$configuration_array = array();
foreach ($configuration_rs->result() as $key) {
	// code...
	$configuration_array[$key->company_configuration_name] = $key;
}



if(isset($this->configuration_array['reception_format']))
    $reception_format = $this->configuration_array['reception_format']->company_configuration_particulars;
else
    $reception_format = NULL;

$minimal_dashboard = $contacts['minimal_dashboard'];

// echo $month_patients.' '.$honoured_appointments;

?>
<div class="" style="margin-top:10px;">

	<?php
	if($minimal_dashboard == 0)
	{
	?>
		<div class="col-md-12">
			<div class="col-md-2">
				<section class="card mb-4">
					<div class="card-body bg-primary">
						<div class="widget-summary">
							<!-- <div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon">
									<i class="fa fa-calendar"></i>
								</div>
							</div> -->
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Appointments</h4>
									<div class="info">
										<strong class="amount"><?php echo $total_appointments?></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>

			<div class="col-md-2">
				<section class="card mb-4">
					<div class="card-body bg-success">
						<div class="widget-summary">
							<!-- <div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon">
									<i class="fa fa-calendar"></i>
								</div>
							</div> -->
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Honoured</h4>
									<div class="info">
										<strong class="amount"><?php echo $honoured_appointments?></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-2">
				<section class="card mb-4">
					<div class="card-body bg-info">
						<div class="widget-summary">
							<!-- <div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon">
									<i class="fa fa-calendar"></i>
								</div>
							</div> -->
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">No Show</h4>
									<div class="info">
										<strong class="amount"><?php echo $noshow_appointments?></strong>
									</div>
								</div>

							</div>
						</div>
					</div>
				</section>
			</div>

			<div class="col-md-2">
				<section class="card mb-4">
					<div class="card-body bg-warning">
						<div class="widget-summary">
							<!-- <div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon">
									<i class="fa fa-calendar"></i>
								</div>
							</div> -->
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Rescheduled</h4>
									<div class="info">
										<strong class="amount"><?php echo $rescheduled_appointments?></strong>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-2">
				<section class="card mb-4">
					<div class="card-body bg-danger">
						<div class="widget-summary">
							<!-- <div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon">
									<i class="fa fa-calendar"></i>
								</div>
							</div> -->
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Cancelled</h4>
									<div class="info">
										<strong class="amount"><?php echo $cancelled_appointments?></strong>
									</div>
								</div>

							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-2">

				<section class="card mb-4">
					<div class="card-body bg-default">
						<div class="widget-summary">

							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title" style="color: #fff">Coming Appts</h4>
									<div class="info">
										<strong class="amount" style="color: #fff"><?php echo $coming_appointments;?></strong>
									</div>
								</div>

							</div>
						</div>
					</div>
				</section>

			</div>



		</div>

<?php
}
?>
<?php
$result = '';

//if users exist display them
if ($todays_visit->num_rows() > 0)
{
	$count = $page;

	$result .=
		'
			<table class="table table-hover table-bordered table-condensed ">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Patient</th>
				  <th>Phone</th>
				  <th>Time</th>
				  <th>Doctor</th>
				</tr>
			  </thead>
			  <tbody>
		';

	$personnel_query = $this->personnel_model->all_personnel();

	foreach ($todays_visit->result() as $row)
	{
		// $appointment_start = date('Y-m-d');
		// $visit_dates_query = $this->reception_model->get_all_appointments_dates($appointment_start);


		$visit_date = date('jS M Y',strtotime($row->visit_date));
		$visit_date_old = $row->visit_date;
		$visit_time = date('H:i a',strtotime($row->visit_time));
		if($row->visit_time_out != '0000-00-00 00:00:00')
		{
			$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
		}
		else
		{
			$visit_time_out = '-';
		}
		$visit_id = $row->visit_id;
		$patient_id = $row->patient_id;
		$personnel_id3 = $row->personnel_id;
		$strath_no = $row->strath_no;
		$patient_number = $row->patient_number;
		$room_id2 = $row->room_id;
		$patient_year = $row->patient_year;
		$coming_from = $this->reception_model->coming_from($visit_id);
		$sent_to = $this->reception_model->going_to($visit_id);
		$patient_othernames = $row->patient_othernames;
		$patient_surname = $row->patient_surname;
		$patient_date_of_birth = $row->patient_date_of_birth;
		$patient_national_id = $row->patient_national_id;
		$patient_phone = $row->patient_phone1;
		$time_start = $row->time_start;
		$time_end = $row->time_end;
		$procedure_done = $row->procedure_done;

		//creators and editors
		if($personnel_query->num_rows() > 0)
		{
			$personnel_result = $personnel_query->result();

			foreach($personnel_result as $adm)
			{
				$personnel_id2 = $adm->personnel_id;

				if($personnel_id3 == $personnel_id2)
				{
					$doctor = $adm->personnel_fname;
					break;
				}

				else
				{
					$doctor = '-';
				}
			}
		}

		else
		{
			$doctor = '-';
		}

		if(empty($reception_format) OR $reception_format == 'V1')
			$patient_surname .= ' '.$patient_othernames;

		$count++;

		$result .=
		'
			<tr>
				<td>'.$count.'</td>
				<td>'.ucfirst(strtoupper($patient_surname)).'</td>
				<td>'.$patient_phone.'</td>
				<td>'.date('H:i A',strtotime($time_start)).'</td>
				<td>'.$doctor.'</td>

			</tr>
		';
	}


	$result .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no queued patients";
}


$todays_appointments_list = '';

//if users exist display them
if ($appointment_list->num_rows() > 0)
{
	$count = $page;

	$todays_appointments_list .=
		'
			<table class="table table-hover table-bordered table-condensed">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Patient</th>
				  <th>Phone</th>
				  <th>Time</th>
				  <th>Doctor</th>
				</tr>
			  </thead>
			  <tbody>
		';

	$personnel_query = $this->personnel_model->all_personnel();

	foreach ($appointment_list->result() as $row)
	{
		// $appointment_start = date('Y-m-d');
		// $visit_dates_query = $this->reception_model->get_all_appointments_dates($appointment_start);


		$visit_date = date('jS M Y',strtotime($row->visit_date));
		$visit_date_old = $row->visit_date;
		$visit_time = date('H:i a',strtotime($row->visit_time));
		if($row->visit_time_out != '0000-00-00 00:00:00')
		{
			$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
		}
		else
		{
			$visit_time_out = '-';
		}
		$visit_id = $row->visit_id;
		$patient_id = $row->patient_id;
		$personnel_id3 = $row->personnel_id;
		$dependant_id = $row->dependant_id;
		$strath_no = $row->strath_no;
		$patient_number = $row->patient_number;
		$room_id2 = $row->room_id;
		$patient_year = $row->patient_year;
		$coming_from = $this->reception_model->coming_from($visit_id);
		$sent_to = $this->reception_model->going_to($visit_id);
		$patient_othernames = $row->patient_othernames;
		$patient_surname = $row->patient_surname;
		$patient_date_of_birth = $row->patient_date_of_birth;
		$patient_national_id = $row->patient_national_id;
		$patient_phone = $row->patient_phone1;
		$time_start = $row->time_start;
		$time_end = $row->time_end;
		$procedure_done = $row->procedure_done;

		//creators and editors
		if($personnel_query->num_rows() > 0)
		{
			$personnel_result = $personnel_query->result();

			foreach($personnel_result as $adm)
			{
				$personnel_id2 = $adm->personnel_id;

				if($personnel_id3 == $personnel_id2)
				{
					$doctor = $adm->personnel_fname;
					break;
				}

				else
				{
					$doctor = '-';
				}
			}
		}

		else
		{
			$doctor = '-';
		}

		$count++;
		if(empty($reception_format) OR $reception_format == 'V1')
			$patient_surname .= ' '.$patient_othernames;

		
		$todays_appointments_list .=
		'
			<tr>
				<td>'.$count.'</td>
				<td>'.ucfirst(strtoupper($patient_surname)).'</td>
				<td>'.$patient_phone.'</td>
				<td>'.date('H:i A',strtotime($time_start)).'</td>
				<td>'.$doctor.'</td>

			</tr>
		';
	}


	$todays_appointments_list .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$todays_appointments_list .= "There are no appointents for this day";
}



$tomorrows_appointments_list = '';

//if users exist display them
if ($tomorrows_appointments->num_rows() > 0)
{
	$count = $page;

	$tomorrows_appointments_list .=
		'
			<table class="table table-hover table-bordered table-condensed">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Patient</th>
				  <th>Phone</th>
				  <th>Time</th>
				  <th>Doctor</th>
				</tr>
			  </thead>
			  <tbody>
		';

	$personnel_query = $this->personnel_model->all_personnel();

	foreach ($tomorrows_appointments->result() as $row)
	{

		$visit_date = date('jS M Y',strtotime($row->visit_date));
		$visit_date_old = $row->visit_date;
		$visit_time = date('H:i a',strtotime($row->visit_time));
		if($row->visit_time_out != '0000-00-00 00:00:00')
		{
			$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
		}
		else
		{
			$visit_time_out = '-';
		}
		$visit_id = $row->visit_id;
		$patient_id = $row->patient_id;
		$personnel_id3 = $row->personnel_id;
		$dependant_id = $row->dependant_id;
		$strath_no = $row->strath_no;
		$patient_number = $row->patient_number;
		$room_id2 = $row->room_id;
		$patient_year = $row->patient_year;
		$coming_from = $this->reception_model->coming_from($visit_id);
		$sent_to = $this->reception_model->going_to($visit_id);
		$patient_othernames = $row->patient_othernames;
		$patient_surname = $row->patient_surname;
		$patient_date_of_birth = $row->patient_date_of_birth;
		$patient_national_id = $row->patient_national_id;
		$patient_phone = $row->patient_phone1;
		$time_start = $row->time_start;
		$appointment_start_time = $row->appointment_start_time;
		$time_end = $row->time_end;
		$procedure_done = $row->procedure_done;
		$schedule_id = $row->schedule_id;

		//creators and editors
		if($personnel_query->num_rows() > 0)
		{
			$personnel_result = $personnel_query->result();

			foreach($personnel_result as $adm)
			{
				$personnel_id2 = $adm->personnel_id;

				if($personnel_id3 == $personnel_id2)
				{
					$doctor = $adm->personnel_fname;
					break;
				}

				else
				{
					$doctor = '-';
				}
			}
		}

		else
		{
			$doctor = '-';
		}
		if(empty($reception_format) OR $reception_format == 'V1')
			$patient_surname .= ' '.$patient_othernames;
		$count++;
		if($schedule_id != 1)
			$color = 'warning';
		else
			$color = 'default';

		$tomorrows_appointments_list .=
		'
			<tr>
				<td class="'.$color.'">'.$count.'</td>
				<td class="'.$color.'">'.ucfirst(strtoupper($patient_surname)).'</td>
				<td class="'.$color.'">'.$patient_phone.'</td>
				<td class="'.$color.'">'.date('H:i A',strtotime($appointment_start_time)).'</td>
				<td class="'.$color.'">'.$doctor.'</td>

			</tr>
		';
	}


	$tomorrows_appointments_list .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$tomorrows_appointments_list .= "There are no appointents for this day";
}

$date_tomorrow = date("Y-m-d",strtotime("tomorrow"));
$dt= $date_tomorrow;
$dt1 = strtotime($dt);
$dt2 = date("l", $dt1);
$dt3 = strtolower($dt2);
// if(($dt3 == "sunday"))
// {
//     // echo $dt3.' is weekend'."\n";

//     $date_tomorrow = strtotime('+1 day', strtotime($dt));
//     $date_tomorrow = date("Y-m-d",$date_tomorrow);
//     $date_to_send = 'Monday';
// }
// else
// {
    // echo $dt3.' is not weekend'."\n";
     $date_tomorrow = $dt;
     $date_to_send = 'Tomorrow';
// }

?>
<div class="col-md-12">
	<!-- <div class="col-md-12"> -->
		<div class="col-md-4">
			<section class="panel panel-primary">
			    <header class="panel-heading">
		          <h4 class="pull-left"><i class="icon-reorder"></i>Today's Visits <?php echo date('Y-m-d');?></h4>
		          <div class="widget-icons pull-right">
		            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
		          </div>
		          <div class="clearfix"></div>
		        </header>
		      	<div class="panel-body" style="padding:0px;height:40vh;overflow-y:scroll;">
					<!-- <div class="padd"> -->
						<?php echo $result?>
					<!-- </div> -->
		        </div>
			</section>
		</div>
		<div class="col-md-4">
			<section class="panel panel-info">
			    <header class="panel-heading">
		          <h4 class="pull-left"><i class="icon-reorder"></i>Today's Appointments <?php echo date('Y-m-d');?></h4>
		          <div class="widget-icons pull-right">
		            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
		          </div>
		          <div class="clearfix"></div>
		        </header>
		      		<div class="panel-body" style="padding:0px;height:40vh;overflow-y:scroll;">
							<!-- <div class="padd"> -->
								<?php echo $todays_appointments_list;?>
							<!-- </div> -->
		        </div>
			</section>
		</div>
		<div class="col-md-4">
			<section class="panel panel-warning">
			    <header class="panel-heading">
		          <h4 class="pull-left"><i class="icon-reorder"></i><?php echo $date_to_send;?>'s Appointments <?php echo $date_tomorrow;?></h4>
		          <div class="widget-icons pull-right">
		            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
		          </div>
		          <div class="clearfix"></div>
		        </header>
		      		<div class="panel-body" style="padding:0px;height:40vh;overflow-y:scroll;">
					<!-- <div class="padd"> -->
						<?php echo $tomorrows_appointments_list;?>
					<!-- </div> -->
		        </div>
			</section>
		</div>
	<!-- </div> -->
</div>

<?php

if($minimal_dashboard == 0)
{
// $where = 'patients.patient_delete = 0 ';
// $table = 'patients';
// $total_patient = $this->reception_model->count_items($table, $where);


// $where = 'patients.patient_delete = 0 AND category_id = 1';
// $table = 'patients';
// $new_patient = $this->reception_model->count_items($table, $where);

// $where = 'patients.patient_delete = 0 AND category_id = 3';
// $table = 'patients';
// $uncategorized_patient = $this->reception_model->count_items($table, $where);

// $where = 'patients.patient_delete = 0 AND category_id = 2';
// $table = 'patients';
// $uhdc_patient = $this->reception_model->count_items($table, $where);




$total_charts = '';
for ($i = 6; $i >= 0; $i--) {
    $months = date("Y-m", strtotime( date( 'Y-m-d' )." -$i months"));
    $months_explode = explode('-', $months);
    $year = $months_explode[0];
    $month = $months_explode[1];
    $last_visit = date('M Y',strtotime($months));
    $where = 'patients.patient_delete = 0 AND YEAR(patient_date) = '.$year.' AND MONTH(patient_date) = "'.$month.'" AND patients.patient_id IN (SELECT visit.patient_id FROM visit)'.$branch_add;
	$table = 'patients';
	$month_patients = $this->reception_model->count_items($table, $where);
	$total_charts .= '["'.$last_visit.'", '.$month_patients.'],';

}



// $where = 'patients.patient_delete = 0 AND gender_id = 1  AND patients.patient_id IN (SELECT visit.patient_id FROM visit)';
// 	$table = 'patients';
// 	$male_patients = $this->reception_model->count_items($table, $where);

// $where = 'patients.patient_delete = 0 AND gender_id = 2  AND patients.patient_id IN (SELECT visit.patient_id FROM visit)';

// $table = 'patients';

$where = 'visit_type_id > 0';
$table = 'visit_type';
$total_gender = '';
$visit_types = $this->dashboard_model->get_content($table, $where,'*',$group_by=NULL,$limit=NULL);

$count = 0;
if($visit_types->num_rows() > 0)
{
	foreach ($visit_types->result() as $key => $value) {
		# code...
		$visit_type_name = $value->visit_type_name;
		$visit_type_id = $value->visit_type_id;

		$where2 = 'visit.visit_type = '.$visit_type_id.' AND (parent_visit = 0 OR parent_visit IS NULL) and visit.visit_delete = 0 AND visit.visit_date > "2018-03-01" AND MONTH(visit.visit_date) = "'.date('m').'" AND YEAR(visit.visit_date) = "'.date('Y').'" '.$visit_branch_add;
		$table2 = 'visit';
		$total_patients = $this->reception_model->count_items($table2, $where2);
		$count++;
		$color = $this->reception_model->random_color();
		$total_gender .= '{
		                        label: "'.$visit_type_name.'",
		                        data: [
		                            ['.$count.', '.$total_patients.']
		                        ],
		                        color: "'.$color.'",
		                    },';
	}
}




$total_visit_report='';
$year = date('Y');
$month = date('m');

$community_where ='patients.patient_id = visit.patient_id AND visit.personnel_id > 0 AND visit.revisit = 0 AND patients.patient_type = 0 AND  YEAR(visit.visit_date) = '.$year.' AND MONTH(visit.visit_date) = "'.$month.'"'.$visit_branch_add;
$community_table = 'visit,patients';
$total_number_new = $this->reception_model->count_items($community_table, $community_where);

$community_where ='patients.patient_id = visit.patient_id AND visit.personnel_id > 0 AND visit.revisit = 1 AND patients.patient_type = 0 AND  YEAR(visit.visit_date) = '.$year.' AND MONTH(visit.visit_date) = "'.$month.'"'.$visit_branch_add;
$community_table = 'visit,patients';
$total_number_old = $this->reception_model->count_items($community_table, $community_where);
$color = $this->reception_model->random_color();

$total_visit_report .= '{
		                        label: "New Visits ('.$total_number_new.')",
		                        data: [
		                            [1, '.$total_number_new.']
		                        ],
		                        color: "purple",
		                    },';
$color = $this->reception_model->random_color();
$total_visit_report .= '{
		                        label: "Re-visits ('.$total_number_old.')",
		                        data: [
		                            [2, '.$total_number_old.']
		                        ],
		                        color: "green",
		                    },';

$this->db->where('place.place_delete = 0');
$query = $this->db->get('place');


$array_rs = $this->dashboard_model->getAssignedBilling($year,$month);


$grouped_array_fixed = array();

foreach ($array_rs->result() as $element_five) {
    $grouped_array_fixed[$element_five->about_us]= $element_five;

}

// var_dump($grouped_array_fixed);die();
$patients_list = '';
$patient_count = 0;
$amount_total = 0;
if($query->num_rows() > 0)
{
	foreach ($query->result() as $key => $value) {
		// code...

		$place_name = $value->place_name;
		$place_id = $value->place_id;

		$community_where ='patients.patient_delete = 0 AND  YEAR(patients.patient_date) = '.$year.' AND patients.about_us = '.$place_id.'  AND MONTH(patients.patient_date) = "'.$month.'" AND patients.branch_id ='.$branch_session;
		$community_table = 'patients';
		$patient_numbers = $this->reception_model->count_items($community_table, $community_where);

		(int)$month;

		// if($patient_numbers > 0)
		// {

			// $where ='patients.patient_delete = 0 AND  YEAR(patients.patient_date) = '.$year.' AND patients.about_us = '.$place_id.'  AND MONTH(patients.patient_date) = "'.$month.'" AND visit_invoice.visit_id = visit.visit_id AND visit_invoice.visit_invoice_delete = 0 AND visit.patient_id = patients.patient_id AND visit.branch_id ='.$branch_session;
			// $table = 'patients,visit_invoice,visit';
			// $select = 'SUM(visit_invoice.invoice_bill) AS number';
			// $cpm_group = 'patients.about_us';
			// $amount = $this->dashboard_model->count_items_group($table, $where, $select,$cpm_group);

			$amount = $grouped_array_fixed[$place_id]->number;

			$patient_count += $patient_numbers;
			$amount_total += $amount;
			$patients_list .= '<tr>
									<th>'.$place_name.'</th>
									<td><a onclick="open_patients('.$place_id.','.$year.','.$month.')">'.$patient_numbers.'</a></td>
									<td><a onclick="open_patients('.$place_id.','.$year.','.$month.')">'.number_format($amount,2).'</a></td>

								</tr>';
		// }


	}
	$patients_list .= '<tr>
									<th>TOTALS</th>
									<td>'.$patient_count.'</td>
									<td>'.number_format($amount_total,2).'</td>

								</tr>';
}
// var_dump($total_visit_report); die();
?>
<!-- start: page -->
<div class="col-md-12" style="margin-top:30px;">
	<div class="col-md-6 col-lg-12 col-xl-12">
		<section class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-4">
						<section class="card">
					            <header class="card-header">

					                <h4 class="card-title">Patients visit</h4>
					                <p class="card-subtitle">Comparison of either New Visit or Re Visit for  <?php echo date('M')?> </p>
					            </header>
					            <div class="card-body">

					                <!-- Flot: Pie -->
					                <div class="chart chart-md" id="flotPie2"></div>
					                <script type="text/javascript">
					                    var flotPieData2 = [<?php echo $total_visit_report?>];

					                    // See: js/examples/examples.charts.js for more settings.
					                </script>

					            </div>
					    </section>

					</div>
					<div class="col-lg-4">
						<?php

						if(isset($configuration_array['pricelist']))
						{

							$price_status = $configuration_array['pricelist']->company_configuration_status;
							if($price_status == 'true')
							{
								?>
								<section class="card">
									<?php echo form_open("reception/register_other_patient", array("class" => "form-horizontal","id"=>'search-price-list'));?>
									<div class="col-md-12">
					            		<div class="col-md-12">
					            			 <div class="form-group">
							                  
							                    
							                    <div class="col-md-12">
							                        <input type="text" class="form-control" name="procedure_name" id="procedure_name"  value="<?php echo set_value('procedure_name');?>" placeholder="Search Procedures" onKeyUp="search_price_list()" autocomplete="off">
							                    </div>
							                </div>
					            		</div>
					            		<div class="col-md-5" style="display:none">
					            			 <div class="form-group">
							                    <label class="col-md-4 control-label">Department: </label>
							                    
							                    <div class="col-md-8">
							                    	<select class="form-control" name="department_id">
							                    		<option value="">------ SELECT A DEPARTMENT ----- </option>
							                    		<?php 

							                    		$department_rs = $this->admin_model->get_departments();

							                    		if($department_rs->num_rows() > 0)
							                    		{
							                    			foreach ($department_rs->result() as $key => $value) {
							                    				# code...
							                    				$department_id = $value->department_id;
							                    				$department_name = $value->department_name;

							                    				echo '<option value="'.$department_id.'">'.$department_name.' </option>';
							                    			}
							                    		}
							                    		?>
							                    	</select>
							                    </div>
							                </div>
					            		</div>
					            		<div class="col-md-2" style="display:none;">
					            			 <div class="form-group center-align">
							                    <button class="btn btn-info btn-sm" type="submit">Search Charges</button>
							                </div>
					            		</div>
					            	</div>
				            	</form>
				     
								<div class="card-body" style="padding:0px; height: 50vh;overflow-y: scroll;">

					            
					            		<div class="price-list-items"></div>
					            	<!-- </div> -->

								</div>
							</section>
						    	<?php
							}
							else
							{
								?>
								<section class="card">
							            <header class="card-header">

							                <h4 class="card-title">Patients Visit Category</h4>
							                <p class="card-subtitle">Comparison of visit types for <?php echo date('M')?> </p>
							            </header>
							            <div class="card-body">

							                <!-- Flot: Pie -->
							                <div class="chart chart-md" id="flotPie"></div>
							                <script type="text/javascript">
							                    var flotPieData = [<?php echo $total_gender?>];

							                    // See: js/examples/examples.charts.js for more settings.
							                </script>

							            </div>
							    </section>
						    	<?php

							}
						}else
						{
							?>
								<section class="card">
							            <header class="card-header">

							                <h4 class="card-title">Patients Visit Category</h4>
							                <p class="card-subtitle">Comparison of visit types for <?php echo date('M')?> </p>
							            </header>
							            <div class="card-body">

							                <!-- Flot: Pie -->
							                <div class="chart chart-md" id="flotPie"></div>
							                <script type="text/javascript">
							                    var flotPieData = [<?php echo $total_gender?>];

							                    // See: js/examples/examples.charts.js for more settings.
							                </script>

							            </div>
							    </section>
						    	<?php

						}
						?>
					</div>

					<div class="col-lg-4">
							<?php
						
							if(isset($configuration_array['timesheet']))
							{

								$config_status = $configuration_array['timesheet']->company_configuration_status;
								if($config_status == 'true')
								{
									?>
										<section class="card">
											<header class="card-header card-header-transparent">
												<div class="card-actions">
													<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
													<a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
												</div>

												<h2 class="card-title">TIME SHEET FOR <?php echo strtoupper(date('F Y'))?></h2>
											</header>
											<div class="card-body" style="padding:0px; height: 50vh;overflow-y: scroll;">
												<table class="table table-condensed table-bordered table-linked">
													<thead>
														<tr>
															<th>DATE</th>
															<th>START TIME</th>
															<th>END TIME</th>
															<th>HOURS</th>
														</tr>
													</thead>
													<?php
													$personnel_id = $this->session->userdata('personnel_id');

													$timesheet_rs = $this->admin_model->get_time_reports($personnel_id);
													$list = '';
													$grand_hours = 0;
													if($timesheet_rs->num_rows() > 0)
													{
														$list .= '<tbody>';
														// $string = array();
														foreach ($timesheet_rs->result() as $key => $value) {
															// code...
															$sign_time_in = $value->sign_time_in;
															$sign_time_out = $time_out = $value->sign_time_out;
															
															if(empty($sign_time_out))
															{
																$sign_time_out = '-';

																$total_hours = 0;
																
															}
															else
															{
																$sign_time_out = date('H:i A',strtotime($sign_time_out));

																$datetime1 = new DateTime($sign_time_in);
																$datetime2 = new DateTime($time_out);
																$interval = $datetime1->diff($datetime2);
																$total_hours =  $interval->format('%h')." Hours ".$interval->format('%i')." Minutes";
																// $total_hours =  $interval->format('%h')." Hours ";

																$grand_hours += $interval->format('%h');
																$hour = $interval->format('%h');
																$minute = $interval->format('%i');

																if($minute < 10)
																	$minute = '0'.$minute;

																if($hour < 10)
																	$hour = '0'.$hour;

																// $string[] = "'$hour:$minute',";
																$string[] = "$hour:$minute:00,";

																// array_push($string, $section);

																
																// $arr = [$string]
																

															}
															
															
															$list .= '<tr>
																		<td>'.date('jS M Y',strtotime($sign_time_in)).'</td>
																		<td>'.date('H:i A',strtotime($sign_time_in)).'</td>
																		<td>'.$sign_time_out.'</td>
																		<td>'.$total_hours.'</td>
																		</tr>';
														}
																
																	
														$total = 0;
															
														// Loop the data items
														foreach( $string as $element):

															// echo $element;
														
															// Explode by separator :
															$temp = explode(":", $element);

															// Convert the hours into seconds
															// and add to total
															$total+= (int) $temp[0] * 3600;

															// Convert the minutes to seconds
															// and add to total
															$total+= (int) $temp[1] * 60;

															// Add the seconds to total
															$total+= (int) $temp[2];
														endforeach;
															
														// Format the seconds back into HH:MM:SS
														$formatted = sprintf('%02d:%02d:%02d',($total / 3600),($total / 60 % 60),$total % 60);

														$explode = explode(':', $formatted);

														$time = $explode[0].' Hours '.$explode[1].' Minutes';

																
														$list .= '</tbody>';


														$list .= '<tfoot>
																	<tr>
																		<th colspan="3">TOTAL WORK TIME</th>
																		<th style="border-top:2px solid black;color: white">'.$time.' </th>
																	</tr>
																  </tfoot>';
													}


													echo $list;
													?>
												</table>
													
											</div>
										</section>
									<?php
								}
								else
								{
									?>
										<section class="card">
								            <header class="card-header">

								                <h4 class="card-title">Patients</h4>
								                <p class="card-subtitle">Comparison of how patients came to know about the clinic in <?php echo date('M')?></p>
								            </header>
								            <div class="card-body" 	style="padding:0px;height:40vh;overflow-y:scroll;">
								            	<table class="table table-condensed table-bordered">
								            		<thead>
								            			<thead>
								            				<th>Place</th>
								            				<th>Count</th>
								            				<th>Revenue</th>
								            			</thead>
								            		</thead>
								            		<tbody>
								            			<?php echo $patients_list;?>
								            		</tbody>
								            	</table>

								            </div>
								        </section>
								    <?php
								}
							}
							else
							{
								?>
									<section class="card">
							            <header class="card-header">

							                <h4 class="card-title">Patients</h4>
							                <p class="card-subtitle">Comparison of how patients came to know about the clinic in <?php echo date('M')?></p>
							            </header>
							            <div class="card-body" 	style="padding:0px;height:40vh;overflow-y:scroll;">
							            	<table class="table table-condensed table-bordered">
							            		<thead>
							            			<thead>
							            				<th>Place</th>
							            				<th>Count</th>
							            				<th>Revenue</th>
							            			</thead>
							            		</thead>
							            		<tbody>
							            			<?php echo $patients_list;?>
							            		</tbody>
							            	</table>

							            </div>
							        </section>
							    <?php
							}
						    ?>
					</div>
				</div>
			</div>
		</section>
	</div>

</div>
</div>

<?php
}
$where = 'patients.patient_delete = 0 '.$branch_add;
$table = 'patients';
$total_patient = $this->reception_model->count_items($table, $where);


$where = 'patients.patient_delete = 0 AND category_id = 1'.$branch_add;
$table = 'patients';
$new_patient = $this->reception_model->count_items($table, $where);

$where = 'patients.patient_delete = 0 AND category_id = 3'.$branch_add;
$table = 'patients';
$uncategorized_patient = $this->reception_model->count_items($table, $where);

$where = 'patients.patient_delete = 0 AND category_id = 2'.$branch_add;
$table = 'patients';
$uhdc_patient = $this->reception_model->count_items($table, $where);







?>


<script type="text/javascript">
	function open_patients(place_id,year,month)
	{
		open_sidebar();

		var config_url = $('#config_url').val();
        var data_url = config_url+"admin/get_all_patients/"+place_id+"/"+year+"/"+month;

    	// alert(data_url);

        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: 1},
        dataType: 'text',
		success:function(data)
		{
			$("#sidebar-div").html(data);
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
	}


	$(document).on("submit","form#search-price-list",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		
		var config_url = $('#config_url').val();	

		 var url = config_url+"admin/search_price_list";
		 // var values = $('#visit_type_id').val();
		 
	       $.ajax({
	       type:'POST',
	       url: url,
	       data:form_data,
	       dataType: 'text',
	       processData: false,
	       contentType: false,
	       success:function(data){
	          var data = jQuery.parseJSON(data);
	        
	          	if(data.message == "success")
				{
					// alert(data.result);
	    			$('.price-list-items').html(data.result);
					
				}
				else
				{
					alert('Please ensure you have added included all the items');
				}
	       
	       },
	       error: function(xhr, status, error) {
	       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	       
	       }
	       });
		 
		
	   
		
	});

	function search_price_list()
	{
		var config_url = $('#config_url').val();	

		 var url = config_url+"admin/search_price_list";
		 var procedure_name = $('#procedure_name').val();
		 
	       $.ajax({
	       type:'POST',
	       url: url,
	       data:{procedure_name: procedure_name},
	       dataType: 'text',
	       // processData: false,
	       // contentType: false,
	       success:function(data){
	          var data = jQuery.parseJSON(data);
	        
	          	if(data.message == "success")
				{
					// alert(data.result);
	    			$('.price-list-items').html(data.result);
					
				}
				else
				{
					alert('Please ensure you have added included all the items');
				}
	       
	       },
	       error: function(xhr, status, error) {
	       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	       
	       }
	       });
		 
	}
</script>
