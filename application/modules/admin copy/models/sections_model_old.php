<?php

class Sections_model extends CI_Model 
{	
	/*
	*	Retrieve all sections
	*
	*/
	public function all_sections()
	{
		$this->db->where('section_status = 1');
		$query = $this->db->get('section');
		
		return $query;
	}
	
	/*
	*	Retrieve all parent sections
	*
	*/
	public function all_parent_sections($order = 'section_name')
	{
		$this->db->where('section_status = 1');
		$this->db->order_by($order, 'ASC');
		$query = $this->db->get('section');
		
		return $query;
	}



	public function all_parent_sections_list($order = 'section_name')
	{
		$select_sql = "SELECT
						* 

						FROM 
						(

							SELECT 

							section.* 
							
							FROM 
								section
							WHERE
								section.section_status = 1
								AND section.section_parent = 0
									
						) AS data ";


		$query = $this->db->query($select_sql);

		return $query;
	}

	public function all_parent_sections_new($order = 'section_parent')
	{
		$this->db->where('section_status = 1');
		// $this->db->order_by($order, 'ASC');
		$this->db->order_by('section_parent','ASC');
		$query = $this->db->get('section');
		
		return $query;
	}
	/*
	*	Retrieve all children sections
	*
	*/
	public function all_child_sections($order = 'section_position')
	{
		$this->db->where('section_status = 1 ');
		$this->db->order_by($order, 'ASC');
		$query = $this->db->get('section');
		
		return $query;
	}
	
	/*
	*	Retrieve all sections
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_sections($table, $where, $per_page, $page, $order = 'section_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Add a new section
	*	@param string $image_name
	*
	*/
	public function add_section()
	{
		$data = array(
				'section_name'=>ucwords(strtolower($this->input->post('section_name'))),
				'section_parent'=>$this->input->post('section_parent'),
				'section_position'=>$this->input->post('section_position'),
				'section_status'=>$this->input->post('section_status'),
				'section_icon'=>$this->input->post('section_icon'),
				'created'=>date('Y-m-d H:i:s'),
				'created_by'=>$this->session->userdata('personnel_id'),
				'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('section', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Update an existing section
	*	@param string $image_name
	*	@param int $section_id
	*
	*/
	public function update_section($section_id)
	{
		$data = array(
				'section_name'=>ucwords(strtolower($this->input->post('section_name'))),
				'section_parent'=>$this->input->post('section_parent'),
				'section_position'=>$this->input->post('section_position'),
				'section_status'=>$this->input->post('section_status'),
				'section_icon'=>$this->input->post('section_icon'),
				'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		$this->db->where('section_id', $section_id);
		if($this->db->update('section', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single section's children
	*	@param int $section_id
	*
	*/
	public function get_sub_sections($section_id)
	{
		//retrieve all users
		$this->db->from('section');
		$this->db->select('*');
		$this->db->where('section_parent = '.$section_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single section's details
	*	@param int $section_id
	*
	*/
	public function get_section($section_id)
	{
		//retrieve all users
		$this->db->from('section');
		$this->db->select('*');
		$this->db->where('section_id = '.$section_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Delete an existing section
	*	@param int $section_id
	*
	*/
	public function delete_section($section_id)
	{
		//delete children
		if($this->db->delete('section', array('section_parent' => $section_id)))
		{
			//delete parent
			if($this->db->delete('section', array('section_id' => $section_id)))
			{
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated section
	*	@param int $section_id
	*
	*/
	public function activate_section($section_id)
	{
		$data = array(
				'section_status' => 1
			);
		$this->db->where('section_id', $section_id);
		

		if($this->db->update('section', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated section
	*	@param int $section_id
	*
	*/
	public function deactivate_section($section_id)
	{
		$data = array(
				'section_status' => 0
			);
		$this->db->where('section_id', $section_id);
		
		if($this->db->update('section', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_personnel_roles($personnel_id)
	{
		$select_sql = "SELECT
						* 

						FROM 
						(

							SELECT 
							section.* 
							
							FROM 
								section,personnel_section
							WHERE
								section.section_status = 1
								AND section.section_parent = 0
								AND section.section_id = personnel_section.section_id 
								AND personnel_section.personnel_id = 1
									
							UNION ALL
							
							
							SELECT
								* 
							FROM
								section 
							WHERE
								section.section_status = 1 
								AND section.section_parent IN (
								SELECT
									a.section_id 
								FROM
									section AS a,
									personnel_section 
								WHERE
									a.section_id = personnel_section.section_id 
								AND personnel_section.personnel_id = 1)
								
								UNION ALL
								
								SELECT 
								section.* 
								
								FROM 
									section,personnel_section
								WHERE
									section.section_status = 1
									AND section.section_parent > 0
									AND section.section_id = personnel_section.section_id 
									AND personnel_section.personnel_id = 1
									
									
									
									
						) AS data ";


		$query = $this->db->query($select_sql);

		return $query;
	}

	public function printTree_old($args){
		$tree = $args["tree"];
		$level = $args["level"];
		$me = $args["me"];

		$rs_section = $this->admin_model->get_section_details($me);
		if($rs_section->num_rows() > 0)
		{

			foreach($rs_section->result() as $res_result)
			{

				$section_parent = $res_result->section_parent;
				$section_id = $res_result->section_id;
				$section_name = $res_result->section_name;
				$section_icon = $res_result->section_icon;


				$section_id = $res_result->section_id;
				$section_name = $res_result->section_name;
				$parent = $res_result->section_parent;
				$section_status = $res_result->section_status;
				$created_by = $res_result->created_by;
				$modified_by = $res_result->modified_by;
				$icon = $res_result->section_icon;
				
				//status
				if($section_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
			
				
			
				
				//create deactivated status display
				if($section_status == 0)
				{
					$status = '<span class="label label-important">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'administration/activate-section/'.$section_id.'" onclick="return confirm(\'Do you want to activate '.$section_name.'?\');" title="Activate '.$section_name.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				//create activated status display
				else if($section_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'administration/deactivate-section/'.$section_id.'" onclick="return confirm(\'Do you want to deactivate '.$section_name.'?\');" title="Deactivate '.$section_name.'"><i class="fa fa-thumbs-down"></i></a>';
				}
			}
		}

		//if(is_int($me) and is_array($tree) and array_key_exists($me, $tree) and array_key_exists("name", $tree[$me]) and array_key_exists("children",$tree[$me])){
			echo "<tr>
						<td> $level: " . $tree[$me]["name"].' </td>
						<td>'.date('jS M Y H:i a',strtotime($row->created)).'</td>
						<td>'.date('jS M Y H:i a',strtotime($row->last_modified)).'</td>
						<td>'.$status.'</td>
						<td>
							
							<!-- Button to trigger modal -->
							<a href="#user'.$section_id.'" class="btn btn-primary" data-toggle="modal" title="Expand '.$section_name.'"><i class="fa fa-plus"></i></a>
							
							<!-- Modal -->
							<div id="user'.$section_id.'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											<h4 class="modal-title">'.$section_name.'</h4>
										</div>
										
										<div class="modal-body">
											<table class="table table-stripped table-condensed table-hover">
												<tr>
													<th>Section name</th>
													<td>'.$section_name.'</td>
												</tr>
												<tr>
													<th>Section parent</th>
													<td>'.$section_parent.'</td>
												</tr>
												<tr>
													<th>Status</th>
													<td>'.$status.'</td>
												</tr>
												<tr>
													<th>Date created</th>
													<td>'.date('jS M Y H:i a',strtotime($row->created)).'</td>
												</tr>
												<tr>
													<th>Created by</th>
													<td>'.$created_by.'</td>
												</tr>
												<tr>
													<th>Date modified</th>
													<td>'.date('jS M Y H:i a',strtotime($row->last_modified)).'</td>
												</tr>
												<tr>
													<th>Modified by</th>
													<td>'.$modified_by.'</td>
												</tr>
												<tr>
													<th>Section icon</th>
													<td><i class="fa fa-'.$icon.' fa-3x"></i></td>
												</tr>
											</table>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
											<a href="'.site_url().'administration/edit-section/'.$section_id.'" class="btn btn-sm btn-success" title="Edit '.$section_name.'"><i class="fa fa-pencil"></i></a>
											'.$button.'
											<a href="'.site_url().'administration/delete-section/'.$section_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$section_name.'?\');" title="Delete '.$section_name.'"><i class="fa fa-trash"></i></a>
										</div>
									</div>
								</div>
							</div>
						
						</td>
						<td><a href="'.site_url().'administration/edit-section/'.$section_id.'" class="btn btn-sm btn-success" title="Edit '.$section_name.'"><i class="fa fa-pencil"></i></a></td>
						<td>'.$button.'</td>
						<td><a href="'.site_url().'administration/delete-section/'.$section_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$section_name.'?\');" title="Delete '.$section_name.'"><i class="fa fa-trash"></i></a></td>
					</tr>';
			foreach($tree[$me]["children"] as $child)
				if(array_key_exists($child, $tree))
					$this->printTree_old(array("tree" => $tree, "me" => $child, "level" => "$level - "));
		//}
	}
}
?>