<?php

class Site_model extends CI_Model
{
	public function display_page_title()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		$last = $total - 1;
		$name = $this->site_model->decode_web_name($page[$last]);

		if(is_numeric($name))
		{
			$last = $last - 1;
			$name = $this->site_model->decode_web_name($page[$last]);
		}
		$page_url = ucwords(strtolower($name));

		return $page_url;
	}

	public function calculate_leave_days($start_date, $end_date, $leave_type_count = NULL)
	{
		if($leave_type_count == 2)
		{
			$start = strtotime($start);
			$end = strtotime($end);
			$datediff = $end - $start;
			return floor($datediff/(60*60*24));
		}
		
		else
		{
			return $this->getWorkingDays($start_date, $end_date);
		}
	}
	
	public function getWorkingDays($startDate, $endDate, $holidays = NULL)
	{
		// do strtotime calculations just once
		$endDate = strtotime($endDate);
		$startDate = strtotime($startDate);
	
	
		//The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
		//We add one to inlude both dates in the interval.
		$days = ($endDate - $startDate) / 86400 + 1;
	
		$no_full_weeks = floor($days / 7);
		$no_remaining_days = fmod($days, 7);
	
		//It will return 1 if it's Monday,.. ,7 for Sunday
		$the_first_day_of_week = date("N", $startDate);
		$the_last_day_of_week = date("N", $endDate);

		// var_dump($the_last_day_of_week);die();
	
		//---->The two can be equal in leap years when february has 29 days, the equal sign is added here
		//In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
		if ($the_first_day_of_week <= $the_last_day_of_week) {
			if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days -= 0.5;
			if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
		}
		else {
			// (edit by Tokes to fix an edge case where the start day was a Sunday
			// and the end day was NOT a Saturday)
	
			// the day of the week for start is later than the day of the week for end
			if ($the_first_day_of_week == 7) {
				// if the start date is a Sunday, then we definitely subtract 1 day
				$no_remaining_days--;
	
				if ($the_last_day_of_week == 6) {
					// if the end date is a Saturday, then we subtract another day
					$no_remaining_days -= 0.5;
				}
			}
			else {
				// the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
				// so we skip an entire weekend and subtract 2 days
				$no_remaining_days -= 1;
			}
		}
	
		//The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
		//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
	   $workingDays = $no_full_weeks * 6;
		if ($no_remaining_days > 0 )
		{
		  $workingDays += $no_remaining_days;
		}
		
		if($holidays != NULL)
		{
			//We subtract the holidays
			foreach($holidays as $holiday){
				$time_stamp=strtotime($holiday);
				//If the holiday doesn't fall in weekend
				if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
					$workingDays--;
			}
		}
		
		return $workingDays;
	}


	function generate_price_range()
	{
		$max_price = $this->products_model->get_max_product_price();
		//$min_price = $this->products_model->get_min_product_price();

		$interval = $max_price/5;

		$range = '';
		$start = 0;
		$end = 0;

		for($r = 0; $r < 5; $r++)
		{
			$end = $start + $interval;
			$value = 'KES '.number_format(($start+1), 0, '.', ',').' - KES '.number_format($end, 0, '.', ',');
			$range .= '
			<label class="radio-fancy">
				<input type="radio" name="agree" value="'.$start.'-'.$end.'">
				<span class="light-blue round-corners"><i class="dark-blue round-corners"></i></span>
				<b>'.$value.'</b>
			</label>';

			$start = $end;
		}

		return $range;
	}

	public function get_navigation()
	{
		$page = explode("/",uri_string());
		$total = count($page);

		$name = strtolower($page[0]);

		$home = '';
		$about = '';
		$shop = '';
		$blog = '';
		$contact = '';
		$spareparts = '';
		$sell = '';

		if($name == 'home')
		{
			$home = 'active';
		}

		if($name == 'about')
		{
			$about = 'active';
		}

		if($name == 'dobi')
		{
			$spareparts = 'active';
		}

		if($name == 'wash')
		{
			$blog = 'active';
		}

		if($name == 'contact')
		{
			$contact = 'active';
		}

		$navigation =
		'
			<li class="'.$home.'"><a href="'.site_url().'home">Home</a></li>
			<li class="dropdown mega-menu-item mega-menu-fullwidth">
				<a class="dropdown-toggle" href="#">
					Wash
				</a>
				<ul class="dropdown-menu">
					<li>
						<div class="mega-menu-content">
							<div class="row">
								<div class="col-md-3">
									<span class="mega-menu-sub-title">Category</span>
									<ul class="sub-menu">
										<li>
											<ul class="sub-menu">
												<li><a href="#">Sub category</a></li>
												<li><a href="#">Sub category</a></li>
												<li><a href="#">Sub category</a></li>
												<li><a href="#">Sub category</a></li>
											</ul>
										</li>
									</ul>
								</div>

								<div class="col-md-3">
									<span class="mega-menu-sub-title">Category</span>
									<ul class="sub-menu">
										<li>
											<ul class="sub-menu">
												<li><a href="#">Sub category</a></li>
												<li><a href="#">Sub category</a></li>
												<li><a href="#">Sub category</a></li>
												<li><a href="#">Sub category</a></li>
											</ul>
										</li>
									</ul>
								</div>
								<div class="col-md-3">
									<span class="mega-menu-sub-title">Category</span>
									<ul class="sub-menu">
										<li>
											<ul class="sub-menu">
												<li><a href="#">Sub category</a></li>
												<li><a href="#">Sub category</a></li>
												<li><a href="#">Sub category</a></li>
												<li><a href="#">Sub category</a></li>
											</ul>
										</li>
									</ul>
								</div>
								<div class="col-md-3">
									<span class="mega-menu-sub-title">Category</span>
									<ul class="sub-menu">
										<li>
											<ul class="sub-menu">
												<li><a href="#">Sub category</a></li>
												<li><a href="#">Sub category</a></li>
												<li><a href="#">Sub category</a></li>
												<li><a href="#">Sub category</a></li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="'.$sell.'"><a href="'.site_url().'blog">Sell</a></li>
			<li class="'.$about.'"><a href="'.site_url().'about">About</a></li>
			<li class="'.$contact.'"><a href="'.site_url().'contact">Contact</a></li>

		';

		return $navigation;
	}

	public function create_web_name($field_name)
	{
		$web_name = str_replace(" ", "-", $field_name);

		return $web_name;
	}

	public function decode_web_name($web_name)
	{
		$field_name = str_replace("-", " ", $web_name);

		return $field_name;
	}

	public function image_display($base_path, $location, $image_name = NULL)
	{
		$default_image = 'http://placehold.it/300x300&text=Autospares';
		$file_path = $base_path.'/'.$image_name;
		//echo $file_path.'<br/>';

		//Check if image was passed
		if($image_name != NULL)
		{
			if(!empty($image_name))
			{
				if((file_exists($file_path)) && ($file_path != $base_path.'\\'))
				{
					return $location.$image_name;
				}

				else
				{
					return $default_image;
				}
			}

			else
			{
				return $default_image;
			}
		}

		else
		{
			return $default_image;
		}
	}

	public function get_contacts($branch_id = NULL)
	{
		if(!empty($branch_id))
		{

			$this->db->where('branch_id',$branch_id);
		}
		else
		{
			$this->db->where('branch_id',2);
		}

  		$table = "branch";

		$query = $this->db->get($table);
		$contacts = array();
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$contacts['email'] = $row->branch_email;
			$contacts['phone'] = $row->branch_phone;
			$contacts['company_name'] = $row->branch_name;
			$contacts['logo'] = $row->branch_image_name;
			$contacts['address'] = $row->branch_address;
			$contacts['city'] = $row->branch_city;
			$contacts['post_code'] = $row->branch_post_code;
			$contacts['building'] = $row->branch_building;
			$contacts['floor'] = $row->branch_floor;
			$contacts['location'] = $row->branch_location;
			$contacts['branch_pin']=$row->branch_pin;
			$contacts['branch_vat']=$row->branch_vat;
			$contacts['color']=$row->branch_color;
			$contacts['working_weekend'] = $row->branch_working_weekend;
			$contacts['working_weekday'] = $row->branch_working_weekday;
			$contacts['minimal_dashboard'] = $row->minimal_dashboard;
			$contacts['doctor_email'] = $row->doctor_email;
			$contacts['calendar_slots'] = $row->calendar_slots;
			$contacts['queue_notification'] = $row->queue_notification;
			$contacts['branch_kra_pin'] = $row->branch_kra_pin;
		}
		return $contacts;
	}

	public function get_breadcrumbs()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		$last = $total - 1;
		$crumbs = '<li><a href="'.site_url().'home">Home </a></li>';

		for($r = 0; $r < $total; $r++)
		{
			$name = $this->decode_web_name($page[$r]);
			if($r == $last)
			{
				$crumbs .= '<li class="active">'.strtoupper($name).'</li>';
			}
			else
			{
				if($total == 3)
				{
					if($r == 1)
					{
						$crumbs .= '<li><a href="'.site_url().$page[$r-1].'/'.strtolower($name).'">'.strtoupper($name).'</a></li>';
					}
					else
					{
						$crumbs .= '<li><a href="'.site_url().strtolower($name).'">'.strtoupper($name).'</a></li>';
					}
				}
				else
				{
					$crumbs .= '<li><a href="'.site_url().strtolower($name).'">'.strtoupper($name).'</a></li>';
				}
			}
		}

		return $crumbs;
	}

	public function get_configuration()
	{
		return $this->db->get('configuration');
	}
	public function get_all_branches()
	{
		$this->db->where('branch_id > 0');

		$query = $this->db->get('branch');

		return $query;
	}

	public function save_other_patient()
	{
		$current_patient_number = $this->input->post('current_patient_number');

		$patient_phone1 = $this->input->post('patient_phone1');
		$session_id = $this->input->post('session_id');


		$year = date('Y');
		$data = array(
						'patient_surname'=>ucwords(strtolower($this->input->post('patient_surname'))),
						'patient_othernames'=>'',
						'title_id'=>$this->input->post('title_id'),
						'patient_date_of_birth'=>$this->input->post('patient_dob'),
						'gender_id'=>$this->input->post('gender_id'),
						'patient_email'=>$this->input->post('patient_email'),
						'patient_phone1'=>$patient_phone1,
						'patient_phone2'=>$this->input->post('patient_phone2'),
						'patient_kin_sname'=>$this->input->post('patient_kin_sname'),
						'patient_kin_othernames'=>$this->input->post('patient_kin_othernames'),
						'relationship_id'=>$this->input->post('relationship_id'),
						'patient_national_id'=>$this->input->post('patient_national_id'),
						'patient_date'=>date('Y-m-d H:i:s'),
						'patient_year'=>$year,
						'created_by'=>$this->session->userdata('personnel_id'),
						'modified_by'=>$this->session->userdata('personnel_id'),
						'visit_type_id'=>$this->input->post('visit_type_id'), //retrieves the data input from the view and stores it in the visit_type_id column in the database
						'patient_occupation'=>$this->input->post('patient_occupation'),
						'dependant_id'=>$this->input->post('dependant_id'),
						'current_patient_number'=>$this->input->post('current_patient_number'),
						'branch_code'=>$this->session->userdata('branch_code'),
						'patient_town'=>$this->input->post('patient_town'),
						'patient_kin_phonenumber1'=>$this->input->post('next_of_kin_contact'),
						'insurance_company_id'=>$this->input->post('insurance_company_id'),
						'about_us'=>$this->input->post('about_us'),
						'about_us_view'=>$this->input->post('about_us_view'),
						'reason_for_visit'=>$this->input->post('reason_for_visit'),
						'other_medical_conditions'=>$this->input->post('other_medical_conditions'),
						'medication'=>$this->input->post('medication'),
						'allergies'=>$this->input->post('allergies'),
						'branch_id'=>2,
						'category_id'=>3
					);
		// var_dump($session_id);die();
			if($this->db->insert('patients', $data))
			{
				$patient_id = $this->db->insert_id();

				// update all patients details with the information
				$visit_array['visit_id'] = 0;
				$visit_array['patient_id'] = $patient_id;
				$this->db->where('session_id = '.$session_id.'');
				$this->db->update('patient_history_result',$visit_array);

				$medical_array['patient_id'] = $patient_id;
				$this->db->where('session_id = '.$session_id.'');
				$this->db->update('patient_medical_history_result',$medical_array);


				return TRUE;
			}
			else{
				return FALSE;
			}

	}
	// public function get_configuration()
	// {
	// 	return $this->db->get('configuration');
	// }

	public function update_trail($array_add)
	{

		$this->db->insert('visit_trail',$array_add);
	}

	public function get_patient_name_by_id($patient_id)
	{
		$patient_surname ='';

		if($patient_id > 0)
		{
			$this->db->where('patient_id', $patient_id);
			$query = $this->db->get('patients');

			
			if($query->num_rows() > 0)
			{
				foreach($query->result() AS $key => $value)
				{
					$patient_surname = $value->patient_surname;
				}
				$patient_surname = $patient_surname;
			}
		}

		
		return $patient_surname;
	}

	public function get_personnel_name_by_id($personnel_id)
	{
		$personnel_name ='';

		if($personnel_id > 0)
		{
			$this->db->where('personnel_id', $personnel_id);
			$query = $this->db->get('personnel');

			
			if($query->num_rows() > 0)
			{
				foreach($query->result() AS $key => $value)
				{
					$personnel_fname = $value->personnel_fname;
					$personnel_onames = $value->personnel_onames;
				}
				$personnel_name = $personnel_fname;
			}
		}
		  // var_dump($personnel_name);die();
		
		return $personnel_name;
	}


	public function get_visit_trail($visit_id)
	{
		$where = 'visit_department.visit_id = '.$visit_id.' AND visit_department.department_id = departments.department_id';
		$this->db->select('departments.department_name, visit_department.*, personnel.personnel_fname');
		$this->db->where($where);
		$this->db->join('personnel', 'visit_department.created_by = personnel.personnel_id', 'left');
		$this->db->order_by('visit_department.created','ASC');
		$query = $this->db->get('visit_department, departments');
		
		return $query;
	}
	public function get_main_trail($visit_id)
	{
		$where = 'visit_trail.visit_id = '.$visit_id.'';
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('visit_trail.datetime','ASC');
		$query = $this->db->get('visit_trail');
		
		return $query;
	}
	public function get_patient_departments_list()
	{

		
		$where = 'departments.department_id > 0 AND departments.color_code IS NOT NULL';
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get('departments');

		return $query;

	}
	public function get_personnel_department_id($personnel_id)
	{
		$this->db->where('personnel_job.department_id > 0 AND personnel_id = '.$personnel_id);
		$this->db->limit(1);
		$query=$this->db->get('personnel_job');
		if($query->num_rows() > 0)
		{
			// $rows = (array)$query->rows();

			foreach($query->result() AS $key => $value)
			{
				$department_id = $value->department_id;
			}

			

			return $department_id;
		}
		else
		{
			return 0;
		}
	}
	public function get_company_configuration()
	{
		return $this->db->get('company_configuration');
	}
	
  	public function get_accounts_names()
  	{
  		$select_query = "SELECT 
	  							data.* 
	  						FROM 
	  							(
	  								SELECT 
	  									creditor.creditor_id AS creditor_id,
	  									creditor.creditor_name AS creditor_name,
	  									1 AS billing_type,
	  									'CREDITOR' AS billing_type_name
	  								FROM 
	  									creditor
	  								WHERE 
	  									creditor_id > 0
	  									AND creditor_status = 0

	  								UNION ALL

	  								SELECT 
	  									provider.provider_id AS creditor_id,
	  									provider.provider_name AS creditor_name,
	  									2 AS billing_type,
	  									'PROVIDER' AS billing_type_name
	  								FROM 
	  									provider
	  								WHERE 
	  									provider_id > 0
	  									AND provider_status = 0

	  								UNION ALL

	  								SELECT 
	  									account.account_id AS creditor_id,
	  									account.account_name AS creditor_name,
	  									3 AS billing_type,
	  									'STATUTORIES' AS billing_type_name
	  								FROM 
	  									statutory_accounts,account
	  								WHERE 
	  									account.account_id > 0
	  									AND account_deleted = 0
	  									AND statutory_accounts.account_id = account.account_id

	  							)
	  						AS data 
	  						WHERE data.creditor_id > 0

  						";

  		$query = $this->db->query($select_query);


  		return $query;
  	}
  	public function get_staging_accounts($report_status = 0)
	{

		if(!empty($report_status))
		{
			$add = ' AND account_staging.bs_account_balance_report = '.$report_status;
		}
		else
		{
			$add =  '';
		}

		$this->db->from('account_staging');
		$this->db->select('account_staging.*');
		$this->db->where('account_staging_delete = 0'.$add);
		$query = $this->db->get();

		return $query;

	}

	public function get_blacklisted()
	{
		$this->db->where('message_blacklist_id > 0');
		$query = $this->db->get('message_blacklist');

		$blacklist_array = array();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				// code...
				$blacklist_array[$key->phone_number] = $key;
			}
		}
		// var_dump($query);die();
		return $blacklist_array;
	}

	public function check_string($input_string) 
	{
        // Check if the string length is 11 characters
        if (strlen($input_string) == 11) {
            // Get the first character of the string
            $first_character = substr($input_string, 0, 1);
            
            // Check if the first character is either 'P' or 'A'
            if ($first_character == 'P' || $first_character == 'A') {
                return true; // First letter is 'P' or 'A', and total characters is 11
            } else {
                return false; // First letter is not 'P' or 'A'
            }
        } else {
            return false; // Total characters is not 11
        }
    }

    public function update_visit_charges($visit_invoice_id = null)
	{
		$add = '';
		if($visit_invoice_id > 0)
			$add = ' AND visit_invoice_id = '.$visit_invoice_id;

		$this->db->where('visit_charge_id > 0 AND (account_id = 0 OR service_id = 0)'.$add);
		// $this->db->limit(10000);
		$query = $this->db->get('visit_charge');
		// var_dump($query);die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...

				$service_charge_id = $value->service_charge_id;
				$visit_charge_id = $value->visit_charge_id;

				$query = $this->cloud_model->get_service_charge_details($service_charge_id);

				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						// code...
						$account_id = $value->account_id;
						$service_id = $value->service_id;
						$service_charge_name = $value->service_charge_name;
						$amount = $value->service_charge_amount;
					}
				}


				$array['account_id'] = $account_id;
				$array['service_id'] = $service_id;
				$array['charge_name'] = $service_charge_name;


				$this->db->where('visit_charge_id',$visit_charge_id);
				$this->db->update('visit_charge',$array);

			}
		}

		$this->update_visit_credit_note($visit_invoice_id);
	}


	public function update_visit_credit_note($visit_invoice_id=null)
	{

		$add = '';
		if($visit_invoice_id > 0)
			$add = ' AND visit_credit_note.visit_invoice_id = '.$visit_invoice_id;
		$this->db->where('visit_credit_note_item_id > 0 AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id AND (visit_credit_note_item.account_id = 0 OR visit_credit_note_item.service_id = 0)'.$add);
		// $this->db->limit(10000);
		$query = $this->db->get('visit_credit_note_item,visit_credit_note');
		// var_dump($query);die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...

				$service_charge_id = $value->service_charge_id;
				$visit_credit_note_item_id = $value->visit_credit_note_item_id;

				$query = $this->cloud_model->get_service_charge_details($service_charge_id);
				$account_id = 0;
				$service_id = 0;
				$service_charge_name = '';
				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						// code...
						$account_id = $value->account_id;
						$service_id = $value->service_id;
						$service_charge_name = $value->service_charge_name;
						$amount = $value->service_charge_amount;
					}
				}




				$array['account_id'] = $account_id;
				$array['service_id'] = $service_id;
				$array['charge_name'] = $service_charge_name;


				$this->db->where('visit_credit_note_item_id',$visit_credit_note_item_id);
				$this->db->update('visit_credit_note_item',$array);

			}
		}
	}

	

    public function get_company()
	{

  		$table = "company";
  		$this->db->limit(1);
		$query = $this->db->get($table);
		$contacts = array();
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$company_status = $row->company_status;
			$company_uid = $row->company_uid;

            setcookie('organization_unique_id', $company_uid, time() + (86400 * 30), "/"); // 86400 = 1 day
            setcookie('active', $company_status, time() + (86400 * 30), "/"); // 86400 = 1 day
		}
		else
		{
			unset($_COOKIE['organization_unique_id']); 
		    setcookie('organization_unique_id', '', -1, '/'); 

		    unset($_COOKIE['active']); 
		    setcookie('active', '', -1, '/'); 

		}

	}

	function run_migration()
	{
		 // Check if 'authorize_multiple_branches' column exists
		
        $this->up();
	}
	function get_company_setting()
	{
		return $this->db->get('company_setting');
	}
	public function up() {


		// $tables = [
		//             'table_name_1' => "
		//                 CREATE TABLE IF NOT EXISTS `visit_treatment_plan` (
		// 				  `visit_treatment_plan_id` int(11) NOT NULL AUTO_INCREMENT,
		// 				  `clinical_findings` text DEFAULT NULL,
		// 				  `patient_id` int(11) DEFAULT 0,
		// 				  `visit_id` int(11) DEFAULT NULL,
		// 				  `visit_treatment_plan_delete` int(11) DEFAULT 0,
		// 				  `created` date DEFAULT NULL,
		// 				  `created_by` int(11) DEFAULT NULL,
		// 				  `modified` date DEFAULT NULL,
		// 				  `modified_by` int(11) DEFAULT NULL,
		// 				  PRIMARY KEY (`visit_treatment_plan_id`) USING BTREE
		// 				) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
		//             ",

		//             'table_name_2' => "
		//                 CREATE TABLE IF NOT EXISTS `visit_treatment_plan_image` (
		// 				  `visit_treatment_plan_image_id` int(11) NOT NULL AUTO_INCREMENT,
		// 				  `visit_treatment_plan_id` int(11) DEFAULT 0,
		// 				  `visit_id` int(11) DEFAULT NULL,
		// 				  `patient_id` int(11) DEFAULT 0,
		// 				  `image_name` text DEFAULT NULL,
		// 				  `created` date DEFAULT NULL,
		// 				  `created_by` int(11) DEFAULT NULL,
		// 				  `modified` date DEFAULT NULL,
		// 				  `modified_by` int(11) DEFAULT NULL,
		// 				  `visit_treatment_plan_image_delete` int(11) DEFAULT 0,
		// 				  PRIMARY KEY (`visit_treatment_plan_image_id`) USING BTREE
		// 				) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
		//             ",

		//             'table_name_3' => "
		//                 CREATE TABLE IF NOT EXISTS `visit_treatment_plan_item` (
		// 				  `visit_treatment_plan_item_id` int(11) NOT NULL AUTO_INCREMENT,
		// 				  `visit_treatment_plan_id` text DEFAULT NULL,
		// 				  `visit_id` int(11) DEFAULT NULL,
		// 				  `patient_id` int(11) DEFAULT 0,
		// 				  `service_charge_id` int(11) DEFAULT 0,
		// 				  `amount` decimal(10,0) DEFAULT 0,
		// 				  `period` varchar(255) DEFAULT NULL,
		// 				  `charge_name` varchar(255) DEFAULT NULL,
		// 				  `created` date DEFAULT NULL,
		// 				  `created_by` int(11) DEFAULT NULL,
		// 				  `modified` date DEFAULT NULL,
		// 				  `modified_by` int(11) DEFAULT NULL,
		// 				  `visit_treatment_plan_item_delete` int(11) DEFAULT 0,
		// 				  PRIMARY KEY (`visit_treatment_plan_item_id`) USING BTREE
		// 				) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
		//             ",
		//             // Add more tables as needed
		//         ];

	    //     // Create tables if they do not exist
	    //     foreach ($tables as $table => $create_sql) {
	    //         if (!$this->db->table_exists($table)) {
	    //             $this->db->query($create_sql);
	    //         }
	    //     }



	        $fields = [
			            ['table' => 'personnel', 'column' => 'authorize_multiple_branches', 'type' => 'int(1)', 'default' => 0],
			            ['table' => 'visit_invoice', 'column' => 'invoice_closed_status', 'type' => 'int(1)', 'default' => 0],
			            ['table' => 'visit', 'column' => 'sync_status', 'type' => 'int(1)', 'default' => 0],
			            ['table' => 'visit', 'column' => 'visit_account', 'type' => 'varchar(200)', 'default' => 'NULL'],
			            ['table' => 'visit_invoice', 'column' => 'billing_account_id', 'type' => 'int(1)', 'default' => 1],
			            ['table' => 'visit_invoice', 'column' => 'billing_account_rate', 'type' => 'decimal(11,2)', 'default' => 1.00],
			            ['table' => 'payment_item', 'column' => 'billing_account_id', 'type' => 'int(1)', 'default' => 1],
			            ['table' => 'payment_item', 'column' => 'billing_account_rate', 'type' => 'decimal(11,2)', 'default' => 1.00],
			            ['table' => 'visit_invoice', 'column' => 'kra_id', 'type' => 'int(11)', 'default' => 0],
			            ['table' => 'visit_invoice', 'column' => 'kra_pin', 'type' => 'varchar(12)', 'default' => 'NULL'],
			            ['table' => 'visit_invoice', 'column' => 'customer_name', 'type' => 'varchar(255)', 'default' => 'NULL'],
			            ['table' => 'visit_invoice', 'column' => 'kra_customer_id', 'type' => 'int(11)', 'default' => 0],
			            ['table' => 'visit_type', 'column' => 'insurance_kra_pin', 'type' => 'int(11)', 'default' => 0],

			            ['table' => 'account_payments', 'column' => 'recipient_name', 'type' => 'int(11)', 'default' => 0],
			            ['table' => 'account_payments', 'column' => 'recipient_id', 'type' => 'int(11)', 'default' => 0],
			            ['table' => 'account_payments', 'column' => 'payment_for', 'type' => 'varchar(300)','default'=> 'NULL'],


			            
			        ];

	        // Process each field
	        foreach ($fields as $field) {
	            $table = $field['table'];
	            $column = $field['column'];
	            $type = $field['type'];
	            
	            if(isset($field['default']))
	            $default = is_numeric($field['default']) ? $field['default'] : "'".$field['default']."'";

	            // Check if the field already exists
	            if (!$this->db->field_exists($column, $table)) {
	                // If the field does not exist, create it
	                $sql = "ALTER TABLE `$table`
	                        ADD COLUMN `$column` $type DEFAULT $default";
	                $this->db->query($sql);
	            }
	        }



        // Define the indexes to be added
         $indexes = [
            ['table' => 'visit_invoice', 'column' => 'created_by', 'index' => 'created_by'],
            ['table' => 'visit_invoice', 'column' => 'dentist_id', 'index' => 'dentist_id'],
            ['table' => 'visit_invoice', 'column' => 'patient_id', 'index' => 'patient_id'],
            ['table' => 'visit_invoice', 'column' => 'created', 'index' => 'created'],
            ['table' => 'visit_invoice', 'column' => 'visit_invoice_delete', 'index' => 'visit_invoice_delete'],
            ['table' => 'visit_invoice', 'column' => 'visit_id', 'index' => 'visit_id'],

            ['table' => 'visit_charge', 'column' => 'visit_id', 'index' => 'visit_id'],
            ['table' => 'visit_charge', 'column' => 'service_charge_id', 'index' => 'service_charge_id'],
            ['table' => 'visit_charge', 'column' => 'patient_id', 'index' => 'patient_id'],
            ['table' => 'visit_charge', 'column' => 'visit_invoice_id', 'index' => 'visit_invoice_id'],
            ['table' => 'visit_charge', 'column' => 'charged', 'index' => 'charged'],
            ['table' => 'visit_charge', 'column' => 'service_id', 'index' => 'service_id'],
            ['table' => 'visit_charge', 'column' => 'account_id', 'index' => 'account_id'],


            ['table' => 'payments', 'column' => 'payment_date', 'index' => 'payment_date'],
            ['table' => 'payments', 'column' => 'payment_method_id', 'index' => 'payment_method_id'],
            ['table' => 'payments', 'column' => 'visit_id', 'index' => 'visit_id'],
            ['table' => 'payments', 'column' => 'patient_id', 'index' => 'patient_id'],
            ['table' => 'payments', 'column' => 'payment_type', 'index' => 'payment_type'],
            ['table' => 'payments', 'column' => 'cancel', 'index' => 'cancel'],
            ['table' => 'payments', 'column' => 'visit_type_id', 'index' => 'visit_type_id'],
            ['table' => 'payment_item', 'column' => 'visit_invoice_id', 'index' => 'visit_invoice_id'],
            ['table' => 'payment_item', 'column' => 'visit_id', 'index' => 'visit_id'],
            ['table' => 'payment_item', 'column' => 'payment_id', 'index' => 'payment_id'],


            ['table' => 'appointments', 'column' => 'visit_id', 'index' => 'visit_id'],
            ['table' => 'appointments', 'column' => 'appointment_date', 'index' => 'appointment_date'],
            ['table' => 'appointments', 'column' => 'resource_id', 'index' => 'resource_id'],
            ['table' => 'appointments', 'column' => 'appointment_status', 'index' => 'appointment_status'],
            ['table' => 'appointments', 'column' => 'appointment_start_time', 'index' => 'appointment_start_time'],
            ['table' => 'appointments', 'column' => 'appointment_end_time', 'index' => 'appointment_end_time'],

            ['table' => 'visit', 'column' => 'personnel_id', 'index' => 'personnel_id'],
            ['table' => 'visit', 'column' => 'appointment_id', 'index' => 'appointment_id'],
            ['table' => 'visit', 'column' => 'visit_date', 'index' => 'visit_date'],
            ['table' => 'visit', 'column' => 'patient_id', 'index' => 'patient_id'],
            ['table' => 'visit', 'column' => 'visit_time', 'index' => 'visit_time'],
            ['table' => 'visit', 'column' => 'department_id', 'index' => 'department_id'],

            ['table' => 'patient_account', 'column' => 'patient_id', 'index' => 'patient_id'],
            ['table' => 'patient_account', 'column' => 'visit_type_id', 'index' => 'visit_type_id'],


            // Add more indexes as needed
        ];

        // Process each index
        foreach ($indexes as $index) {
            $table = $index['table'];
            $column = $index['column'];
            $index_name = $index['index'];

            // Check if the index already exists
            $query = $this->db->query("SHOW INDEX FROM `$table` WHERE Key_name = '$index_name'");
            if ($query->num_rows() == 0) {
                // If the index does not exist, create it with type NORMAL and method BTREE
                $sql = "ALTER TABLE `$table`
                        ADD INDEX `$index_name` (`$column`) USING BTREE";
                $this->db->query($sql);
            }
        }
    }

    public function down() {
        // Define the indexes to be removed
        $indexes = [
            ['table' => 'your_table_name_1', 'index' => 'your_index_name_1'],
            ['table' => 'your_table_name_2', 'index' => 'your_index_name_2'],
            // Add more indexes as needed
        ];

        // Process each index
        foreach ($indexes as $index) {
            $table = $index['table'];
            $index_name = $index['index'];

            // Check if the index exists before attempting to remove it
            $query = $this->db->query("SHOW INDEX FROM `$table` WHERE Key_name = '$index_name'");
            if ($query->num_rows() > 0) {
                // If the index exists, remove it
                $sql = "ALTER TABLE `$table`
                        DROP INDEX `$index_name`";
                $this->db->query($sql);
            }
        }
    }
	
	function generate_code($code_type) 
	{


		// Fetch configuration settings from the database
		$configuration_rs = $this->site_model->get_company_setting();
		$configuration_array = [];

		if (!$configuration_rs) {
		    die('No configuration settings found');
		}

		foreach ($configuration_rs->result() as $key) {
		    $configuration_array[$key->company_setting_name] = $key;
		}


		$patient_prefix = '';
		$patient_suffix = '001';
		$patient_format = 'suffix'; // Example format
		$patient_start_point = 1;
		$year_format = '';
		$patient_suffix_zero_padding = false; // Default value
		$patient_auto_increment_threshold = 100000; // Default value
		$patient_delimiter = '/'; // Default value
		$format_year = 'Y';


		$invoice_prefix = '';
		$invoice_suffix = '001';
		$invoice_format = 'suffix'; // Example format
		$invoice_start_point = 1;
		$year_format = '';
		$invoice_suffix_zero_padding = false; // Default value
		$invoice_auto_increment_threshold = 100000; // Default value
		$invoice_delimiter = '-'; // Default value



		$receipt_prefix = '';
		$receipt_suffix = '001';
		$receipt_format = 'suffix'; // Example format
		$receipt_start_point = 1;
		$year_format = '';
		$receipt_suffix_zero_padding = false; // Default value
		$receipt_auto_increment_threshold = 100000; // Default value
		$receipt_delimiter = ''; // Default value



		if($code_type == 'patient_number')
		{


			if (isset($configuration_array['patient_prefix'])) {
			    $patient_prefix = $configuration_array['patient_prefix']->company_setting_value;
			}

			if (isset($configuration_array['patient_suffix'])) {
			    $patient_suffix = $configuration_array['patient_suffix']->company_setting_value;
			}

			if (isset($configuration_array['patient_format'])) {
			    $patient_format = $configuration_array['patient_format']->company_setting_value;
			}

			if (isset($configuration_array['patient_start_point'])) {
			    $patient_start_point = $configuration_array['patient_start_point']->company_setting_value;
			}

			if (isset($configuration_array['patient_year'])) {
			    $year_format = $configuration_array['patient_year']->company_setting_value;
			    $format_year = $configuration_array['patient_year']->company_setting_particular;
			}

			if (isset($configuration_array['patient_suffix_zero_padding'])) {
			    $patient_suffix_zero_padding = filter_var($configuration_array['patient_suffix_zero_padding']->company_setting_particular, FILTER_VALIDATE_BOOLEAN);
			    $patient_auto_increment_threshold = $configuration_array['patient_suffix_zero_padding']->company_setting_value;
			}

			if (isset($configuration_array['patient_delimiter'])) {
			    $patient_delimiter = $configuration_array['patient_delimiter']->company_setting_value;
			}

		}
		else if($code_type == 'invoice_number')
		{


			if (isset($configuration_array['invoice_prefix'])) {
			    $invoice_prefix = $configuration_array['invoice_prefix']->company_setting_value;
			}

			if (isset($configuration_array['invoice_suffix'])) {
			    $invoice_suffix = $configuration_array['invoice_suffix']->company_setting_value;
			}

			if (isset($configuration_array['invoice_format'])) {
			    $invoice_format = $configuration_array['invoice_format']->company_setting_value;
			}

			if (isset($configuration_array['invoice_start_point'])) {
			    $invoice_start_point = $configuration_array['invoice_start_point']->company_setting_value;
			}

			if (isset($configuration_array['invoice_year'])) {
			    $year_format = $configuration_array['invoice_year']->company_setting_value;
			    $format_year = $configuration_array['invoice_year']->company_setting_particular;
			}
			if (isset($configuration_array['invoice_delimiter'])) {
			    $invoice_delimiter = $configuration_array['invoice_delimiter']->company_setting_value;
			}

			if (isset($configuration_array['invoice_suffix_zero_padding'])) {
			    $invoice_suffix_zero_padding = filter_var($configuration_array['invoice_suffix_zero_padding']->company_setting_particular, FILTER_VALIDATE_BOOLEAN);
			    $invoice_auto_increment_threshold = $configuration_array['invoice_suffix_zero_padding']->company_setting_value;
			}

		}

		else if($code_type == 'receipt_number')
		{


			if (isset($configuration_array['receipt_prefix'])) {
			    $receipt_prefix = $configuration_array['receipt_prefix']->company_setting_value;
			}

			if (isset($configuration_array['receipt_suffix'])) {
			    $receipt_suffix = $configuration_array['receipt_suffix']->company_setting_value;
			}

			if (isset($configuration_array['receipt_format'])) {
			    $receipt_format = $configuration_array['receipt_format']->company_setting_value;
			}

			if (isset($configuration_array['receipt_start_point'])) {
			    $receipt_start_point = $configuration_array['receipt_start_point']->company_setting_value;
			}

			if (isset($configuration_array['receipt_year'])) {
			    $year_format = $configuration_array['receipt_year']->company_setting_value;
			    $format_year = $configuration_array['receipt_year']->company_setting_particular;
			}
			if (isset($configuration_array['receipt_delimiter'])) {
			    $receipt_delimiter = $configuration_array['receipt_delimiter']->company_setting_value;
			}
			if (isset($configuration_array['receipt_suffix_zero_padding'])) {
			    $receipt_suffix_zero_padding = filter_var($configuration_array['receipt_suffix_zero_padding']->company_setting_particular, FILTER_VALIDATE_BOOLEAN);
			    $receipt_auto_increment_threshold = $configuration_array['receipt_suffix_zero_padding']->company_setting_value;
			}

		}
		if(empty($format_year))
			$format_year = 'Y';

		
		$code_formats = [
		    'patient_number' => [
		        'format' => $patient_format,
		        'prefix' => $patient_prefix,
		        'suffix' => $patient_suffix,
		        'start_point' => $patient_start_point,
		        'year_format' => $year_format,
		        'suffix_zero_padding' => $patient_suffix_zero_padding,
		        'auto_increment_threshold' => $patient_auto_increment_threshold
		    ],
		    'invoice_number' => [
		        'format' => $invoice_format,
		        'prefix' => $invoice_prefix,
		        'suffix' => $invoice_suffix,
		        'start_point' => $invoice_start_point,
		        'year_format' => $year_format,
		        'suffix_zero_padding' => $invoice_suffix_zero_padding,
		        'auto_increment_threshold' => $invoice_auto_increment_threshold
		    ],
		    'receipt_number' => [
		        'format' => $receipt_format,
		        'prefix' => $receipt_prefix,
		        'suffix' => $receipt_suffix,
		        'start_point' => $receipt_start_point,
		        'year_format' => $year_format,
		        'suffix_zero_padding' => $receipt_suffix_zero_padding,
		        'auto_increment_threshold' => $receipt_auto_increment_threshold
		    ]
		];


		if($code_type == "patient_number")
		{
			$suffix_zero_padding = $patient_suffix_zero_padding;
			$auto_increment_threshold = $patient_auto_increment_threshold;
			$delimiter = $patient_delimiter;
		}
		else if($code_type == "invoice_number")
		{
			$suffix_zero_padding = $invoice_suffix_zero_padding;
			$auto_increment_threshold = $invoice_auto_increment_threshold;
			$delimiter = $invoice_delimiter;
		}
		else if($code_type == "receipt_number")
		{
			$suffix_zero_padding = $receipt_suffix_zero_padding;
			$auto_increment_threshold = $receipt_auto_increment_threshold;
			$delimiter = $receipt_delimiter;
		}

		if (!isset($code_type)) {
		    die('Code type not set');
		}

		if (!isset($code_formats[$code_type])) {
		    die('Code format not found for type: ' . $code_type);
		}

		$format = $code_formats[$code_type];
		$code = '';
		$parameters = [];

		preg_match_all('/[^\/#&]+/', $format['format'], $matches);
		$delimiters = $matches[0];
		$parts = explode($delimiter, $format['format']);

		foreach ($parts as $index => $part) {
		    switch ($part) {
		        case 'prefix':
		            $code .= !empty($format['prefix']) ? $format['prefix'] : '';
		            $parameters['prefix'] = !empty($format['prefix']) ? $format['prefix'] : '';
		            break;
		        case 'suffix':
		            $auto_increment = 1;
		            // var_dump($format['suffix']);die();
		            if (!empty($format['suffix'])) {

		            	if($code_type == "patient_number" AND empty($year_format))
		                $last_suffix = $this->db->select('MAX(CAST(suffix AS UNSIGNED)) AS suffix')
		                                        ->get_where('patients', ['patient_delete' => 0])
		                                        ->row_array();
		                else if($code_type == "patient_number" AND $year_format == true)
		                	$last_suffix = $this->db->select('MAX(CAST(suffix AS UNSIGNED)) AS suffix')
				                            ->get_where('patients', ['patient_delete' => 0, 'patient_year' => date('Y')])
				                            ->row_array();
		                else if($code_type == "invoice_number")
		                $last_suffix = $this->db->select('MAX(CAST(suffix AS UNSIGNED)) AS suffix')
		                                        ->get_where('visit_invoice', ['visit_invoice_delete' => 0])
		                                        ->row_array();
		                else if($code_type == "receipt_number")
		                $last_suffix = $this->db->select('MAX(CAST(suffix AS UNSIGNED)) AS suffix')
		                                        ->get_where('payments', ['cancel' => 0])
		                                        ->row_array();                     	
		                
		                $auto_increment = isset($last_suffix['suffix']) ? intval($last_suffix['suffix']) + 1 : $format['start_point'];

		                if ($suffix_zero_padding && $auto_increment < $auto_increment_threshold) {
		                    $padding_length = strlen(strval($auto_increment_threshold)) - 1;
		                    $auto_increment = str_pad($auto_increment, $padding_length, '0', STR_PAD_LEFT);
		                }
		            }

		            if($format['year_format'] == true OR ($code_type == 'patient_number' AND $delimiter == "&"))
					$auto_increment = ltrim($auto_increment, '0');

		            $code .= $auto_increment;
		            $parameters['suffix'] = $auto_increment;
		            break;
		        case 'year':
		        	if($code_type == "patient_number")
		        	{
			            $code .= !empty($format['year_format']) ? date(date($format_year)) : '';
			            $parameters['year'] = !empty($format['year_format']) ? date(date($format_year)) : '';
			        }
		            break;
		        default:
		            $code .= $part;
		            $parameters[$part] = $part;
		            break;
		    }
		    if ($index < count($parts) - 1) {
		        $code .= $delimiter;
		    }
		}
		


		if(($code_type == 'receipt_number' OR $code_type == 'invoice_number' OR $code_type == 'patient_number') AND !empty($code) AND $delimiter == "&")
		$code = str_replace('&', '', $code);


		if(empty($format['prefix']))
			$code = ltrim($code, '0');
		else if($format['year_format'] == true AND $code_type == 'patient_number')
			$code = ltrim($code, '0');
			
		if(!empty($code))
			$status = 'success';
		else
			$status = 'fail';
		
		// var_dump($code);die();
		return ['code_type'=>$code_type,'status'=>$status,'code' => $code, 'parameters' => $parameters];








	}
}

?>
