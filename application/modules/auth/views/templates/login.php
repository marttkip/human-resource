<?php

$contacts = $this->site_model->get_contacts();

if (count($contacts) > 0) {
    $email = $contacts['email'];
    $email2 = $contacts['email'];
    $logo = $contacts['logo'];
    $company_name = $contacts['company_name'];
    $phone = $contacts['phone'];

    if (!empty($facebook)) {
        $facebook = '<li class="facebook"><a href="' . $facebook . '" target="_blank" title="Facebook">Facebook</a></li>';
    }
} else {
    $email = '';
    $facebook = '';
    $twitter = '';
    $linkedin = '';
    $logo = '';
    $company_name = '';
    $google = '';
}
?>
<!doctype html>
<html>

<head>
    <?php echo $this->load->view('admin/includes/header', $contacts, TRUE); ?>
    <style>
        .topbar {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #018ec4;
        }

        .circle {
            width: 400px;
            height: 400px;
            background-color: white;
            border-radius: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            overflow: hidden;
        }

        .circle img {
            width: 70%;
            height: 80%;
            object-fit: cover;
        }

        @media (max-width: 600px) {
            .circle {
                width: 70px;
                height: 80px;
            }
        }
    </style>
</head>

<body>
    <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->

    <section class="auth-section" style="background-color: white!important;">
        <div class="height-full">
            <div class="d-sm-none col-print-6">
                <div class="topbar">
                    <div class="circle">
                        <img src="<?php echo $logo;?>" alt="<?php echo $company_name;?>" class="img-responsive" />
                    </div>
                </div>
                
            </div>
            <!-- <div class="flex-center"> -->
            <div class="col-print-6">
                <div class="h-full w-full flex-center">
                    <div class="col-sm-10 col-lg-8">
                        <?php
                        $login_error = $this->session->userdata('login_error');
                        $this->session->unset_userdata('login_error');

                        if (!empty($login_error)) {
                            echo '<div class="alert alert-danger">' . $login_error . '</div>';
                        }
                        ?>
                        <form class="login-form" action="<?php echo site_url() . $this->uri->uri_string(); ?>" method="post">
                        <div class="flex-center">
                            <img src="<?php echo base_url().'assets/img/hr.png'?>" alt="" width="200" height="200">
                            </div>
                            <div class="flex-center"><p class="login-title text-center">Welcome to <?php echo ucwords(strtolower($company_name))?> </p></div>
                            

                            <p class="login-message">Please enter your username and password. </p>
                            <label>
                                <input required="" name="personnel_username" placeholder="" type="text" class="login-input" value="<?php echo $personnel_username; ?>">
                                <span>Username</span>
                            </label>

                            <label>
                                <input required="" name="personnel_password" placeholder="" type="password" class="login-input" value="<?php echo $personnel_password; ?>" autocomplete="off">
                                <span>Password</span>
                            </label>
                            <button class="submit">Submit</button>
                            <p class="login-signin">Don't have an acount ? <a href="#">Signup</a> </p>
                        </form>
                        <!-- <form class="">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Check me out</label>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form> -->
                    </div>
                </div>
            <!-- </div> -->
            </div>
        </div>
    </section>

    <!-- <section class="body-sign">
                <div class="auth__side-card">
                    
                </div>
            <div class="center-sign">
                <a href="<?php echo site_url() . 'login'; ?>" class="logo pull-left">
                    <img src="<?php echo base_url() . 'assets/logo/' . $logo; ?>" height="35" alt="<?php echo $company_name; ?>" class="img-responsive" />
                </a>

                <div class="panel panel-sign">
                    <div class="panel-title-sign mt-xl text-right">
                        <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-personnel mr-xs"></i><?php echo $company_name; ?></h2>
                    </div>
                    <div class="panel-body">
                        <?php
                        $login_error = $this->session->userdata('login_error');
                        $this->session->unset_userdata('login_error');

                        if (!empty($login_error)) {
                            echo '<div class="alert alert-danger">' . $login_error . '</div>';
                        }
                        ?>
                            <form action="<?php echo site_url() . $this->uri->uri_string(); ?>" method="post">
                            <?php
                                    //case of an input error
                                    if (!empty($personnel_username_error)) {
                                    ?>
                                    <div class="form-group mb-lg has-error">
                                        <label>Username</label>
                                        <div class="input-group input-group-icon">
                                            <input name="personnel_username" type="text" class="form-control input-lg" value="<?php echo $personnel_username; ?>" />
                                            <label for="personnel_username" class="error"><?php echo $personnel_username_error; ?></label>
                                            <span class="input-group-addon">
                                                <span class="icon icon-lg">
                                                    <i class="fa fa-envelope"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                    <?php
                                    } else {
                                    ?>
                                    <div class="form-group mb-lg">

                                        <label>Username</label>
                                        <div class="input-group input-group-icon">
                                            <input name="personnel_username" type="text" class="form-control input-lg" placeholder="ENTER HERE YOUR USERNAME"value="<?php echo $personnel_username; ?>" autocomplete="off"/>
                                            <span class="input-group-addon">
                                                <span class="icon icon-lg">
                                                    <i class="fa fa-envelope"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                            
                            <?php
                                        //case of an input error
                                        if (!empty($personnel_password_error)) {
                                        ?>
                                    <div class="form-group mb-lg has-error">
                                        <div class="clearfix">
                                            <label class="pull-left">Password</label>
                                            <a href="#" class="pull-right"> Lost Password?</a>
                                        </div>
                                        <div class="input-group input-group-icon">
                                            <input name="personnel_password" type="password" class="form-control input-lg" value="<?php echo $personnel_password; ?>" autocomplete="off" />
                                            <label for="personnel_username" class="error"><?php echo $personnel_username_error; ?></label>
                                            <span class="input-group-addon">
                                                <span class="icon icon-lg">
                                                    <i class="fa fa-lock"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                    <?php
                                        } else {
                                    ?>
                                    <div class="form-group mb-lg">
                                        <div class="clearfix">
                                            <label class="pull-left">Password</label>
                                        </div>
                                        <div class="input-group input-group-icon">
                                            <input name="personnel_password" type="password" class="form-control input-lg" placeholder="ENTER HERE YOUR PASSWORD"value="<?php echo $personnel_password; ?>" />
                                            <span class="input-group-addon">
                                                <span class="icon icon-lg">
                                                    <i class="fa fa-lock"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                    <?php
                                        }
                                    ?>
                            

                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="checkbox-custom checkbox-default">
                                        <input id="RememberMe" name="rememberme" type="checkbox"/>
                                        <label for="RememberMe">Remember Me</label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary submit-btn btn-block">SIGN IN</button>
                            </div>

                        </form>
                    </div>
                </div>

                <p class="text-center text-muted mt-md mb-md">&copy; Copyright <?php echo date('Y'); ?>. All Rights Reserved.</p>
            </div>
        </section> -->
    <!-- end: page -->

    <!-- Vendor -->
    <script src="<?php echo base_url() . "assets/themes/porto-admin/1.4.1/"; ?>assets/vendor/jquery/jquery.js"></script>
    <script src="<?php echo base_url() . "assets/themes/porto-admin/1.4.1/"; ?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
    <script src="<?php echo base_url() . "assets/themes/porto-admin/1.4.1/"; ?>assets/vendor/jquery-cookie/jquery.cookie.js"></script>
    <script src="<?php echo base_url() . "assets/themes/porto-admin/1.4.1/"; ?>assets/vendor/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url() . "assets/themes/porto-admin/1.4.1/"; ?>assets/vendor/nanoscroller/nanoscroller.js"></script>
    <script src="<?php echo base_url() . "assets/themes/porto-admin/1.4.1/"; ?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() . "assets/themes/porto-admin/1.4.1/"; ?>assets/vendor/magnific-popup/magnific-popup.js"></script>
    <script src="<?php echo base_url() . "assets/themes/porto-admin/1.4.1/"; ?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="<?php echo base_url() . "assets/themes/porto-admin/1.4.1/"; ?>assets/javascripts/theme.js"></script>

    <!-- Theme Custom -->
    <script src="<?php echo base_url() . "assets/themes/porto-admin/1.4.1/"; ?>assets/javascripts/theme.custom.js"></script>

    <!-- Theme Initialization Files -->
    <script src="<?php echo base_url() . "assets/themes/porto-admin/1.4.1/"; ?>assets/javascripts/theme.init.js"></script>
</body>

</html>
