<?php

class Company_invoices_model extends CI_Model 
{
	/*
	*	Retrieve all orders
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_orders($table, $where, $per_page, $page)
	{
		//retrieve all orders
		$this->db->from($table);
		$this->db->select('invoices.*,order_status.order_status_name,company.company_id,company.company_name');
		$this->db->where($where);
		$this->db->order_by('invoices.invoice_id','DESC');
		$this->db->join('company', 'company.company_id = invoices.company_id','left');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	/*
	*	Retrieve all orders
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_order_status()
	{
		//retrieve all orders
		$this->db->from('order_status');
		$this->db->select('*');
		$this->db->order_by('order_status_name');
		$query = $this->db->get();
		
		return $query;
	}
	/*
	*	Retrieve all suppliers
	*
	*/
	public function all_companies()
	{
		$this->db->where('company_id > 0');
		$this->db->select('*');
		$this->db->order_by('company_name');
		$query = $this->db->get('company');
		
		return $query;
	}

	/*
	*	Add a new order
	*
	*/
	public function add_invoice()
	{
		$order_number = $this->create_order_number();
		
		$data = array(
				'invoice_number'=>$order_number,
				'created_by'=>$this->input->post('personnel_id'),
				'invoice_status_id'=>1,
				'invoice_instructions'=>$this->input->post('order_instructions'),
				'created'=>date('Y-m-d H:i:s'),
				'modified_by'=>$this->session->userdata('personnel_id'),
				'company_id'=>$this->input->post('company_id'),
				'invoices_date'=>$this->input->post('invoice_date'),
			);
			
		if($this->db->insert('invoices', $data))
		{
			$invoice_id = $this->db->insert_id();
			// $insert_data = array(
			// 		'invoice_id'=>$invoice_id,
			// 		'invoice_level_status_status'=>0,
			// 		'created'=>date("Y-m-d H:i:s"),
			// 		'created_by' => $this->session->userdata('personnel_id'),
			// 		'modified_by' =>$this->session->userdata('personnel_id')
			// 	);

			// $this->db->insert('order_level_status', $insert_data);
			return $invoice_id;
		}
		else{
			return FALSE;
		}
	}

	public function create_order_number()
	{
		//select product code
		$this->db->from('invoices');
		$this->db->where("invoice_number LIKE 'INV".date('y')."-%'");
		$this->db->select('MAX(invoice_number) AS number');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$number++;//go to the next number
			
			if($number == 1){
				$number = "INV".date('y')."-001";
			}
		}
		else{//start generating receipt numbers
			$number = "INV".date('y')."-001";
		}
		
		return $number;
	}

	public function get_next_approval_status_name($status)
	{
		$this->db->select('inventory_level_status_name');
		$this->db->where('inventory_level_status_id = '.$status);
		$query = $this->db->get('inventory_level_status');
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$inventory_level_status_name = $key->inventory_level_status_name;
			}
		}
		else
		{
			$inventory_level_status_name = 0;
		}
		return $inventory_level_status_name;	
	}
	public function check_if_can_access($order_approval_status,$order_id)
	{
		if($order_approval_status == 0)
		{
			$addition =' AND personnel_approval.approval_status_id = 1';
		}
		else
		{
			$addition = 'AND order_level_status.order_level_status_status = 1 AND personnel_approval.approval_status_id <= '.($order_approval_status+1);
		}
		$this->db->select('*');
		$this->db->where('order_level_status.order_id = '.$order_id.' '.$addition.'  AND personnel_approval.personnel_id = '.$this->session->userdata('personnel_id').'');
		$this->db->order_by('order_level_status.order_level_status_id','DESC');
		$this->db->limit(1);
		$query = $this->db->get('personnel_approval,order_level_status');
		
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

	}


	/*
	*	Add a order product
	*
	*/
	public function add_invoice_item($invoice_id)
	{
		$product_id = $this->input->post('product_id');
		$quantity = $this->input->post('quantity');

		$branch_id = $this->input->post('branch_id');
		// var_dump($branch_id); die();

		$this->db->where('service_charge_id = '.$product_id);
		$query_charge = $this->db->get('service_charge');
		$service_charge_amount = 0;
		if($query_charge->num_rows() > 0)
		{
			foreach ($query_charge->result() as $key => $value) {
				# code...
				$service_charge_amount = $value->service_charge_amount;
			}
		}


		//Check if item exists
		// $this->db->select('*');
		// $this->db->where('service_charge_id = '.$product_id.' AND invoice_id = '.$invoice_id);
		// $query = $this->db->get('invoice_item');
		
		// if($query->num_rows() > 0)
		// {
		// 	$result = $query->row();
		// 	$qty = $result->purchase_quantity;
			
		// 	$quantity += $qty;
			
		// 	$data = array(
		// 			'invoice_item_quantity'=>$quantity,
		// 			'branch_id'=>$branch_id,
		// 			'invoice_item_price'=>$service_charge_amount
		// 		);
		// 	$this->db->where('service_charge_id = '.$product_id.' AND invoice_id = '.$invoice_id);
		// 	if($this->db->update('invoice_item', $data))
		// 	{
		// 		return TRUE;
		// 	}
		// 	else{
		// 		return FALSE;
		// 	}
		// }
		
		// else
		// {
			$data = array(
					'invoice_id'=>$invoice_id,
					'service_charge_id'=>$product_id,
					'invoice_item_quantity'=>$quantity,
					'branch_id'=>$branch_id,
					'invoice_item_price'=>$service_charge_amount
				);
				
			if($this->db->insert('invoice_item', $data))
			{
				return TRUE;
			}
			else{
				return FALSE;
			}
		// }
	}



	public function update_invoice_item($invoice_id,$invoice_item_id)
	{
		$data = array(
					'invoice_item_quantity'=>$this->input->post('quantity'),
					'invoice_item_price'=>$this->input->post('invoice_item_price'),
					// 'branch_id'=>$this->input->post('branch_id')
				);
				
		$this->db->where('invoice_item_id = '.$invoice_item_id);
		if($this->db->update('invoice_item', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_invoice_item_price($invoice_id,$invoice_item_id)
	{
		$data = array(
					'supplier_unit_price'=>$this->input->post('unit_price')
				);
				
		$this->db->where('invoice_item_id = '.$invoice_item_id);
		if($this->db->update('invoice_item', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function get_invoice_details($invoice_id)
	{
		$this->db->where('company.company_id = invoices.company_id AND invoices.invoice_id = '.$invoice_id);
		$this->db->order_by('invoices.created', 'DESC');
		$query = $this->db->get('invoices,company');
		
		return $query;
	}

	public function all_invoice_charges($store_id=null)
	{
		// var_dump($store_id); die();
		
		$this->db->where('service_charge_status = 1');
		$query = $this->db->get('service_charge');
		
		return $query;
	}


	public function all_company_branches($company_id)
	{
		// var_dump($store_id); die();
		
		$this->db->where('branch_status = 1 AND company_id = '.$company_id);
		$query = $this->db->get('branch');
		
		return $query;
	}
	/*
	*	Retrieve all order items of an order
	*
	*/
	public function get_invoice_items($invoice_id)
	{
		$this->db->select('service_charge.service_charge_name,service_charge.service_charge_amount, invoice_item.*,branch.branch_name');
		$this->db->where('service_charge.service_charge_id = invoice_item.service_charge_id AND invoice_item.invoice_id = '.$invoice_id);
		$this->db->join('branch', 'branch.branch_id = invoice_item.branch_id','left');
		$query = $this->db->get('invoice_item, service_charge');
		
		return $query;
	}

	public function get_invoice_detail_summary($where, $table)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		// $this->db->order_by('creditor_name', 'ASC');
		$query = $this->db->get('');
		
		return $query;
	}

	public function update_invoice_status($invoice_id,$invoice_status)
	{
		$data = array(
					'invoice_approval_status'=>$invoice_status
				);
				
		$this->db->where('invoice_id = '.$invoice_id);
		if($this->db->update('invoices', $data))
		{

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function get_invoice_approval_status($invoice_id)
	{
		$this->db->select('invoice_approval_status');
		$this->db->where('invoice_id = '.$invoice_id);
		$query = $this->db->get('invoices');
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$invoice_approval_status = $key->invoice_approval_status;
			}
		}
		else
		{
			$invoice_approval_status = 0;
		}
		return $invoice_approval_status;
	}
	public function get_invoice_amount($invoice_id,$is_vatable)
	{
		$this->db->select('SUM(invoice_item_quantity*invoice_item_price) AS total_amount');
		$this->db->where('invoice_id = '.$invoice_id);
		$query = $this->db->get('invoice_item');
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$total_amount = $key->total_amount;
			}
		}
		else
		{
			$total_amount = 0;
		}

		if($is_vatable)
		{
			$total_amount = $total_amount + $total_amount*0.16;
		}
		return $total_amount;
	}
}