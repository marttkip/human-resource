<?php
// var_dump($order_number); die();

if($is_vatable)
{
	$vatable = 'checked';
	$not_vatable = '';
}
else
{
	$not_vatable = 'checked';
	$vatable = '';
}

$result_order = '<table class="table  table-bordered table-condensed">
					  <thead>
						<tr>	
						  <th >#</th>
						  <th >PARTICULARS</th>
						  <th >QTY</th>
						  <th >UNIT PRICE</th>
						  <th > TOTAL PRICE</th>
						</tr>
					  </thead>
					  <tbody>';
if($invoice_item_query->num_rows() > 0)
{
	$col = '';
	$message = '';

	$count = 0;
	$invoice_total = 0;
	// var_dump($invoice_item_query->result()); die();

	
	$subtotal = 0;
	foreach($invoice_item_query->result() as $res)
	{
		$invoice_id = $res->invoice_id;
		$service_charge_name = $res->service_charge_name;
		$service_charge_amount = $res->service_charge_amount;
		$invoice_item_quantity = $res->invoice_item_quantity;
		$invoice_item_price = $res->invoice_item_price;
		$in_stock = $res->in_stock;
		$invoice_item_id = $res->invoice_item_id;
		$supplier_unit_price = $res->supplier_unit_price;
		$branch_name = $res->branch_name;
		
		$service_charge_total = $invoice_item_price * $invoice_item_quantity;
		$subtotal += $service_charge_total;
        $count++;

        $result_order .= '<tr>
				 				<td>'.$count.'</td>
				 				<td>'.$branch_name.' '.$service_charge_name.'</td>		 
				 				<td>	    
				                    '.$invoice_item_quantity.'
				                </td>
				                 <td>	    
				                   '.number_format($invoice_item_price).'
				                </td>
				 				<td>'.number_format($service_charge_total).'</td>
				 			</tr>';


			
	}

	$result_order .= '<tr>
				 				<td colspan="3"></td>
				 				<td><strong>SUBTOTAL KES.</strong></td>
				 				<td>'.number_format($subtotal,2).'</td>
				 			</tr>';
	if($is_vatable)
	{
		if($management_fee)
		{
			$total_mgt = $subtotal*($percentage/100);
			$mgt_percentage = $total_mgt*0.16;
			$result_order .= '<tr>
				 				<td colspan="3"></td>
				 				<td><strong>Management Fee '.$percentage.'%  KES.</strong></td>
				 				<td>'.number_format($total_mgt,2).'</td>
				 			</tr>';
			$result_order .= '<tr>
				 				<td colspan="3"></td>
				 				<td><strong>ADD 16% VAT KES.</strong></td>
				 				<td>'.number_format($mgt_percentage,2).'</td>
				 			</tr>';
			$result_order .= '<tr>
					 				<td colspan="3"></td>
					 				<td><strong>TOTAL KES.</strong></td>
					 				<td>'.number_format($subtotal +$total_mgt + $mgt_percentage,2).'</td>
					 			</tr>';
		}
		else
		{
			$result_order .= '<tr>
				 				<td colspan="3"></td>
				 				<td><strong>ADD 16% VAT KES.</strong></td>
				 				<td>'.number_format($subtotal*0.16,2).'</td>
				 			</tr>';
			$result_order .= '<tr>
					 				<td colspan="3"></td>
					 				<td><strong>TOTAL KES.</strong></td>
					 				<td>'.number_format($subtotal +$subtotal*0.16,2).'</td>
					 			</tr>';
		}
	}
	else
	{
		$result_order .= '<tr>
				 				<td colspan="3"></td>
				 				<td><strong>ADD 16% VAT KES.</strong></td>
				 				<td>'.number_format(0,2).'</td>
				 			</tr>';
		$result_order .= '<tr>
				 				<td colspan="3"></td>
				 				<td><strong>TOTAL KES.</strong></td>
				 				<td>'.number_format($subtotal,2).'</td>
				 			</tr>';
	}

	$result_order .='<tbody>
			 </table>'; 

}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Order Details</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:12px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 3px;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 10px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.row
			{
				margin-right: 0px !important;
				margin-left: 0px !important;
			}
		</style>
    </head>
    <body class="receipt_spacing">
    	<!-- <div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div> -->
        
      <div class="row receipt_bottom_border" style="padding-top: 200px;">
        	<div class="col-xs-12">
        		<div class="col-xs-8">
        			<h5><strong>INVOICE TO </strong> : <?php echo strtoupper($company_name);?></h5> 
        			<h5><strong>INVOICE DESCRIPTION </strong> </h5>
        			<h5><?php echo strtoupper($invoice_description);?></h5> 
        		</div>
        		<div class="col-xs-4">
        			<h5><strong>INVOICE NUMBER </strong> : <?php echo strtoupper($invoice_number);?></h5> 
        	
        			<h5><strong>INVOICE DATE </strong> : <?php echo date('jS M Y',strtotime($invoices_date));?></h5> 
        			
        		</div>
            </div>
					
        </div>
        
    	<div class="row">
        	<div class="col-md-12" style="padding: 20px;">
            	<?php echo $result_order;?>
            </div>

        </div>


        <div class="row">
		    	<div class="col-xs-12" style="margin-bottom: 30px; margin-top: 10px;">
            <h5><strong>Being Payment to contract for services at <?php echo strtoupper($company_name);?></strong></h5>
		       </div> 
		    </div>
      
		</div>
    </body>
    
</html>