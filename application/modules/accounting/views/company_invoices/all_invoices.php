<?php 
	$v_data['order_status_query'] = $order_status_query; ?>
<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
         <h2 class="panel-title pull-left"><?php echo $title;?></h2>
         <div class="widget-icons pull-right">
            	<!-- <a href="<?php echo base_url();?>inventory/add-order" class="btn btn-success btn-sm">Add Customer Invoice </a> -->
            	<a class="btn btn-success btn-sm" data-toggle='modal' data-target='#add_provider_items'>Add Customer Invoice</a>
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
			<?php echo $this->load->view('views/low_level', '' , TRUE); ?>
    </div>
</section>

<div class="modal fade bs-example-modal-lg" id="add_provider_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Invoice</h4>
            </div>
            <?php echo form_open("add-client-invoice", array("class" => "form-horizontal", "role" => "form"));?>
                <div class="modal-body">
                    <div class="row">
                      
                         <div class="col-md-12" style="margin-top: 20px;">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Company</label>
                                <div class="col-lg-10">
                                    <select class="form-control" name="company_id" id="company_id">
                                        <option>SELECT A COMPANY</option>
                                        <?php
			                    		if($suppliers_query->num_rows() > 0)
			                    		{
			                    			foreach ($suppliers_query->result() as $key_supplier_items ) {
			                    				# code...
			                    				$company_id = $key_supplier_items->company_id;
			                    				$company_name = $key_supplier_items->company_name;

			                    				echo '<option value="'.$company_id.'">'.$company_name.'</option>';
			                    			}
			                    		}
			                    		?>
                                    </select>
                                </div>
                            </div>
                        </div> 

                        <div class="col-md-12" style="margin-top: 20px;">

                            <div class="form-group">
                                <label class="col-md-2 control-label">Invoice date: </label>
                                
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="invoice_date" placeholder="Invoice date" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Invoice Instructions</label>
                                <div class="col-lg-10">
                                    <textarea class="form-control" name="order_instructions">
                                        <?php echo set_value('order_instructions');?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class='btn btn-info btn-sm' type='submit'>Add Invoice</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
                <?php echo form_close();?>
        </div>
    </div>
</div>

