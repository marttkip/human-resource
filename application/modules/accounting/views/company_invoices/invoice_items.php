<?php
$order_approval_status = $this->company_invoices_model->get_invoice_approval_status($invoice_id);

$check_level_approval = 1;//$this->company_invoices_model->check_assigned_next_approval($order_approval_status);
// $order_approval_status = $order_approval_status - 1;

// if($order_approval_status == 0 AND $check_level_approval == TRUE)


// $is_vatable = $res->is_vatable;

if($is_vatable)
{
	$vatable = 'checked';
	$not_vatable = '';
}
else
{
	$not_vatable = 'checked';
	$vatable = '';
}


if($order_approval_status == 0)
{
?>
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title pull-left">Add invoice items for <?php echo $company_name;?> Invoice <?php echo $invoice_number;?></h2>
            <div class="widget-icons pull-right">
                <!-- <a class='btn btn-sm btn-info ' data-toggle='modal' data-target='#add_provider' ><i class="fa fa-plus"></i> Add Provider</a> -->
                <a href="<?php echo base_url();?>accounting/company-invoices" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left"></i> Back to Company Invoices</a>
            </div>
            <div class="clearfix"></div>
        </header>
        <div class="panel-body">
            <?php
				$success = $this->session->userdata('success_message');
				$error = $this->session->userdata('error_message');

				if(!empty($success))
				{
					echo '
						<div class="alert alert-success">'.$success.'</div>
					';

					$this->session->unset_userdata('success_message');
				}

				if(!empty($error))
				{
					echo '
						<div class="alert alert-danger">'.$error.'</div>
					';

					$this->session->unset_userdata('error_message');
				}

			?>

                <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
                    <div class="row">
                    	<div class="col-md-4">
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Branches</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="branch_id" id="branch_id">
                                        <option>SELECT A BRANCH</option>
                                        <?php
				                    		if($companies->num_rows() > 0)
				                    		{
				                    			foreach ($companies->result() as $key ) {
				                    				# code...
				                    				$branch_id = $key->branch_id;
				                    				$branch_name = $key->branch_name;

				                    				echo '<option value="'.$branch_id.'">'.$branch_name.'</option>';
				                    			}
				                    		}
				                    		?>

                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Charges</label>
                                <div class="col-lg-8">
                                    <select class="form-control custom-select" name="product_id" id="product_id">
                                        <option>SELECT A CHARGE</option>
                                        <?php
				                    		if($charges_query->num_rows() > 0)
				                    		{
				                    			foreach ($charges_query->result() as $key ) {
				                    				# code...
				                    				$service_charge_id = $key->service_charge_id;
				                    				$service_charge_name = $key->service_charge_name;

				                    				echo '<option value="'.$service_charge_id.'">'.$service_charge_name.'</option>';
				                    			}
				                    		}
				                    		?>

                                    </select>

                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-4">
					                <div class="form-group">
					                	<label class="col-lg-4 control-label">QTY in Stock</label>
					                    <div class="col-lg-8">
					                    	 <input type="number" class="form-control" name="in_stock" placeholder="Quantity">
					                    </div>
					                </div>
					           </div> -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Units</label>
                                <div class="col-lg-8">
                                    <input type="number" class="form-control" name="quantity" placeholder="Quantity">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="center-align">
                            <button class="btn btn-primary btn-sm" type="submit">Add Invoice Item</button>
                        </div>
                    </div>

                    <?php echo form_close();?>

        </div>
    </section>
    <?php
} 



?>
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title pull-left">Invoice Items for <?php echo $company_name;?> Order <?php echo $invoice_number;?></h2>
                        <div class="widget-icons pull-right">
                            <a href="<?php echo base_url();?>accounting/print-invoice/<?php echo $invoice_id;?>" target="_blank" class="btn btn-default btn-sm"><i class="fa fa-print"></i> Print Invoice</a>
                             <a href="<?php echo base_url();?>accounting/company-invoices"  class="btn btn-primary btn-sm"><i class="fa fa-arrow-left"></i> Back to Invoices</a>
                        </div>
                        <div class="clearfix"></div>
                    </header>
                    <div class="panel-body">
                        <?php
    		$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');
			$search_result ='';
			$search_result2  ='';
			if(!empty($error))
			{
				echo $search_result2 = '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}

			if(!empty($success))
			{
				echo $search_result2 ='<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}

    	?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="center-align">
                                        <?php
					$order_approval_status = $this->company_invoices_model->get_invoice_approval_status($invoice_id);
					$rank = 2;
					$next_order_status = $order_approval_status+1;

					// check if assgned the next level 
					$check_level_approval = 1;//$this->company_invoices_model->check_assigned_next_approval($order_approval_status);

					if($order_approval_status == 0)
					{
						?>
                                            <a class="btn btn-success btn-sm" href="<?php echo base_url();?>accounting/send-for-approval/<?php echo $invoice_id;?>/2" onclick="return confirm('Do you want to send invoice for next approval?');">Send Invoice for approval</a>
                                            <?php
					}

					else if($order_approval_status == 1 AND $check_level_approval == TRUE )
					{
						?>
                                               <!--  <a class="btn btn-warning btn-sm" href="<?php echo base_url();?>inventory/send-for-correction/<?php echo $invoice_id;?>" onclick="return confirm('Do you want to send order for review / correction?');">Send order for correction</a> -->
                                                <a class="btn btn-success btn-sm" href="<?php echo base_url();?>accounting/send-for-approval/<?php echo $invoice_id;?>/<?php echo $next_order_status;?>" onclick="return confirm('Do you want to send invoice for next approval?');">Send Invoice for approval</a>
                                                <?php
					}
					else if($order_approval_status == 2 AND $check_level_approval == TRUE )
					{
						?>
                                                    <a class="btn btn-warning btn-sm" href="<?php echo base_url();?>accounting/send-for-correction/<?php echo $invoice_id;?>/1" onclick="return confirm('Do you want to send invoice for review / correction?');">Send invoice for correction</a>
                                                    <a class="btn btn-success btn-sm" href="<?php echo base_url();?>accounting/send-for-approval/<?php echo $invoice_id;?>/7" onclick="return confirm('Do you want to approve the invoice ?');">Approve Invoice</a>
                                                    <?php
					}

					else if($order_approval_status == 7 AND $check_level_approval == TRUE )
					{
						 // echo '<div class="alert alert-info">The invoice has been approved and thus cannot be edited</div>';
					}

					else
					{
						// echo '<div class="alert alert-info">Your Order is waiting for the next approval</div>';
					}

					?>

                                    </div>
                                </div>
                            </div>
                            <br>
                            <?php
    		$result ='';
    		// var_dump($invoice_item_query->num_rows()); die();
			if($invoice_item_query->num_rows() > 0)
			{
				$col = '';
				$message = '';

				
				$result .= 
				'
				<div class="row">
					<div class="col-md-12">
						<table class="table table-condensed table-stripped  table-bordered " >
						  <thead>
							<tr>
							  <th class="table-sortable:default table-sortable" title="Click to sort">#</th>
							  <th class="table-sortable:default table-sortable" title="Click to sort">Particular Name</th>
							  <th class="table-sortable:default table-sortable" title="Click to sort">QTY</th>
							  <th class="table-sortable:default table-sortable" title="Click to sort">RATE</th>
							  <th class="table-sortable:default table-sortable" title="Click to sort">TOTAL KES</th>
							</tr>
						  </thead>
						  <tbody>
						';
				
						$count = 0;
						$invoice_total = 0;
						// var_dump($invoice_item_query->num_rows()); die();
						
						$subtotal = 0;
						foreach($invoice_item_query->result() as $res)
						{
							$invoice_id = $res->invoice_id;
							$service_charge_name = $res->service_charge_name;
							$service_charge_amount = $res->service_charge_amount;
							$invoice_item_quantity = $res->invoice_item_quantity;
							$invoice_item_price = $res->invoice_item_price;
							$branch_name = $res->branch_name;
							$invoice_item_id = $res->invoice_item_id;
							$supplier_unit_price = $res->supplier_unit_price;
							
							
							$service_charge_total = $invoice_item_price * $invoice_item_quantity;
							$subtotal += $service_charge_total;
		                    $count++;
		                    // var_dump($order_approval_status); die();
								if(($order_approval_status == 0) OR $order_approval_status == 1 OR ($order_approval_status == 2))
								{
				                    $result .= ' '.form_open('accounting/update-invoice-item/'.$invoice_id.'/'.$invoice_number.'/'.$invoice_item_id).'
												<tr>
													<td>'.$count.'</td>
													<td>'.strtoupper($branch_name.' '.$service_charge_name).'</td>
													<td><input type="text" class="form-control" name="quantity" value="'.$invoice_item_quantity.'"></td>
													<td><input type="text" class="form-control" name="invoice_item_price" value="'.$invoice_item_price.'" ></td>
													<td>'.number_format($service_charge_total,2).'</td>
													<td><button class="btn btn-success btn-sm" type="submit" ><i class="fa fa-pencil"></i> </button></td>
													<td><a href="'.site_url().'delete-invoice-item/'.$invoice_item_id.'/'.$invoice_id.'/'.$invoice_number.'" onclick="return confirm("Do you want to delete '.$service_charge_name.'?")" title="Delete '.$service_charge_name.'" class="btn btn-danger btn-sm" > <i class="fa fa-trash"></i></a></td>
												</tr>
												'.form_close().'
												';
								}
								
								else if($order_approval_status == 7 AND $check_level_approval == TRUE)
								{
									$result .= '<tr>
											 				<td>'.$count.'</td>
											 				<td>'.$branch_name.' '.$service_charge_name.'</td>		 
											 				<td>	    
											                    '.$invoice_item_quantity.'
											                </td>
											                 <td>	    
											                   '.number_format($invoice_item_price).'
											                </td>
											 				<td>'.number_format($service_charge_total,2).'</td>
											 			</tr>';
								}
						}
						 $result .= '
												<tr>
													<td colspan="3"></td>
													<td><strong>SUBTOTAL KES.</strong></td>
													<td>'.number_format($subtotal,2).'</td>
												</tr>
												';
						if($management_fee == 1)
						{
							$result .= '	<tr>
													<td colspan="3"></td>
													<td><strong>Add Management Fee '.$percentage.' % </strong>
													</td>
													<td>
														<span> '.number_format($subtotal * ($percentage/100),2).'</span>
								                    </td>
												</tr>
											<tr>
													<td colspan="3"></td>
													<td><strong>ADD 16% VAT?</strong>
														 <input id="optionsRadios2" type="radio" name="appointment_id" value="0"  onclick="display_vat(0,'.$invoice_id.')" '.$not_vatable.'> No
								                         <input id="optionsRadios2" type="radio" name="appointment_id" value="1" onclick="display_vat(1,'.$invoice_id.')" '.$vatable.'>
								                        Yes
													</td>
													<td>
														<span id="vat_tab" style="display:none;"> '.number_format(($subtotal * ($percentage/100))*0.16,2).'</span>
														<span id="vat_tab_no" style="display:block;"> '.number_format(0,2).'</span>
								                    </td>
												</tr>
												';
												$result .= '
															<tr>
																<td colspan="3"></td>
																<td><strong>TOTAL</strong></td>
																<td>
																	<span id="total_vat_tab" style="display:none;"> '.number_format($subtotal + ($subtotal * ($percentage/100)) + ($subtotal * ($percentage/100))*0.16,2).'</span>
																	<span id="total_vat_tab_no" style="display:block;"> '.number_format($subtotal,2).'</span>
																</td>
															</tr>
															';

						}
						else
						{
							$result .= '
												<tr>
													<td colspan="3"></td>
													<td><strong>ADD 16% VAT?</strong>
														 <input id="optionsRadios2" type="radio" name="appointment_id" value="0"  onclick="display_vat(0,'.$invoice_id.')" '.$not_vatable.'> No
								                         <input id="optionsRadios2" type="radio" name="appointment_id" value="1" onclick="display_vat(1,'.$invoice_id.')" '.$vatable.'>
								                        Yes
													</td>
													<td>
														<span id="vat_tab" style="display:none;"> '.number_format($subtotal*0.16,2).'</span>
														<span id="vat_tab_no" style="display:block;"> '.number_format(0,2).'</span>
								                    </td>
												</tr>
												';
									$result .= '
												<tr>
													<td colspan="3"></td>
													<td><strong>TOTAL</strong></td>
													<td>
														<span id="total_vat_tab" style="display:none;"> '.number_format($subtotal + $subtotal*0.16,2).'</span>
														<span id="total_vat_tab_no" style="display:block;"> '.number_format($subtotal,2).'</span>
													</td>
												</tr>
												';
						}
						
						


						$result .= '
							</tbody>
						</table>
						';

						echo $result;
					}
				?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="center-align">
                                                
			            	
                                            </div>
                                        </div>
                                    </div>
                    </div>
                   

                </section>

<script type="text/javascript">
    $(function() {
        $("#product_id").customselect();
        $("#supplier_id").customselect();
        $("#creditor_id").customselect();
        $("#order_product_id").customselect();

        var is_vatable = <?php echo $is_vatable;?>;

        

        var myTarget2 = document.getElementById("vat_tab");
		 var myTarget3 = document.getElementById("vat_tab_no");
		 var myTarget4 = document.getElementById("total_vat_tab");
		 var myTarget5 = document.getElementById("total_vat_tab_no");
		 

		 if(is_vatable == 0)
		 {
		 	myTarget2.style.display = 'none';
		 	myTarget3.style.display = 'block';
		 	myTarget4.style.display = 'none';
		 	myTarget5.style.display = 'block';


		 }
		 else
		 {
		 	myTarget2.style.display = 'block';
		 	myTarget3.style.display = 'none';
		 	myTarget4.style.display = 'block';
		 	myTarget5.style.display = 'none';
		 }

    });

    function get_visit_trail(visit_id) {

        var myTarget2 = document.getElementById("visit_trail" + visit_id);
        var button = document.getElementById("open_visit" + visit_id);
        var button2 = document.getElementById("close_visit" + visit_id);

        myTarget2.style.display = '';
        button.style.display = 'none';
        button2.style.display = '';
    }

    function close_visit_trail(visit_id) {

        var myTarget2 = document.getElementById("visit_trail" + visit_id);
        var button = document.getElementById("open_visit" + visit_id);
        var button2 = document.getElementById("close_visit" + visit_id);

        myTarget2.style.display = 'none';
        button.style.display = '';
        button2.style.display = 'none';
    }

    function display_vat(display_status,invoice_id)
    {
    	 var myTarget2 = document.getElementById("vat_tab");
    	 var myTarget3 = document.getElementById("vat_tab_no");
    	 var myTarget4 = document.getElementById("total_vat_tab");
    	 var myTarget5 = document.getElementById("total_vat_tab_no");
    	 

    	 if(display_status == 0)
    	 {
    	 	myTarget2.style.display = 'none';
    	 	myTarget3.style.display = 'block';
    	 	myTarget4.style.display = 'none';
    	 	myTarget5.style.display = 'block';


    	 }
    	 else
    	 {
    	 	myTarget2.style.display = 'block';
    	 	myTarget3.style.display = 'none';
    	 	myTarget4.style.display = 'block';
    	 	myTarget5.style.display = 'none';
    	 }


    	 var url = "<?php echo site_url();?>accounting/company_invoices/update_company_invoice/"+invoice_id+"/"+display_status;
	 	// alert(url);
		  $.ajax({
		  type:'POST',
		  url: url,
		  data:{invoice_id: invoice_id},
		  dataType: 'json',
		  success:function(data){
		  },
		  error: function(xhr, status, error) {
		  alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		  }
		  });
		  return false;
    }
</script>