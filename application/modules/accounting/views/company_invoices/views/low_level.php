<?php
	$error = $this->session->userdata('error_message');
	$success = $this->session->userdata('success_message');
	$search_result ='';
	$search_result2  ='';
	if(!empty($error))
	{
		$search_result2 = '<div class="alert alert-danger">'.$error.'</div>';
		$this->session->unset_userdata('error_message');
	}
	
	if(!empty($success))
	{
		$search_result2 ='<div class="alert alert-success">'.$success.'</div>';
		$this->session->unset_userdata('success_message');
	}
			
	$search = $this->session->userdata('orders_search');
	
	if(!empty($search))
	{
		$search_result = '<a href="'.site_url().'inventory/close-orders-search" class="btn btn-danger">Close Search</a>';
	}


	$result = '<div class="padd">';	
	$result .= ''.$search_result2.'';
	$result .= '
			';
	
	//if users exist display them
	if ($query->num_rows() > 0)
	{
		$count = $page;
		
		$result .= 
		'
		<div class="row">
			<div class="col-md-12">
				<table class="table table-hover table-bordered " >
				  <thead>
					<tr>
					  <th >#</th>
					  <th>Company</th>
					  <th>Invoice Date</th>
					  <th>Invoice</th>
					  <th>Invoice Amount</th>
					  <th>Created By</th>
					  <th>Invoice Status</th>
					  <th colspan="2">Actions</th>
					</tr>
				  </thead>
				  <tbody>
				';
		
					//get all administrators
					$personnel_query = $this->personnel_model->get_all_personnel();
					
					foreach ($query->result() as $row)
					{
						$invoice_id = $row->invoice_id;
						$invoice_number = $row->invoice_number;
						$invoice_status = $row->invoice_status_id;
						$invoice_instructions = $row->invoice_instructions;
						// $invoice_status_name = $row->invoice_status_name;
						$created_by = $row->created_by;
						$created = $row->created;
						$modified_by = $row->modified_by;
						$company_id = $row->company_id;
						$supplier_invoice_date = $row->supplier_invoice_date;
						$company_name = $row->company_name;
						$last_modified = $row->last_modified;
						$invoice_approval_status = $row->invoice_approval_status;
						$is_vatable = $row->is_vatable;
						$invoices_date = $row->invoices_date;

						// var_dump($invoice_approval_status); die();

						// $invoice_details = $this->invoices_model->get_invoice_items($invoice_id);
						$total_price = 0;
						$total_items = 0;
						//creators & editors
						
						if($personnel_query->num_rows() > 0)
						{
							$personnel_result = $personnel_query->result();
							
							foreach($personnel_result as $adm)
							{
								$personnel_id2 = $adm->personnel_id;
								
								if($created_by == $personnel_id2 ||  $modified_by == $personnel_id2 )
								{
									$created_by = $adm->personnel_fname;
									break;
								}
								
								else
								{
									$created_by = '-';
								}
							}
						}
						
						else
						{
							$created_by = '-';
						}

						
						$button = '';

						


						$approval_levels = $this->company_invoices_model->check_if_can_access($invoice_approval_status,$invoice_id);

						$personnel_id = $this->session->userdata('personnel_id');

						$is_hod = $this->reception_model->check_if_admin($personnel_id,30);
						$is_admin = $this->reception_model->check_if_admin($personnel_id,1);


						
						// if($approval_levels == TRUE OR $personnel_id == 0 )
						// {	

							$next_invoice_status = $invoice_approval_status+1;

							$status_name = $this->company_invoices_model->get_next_approval_status_name($next_invoice_status);
							$is_hod = $this->reception_model->check_if_admin($personnel_id,30);
							$is_admin = $this->reception_model->check_if_admin($personnel_id,1);

						
							//pending invoice
							if($invoice_approval_status == 0 )
							{
								$status = '<span class="label label-warning ">Draft Invoice</span>';
								$button = '<td><a href="'.site_url().'vendor/cancel-invoice/'.$invoice_number.'" class="btn btn-danger btn-sm pull-right" onclick="return confirm(\'Do you really want to cancel this invoice '.$invoice_number.'?\');">Cancel</a></td>';
								$button2 = '';
							}
							else if($invoice_approval_status == 1 OR $invoice_approval_status == 2 )
							{
								$status = '<span class="label label-info"> Waiting for approval </span>';
								$button = '';
								$button2 = '';
							}
							else if($invoice_approval_status == 7)
							{
								$status = '<span class="label label-success">Complete Invoice</span>';
								$button = '';
								$button2 = '';
							}
							$payment_status = '<span class="label label-warning ">Not Paid</span>';
							$invoice_amount = $this->company_invoices_model->get_invoice_amount($invoice_id,$is_vatable);

							// just to mark for the next two stages	
							$count++;
							$result .= 
							'
								<tr>
									<td>'.$count.'</td>
									<td>'.strtoupper($company_name).'</td>
									<td>'.date('jS M Y',strtotime($invoices_date)).'</td>
									<td>'.$invoice_number.'</td>
									<td>'.number_format($invoice_amount,2).'</td>
									<td>'.$created_by.'</td>
									<td>'.$status.'</td>
									<td><a href="'.site_url().'accounting/add-invoice-item/'.$invoice_id.'/'.$invoice_number.'" class="btn btn-warning  btn-sm "><i class="fa fa-plus"></i> Invoice Items</a></td>
									
									
								</tr> 
							';
						// }
					}
		
			$result .= 
			'
					  </tbody>
					</table>
				</div>
			</div>
			';
	}
	
	else
	{
		$result .= "There are no invoices";
	}
	$result .= '</div>';
	echo $result;
?>
