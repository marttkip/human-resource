<!-- search -->
<?php echo $this->load->view('search/company_search', '', TRUE);?>

<!-- end search -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?> </h2>
        <!-- <a href="<?php echo site_url();?>accounting/companys/add_company" class="btn btn-sm btn-primary pull-right" style="margin-top: -25px;"><i class="fa fa-plus"></i> Add companys</a> -->
                	
    </header>

    <!-- Widget content -->
    <div class="panel-body">
    	<div class="padd">
          <?php
            	$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
			?>
           
          <div style="min-height:30px;">
            	<div class="pull-right">
                	<?php
					$search = $this->session->userdata('search_companies');
		
					if(!empty($search))
					{
						echo '<a href="'.site_url().'accounting/receivables/close_search_hospital_receivables" class="btn btn-warning btn-sm">Close Search</a>';
					}
					?>
                </div>
            </div>
                
<?php
		
		$result = '';
		
				// var_dump($query->result()); die();
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= '
					<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>company name</th>
						  <th>Opening Balance</th>
						  <th>30 days</th>
						  <th>60 Days</th>
						  <th>90 Days</th>
						  <th>> 90 Days</th>
						  <th>Total payments</th>
						  <th>Total invoice</th>
						  <th>Account Balance</th>
						  <th colspan="3">Actions</th>
						</tr>
					  </thead>
					  <tbody>
				';
				$total_this_month = 0;
				$total_three_months = 0;
				$total_six_months = 0;
				$total_nine_months = 0;
				$total_payments = 0;
				$total_invoices =0;
				$total_balance = 0;
			
			foreach ($query->result() as $row)
			{
				$count++;
				$company_id = $row->company_id;
				$company_name = $row->company_name;
				$opening_balance = $row->opening_balance;
				$debit_id = $row->debit_id;

				// $invoice_total = $this->receivables_model->get_invoice_total($company_id);
				// $payments_total = $this->receivables_model->get_payments_total($company_id);
				//$payments_total = 0;
				$company_status = $row->company_status;
				
				if($company_status == 1)
				{
					$checked_active = 'checked';
					$checked_inactive = '';
				}
				else
				{
					$checked_active = '';
					$checked_inactive = 'checked';
				}
				// var_dump($invoice_total);
				// if($debit_id == 2)
				// {
				// 	$payments_total = $payments_total +$opening_balance;	
				// }
				// else
				// {
				// 	$invoice_total = $invoice_total + $opening_balance;
				// }

				$company_result = $this->receivables_model->get_receivables_statement($company_id);

				$invoice_total = $company_result['total_invoice_balance'];
				$payments_total =$company_result['total_payment_amount'];


				$date = date('Y-m-d');
	            $this_month = $this->receivables_model->get_statement_value($company_id,$date,1);
	            $three_months = $this->receivables_model->get_statement_value($company_id,$date,2);
	            $six_months = $this->receivables_model->get_statement_value($company_id,$date,3);
	            $nine_months = $this->receivables_model->get_statement_value($company_id,$date,4);

	            $total_this_month +=$this_month;
	            $total_three_months +=$three_months;
	            $total_six_months +=$six_months;
	            $total_nine_months +=$nine_months;
	            $total_payments += $payments_total;
	            $total_invoices += $invoice_total;

	            $total_balance += $invoice_total-$payments_total;


				$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td>'.$company_name.'</td>
							<td>'.number_format($opening_balance, 2).'</td>
							<td>'.number_format($this_month, 2).'</td>
							<td>'.number_format($three_months, 2).'</td>
							<td>'.number_format($six_months, 2).'</td>
							<td>'.number_format($nine_months, 2).'</td>
							<td>'.number_format($payments_total, 2).'</td>
							<td>'.number_format($invoice_total, 2).'</td>
							<td>'.number_format($invoice_total-$payments_total, 2).'</td>
							<td><a href="'.site_url().'company-statement/'.$company_id.'" class="btn btn-sm btn-info" >Account</a></td>
							<td><a href="'.base_url().'accounting/receivables/print_receivable_account/'.$company_id.'" class="btn btn-sm btn-warning"  target="_blank"><i class="fa fa-print"></i> Print</a></td>
							</tr>
							';
				
			}
			
			$result .= '<tr>
							<td colspan=3></td>
							<td>'.number_format($total_this_month, 2).'</td>
							<td>'.number_format($total_three_months, 2).'</td>
							<td>'.number_format($total_six_months, 2).'</td>
							<td>'.number_format($total_nine_months, 2).'</td>
							<td>'.number_format($total_payments, 2).'</td>
							<td>'.number_format($total_invoices, 2).'</td>
							<td>'.number_format($total_balance, 2).'</td>
						</tr>
						';
			$result .= 
				'
							  </tbody>
							</table>
				';
		}
		
		else
		{
			$result .= "There are no companys";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
        <!-- Widget ends -->

      </div>
</section>