<!-- search -->
<?php  echo  $this->load->view('search_receivable_account', '', TRUE);?>
<!-- end search -->

<div class="row">
    <div class="col-md-12">

        <section class="panel">
            <header class="panel-heading">
                
                <h2 class="panel-title"><?php echo $title;?></h2>
                <a href="<?php echo site_url();?>accounting/receivables" class="btn btn-sm btn-warning pull-right" style="margin-top: -25px; "><i class="fa fa-arrow-left"></i> Back to receivables</a>
                <a href="<?php echo base_url().'accounting/receivables/print_receivable_account/'.$company_id?>" class="btn btn-sm btn-success pull-right"  style="margin-top: -25px;margin-right: 5px;" target="_blank"><i class="fa fa-print"></i> Print</a>
                <button type="button" class="btn btn-sm btn-primary pull-right"  data-toggle="modal" data-target="#record_creditor_account" style="margin-top: -25px; margin-right: 5px;"><i class="fa fa-plus"></i> Record</button>

                    
            </header>
            
            <div class="panel-body">
                <div class="pull-right">
                    
                    <!--<a href="<?php echo base_url().'administration/sync_app_creditor_account';?>" class="btn btn-sm btn-info"><i class="fa fa-sign-out"></i> Sync</a>-->
                </div>
                <!-- Modal -->
                <div class="modal fade" id="record_creditor_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Record Payment</h4>
                            </div>
                            <div class="modal-body">
                                <?php echo form_open("accounting/receivables/record_company_account/".$company_id, array("class" => "form-horizontal"));?>
                                <input type="hidden" name="account_from_id" value="<?php echo $company_id;?>">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Payment date: </label>
                                    
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="company_account_date" placeholder="Transaction date" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"> Code *</label>
                                    
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="transaction_code" placeholder="Code e.g cheque number/MPESA code"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Description *</label>
                                    
                                    <div class="col-md-8">
                                        <textarea class="form-control" name="company_account_description"></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Amount *</label>
                                    
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="company_account_amount" placeholder="Amount"/>
                                    </div>
                                </div>

                                
                                
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-4">
                                        <div class="center-align">
                                            <button type="submit" class="btn btn-primary">Save record</button>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close();?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                
            <?php
            $error = $this->session->userdata('error_message');
            $success = $this->session->userdata('success_message');
            
            if(!empty($error))
            {
                echo '<div class="alert alert-danger">'.$error.'</div>';
                $this->session->unset_userdata('error_message');
            }
            
            if(!empty($success))
            {
                echo '<div class="alert alert-success">'.$success.'</div>';
                $this->session->unset_userdata('success_message');
            }
                    
            $search = $this->session->userdata('receivable_payment_search');
            $search_title = $this->session->userdata('receivable_search_title');
        
            if(!empty($search))
            {
                echo '
                <a href="'.site_url().'accounting/receivables/close_receivable_search/'.$company_id.'" class="btn btn-warning btn-sm ">Close Search</a>
                ';
                echo $search_title;
            }   
            // var_dump($company_id); die();
                $company_result = $this->receivables_model->get_receivables_statement($company_id);
            ?>

                <table class="table table-hover table-bordered ">
                    <thead>
                        <tr>
                          <th>Transaction Date</th>                       
                          <th>Invoice Number</th>
                          <th>Description</th>
                          <th>Debit</th>
                          <th>Credit</th>   
                          <th>Balance</th>                      
                        </tr>
                     </thead>
                    <tbody>
                        <?php  echo $company_result['result'];?>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>