<?php

class Company_model extends CI_Model 
{	
	/*
	*	Retrieve all customers
	*
	*/

	public function get_all_companies($table, $where, $per_page, $page, $order = 'customer_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function add_company()
	{
		$one = 1;
		
		$data = array(

				
				'company_name'=>$this->input->post('company_name'),
				'company_reg_no'=>$this->input->post('company_reg_no'),
				'company_phone'=>$this->input->post('company_phone'),
				'company_email'=>$this->input->post('company_email'),
				'company_post_code'=>$this->input->post('company_post_code'),
				'contact_person_phone'=>$this->input->post('contact_person_phone'),
				'contact_person_name'=>$this->input->post('contact_person_name'),
				'company_address'=>$this->input->post('company_address'),
				'company_created'=>date('Y-m-d H:i:s'),
				'company_status'=>$one,
				'opening_balance'=>$this->input->post('opening_balance'),
				'management_fee'=>$this->input->post('management_fee'),
				'percentage'=>$this->input->post('percentage'),
				'debit_id'=>$this->input->post('debit_id'),
				'created'=>date('Y-m-d')


				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('company', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function update_company($company_id) 
	{

		$data = array(
			
				'company_name'=>$this->input->post('company_name'),
				'company_reg_no'=>$this->input->post('company_reg_no'),
				'company_phone'=>$this->input->post('company_phone'),
				'company_email'=>$this->input->post('company_email'),
				'company_post_code'=>$this->input->post('company_post_code'),
				'contact_person_phone'=>$this->input->post('contact_person_phone'),
				'contact_person_name'=>$this->input->post('contact_person_name'),
				'company_address'=>$this->input->post('company_address'),
				'opening_balance'=>$this->input->post('opening_balance'),
				'management_fee'=>$this->input->post('management_fee'),
				'percentage'=>$this->input->post('percentage'),
				'debit_id'=>$this->input->post('debit_id'),
				'last_modified'=>date('Y-m-d H:i:s')
				
				
			);
			
		$this->db->where('company_id', $company_id);
		if($this->db->update('company', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_company($company_id)
	{
		//retrieve all users
		$this->db->from('company');
		$this->db->select('*');
		$this->db->where('company_id = '.$company_id);
		$query = $this->db->get();
		
		return $query;
	}

	public function activate_company($company_id)
	{
		$data = array(
				'company_status' => 1
			);
		$this->db->where('company_id', $company_id);
		
		if($this->db->update('company', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated company
	*	@param int $company_id
	*
	*/
	public function deactivate_company($company_id)
	{
		$data = array(
				'company_status' => 0
			);
		$this->db->where('company_id', $company_id);
		
		if($this->db->update('company', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}








}
?>