<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends MX_Controller 
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('company_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
    
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function index($order = 'company_name', $order_method = 'ASC') 
	{

		$where = 'company.company_id > 0 and company.is_deleted = 0';
		$table = 'company';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->company_model->get_all_companies($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Companies';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['branches'] = $this->branches_model->all_branches();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('company/company/all_companies', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}


	/*
	*
	*	Add a new customer
	*
	*/
	public function add_company() 
	{
		
		//form validation rules
				
		$this->form_validation->set_rules('company_name', 'Company Name', 'required|xss_clean');
		$this->form_validation->set_rules('company_reg_no', 'Company Reg No', 'xss_clean');
		$this->form_validation->set_rules('company_phone', 'Phone Number', 'xss_clean');
		$this->form_validation->set_rules('company_email', 'Email', 'xss_clean');
		$this->form_validation->set_rules('company_post_code', 'Postal Code', 'xss_clean');
		$this->form_validation->set_rules('company_address', 'Address', 'xss_clean');

		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->company_model->add_company())
			{
				$this->session->set_userdata('success_message', 'company added successfully');
				redirect('company');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Could not add company. Please try again');
			}
			
				
		}
		//open the add new companys
		$data['title'] = 'Add Company Details';
		$v_data['title'] = $data['title'];
		$v_data['branches'] = $this->branches_model->all_parent_branches();
		$data['content'] = $this->load->view('company/company/add_company', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

   /*
	*
	*	Edit an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function edit_company($company_id) 
	{
		$this->form_validation->set_rules('company_name', 'Company Name', 'required|xss_clean');
		$this->form_validation->set_rules('company_reg_no', 'Company Reg No', 'xss_clean');
		$this->form_validation->set_rules('company_phone', 'Phone Number', 'xss_clean');
		$this->form_validation->set_rules('company_email', 'Email', 'xss_clean');
		$this->form_validation->set_rules('company_post_code', 'Postal Code', 'xss_clean');
		$this->form_validation->set_rules('company_address', 'Address', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			if($this->company_model->update_company($company_id))
			{
				$this->session->set_userdata('success_message', 'Company updated successfully');
				redirect('company');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update Company. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Edit Company Details';
		$v_data['title'] = $data['title'];
		
		//select the vehicle from the database
		$query = $this->company_model->get_company($company_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['company'] = $query->result();
			$data['content'] = $this->load->view('company/company/edit_company', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'company does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Deactivate an existing Customer
	*	@param int $vehicle_id
	*
	*/
	public function deactivate_company($company_id)
	{
		if($this->company_model->deactivate_company($company_id))
		{
			$this->session->set_userdata('success_message', 'company disabled successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'company could not be disabled. Please try again');
		}
		redirect('company');
	}
	/*
	*
	*	Activate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function activate_company($company_id)
	{
		if($this->company_model->activate_company($company_id))
		{
			$this->session->set_userdata('success_message', 'company activated successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'company could not be activated. Please try again');
		}
		redirect('company');
	}



	

}
?>