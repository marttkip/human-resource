<?php
//personnel data
$company_name = set_value('company_name');
$company_reg_no = set_value('company_reg_no');
$company_phone = set_value('company_phone');
$company_email = set_value('company_email');
$company_post_code = set_value('company_post_code');
$company_address = set_value('company_address');
$contact_person_phone = set_value('contact_person_phone');
$contact_person_name = set_value('contact_person_name');
$opening_balance  = set_value('opening_balance');
$balance_brought_forward = set_value('balance_brought_forward');
$percentage = set_value('percentage');



?>   
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>company" class="btn btn-info pull-right">Back to company</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                        $success = $this->session->userdata('success_message');
                        $error = $this->session->userdata('error_message');
                        
                        if(!empty($success))
                        {
                            echo '
                                <div class="alert alert-success">'.$success.'</div>
                            ';
                            
                            $this->session->unset_userdata('success_message');
                        }
                        
                        if(!empty($error))
                        {
                            echo '
                                <div class="alert alert-danger">'.$error.'</div>
                            ';
                            
                            $this->session->unset_userdata('error_message');
                        }
                        $validation_errors = validation_errors();
                        
                        if(!empty($validation_errors))
                        {
                            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                        }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
       
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Company Name: </label>
            
             <div class="col-lg-7">
                <input type="text" class="form-control" name="company_name" placeholder="Company Name" value="<?php echo $company_name;?>">
            </div>
            
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Registration Number: </label>
            
          <div class="col-lg-7">
                <input type="text" class="form-control" name="company_reg_no" placeholder="Registration Number" value="<?php echo $company_reg_no;?>">
          </div> 
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">Phone Number: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="company_phone" placeholder="Phone Number" value="<?php echo $company_phone;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Opening Balance: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="opening_balance" placeholder="Opening Balance" value="<?php echo $opening_balance;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Prepayment ?</label>
            <div class="col-lg-3">
                <div class="radio">
                    <label>
                    <input id="optionsRadios5" type="radio" value="1" name="debit_id">
                    Yes
                    </label>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="radio">
                    <label>
                    <input id="optionsRadios6" type="radio" value="2" name="debit_id" checked="checked">
                    No
                    </label>
                </div>
            </div>
        </div>


        <div class="form-group">
            <label class="col-lg-5 control-label">Includes Management Fee ?</label>
            <div class="col-lg-3">
                <div class="radio">
                    <label>
                    <input id="optionsRadios5" type="radio" value="1" name="management_fee">
                    Yes
                    </label>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="radio">
                    <label>
                    <input id="optionsRadios6" type="radio" value="2" name="management_fee" checked="checked">
                    No
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">MGT Fee %: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="percentage" placeholder="percentage" value="<?php echo $percentage;?>">
            </div>
        </div>
       
        
        
    </div>
    
    <div class="col-md-6">
        
       
         <div class="form-group">
            <label class="col-lg-5 control-label">Contact Person Name: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="contact_person_name" placeholder="Contact Person" value="<?php echo $contact_person_name;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Email Address: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="company_email" placeholder="Email Address" value="<?php echo $company_email;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Postal Code: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="company_post_code" placeholder="Phone" value="<?php echo $company_post_code;?>">
            </div>
        </div>
        
       
        <div class="form-group">
            <label class="col-lg-5 control-label">Address: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="company_address" placeholder="Address" value="<?php echo $company_address;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Contact Person Phone Number: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="contact_person_phone" placeholder="Contact Person" value="<?php echo $contact_person_phone;?>">
            </div>
        </div>
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add Company
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>