<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Company name</th>
						<th>Phone Number</th>
						<th>Email</th>
						<th>Registration Date</th>
						<th>Opening Balance</th>
						<th>Status</th>
						<th colspan="3">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$company_id = $row->company_id;
				$company_name = $row->company_name;
				$company_reg_no = $row->company_reg_no;
				$company_phone = $row->company_phone;
				$company_email = $row->company_email;
				$company_status = $row->company_status;
				$company_created = $row->company_created;
				$company_status = $row->company_status;
				$opening_balance = $row->opening_balance;
				$debit_id = $row->debit_id;
				if(empty($opening_balance))
				{
					$opening_balance = 0;
				}

				if($debit_id == 1)
				{
					$opening = '('.number_format($opening_balance,2).')';
				}
				else
				{
					$opening = number_format($opening_balance,2);
				}
				
				//status
				if($company_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($company_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button_edit ='';
					$button = '<a class="btn btn-sm btn-info" href="'.site_url().'company/activate-company/'.$company_id.'" onclick="return confirm(\'Do you want to activate '.$company_name.'?\');" title="Activate '.$company_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
				}
				//create activated status display
				else if($company_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-sm btn-default" href="'.site_url().'company/deactivate-company/'.$company_id.'" onclick="return confirm(\'Do you want to deactivate '.$company_name.'?\');" title="Deactivate '.$company_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
					$button_contact_persons = '<a href="company/add_person/'.$company_id.'" class="btn btn-sm btn-info"> Contact Persons</a>';
			        $button_edit ='<a href="'.site_url().'company/edit_company/'.$company_id.'" class="btn btn-sm btn-success" > Edit</a>';
			        $button_delete ='<a href="'.site_url().'company/delete_company/'.$company_id.'" class="btn btn-sm btn-danger" > Delete</a>';
			
				
				}
				
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$company_name.'</td>
						<td>'.$company_phone.'</td>
						<td>'.$company_email.'</td>
						<td>'.date('jS M Y H:i a',strtotime($row->company_created)).'</td>
						<td>'.$opening.'</td>
						<td>'.$status.'</td>
						<td>'.$button_edit.'</td>
						<td>'.$button.'</td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no company";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('company_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/company/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">
                                   
                                    <div class="col-lg-2 col-lg-offset-10">
                                    	<a href="<?php echo site_url();?>company/add_company" class="btn btn-sm btn-info pull-right">Add Company</a>
                                    </div>
                                </div>

				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

