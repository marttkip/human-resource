<?php 
$start_date = date('Y-m-d');
?>
<div class="col-md-12">
	
     <div class="panel-body">
      	<div class="padd" style="height:80vh;overflow-y:scroll;">
      		
      		<div class="row" >
      			<?php echo form_open("reception/update_visit", array("class" => "form-horizontal", "id" => "add-event-planners"));?>
	      		<div class="col-md-12" >
	      			<div class="col-md-6" >
	      				<div class="form-group">
		                    <label class="col-md-4 control-label">Event Name: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="event_name" placeholder="Event Name" autocomplete="off" required>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label">Start Date: </label>
		                    
		                    <div class="col-md-8">
		                    	<div class="input-group">
			                        <span class="input-group-addon">
			                            <i class="fa fa-calendar"></i>
			                        </span>
		                        	<input type="text" class="form-control datepicker" name="start_date" id="event_start_date" id="" value="<?php echo $start_date?>" placeholder="Start Date" autocomplete="off" required>
		                        </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label">Days: </label>
		                    
		                    <div class="col-md-8">
		                    	
		                        <input type="text" class="form-control" name="days" id="event_days" value="" onkeyup="get_available_calendar()" placeholder="Days" autocomplete="off" required>
		                       
		                    </div>
		                </div>
		                <div id="days-schedule">
		                	
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label">End Date: </label>
		                    
		                    <div class="col-md-8">
		                    	<div class="input-group">
			                        <span class="input-group-addon">
			                            <i class="fa fa-calendar"></i>
			                        </span>
		                        	<input type="text" class="form-control datepicker" name="end_date" id="event_end_date" value="" placeholder="End Date" autocomplete="off" readonly>
		                        </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-lg-4 control-label">Event Type</label>
		                    <div class="col-lg-8">
		                        <div class="radio">
		                            <label>
		                                <input id="optionsRadios1" type="radio" checked value="1" name="event_type_id">
		                                UHDC Closed
		                            </label>
		                            <label>
		                                <input id="optionsRadios1" type="radio" value="2" name="event_type_id">
		                                Public Holiday
		                            </label>
		                            
		                        </div>
		                        
		                    </div>
		                    
		                </div>

	      			</div>
	      			<div class="col-md-1" >
	      			</div>
	      			<div class="col-md-5" >
	      				
	      				<div class="form-group">
		                    <label class="col-md-12">Event Decription: </label>
		                    
		                    <div class="col-md-12">
		                    	<textarea name="event_description" class="form-control cleditor" autocomplete="off"></textarea>
		                      
		                    </div>
		                </div>
	      			</div>

	            </div>
	            <br>
	            <div class="col-md-12" id="submit_button" style="display: none;">
	            	<div class="center-align">
						<input type="submit"  class="btn btn-info btn-sm" value="ADD EVENT"/>
					</div>
	            	
	            </div>
            <?php echo form_close();?>
	        </div>
        </div>
	</div>
</div>

<div class="row" style="margin-top: 10px;">
    <div class="col-md-12 center-align">
        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
    
        	
    </div>
</div>