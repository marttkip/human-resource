<?php
$planner_details = json_encode($event_list);

// var_dump($planner_details);die();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Schedule </title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css">	
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/";?>fontawesome/css/font-awesome.css">
		<!-- <script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script>  -->
		<!-- jQuery -->

		<!-- full calendae -->
		<!-- <link href='<?php echo base_url()."assets/fullcalendar/";?>fullcalendar.print.min.css' rel='stylesheet' media='print' />  -->
		<link href='<?php echo base_url()."assets/fullcalendar/";?>fullcalendar.min.css' rel='stylesheet'/>
		
		<script src='<?php echo base_url()."assets/fullcalendar/";?>lib/moment.min.js'></script>
		<script src='<?php echo base_url()."assets/fullcalendar/";?>lib/jquery.min.js'></script>

		
		<style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom: 5px;}
            .row .col-md-12 table {
                /*border:solid #000 !important;*/
                /*border-width:1px 0 0 1px !important;*/
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                /*border:solid #000 !important;*/
                /*border-width:0 1px 1px 0 !important;*/
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px;}
            .col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}
           
        </style>

        <style type="text/css">
			.currentmonth {
			    color: blue;
			    text-align: left;
			}
			.currentday {
			    border: 1px solid black;
			    color: #00FF00;
			    text-align: center;
			}

			table.calendar {
		/*	    margin:1em 1em 2em 1em;*/
			}
			table.calendar td, table.calendar th {
		/*	    padding:0.5em;*/
		/*		width: 14.285714285714286% !important;*/
		/*		width: 1.285714285714286% !important;*/
				width: 190.85px !important;
			}
			table.calendar {
			    display:inline-block;
			    *display:inline; zoom:1;
			    vertical-align:top;
			}
			table.calendar tr td .col-print-2
			{
		/*		width: 10% !important;*/
				padding: 0px !important;
				text-align: right !important;
				padding-right: 5px !important;
			}
			table.calendar tr td .col-print-10
			{
		/*		width: 90% !important;*/
				padding: 0px !important;
				text-align: left !important;
				padding-left: 5px !important;
			}
			.col-print-12 {
				width: 100%;
				float: left;
				position: relative;
				min-height: 1px;
				padding: 0px !important; 
			}
		</style>
		  
    </head>
    <body class="receipt_spacing" onLoad="">
    	
    	<input type="hidden" id="config_url" value="<?php echo site_url();?>">
    	<div class="col-md-12 left-align ">
			<div class="row">
	        	<div class="col-xs-12 " style="padding-bottom: 5px;">
	            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
	            </div>
	        </div>
	        
        </div>
   
        <div class="row receipt_bottom_border" >
        	<div class="col-md-12">
					<div id="calendar"></div>
            </div>
        	
        </div>


        <script type="text/javascript">

			
			


			
			(function () {

				let data_checked = <?php echo $planner_details?>;


		    function calendar(month) {
		    	// alert(data_checked);
		        //Variables to be used later.  Place holders right now.
		        var padding = "";
		    
		        var totalFeb = "";
		        var i = 1;
		        var testing = "";

		        var current = new Date();
		        var cmonth = current.getMonth();
		        var day = current.getDate();
		        var year = current.getFullYear();
		        var tempMonth = month + 1; //+1; //Used to match up the current month with the correct start date.
		        var prevMonth = month - 1;

		        //Determing if Feb has 28 or 29 days in it.  
		        if (month == 1) {
		            if ((year % 100 !== 0) && (year % 4 === 0) || (year % 400 === 0)) {
		                totalFeb = 29;
		            } else {
		                totalFeb = 28;
		            }
		        }

		        //////////////////////////////////////////
		        // Setting up arrays for the name of    //
		        // the	months, days, and the number of	//
		        // days in the month.                   //
		        //////////////////////////////////////////

		        var monthNames = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
		        var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thrusday", "Friday", "Saturday"];
		        var totalDays = ["31", "" + totalFeb + "", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"];

		        //////////////////////////////////////////
		        // Temp values to get the number of days//
		        // in current month, and previous month.//
		        // Also getting the day of the week.	//
		        //////////////////////////////////////////

		        var tempDate = new Date(tempMonth + ' 1 ,' + year);
		        var tempweekday = tempDate.getDay();
		        var tempweekday2 = tempweekday;
		        var dayAmount = totalDays[month];
		        // var preAmount = totalDays[prevMonth] - tempweekday + 1;	

		        //////////////////////////////////////////////////
		        // After getting the first day of the week for	//
		        // the month, padding the other days for that	//
		        // week with the previous months days.  IE, if	//
		        // the first day of the week is on a Thursday,	//
		        // then this fills in Sun - Wed with the last	//
		        // months dates, counting down from the last	//
		        // day on Wed, until Sunday.                    //
		        //////////////////////////////////////////////////

		        while (tempweekday > 0) {
		            padding += "<td class='premonth' style='border: 1px solid lightgrey;color: #000;text-align: center;width: 190.85px !important;'><div class='col-print-12' >&nbsp;</div></td>";
		            //preAmount++;
		            tempweekday--;
		        }
		        //////////////////////////////////////////////////
		        // Filling in the calendar with the current     //
		        // month days in the correct location along.    //
		        //////////////////////////////////////////////////
		       	
		        
		        	
		        while (i <= dayAmount) {


		        	var days_list = '&nbsp;';
		            //////////////////////////////////////////
		            // Determining when to start a new row	//
		            //////////////////////////////////////////

		            if (tempweekday2 > 6) {
		                tempweekday2 = 0;
		                padding += "</tr><tr >";
		            }


		        	// next
		        	let day_variable = i;
		        	if(day_variable<10)
		        	{
		        		day_variable = '0'+day_variable;
		        	}
		        	else
		        	{
		        		day_variable = day_variable;
		        	}


		        	let month_variable = month+1;
		        	if(month_variable<10)
		        	{
		        		month_variable = '0'+month_variable;
		        	}
		        	else
		        	{
		        		month_variable = month_variable;
		        	}


		        	let view_date = year+'-'+month_variable+'-'+day_variable;
		        	
		        	// if(view_date === '2023-01-10')
		        	// {
		        	// 	alert(data_checked['2023-01-11'][0]);
		        	// }

		        	let days_data = data_checked[view_date];
		        	let color_code = '#FFFFFF';
		        	if (typeof days_data === 'undefined') {
						  
					}
					else
					{

						color_code =  days_data[0]['color_code'];

						// alert(color_code)
						for (var k = 0; k < days_data.length; k++) {
							
							days_list += days_data[k]['name']+'\n';
						}
					}

		            //////////////////////////////////////////////////////////////////////////////////////////////////
		            // checking to see if i is equal to the current day, if so then we are making the color of //
		            //that cell a different color using CSS. Also adding a rollover effect to highlight the  //
		            //day the user rolls over. This loop creates the acutal calendar that is displayed.		//
		            //////////////////////////////////////////////////////////////////////////////////////////////////

		            if (i == day && month == cmonth) {

		            	

						

						
						// if(days_data.length > 0){
							
						// }
		                padding += "<td class='currentday'  onMouseOver='this.style.background=\"#00FF00\"; this.style.color=\"#FFFFFF\"' onMouseOut='this.style.background=\"#FFFFFF\"; this.style.color=\"#00FF00\"' style='border: 1px solid lightgrey;color: #000;text-align: center;width: 190.85px !important;'><div class='col-print-12'><div class='col-print-2' style='background-color:"+color_code+";padding: 0px !important;text-align: right !important;padding-right: 5px !important;'>" + i + "</div><div class='col-print-10'  style='border-left:1px solid lightgrey;padding: 0px !important;text-align: left !important;padding-left: 5px !important;'>"+days_list+"</div></div></td>";
		            } else {

		            	

						
				
		                // padding += "<td class='currentmonth' onMouseOver='this.style.background=\"#00FF00\"' onMouseOut='this.style.background=\"#FFFFFF\"' style='border: 1px solid lightgrey;color: #000;text-align: center;'><div class='col-print-12'><div class='col-print-2' style='background-color:"+color_code+"'>" + i + "</div><div class='col-print-10'  style='border-left:1px solid lightgrey'>"+days_list+"</div></div></td>";

		                padding += "<td class='currentmonth' onMouseOver='this.style.background=\"#00FF00\"' onMouseOut='this.style.background=\"#FFFFFF\"' style='border: 1px solid lightgrey;color: #000;text-align: center;width: 190.85px !important;'><span class='col-print-2' style='background-color:"+color_code+";padding: 0px !important;text-align: right !important;padding-right: 5px !important;'>" + i + "</span><span class='col-print-10'  style='border-left:1px solid lightgrey;padding: 0px !important;text-align: left !important;padding-left: 5px !important;font-size:9px;'>"+days_list+"</span></td>";


		            }

		            tempweekday2++;
		            i++;
		        }


		        /////////////////////////////////////////
		        // Ouptputing the calendar onto the	//
		        // site.  Also, putting in the month	//
		        // name and days of the week.		//
		        /////////////////////////////////////////

		        // var calendarTable = "<div class='col-md-12'><table class='calendar' style='width: 100%;'> <tr class='currentmonth'><th colspan='7'>" + monthNames[month] + " " + year + "</th></tr>";
		        // if(monthNames[month] == 'Jan')
		        // {
		        // 	calendarTable += "<tr class='weekdays'>  <td>Sun</td>  <td>Mon</td> <td>Tues</td> <td>Wed</td> <td>Thurs</td> <td>Fri</td> <td>Sat</td> </tr>";
		        // }
		        
		        // calendarTable += "<tr>";
		        // calendarTable += padding;
		        // calendarTable += "</tr></table></div>";

		         var calendarTable = "<div class='col-md-12' style='border-top:1px solid black'><table style='width: 100% !important;'> <tr ><td style='width: 5% !important;'>" + monthNames[month] + "</td>";
			       
			        
			        calendarTable += "<td style='width: 90% !important;'><table class='calendar' style='width: 100%;'><tr class='currentmonth'>";
			         if(monthNames[month] == 'Jan')
			        {
			        	calendarTable += "<tr class='weekdays'>  <th style='border-right: 1px solid lightgrey;text-align:center;width: 190.85px !important;'>Sun</th>  <th style='border-right: 1px solid lightgrey;text-align:center;width: 190.85px !important;'>Mon</th> <th style='border-right: 1px solid lightgrey;text-align:center;width: 190.85px !important;'>Tues</th> <th style='border-right: 1px solid lightgrey;text-align:center;width: 190.85px !important;'>Wed</th> <th style='border-right: 1px solid lightgrey;text-align:center;width: 190.85px !important;'>Thurs</th> <th style='border-right: 1px solid lightgrey;text-align:center;width: 190.85px !important;'>Fri</th> <th style='border-right: 1px solid lightgrey;text-align:center;width: 190.85px !important;'>Sat</th> </tr>";
			        }
			        calendarTable += padding;
			        calendarTable += "</tr></table></td><td style='width: 5% !important;text-align:center'>" + monthNames[month] + "</td></table></div>";
		        document.getElementById("calendar").innerHTML += calendarTable;
		    }

		    function go12() {
		        for (i = 0; i < 12; i++) {
		            calendar(i);
		        }
		    }

		    if (window.addEventListener) {
		        window.addEventListener('load', go12, false);
		    } else if (window.attachEvent) {
		        window.attachEvent('onload', go12);
		    }

		})();


			function add_new_event()
			{

				document.getElementById("sidebar-right").style.display = "block"; 
				document.getElementById("existing-sidebar-div").style.display = "none"; 

				var config_url = $('#config_url').val();
				var data_url = config_url+"planner/add_new_event";
				// window.alert(data_url);
				$.ajax({
				type:'POST',
				url: data_url,
				data:{appointment_id: 1},
				dataType: 'text',
				success:function(data){

					document.getElementById("current-sidebar-div").style.display = "block"; 
					$("#current-sidebar-div").html(data);
					
					tinymce.init({
					                selector: ".cleditor",
					               	height: "200"
						            });
					$('.datepicker').datepicker();
					$('.timepicker').timepicker();


					// alert(data);
					},
					error: function(xhr, status, error) {
					//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
					alert(error);
				}

				});

			}


			function close_side_bar()
			{
				// $('html').removeClass('sidebar-right-opened');
				document.getElementById("sidebar-right").style.display = "none"; 
				document.getElementById("current-sidebar-div").style.display = "none"; 
				document.getElementById("existing-sidebar-div").style.display = "none"; 
				tinymce.remove();
			}
		</script>

       

    </body>
</html>


