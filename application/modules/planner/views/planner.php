<style type="text/css">
	.currentmonth {
	    color: blue;
	    text-align: left;
	}
	.currentday {
	    border: 1px solid black;
	    color: #00FF00;
	    text-align: center;
	}

	table.calendar {
/*	    margin:1em 1em 2em 1em;*/
	}
	table.calendar td, table.calendar th {
/*	    padding:0.5em;*/
/*		width: 14.285714285714286% !important;*/
/*		width: 1.285714285714286% !important;*/
		width: 190.85px !important;
	}
	table.calendar {
	    display:inline-block;
	    *display:inline; zoom:1;
	    vertical-align:top;
	}
	table.calendar tr td .col-print-2
	{
/*		width: 10% !important;*/
		padding: 0px !important;
		text-align: right !important;
		padding-right: 0px !important;
	}
	table.calendar tr td .col-print-10
	{
/*		width: 90% !important;*/
		padding: 0px !important;
		text-align: left !important;
		padding-left: 0px !important;
	}
	.col-print-12 {
		width: 100%;
		float: left;
		position: relative;
		min-height: 1px;
		padding: 0px !important; 
	}
	.col-print-10
	{
		white-space: pre-wrap;
        white-space: -moz-pre-wrap;
        white-space: -pre-wrap;
        white-space: -o-pre-wrap;
        word-wrap: break-word;
/*        width:150px;*/
	}
</style>

<div class="row">
	<div class="col-md-2">
		<div class="row" style="height: 85vh;overflow-y: scroll;">
			<?php
			$this->db->where('leave_type_id > 0');
			$query = $this->db->get('leave_type');
			$result_items = '';
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $key => $value) {
					// code...
					$leave_type_name = $value->leave_type_name;
					$leave_type_id = $value->leave_type_id;
					$color_code = $value->color_code;
					

					$result_items .='<div class="row">
										<div class="col-md-1 ">
											<input type="checkbox" name="leave_type'.$leave_type_id.'" checked>
										</div>
										<label class="col-md-2 control-label">
											<div style="height:20px;background-color:'.$color_code.';width:100%"></div>
										</label>
							            <div class="col-md-8">
							                  '.$leave_type_name.'
							            </div>
							        </div>
							            ';
				}
			}


			?>
			<div class="col-md-12">
				<div class="col-md-12" style="margin-bottom:20px;">
					<a class="btn btn-info btn-sm col-md-12 center-align" onclick="add_new_event()"> <i class="fa fa-plus"></i> ADD EVENT </a>
					<br>
					<br>
					<a class="btn btn-primary btn-sm col-md-12  center-align" href="<?php echo site_url().'leave-management/leave-calendar'?>"> <i class="fa fa-arrow-right"></i> LEAVE CALENDAR </a>

					<br>
					<br>
					<a class="btn btn-warning btn-sm col-md-12  center-align" target="_BLANK" href="<?php echo site_url().'planner/print_planner'?>"> <i class="fa fa-print"></i> PRINT PLANNER </a>
				</div>
				
				<br>
				<h4>LEAVE TYPES </h4>
				
				<br>
				<?php echo $result_items;?>


				<?php
				$this->db->where('event_type_id > 0');
				$query = $this->db->get('event_type');
				$result_items_events = '';
				if($query->num_rows() > 0)
				{
					foreach($query->result() as $key => $value) {
						// code...
						$event_type_name = $value->event_type_name;
						$event_type_id = $value->event_type_id;
						$color_code = $value->color_code;
						

						$result_items_events .='<div class="row">
													<div class="col-md-1 ">
														<input type="checkbox" name="event_type'.$event_type_id.'" checked>
													</div>
													<label class="col-md-2 control-label">
														<div style="height:20px;background-color:'.$color_code.';width:100%"></div>
													</label>
										            <div class="col-md-8">
										                  '.$event_type_name.'
										            </div>
										        </div>';
					}
				}


				?>
				<br>
				
				<h4>OTHER CALENDARS</h4>
				<br>
				<?php echo $result_items_events;?>
				
			</div>
		</div>
	</div>
	<div class="col-md-10">
		<div class="panel-body" style="height:85vh;overflow-y:scroll;padding:0px;">
			<div id="calendar"></div>
		</div>
	</div>
</div>

<?php
$planner_details = json_encode($event_list);

// var_dump($planner_details);die();
?>
<script type="text/javascript">

	function get_planner()
	{
		// alert("sasas");
		var config_url = $('#config_url').val();
		var date = new Date();
	    var d = date.getDate();
	    var m = date.getMonth();
	    var y = date.getFullYear();
	    let appointments = '';
		  $.ajax({
			type:'POST',
			url: config_url+"planner/planner_schedule",
			// cache:false,
			// contentType: false,
			// processData: false,
			dataType: "text",
			success:function(data){
				var data = jQuery.parseJSON(data);
				 appointments = data;
				// var total_events = parseInt(data.total_events, 10);

				// for(i = 0; i < total_events; i++)
				// {
				      
		        //     var data_array = [];	
				// 	data_title = data.title[i];
				// 	data_start = data.start[i];
				// 	data_end = data.end[i];
				// 	data_backgroundColor = data.backgroundColor[i];
				// 	data_borderColor = data.borderColor[i];
				// 	data_allDay = data.allDay[i];
				// 	data_url = data.url[i];
					
				// 	//add the items to an array
				// 	data_array.title = data_title;
				// 	data_array.start = data_start;
				// 	data_array.end = data_end;
				// 	data_array.backgroundColor = data_backgroundColor;
				// 	data_array.borderColor = data_borderColor;
				// 	data_array.allDay = data_allDay;
				// 	data_array.url = data_url;
				// 	//console.log(data_array);
				// 	appointments.push(data_array);
				// }
				/*console.log(appointments);
				for(var i in data){
					appointments.push([i, data [i]]);alert(data[i]);
				}*/
				
				// alert(data['2023-01-10'][0]);

				
			},
			error: function(xhr, status, error) {
				// alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				  appointments = null;
			}
		});
		  // alert(appointments);
		  return appointments;
	}

	


	
	(function () {

		
		// let x = myFunction(4, 3); 

		// let days_data = data_checked['2023-01-10'];

		
		// for (var i = 0; i < days_data.length; i++) {
		// 	alert(days_data[i]);
		// }
		// alert(data_checked['2023-01-10'][1]);





    function go12() {
        for (i = 0; i < 12; i++) {
            calendar(i);
        }
    }

    if (window.addEventListener) {
        window.addEventListener('load', go12, false);
    } else if (window.attachEvent) {
        window.attachEvent('onload', go12);
    }

}

)();
	function get_calendar() {

		// alert("sashkjasa");
        for (i = 0; i < 12; i++) {
            calendar(i);
        }
        // if (window.addEventListener) {
	    //     window.addEventListener('load', get_calendar, false);
	    // } else if (window.attachEvent) {
	    //     window.attachEvent('onload', get_calendar);
	    // }
    }


    function calendar(month) 
    {
    	let data_checked = <?php echo $planner_details?>;
    	// alert(data_checked);
        //Variables to be used later.  Place holders right now.
        var padding = "";
    
        var totalFeb = "";
        var i = 1;
        var testing = "";

        var current = new Date();
        var cmonth = current.getMonth();
        var day = current.getDate();
        var year = current.getFullYear();
        var tempMonth = month + 1; //+1; //Used to match up the current month with the correct start date.
        var prevMonth = month - 1;

        //Determing if Feb has 28 or 29 days in it.  
        if (month == 1) {
            if ((year % 100 !== 0) && (year % 4 === 0) || (year % 400 === 0)) {
                totalFeb = 29;
            } else {
                totalFeb = 28;
            }
        }

        //////////////////////////////////////////
        // Setting up arrays for the name of    //
        // the	months, days, and the number of	//
        // days in the month.                   //
        //////////////////////////////////////////

        var monthNames = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
        var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thrusday", "Friday", "Saturday"];
        var totalDays = ["31", "" + totalFeb + "", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"];

        //////////////////////////////////////////
        // Temp values to get the number of days//
        // in current month, and previous month.//
        // Also getting the day of the week.	//
        //////////////////////////////////////////

        var tempDate = new Date(tempMonth + ' 1 ,' + year);
        var tempweekday = tempDate.getDay();
        var tempweekday2 = tempweekday;
        var dayAmount = totalDays[month];
        // var preAmount = totalDays[prevMonth] - tempweekday + 1;	

        //////////////////////////////////////////////////
        // After getting the first day of the week for	//
        // the month, padding the other days for that	//
        // week with the previous months days.  IE, if	//
        // the first day of the week is on a Thursday,	//
        // then this fills in Sun - Wed with the last	//
        // months dates, counting down from the last	//
        // day on Wed, until Sunday.                    //
        //////////////////////////////////////////////////

        while (tempweekday > 0) {
            padding += "<td class='premonth' style='border: 1px solid lightgrey;color: #000;text-align: center;'><div class='col-print-12' >&nbsp;</div></td>";
            //preAmount++;
            tempweekday--;
        }
        //////////////////////////////////////////////////
        // Filling in the calendar with the current     //
        // month days in the correct location along.    //
        //////////////////////////////////////////////////
       
        
       
        while (i <= dayAmount) {


        	var days_list = '&nbsp;';
            //////////////////////////////////////////
            // Determining when to start a new row	//
            //////////////////////////////////////////

            if (tempweekday2 > 6) {
                tempweekday2 = 0;

                padding += "</tr><tr >";
                
            }


        	// next
        	let day_variable = i;
        	if(day_variable<10)
        	{
        		day_variable = '0'+day_variable;
        	}
        	else
        	{
        		day_variable = day_variable;
        	}


        	let month_variable = month+1;
        	if(month_variable<10)
        	{
        		month_variable = '0'+month_variable;
        	}
        	else
        	{
        		month_variable = month_variable;
        	}


        	let view_date = year+'-'+month_variable+'-'+day_variable;
        	
        	// if(view_date === '2023-01-10')
        	// {
        	// 	alert(data_checked['2023-01-11'][0]);
        	// }

        	let days_data = data_checked[view_date];
        	let color_code = '#FFFFFF';
        	if (typeof days_data === 'undefined') {
				  
			}
			else
			{

				color_code =  days_data[0]['color_code'];

				// alert(color_code)
				for (var k = 0; k < days_data.length; k++) {
					color_code =  days_data[k]['color_code'];

					event_type =  days_data[k]['event_type'];
					// alert(event_type);
					if(event_type == "Event")
					{
						// days_list += '<span style="background-color:'+color_code+';width:100%" class="col-md-12">&nbsp;</span>';
						days_list += days_data[k]['name']+'';
					}
					else{

						days_list += days_data[k]['name']+'';
					}
				}
			}
			// removing last character from the string
			days_list = days_list.slice(0, -1);
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // checking to see if i is equal to the current day, if so then we are making the color of //
            //that cell a different color using CSS. Also adding a rollover effect to highlight the  //
            //day the user rolls over. This loop creates the acutal calendar that is displayed.		//
            //////////////////////////////////////////////////////////////////////////////////////////////////

            if (i == day && month == cmonth) {

            	

				

				
				// if(days_data.length > 0){
					
				// }
                padding += "<td class='currentday'  onMouseOver='this.style.background=\"#00FF00\"; this.style.color=\"#FFFFFF\"' onMouseOut='this.style.background=\"#FFFFFF\"; this.style.color=\"#00FF00\"' style='border: 1px solid lightgrey;color: #000;text-align: center;'><div class='col-print-12'><div class='col-print-2' style=''>" + i + "</div><div class='col-print-10'  style=''>"+days_list+"</div></div></td>";
            } else {

            	

				
		
                // padding += "<td class='currentmonth' onMouseOver='this.style.background=\"#00FF00\"' onMouseOut='this.style.background=\"#FFFFFF\"' style='border: 1px solid lightgrey;color: #000;text-align: center;'><div class='col-print-12'><div class='col-print-2' style='background-color:"+color_code+"'>" + i + "</div><div class='col-print-10'  style='border-left:1px solid lightgrey'>"+days_list+"</div></div></td>";

                padding += "<td class='currentmonth' onMouseOver='this.style.background=\"#00FF00\"' onMouseOut='this.style.background=\"#FFFFFF\"' style='border: 1px solid lightgrey;color: #000;text-align: center;'><span class='col-print-2' >" + i + "</span><span class='col-print-10'  style='border-left:1px solid lightgrey;font-size:9px;'>"+days_list+"</span></td>";


            }
              // padding += " <td lass='currentmonth' onMouseOver='this.style.background=\"#00FF00\"' onMouseOut='this.style.background=\"#FFFFFF\"' style='border: 1px solid lightgrey;color: #000;text-align: center;'>"+i+"</td>";
            tempweekday2++;
            i++;

        }


        /////////////////////////////////////////
        // Ouptputing the calendar onto the	//
        // site.  Also, putting in the month	//
        // name and days of the week.		//
        /////////////////////////////////////////

        // var calendarTable = "<div class='col-md-12'><table class='calendar' style='width: 100%;'> <tr class='currentmonth'><th colspan='7'>" + monthNames[month] + " " + year + "</th></tr>";
        // if(monthNames[month] == 'Jan')
        // {
        // 	calendarTable += "<tr class='weekdays'>  <td>Sun</td>  <td>Mon</td> <td>Tues</td> <td>Wed</td> <td>Thurs</td> <td>Fri</td> <td>Sat</td> </tr>";
        // }
        
        // calendarTable += "<tr>";
        // calendarTable += padding;
        // calendarTable += "</tr></table></div>";

         var calendarTable = "<div class='col-md-12' style='border-top:1px solid black'><table style='width: 100% !important;'> <tr ><td style='width: 5% !important;'>" + monthNames[month] + "</td>";
	       
	        
	        calendarTable += "<td style='width: 90% !important;'><table class='calendar' style='width: 100%;'><tr class='currentmonth'>";
	         if(monthNames[month] == 'Jan')
	        {
	        	calendarTable += "<tr class='weekdays'>  <th style='border-right: 1px solid lightgrey;text-align:center'>Sun</th>  <th style='border-right: 1px solid lightgrey;text-align:center'>Mon</th> <th style='border-right: 1px solid lightgrey;text-align:center'>Tues</th> <th style='border-right: 1px solid lightgrey;text-align:center'>Wed</th> <th style='border-right: 1px solid lightgrey;text-align:center'>Thurs</th> <th style='border-right: 1px solid lightgrey;text-align:center'>Fri</th> <th style='border-right: 1px solid lightgrey;text-align:center'>Sat</th> </tr>";
	        }
	        calendarTable += padding;
	        calendarTable += "</tr></table></td><td style='width: 5% !important;text-align:center'>" + monthNames[month] + "</td></table></div>";
        document.getElementById("calendar").innerHTML += calendarTable;
    }

	function add_new_event()
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"planner/add_new_event";
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);
			
			tinymce.init({
			                selector: ".cleditor",
			               	height: "200"
				            });
			$('.datepicker').datepicker();
			$('.timepicker').timepicker();


			// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});

	}


	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
		var config_url = $('#config_url').val();
		window.location.href = config_url+'leave-management/uhdc-planner';
	}
	function edit_event_details(event_id)
	{
		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"planner/edit_event_event/"+event_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "s"; 
			$("#current-sidebar-div").html(data);
			
			tinymce.init({
			                selector: ".cleditor",
			               	height: "200"
				            });
			// $('.datepicker').datepicker();
			// $('.timepicker').timepicker();
			get_available_calendar()

			// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});

	}
	function get_available_calendar()
    {
        var config_url = $('#config_url').val();
        var days = $('#event_days').val();
        var start_date = $('#event_start_date').val();

        var url = config_url+"planner/get_planner_days";

        $.ajax({
        type:'POST',
        url: url,
        data:{days: days,start_date: start_date},
        dataType: 'text',
        success:function(data){
          var data = jQuery.parseJSON(data);

            if(data.message == 'success')
            {
                $('#days-schedule').html(data.results);
                document.getElementById("submit_button").style.display = 'block'; 
                document.getElementById("event_end_date").value = data.end_date; 
            }
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
        });
    }

    $(document).on("submit","form#add-event-planners",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		
		var config_url = $('#config_url').val();	

		 var url = config_url+"planner/add_event";
		 // var values = $('#visit_type_id').val();
		 
	       $.ajax({
	       type:'POST',
	       url: url,
	       data:form_data,
	       dataType: 'text',
	       processData: false,
	       contentType: false,
	       success:function(data)
	       {
	          	var data = jQuery.parseJSON(data);
	        
	          	if(data.message == "success")
				{
					document.getElementById("sidebar-right").style.display = "none"; 
					close_side_bar();

					// $('#calendar').fullCalendar('destroyEvents');
					// var formDate = window.localStorage.getItem('date_set_old');
	    			// var url = config_url+'planner/get_uhdc_calendar/'+formDate;
					// $('#calendar').fullCalendar( 'refetchEvents',url );

					// get_calendar();
					window.location.href = config_url+'leave-management/uhdc-planner';

			
				}
				else
				{
					alert('Please ensure you have added included all the items');
				}
	       
	       },
	       error: function(xhr, status, error) {
	       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	       
	       }
	       });
		 
		
	   
		
	});

	$(document).on("submit","form#event-event-planners",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		
		var config_url = $('#config_url').val();	

		 var url = config_url+"planner/edit_event";
		 // var values = $('#visit_type_id').val();
		 
	       $.ajax({
	       type:'POST',
	       url: url,
	       data:form_data,
	       dataType: 'text',
	       processData: false,
	       contentType: false,
	       success:function(data)
	       {
	          	var data = jQuery.parseJSON(data);
	        
	          	if(data.message == "success")
				{
					document.getElementById("sidebar-right").style.display = "none"; 
					close_side_bar();

					// $('#calendar').fullCalendar('destroyEvents');
					// var formDate = window.localStorage.getItem('date_set_old');
	    			// var url = config_url+'planner/get_uhdc_calendar/'+formDate;
					// $('#calendar').fullCalendar( 'refetchEvents',url );

					// get_calendar();
					window.location.href = config_url+'leave-management/uhdc-planner';

			
				}
				else
				{
					alert('Please ensure you have added included all the items');
				}
	       
	       },
	       error: function(xhr, status, error) {
	       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	       
	       }
	       });
		 
		
	   
		
	});
</script>