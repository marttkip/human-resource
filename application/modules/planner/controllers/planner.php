<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once "./application/modules/admin/controllers/admin.php";
error_reporting(0);
class Planner extends admin 
{
	function __construct()
	{
		parent:: __construct();
		
		
		$this->load->model('planner/planner_model');

		$this->load->model('auth/auth_model');
		$this->load->model('reception/reception_model');
		$this->load->model('messaging/messaging_model');
		
		// if(!$this->auth_model->check_login())
		// {
		// 	redirect('login');
		// }
	}



	public function index() 
	{
		$branch_id = $this->session->userdata('branch_id');
		$branch_name = $this->session->userdata('branch_name');
		$data['title'] = 'Organization Planner';
		$v_data['title'] = $data['title'];
		// var_dump($v_data);die();

		$leave_result = $this->planner_model->get_all_schedule();




		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$event_list = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			
			foreach($result as $res)
			{
				$event_id = $res->event_id;
				$event_name = $res->event_name;
				$personnel_onames = $res->personnel_onames;
				$start_date = $res->start_date;
				$event_type = $res->event_type;
				$start_date = $res->start_date;
				$color_code = $res->color_code;
				if($date_of_event != $start_date)
				{
					$r = 0;
				}
				$date_of_event = date('Y-m-d',strtotime($start_date)); 


				$event_name = $this->abbreviateName($event_name);


				// $name_of_event = '<p style="margin:0px !important">'.$abbreviateName.'</p>';
				if($event_type == 'Event')
					$name_of_event = '<span onclick="edit_event_details('.$event_id.')" style="background-color:'.$color_code.';width:100%" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>';
				else{
					// $event_name = explode(' ', $event_name);
					$name_of_event = '<p style="margin:0px !important">'.$event_name.'</p>';
				}

				$event_list[$date_of_event][$r]['name'] = $name_of_event;
				$event_list[$date_of_event][$r]['color_code'] = $color_code;
				$event_list[$date_of_event][$r]['event_type'] = $event_type;

				$r++;
			}
		}

		$v_data['event_list'] = $event_list;
		
		// $data['total_events'] = $r;
		// var_dump($data);die();
		// echo json_encode($data);
		$data['content'] = $this->load->view('planner/planner', $v_data, true);
		
		$this->load->view('admin/templates/calendar_page', $data);
	}
	function abbreviateName($whole_name)
	{
	    $alterName = str_word_count($whole_name);
	    
	    if($alterName >= 2)
	    {
	      $nameExploded = explode(' ',$whole_name);
	      
	      $firstName = reset($nameExploded);
	      $lastName = mb_substr(end($nameExploded), 0, 1);
	        
	        return $firstName.'. '.$lastName.'.';
	    }
	  return $whole_name;
	}
	public function planner_view()
	{
		$leave_result = $this->planner_model->get_all_schedule();
		
		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$data = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			foreach($result as $res)
			{
				$event_id = $res->event_id;
				$event_name = $res->event_name;
				$event_type = $res->event_type;
				$start_date = $res->start_date;
				$end_date = $res->end_date;
				$days = $res->days;


				

				$start_date = date('D M d Y',strtotime($start_date)); 
				$end_date = date('D M d Y',strtotime($end_date)); 
				$time_start = $start_date.' 8:00 AM GMT+0300'; 
				$time_end = $end_date.' 11:59 PM GMT+0300';
				//$color = $this->reception_model->random_color();
				// var_dump($time_end);die();
				if($event_type == 'Leave')
				{
					$color = '#0088CC';
				}
				
				else
				{
					$color = '#b71c1c';
				}
				$leave_days = $this->site_model->calculate_leave_days($start_date, $end_date);
				
				$data['title'][$r] = $event_name;
				$data['start'][$r] = $time_start;
				$data['end'][$r] = $time_end;
				$data['backgroundColor'][$r] = $color;
				$data['borderColor'][$r] = $color;
				$data['allDay'][$r] = TRUE;
				$data['url'][$r] = site_url().'hr/personnel-leave-detail/'.$event_id;
				$r++;
			}
		}
		
		$data['total_events'] = $r;
		echo json_encode($data);
	}

	public function get_uhdc_calendar_old()
	{


		// if(empty($todays_date))
		// {
		// 	parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);

		// 	$epoch2 = $_GET['end'];

		// 	$dt = new DateTime("@$epoch2");  // convert UNIX timestamp to PHP DateTime
		// 	$todays_date =  $dt->format('Y-m-d');
		// }
		// else
		// {
		// 	$dt = new DateTime("@$todays_date");  // convert UNIX timestamp to PHP DateTime
		// 	$todays_date =  $dt->format('Y-m-d');
		// }

		// var_dump($todays_date);die();
		$leave_result = $this->planner_model->get_all_schedule();
		
		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$data = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			foreach($result as $res)
			{
				$event_id = $res->event_id;
				$event_name = $res->event_name;
				$event_type = $res->event_type;
				$start_date = $res->start_date;
				$end_date = $res->end_date;
				$color_code = $res->color_code;


				

				$start_date = date('D M d Y',strtotime($start_date)); 
				$end_date = date('D M d Y',strtotime($end_date)); 
				$time_start = $start_date.' 8:00 AM GMT+0300'; 
				$time_end = $end_date.' 11:59 PM GMT+0300';
				//$color = $this->reception_model->random_color();
				// var_dump($time_end);die();
				// if($event_type == 'Leave')
				// {
				// 	$color = '#0088CC';
				// }
				
				// else
				// {
					$color = $color_code;
				// }
				$leave_days = $this->site_model->calculate_leave_days($start_date, $end_date);
				
				$data['title'][$r] = $event_name;
				$data['start'][$r] = $time_start;
				$data['end'][$r] = $time_end;
				$data['backgroundColor'][$r] = $color;
				$data['borderColor'][$r] = $color;
				$data['allDay'][$r] = TRUE;
				$data['url'][$r] = site_url().'hr/personnel-leave-detail/'.$event_id;
				$r++;
			}
		}
		
		$data['total_events'] = $r;
		echo json_encode($data);
	}


	public function get_uhdc_calendar()
	{

		$start = '2021-08-01';//$_POST['start'];
		$end = '2021-09-30';//$_POST['start'];

		parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
		$epoch1 = $_GET['start'];
		$epoch2 = $_GET['end'];

		$dt = new DateTime("@$epoch1");  // convert UNIX timestamp to PHP DateTime
		$start =  $dt->format('Y-m-d');

		$dt = new DateTime("@$epoch2");  // convert UNIX timestamp to PHP DateTime
		$end =  $dt->format('Y-m-d');
	
		$leave_result = $this->planner_model->get_all_schedule($start,$end);
		
		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$data = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			foreach($result as $res)
			{
				$event_id = $res->event_id;
				$event_name = $res->event_name;
				$event_type = $res->event_type;
				$start_date = $res->start_date;
				$end_date = $res->end_date;
				$color_code = $res->color_code;


				

				$start_date = date('Y-m-d',strtotime($start_date)); 
				$end_date = date('Y-m-d',strtotime($end_date)); 
				$time_start = $start_date.'T08:00:00+0300'; 
				$time_end = $end_date.'T11:00:00+0300';
				//$color = $this->reception_model->random_color();
				// var_dump($time_end);die();
				// if($event_type == 'Leave')
				// {
				// 	$color = '#0088CC';
				// }
				
				// else
				// {
					$color = $color_code;
				// }
				$leave_days = $this->site_model->calculate_leave_days($start_date, $end_date);


				$data['results'][] = array(
											'id'=>$event_id,
										    'title' => $event_name,
										    'start' => $time_start,
										    'end' => $time_end,
										    'description' => $event_type,
										    'backgroundColor'=>$color,
										    'borderColor'=>$color,
										    'className'=>'fc-nonbusiness'
										    
										  );
				
				// $data['title'][$r] = $event_name;
				// $data['start'][$r] = $time_start;
				// $data['end'][$r] = $time_end;
				// $data['backgroundColor'][$r] = $color;
				// $data['borderColor'][$r] = $color;
				// $data['allDay'][$r] = TRUE;
				// $data['url'][$r] = site_url().'hr/personnel-leave-detail/'.$event_id;
				$r++;
			}
		}
		$data['success'] = true;
		// $data['total_events'] = $r;
		echo json_encode($data['results']);
	}
	public function add_new_event()
	{
		$v_data['visit_id'] = 1;

		$start_date = $this->input->post('start');
		$end_date = $this->input->post('end');


		if(!empty($start_date) AND !empty($end_date))
		{
			$v_data['start_date'] = $start_date;
			$v_data['end_date'] = $end_date;
		}
		else
		{
			$v_data['start_date'] = date('Y-m-d');
			$v_data['end_date'] = date('Y-m-d');
		}
		
		
		$page = $this->load->view('add_new_event',$v_data,true);
		echo $page;
	}

	public function edit_event_event($event_id)
	{
		$v_data['event_id'] = $event_id;

		$start_date = $this->input->post('start');
		$end_date = $this->input->post('end');


		if(!empty($start_date) AND !empty($end_date))
		{
			$v_data['start_date'] = $start_date;
			$v_data['end_date'] = $end_date;
		}
		else
		{
			$v_data['start_date'] = date('Y-m-d');
			$v_data['end_date'] = date('Y-m-d');
		}
		
		
		$page = $this->load->view('edit_event_details',$v_data,true);
		echo $page;
	}
	public function add_event()
	{
		$this->form_validation->set_rules('event_name', 'Event Name', 'required');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		// $this->form_validation->set_rules('start_time', 'Start Time', 'required');
		// $this->form_validation->set_rules('end_time', 'End Time', 'required');
		$this->form_validation->set_rules('event_description', 'Description', 'required');
		$this->form_validation->set_rules('event_type_id', 'Event Type', 'required');
		$event_name = $this->input->post('event_name');
		$start_date = $this->input->post('start_date');
		$days = $this->input->post('days');
		// $end_date = $this->input->post('end_date');
		// $start_time = $this->input->post('start_time');
		$end_time = $this->input->post('end_time');
		$event_description = $this->input->post('event_description');
		$event_type_id = $this->input->post('event_type_id');

					
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_userdata('error_message', validation_errors());
			// $data['message'] = validation_errors();
			$data['message'] = 'fail';
		}
		else
		{

			// $startTime = date("H:i:00", strtotime($start_time));
			$appointment_array['start_date'] = $start_date;

			// $endTime = date("H:i:00", strtotime($end_time));
			$appointment_array['end_date'] = $end_date;

			$appointment_array['event_type_id'] = $event_type_id;
			$appointment_array['event_description'] = $event_description;			
			$appointment_array['event_name'] = $event_name;
			$appointment_array['event_duration_status'] = 1;
			$appointment_array['created'] = date('Y-m-d');
			$appointment_array['days'] = $days;
			$appointment_array['created_by'] = $this->session->userdata('personnel_id');
			// var_dump($appointment_array);die();

			$this->db->insert('event_duration',$appointment_array);
			$event_duration_id = $this->db->insert_id();

			if($event_duration_id > 0)
			{
				$start_date = $this->input->post('start_date');
				$data['start_date'] = $start_date;

				$result = '';
				$dateVal = $start_date;
				 $days = $days;
				for ($i=0; $i < $days; $i++) { 
					// code...
					//Assign a date as a string
					if($i == 0)
					{
						$dateVal = date('d-M-Y', strtotime($dateVal));
					}
					else
					{
						$dateVal = date('d-M-Y', strtotime($dateVal. ' + 1 days'));
					}

					$date_to_check = $dateVal = date('Y-m-d', strtotime($dateVal));

					
					$dt1 = strtotime($date_to_check);
			        $dt2 = date("l", $dt1);
			        $dt3 = strtolower($dt2);

			    	if(($dt3 == "sunday"))
					{
						$status = 'Sunday';
						$color = 'warning';
						$i--;
			        } 
			    	else
					{
						$status = 'Available';
						$color = '';
						$event_duration_array['event_duration_id'] = $event_duration_id;
						$event_duration_array['personnel_id'] = 0;
						$event_duration_array['event_date'] = $date_to_check;
						$event_duration_array['created_date'] = date('Y-m-d H:i:s');
						$this->db->insert('event_duration_allocations',$event_duration_array);

						// insert into the 
			          
			        }


				}
			}
			
			$data['message'] = 'success';
		}

		echo json_encode($data);

	}
	public function print_planner()
	{

		$branch_id = $this->session->userdata('branch_id');
		$branch_name = $this->session->userdata('branch_name');
		$data['title'] = 'Online Diary';
		$v_data['title'] = $data['title'];
		$leave_result = $this->planner_model->get_all_schedule();




		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$event_list = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			
			foreach($result as $res)
			{
				$event_id = $res->event_id;
				$event_name = $res->event_name;
				$personnel_onames = $res->personnel_onames;
				$start_date = $res->start_date;
				$event_type = $res->event_type;
				$start_date = $res->start_date;
				$color_code = $res->color_code;
				if($date_of_event != $start_date)
				{
					$r = 0;
				}
				$date_of_event = date('Y-m-d',strtotime($start_date)); 
				
				$event_name = $this->abbreviateName($event_name);

				$event_list[$date_of_event][$r]['name'] = $event_name;
				$event_list[$date_of_event][$r]['color_code'] = $color_code;
				$event_list[$date_of_event][$r]['event_type'] = $event_type;

				$r++;
			}
		}

		$v_data['event_list'] = $event_list;

		
		$v_data['contacts'] = $this->site_model->get_contacts();
		
		$html = $this->load->view('print_planner', $v_data);


	}


	public function planner_schedule()
	{
		$leave_result = $this->planner_model->get_all_schedule();




		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$data = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			
			foreach($result as $res)
			{
				$event_id = $res->event_id;
				$event_name = $res->event_name;
				$personnel_onames = $res->personnel_onames;
				$start_date = $res->start_date;
				$event_type = $res->event_type;
				$start_date = $res->start_date;
				$color_code = $res->color_code;

				$date_of_event = date('Y-m-d',strtotime($start_date)); 

				// $start_date = date('D M d Y',strtotime($start_date)); 
				// $end_date = date('D M d Y',strtotime($end_date)); 
				// $time_start = $start_date.' 8:00 AM GMT+0300'; 
				// $time_end = $end_date.' 11:59 PM GMT+0300';
				// //$color = $this->reception_model->random_color();
				// // var_dump($time_end);die();
				// if($leave_duration_status == 1)
				// {
				// 	$color = '#0088CC';
				// }
				
				// else
				// {
				// 	$color = '#b71c1c';
				// }
				// $leave_days = $this->site_model->calculate_leave_days($start_date, $end_date);


				$data[$date_of_event][$r] = $event_name;
				
				// $data['title'][$r] = $personnel_fname.' '.$personnel_onames.' - '.$leave_type_name.'. '.$leave_days.' days';
				// $data['start'][$r] = $time_start;
				// $data['end'][$r] = $time_end;
				// $data['backgroundColor'][$r] = $color;
				// $data['borderColor'][$r] = $color;
				// $data['allDay'][$r] = TRUE;
				// $data['url'][$r] = site_url().'hr/personnel-leave-detail/'.$personnel_id;
				$r++;
			}
		}
		
		// $data['total_events'] = $r;
		// var_dump($data);die();
		echo json_encode($data);
	}
	public function get_planner_days()
	{
		$days = $this->input->post('days');
		$start_date = $this->input->post('start_date');
		$data['start_date'] = $start_date;

		$result = '';
		$dateVal = $start_date;
		 $days = $days;
		for ($i=0; $i < $days; $i++) { 
			// code...
			//Assign a date as a string
			if($i == 0)
			{
				$dateVal = date('d-M-Y', strtotime($dateVal));
			}
			else
			{
				$dateVal = date('d-M-Y', strtotime($dateVal. ' + 1 days'));
			}

			$date_to_check = $dateVal = date('Y-m-d', strtotime($dateVal));

			
			$dt1 = strtotime($date_to_check);
	        $dt2 = date("l", $dt1);
	        $dt3 = strtolower($dt2);
	    	if(($dt3 == "sunday"))
			{
				$status = 'Sunday';
				$color = 'warning';
				$i--;
	        } 
	    	else
			{
				$status = 'Available';
				$color = '';
	          
	        }
			// $check_availability_rs = $this->leave_model->check_date_availability($date_to_check);

			// if($check_availability_rs->num_rows() > 0)
			// {
			// 	// status unavailable
			// 	$status = 'Not Available';
			// 	$i--;
			// }
			// else
			// {
				
			// }
				$result .= '<tr class="'.$color.'">
								<td>'.$dateVal.'</td>
								<td>'.$status.'</td>
							</tr>';




		}

		if(!empty($dateVal))
		{
			$end_date = date('Y-m-d', strtotime($dateVal));
		}

		$data['result'] = $result;

		$results = $this->load->view('calendar_days', $data,true);


		$response['message'] = 'success';
		$response['results'] = $results;
		$response['end_date'] = $end_date;
		// echo json_encode($data);
		echo json_encode($response);
	}
	public function edit_event()
	{
		$this->form_validation->set_rules('event_name', 'Event Name', 'required');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required');
		$this->form_validation->set_rules('end_date', 'End Date', 'required');
		// $this->form_validation->set_rules('start_time', 'Start Time', 'required');
		// $this->form_validation->set_rules('end_time', 'End Time', 'required');
		$this->form_validation->set_rules('event_description', 'Description', 'required');
		$this->form_validation->set_rules('event_type_id', 'Event Type', 'required');
		$event_name = $this->input->post('event_name');
		$start_date = $this->input->post('start_date');
		$event_duration_id = $this->input->post('event_duration_id');
		$days = $this->input->post('days');
		// $end_date = $this->input->post('end_date');
		// $start_time = $this->input->post('start_time');
		$end_time = $this->input->post('end_time');
		$event_description = $this->input->post('event_description');
		$event_type_id = $this->input->post('event_type_id');

					
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_userdata('error_message', validation_errors());
			// $data['message'] = validation_errors();
			$data['message'] = 'fail';
		}
		else
		{

			// $startTime = date("H:i:00", strtotime($start_time));
			$appointment_array['start_date'] = $start_date;

			// $endTime = date("H:i:00", strtotime($end_time));
			$appointment_array['end_date'] = $end_date;

			$appointment_array['event_type_id'] = $event_type_id;
			$appointment_array['event_description'] = $event_description;			
			$appointment_array['event_name'] = $event_name;
			$appointment_array['event_duration_status'] = 1;
			$appointment_array['created'] = date('Y-m-d');
			$appointment_array['days'] = $days;
			$appointment_array['created_by'] = $this->session->userdata('personnel_id');
			// var_dump($appointment_array);die();
			$this->db->where('event_duration_id',$event_duration_id);
			$this->db->update('event_duration',$appointment_array);
			

			$this->db->where('event_duration_id',$event_duration_id);
			$this->db->delete('event_duration_allocations');

			if($event_duration_id > 0)
			{
				$start_date = $this->input->post('start_date');
				$data['start_date'] = $start_date;

				$result = '';
				$dateVal = $start_date;
				 $days = $days;
				for ($i=0; $i < $days; $i++) { 
					// code...
					//Assign a date as a string
					if($i == 0)
					{
						$dateVal = date('d-M-Y', strtotime($dateVal));
					}
					else
					{
						$dateVal = date('d-M-Y', strtotime($dateVal. ' + 1 days'));
					}

					$date_to_check = $dateVal = date('Y-m-d', strtotime($dateVal));

					
					$dt1 = strtotime($date_to_check);
			        $dt2 = date("l", $dt1);
			        $dt3 = strtolower($dt2);

			    	if(($dt3 == "sunday"))
					{
						$status = 'Sunday';
						$color = 'warning';
						$i--;
			        } 
			    	else
					{
						$status = 'Available';
						$color = '';
						$event_duration_array['event_duration_id'] = $event_duration_id;
						$event_duration_array['personnel_id'] = 0;
						$event_duration_array['event_date'] = $date_to_check;
						$event_duration_array['created_date'] = date('Y-m-d H:i:s');
						$this->db->insert('event_duration_allocations',$event_duration_array);

						// insert into the 
			          
			        }


				}
			}
			
			$data['message'] = 'success';
		}

		echo json_encode($data);

	}
	

}
?>