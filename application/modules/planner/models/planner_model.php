<?php

class Planner_model extends CI_Model 
{


	/*
	*	Check if parent has children
	*
	*/

	public function get_branch_doctors($branch_id=NULL)
	{

		$add = '';
		// if(!empty($branch_id))
		// {
		// 	$add = ' AND personnel.branch_id = '.$branch_id;
		// }
		$table = "personnel, personnel_job,job_title";
		$where = "personnel_job.personnel_id = personnel.personnel_id AND personnel_job.job_title_id = job_title.job_title_id AND job_title.job_title_name = 'Dentist' ".$add;
		$items = "personnel.personnel_onames, personnel.personnel_fname, personnel.personnel_id,personnel.branch_id,personnel.authorize_invoice_changes";
		$order = "personnel_onames";
		


		$this->db->where($where);
		$this->db->select($items);
		$query = $this->db->get($table);
		
		return $query;
	}

	public function get_all_plans($date = NULL)
	{
		
		$where = 'appointments.appointment_delete = 0 AND appointments.appointment_status > 0 AND appointments.appointment_rescheduled = 0';
		
		$this->db->select('visit_date, time_start, time_end, appointments.*,appointments.appointment_id AS appointment,appointments.appointment_type,patients.patient_phone1,patients.patient_id,patients.patient_othernames,patients.patient_surname,visit.procedure_done,appointments.visit_id');
		$this->db->where($where);
		$this->db->join('visit','visit.visit_id = appointments.visit_id','left');
		$this->db->join('patients','patients.patient_id = visit.patient_id','left');

		$query = $this->db->get('appointments');
		
		return $query;
	}

	public function get_all_schedule($start = NULL,$end=NULL)
	{

		if(!empty($start) OR !empty($end))
		{
			$add = ' WHERE data.start_date BETWEEN "'.$start.'" AND "'.$end.'"';
		}
		else
		{
			$add = '';
		}

		$add = 'WHERE YEAR(data.start_date) = '.date('Y').' ';
		$select_statement = "
                        SELECT
                          data.event_id AS event_id,
                          data.event_name AS event_name,
                          data.start_date AS start_date,
                          data.end_date AS end_date,
                          data.event_type AS event_type

                        FROM 
                        (
                          SELECT
                            `leave_duration`.`leave_duration_id` AS event_id,
                            CONCAT(personnel.personnel_fname,' ',personnel.personnel_onames) as event_name,
                            leave_duration_allocations.leave_date as start_date,
                            leave_duration.end_date as end_date,
                            'Leave' AS event_type
                            FROM (leave_duration,leave_type,personnel,leave_duration_allocations)
                            WHERE leave_duration.leave_type_id = leave_type.leave_type_id 
                            AND personnel.personnel_id = leave_duration.personnel_id
                            AND leave_duration_allocations.leave_duration_id = leave_duration.leave_duration_id
                            AND leave_duration.leave_duration_status = 1
                           
                          UNION ALL

                          	SELECT
	                            `event_duration`.`event_duration_id` AS event_id,
	                            event_duration.event_name as event_name,
	                            event_duration_allocations.event_date as start_date,
	                            event_duration.end_date as end_date,
	                            'Event' AS event_type
	                            FROM (event_duration,event_type,event_duration_allocations)
	                            WHERE event_duration.event_type_id = event_type.event_type_id 
	                            AND event_duration.event_duration_status = 1
	                            AND event_duration_allocations.event_duration_id = event_duration.event_duration_id
                          ) AS data ".$add." ORDER BY data.start_date ASC";
		$query = $this->db->query($select_statement);
		return $query;


		
		// $where = 'leave_duration.personnel_id = personnel.personnel_id AND leave_duration.leave_type_id = leave_type.leave_type_id ';
		
		// $this->db->select('personnel.personnel_id, personnel.personnel_fname, personnel.personnel_onames, leave_type.leave_type_name, leave_duration.start_date, leave_duration.end_date, leave_duration.leave_duration_status');
		// $this->db->where($where);
		// $query = $this->db->get('leave_duration, personnel, leave_type');
		// // var_dump($query); die();
		// return $query;
	}

	public function get_event_details($event_id)
	{
		  $where = 'event_duration.event_duration_id = '.$event_id;
			$this->db->select('*');
			$this->db->where($where);

			$query = $this->db->get('event_duration');
			
			return $query;

	}
}
?>