<?php

class Leave_model extends CI_Model 
{
	public function get_assigned_leave($date = NULL)
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}
		$where = 'leave_duration.personnel_id = personnel.personnel_id AND leave_duration.leave_type_id = leave_type.leave_type_id ';
		
		$this->db->select('personnel.personnel_id, personnel.personnel_fname, personnel.personnel_onames, leave_type.leave_type_name, leave_duration.start_date, leave_duration.end_date, leave_duration.leave_duration_status');
		$this->db->where($where);
		$query = $this->db->get('leave_duration, personnel, leave_type');
		// var_dump($query); die();
		return $query;
	}
	
	public function get_day_leave($date)
	{
		//retrieve all users
 		$this->db->where('start_date', $date);
		$this->db->from('leave_duration, leave_type, personnel');
		$this->db->select('leave_duration.*, leave_type.leave_type_name, personnel.personnel_fname, personnel.personnel_onames');
		$this->db->order_by('leave_type.leave_type_name');
		$this->db->where('personnel.personnel_id = leave_duration.personnel_id AND leave_duration.leave_type_id = leave_type.leave_type_id AND start_date = \''.$date.'\'');
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Delete an existing leave_duration
	*	@param int $leave_duration_id
	*
	*/
	public function delete_leave_duration($leave_duration_id)
	{
		//delete parent
		if($this->db->delete('leave_duration', array('leave_duration_id' => $leave_duration_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated leave_duration
	*	@param int $leave_duration_id
	*
	*/
	public function activate_leave_duration($leave_duration_id)
	{
		$data = array(
				'leave_duration_status' => 1
			);
		$this->db->where('leave_duration_id', $leave_duration_id);
		
		if($this->db->update('leave_duration', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated leave_duration
	*	@param int $leave_duration_id
	*
	*/
	public function deactivate_leave_duration($leave_duration_id)
	{
		$data = array(
				'leave_duration_status' => 0
			);
		$this->db->where('leave_duration_id', $leave_duration_id);
		
		if($this->db->update('leave_duration', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function add_personnel_leave_request($personnel_id)
	{
		$leave_type_request_id = $this->input->post('leave_type_request_id');

		$exploded = explode('#',$leave_type_request_id);

		$leave_type_id = $exploded[0];
		$leave_type_count = $exploded[1];
		$start_date = $this->input->post("start_date");
		$days = $this->input->post("days");

		$items = array(
						'personnel_id' => $personnel_id,
						'start_date' => $this->input->post("start_date"),
						'end_date' => $this->input->post("end_date"),
						'leave_type_id' => $leave_type_id,
						'created_by'=>$this->session->userdata('personnel_id'),
						'modified_by'=>$this->session->userdata('personnel_id'),
						'created'=>date('Y-m-d H:i:s'),
						'days'=>$days
					  );
		$personnel_id = $this->session->userdata('personnel_id');
		$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
		if($personnel_id == 0 AND $authorize_invoice_changes)
			$items['leave_duration_status'] = 1;
		// var_dump($items);die();
		$startdate = null;
		$enddate = null;
		if($this->db->insert("leave_duration", $items))
		{
			$leave_duration_id = $this->db->insert_id();

			$dateVal = $start_date;
			 $days = $days;
			for ($i=0; $i < $days; $i++) { 
				// code...
				//Assign a date as a string
				if($i == 0)
				{
					$dateVal = date('d-M-Y', strtotime($dateVal));
				}
				else
				{
					$dateVal = date('d-M-Y', strtotime($dateVal. ' + 1 days'));
				}

				$date_to_check = $dateVal = date('Y-m-d', strtotime($dateVal));
				
				$check_availability_rs = $this->check_date_availability($date_to_check);
				$date_to_check_sun = $dateVal = date('Y-m-d', strtotime($dateVal));

					
				$dt1 = strtotime($date_to_check_sun);
		        $dt2 = date("l", $dt1);
		        $dt3 = strtolower($dt2);


				if($check_availability_rs->num_rows() > 0 OR ($dt3 == "sunday"))
				{
					// status unavailable
					$status = 'Not Available';
					$i--;
				}
				else
				{
					// add start date 
					// 
					if(empty($startdate))
					 $startdate = $date_to_check;

					if(empty($enddate))
					$enddate = $date_to_check;
					// insert the data
					$leave_duration_array['leave_duration_id'] = $leave_duration_id;
					$leave_duration_array['personnel_id'] = $personnel_id;
					$leave_duration_array['leave_date'] = $date_to_check;
					$leave_duration_array['created_date'] = date('Y-m-d H:i:s');
					$this->db->insert('leave_duration_allocations',$leave_duration_array);
					
				}
			




			}


			// update the values
			$array_update['start_date'] = $startdate;
			$array_update['end_date'] = $enddate;
			$this->db->where('leave_duration_id',$leave_duration_id);
			$this->db->update('leave_duration',$array_update);

			return TRUE;
		}
		else{
			return FALSE;
		}
	}


	public function add_personnel_leave_request_old($personnel_id)
	{
		$leave_type_request_id = $this->input->post('leave_type_request_id');
		$days = $this->input->post('days');

		$exploded = explode('#',$leave_type_request_id);

		$leave_type_id = $exploded[0];
		$leave_type_count = $exploded[1];

		// var_dump($exploded);die();
		$items = array(
						'personnel_id' => $personnel_id,
						'start_date' => $this->input->post("start_date"),
						'end_date' => $this->input->post("end_date"),
						'leave_type_id' => $leave_type_id,
						'created_by'=>$this->session->userdata('personnel_id'),
						'modified_by'=>$this->session->userdata('personnel_id'),
						'created'=>date('Y-m-d H:i:s'),
						'days'=>$days
					  );
		if($this->db->insert("leave_durationa", $items))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function get_leave_types_by_id($leave_type_id)
	{
		$table = "leave_type";
		$where = "leave_type_status = 0 AND leave_type.leave_type_id = ".$leave_type_id;
		$items = "leave_type_id, leave_type_name";
		$order = "leave_type_name";
		
		$this->db->where($where);
		$this->db->order_by($order);
		$result = $this->db->get($table);
		
		return $result;
	}

	public function get_personnel_leave_allocated($personnel_id,$leave_type_id,$year)
	{
		//retrieve all users
		$this->db->from('leave_allocations');
		$this->db->select('SUM(leave_allocations.allocated_days) AS total_allocated_days');
		// $this->db->order_by('leave_type.leave_type_name');
		$this->db->where('leave_allocations.leave_type_id = '.$leave_type_id.' AND leave_allocations.year = '.$year.' AND leave_allocations.leave_allocation_status = 0 AND leave_allocations.personnel_id = '.$personnel_id);
		$query = $this->db->get();
		$total_allocated_days = 0;
		if($query->num_rows() > 0)
		{
			foreach($query->result() AS $key => $value)
			{
				$total_allocated_days = $value->total_allocated_days;
			}
		}
		if(empty($total_allocated_days))
		{
			$total_allocated_days = 0;
		}
		return $total_allocated_days;
	}
	
	public function get_leave_balance($personnel_id, $leave_type_id)
	{
		//retrieve all users
		$this->db->from('leave_duration, leave_type');
		$this->db->select('leave_duration.*, leave_type.leave_type_name, leave_type.leave_type_count, leave_type.leave_days');
		$this->db->order_by('leave_type.leave_type_name');
		$this->db->where('leave_duration.leave_type_id = leave_type.leave_type_id AND leave_duration.personnel_id = '.$personnel_id);
		$query = $this->db->get();
		
		return $query;
	}

	public function get_leave_types($leave_type_id=null,$leave_type_count=null)
	{
		$add ='';
		if(!empty($leave_type_id))
		{
			$add = ' AND leave_type_id ='.$leave_type_id;
		}

		if(!empty($leave_type_count))
		{
			$add .= ' AND leave_type_count ='.$leave_type_count;
		}
		$table = "leave_type";
		$where = "leave_type_status = 0".$add;
		$items = "leave_type_id, leave_type_name";
		$order = "leave_type_name";
		
		$this->db->where($where);
		$this->db->order_by($order);
		$result = $this->db->get($table);
		
		return $result;
	}
	public function personnel_leave_opening_balance($personnel_id,$leave_type_id)
	{
		$current_year = date('Y');

		$this->db->where('personnel_id = '.$personnel_id.' AND year = '.$current_year.' AND leave_type_id = '.$leave_type_id);
		$query = $this->db->get('personnel_leave_opening_balance');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				$days = $value->days;
			}
		}
		else
		{
			$days = 0;
		}

		if(empty($days))
		{
			$days = 0;
		}

		return $days;
	}

	public function check_pending_leave($personnel_id,$leave_type_id=0)
	{

		$add = '';
		if(!empty($leave_type_id))
		{
			$add = ' AND leave_duration.leave_type_id = '.$leave_type_id;
		}

		//retrieve all users
		$this->db->from('leave_duration, leave_type');
		$this->db->select('leave_duration.*, leave_type.leave_type_name, leave_type.leave_type_count, leave_type.leave_days');
		$this->db->order_by('leave_type.leave_type_name');
		$this->db->where('leave_duration.leave_type_id = leave_type.leave_type_id AND leave_duration.leave_duration_status = 0 AND leave_duration.personnel_id = '.$personnel_id.$add);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function get_personnel_leave_balance($personnel_id,$leave_type_id)
	{
		$personnel = $this->personnel_model->get_personnel($personnel_id);

		$leave_types = $this->leave_model->get_leave_types_by_id($leave_type_id);
		$leave = $this->personnel_model->get_personnel_leave($personnel_id);


		$row = $personnel->row();

		$personnel_id = $row->personnel_id;
		$engagement_date = $row->engagement_date;

		if(empty($engagement_date) OR $engagement_date == '0000-00-00')
		{
			$engagement_date = date('Y-01-01');
		}

		$engagement_array = explode('-', $engagement_date);
		$engagement_year = $engagement_array[0];
		$engagement_month = (int)$engagement_array[1];
		$engagement_day = $engagement_array[2];

		$leave_balance = 0;
		$days_taken = 0;
		if($leave_types->num_rows() > 0)
	    {
	        foreach($leave_types->result() as $res)
	        {
	            $leave_type_id = $res->leave_type_id;
	            $leave_type_count = $res->leave_type_count;
	            $leave_type_name = strtoupper($res->leave_type_name);
	            $leave_days = $res->leave_days;
	            // devide accounting to the month
	            $leave_opening_balance = $this->leave_model->personnel_leave_opening_balance($personnel_id,$leave_type_id);
	            $leave_balance = $leave_days;
	            if($leave_type_count == 3)
	            {	

	            	$leave_balance = 0;

	            	$leave_per_month = $leave_days/12;

	            		// get the current month

	            	$current_month = (int)date('m');


	            	if($engagement_year == date('Y'))
	            	{
	            		// calculate the remaining leave days for the annual leave


	            		$total_months = $current_month - $engagement_month;
	            		if($current_month == $engagement_month)
		            	{
		            		$total_months = 1;
		            	}

	            		$leave_per_month = ($total_months*$leave_per_month);
	            		$leave_balance = $leave_per_month;

	            	}
	            

	            	// get avaulable leave days 

	            	// for ($i=1; $i <= $current_month; $i++) { 
	            	// 	// code...
	            	// 	$leave_balance += $leave_per_month;
	            	// }

	            	


	            }

	            $leave_balance += $leave_opening_balance;
	            
	            // $leave_balance += $leave_opening_balance;
					            
	            if($leave->num_rows() > 0)
				{
					foreach($leave->result() as $row_end)
					{
						$leave_type_id2 = $row_end->leave_type_id;
						$leave_duration_status = $row_end->leave_duration_status;
						// var_dump($leave_duration_status); die();
						if(($leave_type_id == $leave_type_id2) && ($leave_duration_status == 1))
						{
							$leave_type_count = $row_end->leave_type_count;
							$days_taken = $row_end->days;
							$start_date = date('jS M Y',strtotime($row_end->start_date));
							$end_date = date('jS M Y',strtotime($row_end->end_date));
							// $days_taken = $this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
							$leave_balance -= $days_taken;
							
						}
					
						
				    }
				}

	            
	        }
	    }
	    $response['leave_balance'] = $leave_balance;
	    $response['days_taken'] = $days_taken;

	    return $response;


	}

	public function get_personnel_leave_duration($leave_duration_id)
	{
		//retrieve all users
		$this->db->from('leave_duration, leave_type');
		$this->db->select('leave_duration.*, leave_type.leave_type_name, leave_type.leave_type_count, leave_type.leave_days');
		$this->db->order_by('leave_type.leave_type_name');
		$this->db->where('leave_duration.leave_type_id = leave_type.leave_type_id AND leave_duration.leave_duration_id = '.$leave_duration_id.$add);
		$query = $this->db->get();

		return $query;

	}

	public function update_leave_status($leave_duration_id,$personnel_id,$leave_duration_status)
	{
		$data = array(
						'leave_duration_status' => $leave_duration_status,
						'leave_duration_note'=> $this->input->post('leave_duration_note'),
						'modified_by'=>$this->session->userdata('personnel_id'),
						'last_modified'=>date('Y-m-d H:i:s')

					 );
		$this->db->where('leave_duration_id', $leave_duration_id);
		
		if($this->db->update('leave_duration', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function check_date_availability($date)
	{



		$add = ' AND start_date = "'.$date.'" ';
		$select_statement = "
                        SELECT
	                            *
	                            FROM (event_duration,event_type)
	                            WHERE event_duration.event_type_id = event_type.event_type_id 
	                            AND event_duration.event_duration_status = 1 ".$add." ORDER BY start_date ASC";
		$query = $this->db->query($select_statement);
		return $query;

	}


	public function check_leave_application($personnel_id,$date)
	{
		// $this->db->where('personnel_id = '.$personnel_id.' AND "'.$date.'" BETWEEN start_date AND end_date  AND leave_duration_status <> 2');
		// $query = $this->db->get('leave_duration');


		$add = ' AND "'.$date.'" BETWEEN start_date AND end_date';
		$select_statement = "
                        SELECT
	                            *
	                            FROM (leave_duration)
	                            WHERE personnel_id = $personnel_id
	                            AND leave_duration_status <> 2 ".$add." ORDER BY start_date ASC";
		$query = $this->db->query($select_statement);
		return $query;



		return $query;
	}


	
}
?>