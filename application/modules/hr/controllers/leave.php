<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/hr/controllers/hr.php";
error_reporting(0);
class Leave extends hr 
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('admin/email_model');
		$this->load->model('planner/planner_model');
	}
	
		function calender()
	{
		// var_dump(d)

		$this->form_validation->set_rules('personnel_id', 'Personnel', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_request_id', 'Leave Type', 'trim|required|xss_clean');
		
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');


		
		if($end_date < $start_date)
		{
			$this->session->set_userdata("error_message","The leave end date cannot be earlier than the start date.");
		}
		
		else if ($this->form_validation->run())
		{
			$leave_type_request_id = $this->input->post('leave_type_request_id');

			$exploded = explode('#',$leave_type_request_id);

			$leave_type_id = $exploded[0];
			$leave_type_count = $exploded[1];

			$personnel_id = $this->input->post('personnel_id');

			$leave_balance = $this->leave_model->get_personnel_leave_balance($personnel_id,$leave_type_id);
		            
		 
			// $start_date = date('jS M Y',strtotime($start_date));
	        // $end_date = date('jS M Y',strtotime($end_date));
	        // $days_taken = $this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
	        // $leave_balance -= $days_taken;

		    $now = strtotime($end_date); // or your date as well
			$your_date = strtotime($start_date);
			$datediff = $now - $your_date;

			$days =  round($datediff / (60 * 60 * 24));

			// var_dump($leave_balance);die();

            if($leave_balance > 0 AND $leave_balance >= $days)
            {

            	if($this->leave_model->add_personnel_leave_request($this->input->post('personnel_id')))
				{

					$personnel_fname = $this->session->userdata('first_name');
			       

					$this->session->set_userdata("success_message", "Leave application successfully pending approval.");

					// send the notifications to the admins
				
					redirect('leave-management/leave-calendar');
				}
				
				else
				{
					$this->session->set_userdata("error_message","Could not apply for leave. Please try again");
				}

            }
            else
            {
            	$this->session->set_userdata("error_message","Sorry the leave days available is only ".$leave_balance);
            }

			
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}


		$v_data['title'] = $data['title'] = 'Leave Schedule';
		
		$v_data['personnel'] = $this->personnel_model->retrieve_personnel("personnel_status = 1 AND (personnel_type_id = 1 OR personnel_type_id = 5)");
		$v_data['leave_types'] = $this->personnel_model->get_leave_types();
		$leave_result = $this->planner_model->get_all_schedule();




		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$event_list = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			
			foreach($result as $res)
			{
				$event_id = $res->event_id;
				$event_name = $res->event_name;
				$personnel_onames = $res->personnel_onames;
				$start_date = $res->start_date;
				$event_type = $res->event_type;
				$start_date = $res->start_date;
				$color_code = 'black';//$res->color_code;
				if($date_of_event != $start_date)
				{
					$r = 0;
				}
				$date_of_event = date('Y-m-d',strtotime($start_date)); 


				$abbreviateName = $this->abbreviateName($event_name);
				if($event_type == 'Event')
					$name_of_event = '<span onclick="edit_event_details('.$event_id.')">'.$abbreviateName.'</span>';
				else
					$name_of_event = '<p style="margin:0px !important">'.$abbreviateName.'</p>';

				$event_list[$date_of_event][$r]['name'] = $name_of_event;
				$event_list[$date_of_event][$r]['color_code'] = $color_code;
				$event_list[$date_of_event][$r]['event_type'] = $event_type;

				$r++;
			}
		}

		$v_data['event_list'] = $event_list;
	
		
		$data['content'] = $this->load->view('leave/calender', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}



	function abbreviateName($whole_name)
	{
	    $alterName = str_word_count($whole_name);
	    
	    if($alterName >= 2)
	    {
	      $nameExploded = explode(' ',$whole_name);
	      
	      $firstName = reset($nameExploded);
	      $lastName = mb_substr(end($nameExploded), 0, 1);
	        
	        return $firstName.'. '.$lastName.'.';
	    }
	  return $whole_name;
	}

	public function leave_schedule()
	{
		$leave_result = $this->leave_model->get_assigned_leave();
		
		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$data = array();
		
		if($leave_result->num_rows() > 0)
		{
			$result = $leave_result->result();
			
			foreach($result as $res)
			{
				$personnel_id = $res->personnel_id;
				$personnel_fname = $res->personnel_fname;
				$personnel_onames = $res->personnel_onames;
				$leave_type_name = $res->leave_type_name;
				$leave_duration_status = $res->leave_duration_status;
				$start_date = $res->start_date;
				$end_date = $res->end_date;

				

				$start_date = date('D M d Y',strtotime($start_date)); 
				$end_date = date('D M d Y',strtotime($end_date)); 
				$time_start = $start_date.' 8:00 AM GMT+0300'; 
				$time_end = $end_date.' 11:59 PM GMT+0300';
				//$color = $this->reception_model->random_color();
				// var_dump($time_end);die();
				if($leave_duration_status == 1)
				{
					$color = '#0088CC';
				}
				
				else
				{
					$color = '#b71c1c';
				}
				$leave_days = $this->site_model->calculate_leave_days($start_date, $end_date);
				
				$data['title'][$r] = $personnel_fname.' '.$personnel_onames.' - '.$leave_type_name.'. '.$leave_days.' days';
				$data['start'][$r] = $time_start;
				$data['end'][$r] = $time_end;
				$data['backgroundColor'][$r] = $color;
				$data['borderColor'][$r] = $color;
				$data['allDay'][$r] = TRUE;
				$data['url'][$r] = site_url().'hr/personnel-leave-detail/'.$personnel_id;
				$r++;
			}
		}
		
		$data['total_events'] = $r;
		echo json_encode($data);
	}
	
	public function view_leave($date)
    {
        $v_data['leave'] = $this->leave_model->get_day_leave($date);
        $v_data['date'] = $date;
		
		$v_data['title'] = $data['title'] = 'Leave starting '.date('jS M Y',strtotime($date));
		
		$v_data['personnel'] = $this->personnel_model->retrieve_personnel();
		$v_data['leave_types'] = $this->personnel_model->get_leave_types();
		
		$data['content'] = $this->load->view('leave/list', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
    }
	
	function add_leave($date, $personnel_id)
	{
		$this->form_validation->set_rules('personnel_id', 'Personnel', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_id', 'Leave Type', 'trim|numeric|required|xss_clean');

		if ($this->form_validation->run())
		{
			if($this->personnel_model->add_personnel_leave($this->input->post('personnel_id')))
			{
				$this->session->set_userdata("success_message", "Personnel leave  added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add personnel leave. Please try again");
			}
			
			redirect('hr/personnel-leave-detail/'.$personnel_id);
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$this->view_leave($date);
		}
	}

	/*
	*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	public function activate_leave($personnel_leave_id, $personnel_id)
	{
		if($this->leave_model->activate_leave_duration($personnel_leave_id))
		{
			$this->session->set_userdata('success_message', 'Leave has been claimed');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Leave could not claimed');
		}
		redirect('hr/personnel-leave-detail/'.$personnel_id);
	}

	/*
	*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	public function deactivate_leave($leave_duration_id, $personnel_id)
	{
		if($this->leave_model->deactivate_leave_duration($leave_duration_id))
		{
			$this->session->set_userdata('success_message', 'Leave has been unclaimed');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Leave could not unclaimed');
		}
		redirect('hr/personnel-leave-detail/'.$personnel_id);
	}

	/*
	*
	*	Delete an existing personnel
	*	@param int $personnel_id
	*
	*/
	public function delete_leave($leave_duration_id, $personnel_id)
	{
		if($this->leave_model->delete_leave_duration($leave_duration_id))
		{
			$this->session->set_userdata('success_message', 'Leave has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Leave could not deleted');
		}
		redirect('hr/personnel-leave-detail/'.$personnel_id);
	}
	
	function add_calender_leave()
	{
		$this->form_validation->set_rules('personnel_id', 'Personnel', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_id', 'Leave Type', 'trim|numeric|required|xss_clean');

		if ($this->form_validation->run())
		{
			if($this->personnel_model->add_personnel_leave($this->input->post('personnel_id')))
			{
				$this->session->set_userdata("success_message", "Personnel leave  added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add personnel leave. Please try again");
			}
			
			redirect('hr/leave');
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$this->calender();
		}
	}
	
	public function leave_application($leave_type_id)
	{
			// var_dump($leave_type_id);die();
		$this->form_validation->set_rules('personnel_id', 'Personnel', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_request_id', 'Leave Type', 'trim|required|xss_clean');
		
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');


		
		if($end_date < $start_date)
		{
			$this->session->set_userdata("error_message","The leave end date cannot be earlier than the start date.");
		}
		
		else if ($this->form_validation->run())
		{
			$leave_type_request_id = $this->input->post('leave_type_request_id');

			$exploded = explode('#',$leave_type_request_id);

			$leave_type_id = $exploded[0];
			$leave_type_count = $exploded[1];

			$personnel_id = $this->input->post('personnel_id');

			$response_leave = $this->leave_model->get_personnel_leave_balance($personnel_id,$leave_type_id);
		    $leave_balance = $response_leave['leave_balance'];
		     // $days = $response_leave['days_taken'];
		    $days = $this->input->post('days');

		    // var_dump($days);die();
		 	
			// $start_date = date('jS M Y',strtotime($start_date));
	        // $end_date = date('jS M Y',strtotime($end_date));
	        // $days_taken = $this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
	        // $leave_balance -= $days_taken;

		     $start_date = date('jS M Y',strtotime($start_date));
			$end_date = date('jS M Y',strtotime($end_date));
			// $days = $this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
			// var_dump($days);die();
		    // $now = strtotime($end_date); // or your date as well
			// $your_date = strtotime($start_date);
			// $datediff = $now - $your_date;

			// $days =  round($datediff / (60 * 60 * 24));

			// var_dump($leave_type_count);die();

            if($leave_balance > 0 AND $leave_balance >= $days)
            {

            	if($this->leave_model->add_personnel_leave_request($this->input->post('personnel_id')))
				{



					$personnel_fname = $this->session->userdata('first_name');
			        $message['subject'] =  $personnel_fname.' '.$leave_type_name.' Leave Request';
					$v_data2['leave_type_name'] = $leave_type_name;
					$v_data2['leave_balance'] = $leave_balance;
					$v_data2['days_taken'] = $days;
					$v_data2['start_date'] = $start_date;
					$v_data2['end_date'] = $end_date;

					$text =  $this->load->view('leave/leave_request_email', $v_data2,true);
				
					$message['text'] =$text;
					$contacts = $this->site_model->get_contacts();
					
					$sender_email = "no-reply@maat-ea.com";//$this->config->item('sender_email');//$contacts['email'];
					$shopping = "";
					$from = $sender_email; 
					
					$button = '';
					$sender['email']= $sender_email; 
					$sender['name'] = $contacts['company_name'];
					$receiver['name'] = 'Admin';

					// $payslip = $title;
					$admin_emails = $contacts['doctor_email'];

					if(empty($admin_emails))
						$admin_emails = 'marttkip@gmail.com';
					else
						$admin_emails .= '/marttkip@gmail.com';


					$sender_email = $sender_email;
					$tenant_email = $admin_emails;
						// $receiver['email'] = 'Admin';
					
					$email_array = explode('/', $tenant_email);
					$total_rows_email = count($email_array);

					for($x = 0; $x < $total_rows_email; $x++)
					{
						$receiver['email'] = $email_tenant = $email_array[$x];
						// var_dump($sender); die();
						$this->email_model->send_sendgrid_mail($receiver, $sender, $message);	
					}


					$this->session->set_userdata("success_message", "Leave application successfully pending approval.");

					// send the notifications to the admins
				
					redirect('my-profile');
				}
				
				else
				{
					$this->session->set_userdata("error_message","Could not apply for leave. Please try again");
				}

            }

			
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		$v_data['title'] = $data['title'] = 'Leave Application';
		$v_data['leave_types'] = $this->personnel_model->get_leave_types();
		$v_data['personnel_id'] = $personnel_id = $this->session->userdata('personnel_id');
		$v_data['selected_leave_type_id'] = $leave_type_id;
		$v_data['leave'] = $this->personnel_model->get_personnel_leave($personnel_id);

	
		$data['content'] = $this->load->view('leave/leave_application', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function add_personnel_leave($personnel_id,$leave_type_id)
	{
		$this->form_validation->set_rules('personnel_id', 'Personnel', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('leave_type_id', 'Leave Type', 'trim|required|xss_clean');
		
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');


		// var_dump($_POST);die();
		if($end_date < $start_date)
		{
			$this->session->set_userdata("error_message","The leave end date cannot be earlier than the start date.");
		}
		
		else if ($this->form_validation->run())
		{
			// $leave_type_request_id = $this->input->post('leave_type_request_id');

			// $exploded = explode('#',$leave_type_request_id);

			// $leave_type_id = $exploded[0];
			// $leave_type_count = $exploded[1];

			$personnel_id = $this->input->post('personnel_id');

			$response_leave = $this->leave_model->get_personnel_leave_balance($personnel_id,$leave_type_id);
		    $leave_balance = $response_leave['leave_balance'];
		     // $days = $response_leave['days_taken'];
		    $days = $this->input->post('days');

		    // var_dump($days);die();
		 	
			// $start_date = date('jS M Y',strtotime($start_date));
	        // $end_date = date('jS M Y',strtotime($end_date));
	        // $days_taken = $this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
	        // $leave_balance -= $days_taken;

		     $start_date = date('jS M Y',strtotime($start_date));
			$end_date = date('jS M Y',strtotime($end_date));
			// $days = $this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
			// var_dump($days);die();
		    // $now = strtotime($end_date); // or your date as well
			// $your_date = strtotime($start_date);
			// $datediff = $now - $your_date;

			// $days =  round($datediff / (60 * 60 * 24));

			// var_dump($leave_type_count);die();

            if($leave_balance > 0 AND $leave_balance >= $days)
            {

            	if($this->leave_model->add_personnel_leave_request($this->input->post('personnel_id')))
				{
					$this->session->set_userdata("success_message", "Leave application successfully pending approval.");

				}
				
				else
				{
					$this->session->set_userdata("error_message","Could not apply for leave. Please try again");
				}

            }

			
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}

		redirect('hr/personnel-leave-detail/'.$personnel_id);
	}
	public function personnel_leaves($personnel_id)
	{
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_request_id', 'Leave Type', 'trim|required|xss_clean');
		
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');


		
		if($end_date < $start_date)
		{
			$this->session->set_userdata("error_message","The leave end date cannot be earlier than the start date.");
		}
		
		else if ($this->form_validation->run())
		{
			$leave_type_request_id = $this->input->post('leave_type_request_id');

			$exploded = explode('#',$leave_type_request_id);

			$leave_type_id = $exploded[0];
			$leave_type_count = $exploded[1];

			$personnel_id = $this->input->post('personnel_id');

			// $leave_balance = $this->leave_model->get_personnel_leave_balance($personnel_id,$leave_type_id);

			$response_leave = $this->leave_model->get_personnel_leave_balance($personnel_id,$leave_type_id);
		    $leave_balance = $response_leave['leave_balance'];
		            
		 
			// $start_date = date('jS M Y',strtotime($start_date));
	        // $end_date = date('jS M Y',strtotime($end_date));
	        // $days_taken = $this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
	        // $leave_balance -= $days_taken;

		    $now = strtotime($end_date); // or your date as well
			$your_date = strtotime($start_date);
			$datediff = $now - $your_date;

			$days =  round($datediff / (60 * 60 * 24));

			// var_dump($leave_balance);die();

            if($leave_balance > 0 AND $leave_balance >= $days)
            {

            	if($this->leave_model->add_personnel_leave_request($this->input->post('personnel_id')))
				{

					$personnel_fname = $this->session->userdata('first_name');
			       

					$this->session->set_userdata("success_message", "Leave application successfully pending approval.");

					// send the notifications to the admins
				
					// redirect('hr/personnel-leave-detail/'.$personnel_id);
				}
				
				else
				{
					$this->session->set_userdata("error_message","Could not apply for leave. Please try again");
				}

            }
            else
            {
            	$this->session->set_userdata("error_message","Sorry the leave days available is only ".$leave_balance);
            }

			
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		$v_data['personnel'] = $this->personnel_model->get_personnel($personnel_id);

		$v_data['leave'] = $this->personnel_model->get_all_personnel_leaves($personnel_id);
		$v_data['leave_types'] = $this->personnel_model->get_leave_types();
		$row = $v_data['personnel']->row();
		$names  = $row->personnel_fname;
		$data['title'] = $names.' Leaves';
		$v_data['title'] = $data['title'];
			// var_dump($v_data['leave']);die();
		$data['content'] = $this->load->view('leave/personnel_leave', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function leave_summaries()
	{

		$v_data['annual_leave_list'] = $this->leave_model->get_leave_types_by_id(4);
		$v_data['leave_types'] = $this->leave_model->get_leave_types();
		
		$data['title'] = $names.' Leaves';
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('leave/leave_summaries', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function leave_calculator()
	{
		$where = 'personnel.personnel_status = 1 AND personnel_type_id = 1';
		$personnel = $this->personnel_model->retrieve_personnel($where);
		$leave_types = $this->personnel_model->get_leave_types(null,1);


		if($personnel->num_rows() > 0)
		{
			foreach($personnel->result() AS $key => $value)
			{
				foreach ($personnel->result() as $row)
				{
					$personnel_fname = $row->personnel_fname;
					$personnel_onames = $row->personnel_onames;
					$personnel_id = $row->personnel_id;
					$engagement_date = $row->engagement_date;
					$current_year = date('Y');

					$month = date('m',strtotime($engagement_date));
					$year = date('Y',strtotime($engagement_date));


					if($current_year > $year)
					{
						// do the full year
						if($leave_types->num_rows() > 0)
						{
							foreach($leave_types->result() as $res)
							{
								$leave_type_id = $res->leave_type_id;
								$leave_type_name = $res->leave_type_name;
								$total_leave = $res->leave_days;




								// insert into leave allocations 

								if($leave_type_id == 2 AND $gender_id == 2)
								{

									
									$array_allocation['personnel_id'] = $personnel_id;
									$array_allocation['leave_type_id'] = $leave_type_id;
									$array_allocation['leave_allocation_status'] = 0;
									$array_allocation['created'] = date('Y-m-d');
									$array_allocation['created_by'] = 0;
									$array_allocation['allocated_days'] = $total_leave;
									$array_allocation['year'] = $current_year;
									$array_allocation['remarks'] = $leave_type_name.' Leave Allocation';

									
									$this->db->where('leave_allocation_status = 0 AND leave_type_id = '.$leave_type_id.' AND personnel_id ='.$personnel_id);
									$query = $this->db->get('leave_allocations');

									if($query->num_rows() == 0)
									{
										// add allocations

										$this->db->insert('leave_allocations',$array_allocation);
									}

								}

								else if($leave_type_id == 1 AND $gender_id == 1)
								{


									$array_allocation['personnel_id'] = $personnel_id;
									$array_allocation['leave_type_id'] = $leave_type_id;
									$array_allocation['leave_allocation_status'] = 0;
									$array_allocation['created'] = date('Y-m-d');
									$array_allocation['created_by'] = 0;
									$array_allocation['allocated_days'] = $total_leave;
									$array_allocation['year'] = $current_year;
									$array_allocation['remarks'] = $leave_type_name.' Leave Allocation';

									
									$this->db->where('leave_allocation_status = 0 AND leave_type_id = '.$leave_type_id.' AND personnel_id ='.$personnel_id);
									$query = $this->db->get('leave_allocations');

									if($query->num_rows() == 0)
									{
										// add allocations

										$this->db->insert('leave_allocations',$array_allocation);
									}

								}
								else
								{



									$array_allocation['personnel_id'] = $personnel_id;
									$array_allocation['leave_type_id'] = $leave_type_id;
									$array_allocation['leave_allocation_status'] = 0;
									$array_allocation['created'] = date('Y-m-d');
									$array_allocation['created_by'] = 0;
									$array_allocation['allocated_days'] = $total_leave;
									$array_allocation['year'] = $current_year;
									$array_allocation['remarks'] = $leave_type_name.' Leave Allocation';

									
									$this->db->where('leave_allocation_status = 0 AND leave_type_id = '.$leave_type_id.' AND personnel_id ='.$personnel_id);
									$query = $this->db->get('leave_allocations');

									if($query->num_rows() == 0)
									{
										// add allocations

										$this->db->insert('leave_allocations',$array_allocation);
									}

								}
								
							}
						}
					}
					else if($current_year == $year)
					{

					

						// use engagement date

						$month = (int)$month;

						// for use to calculate the annual leave
						$remaining_months = 12-$month;
						$remaining_months += 1;
					

						// var_dump($remaining_months);die();
						if($leave_types->num_rows() > 0)
						{
							foreach($leave_types->result() as $res)
							{
								$leave_type_id = $res->leave_type_id;
								$leave_type_name = $res->leave_type_name;
								$total_leave = $res->leave_days;


								if($leave_type_id == 4 AND $remaining_months != 0)
								{
									$total_leave = round($remaining_months*1.75);
								}
								else if($leave_type_id == 4 AND $remaining_months == 0)
								{
									$total_leave = $total_leave;
								}

								if($leave_type_id == 2 AND $gender_id == 2)
								{

									
									$array_allocation['personnel_id'] = $personnel_id;
									$array_allocation['leave_type_id'] = $leave_type_id;
									$array_allocation['leave_allocation_status'] = 0;
									$array_allocation['created'] = date('Y-m-d');
									$array_allocation['created_by'] = 0;
									$array_allocation['allocated_days'] = $total_leave;
									$array_allocation['year'] = $current_year;
									$array_allocation['remarks'] = $leave_type_name.' Leave Allocation';

									
									$this->db->where('leave_allocation_status = 0 AND leave_type_id = '.$leave_type_id.' AND personnel_id ='.$personnel_id);
									$query = $this->db->get('leave_allocations');

									if($query->num_rows() == 0)
									{
										// add allocations

										$this->db->insert('leave_allocations',$array_allocation);
									}

								}

								else if($leave_type_id == 1 AND $gender_id == 1)
								{


									$array_allocation['personnel_id'] = $personnel_id;
									$array_allocation['leave_type_id'] = $leave_type_id;
									$array_allocation['leave_allocation_status'] = 0;
									$array_allocation['created'] = date('Y-m-d');
									$array_allocation['created_by'] = 0;
									$array_allocation['allocated_days'] = $total_leave;
									$array_allocation['year'] = $current_year;
									$array_allocation['remarks'] = $leave_type_name.' Leave Allocation';

									
									$this->db->where('leave_allocation_status = 0 AND leave_type_id = '.$leave_type_id.' AND personnel_id ='.$personnel_id);
									$query = $this->db->get('leave_allocations');

									if($query->num_rows() == 0)
									{
										// add allocations

										$this->db->insert('leave_allocations',$array_allocation);
									}

								}
								else
								{



									$array_allocation['personnel_id'] = $personnel_id;
									$array_allocation['leave_type_id'] = $leave_type_id;
									$array_allocation['leave_allocation_status'] = 0;
									$array_allocation['created'] = date('Y-m-d');
									$array_allocation['created_by'] = 0;
									$array_allocation['allocated_days'] = $total_leave;
									$array_allocation['year'] = $current_year;
									$array_allocation['remarks'] = $leave_type_name.' Leave Allocation';

									
									$this->db->where('leave_allocation_status = 0 AND leave_type_id = '.$leave_type_id.' AND personnel_id ='.$personnel_id);
									$query = $this->db->get('leave_allocations');

									if($query->num_rows() == 0)
									{
										// add allocations

										$this->db->insert('leave_allocations',$array_allocation);
									}

								}
								// insert into leave allocations 

								
							}
						}

					}

				}
			}
		}
	}
	public function update_opening_leave_days($personnel_id,$leave_type_id)
	{
		$opening_leave_days = $this->input->post('opening_leave_days');

		$this->db->where('personnel_id = '.$personnel_id.' AND year = '.date('Y').' AND leave_type_id = '.$leave_type_id);
		$query = $this->db->get('personnel_leave_opening_balance');

		if($query->num_rows() > 0)
		{
			// update the leave days

			$array_update['days'] = $opening_leave_days;
			$array_update['leave_type_id'] = $leave_type_id;
			$array_update['personnel_id'] = $personnel_id;
			$this->db->where('personnel_id = '.$personnel_id.' AND year = '.date('Y').' AND leave_type_id = '.$leave_type_id);
			$this->db->update('personnel_leave_opening_balance',$array_update);
		}
		else
		{
			// insert new leave days

			$array['days'] = $opening_leave_days;
			$array['created'] = date('Y-m-d');
			$array['created_by'] = $this->session->userdata('personnel_id');
			$array['leave_type_id'] = $leave_type_id;
			$array['personnel_id'] = $personnel_id;
			$array['year'] = date('Y');
			$array['start_date'] = date('Y-01-01');

			$this->db->insert('personnel_leave_opening_balance',$array);

		}

		$response['message'] = 'success';
		$response['result'] = 'You have successfully updated the leave opening balances';

		echo json_encode($response);
	}

	public function check_leave_balance($personnel_id,$leave_type_id)
	{
		$leave_balance = $this->leave_model->get_personnel_leave_balance($personnel_id,$leave_type_id);
		$leave_bal = $leave_balance['leave_balance'];
		$days_taken = $leave_balance['days_taken'];
		$response['message'] = 'success';
		$response['result'] = '<h4> Balance : '.$leave_bal.'</h4>';
		echo json_encode($response);

	}

	public function leave_consent_request($leave_duration_id,$personnel_id,$status)
	{
		$data['status'] = $status;
		$data['leave_duration_id'] = $leave_duration_id;
		$data['personnel_id'] = $personnel_id;
		$results = $this->load->view('leave/leave_consent_form', $data,true);

		// echo json_encode($data);
		echo $results;
	}
	public function update_leave_consent_status($personnel_id,$leave_duration_id,$status)
	{
		
		$leave_duration_note = $this->input->post('leave_duration_note');
		if($this->leave_model->update_leave_status($leave_duration_id,$personnel_id,$status))
		{
				$leave_details = $this->leave_model->get_personnel_leave_duration($leave_duration_id);

				$leave_duration_note = '';
				$days_taken = 0;
				if($leave_details->num_rows() > 0)
				{
					foreach ($leave_details->result() as $key => $value) {
						// code...

						$leave_duration_note = $value->leave_duration_note;
						$start_date = $value->start_date;
						$end_date = $value->end_date;
						$leave_type_name = $value->leave_type_name;
						$leave_type_id = $value->leave_type_id;
						$days_taken = $value->days;

					}
				}




				$personnel = $this->personnel_model->get_personnel($personnel_id);


				$row = $personnel->row();

				// var_dump($row) or die();
				$personnel_onames = $row->personnel_onames;
				$personnel_fname = $row->personnel_fname;
				$personnel_dob = $row->personnel_dob;
				$personnel_email = $row->personnel_email;
				$personnel_phone = $row->personnel_phone;
				$personnel_address = $row->personnel_address;
				$civil_status_id = $row->civilstatus_id;
				$personnel_locality = $row->personnel_locality;
				$title_id = $row->title_id;
				$gender_id = $row->gender_id;
				$personnel_username = $row->personnel_username;
				$personnel_kin_fname = $row->personnel_kin_fname;
				$personnel_kin_onames = $row->personnel_kin_onames;
				$personnel_kin_contact = $row->personnel_kin_contact;
				$personnel_kin_address = $row->personnel_kin_address;
				$kin_relationship_id = $row->kin_relationship_id;
				$job_title_idd = $row->job_title_id;
				$staff_id = $row->personnel_staff_id;
				$personnel_id = $row->personnel_id;
				$engagement_date = $row->engagement_date;
				$response_leave = $this->leave_model->get_personnel_leave_balance($personnel_id,$leave_type_id);
		   		$leave_balance = $response_leave['leave_balance'];

				$start_date = date('jS M Y',strtotime($start_date));
		        $end_date = date('jS M Y',strtotime($end_date));
		        $days_taken = $days_taken;//$this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);


				$v_data2['leave_type_name'] = $leave_type_name;
				$v_data2['leave_balance'] = $leave_balance;
				$v_data2['days_taken'] = $days_taken;
				$v_data2['start_date'] = $start_date;
				$v_data2['end_date'] = $end_date;
				$v_data2['leave_duration_note'] = $leave_duration_note;





				if($status == 1)
				{
					// approved leave
					$subject = $leave_type_name.' Leave application for '.$start_date.' - '.$end_date.' as been approved ';
				}
				else if($status == 2)
				{
					// leave declined
					$subject = $leave_type_name.' Leave application for '.$start_date.' - '.$end_date.' as been declined ';
				}

		        $message['subject'] =  $subject;
			

				$text =  $this->load->view('leave/leave_notification_mail', $v_data2,true);
			
				$message['text'] =$text;
				$contacts = $this->site_model->get_contacts();
				$sender_email = "no-reply@maat-ea.com";//$this->config->item('sender_email');//$contacts['email'];
				$shopping = "";
				$from = $sender_email; 
				
				$button = '';
				$sender['email']= $sender_email; 
				$sender['name'] = $contacts['company_name'];
				$receiver['name'] = $personnel_fname;
				$admin_emails = $contacts['doctor_email'];

				if(empty($admin_emails))
					$admin_emails = 'marttkip@gmail.com';
				// $payslip = $title;



				$sender_email = $sender_email;
				$tenant_email = $this->input->post('personnel_email');
					// $receiver['email'] = 'Admin';
				
				$email_array = explode('/', $tenant_email);
				$total_rows_email = count($email_array);

				for($x = 0; $x < $total_rows_email; $x++)
				{
					$receiver['email'] = $email_tenant = $email_array[$x];
					// var_dump($sender); die();
					$this->email_model->send_sendgrid_mail($receiver, $sender, $message);	
				}

				$response['message'] = 'success';
				$response['result'] = 'You have successfully updated the leave';

				$this->session->set_userdata("success_message", "Leave application successfully pending approval.");

		}
		else
		{
			$response['message'] = 'fail';
			$response['result'] = 'Sorry Something went wrong please try again';
		}

		echo json_encode($response);
	}
	
	public function get_leave_schedule($leave_type_id)
	{
		$days = $this->input->post('days');
		$start_date = $this->input->post('start_date');
		$data['start_date'] = $start_date;
		$data['leave_type_id'] = $leave_type_id;
		$data['personnel_id'] = $personnel_id = $this->input->post('personnel_id');//$this->session->userdata('personnel_id');

		$leave_balance = $this->leave_model->get_personnel_leave_balance($personnel_id,$leave_type_id);

		
		$result = '';
		$dateVal = $start_date;
		 $days = $days;

		 if($days >= 1)
		 {


			for ($i=1; $i <= $days; $i++) { 
				// code...
				//Assign a date as a string
				// if($i == 0)
				// {
					$dateVal = date('d-M-Y', strtotime($dateVal));
				// }
				// else
				// {
				// }

				$date_to_check = $dateVal = date('Y-m-d', strtotime($dateVal));
				
				$check_leave_application_rs = $this->leave_model->check_leave_application($personnel_id,$date_to_check);

				$checked_rs = $check_leave_application_rs->num_rows();
				$check_availability_rs = $this->leave_model->check_date_availability($date_to_check);

				$date_to_check_sun = $dateVal = date('Y-m-d', strtotime($dateVal));

						
				$dt1 = strtotime($date_to_check_sun);
		        $dt2 = date("l", $dt1);
		        $dt3 = strtolower($dt2);

		        $personnel_rs = $this->personnel_model->get_personnel($personnel_id);

				if($personnel_rs->num_rows() > 0)
				{
					foreach ($personnel_rs->result() as $key => $value) {
						// code...
						$personnel_type_id = $value->personnel_type_id;
					}
				}

		        $checked = false;
		        if($personnel_type_id == 5)
		        	if(($dt3 == "saturday"))
		        		$checked = true;


				if(($check_availability_rs->num_rows() > 0) OR $checked_rs > 0 OR ($dt3 == "sunday") OR $checked )
				{
					// status unavailable

					// if($check_availability_rs)
					$status = 'Not Available';
					$i--;
				}
				else
				{
					$status = 'Available';
				}
				$result .= '<tr>
								<td>'.$dateVal.'</td>
								<td>'.$status.'</td>
							</tr>';

				
				$dateVal = date('d-M-Y', strtotime($dateVal. ' + 1 days'));


			}
		}
		else
		{

			$date_to_check = $dateVal = date('Y-m-d', strtotime($start_date));
			// var_dump($date_to_check);die();
			$check_leave_application_rs = $this->leave_model->check_leave_application($personnel_id,$date_to_check);

			$checked_rs = $check_leave_application_rs->num_rows();
			$check_availability_rs = $this->leave_model->check_date_availability($date_to_check);

			$date_to_check_sun = $dateVal = date('Y-m-d', strtotime($dateVal));

					
			$dt1 = strtotime($date_to_check_sun);
	        $dt2 = date("l", $dt1);
	        $dt3 = strtolower($dt2);

	        $personnel_rs = $this->personnel_model->get_personnel($personnel_id);

			if($personnel_rs->num_rows() > 0)
			{
				foreach ($personnel_rs->result() as $key => $value) {
					// code...
					$personnel_type_id = $value->personnel_type_id;
				}
			}

	        $checked = false;
	        if($personnel_type_id == 5)
	        	if(($dt3 == "saturday"))
	        		$checked = true;

		    if(($check_availability_rs->num_rows() > 0) OR $checked_rs > 0 OR ($dt3 == "sunday") OR $checked )
			{
				// status unavailable

				// if($check_availability_rs)
				$status = 'Not Available';
				$i--;
			}
			else
			{
				$status = 'Available';
			}

			$result .= '<tr>
								<td>'.$dateVal.'</td>
								<td>'.$status.'</td>
							</tr>';
		}

		if(!empty($dateVal))
		{
			$end_date = date('Y-m-d', strtotime($dateVal));
		}

		$data['result'] = $result;

		$results = $this->load->view('leave/leave_schedule', $data,true);

		if($leave_balance > 0)
		{
			$response['message'] = 'success';
			$response['results'] = $results;
			$response['end_date'] = $end_date;
		}
		else
		{
			$response['message'] = 'fail';
			$response['results'] = '<tr>
										<td colspan="2"><div class="alert alert-xs alert-warning col-md-12">Sorry Seems like you have exhausted the leave days</div></td>
									</tr>';
			$response['end_date'] = '';
		}
	
		// echo json_encode($data);
		echo json_encode($response);
	}
}
?>