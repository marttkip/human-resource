<?php
$contacts = $this->site_model->get_contacts();
$logo = $contacts['logo'];
?>
<?php
$success = $this->session->userdata('success_message');

if(!empty($success))
{
    echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
    $this->session->unset_userdata('success_message');
}

$error = $this->session->userdata('error_message');

if(!empty($error))
{
    echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
    $this->session->unset_userdata('error_message');
}


$personnel_id = $this->session->userdata('personnel_id');



if($personnel_details->num_rows() > 0)
{
	foreach($personnel_details->result() AS $key => $value)
	{
		$personnel_onames = $value->personnel_onames;
		$personnel_fname = $value->personnel_fname;
		$personnel_phone = $value->personnel_phone;
		$personnel_email = $value->personnel_email;
		$staff_id = $value->staff_id;
		$engagement_date = $value->engagement_date;
	}
}

if(empty($engagement_date) OR $engagement_date == '0000-00-00')
{
	$engagement_date = date('Y-01-01');
}
?>

	<div class="row" >
		<div class="col-md-2" >
			<div style="height:100vh;overflow-y:scroll;">

				<section class="card">
					<div class="card-body">
						<div class="thumb-info mb-3">
							<img src="https://via.placeholder.com/150" class="rounded img-fluid" alt="<?php echo $personnel_fname;?> ">
							<div class="thumb-info-title">
								<span class="thumb-info-inner"> <?php echo $personnel_fname;?> <?php echo $personnel_fname;?></span>
								<span class="thumb-info-type"> <?php echo $staff_id;?></span>
							</div>
						</div>

						<div class="widget-toggle-expand mb-3">
							
							<div class="widget-content-expanded">
								<ul class="mt-3" style="list-style: none;padding: 0px !important;">
									<li><strong>Phone: </strong><br>  <?php echo $personnel_phone;?></li>
									<li><strong>Email: </strong><br> <?php echo $personnel_email;?></li>

								</ul>
							</div>
						</div>


					</div>
				</section>
				
			</div>
			

		<!-- 	<hr class="dotted short">
			<h4 class="mb-3 mt-0">Personal Stats</h4>
			<ul class="simple-card-list mb-3">
				<li class="primary">
					<h4><?php echo $total_appointments?></h4>
					<p class="text-light">Appointments.</p>
				</li>
				<li class="warning">
					<h4>Kes. <?php echo number_format(0,2);?></h4>
					<p class="text-light">Invoice</p>
				</li>
				<li class="info">
					<h4>Kes. <?php echo number_format(0,2);?></h4>
					<p class="text-light">Payments</p>
				</li>
				<li class="danger">
					<h3>Kes. <?php echo number_format(0,2);?></h3>
					<p class="text-light">Balance</p>
				</li>
			</ul> -->
			
		</div>
		<div class="col-md-6" style="height:100vh;overflow-y:scroll;">


			 <div class="panel-body" style="height:100vh;overflow-y:scroll;">
		    <?php
		            $success = $this->session->userdata('success_message');
		            $error = $this->session->userdata('error_message');
		            
		            if(!empty($success))
		            {
		                echo '
		                    <div class="alert alert-success">'.$success.'</div>
		                ';
		                
		                $this->session->unset_userdata('success_message');
		            }
		            
		            if(!empty($error))
		            {
		                echo '
		                    <div class="alert alert-danger">'.$error.'</div>
		                ';
		                
		                $this->session->unset_userdata('error_message');
		            }
		            
		        ?>
		        <div class="col-md-12">
		           <h5><strong>Change Password</strong></h5>
	                <form enctype="multipart/form-data" action="<?php echo base_url();?>change-password"  id = "change_password" method="post">
	                    <div class="row">
	                        <div class="col-sm-10">
	                            <div class="row">
	                                <div class="form-group">
	                                    <label class="col-md-4 control-label" for="slideshow_name"> <strong>Current Password </strong></label>
	                                    <div class="col-md-8">
	                                        <input type="password" class="form-control" name="current_password" placeholder="Current Password" autocomplete="off">
	                                    </div>
	                                </div>
	                            </div>
	                            <br>
	                            <div class="row">
	                                <div class="form-group">
	                                    <label class="col-md-4 control-label" for="slideshow_name"><strong>New Password</strong></label>
	                                    <div class="col-md-8">
	                                        <input type="password" class="form-control" name="new_password" placeholder="New Password" autocomplete="off">
	                                    </div>
	                                </div>
	                            </div>
	                            <br>
	                            <div class="row">
	                                <div class="form-group">
	                                    <label class="col-md-4 control-label" for="slideshow_name"><strong>Confirm New Password</strong></label>
	                                    <div class="col-md-8">
	                                        <input type="password" class="form-control" name="confirm_new_password" placeholder="Confirm new password" autocomplete="off">
	                                    </div>
	                                </div>
	                            </div>
	                            <br>
	                            <div class="row">
	                                    <div class="form-group center-align">
	                                        <button type="submit" class="btn btn-sm btn-success" name="submit" >Change Password</button>
	                                    </div>
	                                
	                            </div>
	                                
	                        </div>
	                        
	                    </div>
	                </form>
		        </div>
        	</div>
		</div>
		<div class="col-md-4" >
			
			    <div class="panel-body" style="height:100vh;overflow-y:scroll;">
			    	<h4> LEAVE DAYS </h4>
					<table class="table table-condensed table-bordered table-hover table-striped">
						<thead>
							<th>LEAVE TYPE</th>
							<th>REMAINING DAYS</th>
							<th>ACTION</th>
							
						</thead>
						<tbody>
						<?php 
						$personnel_id = $this->session->userdata('personnel_id');
					    if($personnel_id == 0)
					    {
					    	$engagement_date = date('2023-01-01');
					    }
					    if(empty($engagement_date))
					    	$engagement_date = date('Y-m-d');

					    $engagement_array = explode('-', $engagement_date);
					    $engagement_year = $engagement_array[0];
					    $engagement_month = (int)$engagement_array[1];
					    $engagement_day = $engagement_array[2];
					    // var_dump($personnel_id);die();
					    if($personnel_query->num_rows() > 0)
					    {
					        $row = $personnel_query->row();
					        $gender_id = $row->gender_id;
					    }
					    
					    else
					    {
					        $gender_id = 0;
					    }
					    
					    if($leave_types->num_rows() > 0)
					    {
					        foreach($leave_types->result() as $res)
					        {
					            $leave_type_id = $res->leave_type_id;
					            $leave_type_count = $res->leave_type_count;
					            $leave_type_name = strtoupper($res->leave_type_name);
					            $leave_days = $res->leave_days;
					            // devide accounting to the month

					            $leave_balance = 0;
					            $leave_opening_balance = $this->leave_model->personnel_leave_opening_balance($personnel_id,$leave_type_id);

					            $leave_balance = $leave_days;
					            if($leave_type_count == 3)
					            {	

					            	$leave_balance = 0;
					            	// total leave 21
					            	// leave per 1.75 *5
					            	$leave_per_month = $leave_days/12;
					            		// get the current month

					            	$current_month = (int)date('m');


					            	if($engagement_year == date('Y'))
					            	{
					            		// calculate the remaining leave days for the annual leave


					            		$total_months = $current_month - $engagement_month;
					            		if($current_month == $engagement_month)
						            	{
						            		$total_months = 1;
						            	}
						            	// var_dump($leave_per_month);die();
					            		$leave_balance = ($total_months*$leave_per_month);


					            	}
					            	// get avaulable leave days 
					            	// var_dump($leave_balance);die();
					            	// for ($i=1; $i <= $current_month; $i++) { 
					            	// 	// code...
					            	// 	$leave_balance += $leave_per_month;
					            	// }

					            	
					            	// $leave_balance += $leave_opening_balance;
					            		
				

					            }

					            $leave = $this->personnel_model->get_leave_balance($personnel_id,$leave_type_id);
					            if($leave->num_rows() > 0)
					            {
					                foreach($leave->result() as $row)
					                {
					                    $leave_type_id2 = $row->leave_type_id;
					                    $leave_duration_status = $row->leave_duration_status;
					                    
					                    if(($leave_type_id == $leave_type_id2) && ($leave_duration_status == 1))
					                    {
					                        $leave_type_count = $row->leave_type_count;
					                      	$days = $row->days;
					                        $start_date = date('jS M Y',strtotime($row->start_date));
					                        $end_date = date('jS M Y',strtotime($row->end_date));
					                        $days_taken = $days;//$this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
					                        $leave_balance -= $days_taken;
					                    }
					                }
					            }
					            
					            //maternity & femail
					            if(($leave_type_id == 2) && ($gender_id == 2))
					            {
					                echo '
							                <tr>
							                	<td>'.$leave_type_name.'</td>
							                	<td> '.$leave_balance.'</td>
							                	<td><a class="text-muted text-uppercase" href="'.site_url().'apply-leave/'.$leave_type_id.'">(apply)</a></td>
							               	</tr>
					                ';
					            }
					            
					            //paternity & male
					            else if(($leave_type_id == 1) && ($gender_id == 1))
					            {
					                echo '
					                		<tr>
							                	<td>'.$leave_type_name.'</td>
							                	<td> '.$leave_balance.'</td>
							                	<td><a class="text-muted text-uppercase" href="'.site_url().'apply-leave/'.$leave_type_id.'">(apply)</a></td>
							               	</tr>
					                ';
					            }


					            
					            else if($leave_type_id > 2)
					            {
					                echo '
					                 		<tr>
							                	<td>'.$leave_type_name.'</td>
							                	<td> '.$leave_balance.'</td>
							                	<td><a class="text-muted text-uppercase" href="'.site_url().'apply-leave/'.$leave_type_id.'">(apply)</a></td>
							               	</tr>
					                ';
					            }
					        }
					    }
					    ?>
						</tbody>
					</table>

					<?php

					$result ='';
					$personnel_id = $this->session->userdata('personnel_id');
					$leave = $this->personnel_model->get_personnel_leave_days($personnel_id);
					if($leave->num_rows() > 0)
					{
						$count = 0;
							
						$result .= 
						'
						
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>#</th>
									<th>Start date</a></th>
									<th>End date</a></th>
									<th>Type</a></th>
									<th>Days</a></th>
									<th>Status</a></th>
								</tr>
							</thead>
							  <tbody>
							  
						';
						
						foreach ($leave->result() as $row)
						{
							
							$leave_duration_id = $row->leave_duration_id;
							$leave_type_name = $row->leave_type_name;
							$leave_duration_status = $row->leave_duration_status;
							$days = $row->days;
							$start_date = date('jS M Y',strtotime($row->start_date));
							$end_date = date('jS M Y',strtotime($row->end_date));
							
							//create deactivated status display
							if($leave_duration_status == 0)
							{
								$status = '<span class="label label-danger">Unclaimed</span>';
								$button = '<a href="'.site_url().'human-resource/delete-personnel-leave/'.$leave_duration_id.'/'.$personnel_id.'" class="btn btn-xs btn-danger" onclick="return confirm(\'Do you really want to delete?\');" title="Delete"><i class="fa fa-trash"></i></a>';
							}
							//create activated status display
							else if($leave_duration_status == 1)
							{
								$status = '<span class="label label-success">Claimed</span>';
								$button = '';
							}
							
							$count++;
							$result .= 
							'
								<tr>
									<td>'.$count.'</td>
									<td>'.$start_date.'</td>
									<td>'.$end_date.'</td>
									<td>'.$leave_type_name.'</td>
									<td>'.$days.'</td>
									<td>'.$status.'</td>
									<td>'.$button.'</td>
								
								</tr> 
							';
						}
						
						$result .= 
						'
									  </tbody>
									</table>
						';
					}
					
					else
					{
						$result = "<p>No leave have been assigned</p>";
					}
	
					?>
		
			    	<h4> Leave Request Status </h4>
			    	<br>
			    	<?php echo $result;?>
					
			
					    
					    
				
			    </div>

			   
		
		</div>

		
	</div>





<?php //echo $this->load->view('calendar', '', TRUE);?>





    