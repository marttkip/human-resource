<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Leave Request</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				/*border:solid #000 !important;*/
				/*border-width:1px 0 0 1px !important;*/
				font-size:12px;
				margin-top:10px;
			}
			.col-md-6 {
			    width: 50%;
			 }
			.row .col-md-12 th, .row .col-md-12 td {
				/*border:solid #000 !important;*/
				/*border-width:0 1px 1px 0 !important;*/
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 /*padding: 2px;*/
				 padding: 2px;
			}
			h3
			{
				font-size: 30px;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{ margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">

		<p>Good evening Admin,</p>
		<p>I would like to request for leave as follows:</p>

		<h5 style="text-decoration:underline"><strong>LEAVE REQUEST</strong></h5>
		<table  class="table table-hover table-bordered ">
	


			</tbody>
		  		<tr >
					<th style="text-align:left;">LEAVE TYPE</th>
					<td style="text-align:left;"><?php echo $leave_type_name;?></td>
				</tr>
				<tr >
					<th style="text-align:left;">REMAINING DAYS</th>
					<td style="text-align:left;"><?php echo ($leave_balance);?></td>
				</tr>
				<tr >
					<th style="text-align:left;">REQUEST DATES</th>
					<td style="text-align:left;"><?php echo $start_date.' - '.$end_date;?></td>
				</tr>
				<tr >
					<th style="text-align:left;">NO OF DAYS REQUESTED</th>
					<td style="text-align:left;"><?php echo $days_taken;?></td>
				</tr>
				<tr >
					<th style="text-align:left;">NO OF DAYS REMAINING AFTER ALLOWED</th>
					<th style="text-align:left;"><?php echo $leave_balance-$days_taken;?></th>
				</tr>
	  		</tbody>

		</table>


	</body>
</html>
