<?php
//personnel data
$row = $personnel->row();

// var_dump($row) or die();
$personnel_onames = $row->personnel_onames;
$personnel_fname = $row->personnel_fname;
$personnel_dob = $row->personnel_dob;
$personnel_email = $row->personnel_email;
$personnel_phone = $row->personnel_phone;
$personnel_address = $row->personnel_address;
$civil_status_id = $row->civilstatus_id;
$personnel_locality = $row->personnel_locality;
$title_id = $row->title_id;
$gender_id = $row->gender_id;
$personnel_username = $row->personnel_username;
$personnel_kin_fname = $row->personnel_kin_fname;
$personnel_kin_onames = $row->personnel_kin_onames;
$personnel_kin_contact = $row->personnel_kin_contact;
$personnel_kin_address = $row->personnel_kin_address;
$kin_relationship_id = $row->kin_relationship_id;
$job_title_idd = $row->job_title_id;
$staff_id = $row->personnel_staff_id;
$personnel_id = $row->personnel_id;
$engagement_date = $row->engagement_date;

if(empty($engagement_date) OR $engagement_date == '0000-00-00')
{
	$engagement_date = date('Y-01-01');
}

$engagement_array = explode('-', $engagement_date);
$engagement_year = $engagement_array[0];
$engagement_month = (int)$engagement_array[1];
$engagement_day = $engagement_array[2];

//echo $gender_id;
//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$personnel_onames =set_value('personnel_onames');
	$personnel_fname =set_value('personnel_fname');
	$personnel_dob =set_value('personnel_dob');
	$personnel_email =set_value('personnel_email');
	$personnel_phone =set_value('personnel_phone');
	$personnel_address =set_value('personnel_address');
	$civil_status_id =set_value('civil_status_id');
	$personnel_locality =set_value('personnel_locality');
	$title_id =set_value('title_id');
	$gender_id =set_value('gender_id');
	$personnel_username =set_value('personnel_username');
	$personnel_kin_fname =set_value('personnel_kin_fname');
	$personnel_kin_onames =set_value('personnel_kin_onames');
	$personnel_kin_contact =set_value('personnel_kin_contact');
	$personnel_kin_address =set_value('personnel_kin_address');
	$kin_relationship_id =set_value('kin_relationship_id');
	$job_title_id =set_value('job_title_id');
	$staff_id =set_value('staff_id');
}
	$result ='';
	if($leave->num_rows() > 0)
	{
		$count = 0;
			
		$result .= 
		'
		<br/>
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>Type</a></th>
					<th>Start date</a></th>
					<th>End date</a></th>
					<th>Days</a></th>
					<th>Status</a></th>
					<th colspan="5">Actions</th>
				</tr>
			</thead>
			  <tbody>
			  
		';
		
		foreach ($leave->result() as $row)
		{
			$leave_days = $row->leave_days;
			$leave_type_name = $row->leave_type_name;
			$leave_duration_status = $row->leave_duration_status;
			$leave_duration_id = $row->leave_duration_id;
			$leave_type_count = $row->leave_type_count;
			$days = $row->days;
			$start_date = date('jS M Y',strtotime($row->start_date));
			$end_date = date('jS M Y',strtotime($row->end_date));
			$days_taken = $this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
			
			//create deactivated status display
			if($leave_duration_status == 0)
			{
				$status = '<span class="label label-danger">Unclaimed</span>';
				// $button = '<a class="btn btn-sm btn-info" href="'.site_url().'hr/activate-leave/'.$leave_duration_id.'/'.$personnel_id.'/1" onclick="return confirm(\'Do you want to activate '.$start_date.' Leave?\');" title="Activate '.$start_date.' Leave"><i class="fa fa-thumbs-up"></i></a>';


				$button = '<td><a class="btn btn-sm btn-success" onclick="approve_leave('.$leave_duration_id.','.$personnel_id.',1)" ><i class="fa fa-thumbs-up"></i> Approve leave</a></td>';
				$button .= '<td><a class="btn btn-sm btn-danger" onclick="approve_leave('.$leave_duration_id.','.$personnel_id.',2)" title="Decline '.$start_date.' Leave"><i class="fa fa-thumbs-up"></i> Decline Leave Request</a></td>';

			}
			//create activated status display
			else if($leave_duration_status == 1)
			{
				$status = '<span class="label label-success">Claimed</span>';
				// $button = '<a class="btn btn-sm btn-default" href="'.site_url().'hr/deactivate-leave/'.$leave_duration_id.'/'.$personnel_id.'/1" onclick="return confirm(\'Do you want to deactivate '.$start_date.' Leave?\');" title="Deactivate '.$start_date.' Leave"><i class="fa fa-thumbs-down"></i></a>';

				// $button = '<a class="btn btn-sm btn-default" href="'.site_url().'hr/deactivate-leave/'.$leave_duration_id.'/'.$personnel_id.'/1" onclick="return confirm(\'Do you want to deactivate '.$start_date.' Leave?\');" title="Deactivate '.$start_date.' Leave"><i class="fa fa-thumbs-down"></i></a>';
			}

			
			
			$count++;
			$result .= 
			'
				<tr>
					<td>'.$count.'</td>
					<td>'.$leave_type_name.'</td>
					<td>'.$start_date.'</td>
					<td>'.$end_date.'</td>
					<td>'.$days.'</td>
					<td>'.$status.'</td>
					'.$button.'
					<td><a href="'.site_url().'human-resource/delete-personnel-leave/'.$leave_duration_id.'/'.$personnel_id.'/1" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete?\');" title="Delete"><i class="fa fa-trash"></i></a></td>
				</tr> 
			';
		}
		
		$result .= 
		'
					  </tbody>
					</table>
		';
	}
	
	else
	{
		$result = "<p>No leave have been assigned</p>";
	}
	

//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$start_date = set_value('start_date');
	$end_date = set_value('end_date');
	$leave_type_id = set_value('leave_type_id');
}

else
{
	$start_date = '';
	$end_date = '';
	$leave_type_id = '';
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-4">


    			<input type="hidden" name="personnel_id" id="personnel_id" value="<?php echo $personnel_id?>">
			<h2 class="panel-title"><?php echo $personnel_fname.' '.$personnel_onames;?> Leave Balance</h2>
			<div class="panel-body">
				<table class="table table-condensed table-bordered table-hover table-striped">
					<thead>
						<th>LEAVE TYPE</th>
						<th>REMAINING DAYS</th>
						
					</thead>
					<tbody>
					<?php 
					// $personnel_id = $this->session->userdata('personnel_id');
				    // if($personnel_id == 0)
				    // {
				    // 	$engagement_date = date('2023-01-01');
				    // }
				     if(empty($engagement_date))
					    	$engagement_date = date('Y-m-d');


				    $engagement_array = explode('-', $engagement_date);
					    $engagement_year = $engagement_array[0];
					    $engagement_month = (int)$engagement_array[1];
					    $engagement_day = $engagement_array[2];
					    // var_dump($personnel_id);die();
					    
					    if($leave_types->num_rows() > 0)
					    {
					        foreach($leave_types->result() as $res)
					        {
					            $leave_type_id = $res->leave_type_id;
					            $leave_type_count = $res->leave_type_count;
					            $leave_type_name = strtoupper($res->leave_type_name);
					            $leave_days = $res->leave_days;
					            // devide accounting to the month

					            $leave_balance = 0;
					            $leave_opening_balance = $this->leave_model->personnel_leave_opening_balance($personnel_id,$leave_type_id);

					            $leave_balance = $leave_days;

					            if($leave_type_count == 3)
					            {	

					            	$leave_balance = 0;
					            	// total leave 21
					            	// leave per 1.75 *5
					            	$leave_per_month = $leave_days/12;
					            		// get the current month

					            	$current_month = (int)date('m');


					            	if($engagement_year == date('Y'))
					            	{
					            		// calculate the remaining leave days for the annual leave


					            		$total_months = $current_month - $engagement_month;
					            		if($current_month == $engagement_month)
						            	{
						            		$total_months = 1;
						            	}
						            	// var_dump($leave_per_month);die();
					            		$leave_balance = ($total_months*$leave_per_month);


					            	}
					            	// get avaulable leave days 
					            	// var_dump($leave_balance);die();
					            	// for ($i=1; $i <= $current_month; $i++) { 
					            	// 	// code...
					            	// 	$leave_balance += $leave_per_month;
					            	// }

					            	
					            	// $leave_balance += $leave_opening_balance;
					            		
				

					            }

					            $leave = $this->personnel_model->get_leave_balance($personnel_id,$leave_type_id);
					            if($leave->num_rows() > 0)
					            {
					                foreach($leave->result() as $row)
					                {
					                    $leave_type_id2 = $row->leave_type_id;
					                    $leave_duration_status = $row->leave_duration_status;
					                    
					                    if(($leave_type_id == $leave_type_id2) && ($leave_duration_status == 1))
					                    {
					                        $leave_type_count = $row->leave_type_count;
					                      	$days = $row->days;
					                        $start_date = date('jS M Y',strtotime($row->start_date));
					                        $end_date = date('jS M Y',strtotime($row->end_date));
					                        $days_taken = $days;//$this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
					                        $leave_balance -= $days_taken;
					                    }
					                }
					            }
					            
					            //maternity & femail
					            if(($leave_type_id == 2) && ($gender_id == 2))
					            {
					                echo '
							                <tr>
							                	<td>'.$leave_type_name.'</td>
							                	<td> '.$leave_balance.'</td>
							               	</tr>
					                ';
					            }
					            
					            //paternity & male
					            else if(($leave_type_id == 1) && ($gender_id == 1))
					            {
					                echo '
					                		<tr>
							                	<td>'.$leave_type_name.'</td>
							                	<td> '.$leave_balance.'</td>
							               	</tr>
					                ';
					            }


					            
					            else if($leave_type_id > 2)
					            {
					                echo '
					                 		<tr>
							                	<td>'.$leave_type_name.'</td>
							                	<td> '.$leave_balance.'</td>
							               	</tr>
					                ';
					            }
					        }
					    }
				    ?>
					</tbody>
				</table>
				
			</div>
			
			
		</div>
		<div class="col-md-8">
			<section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Leave Application</h2>
                    </header>
                    <div class="panel-body">
                
                    <!-- Adding Errors -->
                <?php
                if(isset($error)){
                    echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
                }
                if(!empty($validation_errors))
                {
                    echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                }
    			
                ?>
                <?php
    			$success = $this->session->userdata('success_message');
    			
    			if(!empty($success))
    			{
    				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
    				$this->session->unset_userdata('success_message');
    			}
    			
    			$error = $this->session->userdata('error_message');
    			
    			if(!empty($error))
    			{
    				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
    				$this->session->unset_userdata('error_message');
    			}
    			?>
                <?php echo form_open($this->uri->uri_string());?>
	                <div class="row">
	                    <div class="col-md-12">
	                    	<div class="form-group">
	                            <label class="col-lg-5 control-label">Start date: </label>
	                            
	                            <div class="col-lg-7">
		                            <select class="form-control" name="personnel_id" id="personnel_id">
		                                <!-- <option value="">--Select personnel--</option> -->
		                          
		                                <?php

		                                    if($personnel->num_rows() > 0)
		                                    {
		                                        foreach($personnel->result() as $res)
		                                        {
		                                            $personnel_id = $res->personnel_id;
		                                            $personnel_fname = $res->personnel_fname;
		                                            $personnel_onames = $res->personnel_onames;
		                                            
		                                           
		                                                echo "<option value='".$personnel_id."'>".$personnel_fname." ".$personnel_onames."</option>";
		                                            
		                                        }
		                                    }
		                                ?>
		                            </select>
		                        </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-lg-5 control-label">Leave type: </label>
	                            
	                            <div class="col-lg-7">
	                        
	                                <select class="form-control" name="leave_type_request_id" id="leave_type_request_id">
	                                    <option value="">--Select leave type--</option>
	                                    <?php
	                                        if($leave_types->num_rows() > 0)
	                                        {
	                                            foreach($leave_types->result() as $res)
	                                            {
	                                                $leave_type_id = $res->leave_type_id;
	                                                $leave_type_name = $res->leave_type_name;
	                                                $leave_type_count = $res->leave_type_count;
	                                                
	                                                if($selected_leave_type_id == $leave_type_id)
	                                                {
	                                                    echo '<option value="'.$leave_type_id.'#'.$leave_type_count.'" selected>'.$leave_type_name.'</option>';
	                                                }
	                                                
	                                                else
	                                                {
	                                                    echo '<option value="'.$leave_type_id.'#'.$leave_type_count.'" >'.$leave_type_name.'</option>';
	                                                }
	                                            }
	                                        }
	                                    ?>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-lg-5 control-label">Start date: </label>
	                            
	                            <div class="col-lg-7">
	                                <div class="input-group">
	                                    <span class="input-group-addon">
	                                        <i class="fa fa-calendar"></i>
	                                    </span>
	                                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="start_date" id="start_date" placeholder="Start date" value="<?php echo date('Y-m-d');?>">
	                                </div>
	                            </div>
	                        </div>
	                    	
	                       
	                        <div class="form-group">
	                            <label class="col-lg-5 control-label">No Days: </label>
	                            
	                            <div class="col-lg-7">
	                                <input  type="number"  class="form-control" name="days" id="days" onkeyup="get_available_calendar()" placeholder="Days" value="<?php echo set_value('days');?>">
	                            </div>
	                        </div>
	                        
	                       
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-md-12">
	                        <div id="days-schedule"></div>
	                    </div>
	                </div>
	                <div class="form-group" >
	                    <label class="col-lg-5 control-label">End date: </label>
	                    
	                    <div class="col-lg-7">
	                
	                        <div class="input-group">
	                            <span class="input-group-addon">
	                                <i class="fa fa-calendar"></i>
	                            </span>
	                            <input  type="text" class="form-control" name="end_date" id="end_date" placeholder="End date" readonly>
	                        </div>
	                    </div>
	                </div>
	                        
	                
	                <div class="row" id="submit_button" style="margin-top:10px;display: none;">
	                    <div class="col-md-6 col-md-offset-6">
	                        <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure you want to send this request for your leave request ? ')"> Apply for leave</button>
	                    </div>
	                </div>
	                <?php echo form_close();?> 
                    </div>
                </section>

			
		</div>
	</div>
</div>



<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Leave List</h2>
    </header>
    <div class="panel-body">
            <?php echo $result;?>
    </div>
</section>


<script type="text/javascript">
	function check_leave_balance(leave_type_id,personnel_id)
	{
		var config_url = $('#config_url').val();


        var url = config_url+"hr/leave/check_leave_balance/"+personnel_id+"/"+leave_type_id;

        $.ajax({
        type:'POST',
        url: url,
        data:{leave_type_id: leave_type_id,personnel_id: personnel_id},
        dataType: 'text',
        success:function(data){
          var data = jQuery.parseJSON(data);

          $( "#leave-balance" ).html( data.result );

        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
        });
	}


	function check_balance(personnel_id)
	{
		var config_url = $('#config_url').val();
		var leave_type_id = $('#leave_type_id').val();


        var url = config_url+"hr/leave/check_leave_balance/"+personnel_id+"/"+leave_type_id;

        $.ajax({
        type:'POST',
        url: url,
        data:{leave_type_id: leave_type_id,personnel_id: personnel_id},
        dataType: 'text',
        success:function(data){
          var data = jQuery.parseJSON(data);

          $( "#leave-balance" ).html( data.result );

        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
        });
	}

	function approve_leave(leave_duration_id,personnel_id,status)
	{

		open_sidebar();

		var config_url = $('#config_url').val();
        var url = config_url+"hr/leave/leave_consent_request/"+leave_duration_id+"/"+personnel_id+"/"+status;
        // alert(url);
        $.ajax({
            type:'POST',
            url: url,
            data:{query: null},
            dataType: 'text',
            processData: false,
            contentType: false,
            success:function(data){
            // var data = jQuery.parseJSON(data);
              // alert(data);
            // if(data.message == "success")
            // {
               $('#sidebar-div').html(data);

               
                // tinymce.init({
	            //     selector: ".cleditor",
	            //    	height: "150"
		        //     });

            // }
            // else
            // {
            //     alert('Please ensure you have added included all the items');
            // }

        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
        });

	}



$(document).on("submit","form#update-leave",function(e)
{
	e.preventDefault();
	// myApp.showIndicator();
	
	var form_data = new FormData(this);


	var config_url = $('#config_url').val();
	var status = $('#status').val();
	var personnel_id = $('#personnel_id').val();
	var leave_duration_id = $('#leave_duration_id').val();

	if(status == 1)
	{
		var res = confirm('Are you sure you want to approve the leave ?');
	}
	else if(status == 2)
	{
		var res = confirm('Are you sure you want to decline the leave ?');
	}

	if(res)
	{
		show_loader();
	 var url = config_url+"hr/leave/update_leave_consent_status/"+personnel_id+"/"+leave_duration_id+"/"+status;
       $.ajax({
       type:'POST',
       url: url,
       data:form_data,
       dataType: 'text',
       processData: false,
       contentType: false,
       success:function(data){
          var data = jQuery.parseJSON(data);

          	if(data.message == "success")
			{
				close_side_bar();
				window.location.href = config_url+'hr/personnel-leave-detail/'+personnel_id;


			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

			hide_loader();

       },
       error: function(xhr, status, error) {
       	alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

       }
       });
    }

});

function get_available_calendar()
        {
            var config_url = $('#config_url').val();
            var days = $('#days').val();
            var start_date = $('#start_date').val();
            var leave_type_id = $('#leave_type_request_id').val();
            var personnel_id = $('#personnel_id').val();


            if(leave_type_id > 0 || personnel_id > 0)
            {

            

	            var url = config_url+"hr/leave/get_leave_schedule/"+leave_type_id;

	            $.ajax({
	            type:'POST',
	            url: url,
	            data:{days: days,leave_type_id: leave_type_id,start_date: start_date,personnel_id: personnel_id},
	            dataType: 'text',
	            success:function(data){
	              var data = jQuery.parseJSON(data);

	                if(data.message == 'success')
	                {
	                    $('#days-schedule').html(data.results);
	                    document.getElementById("submit_button").style.display = 'block'; 
	                    document.getElementById("end_date").value = data.end_date; 
	                }
	            },
	            error: function(xhr, status, error) {
	            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	            }
	            });
	        }
        }
</script>