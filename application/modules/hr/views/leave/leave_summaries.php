<?php

$title = 'Leave starting '.date('jS M Y');
$date = date('Y-m-d');
$current_year = date('Y');
$where = 'personnel.personnel_status = 1 AND personnel_type_id = 1';
$personnel = $this->personnel_model->retrieve_personnel($where);
$leave_types = $this->personnel_model->get_leave_types(4);

$result_annual ='';

$personnel_id = $this->session->userdata('personnel_id');
if($personnel_id == 0)
{
	$engagement_date = date('2023-01-01');
}
if($personnel->num_rows() > 0)
{
	$count = 0;
		
	$result_annual .= 
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
		
				<th width="10%">#</th>
				<th  width="25%">STAFF MEMBER (STAFF ARE ENTITLED TO 1.75 LEAVE DAYS PER MONTH)</th>
				<th width="20%">ANNUAL LEAVE DAYS</th>
				<th width="15%">DAYS BOOKED</th>
				<th width="15%">DAYS TAKEN</th>
				<th width="15%">BALANCE</th>
			
		
		</thead>
		  <tbody>
		  
	';
	
	foreach ($personnel->result() as $row)
	{
		$personnel_fname = $row->personnel_fname;
		$personnel_onames = $row->personnel_onames;
		$personnel_id = $row->personnel_id;
		$gender_id = $row->gender_id;
		$engagement_date = $row->engagement_date;

		if(empty($engagement_date) OR $engagement_date == '0000-00-00')
		{
			$engagement_date = date('Y-m-d');
		}

		$engagement_array = explode('-', $engagement_date);
	    $engagement_year = $engagement_array[0];
	    $engagement_month = (int)$engagement_array[1];
	    $engagement_day = $engagement_array[2];
		$leave = $this->personnel_model->get_personnel_leave($personnel_id);

		$pending = $this->leave_model->check_pending_leave($personnel_id,4);
	
		// get the leave types
		if($annual_leave_list->num_rows() > 0)
		{
			foreach($annual_leave_list->result() as $res)
			{
				$leave_type_id = $res->leave_type_id;
				$leave_type_name = $res->leave_type_name;
				$leave_days = $res->leave_days;
				$leave_type_count = $res->leave_type_count;


				// devide accounting to the month
	            $leave_opening_balance = $this->leave_model->personnel_leave_opening_balance($personnel_id,$leave_type_id);
	            $leave_balance = $leave_days;
	            $total_leave = 0;
	            if($leave_type_count == 3)
	            {	

	            	$leave_balance = 0;

	            	$leave_per_month = $leave_days/12;

	            		// get the current month

	            	$current_month = (int)date('m');


	            	if($engagement_year == date('Y'))
	            	{
	            		// calculate the remaining leave days for the annual leave

	            		$total_months = $current_month - $engagement_month;
	            		if($current_month == $engagement_month)
		            	{
		            		$total_months = 1;
		            	}

	            		$leave_per_month = ($total_months*$leave_days)/12;


	            	}
	            

	            	// get avaulable leave days 

	            	for ($i=1; $i <= $current_month; $i++) { 
	            		// code...
	            		$leave_balance += $leave_per_month;
	            	}

	            	


	            }

	            $leave_balance += $leave_opening_balance;
	            
	            $total_leave = $leave_balance;

	           
				// $leave_balance = $total_leave = $res->leave_days;

				$allocated_leave = $this->leave_model->get_personnel_leave_allocated($personnel_id,$leave_type_id,$current_year);

				$days_taken = 0;
				$days =0;
				if($leave->num_rows() > 0)
				{
					foreach($leave->result() as $row_end)
					{
						$leave_type_id2 = $row_end->leave_type_id;
						$leave_duration_status = $row_end->leave_duration_status;
							$days = $row_end->days;

							if($personnel_id == 92 AND $leave_type_id = 4){
								// var_dump($leave);
							}
						// $leave_type_id = $row_end->leave_type_id;
						// var_dump($leave_duration_status); die();
						if(($leave_type_id == $leave_type_id2) && ($leave_duration_status == 1) AND $leave_type_id == 4)
						{

							$leave_type_count = $row_end->leave_type_count;
							$start_date = date('jS M Y',strtotime($row_end->start_date));
							$end_date = date('jS M Y',strtotime($row_end->end_date));
							$days_taken += $days;//$this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
							
							
						}
					
						
				    }
				}
				$leave_balance -= $days_taken;

				
				// annual
				if($leave_type_id == 4)
				{
					$total_leave = $total_leave;
					$annual = $leave_balance;
				}
				
			}
		}


		$button = '';
		$count++;
		$result_annual .= 
		'
			<tr >
				<td>'.$count.'</td>
				<td>'.$personnel_fname.' '.$personnel_onames.'</td>
				<td>'.$total_leave.'</td>
				<td>'.$pending.'</td>
				<td>'.$days_taken.'</td>
				<td>'.$annual.'</td>
		
			</tr> 
		';
			
	}
	
	$result_annual .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result_annual = "<p>No personnel assigned </p>";
}


			
?>
<div class="row">
	<div class="col-md-12">

		

		<div class="col-md-4" >

			<?php
			// $leave_types = $this->personnel_model->get_leave_types(13);
			$off_day_tracker ='';
			if($personnel->num_rows() > 0)
			{
				$count = 0;
					
				$off_day_tracker .= 
				'
				<table class="table table-bordered table-striped table-condensed">
					<thead>
					
							<th width="10%">#</th>
							<th  width="50%">STAFF MEMBER</th>
							<th  width="40%">OPENING BALANCE ANNUAL LEAVE ('.date('Y').')</th>
						
					
					</thead>
					  <tbody>
					  
				';
				
				foreach ($personnel->result() as $row)
				{
					$personnel_fname = $row->personnel_fname;
					$personnel_onames = $row->personnel_onames;
					$personnel_id = $row->personnel_id;
					$gender_id = $row->gender_id;
					$leave_type_id = 4;

					// $leave = $this->personnel_model->get_personnel_leave($personnel_id);
					$leave_opening_balance = $this->leave_model->personnel_leave_opening_balance($personnel_id,4);

					
					$button = '';
					$count++;
					$off_day_tracker .= 
					'
						<tr >
							<td>'.$count.'</td>
							<td>'.$personnel_fname.' '.$personnel_onames.'</td>
							<td><input type="text" class="form-control" name="opening_leave_days'.$personnel_id.'" id="opening_leave_days'.$personnel_id.'" onkeyup="update_opening_leave_days('.$personnel_id.','.$leave_type_id.')" value="'.$leave_opening_balance.'"></td>
					
						</tr> 
					';
						
				}
				
				$off_day_tracker .= 
				'
							  </tbody>
							</table>
				';
			}

			else
			{
				$off_day_tracker = "<p>No personnel assigned </p>";
			}


						
			?>

			<section class="panel">
			    <header class="panel-heading">
			        <h2 class="panel-title">OFF DAY TRACKER  </h2>
			    </header>
			    <div class="panel-body" style="height:30vh;padding:0px;overflow-y:scroll;">
			    <!-- Adding Errors -->
			       
			      
			        <?php echo $off_day_tracker;?>
			    </div>
			</section>
			
		</div>
		<div class="col-md-8">
			<section class="panel">
			    <header class="panel-heading">
			        <h2 class="panel-title">ANNUAL LEAVE TRACKER </h2>
			    </header>
			    <div class="panel-body" style="height:30vh;padding:0px;overflow-y:scroll;">
			    
			        <?php echo $result_annual;?>
			    </div>
			</section>
			
		</div>
	</div>
</div>
<div class="row">
	
		<?php
		
		$title = 'Leave starting '.date('jS M Y');
		$date = date('Y-m-d');
		$where = 'personnel.personnel_status = 1 AND personnel_type_id = 1';
		$personnel = $this->personnel_model->retrieve_personnel($where);
		$leave_types = $this->personnel_model->get_leave_types();


		$headers = '';

		if($leave_types->num_rows() > 0)
		{
			foreach($leave_types->result() as $res)
			{
				$leave_type_id = $res->leave_type_id;
				$leave_type_name = $res->leave_type_name;

				$header .= '<th width="10%">'.$leave_type_name.'</th>';
			}
		}
		else
		{

		}
		$result ='';
		if($personnel->num_rows() > 0)
		{
			$count = 0;
				
			$result .= 
			'
			<br/>
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th width="5%">#</th>
						<th >Personnel</th>
						'.$header.'
						<th width="10%">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			foreach ($personnel->result() as $row)
			{
				$personnel_fname = $row->personnel_fname;
				$personnel_onames = $row->personnel_onames;
				$personnel_id = $row->personnel_id;
				$gender_id = $row->gender_id;
				$engagement_date = $row->engagement_date;

				if(empty($engagement_date) OR $engagement_date == '0000-00-00')
				{
					$engagement_date = date('Y-m-d');
				}

				$engagement_array = explode('-', $engagement_date);
			    $engagement_year = $engagement_array[0];
			    $engagement_month = (int)$engagement_array[1];
			    $engagement_day = $engagement_array[2];


				$leave = $this->personnel_model->get_personnel_leave($personnel_id);

				// get the leave types


				$result_two = '';
				if($leave_types->num_rows() > 0)
				{
					foreach($leave_types->result() as $res)
					{
						$leave_type_id = $res->leave_type_id;
						$leave_type_name = $res->leave_type_name;
						$leave_balance = $res->leave_days;
						$leave_days = $res->leave_days;
						$leave_type_count = $res->leave_type_count;


						// devide accounting to the month
			            $leave_opening_balance = $this->leave_model->personnel_leave_opening_balance($personnel_id,$leave_type_id);
			            $leave_balance = $leave_days;
			            $total_leave = 0;
			            if($leave_type_count == 3)
			            {	

			            	$leave_balance = 0;

			            	$leave_per_month = $leave_days/12;

			            		// get the current month

			            	$current_month = (int)date('m');


			            	if($engagement_year == date('Y'))
			            	{
			            		// calculate the remaining leave days for the annual leave

			            		$total_months = $current_month - $engagement_month;
			            		if($current_month == $engagement_month)
				            	{
				            		$total_months = 1;
				            	}

			            		$leave_per_month = ($total_months*$leave_days)/12;


			            	}
			            

			            	// get avaulable leave days 

			            	for ($i=1; $i <= $current_month; $i++) { 
			            		// code...
			            		$leave_balance += $leave_per_month;
			            	}

			            	


			            }

			            $leave_balance += $leave_opening_balance;
						
						if($leave->num_rows() > 0)
						{
							foreach($leave->result() as $row_end)
							{
								$leave_type_id2 = $row_end->leave_type_id;
								$leave_duration_status = $row_end->leave_duration_status;
								$days = $row_end->days;
								// var_dump($leave_duration_status); die();
								if(($leave_type_id == $leave_type_id2) && ($leave_duration_status == 1))
								{
									$leave_type_count = $row_end->leave_type_count;
									// $days = $row_end->days;
									$start_date = date('jS M Y',strtotime($row_end->start_date));
									$end_date = date('jS M Y',strtotime($row_end->end_date));
									$days_taken = $days;//$this->site_model->calculate_leave_days($start_date, $end_date, $leave_type_count);
									$leave_balance -= $days_taken;
									
								}
							
								
						    }
						}

						//maternity & femail
						if(($leave_type_id == 2) && ($gender_id == 2))
						{
							$maternity = $leave_balance;
						}
						
						//paternity & male
						else if(($leave_type_id == 1) && ($gender_id == 1))
						{
							$maternity = $leave_balance;
						}
						// sick leave
						else if($leave_type_id == 3)
						{
							$sick = $leave_balance;
						}
						// annual
						else if($leave_type_id == 4)
						{
							$annual = $leave_balance;
						}
						// compassionat
						else if($leave_type_id == 6)
						{
							$compassionate = $leave_balance;
							
						}

						$result_two .= '<td>'.$leave_balance.'</td>';
					}
				}
				$pending = $this->personnel_model->check_pending_leave($personnel_id);

				if($pending)
				{
					$pending_status = 'info';
				}
				else
				{
					$pending_status = 'default';
				}
				$button = '';
				$count++;
				$result .= 
				'
					<tr class="'.$pending_status.'">
						<td>'.$count.'</td>
						<td>'.$personnel_fname.' '.$personnel_onames.'</td>
						'.$result_two.'
						<td><a class="btn btn-xs btn-default" href="'.site_url().'hr/personnel-leave-detail/'.$personnel_id.'" title=" Leave"><i class="fa fa-folder-open"></i> Detail </a></td>
					</tr> 
				';
					
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result = "<p>No leave have been assigned</p>";
		}
		

	//repopulate data if validation errors occur
	$validation_errors = validation_errors();
					
	if(!empty($validation_errors))
	{
		$old_personnel_id = set_value('personnel_id');
		$start_date = set_value('start_date');
		$end_date = set_value('end_date');
		$old_leave_type_id = set_value('leave_type_id');
	}

	else
	{
		$old_personnel_id = '';
		$start_date = $date;
		$end_date = '';
		$old_leave_type_id = '';
	}
	?>
	<section class="panel">
	    <header class="panel-heading">
	        <h2 class="panel-title">Staff Leave Summary </h2>
	    </header>
	    <div class="panel-body">
	    <!-- Adding Errors -->
	       
	        <?php echo $result;?>
	    </div>
	</section>
</div>

<script>
	
	function update_opening_leave_days(personnel_id,leave_type_id)
	{

        var config_url = $('#config_url').val();
         var opening_leave_days = $('#opening_leave_days'+personnel_id).val();


        var url = config_url+"hr/leave/update_opening_leave_days/"+personnel_id+"/"+leave_type_id;

        $.ajax({
        type:'POST',
        url: url,
        data:{opening_leave_days: opening_leave_days,personnel_id: personnel_id},
        dataType: 'text',
        success:function(data){
          var data = jQuery.parseJSON(data);


          
        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
        });
	}
</script>
