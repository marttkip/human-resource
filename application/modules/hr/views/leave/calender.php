<div class="row">
    <div class="row" style="margin-bottom:5px;">
        <div class="col-sm-12 ">
            <div class="form-actions ">
                <a href="<?php echo site_url().'my-profile';?>" class="btn btn-info pull-right" type="submit">
                    Back to profile
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-4">

            <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Leave Application</h2>
                    </header>
                    <div class="panel-body">
                
                    <!-- Adding Errors -->
                <?php
                if(isset($error)){
                    echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
                }
                if(!empty($validation_errors))
                {
                    echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                }
    			
                ?>
                <?php
    			$success = $this->session->userdata('success_message');
    			
    			if(!empty($success))
    			{
    				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
    				$this->session->unset_userdata('success_message');
    			}
    			
    			$error = $this->session->userdata('error_message');
    			
    			if(!empty($error))
    			{
    				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
    				$this->session->unset_userdata('error_message');
    			}
    			?>
                <?php echo form_open($this->uri->uri_string());?>
                <div class="row">
                    <div class="col-md-12">
                    	<div class="form-group">
                            <label class="col-lg-5 control-label">Start date: </label>
                            
                            <div class="col-lg-7">
	                            <select class="form-control" name="personnel_id" id="personnel_id">
	                                <option value="">--Select personnel--</option>
	                          
	                                <?php

	                                    if($personnel->num_rows() > 0)
	                                    {
	                                        foreach($personnel->result() as $res)
	                                        {
	                                            $personnel_id = $res->personnel_id;
	                                            $personnel_fname = $res->personnel_fname;
	                                            $personnel_onames = $res->personnel_onames;
	                                            
	                                           
	                                                echo "<option value='".$personnel_id."'>".$personnel_fname." ".$personnel_onames."</option>";
	                                            
	                                        }
	                                    }
	                                ?>
	                            </select>
	                        </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-5 control-label">Leave type: </label>
                            
                            <div class="col-lg-7">
                        
                                <select class="form-control" name="leave_type_request_id" id="leave_type_request_id">
                                    <option value="">--Select leave type--</option>
                                    <?php
                                        if($leave_types->num_rows() > 0)
                                        {
                                            foreach($leave_types->result() as $res)
                                            {
                                                $leave_type_id = $res->leave_type_id;
                                                $leave_type_name = $res->leave_type_name;
                                                $leave_type_count = $res->leave_type_count;
                                                
                                                if($selected_leave_type_id == $leave_type_id)
                                                {
                                                    echo '<option value="'.$leave_type_id.'#'.$leave_type_count.'" selected>'.$leave_type_name.'</option>';
                                                }
                                                
                                                else
                                                {
                                                    echo '<option value="'.$leave_type_id.'#'.$leave_type_count.'" >'.$leave_type_name.'</option>';
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-5 control-label">Start date: </label>
                            
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="start_date" id="start_date" placeholder="Start date" value="<?php echo date('Y-m-d');?>">
                                </div>
                            </div>
                        </div>
                    	
                       
                        <div class="form-group">
                            <label class="col-lg-5 control-label">No Days: </label>
                            
                            <div class="col-lg-7">
                                <input  type="number"  class="form-control" name="days" id="days" onkeyup="get_available_calendar()" placeholder="Days" value="<?php echo set_value('days');?>">
                            </div>
                        </div>
                        
                       
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="days-schedule"></div>
                    </div>
                </div>
                <div class="form-group" >
                    <label class="col-lg-5 control-label">End date: </label>
                    
                    <div class="col-lg-7">
                
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input  type="text" class="form-control" name="end_date" id="end_date" placeholder="End date" readonly>
                        </div>
                    </div>
                </div>
                        
                
                <div class="row" id="submit_button" style="margin-top:10px;display: none;">
                    <div class="col-md-6 col-md-offset-6">
                        <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure you want to send this request for your leave request ? ')"> Apply for leave</button>
                    </div>
                </div>
                <?php echo form_close();?> 
                    </div>
                </section>
            </div>
            <div class="col-md-8">
                <style type="text/css">
                    .currentmonth {
                        color: blue;
                        text-align: left;
                    }
                    .currentday {
                        border: 1px solid black;
                        color: #00FF00;
                        text-align: center;
                    }

                    table.calendar {
                /*      margin:1em 1em 2em 1em;*/
                    }
                    table.calendar td, table.calendar th {
                /*      padding:0.5em;*/
                /*      width: 14.285714285714286% !important;*/
                /*      width: 1.285714285714286% !important;*/
                        width: 190.85px !important;
                    }
                    table.calendar {
                        display:inline-block;
                        *display:inline; zoom:1;
                        vertical-align:top;
                    }
                    table.calendar tr td .col-print-2
                    {
                /*      width: 10% !important;*/
                        padding: 0px !important;
                        text-align: right !important;
                        padding-right: 5px !important;
                    }
                    table.calendar tr td .col-print-10
                    {
                /*      width: 90% !important;*/
                        padding: 0px !important;
                        text-align: left !important;
                        padding-left: 5px !important;
                    }
                    .col-print-12 {
                        width: 100%;
                        float: left;
                        position: relative;
                        min-height: 1px;
                        padding: 0px !important; 
                    }
                </style>
                <?php
                $planner_details = json_encode($event_list);

                // var_dump($planner_details);die();
                ?>

                <div class="panel-body" style="height:85vh;overflow-y:scroll;padding:0px;">
                    <div id="calendar"></div>
                </div>

        </div>
    </div>
</div>
    <script type="text/javascript">
        function get_available_calendar()
        {
            var config_url = $('#config_url').val();
            var days = $('#days').val();
            var start_date = $('#start_date').val();
            var leave_type_id = $('#leave_type_request_id').val();
            var personnel_id = $('#personnel_id').val();


            if(leave_type_id > 0 || personnel_id > 0)
            {

            

	            var url = config_url+"hr/leave/get_leave_schedule/"+leave_type_id;

	            $.ajax({
	            type:'POST',
	            url: url,
	            data:{days: days,leave_type_id: leave_type_id,start_date: start_date,personnel_id: personnel_id},
	            dataType: 'text',
	            success:function(data){
	              var data = jQuery.parseJSON(data);

	                if(data.message == 'success')
	                {
	                    $('#days-schedule').html(data.results);
	                    document.getElementById("submit_button").style.display = 'block'; 
	                    document.getElementById("end_date").value = data.end_date; 
	                }
	            },
	            error: function(xhr, status, error) {
	            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	            }
	            });
	        }
        }

    
        (function () {

            let data_checked = <?php echo $planner_details?>;
            // let x = myFunction(4, 3); 

            // let days_data = data_checked['2023-01-10'];

            
            // for (var i = 0; i < days_data.length; i++) {
            //  alert(days_data[i]);
            // }
            // alert(data_checked['2023-01-10'][1]);



        function calendar(month) {
            // alert(data_checked);
            //Variables to be used later.  Place holders right now.
            var padding = "";
        
            var totalFeb = "";
            var i = 1;
            var testing = "";

            var current = new Date();
            var cmonth = current.getMonth();
            var day = current.getDate();
            var year = current.getFullYear();
            var tempMonth = month + 1; //+1; //Used to match up the current month with the correct start date.
            var prevMonth = month - 1;

            //Determing if Feb has 28 or 29 days in it.  
            if (month == 1) {
                if ((year % 100 !== 0) && (year % 4 === 0) || (year % 400 === 0)) {
                    totalFeb = 29;
                } else {
                    totalFeb = 28;
                }
            }

            //////////////////////////////////////////
            // Setting up arrays for the name of    //
            // the  months, days, and the number of //
            // days in the month.                   //
            //////////////////////////////////////////

            var monthNames = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
            var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thrusday", "Friday", "Saturday"];
            var totalDays = ["31", "" + totalFeb + "", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"];

            //////////////////////////////////////////
            // Temp values to get the number of days//
            // in current month, and previous month.//
            // Also getting the day of the week.    //
            //////////////////////////////////////////

            var tempDate = new Date(tempMonth + ' 1 ,' + year);
            var tempweekday = tempDate.getDay();
            var tempweekday2 = tempweekday;
            var dayAmount = totalDays[month];
            // var preAmount = totalDays[prevMonth] - tempweekday + 1;  

            //////////////////////////////////////////////////
            // After getting the first day of the week for  //
            // the month, padding the other days for that   //
            // week with the previous months days.  IE, if  //
            // the first day of the week is on a Thursday,  //
            // then this fills in Sun - Wed with the last   //
            // months dates, counting down from the last    //
            // day on Wed, until Sunday.                    //
            //////////////////////////////////////////////////

            while (tempweekday > 0) {
                padding += "<td class='premonth' style='border: 1px solid lightgrey;color: #000;text-align: center;'><div class='col-print-12' >&nbsp;</div></td>";
                //preAmount++;
                tempweekday--;
            }
            //////////////////////////////////////////////////
            // Filling in the calendar with the current     //
            // month days in the correct location along.    //
            //////////////////////////////////////////////////
            
            
                
            while (i <= dayAmount) {


                var days_list = '&nbsp;';
                //////////////////////////////////////////
                // Determining when to start a new row  //
                //////////////////////////////////////////

                if (tempweekday2 > 6) {
                    tempweekday2 = 0;
                    padding += "</tr><tr >";
                }


                // next
                let day_variable = i;
                if(day_variable<10)
                {
                    day_variable = '0'+day_variable;
                }
                else
                {
                    day_variable = day_variable;
                }


                let month_variable = month+1;
                if(month_variable<10)
                {
                    month_variable = '0'+month_variable;
                }
                else
                {
                    month_variable = month_variable;
                }


                let view_date = year+'-'+month_variable+'-'+day_variable;
                
                // if(view_date === '2023-01-10')
                // {
                //  alert(data_checked['2023-01-11'][0]);
                // }

                let days_data = data_checked[view_date];
                let color_code = '#FFFFFF';
                if (typeof days_data === 'undefined') {
                      
                }
                else
                {

                    color_code =  days_data[0]['color_code'];

                    // alert(color_code)
                    for (var k = 0; k < days_data.length; k++) {
                        
                        days_list += days_data[k]['name']+'\n';
                    }
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////
                // checking to see if i is equal to the current day, if so then we are making the color of //
                //that cell a different color using CSS. Also adding a rollover effect to highlight the  //
                //day the user rolls over. This loop creates the acutal calendar that is displayed.     //
                //////////////////////////////////////////////////////////////////////////////////////////////////

                if (i == day && month == cmonth) {

                    

                    

                    
                    // if(days_data.length > 0){
                        
                    // }
                    padding += "<td class='currentday'  onMouseOver='this.style.background=\"#00FF00\"; this.style.color=\"#FFFFFF\"' onMouseOut='this.style.background=\"#FFFFFF\"; this.style.color=\"#00FF00\"' style='border: 1px solid lightgrey;color: #000;text-align: center;'><div class='col-print-12'><div class='col-print-2' style=''>" + i + "</div><div class='col-print-10'  style='font-size:9px'>"+days_list+"</div></div></td>";
                } else {

                    

                    
            
                    // padding += "<td class='currentmonth' onMouseOver='this.style.background=\"#00FF00\"' onMouseOut='this.style.background=\"#FFFFFF\"' style='border: 1px solid lightgrey;color: #000;text-align: center;'><div class='col-print-12'><div class='col-print-2' style='background-color:"+color_code+"'>" + i + "</div><div class='col-print-10'  style='border-left:1px solid lightgrey'>"+days_list+"</div></div></td>";

                    padding += "<td class='currentmonth' onMouseOver='this.style.background=\"#00FF00\"' onMouseOut='this.style.background=\"#FFFFFF\"' style='border: 1px solid lightgrey;color: #000;text-align: center;'><span class='col-print-2' style='background-color:"+color_code+"'>" + i + "</span><span class='col-print-10'  style='border-left:1px solid lightgrey;font-size:9px'>"+days_list+"</span></td>";


                }

                tempweekday2++;
                i++;
            }


            /////////////////////////////////////////
            // Ouptputing the calendar onto the //
            // site.  Also, putting in the month    //
            // name and days of the week.       //
            /////////////////////////////////////////

            // var calendarTable = "<div class='col-md-12'><table class='calendar' style='width: 100%;'> <tr class='currentmonth'><th colspan='7'>" + monthNames[month] + " " + year + "</th></tr>";
            // if(monthNames[month] == 'Jan')
            // {
            //  calendarTable += "<tr class='weekdays'>  <td>Sun</td>  <td>Mon</td> <td>Tues</td> <td>Wed</td> <td>Thurs</td> <td>Fri</td> <td>Sat</td> </tr>";
            // }
            
            // calendarTable += "<tr>";
            // calendarTable += padding;
            // calendarTable += "</tr></table></div>";

             var calendarTable = "<div class='col-md-12' style='border-top:1px solid black'><table style='width: 100% !important;'> <tr ><td style='width: 5% !important;'>" + monthNames[month] + "</td>";
               
                
                calendarTable += "<td style='width: 90% !important;'><table class='calendar' style='width: 100%;'><tr class='currentmonth'>";
                 if(monthNames[month] == 'Jan')
                {
                    calendarTable += "<tr class='weekdays'>  <th style='border-right: 1px solid lightgrey;text-align:center'>Sun</th>  <th style='border-right: 1px solid lightgrey;text-align:center'>Mon</th> <th style='border-right: 1px solid lightgrey;text-align:center'>Tues</th> <th style='border-right: 1px solid lightgrey;text-align:center'>Wed</th> <th style='border-right: 1px solid lightgrey;text-align:center'>Thurs</th> <th style='border-right: 1px solid lightgrey;text-align:center'>Fri</th> <th style='border-right: 1px solid lightgrey;text-align:center'>Sat</th> </tr>";
                }
                calendarTable += padding;
                calendarTable += "</tr></table></td><td style='width: 5% !important;text-align:center'>" + monthNames[month] + "</td></table></div>";
            document.getElementById("calendar").innerHTML += calendarTable;
        }

        function go12() {
            for (i = 0; i < 12; i++) {
                calendar(i);
            }
        }

        if (window.addEventListener) {
            window.addEventListener('load', go12, false);
        } else if (window.attachEvent) {
            window.attachEvent('onload', go12);
        }

    })();


        function add_new_event()
        {

            document.getElementById("sidebar-right").style.display = "block"; 
            document.getElementById("existing-sidebar-div").style.display = "none"; 

            var config_url = $('#config_url').val();
            var data_url = config_url+"planner/add_new_event";
            // window.alert(data_url);
            $.ajax({
            type:'POST',
            url: data_url,
            data:{appointment_id: 1},
            dataType: 'text',
            success:function(data){

                document.getElementById("current-sidebar-div").style.display = "block"; 
                $("#current-sidebar-div").html(data);
                
                tinymce.init({
                                selector: ".cleditor",
                                height: "200"
                                });
                $('.datepicker').datepicker();
                $('.timepicker').timepicker();


                // alert(data);
                },
                error: function(xhr, status, error) {
                //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                alert(error);
            }

            });

        }


        function close_side_bar()
        {
            // $('html').removeClass('sidebar-right-opened');
            document.getElementById("sidebar-right").style.display = "none"; 
            document.getElementById("current-sidebar-div").style.display = "none"; 
            document.getElementById("existing-sidebar-div").style.display = "none"; 
            tinymce.remove();
        }
    </script>