<?php
$personnel = $this->personnel_model->get_personnel($personnel_id);


$row = $personnel->row();

// var_dump($row) or die();
$personnel_onames = $row->personnel_onames;
$personnel_fname = $row->personnel_fname;
$personnel_dob = $row->personnel_dob;
$personnel_email = $row->personnel_email;
$personnel_phone = $row->personnel_phone;
$personnel_address = $row->personnel_address;
$civil_status_id = $row->civilstatus_id;
$personnel_locality = $row->personnel_locality;
$title_id = $row->title_id;
$gender_id = $row->gender_id;
$personnel_username = $row->personnel_username;
$personnel_kin_fname = $row->personnel_kin_fname;
$personnel_kin_onames = $row->personnel_kin_onames;
$personnel_kin_contact = $row->personnel_kin_contact;
$personnel_kin_address = $row->personnel_kin_address;
$kin_relationship_id = $row->kin_relationship_id;
$job_title_idd = $row->job_title_id;
$staff_id = $row->personnel_staff_id;
$personnel_id = $row->personnel_id;
$engagement_date = $row->engagement_date;




if($status == 1)
{
	$status_name = 'approving';
}
else
{
	$status_name = 'rejection';
}

$leave_details = $this->leave_model->get_personnel_leave_duration($leave_duration_id);

$leave_duration_note = '';
if($leave_details->num_rows() > 0)
{
	foreach ($leave_details->result() as $key => $value) {
		// code...

		$leave_duration_note = $value->leave_duration_note;
	}
}

?>
<section class="panel" >
	<div class="panel-body" style="height:80vh">
		
		<div class="col-md-12">
			<form id="update-leave" method="post">
				<input type="hidden" id="personnel_id" name="personnel_id" value="<?php echo $personnel_id?>">
				<input type="hidden" id="leave_duration_id" name="leave_duration_id" value="<?php echo $leave_duration_id?>">
				<input type="hidden" id="status" name="status" value="<?php echo $status?>">
				<div class="col-md-2">	
				</div>
				<div class="col-md-8">		

					<div class="form-group">
						<label class="col-lg-12 control-label"> <?php echo $status_name?> note: </label>
						<div class="col-lg-12">
							<textarea class="form-control cleditor" id="leave_duration_note" name="leave_duration_note" required><?php echo $leave_duration_note?></textarea>
						</div>
					</div>


					<div class="form-group">
						<label class="col-lg-12 control-label"> Personnel Email </label>
						<div class="col-lg-12">
							<input type="text" class="form-control" name="personnel_email" id="personnel_email" value="<?php echo $personnel_email?>" required>
						</div>
					</div>

						
				</div>
				<div class="col-md-2">	
				</div>
				<div class="row" >
			        <div class="col-md-12">
			        	<div class=" center-align" style="margin-top: 10px;">
			        		<button type="submit" class="btn btn-sm btn-success ">Update <?php echo $status_name?> note</button>
			        		<input type="submit" name="">
			        	</div>
			               
			        </div>
			    </div>
			</form>
		</div>
		
		
	</div>
</section>
<div class="row" style="margin-top: 5px;">
	<ul>
		<li style="margin-bottom: 5px;">
			<div class="row">
		        <div class="col-md-12 center-align">
			      <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a> 
		        </div>
		    </div>
			
		</li>
	</ul>
</div>