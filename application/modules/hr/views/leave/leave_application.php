<div class="row">
    <div class="row" style="margin-bottom:5px;">
        <div class="col-sm-12 ">
            <div class="form-actions ">
                <a href="<?php echo site_url().'my-profile';?>" class="btn btn-info pull-right" type="submit">
                    Back to profile
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">

            <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Leave Application</h2>
                    </header>
                    <div class="panel-body">
                
                    <!-- Adding Errors -->
                <?php
                if(isset($error)){
                    echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
                }
                if(!empty($validation_errors))
                {
                    echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                }
                
                ?>
                <?php
                $success = $this->session->userdata('success_message');
                
                if(!empty($success))
                {
                    echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
                    $this->session->unset_userdata('success_message');
                }
                
                $error = $this->session->userdata('error_message');
                
                if(!empty($error))
                {
                    echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
                    $this->session->unset_userdata('error_message');
                }
                ?>
                <?php echo form_open($this->uri->uri_string());?>
                <input type="hidden" name="personnel_id" id="personnel_id" value="<?php echo $personnel_id;?>"/>
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="form-group">
                            <label class="col-lg-5 control-label">Start date: </label>
                            
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="start_date" id="start_date" placeholder="Start date" value="<?php echo date('Y-m-d');?>">
                                </div>
                            </div>
                        </div>
                        
                       
                        <div class="form-group">
                            <label class="col-lg-5 control-label">No Days: </label>
                            
                            <div class="col-lg-7">
                                <input  type="text"  class="form-control" name="days" id="days" onkeyup="get_available_calendar()" placeholder="Days" value="<?php echo set_value('days');?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-5 control-label">Leave type: </label>
                            
                            <div class="col-lg-7">
                        
                                <select class="form-control" name="leave_type_request_id" id="leave_type_request_id">
                                    <option value="">--Select leave type--</option>
                                    <?php
                                        if($leave_types->num_rows() > 0)
                                        {
                                            foreach($leave_types->result() as $res)
                                            {
                                                $leave_type_id = $res->leave_type_id;
                                                $leave_type_name = $res->leave_type_name;
                                                $leave_type_count = $res->leave_type_count;
                                                
                                                if($selected_leave_type_id == $leave_type_id)
                                                {
                                                    echo '<option value="'.$leave_type_id.'#'.$leave_type_count.'" selected>'.$leave_type_name.'</option>';
                                                }
                                                
                                                else
                                                {
                                                    echo '<option value="'.$leave_type_id.'#'.$leave_type_count.'" >'.$leave_type_name.'</option>';
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="days-schedule"></div>
                    </div>
                </div>
                <div class="form-group" >
                    <label class="col-lg-5 control-label">End date: </label>
                    
                    <div class="col-lg-7">
                
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input  type="text" class="form-control" name="end_date" id="leave_end_date" placeholder="End date" readonly>
                        </div>
                    </div>
                </div>
                        
                
                <div class="row" id="submit_button" style="margin-top:10px;display: none;">
                    <div class="col-md-6 col-md-offset-6">
                        <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure you want to send this request for your leave request ? ')"> Apply for leave</button>
                    </div>
                </div>
                <?php echo form_close();?> 
                    </div>
                </section>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
    <script type="text/javascript">
        // alert("sasjhakjshjka");
          function get_available_calendar()
        {
            var config_url = $('#config_url').val();
            var days = $('#days').val();
            var start_date = $('#start_date').val();
            var leave_type_id = $('#leave_type_request_id').val();
            var personnel_id = $('#personnel_id').val();


            var url = config_url+"hr/leave/get_leave_schedule/"+leave_type_id;

            $.ajax({
            type:'POST',
            url: url,
            data:{days: days,leave_type_id: leave_type_id,start_date: start_date,personnel_id: personnel_id},
            dataType: 'text',
            success:function(data){
              var data = jQuery.parseJSON(data);

                if(data.message == 'success')
                {
                    $('#days-schedule').html(data.results);
                    document.getElementById("submit_button").style.display = 'block'; 
                    document.getElementById("leave_end_date").value = data.end_date; 
                }
                else
                {
                     $('#days-schedule').html(data.results);
                }
            },
            error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

            }
            });
        }

    </script>