<table class="table table-condensed table-bordered table-linked">
	<thead>
		<tr>
			<th>DATE</th>
			<th>START TIME</th>
			<th>END TIME</th>
			<th>HOURS</th>
		</tr>
	</thead>
	<?php
	

	$timesheet_rs = $this->personnel_model->get_time_reports($personnel_id);
	$list = '';
	$grand_hours = '';
	if($timesheet_rs->num_rows() > 0)
	{
		$list .= '<tbody>';
		foreach ($timesheet_rs->result() as $key => $value) {
			// code...
			$sign_time_in = $value->sign_time_in;
			$sign_time_out = $time_out = $value->sign_time_out;
			
			if(empty($sign_time_out))
			{
				$sign_time_out = '-';

				$total_hours = 0;
				
			}
			else
			{
				$sign_time_out = date('H:i A',strtotime($sign_time_out));

				$datetime1 = new DateTime($sign_time_in);
				$datetime2 = new DateTime($time_out);
				$interval = $datetime1->diff($datetime2);
				$total_hours =  $interval->format('%h')." Hours ".$interval->format('%i')." Minutes";
				// $total_hours =  $interval->format('%h')." Hours ";

				$grand_hours += $interval->format('%h');
				$hour = $interval->format('%h');
				$minute = $interval->format('%i');

				if($minute < 10)
					$minute = '0'.$minute;

				if($hour < 10)
					$hour = '0'.$hour;

				// $string[] = "'$hour:$minute',";
				$string[] = "$hour:$minute:00,";

				// array_push($string, $section);

				
				// $arr = [$string]
				

			}
			
			
			$list .= '<tr>
						<td>'.date('jS M Y',strtotime($sign_time_in)).'</td>
						<td>'.date('H:i A',strtotime($sign_time_in)).'</td>
						<td>'.$sign_time_out.'</td>
						<td>'.$total_hours.'</td>
						</tr>';
		}
				
					
		$total = 0;
			
		// Loop the data items
		foreach( $string as $element):

			// echo $element;
		
			// Explode by separator :
			$temp = explode(":", $element);

			// Convert the hours into seconds
			// and add to total
			$total+= (int) $temp[0] * 3600;

			// Convert the minutes to seconds
			// and add to total
			$total+= (int) $temp[1] * 60;

			// Add the seconds to total
			$total+= (int) $temp[2];
		endforeach;
			
		// Format the seconds back into HH:MM:SS
		$formatted = sprintf('%02d:%02d:%02d',($total / 3600),($total / 60 % 60),$total % 60);

		$explode = explode(':', $formatted);

		$time = $explode[0].' Hours '.$explode[1].' Minutes';

		$list .= '</tbody>';


		$list .= '<tfoot>
					<tr>
						<th colspan="3">TOTAL WORK TIME</th>
						<th style="border-top:2px solid black;color: white">'.$time.'</th>
					</tr>
				  </tfoot>';
	}


	echo $list;
	?>
</table>