<table class="table table-striped table-bordered table-linked">
	<thead>
		<tr>
			<th></th>
			<th>STAFF NAME</th>
			<th>STAFF STATUS</th>
			<th>TOTAL HOURS</th>
		</tr>
		
	</thead>
	<?php 

	$personnel_timesheet = $this->personnel_model->get_all_timesheet();
	$previous_item_list = array();


	foreach ($personnel_timesheet->result() as $element_six) {
	    $previous_item_list[$element_six->personnel_id] = $element_six;
	}


	$personnel_rs = $this->personnel_model->all_personnel();

	if($personnel_rs->num_rows() > 0)
	{
		$personnel_list = '<tbody>';
		$count = 0;
		$string = array();
		foreach ($personnel_rs->result() as $key => $value) {
			// code...
			$personnel_onames = $value->personnel_onames;
			$personnel_fname = $value->personnel_fname;
			$personnel_id = $value->personnel_id;

			$total_hours = 0;
			
			$formatted = $this->personnel_model->calculate_hours_worked($personnel_id);
			if(!empty($formatted))
			{

				// var_dump($formatted);die();
				$string[] = "$formatted,";
				$explode = explode(':', $formatted);

				$time = $explode[0].' Hours '.$explode[1].' Minutes';
			}
			else
			{
				$time = '';
			}
			$count++;
			$personnel_list .='<tr onclick="get_personnel_timesheet('.$personnel_id.')">
								<td>'.$count.'</td>
								<td> '.$personnel_fname.' '.$personnel_onames.'</td>
								<td>ACTIVE</td>
								<td>'.$time.'</td>
								</tr>';
		}
		$personnel_list .= '</tbody>';

		$total = 0;
				
		// Loop the data items
		foreach( $string as $element):

			// echo $element;
		
			// Explode by separator :
			$temp = explode(":", $element);

			// Convert the hours into seconds
			// and add to total
			$total+= (int) $temp[0] * 3600;

			// Convert the minutes to seconds
			// and add to total
			$total+= (int) $temp[1] * 60;

			// Add the seconds to total
			$total+= (int) $temp[2];
		endforeach;
			
		// Format the seconds back into HH:MM:SS
		$formatted_minutes = sprintf('%02d:%02d:%02d',($total / 3600),($total / 60 % 60),$total % 60);
		$explode = explode(':', $formatted_minutes);
		$time = $explode[0].' Hours '.$explode[1].' Minutes';

		$personnel_list .='<tfoot>
								<tr>
								<th colspan="3">TOTAL WORKING HOURS</th>
								
								<th>'.$time.'</th>
								</tr>
							<tfoot>';
	}
	echo $personnel_list;
	?>
</table>