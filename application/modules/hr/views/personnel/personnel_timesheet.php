<?php
// var_dump($month_list);die();
$year_search = $this->session->userdata('shift_year');
if(empty($year_search))
{
	$year_search = date('Y');
}


$month_search = $this->session->userdata('shift_month');
if(empty($month_search))
{
	$month_search = date('m');
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-4">

		    	<section class="panel">
		            <header class="panel-heading">						
		                <h2 class="panel-title">Search monthly timesheets</h2>
		            </header>
		            <div class="panel-body">
		            	<?php 
						echo form_open('hr/search_timesheet');
						?>
						<div class="row">
							<div class="form-group">
			                    <label class="col-lg-5 control-label">Year: </label>
			                    
			                    <div class="col-lg-7">
			                        <input type="text" name="year" class="form-control" size="54" value="<?php echo $year_search;?>" />
			                    </div>
			                </div>
							
							<div class="form-group">
			                    <label class="col-lg-5 control-label">Month: </label>
			                    
			                    <div class="col-lg-7">
			                        <select name="month" class="form-control">
			                            <?php
			                            // $month_search = null;
			                                if($month_list->num_rows() > 0){
			                                    foreach ($month_list->result() as $row):
			                                        $mth = $row->month_name;
			                                        $mth_id = $row->month_id;
			                                        if($mth_id == $month_search){

			                                        	
			                                            echo "<option value=".$mth_id." selected>".$row->month_name."</option>";
			                                        }
			                                        else{
			                                        	
			                                            echo "<option value=".$mth_id.">".$row->month_name."</option>";
			                                        }
			                                    endforeach;
			                                }
			                            ?>
			                        </select>
			                    </div>
			                </div>
		        
			          
			                    <div class="col-lg-7 col-lg-offset-5">
			                        <div class="form-actions center-align">
			                            <button class="submit btn btn-primary" type="submit">
			                                <i class='fa fa-search'></i> Search
			                            </button>
			                        </div>
			                    </div>
			                </div>
		                <?php echo form_close();?>

		                <?php

		                $search = $this->session->userdata('timesheet_searched');
						if(!empty($search))
						{
							echo '<a href="'.site_url().'hospital_reports/close_doctors_turnover_search" class="btn btn-sm btn-warning">Close Search</a>';
						}
						echo '<h4 class="center-align"> '.$report_date.'</h4>';
		                ?>
		            </div>
		        </section>

		       

		     	<div class="panel-body" style="height: 50vh;overflow-y: scroll;padding: 0px;">
		         		<div id="personnel-list"></div>
		        </div>
		    
			
		</div>
		                       
		 
		<div class="col-md-8">
		    <section class="panel panel-featured panel-featured-info">
		        <header class="panel-heading">
		        	 <h2 class="panel-title"><?php echo $title.' '.$report_date.' Report';?></h2>
		        	  <div class="widget-icons pull-right" style="margin-top: -24px !important;">
		        	  	
		        	</div>
		        </header>             
		     	
		        <div class="panel-body" style="width:auto;overflow-x:scroll;height: 60vh;overflow-y: scroll;padding: 0px !important;">
		  
		        		<div id="personnel-timesheet"></div>
		  		</div>
			</section>
		</div>
	</div>
</div>
<script type="text/javascript">
	
	$(document).ready(function(){
      
		get_all_personnel();
    });
    function get_all_personnel()
    {
    	var config_url = $('#config_url').val();
		var data_url = config_url+"hr/personnel_list";
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			
			$("#personnel-list").html(data);
			
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
    }

    function get_personnel_timesheet(personnel_id)
    {
    	var config_url = $('#config_url').val();
		var data_url = config_url+"hr/timesheet/"+personnel_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			
			$("#personnel-timesheet").html(data);
			
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
    }

</script>