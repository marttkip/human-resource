<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a href="'.site_url().'human-resource/personnel/personnel.personnel_type_id/'.$order_method.'/'.$page.'">Type</a></th>
						<th><a href="'.site_url().'human-resource/personnel/personnel_number/'.$order_method.'/'.$page.'">Number</a></th>
						<th><a href="'.site_url().'human-resource/personnel/personnel_onames/'.$order_method.'/'.$page.'">Name of Employee</a></th>
						<th>National ID</th>
						<th>KRA PIN</th>
						<th>NSSF</th>
						<th>NHIF</th>
						<th><a href="'.site_url().'human-resource/personnel/personnel_status/'.$order_method.'/'.$page.'">Status</a></th>
						<th colspan="3">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$personnel_id = $row->personnel_id;
				$branch_id = $row->branch_id;
				$personnel_fname = $row->personnel_fname;
				$personnel_onames = $row->personnel_onames;
				$personnel_username = $row->personnel_username;
				$personnel_phone = $row->personnel_phone;
				$personnel_number = $row->personnel_number;
				$personnel_email = $row->personnel_email;
				$personnel_status = $row->personnel_status;
				$personnel_type_name = $row->personnel_type_name;
				$personnel_kra_pin = $row->personnel_kra_pin;
				$personnel_nssf_number = $row->personnel_nssf_number;
				$personnel_nhif_number = $row->personnel_nhif_number;
				$personnel_national_id_number = $row->personnel_national_id_number;
				$personnel_name = $personnel_fname.' '.$personnel_onames;
				
				//status
				if($personnel_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($personnel_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-xs btn-info" href="'.site_url().'human-resource/activate-personnel/'.$personnel_id.'" onclick="return confirm(\'Do you want to activate '.$personnel_name.'?\');" title="Activate '.$personnel_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
				}
				//create activated status display
				else if($personnel_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-xs btn-default" href="'.site_url().'human-resource/deactivate-personnel/'.$personnel_id.'" onclick="return confirm(\'Do you want to deactivate '.$personnel_name.'?\');" title="Deactivate '.$personnel_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
				}
				
				//get branch
				$branch = '';
				if($branches->num_rows() > 0)
				{
					foreach($branches->result() as $res)
					{
						$branch_id2 = $res->branch_id;
						if($branch_id == $branch_id2)
						{
							$branch = $res->branch_name;
						}
					}
				}
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$personnel_type_name.'</td>
						<td>'.$personnel_number.'</td>
						<td>'.$personnel_fname.' '.$personnel_onames.'</td>

						<td>'.$personnel_national_id_number.'</td>
						<td>'.$personnel_kra_pin.'</td>
						<td>'.$personnel_nssf_number.'</td>
						<td>'.$personnel_nhif_number.'</td>
						<td>'.$status.'</td>
						<td><a href="'.site_url().'human-resource/reset-password/'.$personnel_id.'" class="btn btn-xs btn-warning" onclick="return confirm(\'Reset password for '.$personnel_fname.'?\');">Reset Password</a></td>
						<td><a href="'.site_url().'human-resource/edit-personnel/'.$personnel_id.'" class="btn btn-xs btn-success" title="Edit '.$personnel_name.'"><i class="fa fa-pencil"></i>Edit Personnel</a></td>
						<td>'.$button.'</td>
					
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no personnel";
		}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search personnel</h2>
    </header>
    
    <!-- Widget content -->
   <div class="panel-body">
    	<div class="padd">
			<?php
            echo form_open("hr/personnel/search_personnel", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Branch: </label>
                        
                        <div class="col-md-8">
                            <select class="form-control" name="branch_id">
                            	<option value="">---Select Branch---</option>
                                <?php
                                    if($branches->num_rows() > 0){
                                        foreach($branches->result() as $row):
                                            $branch_name = $row->branch_name;
                                            $branch_id= $row->branch_id;
                                            ?><option value="<?php echo $branch_id; ?>" ><?php echo $branch_name; ?></option>
                                        <?php	
                                        endforeach;
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    
                </div>
                
                <div class="col-md-3">
                	<div class="form-group">
                        <label class="col-md-4 control-label">Staff Number: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="personnel_number" placeholder="Staff Number">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                	 <div class="form-group">
                        <label class="col-md-4 control-label">Staff Name: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="personnel_fname" placeholder="Staff Name">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-8 col-md-offset-4">
                    	<div class="center-align">
                        	<button type="submit" class="btn btn-info btn-xs">Search</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
    </div>
</section>
						<section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?></h2>
								 <div class="pull-right">
						        	<?php
						        	echo '
									<a href="'.site_url().'human-resource/add-personnel" class="btn btn-xs btn-info" style="margin-left:15px; margin-top:-40px;">Add Personnel</a>
									<a href="'.site_url().'human-resource/import-personnel" class="btn btn-xs btn-success" style="margin-top:-40px;">Import Personnel</a>
									<a href="'.site_url().'human-resource/download-personnel" class="btn btn-xs btn-warning" style="margin-top:-40px;">Download Personnel</a>
									';
						        	?>
						        	
						        </div>
							</header>
							<div class="panel-body">
								<div class="col-md-12">
									<?php
									$search = $this->session->userdata('personnel_search_title2');
									
									if(!empty($search))
									{
										echo '<h6>Filtered by: '.$search.'</h6>';
										echo '<a href="'.site_url().'hr/personnel/close_search" class="btn btn-xs btn-info pull-left">Close search</a>';
									}
	                                $success = $this->session->userdata('success_message');
			
									if(!empty($success))
									{
										echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
										$this->session->unset_userdata('success_message');
									}
									
									$error = $this->session->userdata('error_message');
									
									if(!empty($error))
									{
										echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
										$this->session->unset_userdata('error_message');
									}
									?>
									
								</div>
                            	
								<div class="col-md-12">
                            
								<div class="table-responsive">
                                	
									<?php echo $result;?>
							
                                </div>
                            </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>