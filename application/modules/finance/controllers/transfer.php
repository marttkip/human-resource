<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";
error_reporting(E_ALL);
class Transfer extends admin
{

  private $financials_accounts;
	function __construct()
	{
		parent:: __construct();

	    $this->load->model('finance/purchases_model');
	    $this->load->model('finance/transfer_model');
      $this->load->model('accounts/accounts_model');
      $this->load->model('finance/creditors_model');
      $this->load->model('finance/providers_model');
      $this->load->model('finance/payroll_model');
      $this->load->model('financials/company_financial_model');


      $accounts_config_rs = $this->company_financial_model->get_staging_accounts();

      $patients = array();

      if($accounts_config_rs->num_rows() > 0)
      {
        foreach ($accounts_config_rs->result() as $key => $value) {
          // code...
          $staing_account_id = $value->account_id;
          $reference_name = $value->reference_name;

          $session_account[$reference_name] = $staing_account_id;


        }
      }
      $this->financials_accounts = $session_account;


    	if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}



  public function write_cheque($location_id = 2)
	{
		//form validation
		$this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
		$this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
		$this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('transfer_date', 'Transfer Date','required|xss_clean');

		if ($this->form_validation->run())
		{
			//update order
			if($this->transfer_model->transfer_funds())
			{
				$this->session->set_userdata('success_message', 'Cheque successfully writted to account');


				redirect('accounting/accounts-transfer');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}



		//open the add new order
		$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");

		$where = 'finance_transfer_status = 1 AND finance_transfer_deleted = 0';


	    $search_transfers = $this->session->userdata('search_transfers');
	    if(!empty($search_transfers))
	    {
	      $where .= $search_transfers;
	    }

		$table = 'finance_transfer';


		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounting/accounts-transfer';
		$config['total_rows'] = $this->transfer_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->transfer_model->get_account_transfer_transactions($table, $where, $config["per_page"], $page, $order='finance_transfer.transaction_date', $order_method='DESC');
		// var_dump($query); die();

		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];

		$v_data['query'] = $query;
		$v_data['page'] = $page;
    $v_data['location_id'] = $location_id;

		$data['title'] = $v_data['title']= 'Transfer Cheque';

		$data['content'] = $this->load->view('finance/transfer/write_cheques', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

  public function get_account_list_type($type)
  {
        $query = $this->purchases_model->get_child_accounts("Bank",$type);
        echo '<option value="0">--Select an option --</option>';
        $options = $query;
        foreach($options-> result() AS $key_old) {
            if($key_old->account_id != $type)
            {
                echo '<option value="'.$key_old->account_id.'">'.$key_old->account_name.'</option>';
            }

        }
    }
     public function search_transfers($finance_transfer_id=null)
    {
      $visit_date_from = $this->input->post('date_from');
      $reference_number = $this->input->post('transaction_number');
      $visit_date_to = $this->input->post('date_to');

      $search_title = '';

      if(!empty($reference_number))
      {
        $search_title .= $tenant_name.' ';
        $transaction_number = ' AND finance_transfer.reference_number LIKE \'%'.$reference_number.'%\'';


      }
      else
      {
        $transaction_number = '';
        $search_title .= '';
      }

       if(!empty($visit_date_from) && !empty($visit_date_to))
       {
         $visit_date = ' AND finance_transfer.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
         $search_title .= 'Payments from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
       }

       else if(!empty($visit_date_from))
       {
         $visit_date = ' AND finance_transfer.transaction_date = \''.$visit_date_from.'\'';
         $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_from)).' ';
       }

       else if(!empty($visit_date_to))
       {
         $visit_date = ' AND finance_transfer.transaction_date = \''.$visit_date_to.'\'';
         $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_to)).' ';
       }

       else
       {
         $visit_date = '';
       }


      if(!empty($finance_transfer_id))
      {
        $search_title .= $tenant_name.' ';
        $finance_transfer = ' AND finance_transfer.finance_transfer_id = '.$finance_transfer_id.'';


      }
      else
      {
        $finance_transfer = '';
        $search_title .= '';
      }


      $search = $visit_date.$transaction_number.$finance_transfer;

      $this->session->set_userdata('search_transfers', $search);

      redirect('accounting/accounts-transfer');
    }
    public function search_transfers_old()
    {
      $visit_date_from = $this->input->post('date_from');
      $reference_number = $this->input->post('transaction_number');
      $visit_date_to = $this->input->post('date_to');

      $search_title = '';

      if(!empty($reference_number))
      {
        $search_title .= $tenant_name.' ';
        $transaction_number = ' AND finance_transfer.reference_number LIKE \'%'.$reference_number.'%\'';


      }
      else
      {
        $transaction_number = '';
        $search_title .= '';
      }

       if(!empty($visit_date_from) && !empty($visit_date_to))
       {
         $visit_date = ' AND finance_transfer.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
         $search_title .= 'Payments from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
       }

       else if(!empty($visit_date_from))
       {
         $visit_date = ' AND finance_transfer.transaction_date = \''.$visit_date_from.'\'';
         $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_from)).' ';
       }

       else if(!empty($visit_date_to))
       {
         $visit_date = ' AND finance_transfer.transaction_date = \''.$visit_date_to.'\'';
         $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_to)).' ';
       }

       else
       {
         $visit_date = '';
       }


      $search = $visit_date.$transaction_number;

      $this->session->set_userdata('search_transfers', $search);

      redirect('accounting/accounts-transfer');
    }

    public function close_search()
  	{
  		$this->session->unset_userdata('search_transfers');
  		redirect('accounting/accounts-transfer');
  	}
  	public function delete_transfer_payment($finance_transfer_id)
  	{
  		$personnel_id = $this->session->userdata('personnel_id');
		$this->db->where('finance_transfer_id = '.$finance_transfer_id);
		$query = $this->db->get('finance_transfer');
		$item = $query->row();

		// $deleted_by = $item->deleted_by;
		$finance_transfer_deleted = $item->finance_transfer_deleted;



  		// $update_array['deleted_by'] = $personnel_id;
  		// $update_array['date_deleted'] = date('Y-m-d');
    $update_array['finance_transfer_deleted'] = 0;
  		$update_array['finance_transfer_deleted'] = 1;
  		// $update_array['deleted_remarks'] = $deleted_status;
  		$this->db->where('finance_transfer_id = '.$finance_transfer_id);
  		if($this->db->update('finance_transfer',$update_array))
  		{
  			$this->session->set_userdata('success_message', 'You have successfully deleted this entry');
  		}
  		else
  		{
  			$this->session->set_userdata('error_message', 'Sorry could not perform the action. Please try again');
  		}


  		redirect('accounting/accounts-transfer');
  	}


  	public function journal_entry()
    {
      //form validation
      $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
      $this->form_validation->set_rules('account_to_id', 'Charge To','required|xss_clean');
      $this->form_validation->set_rules('amount', 'Amount','required|xss_clean');
      $this->form_validation->set_rules('description', 'Description','required|xss_clean');
      $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');


      if ($this->form_validation->run())
      {
        //update order
        if($this->transfer_model->add_journal_entry())
        {
          $this->session->set_userdata('success_message', 'Cheque successfully writted to account');


          redirect('accounting/journal-entry');
        }

        else
        {
          $this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
        }
      }
      else
      {
        $this->session->set_userdata('error_message', validation_errors());
      }



      //open the add new order
      $v_data['accounts'] = $accounts = $this->purchases_model->get_all_accounts();

      // var_dump($accounts->result());die();
      $v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Expense Accounts");

      $where = 'journal_entry_deleted = 0 ';
      $table = 'journal_entry';

      $journal = $this->session->userdata('search_journal');

      if(!empty($journal))
      {
        $where .= $journal;
      }

      $segment = 3;
      $this->load->library('pagination');
      $config['base_url'] = site_url().'accounting/journal-entry';
      $config['total_rows'] = $this->transfer_model->count_items($table, $where);
      $config['uri_segment'] = $segment;
      $config['per_page'] = 20;
      $config['num_links'] = 5;

      $config['full_tag_open'] = '<ul class="pagination pull-right">';
      $config['full_tag_close'] = '</ul>';

      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';

      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';

      $config['next_tag_open'] = '<li>';
      $config['next_link'] = 'Next';
      $config['next_tag_close'] = '</span>';

      $config['prev_tag_open'] = '<li>';
      $config['prev_link'] = 'Prev';
      $config['prev_tag_close'] = '</li>';

      $config['cur_tag_open'] = '<li class="active"><a href="#">';
      $config['cur_tag_close'] = '</a></li>';

      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $this->pagination->initialize($config);

      $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
          $v_data["links"] = $this->pagination->create_links();
      $query = $this->transfer_model->get_account_payments_transactions($table, $where, $config["per_page"], $page, $order='journal_entry.created', $order_method='DESC');
      // var_dump($query); die();

      $data['title'] = 'Accounts';
      $v_data['title'] = $data['title'];

      $v_data['query'] = $query;
      $v_data['page'] = $page;

      $data['title'] = $v_data['title']= 'Journal Entry';

      $data['content'] = $this->load->view('transfer/journal_entry', $v_data, true);
      $this->load->view('admin/templates/general_page', $data);
    }

  public function edit_journal($account_id)
  {
    $data['accounts'] = $accounts = $this->purchases_model->get_all_accounts();
     $data['account_id'] = $account_id;

    $page = $this->load->view('finance/transfer/edit_journal',$data,true);
    // var_dump($page);die();
    echo $page;
  }

  public function edit_journal_data()
  {
    $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
    $this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
    $this->form_validation->set_rules('amount', 'Amount','required|xss_clean');
    $this->form_validation->set_rules('description', 'Description','required|xss_clean');
    //$this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
    $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');
    //$this->form_validation->set_rules('account_payment_id', 'Account Payment','required|xss_clean');

    if ($this->form_validation->run())
    {
     // var_dump($_POST);
      //die();

      if($this->transfer_model->edit_journal_details())
      {

        $response['message'] ='success';
        $response['result'] ='You have successfully updated the payment';

      } else {
        $response['message'] ='fail';
        $response['result'] ='Sorry could not update this payment detail';
      }
    } else {
      $response['message'] ='fail';
      $response['result'] = strip_tags(validation_errors());
    }
    echo json_encode($response);

  }


  public function edit_account_transfer($transfer_id)
  {
   //var_dump($transfer_id);
    //die();
    $data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
    $data['transfer_id'] = $transfer_id;
    $page = $this->load->view('finance/transfer/edit_account_transfer',$data,true);
    echo $page;

  }

  public function edit_account_transfer_data()
  {
    //var_dump($_POST);
    //die();
     $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
    $this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
    $this->form_validation->set_rules('transfer_date', 'Transfer Date','required|xss_clean');
    $this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
    //$this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
    $this->form_validation->set_rules('amount', 'Amount','required|xss_clean');
    $this->form_validation->set_rules('description', 'Remarks','required|xss_clean');

    if ($this->form_validation->run())
    {
        if($this->transfer_model->edit_account_transfer_data())
      {

        $response['message'] ='success';
        $response['result'] ='You have successfully updated the payment';

      } else {
        $response['message'] ='fail';
        $response['result'] ='Sorry could not update this payment detail';
      }


    } else {
      $response['message'] ='fail';
      $response['result'] = strip_tags(validation_errors());
    }
    echo json_encode($response);
  }

  public function get_other_accounts($account_id)
  {
        $query = $this->purchases_model->get_all_accounts($account_id);
        $changed = '<option value="">--Select an option --</option>';
        // $options = $query;
        // foreach($options-> result() AS $key_old) {
        //     if($key_old->account_id != $type)
        //     {
        //         echo '<option value="'.$key_old->account_id.'">'.$key_old->account_name.'</option>';
        //     }

        // }

       if($query->num_rows() > 0)
       {
           foreach($query->result() as $row):
               // $company_name = $row->company_name;
               $account_name = $row->account_name;
               $account_id = $row->account_id;
               $parent_account = $row->parent_account;

               if($parent_account != $current_parent)
               {
                  $account_from_name = $this->transfer_model->get_account_name($parent_account);
                $changed .= '<optgroup label="'.$account_from_name.'">';
               }

               $changed .= "<option value=".$account_id."> ".$account_name."</option>";
               $current_parent = $parent_account;
               if($parent_account != $current_parent)
               {
                $changed .= '</optgroup>';
               }



           endforeach;
       }

       echo $changed;
    }

    public function delete_journal_entry($journal_entry_id,$attached_account_id=0,$type_id=0)
    {
      //delete creditor

        $array['journal_entry_deleted'] = 1;
        $array['journal_entry_deleted_by'] = $this->session->userdata('personnel_id');
        $array['journal_entry_deleted_date'] = date('Y-m-d');

        $this->db->where('journal_entry_id',$journal_entry_id);
        $this->db->update('journal_entry',$array);
        $this->session->set_userdata('success_message', 'You have successfully removed the entry');


        // if($attached_account_id > 0)
        // {

         

          if($type_id == 1)
            redirect('creditor-adjustments/'.$attached_account_id);
          else if($type_id == 2)
            redirect('provider-adjustments/'.$attached_account_id);
          else if($type_id == 3)
            redirect('statutory-adjustments/'.$attached_account_id);
          else if($type_id == 4)
            redirect('journals/director-and-shareholders-journals');
          else if($type_id == 5)
            redirect('journals/providers-payable-journals');
          else if($type_id == 6)
            redirect('journals/creditors-payable-journals');
          else if($type_id == 7)
            redirect('journals/payroll-payable-journals');
          else if($type_id == 8)
            redirect('accounting/journal-entry');

        // }
        // else
        // {
        //  redirect('accounting/journal-entry');

        // }
    }


    public function search_journal_entry($journal_entry_id = NULL)
    {
      $visit_date_from = $this->input->post('date_from');
      $account_id = $this->input->post('account');
      $reference_number = $this->input->post('reference_number');
      $visit_date_to = $this->input->post('date_to');

      $search_title = '';

      if(!empty($account_id))
      {
        $search_title .= $tenant_name.' ';
        $account_id = ' AND (journal_entry.account_from_id = '.$account_id.' OR journal_entry.account_to_id ='.$account_id.')';


      }
      else
      {
        $account_id = '';
        $search_title .= '';
      }


      if(!empty($journal_entry_id))
      {

        $journal = ' AND (journal_entry.journal_entry_id = '.$journal_entry_id.')';


      }
      else
      {
        $journal = '';

      }




      if(!empty($reference_number))
      {
        $search_title .= $tenant_name.' ';
        $reference_number = ' AND journal_entry.document_number LIKE \'%'.$transaction_number.'%\'';


      }
      else
      {
        $transaction_number = '';
        $search_title .= '';
      }

       if(!empty($visit_date_from) && !empty($visit_date_to))
       {
         $visit_date = ' AND journal_entry.payment_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
         $search_title .= 'Journal from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
       }

       else if(!empty($visit_date_from))
       {
         $visit_date = ' AND journal_entry.payment_date = \''.$visit_date_from.'\'';
         $search_title .= 'Journal of '.date('jS M Y', strtotime($visit_date_from)).' ';
       }

       else if(!empty($visit_date_to))
       {
         $visit_date = ' AND journal_entry.payment_date = \''.$visit_date_to.'\'';
         $search_title .= 'Journal of '.date('jS M Y', strtotime($visit_date_to)).' ';
       }

       else
       {
         $visit_date = '';
       }


      $search = $visit_date.$transaction_number.$account_id.$journal;

      $this->session->set_userdata('search_journal', $search);

      redirect('accounting/journal-entry');
    }


    public function close_journal_search()
    {
      $this->session->unset_userdata('search_journal');
      redirect('accounting/journal-entry');
    }



  public function direct_payments()
  {

    // var_dump('fsfs');die();
    //form validation
    $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
    // $this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
    $this->form_validation->set_rules('amount', 'Amount','required|xss_clean');
    $this->form_validation->set_rules('description', 'Description','required|xss_clean');
    $this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
    $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');

    if ($this->form_validation->run())
    {
      //update order
      if($this->transfer_model->add_account_payment())
      {
        $this->session->set_userdata('success_message', 'Cheque successfully writted to account');


        redirect('accounting/journal-entry');
      }

      else
      {
        $this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
      }
    }
    else
    {

      $this->session->set_userdata('error_message', validation_errors());
    }



   //open the add new order
    $v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");

    $where = 'account_payment_deleted = 0 ';
    $table = 'account_payments';


    $search = $this->session->userdata('search_direct_payments');

    if(!empty($search))
    {
      $where .=$search;
    }

    $segment = 3;
    $this->load->library('pagination');
    $config['base_url'] = site_url().'accounting/journal-entry';
    $config['total_rows'] = $this->transfer_model->count_items($table, $where);
    $config['uri_segment'] = $segment;
    $config['per_page'] = 20;
    $config['num_links'] = 5;

    $config['full_tag_open'] = '<ul class="pagination pull-right">';
    $config['full_tag_close'] = '</ul>';

    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';

    $config['next_tag_open'] = '<li>';
    $config['next_link'] = 'Next';
    $config['next_tag_close'] = '</span>';

    $config['prev_tag_open'] = '<li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);

    $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
    $query = $this->transfer_model->get_account_payments_transactions($table, $where, $config["per_page"], $page, $order='account_payments.payment_date', $order_method='DESC');
    // var_dump($query); die();
  $v_data['expense_accounts'] = $this->purchases_model->get_child_accounts("Expense Accounts");
    $data['title'] = 'Accounts';
    $v_data['title'] = $data['title'];

    $v_data['query'] = $query;
    $v_data['page'] = $page;

    $data['title'] = $v_data['title']= 'Direct Payments';

    $data['content'] = $this->load->view('finance/transfer/direct_payments', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }

  public function get_list_type($type)
  {

    if($type == 2)
    {
      $table = "creditor";
      $where = "creditor_id > 0";
      $select = "creditor_name AS charge_to_name, creditor_id AS charge_to_id";
    }
    else if($type == 3)
    {
      $table = "property_owners";
      $where = "property_owners.property_owner_id > 0";
      $select = "property_owner_name AS charge_to_name, property_owners.property_owner_id AS charge_to_id";

    }
    else if($type == 1)
    {
      $query = $this->transfer_model->get_child_accounts("Bank");
    }

    else if($type == 4)
    {
      $query = $this->purchases_model->get_child_accounts("Expense Accounts");
    }

    echo '<option value="0">--Select an option --</option>';
    if($type == 2 OR $type == 3)
    {

      $options = $this->transfer_model->get_type_variables($table,$where,$select);
      foreach($options->result() AS $key)
      {
        echo '<option value="'.$key->charge_to_id.'">'.$key->charge_to_name.'</option>';
      }
    }
    else
    {
      $options = $query;
      foreach($options->result() AS $key_old)
      {
        echo '<option value="'.$key_old->account_id.'">'.$key_old->account_name.'</option>';
      }
    }

  }
  public function delete_direct_payment($account_payment_id)
  {
    $array['account_payment_deleted'] = 1;
    $array['account_payment_deleted_by'] = $this->session->userdata('personnel_id');
    $array['account_payment_deleted_date'] = date('Y-m-d');
    $this->db->where('account_payment_id',$account_payment_id);
    $this->db->update('account_payments',$array_update);

    redirect('accounting/journal-entry');
  }

   public function search_direct_payments($transactionid = NULL)
  {
     $visit_date_from = $this->input->post('date_from');
      $account_from_id = $this->input->post('account_from_id');
      $account_to_id = $this->input->post('account_to_id');
      $visit_date_to = $this->input->post('date_to');

      $search_title = '';



      if(!empty($property_owner_id))
      {
        $property_owner_name = $this->transfer_model->get_property_owner_name($property_owner_id);
        $search_title .= $property_owner_name;
        $property_owner_id = ' AND account_payments.payment_to = '.$property_owner_id;


      }
      else
      {
        $property_owner_id = '';
        $search_title .= '';
      }





       if(!empty($visit_date_from) && !empty($visit_date_to))
       {
         $visit_date = ' AND account_payments.payment_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
         $search_title .= ' FROM '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
       }

       else if(!empty($visit_date_from))
       {
         $visit_date = ' AND account_payments.payment_date = \''.$visit_date_from.'\'';
         $search_title .= ' FOR '.date('jS M Y', strtotime($visit_date_from)).' ';
       }

       else if(!empty($visit_date_to))
       {
         $visit_date = ' AND account_payments.payment_date = \''.$visit_date_to.'\'';
         $search_title .= ' FOR '.date('jS M Y', strtotime($visit_date_to)).' ';
       }

       else
       {
         $visit_date = '';
       }

      if(!empty($account_from_id))
      {
        $account_name = $this->transfer_model->get_account_name($account_from_id);
        $search_title .= 'FROM '.$account_name;
        $account_from_id = ' AND (account_payments.account_from_id = '.$account_from_id.')';


      }
      else
      {
        $account_from_id = '';
        $search_title .= '';
      }

      if(!empty($account_to_id))
      {
        $account_name = $this->transfer_model->get_account_name($account_to_id);
        $search_title .= 'FROM '.$account_name;
        $account_to_id = ' AND (account_payments.account_to_id = '.$account_to_id.')';


      }
      else
      {
        $account_to_id = '';
        $search_title .= '';
      }



      if(!empty($transactionid))
      {

        $transactionid = ' AND (account_payments.account_payment_id = '.$transactionid.')';


      }
      else
      {
        $transactionid = '';

      }


      $search = $visit_date.$property_owner_id.$account_from_id.$transactionid.$account_to_id;

      // var_dump($search);die();

      $this->session->set_userdata('search_direct_payments', $search);
       $this->session->set_userdata('title_direct_payments', $search_title);

      redirect('accounting/direct-purchases');
  }

    public function close_direct_payments_search()
    {
      $this->session->unset_userdata('search_direct_payments');
       $this->session->unset_userdata('title_direct_payments');
      redirect('accounting/journal-entry');
    }

    public function print_direct_payments()
    {
        // var_dump($account); die();

       $where = 'account_payment_deleted = 0 ';
      $table = 'account_payments';


      $search = $this->session->userdata('search_direct_payments');

      if(!empty($search))
      {
        $where .=$search;
      }
      $this->db->where($where);
      $this->db->order_by('account_payments.payment_date', 'ASC');
      $this->db->select('*');
      $defaulters_query = $this->db->get($table);

      $v_data['query'] = $defaulters_query;

      $v_data['contacts'] = $this->site_model->get_contacts();
      $v_data['search_title'] = 'Direct Payments';
      $v_data['title'] = 'Direct Payments';
      $this->load->view('finance/transfer/print_direct_payments', $v_data);
    }

    public function export_direct_payments()
    {

      $this->transfer_model->export_direct_payments();

    }








    // landloard transfers


    public function landlord_transfer()
    {
      //form validation
      $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
      $this->form_validation->set_rules('account_to_id', 'Charge To','required|xss_clean');
      $this->form_validation->set_rules('amount', 'Amount','required|xss_clean');
      $this->form_validation->set_rules('description', 'Description','required|xss_clean');
      $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');


      if ($this->form_validation->run())
      {
        //update order
        if($this->transfer_model->add_journal_entry())
        {
          $this->session->set_userdata('success_message', 'Cheque successfully writted to account');


          redirect('accounting/journal-entry');
        }

        else
        {
          $this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
        }
      }
      else
      {
        $this->session->set_userdata('error_message', validation_errors());
      }



      //open the add new order
      $v_data['accounts'] = $accounts = $this->purchases_model->get_all_accounts();

      // var_dump($accounts->result());die();
      $v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Expense Accounts");

      $where = 'journal_entry_deleted = 0 ';
      $table = 'journal_entry';

      $journal = $this->session->userdata('search_journal');

      if(!empty($journal))
      {
        $where .= $journal;
      }

      $segment = 3;
      $this->load->library('pagination');
      $config['base_url'] = site_url().'accounting/journal-entry';
      $config['total_rows'] = $this->transfer_model->count_items($table, $where);
      $config['uri_segment'] = $segment;
      $config['per_page'] = 20;
      $config['num_links'] = 5;

      $config['full_tag_open'] = '<ul class="pagination pull-right">';
      $config['full_tag_close'] = '</ul>';

      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';

      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';

      $config['next_tag_open'] = '<li>';
      $config['next_link'] = 'Next';
      $config['next_tag_close'] = '</span>';

      $config['prev_tag_open'] = '<li>';
      $config['prev_link'] = 'Prev';
      $config['prev_tag_close'] = '</li>';

      $config['cur_tag_open'] = '<li class="active"><a href="#">';
      $config['cur_tag_close'] = '</a></li>';

      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $this->pagination->initialize($config);

      $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
          $v_data["links"] = $this->pagination->create_links();
      $query = $this->transfer_model->get_account_payments_transactions($table, $where, $config["per_page"], $page, $order='journal_entry.created', $order_method='DESC');
      // var_dump($query); die();

      $data['title'] = 'Accounts';
      $v_data['title'] = $data['title'];

      $v_data['query'] = $query;
      $v_data['page'] = $page;

      $data['title'] = $v_data['title']= 'Landlord Transfers';

      $data['content'] = $this->load->view('transfer/landlord_transfer', $v_data, true);
      $this->load->view('admin/templates/general_page', $data);
    }




  public function cheque_sequence()
  {
    //form validation
    $this->form_validation->set_rules('sequence_start', 'From','required|xss_clean');
    $this->form_validation->set_rules('sequence_end', 'Account To','required|xss_clean');
    $this->form_validation->set_rules('bank_id', 'Amount','required|xss_clean');

    if ($this->form_validation->run())
    {
      //update order
      $sequence_start = $this->input->post('sequence_start');
      $sequence_end = $this->input->post('sequence_end');

      if($sequence_start < $sequence_end)
      {
          if($this->transfer_model->add_cheque_sequence())
          {

            $this->session->set_userdata('success_message', 'Cheque successfully writted to account');
            redirect('cheques/cheque-sequences');

          }

          else
          {
            $this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
          }

      }
      else
      {
         $this->session->set_userdata('error_message', 'Sorry seems like the sequence you are creating is not correct. Please enter it correctly');
      }

    }
    else
    {
      $this->session->set_userdata('error_message', validation_errors());
    }



    //open the add new order
    $v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");

    $where = 'cheque_sequence_deleted = 0';


      $search_cheque_sequences = $this->session->userdata('search_cheque_sequences');
      if(!empty($search_cheque_sequences))
      {
        $where .= $search_cheque_sequences;
      }

    $table = 'cheque_sequence';


    $segment = 3;
    $this->load->library('pagination');
    $config['base_url'] = site_url().'cheques/cheque-sequences';
    $config['total_rows'] = $this->transfer_model->count_items($table, $where);
    $config['uri_segment'] = $segment;
    $config['per_page'] = 20;
    $config['num_links'] = 5;

    $config['full_tag_open'] = '<ul class="pagination pull-right">';
    $config['full_tag_close'] = '</ul>';

    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';

    $config['next_tag_open'] = '<li>';
    $config['next_link'] = 'Next';
    $config['next_tag_close'] = '</span>';

    $config['prev_tag_open'] = '<li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);

    $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
    $query = $this->transfer_model->get_transactions
($table, $where, $config["per_page"], $page, $order='cheque_sequence.created_on', $order_method='DESC');
    // var_dump($query); die();

    $data['title'] = 'Accounts';
    $v_data['title'] = $data['title'];

    $v_data['query'] = $query;
    $v_data['page'] = $page;

    $data['title'] = $v_data['title']= 'CHEQUES';
    $v_data['account_list'] = $this->accounts_model->get_payment_account();


    $data['content'] = $this->load->view('finance/cheques/cheque_sequence', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }

  public function view_cheque_sequence($cheque_sequence_id)
  {

    //var_dump($transfer_id);
    //die();
    $data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
    $data['cheque_sequence_id'] = $cheque_sequence_id;
    $page = $this->load->view('finance/cheques/cheque_sequence_detail',$data,true);
    echo $page;

  }

   public function confirm_for_invoices($account_id,$type,$attached_account_id =0,$journal_entry_id=0)
  {

    $accounts_config_rs = $this->company_financial_model->get_staging_accounts();

    $patients = array();

    if($accounts_config_rs->num_rows() > 0)
    {
      foreach ($accounts_config_rs->result() as $key => $value) {
        // code...
        $staing_account_id = $value->account_id;
        $reference_name = $value->reference_name;

        $session_account[$reference_name] = $staing_account_id;


      }
    }
    $parent_creditor_payable_id = $session_account['parent_creditor_payable_id'];
    $parent_provider_payable_id = $session_account['parent_provider_payable_id'];
    $parent_payroll_payable_id =  $session_account['parent_payroll_payable_id'];

    if($type == 3)
      $account_name = 'payroll';
    else
      $account_name = $this->transfer_model->confirm_parent_account($account_id,$parent_creditor_payable_id,$parent_provider_payable_id,$parent_payroll_payable_id);



    if($journal_entry_id > 0)
    {
      $this->db->where('journal_entry_id',$journal_entry_id);

      $query = $this->db->get('journal_entry');

      if($query->num_rows() > 0)
      {
         foreach ($query->result() as $key => $value) {
           // code...
            $invoice_to_id = $value->invoice_to_id;
            $account_to_type_id = $value->account_to_type_id;
            $invoice_to_number = $value->invoice_to_number;
            $invoice_to_type = $value->invoice_to_type;
            $attached_to_account_id = $value->attached_to_account_id;



            $invoice_from_id = $value->invoice_from_id;
            $account_from_type_id = $value->account_from_type_id;
            $invoice_from_number = $value->invoice_from_number;
            $invoice_from_type = $value->invoice_from_type;
            $attached_from_account_id = $value->attached_from_account_id;




         }
      }
    }

    if($account_name == 'creditor')
    {
        

        $creditor_invoices = $this->creditors_model->get_creditor_invoice_number($attached_account_id);

        if($creditor_invoices->num_rows() > 0)
        {
          $data['message'] = 'success';
          $result =  '<option value="0">--Select a supplier invoice--</option>';
          foreach ($creditor_invoices->result() as $key => $value) {
            // code...
            $creditor_invoice_id = $value->creditor_invoice_id;
            $invoice_number = $value->invoice_number;
            $creditor_invoice_type = $value->creditor_invoice_type;
            $balance = $value->balance;
            $dr_amount = $value->dr_amount;
            $cr_amount = $value->cr_amount;
            $invoice_date = $value->invoice_date;
            $creditor_name = $value->creditor_name;
            $creditor_id = $value->creditor_id;



            if($creditor_invoice_type == "Supplies Invoice")
            {
              $invoice_type = 1;
            }
            else if($creditor_invoice_type == "Opening Balance")
            {
              $invoice_type = 2;
            }
            else if($creditor_invoice_type == "Asset Purchase")
            {
              $invoice_type = 4;
            }
            else
            {
              $invoice_type = 0;
            }

            if($cr_amount > 0)
            {
              $color_checked = 'orange';
            }
            else if($cr_amount == 0)
            {
              $color_checked = 'red';
            }
            else
            {
              $color_checked = 'white';
            }

            if($journal_entry_id > 0)
            {
              if(($creditor_id == $attached_to_account_id OR $creditor_id == $attached_from_account_id) AND ($invoice_to_id == $creditor_invoice_id OR $invoice_from_id == $creditor_invoice_id) AND ($invoice_to_number == $invoice_number OR $invoice_from_number == $invoice_number) AND ($invoice_to_type == $invoice_type OR $invoice_from_type == $invoice_type) )
              {

                $result .=  '<option value="'.$creditor_invoice_id.'...'.$invoice_number.'...'.$invoice_type.'...'.$creditor_id.'...1" style="background:'.$color_checked.';color:white;" selected> '.$creditor_name.' : '.$creditor_invoice_type.' '.$invoice_date.' # '.$invoice_number.' Bill .'.number_format($dr_amount,2).' Payments.('.number_format($cr_amount,2).') Bal.'.number_format($balance,2).'</option>';

              }
              else
              {
                $result .=  '<option value="'.$creditor_invoice_id.'...'.$invoice_number.'...'.$invoice_type.'...'.$creditor_id.'...1" style="background:'.$color_checked.';color:white;"> '.$creditor_name.' : '.$creditor_invoice_type.' '.$invoice_date.' # '.$invoice_number.' Bill .'.number_format($dr_amount,2).' Payments.('.number_format($cr_amount,2).') Bal.'.number_format($balance,2).'</option>';
              }
            }
            else
            {
              if($balance > 0)
              {
                 $result .=  '<option value="'.$creditor_invoice_id.'...'.$invoice_number.'...'.$invoice_type.'...'.$creditor_id.'...1" style="background:'.$color_checked.';color:white;"> '.$creditor_name.' : '.$creditor_invoice_type.' '.$invoice_date.' # '.$invoice_number.' Bill .'.number_format($dr_amount,2).' Payments.('.number_format($cr_amount,2).') Bal.'.number_format($balance,2).'</option>';
              }
            }
           
          }

          $data['result'] = $result;
        }


    }
    else if($account_name == 'provider')
    {

        

        $provider_invoices = $this->providers_model->get_provider_invoice_number($attached_account_id);

        // var_dump($provider_invoices->result()); die();
        if($provider_invoices->num_rows() > 0)
        {
          $data['message'] = 'success';
          $result = '<option value="0">--Select a provider invoice --</option>';
          foreach ($provider_invoices->result() as $key => $value) {
            // code...
            $provider_invoice_id = $value->provider_invoice_id;
            $invoice_number = $value->invoice_number;
            $provider_invoice_type = $value->provider_invoice_type;
            $balance = $value->balance;
            $dr_amount = $value->dr_amount;
            $cr_amount = $value->cr_amount;
            $invoice_date = $value->invoice_date;
            $provider_name = $value->provider_name;
            $provider_id = $value->provider_id;



            if($provider_invoice_type == "Supplies Invoice")
            {
              $invoice_type = 1;
            }
            else if($provider_invoice_type == "Opening Balance")
            {
              $invoice_type = 2;
            }
            else
            {
              $invoice_type = 0;
            }

            if($cr_amount > 0)
            {
              $color_checked = 'orange';
            }
            else if($cr_amount == 0)
            {
              $color_checked = 'red';
            }
            else
            {
              $color_checked = 'white';
            }



            if($journal_entry_id > 0)
            {
              if(($provider_id == $attached_to_account_id OR $provider_id == $attached_from_account_id) AND ($invoice_to_id == $provider_invoice_id OR $invoice_from_id == $provider_invoice_id) AND ($invoice_to_number == $invoice_number OR $invoice_from_number == $invoice_number) AND ($invoice_to_type == $invoice_type OR $invoice_from_type == $invoice_type) )
              {
                $result .=  '<option value="'.$provider_invoice_id.'...'.$invoice_number.'...'.$invoice_type.'...'.$provider_id.'...2" style="background:'.$color_checked.';color:white;" selected> '.$provider_name.' '.$invoice_date.' # '.$invoice_number.' Bill .'.number_format($dr_amount,2).' Payments.('.number_format($cr_amount,2).') Bal.'.number_format($balance,2).'</option>';
              }
              else
              {

                // var_dump($provider_invoice_id);die();
                if($balance > 0)
                {
                   $result .=  '<option value="'.$provider_invoice_id.'...'.$invoice_number.'...'.$invoice_type.'...'.$provider_id.'...2" style="background:'.$color_checked.';color:white;"> '.$provider_name.' '.$invoice_date.' # '.$invoice_number.' Bill .'.number_format($dr_amount,2).' Payments.('.number_format($cr_amount,2).') Bal.'.number_format($balance,2).'</option>';
                }

              }
            }
            else
            {
              // var_dump($provider_invoice_id);die();
              if($balance > 0)
              {
                 $result .=  '<option value="'.$provider_invoice_id.'...'.$invoice_number.'...'.$invoice_type.'...'.$provider_id.'...2" style="background:'.$color_checked.';color:white;"> '.$provider_name.' '.$invoice_date.' # '.$invoice_number.' Bill .'.number_format($dr_amount,2).' Payments.('.number_format($cr_amount,2).') Bal.'.number_format($balance,2).'</option>';
              }
            }
            
           
          }


          $data['result'] = $result;
        }


    }
    else if($account_name == 'payroll')
    {

      $payroll_invoices = $this->payroll_model->get_statutory_invoice_number($attached_account_id);
       // var_dump($payroll_invoices);die();
      if($payroll_invoices->num_rows() > 0)
      {
        $data['message'] = 'success';
        $result = '<option value="0"> --Select a statutory account --</option>';
        foreach ($payroll_invoices->result() as $key => $value) {
          // code...
          $provider_invoice_id = $value->provider_invoice_id;
          $invoice_number = $value->invoice_number;
          $provider_invoice_type = $value->provider_invoice_type;
          $balance = $value->balance;
          $dr_amount = $value->dr_amount;
          $cr_amount = $value->cr_amount;
          $invoice_date = $value->invoice_date;
          $statutory_account_id = $value->statutory_account_id;
          $statutory_account_name = $value->statutory_account_name;



          if($provider_invoice_type == "Payroll Invoices")
          {
            $invoice_type = 1;
          }
          else if($provider_invoice_type == "Opening Balance")
          {
            $invoice_type = 2;
          }
          else
          {
            $invoice_type = 0;
          }

          if($cr_amount > 0)
          {
            $color_checked = 'orange';
          }
          else if($cr_amount == 0)
          {
            $color_checked = 'red';
          }
          else
          {
            $color_checked = 'white';
          }

          // var_dump($provider_invoice_id);die();
          if($journal_entry_id > 0)
          {
            if(($statutory_account_id == $attached_to_account_id OR $statutory_account_id == $attached_from_account_id) AND ($invoice_to_id == $provider_invoice_id OR $invoice_from_id == $provider_invoice_id) AND ($invoice_to_number == $invoice_number OR $invoice_from_number == $invoice_number) AND ($invoice_to_type == $invoice_type OR $invoice_from_type == $invoice_type) )
            {
              $result .=  '<option value="'.$provider_invoice_id.'...'.$invoice_number.'...'.$invoice_type.'...'.$statutory_account_id.'...3" style="background:'.$color_checked.';color:white;" selected> '.$statutory_account_name.' '.$invoice_date.' # '.$invoice_number.' Bill .'.number_format($dr_amount,2).' Payments.('.number_format($cr_amount,2).') Bal.'.number_format($balance,2).'</option>';
            }
            else
            {

              // var_dump($provider_invoice_id);die();
              if($balance > 0)
              {
                 $result .=  '<option value="'.$provider_invoice_id.'...'.$invoice_number.'...'.$invoice_type.'...'.$statutory_account_id.'...3" style="background:'.$color_checked.';color:white;"> '.$statutory_account_name.' '.$invoice_date.' # '.$invoice_number.' Bill .'.number_format($dr_amount,2).' Payments.('.number_format($cr_amount,2).') Bal.'.number_format($balance,2).'</option>';
              }

            }
          }
          else
          {
            if($balance > 0)
            {
                $result .= '<option value="'.$provider_invoice_id.'...'.$invoice_number.'...'.$invoice_type.'...'.$statutory_account_id.'...3" style="background:'.$color_checked.';color:white;"> '.$provider_invoice_type.' '.$invoice_date.' # '.$invoice_number.' Bill .'.number_format($dr_amount,2).' Payments.('.number_format($cr_amount,2).') Bal.'.number_format($balance,2).'</option>';
            }
          }
         
        }

         $data['result'] = $result;
      }

    }
    else
    {
      $data['message'] = 'fail';
    }

// var_dump($data['result']);die();
    echo json_encode($data);
     
  }

  public function journal_particulars($journal_entry_id)
  {
    $this->db->where('journal_entry_id',$journal_entry_id);

    $query = $this->db->get('journal_entry');

    if($query->num_rows() > 0)
    {
       foreach ($query->result() as $key => $value) {
         // code...

       }
    }

    echo json_encode($value);
  }


  // Create an option to make the adjustments for creditors, providers,payroll
  // Type 1 = Creditors, Type 2 = Providers, Type 3 Payroll 


  public function journal_adjustments($account_id=null,$type_id)
  {
      //form validation
      $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
      $this->form_validation->set_rules('account_to_id', 'Charge To','required|xss_clean');
      $this->form_validation->set_rules('amount', 'Amount','required|xss_clean');
      $this->form_validation->set_rules('description', 'Description','required|xss_clean');
      $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');


      if ($this->form_validation->run())
      {
        // var_dump("saksjaks");die();
        //update order
        if($this->transfer_model->add_journal_entry($type_id))
        {
          $this->session->set_userdata('success_message', 'Cheque successfully writted to account');

          if($type_id == 1)
            redirect('creditor-adjustments/'.$account_id);
          else if($type_id == 2)
            redirect('provider-adjustments/'.$account_id);
          else if($type_id == 3)
            redirect('statutory-adjustments/'.$account_id);
          else if($type_id == 4)
            redirect('journals/director-and-shareholders-journals');
          else if($type_id == 5)
            redirect('journals/providers-payable-journals');
          else if($type_id == 6)
            redirect('journals/creditors-payable-journals');
          else if($type_id == 7)
            redirect('journals/payroll-payable-journals');
          else if($type_id == 8)
            redirect('journals/intangible-assets');
          else if($type_id == 9)
            redirect('journals/fixed-assets-journals');
          else if($type_id == 10)
            redirect('accounting/journal-entry');
        }

        else
        {
          $this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
        }
      }
      else
      {
        $this->session->set_userdata('error_message', validation_errors());
      }




      // var_dump($accounts->result());die();
      $v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Expense Accounts");
      
      if($type_id >= 4)
        $where = 'journal_entry_deleted = 0 AND journal_entry.journal_type_id = '.$type_id.'  ';
      else
        $where = 'journal_entry_deleted = 0 AND journal_entry.journal_type_id = '.$type_id.' AND (attached_from_account_id = '.$account_id.' OR attached_to_account_id = '.$account_id.') ';


      $table = 'journal_entry';

      $journal = $this->session->userdata('search_journal');

      if(!empty($journal))
      {
        $where .= $journal;
      }

      if($type_id >= 4)
        $segment = 4;
      else
        $segment = 3;

      $this->load->library('pagination');

      if($type_id == 1)
          $config['base_url'] = site_url().'creditor-adjustments/'.$account_id;
      else if($type_id == 2)
          $config['base_url'] = site_url().'provider-adjustments/'.$account_id;
      else if($type_id == 3)
          $config['base_url'] = site_url().'statutory-adjustments/'.$account_id;
      else if($type_id == 4)
          $config['base_url'] = site_url().'journals/director-and-shareholders-journals';
      else if($type_id == 5)
          $config['base_url'] = site_url().'journals/providers-payable-journals';
      else if($type_id == 6)
          $config['base_url'] = site_url().'journals/creditors-payable-journals';
      else if($type_id == 7)
          $config['base_url'] = site_url().'journals/payroll-payable-journals';
      else if($type_id == 8)
          $config['base_url'] = site_url().'journals/intangible-assets';
      else if($type_id == 9)
          $config['base_url'] = site_url().'journals/fixed-assets-journals';


      $config['total_rows'] = $this->transfer_model->count_items($table, $where);
      $config['uri_segment'] = $segment;
      $config['per_page'] = 20;
      $config['num_links'] = 5;

      $config['full_tag_open'] = '<ul class="pagination pull-right">';
      $config['full_tag_close'] = '</ul>';

      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';

      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';

      $config['next_tag_open'] = '<li>';
      $config['next_link'] = 'Next';
      $config['next_tag_close'] = '</span>';

      $config['prev_tag_open'] = '<li>';
      $config['prev_link'] = 'Prev';
      $config['prev_tag_close'] = '</li>';

      $config['cur_tag_open'] = '<li class="active"><a href="#">';
      $config['cur_tag_close'] = '</a></li>';

      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $this->pagination->initialize($config);

      $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
          $v_data["links"] = $this->pagination->create_links();
      $query = $this->transfer_model->get_account_payments_transactions($table, $where, $config["per_page"], $page, $order='journal_entry.created', $order_method='DESC');
      // var_dump($query); die();
      if($type_id == 1)
          $data['title'] = 'Creditors Adjustments';
      else if($type_id == 2)
          $data['title'] = 'Providers Adjustments';
      else if($type_id == 3)
          $data['title'] = 'Payroll Adjustments';
      else if($type_id == 4)
          $data['title'] = 'Directors & Shareholders Journals';
      else if($type_id == 5)
          $data['title'] = 'Providers Payable Journals';
      else if($type_id == 6)
          $data['title'] = 'Creditors Payable Journals';
      else if($type_id == 7)
          $data['title'] = 'Payroll Payable Journals';
      else if($type_id == 8)
          $data['title'] = 'Intangible Assets Journals';
      else if($type_id == 9)
          $data['title'] = 'Fixed Assets Journals';
      else if($type_id == 9)
          $data['title'] = 'Others';



      if($type_id == 1)
        $account_from_id = $this->financials_accounts['accounts_payable_id'];
      else if($type_id == 2)
        $account_from_id = $this->financials_accounts['providers_liability_id'];
      else if($type_id == 3)
        $account_from_id = $this->financials_accounts['payroll_liability_id'];


    
      if($type_id == 4)
           $account_from_id = $this->financials_accounts['payroll_liability_id'];
      else if($type_id == 5)
          $account_from_id = $this->financials_accounts['providers_liability_id'];
      else if($type_id == 6)
          $account_from_id = $this->financials_accounts['accounts_payable_id'];
      else if($type_id == 7)
          $account_from_id = $this->financials_accounts['payroll_liability_id'];
      else if($type_id == 8)
          $account_from_id = $this->financials_accounts['fixed_assets_id'];
      else if($type_id == 9)
          $account_from_id = $this->financials_accounts['fixed_assets_id'];


      if($type_id < 3)
        $v_data['accounts'] = $accounts = $this->company_financial_model->get_child_accounts_by_parent_id($account_from_id);
      else if($type_id == 3)
         $v_data['accounts'] = $accounts = $this->company_financial_model->get_child_accounts_by_parent_id(2);
      else if($type_id == 4)
          // $v_data['accounts'] = $accounts = $this->company_financial_model->get_child_accounts_by_parent_id(2);
         $v_data['accounts'] = $accounts = $this->purchases_model->get_child_accounts('Bank#Directors Account');
      else if($type_id > 4)
         $v_data['accounts'] = $accounts = $this->company_financial_model->get_child_accounts_by_parent_id($account_from_id);


        // var_dump($account_id);die();
      if($type_id >= 4)
      {
        $v_data['other_accounts'] = $other_accounts = $this->purchases_model->get_child_accounts('Bank#Directors Account');
          // $v_data['other_accounts'] = $other_accounts = $this->company_financial_model->get_child_accounts_by_parent_id($account_from_id);
          // $account_id = $account_from_id;
                 // var_dump($other_accounts->result());die();
      }
      else
      {
         $v_data['other_accounts'] = $other_accounts = $this->company_financial_model->get_accounts_list();
      }
        

      //open the add new order
      
       // var_dump($other_accounts);die();

       $v_data['account_from_id'] = $account_from_id;
      
      $v_data['title'] = $data['title'];

      $v_data['query'] = $query;
      $v_data['attached_account_id'] = $account_id;
      // $v_data['account_from_idd'] = $account_id;
      // var_dump($account_id);die();
      $v_data['page'] = $page;
      $v_data['type_id'] = $type_id;

      // $data['title'] = $v_data['title']= 'Journal Entry';

      $data['content'] = $this->load->view('transfer/adjustments_journals', $v_data, true);
      $this->load->view('admin/templates/general_page', $data);
    }
    public function edit_journal_adjustments($account_id)
    {

      $journal_details = $this->transfer_model->get_journal_details($account_id);

      if($journal_details->num_rows() > 0)
      {
        foreach ($journal_details->result() as $key => $value) {
          # code...
          $account_from_id = $value->account_from_id;
          $account_to_id = $value->account_to_id;
          //$account_payment_description = $value->account_payment_description;
          $amount_paid = $value->amount_paid;
          $account_payment_description = $value->journal_entry_description;
          //$receipt_number = $value->receipt_number;
          $payment_date = $value->payment_date;
          $document_id = $value->document_number;
          $journal_type_id = $value->journal_type_id;
        }
      }

      // $data['accounts'] = $accounts = $this->purchases_model->get_all_accounts();

      // $data['other_accounts'] = $this->company_financial_model->get_accounts_list();
       $data['account_id'] = $account_id;
       $data['journal_details'] = $journal_details;


       $parent_account_id = $this->company_financial_model->get_parent_account_by_id($account_from_id);
      // if($journal_type_id != 3)
      //   $data['accounts'] = $accounts = $this->company_financial_model->get_child_accounts_by_parent_id($account_from_id);
      // else
      //    $data['accounts'] = $accounts = $this->company_financial_model->get_child_accounts_by_parent_id(2);
      if($journal_type_id < 3)
        $data['accounts'] = $accounts = $this->company_financial_model->get_child_accounts_by_parent_id($account_from_id);
      else if($journal_type_id == 3)
         $data['accounts'] = $accounts = $this->company_financial_model->get_child_accounts_by_parent_id(2);
      else if($journal_type_id >= 4)
          // $data['accounts'] = $accounts = $this->company_financial_model->get_child_accounts_by_parent_id(2);
         $data['accounts'] = $accounts = $this->company_financial_model->get_child_accounts_by_parent_id($parent_account_id);



         // var_dump($account_from_id);die();
      if($journal_type_id >= 4)
      {
        $data['other_accounts'] = $other_accounts = $this->company_financial_model->get_child_accounts_by_parent_id(2);
      }
      else
      {
         $data['other_accounts'] = $other_accounts = $this->company_financial_model->get_accounts_list();
      }



      // var_dump($account_from_id);die();
      $page = $this->load->view('finance/transfer/edit_journal_adjustments',$data,true);
      // var_dump($page);die();
      echo $page;
    }
}
?>
