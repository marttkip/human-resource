<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require_once "./application/modules/admin/controllers/admin.php";
date_default_timezone_set('Africa/Nairobi');
error_reporting(E_ALL);


class Creditors extends admin
{
  var $configuration_array;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('creditors_model');
		$this->load->model('purchases_model');
    $this->load->model('transfer_model');
    $this->load->model('financials/company_financial_model');
    $this->load->model('accounts/accounts_model');


    $configuration_rs = $this->site_model->get_company_configuration();

    $this->configuration_array = array();
    foreach ($configuration_rs->result() as $key) {
      // code...
      $this->configuration_array[$key->company_configuration_name] = $key;
    }

	}



  public function creditors_invoices()
  {
    // $v_data['property_list'] = $property_list;


   
    $creditor_id = $this->session->userdata('invoice_creditor_id_searched');


    $where = 'creditor_invoice.creditor_invoice_status = 1 AND creditor_invoice.creditor_id = '.$creditor_id;

    $search_purchases = $this->session->userdata('search_purchases');
    if($search_purchases)
    {
      $where .= $search_purchases;
    }
    $table = 'creditor_invoice';


    $segment = 3;
    $this->load->library('pagination');
    $config['base_url'] = site_url().'accounting/creditor-invoices';
    $config['total_rows'] = $this->purchases_model->count_items($table, $where);
    $config['uri_segment'] = $segment;
    $config['per_page'] = 20;
    $config['num_links'] = 5;

    $config['full_tag_open'] = '<ul class="pagination pull-right">';
    $config['full_tag_close'] = '</ul>';

    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';

    $config['next_tag_open'] = '<li>';
    $config['next_link'] = 'Next';
    $config['next_tag_close'] = '</span>';

    $config['prev_tag_open'] = '<li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);

    $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
    $query = $this->creditors_model->get_all_creditors_details($table, $where, $config["per_page"], $page, $order='creditor_invoice.transaction_date', $order_method='DESC');
    $v_data['creditor_invoices'] = $query;
    $v_data['page'] = $page;
    $data['title'] = 'Creditor Invoices';
    $v_data['title'] = $data['title'];
    $data['content'] = $this->load->view('creditors/creditors_statement', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }


  public function search_creditors_invoice()
  {
    // var_dump($_POST);die();
    $creditor_id = $creditor_id_searched = $this->input->post('creditor_id');

    $search_title = '';

    if(!empty($creditor_id))
    {

      $creditor_id = ' AND creditor.creditor_id = '.$creditor_id.' ';
    }
    $search = $creditor_id;

    $this->session->set_userdata('invoice_creditor_id_searched', $creditor_id_searched);
    $this->session->set_userdata('search_creditors_invoice', $search);
    redirect('accounting/creditor-invoices');
  }

  public function search_creditors_bill($creditor_id)
  {

    $creditor_id = $creditor_id_searched = $creditor_id;

    $search_title = '';

    if(!empty($creditor_id))
    {

      $creditor_id = ' AND creditor.creditor_id = '.$creditor_id.' ';
    }
    $search = $creditor_id;

    $this->session->set_userdata('invoice_creditor_id_searched', $creditor_id_searched);
    $this->session->set_userdata('search_creditors_invoice', $search);
    redirect('accounting/creditor-invoices');
  }

  public function close_searched_invoices_creditor()
  {
    $this->session->unset_userdata('invoice_creditor_id_searched');
    $this->session->unset_userdata('search_creditors_invoice');
    redirect('accounting/creditors');
  }
  public function calculate_value()
  {
    $quantity = $this->input->post('quantity');
    $tax_type_id = $this->input->post('tax_type_id');
    $unit_price = $this->input->post('unit_price');


    if(empty($quantity))
    {
      $quantity = 1;
    }
    if(empty($unit_price))
    {
      $unit_price = 0;
    }
    if(empty($tax_type_id))
    {
      $tax_type_id = 0;
    }

    if($tax_type_id == 0)
    {
      $total_amount = $unit_price *$quantity;
      $vat = 0;
    }
    if($tax_type_id == 1)
    {
      $total_amount = ($unit_price * $quantity)*1.16;
      $vat = ($unit_price * $quantity)*0.16;
    }
    else if($tax_type_id == 2)
    {
      $total_amount = ($unit_price * $quantity)*1.05;
      $vat = ($unit_price * $quantity)*0.05;
    }

    else if($tax_type_id == 3)
    {
      $total_amount = ($unit_price * $quantity);
      $vat = ($unit_price * $quantity)*0.16;
    }


    else if($tax_type_id == 4)
    {
      $total_amount = ($unit_price * $quantity);
      $vat = ($unit_price * $quantity)*0.05;
    }

    $response['message'] = 'success';
    $response['amount'] = $total_amount;
    $response['vat'] = $vat;

    echo json_encode($response);


  }

  public function add_invoice_item($creditor_id,$creditor_invoice_id = NULL)
  {

    $this->form_validation->set_rules('quantity', 'Invoice Item', 'trim|required|xss_clean');
    $this->form_validation->set_rules('unit_price', 'Unit Price', 'trim|required|xss_clean');
    $this->form_validation->set_rules('account_to_id', 'Expense Account', 'trim|required|xss_clean');
    $this->form_validation->set_rules('item_description', 'Item', 'trim|required|xss_clean');
    $this->form_validation->set_rules('tax_type_id', 'VAT Type', 'trim|xss_clean');
    $this->form_validation->set_rules('vat_amount', 'VAT Amount', 'trim|xss_clean');
    $this->form_validation->set_rules('total_amount', 'Total Amount', 'trim|xss_clean');

    //if form conatins invalid data
    if ($this->form_validation->run())
    {
			// var_dump($_POST);die();
      $this->creditors_model->add_invoice_item($creditor_id,$creditor_invoice_id);
      $this->session->set_userdata("success_message", 'Invoice Item successfully added');
      $response['status'] = 'success';
      $response['message'] = 'Payment successfully added';
    }
    else
    {
      $this->session->set_userdata("error_message", validation_errors());
      $response['status'] = 'fail';
      $response['message'] = validation_errors();

    }
    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);

  }

  public function confirm_invoice_note($creditor_id,$creditor_invoice_id = NULL)
  {
    $this->form_validation->set_rules('vat_charged', 'tax', 'trim|xss_clean');
		$this->form_validation->set_rules('amount_charged', 'Amount Charged', 'trim|xss_clean');
    $this->form_validation->set_rules('invoice_date', 'Invoice Date ', 'trim|required|xss_clean');
    $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
    $this->form_validation->set_rules('invoice_number', 'Invoice Number', 'trim|required|xss_clean');
		if ($this->form_validation->run())
		{
			// var_dump($_POST);die();
				$this->creditors_model->confirm_creditor_invoice($creditor_id,$creditor_invoice_id);

				$this->session->set_userdata("success_message", 'Creditor invoice successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}


    if(!empty($creditor_invoice_id))
    {
        redirect('accounting/creditor-invoices');
    }
    else
    {
      $redirect_url = $this->input->post('redirect_url');
      redirect($redirect_url);
    }

        
  }


  // credit notes

  public function creditors_credit_note()
  {
    // $v_data['property_list'] = $property_list;


     $creditor_id = $this->session->userdata('credit_note_creditor_id_searched');


    $where = 'creditor_credit_note.creditor_credit_note_status = 1 AND creditor_credit_note.creditor_id = '.$creditor_id;

    $search_purchases = $this->session->userdata('search_purchases');
    if($search_purchases)
    {
      $where .= $search_purchases;
    }
    $table = 'creditor_credit_note';


    $segment = 3;
    $this->load->library('pagination');
    $config['base_url'] = site_url().'accounting/creditor-credit-notes';
    $config['total_rows'] = $this->purchases_model->count_items($table, $where);
    $config['uri_segment'] = $segment;
    $config['per_page'] = 20;
    $config['num_links'] = 5;

    $config['full_tag_open'] = '<ul class="pagination pull-right">';
    $config['full_tag_close'] = '</ul>';

    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';

    $config['next_tag_open'] = '<li>';
    $config['next_link'] = 'Next';
    $config['next_tag_close'] = '</span>';

    $config['prev_tag_open'] = '<li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);

    $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
    $query = $this->creditors_model->get_all_creditors_details($table, $where, $config["per_page"], $page, $order='creditor_credit_note.transaction_date', $order_method='DESC');
    $v_data['creditor_credit_notes'] = $query;
    $v_data['page'] = $page;

    $data['title'] = 'Creditor Credit Notes';
    $v_data['title'] = $data['title'];
    $data['content'] = $this->load->view('creditors/creditors_credit_notes', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }


  public function search_creditors_credit_notes($creditor_id=null)
  {
    if(!empty($creditor_id))
    {
      $creditor_id = $creditor_id_searched = $creditor_id;
    }
    else
    {
      $creditor_id = $creditor_id_searched = $this->input->post('creditor_id');
    }



    $search_title = '';

    if(!empty($creditor_id))
    {

      $creditor_id = ' AND creditor.creditor_id = '.$creditor_id.' ';
    }
    $search = $creditor_id;
      // var_dump($creditor_id_searched);die();
    $this->session->set_userdata('credit_note_creditor_id_searched', $creditor_id_searched);
    $this->session->set_userdata('search_creditors_credit_notes', $search);
    redirect('accounting/creditor-credit-notes');
  }

  public function close_searched_credit_notes_creditor()
  {
    $this->session->unset_userdata('credit_note_creditor_id_searched');
    $this->session->unset_userdata('search_creditors_credit_notes');
    redirect('accounting/creditors');
  }

  public function add_credit_note_item($creditor_id,$creditor_credit_note_id=NULL)
  {

    $this->form_validation->set_rules('amount', 'Unit Price', 'trim|required|xss_clean');
    $this->form_validation->set_rules('account_to_id', 'Invoice', 'trim|required|xss_clean');
    $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
    $this->form_validation->set_rules('tax_type_id', 'VAT Type', 'trim|xss_clean');

    //if form conatins invalid data
    if ($this->form_validation->run())
    {
      $this->creditors_model->add_credit_note_item($creditor_id,$creditor_credit_note_id);
      $this->session->set_userdata("success_message", 'Invoice Item successfully added');
      $response['status'] = 'success';
      $response['message'] = 'Payment successfully added';
    }
    else
    {
      $this->session->set_userdata("error_message", validation_errors());
      $response['status'] = 'fail';
      $response['message'] = validation_errors();

    }
    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);

  }

  public function confirm_credit_note($creditor_id,$creditor_credit_note_id=NULL)
  {
    $this->form_validation->set_rules('vat_charged', 'tax', 'trim|xss_clean');
		$this->form_validation->set_rules('amount_charged', 'Amount Charged', 'trim|xss_clean');
    $this->form_validation->set_rules('credit_note_date', 'Invoice Date ', 'trim|required|xss_clean');
    $this->form_validation->set_rules('invoice_id', 'Invoice ', 'trim|required|xss_clean');
    $this->form_validation->set_rules('amount', 'Amount', 'trim|xss_clean');
    $this->form_validation->set_rules('credit_note_number', 'Invoice Number', 'trim|xss_clean');

		if ($this->form_validation->run())
		{
        // var_dump($_POST);die();
				$this->creditors_model->confirm_creditor_credit_note($creditor_id,$creditor_credit_note_id);

				$this->session->set_userdata("success_message", 'Creditor invoice successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}

    if(!empty($creditor_credit_note_id))
    {
     
      redirect('accounting/creditor-credit-notes');
    }
    else
    {
      $redirect_url = $this->input->post('redirect_url');
      redirect($redirect_url);
    }
  }

  // creditors payments_import


  public function creditors_payments()
  {
    // $v_data['property_list'] = $property_list;
      $creditor_id = $this->session->userdata('payment_creditor_id_searched');


    $where = 'creditor_payment.creditor_payment_status = 1 AND account.account_id = creditor_payment.account_from_id AND creditor_payment.creditor_id = '.$creditor_id;

    $search_purchases = $this->session->userdata('search_purchases');
    if($search_purchases)
    {
      $where .= $search_purchases;
    }
    $table = 'creditor_payment,account';

 // $this->db->join('account','account.account_id = creditor_payment.account_from_id','left');

    $segment = 3;
    $this->load->library('pagination');
    $config['base_url'] = site_url().'accounting/creditor-payments';
    $config['total_rows'] = $this->purchases_model->count_items($table, $where);
    $config['uri_segment'] = $segment;
    $config['per_page'] = 20;
    $config['num_links'] = 5;

    $config['full_tag_open'] = '<ul class="pagination pull-right">';
    $config['full_tag_close'] = '</ul>';

    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';

    $config['next_tag_open'] = '<li>';
    $config['next_link'] = 'Next';
    $config['next_tag_close'] = '</span>';

    $config['prev_tag_open'] = '<li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $this->pagination->initialize($config);

    $page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
    $query = $this->creditors_model->get_all_creditors_details($table, $where, $config["per_page"], $page, $order='creditor_payment.transaction_date', $order_method='DESC');
    $v_data['creditor_payments'] = $query;
    $v_data['page'] = $page;
    $data['title'] = 'Creditor Payments';

    $v_data['title'] = $data['title'];
    $data['content'] = $this->load->view('creditors/creditors_payments', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }


  public function search_creditors_payments($creditor_id=null,$creditor_payment_id=null)
  {
    if(!empty($creditor_id))
    {
       $creditor_id = $creditor_id_searched = $creditor_id;
    }
    else
    {
       $creditor_id = $creditor_id_searched = $this->input->post('creditor_id');
    }



    $search_title = '';

    if(!empty($creditor_id))
    {

      $creditor_id = ' AND creditor_payment.creditor_id = '.$creditor_id.' ';
    }

    if(!empty($creditor_payment_id))
    {

      $creditor_payment_id = ' AND creditor_payment.creditor_payment_id = '.$creditor_payment_id.' ';
    }
    $search = $creditor_id.$creditor_payment_id;
      // var_dump($creditor_id_searched);die();
    $this->session->set_userdata('payment_creditor_id_searched', $creditor_id_searched);
    $this->session->set_userdata('search_creditors_payments', $search);
    redirect('accounting/creditor-payments');
  }

  public function close_searched_payments_creditor()
  {
    $this->session->unset_userdata('payment_creditor_id_searched');
    $this->session->unset_userdata('search_creditors_payments');
    redirect('accounting/creditors');
  }
  public function add_payment_item($creditor_id,$creditor_payment_id = NULL)
  {

    $this->form_validation->set_rules('amount_paid', 'Unit Price', 'trim|required|xss_clean');
    // $this->form_validation->set_rules('invoice_id', 'Invoice', 'trim|required|xss_clean');

    //if form conatins invalid data
    if ($this->form_validation->run())
    {

      $this->creditors_model->add_payment_item($creditor_id,$creditor_payment_id);
      $this->session->set_userdata("success_message", 'Payment successfully added');
      $response['status'] = 'success';
      $response['message'] = 'Payment successfully added';
    }
    else
    {
      $this->session->set_userdata("error_message", validation_errors());
      $response['status'] = 'fail';
      $response['message'] = validation_errors();

    }
    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);

  }

  public function confirm_payment($creditor_id,$creditor_payment_id=NULL)
  {
    $this->form_validation->set_rules('reference_number', 'Reference Number', 'trim|required|xss_clean');
    $this->form_validation->set_rules('payment_date', 'Invoice Date ', 'trim|required|xss_clean');
    $this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
    $this->form_validation->set_rules('account_from_id', 'Payment Amount', 'trim|required|xss_clean');

    if ($this->form_validation->run())
    {
        // var_dump($_POST);die();


        $status = $this->creditors_model->confirm_creditor_payment($creditor_id,$creditor_payment_id);

        if($status == TRUE)
        {

          if(!empty($creditor_payment_id))
          {
            $this->session->set_userdata("success_message", 'Payment has been successfully updated');
            $response['status'] = 'success';
            $response['message'] = 'Payment successfully added';
            $redirect_url = $this->input->post('redirect_url');
            redirect('creditor-statement/'.$creditor_id);

          }
          else
          {
            $this->session->set_userdata("success_message", 'Creditor invoice successfully added');
            $response['status'] = 'success';
            $response['message'] = 'Payment successfully added';
            $redirect_url = $this->input->post('redirect_url');
            redirect($redirect_url);
          }

        }
        else
        {
          $this->session->set_userdata("success_message", 'Sorry could not make the update. Please try again');
          $response['status'] = 'success';
          $response['message'] = 'Payment successfully added';
          $redirect_url = $this->input->post('redirect_url');
          redirect($redirect_url);
        }
        

    }
    else
    {
      $this->session->set_userdata("error_message", validation_errors());
      $response['status'] = 'fail';
      $response['message'] = validation_errors();

    }


    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);

    


  }


  /*
  *
  * Add a new creditor
  *
  */
  public function add_creditor()
  {
    //form validation rules
    $this->form_validation->set_rules('creditor_name', 'Name', 'required|xss_clean');
    $this->form_validation->set_rules('creditor_email', 'Email', 'xss_clean');
    $this->form_validation->set_rules('creditor_phone', 'Phone', 'xss_clean');
    $this->form_validation->set_rules('creditor_location', 'Location', 'xss_clean');
    $this->form_validation->set_rules('creditor_building', 'Building', 'xss_clean');
    $this->form_validation->set_rules('creditor_floor', 'Floor', 'xss_clean');
    $this->form_validation->set_rules('creditor_address', 'Address', 'xss_clean');
    $this->form_validation->set_rules('creditor_post_code', 'Post code', 'xss_clean');
    $this->form_validation->set_rules('creditor_city', 'City', 'xss_clean');
    $this->form_validation->set_rules('creditor_contact_person_name', 'Contact Name', 'xss_clean');
    $this->form_validation->set_rules('creditor_contact_person_onames', 'Contact Other Names', 'xss_clean');
    $this->form_validation->set_rules('creditor_contact_person_phone1', 'Contact Phone 1', 'xss_clean');
    $this->form_validation->set_rules('creditor_contact_person_phone2', 'Contact Phone 2', 'xss_clean');
    $this->form_validation->set_rules('creditor_contact_person_email', 'Contact Email', 'valid_email|xss_clean');
    $this->form_validation->set_rules('creditor_description', 'Description', 'xss_clean');
    $this->form_validation->set_rules('balance_brought_forward', 'Balance BroughtF','xss_clean');
    $this->form_validation->set_rules('debit_id', 'Balance BroughtF','xss_clean');

    // var_dump($_POST); die();
    //if form conatins invalid data
    if ($this->form_validation->run())
    {
      $creditor_id = $this->creditors_model->add_creditor();
      if($creditor_id > 0)
      {
        $this->session->set_userdata("success_message", "Creditor added successfully");
        $redirect_url = $this->input->post('redirect_url');
        if(!empty($redirect_url))
        {
          redirect($redirect_url);
        }
        else
        {
          redirect('accounting/creditors');
        }
      }

      else
      {
        $this->session->set_userdata("error_message","Could not add creditor. Please try again");

        $redirect_url = $this->input->post('redirect_url');
        if(!empty($redirect_url))
        {
          redirect($redirect_url);
        }
        else
        {
          redirect('accounting/creditors');
        }
      }
    }
    $data['title'] = 'Add creditor';
    $v_data['title'] = $data['title'];
    $data['content'] = $this->load->view('creditors/add_creditor', $v_data, true);

    $this->load->view('admin/templates/general_page', $data);
  }

  /*
  *
  * Add a new creditor
  *
  */
  public function edit_creditor($creditor_id)
  {
    //form validation rules
    $this->form_validation->set_rules('creditor_name', 'Name', 'required|xss_clean');
    $this->form_validation->set_rules('creditor_email', 'Email', 'xss_clean');
    $this->form_validation->set_rules('creditor_phone', 'Phone', 'xss_clean');
    $this->form_validation->set_rules('creditor_location', 'Location', 'xss_clean');
    $this->form_validation->set_rules('creditor_building', 'Building', 'xss_clean');
    $this->form_validation->set_rules('creditor_floor', 'Floor', 'xss_clean');
    $this->form_validation->set_rules('creditor_address', 'Address', 'xss_clean');
    $this->form_validation->set_rules('creditor_post_code', 'Post code', 'xss_clean');
    $this->form_validation->set_rules('creditor_city', 'City', 'xss_clean');
    $this->form_validation->set_rules('creditor_contact_person_name', 'Contact Name', 'xss_clean');
    $this->form_validation->set_rules('creditor_contact_person_onames', 'Contact Other Names', 'xss_clean');
    $this->form_validation->set_rules('creditor_contact_person_phone1', 'Contact Phone 1', 'xss_clean');
    $this->form_validation->set_rules('creditor_contact_person_phone2', 'Contact Phone 2', 'xss_clean');
    $this->form_validation->set_rules('creditor_contact_person_email', 'Contact Email', 'valid_email|xss_clean');
    $this->form_validation->set_rules('creditor_description', 'Description', 'xss_clean');
    $this->form_validation->set_rules('balance_brought_forward', 'Balance BroughtF','xss_clean');
    $this->form_validation->set_rules('debit_id', 'Balance BroughtF','xss_clean');

    //if form conatins invalid data
    if ($this->form_validation->run())
    {
      $creditor_id = $this->creditors_model->edit_creditor($creditor_id);
      if($creditor_id > 0)
      {
        $this->session->set_userdata("success_message", "Creditor updated successfully");
        redirect('accounting/creditors');
      }

      else
      {
        $this->session->set_userdata("error_message","Could not add creditor. Please try again");
        redirect('finance/edit-creditor/'.$creditor_id);
      }
    }
    $data['title'] = 'Add creditor';
    $v_data['title'] = $data['title'];
    $v_data['creditor'] = $this->creditors_model->get_creditor($creditor_id);
    $data['content'] = $this->load->view('creditors/edit_creditor', $v_data, true);

    $this->load->view('admin/templates/general_page', $data);
  }






  public function creditors_list()
  {
    // $this->output->delete_cache();
    $data['title'] = 'Vendor Expenses';
    $v_data['title'] = $data['title'];

    $report_config = $this->configuration_array;

    // $this->creditors_model->calculate_creditors_aging_report($report_type);

    $report_detail = $this->session->userdata('report_detail');

    if(empty($report_detail))
    {
      $this->session->set_userdata('report_detail','180');
    }

    $v_data['report_config'] = $report_config;
    $v_data['query_list'] = $this->creditors_model->get_all_vendors($report_config);

		// var_dump($v_date); die();
    $data['content'] = $this->load->view('creditors/creditors_accounts', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }
	public function delete_creditor_payment_item($creditor_payment_item_id,$creditor_id,$creditor_payment_id = NULL)
	{
		$this->db->where('creditor_payment_item_id',$creditor_payment_item_id);
		$this->db->delete('creditor_payment_item');

    if(!empty($creditor_payment_id))
    {
        redirect('edit-creditor-payment/'.$creditor_payment_id);
    }
		else
    {
      redirect('accounting/creditor-payments');
    }
	}


	public function delete_creditor_invoice_item($creditor_invoice_item_id,$creditor_id,$creditor_invoice_id = NUll)
	{
		$this->db->where('creditor_invoice_item_id',$creditor_invoice_item_id);
		$this->db->delete('creditor_invoice_item');

    if(!empty($creditor_invoice_id))
    {
        redirect('creditor-invoice/edit-creditor-invoice/'.$creditor_invoice_id);
    }
    else
    {
          redirect('accounting/creditor-invoices');
    }
		
	}


  public function allocate_creditor_payment($creditor_payment_id,$creditor_payment_item_id,$creditor_id)
  {


    $data['title'] = 'Allocating Creditor Payment';
    $v_data['title'] = $data['title'];
    $v_data['creditor_id'] = $creditor_id;
    $v_data['creditor_payment_id'] = $creditor_payment_id;
    $v_data['creditor_payment_item_id'] = $creditor_payment_item_id;


    // var_dump($v_date); die();
    $data['content'] = $this->load->view('creditors/allocate_creditor_payment', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
  }


  public function delete_payment_item($creditor_payment_id,$creditor_payment_item_id,$creditor_payment_item_id_db,$creditor_id)
  {

    // var_dump($creditor_payment_item_id_db);die();
    $this->db->where('creditor_payment_item_id',$creditor_payment_item_id_db);
    $this->db->delete('creditor_payment_item');

    redirect('allocate-payment/'.$creditor_payment_id.'/'.$creditor_payment_item_id.'/'.$creditor_id);
  }
  

 public function search_creditors()
  {
    $creditor_name = $year_from = $this->input->post('creditor_name');
    $redirect_url = $this->input->post('redirect_url');
    $creditor_search = '';
     if(!empty($creditor_name))
    {
      $creditor_search .= 'payable LIKE \'%'.$creditor_name.'%\'';

    }

    $search = $creditor_search;



    $visit_type_name = $this->input->post('visit_type_name');
    $date_to = $this->input->post('date_to');
    $date_from = $this->input->post('date_from');
    $interval_limit = $this->input->post('interval_limit');
    $report_type = $this->input->post('report_type');
    // var_dump($_POST);die();
    if(!empty($creditor_name))
    {

      $this->session->set_userdata('search_creditors_batches', ' AND payables LIKE \'%'.$creditor_name.'%\'');
    }

    if(!empty($date_from))
    {
      $this->session->set_userdata('date_creditors_batches', $date_from);
    }

    if(!empty($date_to))
    {
      $this->session->set_userdata('date_creditors_batches_to', $date_to);
    }

    if(!empty($interval_limit))
    {
      $this->session->set_userdata('report_detail', $interval_limit);
    }


    if(!empty($report_type))
    {
      $this->session->set_userdata('report_creditor_type', $report_type);
    }
    $this->session->set_userdata('creditors_account_search', true);


    // if(!empty($visit_date_from) && !empty($visit_date_to))
    // {
    //  $visit_payments = ' AND data.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
    //  $visit_invoices = ' AND data.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
    //  $search_title .= date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
    // }
    
    // else if(!empty($visit_date_from))
    // {
    //  $visit_payments = ' AND data.transaction_date = \''.$visit_date_from.'\'';
    //  $visit_invoices = ' AND data.transaction_date <= \''.$visit_date_from.'\'';
    //  $search_title .= date('jS M Y', strtotime($visit_date_from)).' ';
    // }
    
    // else if(!empty($visit_date_to))
    // {
    //  $visit_payments = ' AND data.transaction_date = \''.$visit_date_to.'\'';
    //  $visit_invoices = ' AND data.transaction_date <= \''.$visit_date_to.'\'';
    //  $search_title .= date('jS M Y', strtotime($visit_date_to)).' ';
    // }
    
    // else
    // {
    //  $visit_payments = '';
    //  $visit_invoices = '';
    // }


    

    

    // var_dump($date_to); die();
    // $this->session->set_userdata('creditor_search',$search);
    redirect($redirect_url);

  }
  public function close_creditor_creditor_search()
  {

    $this->session->unset_userdata('creditor_search');
    $this->session->unset_userdata('search_creditors_batches');
    $this->session->unset_userdata('date_creditors_batches');
    $this->session->unset_userdata('date_creditors_batches_to');
    $this->session->set_userdata('creditors_account_search', false);
    $this->session->unset_userdata('report_creditor_type');
    $this->session->unset_userdata('report_detail');

     $this->session->set_userdata('report_detail', 180);
    redirect('accounting/creditors');

  }

  public function get_invoice_details($creditor_invoice_id)
  {
    $data['creditor_invoice_id'] = $creditor_invoice_id;
    $this->load->view('creditors/view_creditor_invoice', $data);  
  }


  public function get_suppliers_invoice_details($order_id)
  {
    $data['order_id'] = $order_id;
    $this->load->view('creditors/view_supplier_invoice', $data);  
  }
  public function get_payment_details($creditor_payment_id)
  {
    $data['creditor_payment_id'] = $creditor_payment_id;
    $this->load->view('creditors/view_creditor_payment', $data);  
  }

  public function delete_creditor_invoice($creditor_invoice_id)
  {
    $update_query['creditor_invoice_status'] = 2;
    $update_query['last_modified_by'] = $this->session->userdata('personnel_id');
    $this->db->where('creditor_invoice_id',$creditor_invoice_id);
    $this->db->update('creditor_invoice',$update_query);

    redirect('accounting/creditor-invoices');
  }

  public function edit_creditor_invoice($creditor_invoice_id)
  {

      $data['title'] = 'Edit Creditor Invoice';
      $v_data['creditor_invoice_id'] = $creditor_invoice_id;
      $creditor_id = $this->session->userdata('invoice_creditor_id_searched');
      $v_data['title'] = $data['title'];
      $data['content'] = $this->load->view('creditors/edit_creditor_invoice', $v_data, true);
      $this->load->view('admin/templates/general_page', $data);

  }


  public function delete_creditor_payment($creditor_payment_id)
  {
    $update_query['creditor_payment_status'] = 2;
    $update_query['last_modified_by'] = $this->session->userdata('personnel_id');
    $this->db->where('creditor_payment_id',$creditor_payment_id);
    $this->db->update('creditor_payment',$update_query);

    redirect('accounting/creditor-payments');
  }

  public function edit_creditor_payment($creditor_payment_id)
  {

      $data['title'] = 'Edit Creditor Invoice';
      $v_data['creditor_payment_id'] = $creditor_payment_id;
      $creditor_id = $this->session->userdata('payment_creditor_id_searched');
      $v_data['title'] = $data['title'];
      $data['content'] = $this->load->view('creditors/edit_creditor_payment', $v_data, true);
      $this->load->view('admin/templates/general_page', $data);

  }

   public function delete_credit_note_item($creditor_credit_note_item_id,$creditor_credit_note_id=NULL)
  {
   
    $this->db->where('creditor_credit_note_item_id',$creditor_credit_note_item_id);
    $this->db->delete('creditor_credit_note_item',$update_query);

    if(!empty($creditor_credit_note_id))
    {
      redirect('edit-creditor-credit-note/'.$creditor_credit_note_id);
    }
    else
    {
      redirect('accounting/creditor-credit-notes');
    }
  }

  public function delete_creditor_credit_note($creditor_credit_note_id,$creditor_id)
  {


    $update_query['creditor_credit_note_status'] = 2;
    $update_query['last_modified_by'] = $this->session->userdata('personnel_id');
    $this->db->where('creditor_credit_note_id',$creditor_credit_note_id);
    $this->db->update('creditor_credit_note',$update_query);

    redirect('accounting/creditor-credit-notes');
  }
  public function edit_creditor_credit_note($creditor_credit_note_id)
  {
     $data['title'] = 'Edit Creditor Credit Note';
      $v_data['creditor_credit_note_id'] = $creditor_credit_note_id;
      $creditor_id = $this->session->userdata('credit_note_creditor_id_searched');
      $v_data['title'] = $data['title'];
      $data['content'] = $this->load->view('creditors/edit_creditor_credit_note', $v_data, true);
      $this->load->view('admin/templates/general_page', $data);
  }

  public function get_credit_note_details($creditor_credit_note_id)
  {
    $data['creditor_credit_note_id'] = $creditor_credit_note_id;
    $this->load->view('creditors/view_creditor_credit_notes', $data); 
  }

  public function delete_creditor($creditor_id)
  {
      $creditor_entry['creditor_status'] = 1;
      $this->db->where('creditor_id',$creditor_id);
      $this->db->update('creditor',$creditor_entry);

      redirect('accounting/creditors');
  }

   public function export_payables()
  {
    $this->creditors_model->export_payables();
  }
  public function print_creditor_payment($creditor_payment_id)
  {


      $payment_voucher_details = $this->creditors_model->get_creditor_payment_details($creditor_payment_id);
    $v_data['title'] = 'Supplier Payments';
    $data['title'] = 'Supplier Payments';
      $v_data['contacts'] = $this->site_model->get_contacts();
    //$v_data['paid_amount'] = $amount_to_reconcille;
    $v_data['payment_voucher_details'] = $payment_voucher_details;
    // $v_data['transaction_code'] = $transaction_code;
    // $v_data['payment_date'] = $trans_date;
    // $v_data['accounts'] = $this->petty_cash_model->get_child_accounts("Bank");

    // var_dump($creditor_id);die();

    $this->load->view('creditors/print_advise', $v_data);


    }
}
?>
