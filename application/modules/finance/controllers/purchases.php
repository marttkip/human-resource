<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";
error_reporting(0);
class Purchases extends admin
{
	function __construct()
	{
		parent:: __construct();

    $this->load->model('finance/purchases_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('messaging/messaging_model');
		$this->load->model('financials/company_financial_model');
	}

	public function all_purchases()
	{
		//form validation
		// $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
		$this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
    $this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
		// $this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
		// $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');

		if ($this->form_validation->run())
		{
			//update order
			if($this->purchases_model->add_payment_amount())
			{
				$this->session->set_userdata('success_message', 'Cheque successfully writted to account');


				redirect('accounting/purchases');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}



		//open the add new order
		$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
    $v_data['creditors'] = $this->purchases_model->get_creditor();

    $v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Expense Accounts");

		$where = 'finance_purchase_id > 0 and finance_purchase_delete = 0 ';

    $search_purchases = $this->session->userdata('search_purchases');
    if($search_purchases)
    {
      $where .= $search_purchases;
    }
		$table = 'finance_purchase';


		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounting/purchases';
		$config['total_rows'] = $this->purchases_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->purchases_model->get_account_payments_transactions($table, $where, $config["per_page"], $page, $order='finance_purchase.created', $order_method='DESC');
		// var_dump($table); die();

		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];

		$v_data['query_purchases'] = $query;
		$v_data['page'] = $page;

		$data['title'] = $v_data['title']= 'Purchases';

		$data['content'] = $this->load->view('purchases/purchases', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
  public function purchase_payments()
  {
    //form validation
		// $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
		$this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
    $this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
		// $this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
		// $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');

		if ($this->form_validation->run())
		{
			//update order
			if($this->purchases_model->add_payment_amount())
			{
				$this->session->set_userdata('success_message', 'Cheque successfully writted to account');


				redirect('accounting/purchases');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}



		//open the add new order
		$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
    $v_data['purchase_items'] = $this->purchases_model->get_all_purchase_invoices();

		$where = 'account_payment_deleted = 0 ';
		$table = 'account_payments';


		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounting/write-cheque';
		$config['total_rows'] = $this->purchases_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->purchases_model->get_account_payments_transactions($table, $where, $config["per_page"], $page, $order='account_payments.payment_date', $order_method='ASC');
		// var_dump($query); die();

		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];

		$v_data['query'] = $query;
		$v_data['page'] = $page;

		$data['title'] = $v_data['title']= 'Purchase Payment';

		$data['content'] = $this->load->view('purchases/purchase_payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
  }
  public function record_purchased_items()
  {
    //form validation
		$this->form_validation->set_rules('account_to_id', 'Expense Account','required|xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
    $this->form_validation->set_rules('transaction_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
    	// var_dump($_POST);die();
		if ($this->form_validation->run())
		{

				//update order
				if($this->purchases_model->add_payment_amount())
				{
					$this->session->set_userdata('success_message', 'Expense has been successfully added');



				}

				else
				{
					$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
				}


		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}

    redirect('accounting/purchases');
  }


  public function make_payment($finance_purchase_id)
  {
    //form validation
    // $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
    $this->form_validation->set_rules('account_from_id', 'Expense Account','required|xss_clean');
    $this->form_validation->set_rules('amount_paid', 'Amount','required|xss_clean');
    $this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
    $this->form_validation->set_rules('payment_date', 'Description','required|xss_clean');
    // $this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
    // $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');
    // var_dump($_POST);die();
    if ($this->form_validation->run())
    {
      //update order
      $balance = $this->input->post('balance');
      $amount_paid = $this->input->post('amount_paid');
      if($amount_paid > $balance)
      {
        $this->session->set_userdata('error_message', 'Sorry you are not allowed to make an overpayment to this account ');
      }
      else {
        if($this->purchases_model->payaninvoice($finance_purchase_id))
        {
          $this->session->set_userdata('success_message', 'You have successfully added the payment');
        }

        else
        {
          $this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
        }
      }

    }
    else
    {
      $this->session->set_userdata('error_message', validation_errors());
    }

    redirect('accounting/purchases');
  }

  public function search_purchases()
  {
    $visit_date_from = $this->input->post('date_from');
		$transaction_number = $this->input->post('transaction_number');
		$visit_date_to = $this->input->post('date_to');

		$search_title = '';

		if(!empty($transaction_number))
		{
			$search_title .= $tenant_name.' ';
			$transaction_number = ' AND finance_purchase.transaction_number LIKE \'%'.$transaction_number.'%\'';


		}
		else
		{
			$transaction_number = '';
			$search_title .= '';
		}

     if(!empty($visit_date_from) && !empty($visit_date_to))
     {
       $visit_date = ' AND finance_purchase.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
       $search_title .= 'Payments from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
     }

     else if(!empty($visit_date_from))
     {
       $visit_date = ' AND finance_purchase.transaction_date = \''.$visit_date_from.'\'';
       $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_from)).' ';
     }

     else if(!empty($visit_date_to))
     {
       $visit_date = ' AND finance_purchase.transaction_date = \''.$visit_date_to.'\'';
       $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_to)).' ';
     }

     else
     {
       $visit_date = '';
     }


		$search = $visit_date.$transaction_number;

		$this->session->set_userdata('search_purchases', $search);

    redirect('accounting/purchases');
  }
	public function close_search()
	{
		$this->session->unset_userdata('search_purchases');

		redirect('accounting/purchases');
	}

	public function petty_cash($location_id=0)
	{

		$this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
		$this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');

		// var_dump();die();

		$account_name = $this->site_model->display_page_title();
			// var_dump($account_name);die();


		// 30-03-2023 Add a section to get the account id for a particular branch
		// using the branch id and the account name passed to search the accounts under bank as it resebles

		$account_from_id = $this->purchases_model->get_branch_account_id($account_name);

		if ($this->form_validation->run())
		{
			$account_from_id = $this->input->post('account_from_id');
			$transacted_amount = $this->input->post('transacted_amount');

		
			$balance = $this->company_financial_model->get_account_balance($account_from_id);

			$difference = $balance - $transacted_amount;
			$redirect_url = $this->input->post('redirect_url');

			if($difference >= 0 )
			{
					//update order
				if($this->purchases_model->add_payment_amount())
				{
					$this->session->set_userdata('success_message', 'Cheque successfully writted to account');


					redirect($redirect_url);
				}

				else
				{
					$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
				}



			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry the balance on this account is lower than amount to be transacted');
				// redirect('accounting/purchases');
				redirect($redirect_url);
			}


		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}


		$branch_id = $this->session->userdata('branch_id');
		//open the add new order
		$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
		$v_data['creditors'] = $this->purchases_model->get_creditor();

		$v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Expense Accounts#Cost of Goods");
		// $account_from_id = $this->purchases_model->get_account_id($account_name);
		// if($location_id == 2)
		// 	$account_from_id = 5;
		// else
		// 	$account_from_id = $this->purchases_model->get_account_petty_account($branch_id);

		// var_dump($location_id);die();

		$where =  'v_account_ledger_by_date.accountId = '.$account_from_id;

		$search_purchases = $this->session->userdata('search_petty_cash');
		if($search_purchases)
		{
			$where .= $search_purchases;
			$search_title = $this->session->userdata('search_petty_cash_title');
		}
		else {

			$add7days = date('Y-m-d', strtotime('-7 days'));
			// $where .= ' AND v_account_ledger_by_date.transactionDate BETWEEN \''.$add7days.'\' AND \''.date('Y-m-d').'\'';
			$transaction_date = date('jS M Y',strtotime($add7days));

			$todays_date = date('jS M Y',strtotime(date('Y-m-d')));
			$search_title = 'Transactions for period '.$transaction_date.' to  '.$todays_date.' ';

			$this->session->set_userdata('petty_cash_search', TRUE);
			$this->session->set_userdata('petty_cash_visit_date_from', $add7days);
			$this->session->set_userdata('petty_cash_visit_date_to', date('Y-m-d'));
		}

		// if(!empty($branch_id))
		// {
		// 	$where .= ' AND v_account_ledger_by_date.branch_id = '.$branch_id;
		// }
		// $table = 'v_account_ledger_by_date';

		// var_dump($account_from_id);die();
		$v_data['search_title'] = $search_title;
		$v_data['query_purchases'] = null;//$this->purchases_model->get_petty_cash_statement($account_from_id);
		// var_dump($table); die();
			$v_data['departments'] = $this->purchases_model->get_all_departments();
		$v_data['account_from_id'] = $account_from_id;
		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];
		$v_data['location_id'] = 0;
		$v_data['account_name_title'] = $account_name;
		// $v_data['query_purchases'] = $query;
			$v_data['search_title'] = $search_title;
		// $v_data['page'] = $page;

		$data['title'] = $v_data['title']= 'Petty Cash';
		
		$data['content'] = $this->load->view('purchases/petty_cash', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}


  public function record_petty_cash()
  {
    //form validation
		$this->form_validation->set_rules('department_id', 'Department','required|xss_clean');
		$this->form_validation->set_rules('account_to_id', 'Expense Account','required|xss_clean');
		$this->form_validation->set_rules('account_from_id', 'Paying Account','required|xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
    $this->form_validation->set_rules('transaction_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
    // var_dump($_POST);die();
		if ($this->form_validation->run())
		{
			//update order
			// check balance

			$account_from_id = $this->input->post('account_from_id');
			$transacted_amount = $this->input->post('transacted_amount');
			$balance = $this->company_financial_model->get_account_balance($account_from_id);

			$difference = $balance - $transacted_amount;

			// var_dump($difference);die();

			if($difference >= 0 )
			{
				if($this->purchases_model->record_petty_cash_transaction())
				{
					$this->session->set_userdata('success_message', 'Expense has been successfully added');
				}

				else
				{
					$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
				}
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry the balance on this account is lower than amount to be transacted');

			}


		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}
	$redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);
  }

  public function search_petty_cash($transaction_id = NULL)
  {
    $visit_date_from = $this->input->post('date_from');
		// $transaction_number = $this->input->post('transaction_number');
		$visit_date_to = $this->input->post('date_to');

		$search_title = '';



     if(!empty($visit_date_from) && !empty($visit_date_to))
     {
       $visit_date = ' AND v_account_ledger_by_date.transactionDate BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
       $search_title .= 'Transaction from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
     }

     else if(!empty($visit_date_from))
     {
			 // $visit_date_from = $visit_date_from;
       $visit_date = ' AND v_account_ledger_by_date.transactionDate = \''.$visit_date_from.'\'';
       $search_title .= 'Transactions of '.date('jS M Y', strtotime($visit_date_from)).' ';
     }

     else if(!empty($visit_date_to))
     {
       $visit_date = ' AND v_account_ledger_by_date.transactionDate = \''.$visit_date_to.'\'';
       $search_title .= 'Transactions of '.date('jS M Y', strtotime($visit_date_to)).' ';
     }

     else
     {
       $visit_date = '';
     }


		$search = $visit_date;
		$this->session->set_userdata('petty_cash_search', TRUE);
		$this->session->set_userdata('search_petty_cash', $search);
		$this->session->set_userdata('petty_cash_visit_date_from', $visit_date_from);
		$this->session->set_userdata('petty_cash_visit_date_to', $visit_date_to);

    $redirect_url = $this->input->post('redirect_url');
    redirect($redirect_url);
  }

	public function close_petty_cash_search($location_id)
	{
		$this->session->unset_userdata('search_petty_cash');
		$this->session->unset_userdata('petty_cash_visit_date_from');


		if($location_id == 1)
		{
			redirect('accounting/petty-cash');
		}
		else
		{
			redirect('accounting/petty-cash');
		}

	}

	public function print_petty_cash($web_name)
	{
		// var_dump($account); die();
		$account_name = $this->site_model->decode_web_name($web_name);
		$v_data['contacts'] = $this->site_model->get_contacts();


		$search_purchases = $this->session->userdata('search_petty_cash');
		if($search_purchases)
		{
			$where .= $search_purchases;
			$search_title = $this->session->userdata('search_petty_cash_title');
		}
		else {

			$add7days = date('Y-m-d', strtotime('-7 days'));
			// $where .= ' AND v_account_ledger_by_date.transactionDate BETWEEN \''.$add7days.'\' AND \''.date('Y-m-d').'\'';
			$transaction_date = date('jS M Y',strtotime($add7days));

			$todays_date = date('jS M Y',strtotime(date('Y-m-d')));
			$search_title = 'Transactions for period '.$transaction_date.' to  '.$todays_date.' ';

			$this->session->set_userdata('petty_cash_search', TRUE);
			$this->session->set_userdata('petty_cash_visit_date_from', $add7days);
			$this->session->set_userdata('petty_cash_visit_date_to', date('Y-m-d'));
		}


		$branch_id = $this->session->userdata('branch_id');
		// var_dump($search_title);die();
		$v_data['search_title'] = $search_title;
		$v_data['location_id'] = $branch_id;
		$v_data['account_name_title'] = $account_name;

		$account_name = $this->site_model->display_page_title();
		$account_from_id = $this->purchases_model->get_account_id($account_name);

		// $account_from_id = $this->purchases_model->get_account_petty_account($branch_id);
		// var_dump($account_from_id);die();
		$v_data['account_from_id'] = $account_from_id;
		$v_data['query_purchases'] = $this->purchases_model->get_petty_cash_statement($account_from_id);
		$v_data['title'] = $search_title;
		$this->load->view('purchases/print_petty_cash', $v_data);
	}

		

	
    public function delete_petty_cash_record($finance_purchase_id,$location_id=0)
	{
		if($this->purchases_model->delete_petty_cash_record($finance_purchase_id))
		{
			$this->session->set_userdata('success_message', 'Petty Cash Record has been deleted');
		}

		else
		{
			$this->session->set_userdata('error_message', 'Petty Cash could not deleted');
		}
		if($location_id == 2)
			redirect('accounting/unbanked-cash');
		else
			redirect('accounting/petty-cash');
	}

	public function edit_petty_cash($finance_purchase_id)
	{
		$data['accounts'] = $this->purchases_model->get_child_accounts("Bank");

		$data['expense_accounts'] = $this->purchases_model->get_all_expense_accounts();
		$data['finance_purchase_id'] = $finance_purchase_id;


		$page = $this->load->view('finance/purchases/edit_petty_cash',$data,true);
		// var_dump($page);die();
		echo $page;

	}

	public function edit_petty_cash_data()
	{
		// $this->form_validation->set_rules('department_id', 'Department','required|xss_clean');
		$this->form_validation->set_rules('account_to_id', 'Expense Account','required|xss_clean');
		$this->form_validation->set_rules('account_from_id', 'Paying Account','required|xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
        $this->form_validation->set_rules('transaction_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');

		if ($this->form_validation->run())
		{
			//update order
			if($this->purchases_model->update_record_petty_cash_transaction())
			{
				// $this->session->set_userdata('success_message', 'Cheque successfully writted to account');

				$location_id = $this->input->post('location_id');
				$response['message'] ='success';
				$response['location_id'] =$location_id;
				$response['result'] ='You have successfully updated the payment';

			}

			else
			{
				// $this->session->set_userdata('error_message', 'Could not Direct Purchases. Please try again');
				$response['message'] ='fail';
				$response['result'] ='Sorry could not update this payment detail';
			}
		}
		else
		{
			// $this->session->set_userdata('error_message', validation_errors());

			$response['message'] ='fail';
			$response['result'] = strip_tags(validation_errors());
		}

		echo json_encode($response);
	}


	public function update_branch_petty_cash()
	{
		$this->db->where('finance_purchase_id > 0')	;
		$query = $this->db->get('finance_purchase');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...

				$branch_id = $value->branch_id;
				$finance_purchase_id = $value->finance_purchase_id;


				$account_from_id = $this->purchases_model->get_account_petty_account($branch_id);

				if($account_from_id > 0)
				{
						$array['account_from_id'] = $account_from_id;
						$this->db->where('finance_purchase_id',$finance_purchase_id);
						$this->db->update('finance_purchase_payment',$array);
				}


			}
		}
	}

	public function autocompleteData() {
        $returnData = array();

        // Get skills data
        $conditions['searchTerm'] = $this->input->get('term');
        $conditions['conditions']['status'] = '1';
        $skillData = $this->purchases_model->getRows($conditions);

        // Generate array
        if(!empty($skillData)){
            foreach ($skillData as $row){
                $data['id'] = $row['id'];
                $data['value'] = $row['name'];
                array_push($returnData, $data);
            }
        }

        // Return results as json encoded array
        echo json_encode($returnData);die;
    }
}
?>
