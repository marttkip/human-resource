<?php

class Creditors_model extends CI_Model
{
  /*
  * Retrieve all creditor
  * @param string $table
  *   @param string $where
  *
  */
  public function get_all_creditors($table, $where, $per_page, $page, $order = 'creditor_name', $order_method = 'ASC')
  {
    //retrieve all users
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }

	/*
	*	Add a new creditor
	*
	*/
  public function get_creditors_list($table, $where, $order)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order,'asc');
		$query = $this->db->get('');

		return $query;
	}

  /*
	*	get a single creditor's details
	*	@param int $creditor_id
	*
	*/
	public function get_creditor($creditor_id)
	{
		//retrieve all users
		$this->db->from('creditor');
		$this->db->select('*');
		$this->db->where('creditor_id = '.$creditor_id);
		$query = $this->db->get();

		return $query;
	}


  public function add_invoice_item($creditor_id,$creditor_invoice_id)
	{
		$amount = $this->input->post('unit_price');
		$account_to_id=$this->input->post('account_to_id');
    $item_description = $this->input->post('item_description');
		$quantity=$this->input->post('quantity');
    $vat_amount = $this->input->post('vat_amount');
    $total_amount = $this->input->post('total_amount');
		$tax_type_id=$this->input->post('tax_type_id');


		$service = array(
							'creditor_invoice_id'=>0,
							'unit_price'=> $amount,
							'account_to_id' => $account_to_id,
							'creditor_id' => $creditor_id,
              'item_description'=>$item_description,
							'created_by' => $this->session->userdata('personnel_id'),
							'created' => date('Y-m-d'),
              'total_amount'=>$total_amount,
              'vat_amount'=>$vat_amount,
              'quantity'=>$quantity,
              'vat_type_id'=>$tax_type_id
						);
    if(!empty($creditor_invoice_id))
    {
      $service['creditor_invoice_id'] = $creditor_invoice_id;
      $service['creditor_invoice_item_status'] = 1;
    }
    else
    {
      $service['creditor_invoice_item_status'] = 0;
    }


		$this->db->insert('creditor_invoice_item',$service);
		return TRUE;

	}

  public function confirm_creditor_invoice($creditor_id,$creditor_invoice_id = NULL)
	{
		$amount = $this->input->post('amount');
		$amount_charged = $this->input->post('amount_charged');
		$invoice_date = $this->input->post('invoice_date');
    $vat_charged = $this->input->post('vat_charged');
    $invoice_number = $this->input->post('invoice_number');

		$date_check = explode('-', $invoice_date);
		$month = $date_check[1];
		$year = $date_check[0];


		$document_number = $this->create_invoice_number();

		// var_dump($checked); die();

		$insertarray['transaction_date'] = $invoice_date;
		$insertarray['invoice_year'] = $year;
		$insertarray['invoice_month'] = $month;
		$insertarray['creditor_id'] = $creditor_id;
		$insertarray['document_number'] = $document_number;
    $insertarray['invoice_number'] = strtoupper($invoice_number);
		$insertarray['total_amount'] = $amount_charged;
		$insertarray['vat_charged'] = $vat_charged;
		$insertarray['created_by'] = $this->session->userdata('personnel_id');
		$insertarray['created'] = date('Y-m-d');
		$insertarray['amount'] = $amount;

    if(!empty($creditor_invoice_id))
    {
        $this->db->where('creditor_invoice_id',$creditor_invoice_id);
        if($this->db->update('creditor_invoice', $insertarray))
        {

          $total_visits = sizeof($_POST['creditor_invoice_items']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['creditor_invoice_items'];
              $creditor_invoice_item_id = $visit[$r];
              //check if card is held
              $service = array(
                        'creditor_invoice_id'=>$creditor_invoice_id,
                        'created' =>$invoice_date,
                        'creditor_invoice_item_status'=>1,
                        'year'=>$year,
                        'month'=>$month,
                      );
              $this->db->where('creditor_invoice_item_id',$creditor_invoice_item_id);
              $this->db->update('creditor_invoice_item',$service);
            }
          }

          return TRUE;
        }
    }
    else
    {
      if($this->db->insert('creditor_invoice', $insertarray))
      {

        $creditor_invoice_id = $this->db->insert_id();
        $total_visits = sizeof($_POST['creditor_invoice_items']);
        //check if any checkboxes have been ticked
        if($total_visits > 0)
        {
          for($r = 0; $r < $total_visits; $r++)
          {
            $visit = $_POST['creditor_invoice_items'];
            $creditor_invoice_item_id = $visit[$r];
            //check if card is held
            $service = array(
                      'creditor_invoice_id'=>$creditor_invoice_id,
                      'created' =>$invoice_date,
                      'creditor_invoice_item_status'=>1,
                      'year'=>$year,
                      'month'=>$month,
                    );
            $this->db->where('creditor_invoice_item_id',$creditor_invoice_item_id);
            $this->db->update('creditor_invoice_item',$service);
          }
        }

        return TRUE;
      }
    }
		

	}

  public function create_invoice_number()
	{
		//select product code
		$this->db->where('creditor_invoice_id > 0');
		$this->db->from('creditor_invoice');
		$this->db->select('MAX(document_number) AS number');
		$this->db->order_by('creditor_invoice_id','DESC');
		// $this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			// var_dump($number);die();
			$number++;

		}
		else{
			$number = 1;
		}
		// var_dump($number);die();
		return $number;
	}

  public function get_creditor_invoice($creditor_id,$limit=null)
	{
		$this->db->where('creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id AND creditor_invoice.creditor_invoice_status = 1 AND creditor_invoice.creditor_id = '.$creditor_id);
		if($limit)
		{
			$this->db->limit($limit);
		}
		$this->db->group_by('creditor_invoice.creditor_invoice_id');
		$this->db->order_by('creditor_invoice.transaction_date','DESC');
		return $this->db->get('creditor_invoice_item,creditor_invoice');
	}



  public function get_creditor_invoice_number($creditor_id,$limit=null)
  {
    // $this->db->where('v_creditors_invoice_balances.creditor_id = '.$creditor_id);
    // $this->db->select('*');
    // return $this->db->get('v_creditors_invoice_balances');

    $select_statement = "
                        SELECT
                          data.invoice_id AS creditor_invoice_id,
                          data.invoice_number AS invoice_number,
                          data.invoice_date AS invoice_date,
                          data.creditor_invoice_type AS creditor_invoice_type,
                          COALESCE (SUM(data.dr_amount),0) AS dr_amount,
                          COALESCE (SUM(data.cr_amount),0) AS cr_amount,
                          COALESCE (SUM(data.dr_amount),0) - COALESCE (SUM(data.cr_amount),0) AS balance
                        FROM 
                        (
                          SELECT
                            `orders`.`supplier_id` AS creditor_id,
                            `orders`.`order_id` as invoice_id,
                            `orders`.`supplier_invoice_number` as invoice_number,
                            `orders`.`supplier_invoice_date` as invoice_date,
                            'Supplies Invoice' AS creditor_invoice_type,
                            COALESCE (SUM(`order_supplier`.`less_vat`),0) AS dr_amount,
                            0 AS cr_amount
                            FROM (`orders`,order_supplier,order_item)
                            WHERE `order_supplier`.`order_id` = `orders`.`order_id`
                            AND `order_item`.`order_item_id` = `order_supplier`.`order_item_id`
                            AND orders.is_store = 0
                            AND orders.order_approval_status = 7
                            GROUP BY orders.order_id

                            UNION ALL 

                            SELECT
                            `orders`.`supplier_id` AS creditor_id,
                            `orders`.`reference_id` as invoice_id,
                            `orders`.`reference_number` as invoice_number,
                            `orders`.`supplier_invoice_date` as invoice_date,
                            'Supplies Invoice' AS creditor_invoice_type,
                            0 AS dr_amount,
                            COALESCE (SUM(`order_supplier`.`less_vat`),0) AS cr_amount
                            FROM (`orders`,order_supplier,order_item)
                            WHERE `order_supplier`.`order_id` = `orders`.`order_id`
                            AND `order_item`.`order_item_id` = `order_supplier`.`order_item_id`
                            AND orders.is_store = 3
                            AND orders.order_approval_status = 7
                            GROUP BY orders.reference_id

                            UNION ALL 


                            SELECT
                            `orders`.`supplier_id` AS creditor_id,
                            `orders`.`order_id` as invoice_id,
                            `orders`.`supplier_invoice_number` as invoice_number,
                            `creditor_payment`.`transaction_date` as invoice_date,
                            'Supplies Payments' AS creditor_invoice_type,
                            0 AS dr_amount,
                            COALESCE (SUM(`creditor_payment_item`.`amount_paid`),0) AS cr_amount
                            FROM (creditor_payment_item,creditor_payment,orders)
                            WHERE `creditor_payment_item`.`creditor_invoice_id` = `orders`.`order_id` 
                            AND `creditor_payment_item`.`creditor_payment_id` = `creditor_payment`.`creditor_payment_id` 
                            AND creditor_payment_item.invoice_type = 1
                            GROUP BY orders.order_id


                            UNION ALL

                            SELECT
                            `creditor_invoice`.`creditor_id` AS creditor_id,
                            `creditor_invoice`.`creditor_invoice_id` AS invoice_id,
                            `creditor_invoice`.`invoice_number` AS invoice_number,
                            `creditor_invoice`.`transaction_date` AS invoice_date,
                            'Creditor Bills' AS creditor_invoice_type,
                            COALESCE (SUM(`creditor_invoice_item`.`total_amount`),0) AS dr_amount,
                            0 AS cr_amount
                            FROM (`creditor_invoice`,creditor_invoice_item,creditor)
                            WHERE `creditor_invoice_item`.`creditor_invoice_id` = `creditor_invoice`.`creditor_invoice_id` AND creditor_invoice.creditor_invoice_status = 1 AND creditor.creditor_id = creditor_invoice.creditor_id AND creditor_invoice.transaction_date >= creditor.start_date
                            GROUP BY `creditor_invoice`.`creditor_invoice_id`

                            UNION ALL 

                            SELECT
                            `creditor_invoice`.`creditor_id` AS creditor_id,
                            `creditor_invoice`.`creditor_invoice_id` AS invoice_id,
                            `creditor_invoice`.`invoice_number` AS invoice_number,
                            `creditor_invoice`.`transaction_date` AS invoice_date,
                            'Creditor Bills Credit Note' AS creditor_invoice_type,
                            0 AS dr_amount,
                            COALESCE (SUM(`creditor_credit_note_item`.`credit_note_amount`),0) AS cr_amount
                            FROM (`creditor_invoice`,creditor_credit_note,creditor_credit_note_item)
                            WHERE `creditor_credit_note_item`.`creditor_credit_note_id` = `creditor_credit_note`.`creditor_credit_note_id`
                            AND `creditor_invoice`.`creditor_invoice_id` = `creditor_credit_note_item`.`creditor_invoice_id` AND creditor_credit_note.creditor_credit_note_status = 1
                            GROUP BY `creditor_credit_note_item`.`creditor_invoice_id`

                            UNION ALL


                            SELECT
                            `creditor_invoice`.`creditor_id` AS creditor_id,
                            `creditor_invoice`.`creditor_invoice_id` AS invoice_id,
                            `creditor_invoice`.`invoice_number` AS invoice_number,
                            `creditor_invoice`.`transaction_date` AS invoice_date,
                            'Bill Payments' AS creditor_invoice_type,
                            0 AS dr_amount,
                            COALESCE (SUM(`creditor_payment_item`.`amount_paid`),0) AS cr_amount
                            FROM (creditor_payment_item,creditor_payment,creditor_invoice)
                            WHERE `creditor_payment_item`.`creditor_invoice_id` = `creditor_invoice`.`creditor_invoice_id` 
                            AND `creditor_payment_item`.`creditor_payment_id` = `creditor_payment`.`creditor_payment_id` AND creditor_payment_item.invoice_type = 0 AND creditor_payment.creditor_payment_status = 1
                            GROUP BY creditor_invoice.creditor_invoice_id

                            UNION ALL 

                          SELECT
                            `creditor`.`creditor_id` AS creditor_id,
                            `creditor`.`creditor_id` AS invoice_id,
                            `creditor`.`creditor_id` AS invoice_number,
                            `creditor`.`start_date` AS invoice_date,
                            'Opening Balance' AS creditor_invoice_type,
                             COALESCE (SUM(opening_balance),0) AS dr_amount,
                            '0' AS cr_amount
                            FROM (creditor)
                            WHERE creditor.creditor_id > 0
                            GROUP BY creditor.creditor_id

                            UNION ALL 

                            SELECT
                            `creditor`.`creditor_id` AS creditor_id,
                            `creditor`.`creditor_id` as invoice_id,
                            `creditor`.`creditor_id` as invoice_number,
                            `creditor`.`start_date` as invoice_date,
                            'Opening Balance Payment' AS creditor_invoice_type,
                            0 AS dr_amount,
                            COALESCE (SUM(`creditor_payment_item`.`amount_paid`),0) AS cr_amount
                            FROM (creditor_payment_item,creditor_payment,creditor)
                            WHERE `creditor_payment_item`.`creditor_id` = `creditor`.`creditor_id` 
                            AND `creditor_payment_item`.`creditor_payment_id` = `creditor_payment`.`creditor_payment_id` 
                            AND creditor_payment_item.invoice_type = 2 AND creditor_payment.creditor_payment_status = 1
                            GROUP BY creditor.creditor_id


                            UNION ALL

                            SELECT
                            `assets_details`.`supplier_id` AS creditor_id,
                            assets_details.asset_id AS invoice_id,
                            `assets_details`.`asset_serial_no` AS invoice_number,
                            `assets_details`.`asset_pd_period` AS invoice_date,
                            'Asset Purchase' AS creditor_invoice_type,
                            COALESCE (SUM(`assets_details`.`asset_value`),0) AS dr_amount,
                            0 AS cr_amount
                            FROM (`assets_details`,creditor)
                            WHERE `assets_details`.`supplier_id` = `creditor`.`creditor_id` AND assets_details.asset_pd_period >= creditor.start_date
                            AND assets_details.bill_asset = 1
                            GROUP BY assets_details.asset_id 


                            UNION ALL


                            SELECT
                            `assets_details`.`supplier_id` AS creditor_id,
                            assets_details.asset_id AS invoice_id,
                            `assets_details`.`asset_serial_no` AS invoice_number,
                            `assets_details`.`asset_pd_period` AS invoice_date,
                            'Asset Purchase' AS creditor_invoice_type,
                            0 AS dr_amount,
                            COALESCE (SUM(`creditor_payment_item`.`amount_paid`),0) AS cr_amount
                            FROM (creditor_payment_item,creditor_payment,assets_details)
                            WHERE `creditor_payment_item`.`creditor_invoice_id` = `assets_details`.`asset_id` 
                            AND `creditor_payment_item`.`creditor_payment_id` = `creditor_payment`.`creditor_payment_id` AND creditor_payment_item.invoice_type = 3 AND creditor_payment.creditor_payment_status = 1
                            GROUP BY assets_details.asset_id


                          ) AS data,creditor WHERE data.creditor_id = ".$creditor_id." AND data.creditor_id = creditor.creditor_id AND data.invoice_date >= creditor.start_date   GROUP BY data.invoice_number ORDER BY data.invoice_date ASC ";
                          $query = $this->db->query($select_statement);
                  return $query;


  }

  public function add_credit_note_item($creditor_id,$creditor_credit_note_id)
  {

    $amount = $this->input->post('amount');
		$account_to_id=$this->input->post('account_to_id');
    $description = $this->input->post('description');
		$tax_type_id=$this->input->post('tax_type_id');

    if($tax_type_id == 0)
    {
      $amount = $amount;
      $vat = 0;
    }
    else if($tax_type_id == 1)
    {

      $vat = $amount *0.16;
      $amount = $amount*1.16;
    }
    else if($tax_type_id == 2){

      $vat = $amount*0.05;
      $amount = $amount *1.05;
    }

    // var_dump($amount);die();


		$service = array(
							'creditor_id' => $creditor_id,
              'account_to_id' => $account_to_id,
              'description'=>$description,
							'created_by' => $this->session->userdata('personnel_id'),
							'created' => date('Y-m-d'),
              'credit_note_amount'=>$amount,
              'credit_note_charged_vat'=>$vat,
              'vat_type_id'=>$tax_type_id
						);

    if(!empty($creditor_credit_note_id))
    {
      $service['creditor_credit_note_id'] = $creditor_credit_note_id;
      $service['creditor_credit_note_item_status'] = 1;
    }
    else
    {
       $service['creditor_credit_note_item_status'] = 0;
    }

		$this->db->insert('creditor_credit_note_item',$service);
		return TRUE;

  }


  public function confirm_creditor_credit_note($creditor_id,$creditor_credit_note_id)
  {
    $amount = $this->input->post('amount');
    $amount_charged = $this->input->post('amount_charged');
    $invoice_date = $this->input->post('credit_note_date');
    $creditor_invoice_id = $this->input->post('invoice_id');
    $vat_charged = $this->input->post('vat_charged');
    $invoice_number = $this->input->post('credit_note_number');

    $date_check = explode('-', $invoice_date);
    $month = $date_check[1];
    $year = $date_check[0];


    $document_number = $this->create_credit_note_number();

    // var_dump($checked); die();

    $insertarray['transaction_date'] = $invoice_date;
    $insertarray['invoice_year'] = $year;
    $insertarray['invoice_month'] = $month;
    $insertarray['creditor_id'] = $creditor_id;
    $insertarray['creditor_invoice_id'] = $creditor_invoice_id;
    $insertarray['document_number'] = $document_number;
    $insertarray['invoice_number'] = strtoupper($invoice_number);
    $insertarray['total_amount'] = $amount_charged;
    $insertarray['vat_charged'] = $vat_charged;
 
    $insertarray['amount'] = $amount;
    $insertarray['account_from_id'] = 83;


     $total_visits = sizeof($_POST['creditor_notes_items']);

     // var_dump($total_visits);die();

     if(!empty($creditor_credit_note_id))
     {
        $this->db->where('creditor_credit_note_id',$creditor_credit_note_id);
        if($this->db->update('creditor_credit_note', $insertarray))
        {


          $total_visits = sizeof($_POST['creditor_notes_items']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['creditor_notes_items'];
              $creditor_credit_note_item_id = $visit[$r];
              //check if card is held
              $service = array(
                        'creditor_credit_note_id'=>$creditor_credit_note_id,
                        'created' =>$invoice_date,
                        'creditor_credit_note_item_status'=>1,
                        'creditor_invoice_id'=>$creditor_invoice_id,
                        'year'=>$year,
                        'month'=>$month,
                      );
              $this->db->where('creditor_credit_note_item_id',$creditor_credit_note_item_id);
              $this->db->update('creditor_credit_note_item',$service);
            }
          }
          return TRUE;
        }
     }
     else
     {

        $insertarray['created_by'] = $this->session->userdata('personnel_id');
        $insertarray['created'] = date('Y-m-d');
        if($this->db->insert('creditor_credit_note', $insertarray))
        {
          $creditor_credit_note_id = $this->db->insert_id();


          $total_visits = sizeof($_POST['creditor_notes_items']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['creditor_notes_items'];
              $creditor_credit_note_item_id = $visit[$r];
              //check if card is held
              $service = array(
                        'creditor_credit_note_id'=>$creditor_credit_note_id,
                        'created' =>$invoice_date,
                        'creditor_credit_note_item_status'=>1,
                        'creditor_invoice_id'=>$creditor_invoice_id,
                        'year'=>$year,
                        'month'=>$month,
                      );
              $this->db->where('creditor_credit_note_item_id',$creditor_credit_note_item_id);
              $this->db->update('creditor_credit_note_item',$service);
            }
          }
          return TRUE;
        }
     }

  }

  public function create_credit_note_number()
	{
		//select product code
		$this->db->where('creditor_invoice_id > 0');
		$this->db->from('creditor_invoice');
		$this->db->select('MAX(document_number) AS number');
		$this->db->order_by('creditor_invoice_id','DESC');
		// $this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			// var_dump($number);die();
			$number++;

		}
		else{
			$number = 1;
		}
		// var_dump($number);die();
		return $number;
	}


  public function get_creditor_credit_notes($creditor_id,$limit=null)
	{
		$this->db->where('creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id AND creditor_credit_note.creditor_credit_note_status = 1 AND creditor_credit_note.creditor_id = '.$creditor_id);
		if($limit)
		{
			$this->db->limit($limit);
		}
		$this->db->group_by('creditor_credit_note.creditor_credit_note_id');
		$this->db->order_by('creditor_credit_note.transaction_date','DESC');
		return $this->db->get('creditor_credit_note_item,creditor_credit_note');
	}


  public function get_creditor_payments($creditor_id,$limit=null)
  {
    $this->db->where('creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1 AND creditor_payment.creditor_id = '.$creditor_id);
    if($limit)
    {
      $this->db->limit($limit);
    }
    $this->db->group_by('creditor_payment.creditor_payment_id');
    $this->db->join('account','account.account_id = creditor_payment.account_from_id','left');
    $this->db->order_by('creditor_payment.transaction_date','DESC');
    return $this->db->get('creditor_payment_item,creditor_payment');
  }

  public function add_payment_item($creditor_id,$creditor_payment_id)
  {

    $amount = $this->input->post('amount_paid');
    $creditor_invoice_id = $this->input->post('invoice_id');



    // if(empty($creditor_invoice_id))
    // {
    //   $invoice_type = 2;
    // }
    // else
    // {
      $exploded = explode('.', $creditor_invoice_id);
      $invoice_id = $exploded[0];
      $invoice_number = $exploded[1];
      $invoice_type = $exploded[2];

    // }

    $service = array(
              'creditor_invoice_id'=>$invoice_id,
              'invoice_number'=>$invoice_number,
              'invoice_type'=>$invoice_type,
              'creditor_payment_item_status' => 0,
              'creditor_payment_id' => 0,
              'creditor_id' => $creditor_id,
              'created_by' => $this->session->userdata('personnel_id'),
              'created' => date('Y-m-d'),
              'amount_paid'=>$amount,
            );
            // var_dump($service);die();

    if(!empty($creditor_payment_id))
    {
      $service['creditor_payment_id'] = $creditor_payment_id;
      $service['creditor_payment_item_status'] = 1;
    }

    $this->db->insert('creditor_payment_item',$service);
    return TRUE;

  }

  public function confirm_creditor_payment($creditor_id,$creditor_payment_id)
  {
    $amount_paid = $this->input->post('amount_paid');
    $payment_date = $this->input->post('payment_date');
    $reference_number = $this->input->post('reference_number');
    $account_from_id = $this->input->post('account_from_id');

    $date_check = explode('-', $payment_date);
    $month = $date_check[1];
    $year = $date_check[0];


    // var_dump($year);die();

    if(!empty($creditor_payment_id))
    {
     

      // $document_number = $this->create_credit_payment_number();

      $insertarray['transaction_date'] = $payment_date;
      $insertarray['payment_year'] = $year;
      $insertarray['payment_month'] = $month;
      $insertarray['creditor_id'] = $creditor_id;
      $insertarray['reference_number'] = strtoupper($reference_number);
      $insertarray['total_amount'] = $amount_paid;
      $insertarray['account_from_id'] = $account_from_id;
      $insertarray['created_by'] = $this->session->userdata('personnel_id');
       $this->db->where('creditor_payment_id',$creditor_payment_id);

      if($this->db->update('creditor_payment', $insertarray))
      {


        $total_visits = sizeof($_POST['creditor_payments_items']);

        //check if any checkboxes have been ticked
        if($total_visits > 0)
        {
          for($r = 0; $r < $total_visits; $r++)
          {
            $visit = $_POST['creditor_payments_items'];
            $creditor_payment_item_id = $visit[$r];
            //check if card is held
            $service = array(
                      'creditor_payment_id'=>$creditor_payment_id,
                      'created' =>$payment_date,
                      'creditor_payment_item_status'=>1,
                      'year'=>$year,
                      'month'=>$month,
                    );
            $this->db->where('creditor_payment_item_id',$creditor_payment_item_id);
            $this->db->update('creditor_payment_item',$service);
          }
        }



          return TRUE;
      }

    }

    else
    {
      $document_number = $this->create_credit_payment_number();

      $insertarray['transaction_date'] = $payment_date;
      $insertarray['payment_year'] = $year;
      $insertarray['payment_month'] = $month;
      $insertarray['creditor_id'] = $creditor_id;
      $insertarray['document_number'] = $document_number;
      $insertarray['reference_number'] = strtoupper($reference_number);
      $insertarray['total_amount'] = $amount_paid;
      $insertarray['account_from_id'] = $account_from_id;
      $insertarray['created_by'] = $this->session->userdata('personnel_id');
      $insertarray['created'] = date('Y-m-d');

      if($this->db->insert('creditor_payment', $insertarray))
      {
        $creditor_payment_id = $this->db->insert_id();


        $total_visits = sizeof($_POST['creditor_payments_items']);

        //check if any checkboxes have been ticked
        if($total_visits > 0)
        {
          for($r = 0; $r < $total_visits; $r++)
          {
            $visit = $_POST['creditor_payments_items'];
            $creditor_payment_item_id = $visit[$r];
            //check if card is held
            $service = array(
                      'creditor_payment_id'=>$creditor_payment_id,
                      'created' =>$payment_date,
                      'creditor_payment_item_status'=>1,
                      'year'=>$year,
                      'month'=>$month,
                    );
            $this->db->where('creditor_payment_item_id',$creditor_payment_item_id);
            $this->db->update('creditor_payment_item',$service);
          }
        }



        return TRUE;
      }
    }

    
  }

  public function create_credit_payment_number()
  {
    //select product code
    $this->db->where('creditor_payment_id > 0');
    $this->db->from('creditor_payment');
    $this->db->select('MAX(document_number) AS number');
    $this->db->order_by('creditor_payment_id','DESC');
    // $this->db->limit(1);
    $query = $this->db->get();
    // var_dump($query); die();
    if($query->num_rows() > 0)
    {
      $result = $query->result();
      $number =  $result[0]->number;
      // var_dump($number);die();
      $number++;

    }
    else{
      $number = 1;
    }
    // var_dump($number);die();
    return $number;
  }




  /*
  * Add a new creditor
  *
  */
  public function add_creditor()
  {
    $creditor_type_id = $this->input->post('creditor_type_id');

    if(isset($creditor_type_id))
    {
      $creditor_type_id = 1;
    }
    else
    {
      $creditor_type_id = 0;
    }
    $data = array(
      'creditor_name'=>$this->input->post('creditor_name'),
      'creditor_email'=>$this->input->post('creditor_email'),
      'creditor_phone'=>$this->input->post('creditor_phone'),
      'creditor_location'=>$this->input->post('creditor_location'),
      'creditor_building'=>$this->input->post('creditor_building'),
      'creditor_floor'=>$this->input->post('creditor_floor'),
      'creditor_address'=>$this->input->post('creditor_address'),
      'creditor_post_code'=>$this->input->post('creditor_post_code'),
      'creditor_city'=>$this->input->post('creditor_city'),
      'opening_balance'=>$this->input->post('opening_balance'),
      'start_date'=>$this->input->post('creditor_account_date'),
      'creditor_contact_person_name'=>$this->input->post('creditor_contact_person_name'),
      'creditor_contact_person_onames'=>$this->input->post('creditor_contact_person_onames'),
      'creditor_contact_person_phone1'=>$this->input->post('creditor_contact_person_phone1'),
      'creditor_contact_person_phone2'=>$this->input->post('creditor_contact_person_phone2'),
      'creditor_contact_person_email'=>$this->input->post('creditor_contact_person_email'),
      'creditor_description'=>$this->input->post('creditor_description'),
      'branch_code'=>$this->session->userdata('branch_code'),
      'branch_id'=>$this->session->userdata('branch_id'),
      'created_by'=>$this->session->userdata('creditor_id'),
      'debit_id'=>$this->input->post('debit_id'),
      'modified_by'=>$this->session->userdata('creditor_id'),
      'creditor_type_id'=>$creditor_type_id,
      'created'=>date('Y-m-d H:i:s')
    );

    if($this->db->insert('creditor', $data))
    {
      return $this->db->insert_id();
    }
    else{
      return FALSE;
    }
  }

  /*
  * Update an existing creditor
  * @param string $image_name
  * @param int $creditor_id
  *
  */
  public function edit_creditor($creditor_id)
  {
    $data = array(
      'creditor_name'=>$this->input->post('creditor_name'),
      'creditor_email'=>$this->input->post('creditor_email'),
      'creditor_phone'=>$this->input->post('creditor_phone'),
      'creditor_location'=>$this->input->post('creditor_location'),
      'creditor_building'=>$this->input->post('creditor_building'),
      'creditor_floor'=>$this->input->post('creditor_floor'),
      'creditor_address'=>$this->input->post('creditor_address'),
      'creditor_post_code'=>$this->input->post('creditor_post_code'),
      'creditor_city'=>$this->input->post('creditor_city'),
      'opening_balance'=>$this->input->post('opening_balance'),
      'start_date'=>$this->input->post('creditor_account_date'),
      'creditor_contact_person_name'=>$this->input->post('creditor_contact_person_name'),
      'creditor_contact_person_onames'=>$this->input->post('creditor_contact_person_onames'),
      'creditor_contact_person_phone1'=>$this->input->post('creditor_contact_person_phone1'),
      'creditor_contact_person_phone2'=>$this->input->post('creditor_contact_person_phone2'),
      'creditor_contact_person_email'=>$this->input->post('creditor_contact_person_email'),
      'creditor_description'=>$this->input->post('creditor_description'),
      'branch_id'=>$this->session->userdata('branch_id'),
      'debit_id'=>$this->input->post('debit_id'),
      'modified_by'=>$this->session->userdata('creditor_id'),
    );

    $this->db->where('creditor_id', $creditor_id);
    if($this->db->update('creditor', $data))
    {
      return TRUE;
    }
    else{
      return FALSE;
    }
  }


  /*
  * get a single creditor's details
  * @param int $creditor_id
  *
  */
  public function get_creditor_account($creditor_id)
  {
    //retrieve all users
    $this->db->from('v_general_ledger');
    $this->db->select('SUM(dr_amount) AS total_invoice_amount');
    $this->db->where('transactionClassification = "Creditors Invoices" AND recepientId = '.$creditor_id);
    $query = $this->db->get();
    $invoices = $query->row();

    $total_invoice_amount = $invoices->total_invoice_amount;


    $this->db->from('v_general_ledger');
    $this->db->select('SUM(cr_amount) AS total_paid_amount');
    $this->db->where('transactionClassification = "Creditors Invoices Payments" AND recepientId = '.$creditor_id);
    $query = $this->db->get();
    $payments = $query->row();

    $total_paid_amount = $payments->total_paid_amount;


    $response['total_invoice'] = $total_invoice_amount;
    $response['total_paid_amount'] = $total_paid_amount;
    $response['total_credit_note'] = 0;

    return $response;
  }



   /*
  * Retrieve all creditor
  * @param string $table
  *   @param string $where
  *
  */
  public function get_all_creditors_details($table, $where, $per_page, $page, $order = 'creditor_name', $order_method = 'ASC')
  {
    //retrieve all users
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    // $this->db->group_by('creditor_invoice.creditor_invoice_id');
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }


  public function get_creditor_invoice_details($creditor_invoice_id)
  {

      $this->db->from('creditor_invoice');
      $this->db->select('*');
      $this->db->where('creditor_invoice_id = '.$creditor_invoice_id);
      $query = $this->db->get();
      return $query;
  }

  public function get_creditor_payment_details($creditor_payment_id)
  {

      $this->db->from('creditor_payment');
      $this->db->select('*');
      $this->db->where('creditor_payment_id = '.$creditor_payment_id);
      $query = $this->db->get();
      return $query;
  }

  public function check_on_account($creditor_payment_id)
  {

     $this->db->from('creditor_payment_item');
      $this->db->select('*');
      $this->db->where('invoice_type = 3 AND creditor_payment_id = '.$creditor_payment_id);
      $query = $this->db->get();
      if($query->num_rows() > 0)
      {
          return TRUE;
      }
      else
      {
        return FALSE;
      }


  }

  public function get_creditor_credit_note_details($creditor_credit_note_id)
  {

      $this->db->from('creditor_credit_note');
      $this->db->select('*');
      $this->db->where('creditor_credit_note_id = '.$creditor_credit_note_id);
      $query = $this->db->get();
      return $query;
  }
  
  public function get_content($table, $where,$select,$group_by=NULL,$limit=NULL)
  {
    $this->db->from($table);
    $this->db->select($select);
    $this->db->where($where);
    if($group_by != NULL)
    {
      $this->db->group_by($group_by);
    }
    $query = $this->db->get('');
    
    return $query;
  }

  
   public function export_payables()
  {
    $this->load->library('excel');
    
    $report_detail = $this->session->userdata('report_detail');

    $this->creditors_model->calculate_creditors_aging_report();
    $creditor_query = $this->creditors_model->get_payables_aging();
    $creditor_array = array();
    if($creditor_query->num_rows() > 0)
    {
      foreach ($creditor_query->result() as $key) {
        // code...
        $creditor_array[$key->recepientId] = $key;
      }
    }

    $income_rs = $this->get_all_vendors();
    
    
    $title = 'Creditors List';



    $income_result = '';


    $total_this_month = 0;
    $total_three_months = 0;
    $total_six_months = 0;
    $total_nine_months = 0;
    $total_payments = 0;
    $total_invoices =0;
    $total_balance = 0;
    $total_one_twenty_days =0;
    $total_one_fifty_days =0;
    $total_one_eighty_days =0;

    $total_two_ten_days =0;
    $total_two_fourty_days =0;
    $total_two_seventy_days =0;
    $total_three_hundred_days =0;
    $total_three_thirty_days =0;
    $total_three_sixty_days =0;
    $total_three_ninety_days =0;
    $total_four_twenty_days =0;
    $total_four_fifty_days =0;
    $total_four_eighty_days =0;
    $total_five_ten_days =0;
    $total_five_fourty_days =0;
    $total_five_seventy_days =0;
    $total_six_hundred_days =0;
    $total_six_thirty_days =0;
    $total_six_sixty_days =0;
    $total_six_ninety_days =0;
    $total_seven_twenty_days =0;
    $total_over_seven_twenty_days =0;
    $total_over_three_sixty_days =0;



    $total_one_eighty_days =0;
    $total_over_three_sixty_days =0;
    $total_over_one_eighty_days = 0;
    $total_unallocated  =0;



    $total_income = 0;
    $total_income = 0;
    $total_thirty = 0;
    $total_sixty = 0;
    $total_ninety = 0;
    $total_over_ninety = 0;
    $total_coming_due = 0;
    $grand_total = 0;
    $total_unallocated = 0;
    $grand_dr =0;
    $grand_cr = 0;
    
    if($income_rs->num_rows() > 0)
    {
      $count = 0;
      /*
        -----------------------------------------------------------------------------------------
        Document Header
        -----------------------------------------------------------------------------------------
      */

       
      $row_count = 0;

      if($report_detail == 180)
      {
        $report[$row_count][0] = 'Num';
        $report[$row_count][1] = 'Debtor name';
        $report[$row_count][2] = 'Current';
        $report[$row_count][3] = '30 Days';
        $report[$row_count][4] = '60 Days';
        $report[$row_count][5] = '90 Days';
        $report[$row_count][6] = '120 Days';
        $report[$row_count][7] = '150 Days';
        $report[$row_count][8] = '180 Days';
        $report[$row_count][9] = 'Over 180 Days';
        $report[$row_count][10] = 'Unallocated Funds';
        $report[$row_count][11] = 'Balance';
      }
      else if($report_detail == 360)
      {
        $report[$row_count][0] = 'Num';
        $report[$row_count][1] = 'Debtor name';
        $report[$row_count][2] = 'Current';
        $report[$row_count][3] = '30 Days';
        $report[$row_count][4] = '60 Days';
        $report[$row_count][5] = '90 Days';
        $report[$row_count][6] = '120 Days';
        $report[$row_count][7] = '150 Days';
        $report[$row_count][8] = '180 Days';
        $report[$row_count][9] = '210 Days';
        $report[$row_count][10] = '240 Days';
        $report[$row_count][11] = '270 Days';
        $report[$row_count][12] = '330 Days';
        $report[$row_count][13] = '360 Days';
        $report[$row_count][14] = 'Over 360 Days';
        $report[$row_count][15] = 'Unallocated Funds';
        $report[$row_count][16] = 'Balance';

      }
      else if($report_detail == 720)
      {


        $report[$row_count][0] = 'Num';
        $report[$row_count][1] = 'Debtor name';
        $report[$row_count][2] = 'Current';
        $report[$row_count][3] = '30 Days';
        $report[$row_count][4] = '60 Days';
        $report[$row_count][5] = '90 Days';
        $report[$row_count][6] = '120 Days';
        $report[$row_count][7] = '150 Days';
        $report[$row_count][8] = '180 Days';
        $report[$row_count][9] = '210 Days';
        $report[$row_count][10] = '240 Days';
        $report[$row_count][11] = '270 Days';
        $report[$row_count][12] = '300 Days';
        $report[$row_count][13] = '330 Days';
        $report[$row_count][14] = '360 Days';
        $report[$row_count][15] = '390 Days';
        $report[$row_count][16] = '420 Days';
        $report[$row_count][17] = '450 Days';
        $report[$row_count][18] = '480 Days';
        $report[$row_count][19] = '510 Days';
        $report[$row_count][20] = '540 Days';
        $report[$row_count][21] = '570 Days';
        $report[$row_count][22] = '600 Days';
        $report[$row_count][23] = '630 Days';
        $report[$row_count][24] = '660 Days';
        $report[$row_count][25] = '690 Days';
        $report[$row_count][26] = '720 Days';
        $report[$row_count][27] = 'Over 720 Days';
        $report[$row_count][28] = 'Unallocated Funds';
        $report[$row_count][29] = 'Balance';


      }
      else
      {
         $report[$row_count][0] = 'Num';
        $report[$row_count][1] = 'Debtor name';
        $report[$row_count][2] = 'Current';
        $report[$row_count][3] = '30 Days';
        $report[$row_count][4] = '60 Days';
        $report[$row_count][5] = '90 Days';
        $report[$row_count][6] = '120 Days';
        $report[$row_count][7] = '150 Days';
        $report[$row_count][8] = '180 Days';
        $report[$row_count][9] = 'Over 180 Days';
        $report[$row_count][10] = 'Unallocated Funds';
        $report[$row_count][11] = 'Balance';
      }
     
      //get & display all services
      
      //display all patient data in the leftmost columns
      foreach($income_rs->result() as $key => $value_two)
      {
        $row_count++;
        $total_invoiced = 0;
        # code...
        // $total_amount = $value->total_amount;
        $creditor_id = $value_two->creditor_id;
        $creditor_name = $value_two->creditor_name;

        $opening_balance = $value_two->opening_balance;
        $debit_id = $value_two->debit_id;

        $opening_date = $value_two->created;

        
        if($debit_id == 2)
        {
          $opening_balance = $opening_balance;  
        }
        else
        {
          $opening_balance = -$opening_balance;
        }

        $unallocated = $this->company_financial_model->get_unallocated_funds($creditor_id);

        if(empty($unallocated))
          $unallocated = 0;
        $total_unallocated += $unallocated;


        $count++;



        if(array_key_exists($creditor_id, $creditor_array))
              {
                $this_month = $creditor_array[$creditor_id]->coming_due;
                $three_months = $creditor_array[$creditor_id]->thirty_days;
                $six_months = $creditor_array[$creditor_id]->sixty_days;
                $nine_months = $creditor_array[$creditor_id]->ninety_days;
                $one_twenty_days = $creditor_array[$creditor_id]->one_twenty_days;
                $one_fifty_days = $creditor_array[$creditor_id]->one_fifty_days;
                $one_eighty_days = $creditor_array[$creditor_id]->one_eighty_days;
                $over_one_eighty_days = $creditor_array[$creditor_id]->over_one_eighty_days;

                if($report_detail == 360)
                {
                  $two_ten_days = $creditor_array[$creditor_id]->two_ten_days;
                  $two_fourty_days = $creditor_array[$creditor_id]->two_fourty_days;
                  $two_seventy_days = $creditor_array[$creditor_id]->two_seventy_days;
                  $three_hundred_days = $creditor_array[$creditor_id]->three_hundred_days;
                  $three_thirty_days = $creditor_array[$creditor_id]->three_thirty_days;


                  $three_sixty_days = $creditor_array[$creditor_id]->three_sixty_days;
                  $over_three_sixty_days = $creditor_array[$creditor_id]->over_three_sixty_days;

                  $three_ninety_days = $creditor_array[$creditor_id]->three_ninety_days;
                }

                if($report_detail == 360)
                {
                  $four_twenty_days = $creditor_array[$creditor_id]->four_twenty_days;
                  $four_fifty_days = $creditor_array[$creditor_id]->four_fifty_days;
                  $four_eighty_days = $creditor_array[$creditor_id]->four_eighty_days;

                  $five_ten_days = $creditor_array[$creditor_id]->five_ten_days;
                  $five_fourty_days = $creditor_array[$creditor_id]->five_fourty_days;
                  $five_seventy_days = $creditor_array[$creditor_id]->five_seventy_days;
                  $six_hundred_days = $creditor_array[$creditor_id]->six_hundred_days;
                  $six_thirty_days = $creditor_array[$creditor_id]->six_thirty_days;
                  $six_sixty_days = $creditor_array[$creditor_id]->six_sixty_days;
                  $six_ninety_days = $creditor_array[$creditor_id]->six_ninety_days;
                  $seven_twenty_days = $creditor_array[$creditor_id]->seven_twenty_days;
                  $over_seven_twenty_days = $creditor_array[$creditor_id]->over_seven_twenty_days;
                }



                $date = date('Y-m-d');
                  

                if($report_detail == 180)
                {
                  $total_this_month +=$this_month;
                  $total_three_months +=$three_months;
                  $total_six_months +=$six_months;
                  $total_nine_months +=$nine_months;
                  $total_one_twenty_days +=$one_twenty_days;
                  $total_one_fifty_days +=$one_fifty_days;
                  $total_one_eighty_days +=$one_eighty_days;
                  $total_over_one_eighty_days +=$over_one_eighty_days;
                }
                else if($report_detail == 360)
                {
                 $total_this_month +=$this_month;
                  $total_three_months +=$three_months;
                  $total_six_months +=$six_months;
                  $total_nine_months +=$nine_months;
                  $total_one_twenty_days +=$one_twenty_days;
                  $total_one_fifty_days +=$one_fifty_days;
                  $total_one_eighty_days +=$one_eighty_days;
                  $total_two_ten_days += $two_ten_days;
                  $total_two_fourty_days += $two_fourty_days;
                  $total_two_seventy_days += $two_seventy_days;
                  $total_three_hundred_days += $three_hundred_days;
                  $total_three_thirty_days += $three_thirty_days;
                  $total_three_sixty_days += $three_sixty_days;
                  $total_over_three_sixty_days += $over_three_sixty_days;

                }
                else if($report_detail == 720)
                {

                  $total_this_month +=$this_month;
                  $total_three_months +=$three_months;
                  $total_six_months +=$six_months;
                  $total_nine_months +=$nine_months;
                  $total_one_twenty_days +=$one_twenty_days;
                  $total_one_fifty_days +=$one_fifty_days;
                  $total_one_eighty_days +=$one_eighty_days;

                  $total_two_ten_days += $two_ten_days;
                  $total_two_fourty_days += $two_fourty_days;
                  $total_two_seventy_days += $two_seventy_days;
                  $total_three_hundred_days += $three_hundred_days;
                  $total_three_thirty_days += $three_thirty_days;
                  $total_three_sixty_days += $three_sixty_days;
                  $total_three_ninety_days += $three_ninety_days;
                  $total_four_twenty_days += $four_twenty_days;
                  $total_four_fity_days += $four_fity_days;
                  $total_four_eighty_days += $four_eighty_days;
                  $total_five_ten_days += $five_ten_days;
                  $total_five_fourty_days += $five_fourty_days;
                  $total_five_seventy_days += $five_seventy_days;
                  $total_six_hundred_days += $six_hundred_days;
                  $total_six_thirty_days += $six_thirty_days;
                  $total_six_sixty_days += $six_sixty_days;
                  $total_six_ninety_days += $six_ninety_days;
                  $total_seven_twenty_days += $seven_twenty_days;
                  $total_over_seven_twenty_days += $over_seven_twenty_days;


                }

               
             
                // $total_payments += $payments_total;
                // $total_invoices += $invoice_total;

                if($report_detail == 180)
                  $invoice_total = $this_month + $three_months + $six_months + $nine_months + $one_twenty_days + $one_fifty_days + $one_eighty_days + $over_one_eighty_days;
                else if($report_detail == 360)
                 $invoice_total = $this_month + $three_months + $six_months + $nine_months + $one_twenty_days + $one_fifty_days + $one_eighty_days + $two_ten_days + $two_fourty_days + $two_seventy_days + $three_hundred_days + $three_thirty_days + $three_sixty_days + $over_three_sixty_days;
               else if($report_detail == 720)
                 $invoice_total = $this_month + $three_months + $six_months + $nine_months + $one_twenty_days + $one_fifty_days + $one_eighty_days + $two_ten_days + $two_fourty_days + $two_seventy_days + $three_hundred_days + $three_thirty_days + $three_sixty_days + $three_ninety_days + $four_twenty_days + $four_fifty_days + $four_eighty_days + $five_ten_days + $five_fourty_days + $five_seventy_days + $six_hundred_days + $six_thirty_days + $six_sixty_days + $six_ninety_days + $seven_twenty_days + $over_seven_twenty_days;
                


                $total_balance += $invoice_total;


                if($report_detail == 180)
                {


                    $report[$row_count][0] = $count;
                    $report[$row_count][1] = $creditor_name;
                    $report[$row_count][2] = number_format($this_month,2);
                    $report[$row_count][3] = number_format($three_months,2);
                    $report[$row_count][4] = number_format($six_months,2);
                    $report[$row_count][5] = number_format($nine_months,2);
                    $report[$row_count][6] = number_format($one_twenty_days,2);
                    $report[$row_count][7] = number_format($one_fifty_days,2);
                    $report[$row_count][8] = number_format($one_eighty_days,2);
                    $report[$row_count][9] = number_format($over_one_eighty_days,2);
                    $report[$row_count][10] = number_format($unallocated,2);
                    $report[$row_count][11] = number_format($invoice_total,2);

        

                }
                else if($report_detail == 360)
                {


                    $report[$row_count][0] = $count;
                    $report[$row_count][1] = $creditor_name;
                    $report[$row_count][2] = number_format($this_month,2);
                    $report[$row_count][3] = number_format($three_months,2);
                    $report[$row_count][4] = number_format($six_months,2);
                    $report[$row_count][5] = number_format($nine_months,2);
                    $report[$row_count][6] = number_format($one_twenty_days,2);
                    $report[$row_count][7] = number_format($one_fifty_days,2);
                    $report[$row_count][8] = number_format($one_eighty_days,2);
                    $report[$row_count][9] = number_format($two_ten_days,2);
                    $report[$row_count][10] = number_format($two_fourty_days,2);
                    $report[$row_count][11] = number_format($two_seventy_days,2);
                    $report[$row_count][12] = number_format($three_hundred_days,2);
                    $report[$row_count][13] = number_format($three_thirty_days,2);
                    $report[$row_count][14] = number_format($three_sixty_days,2);
                    $report[$row_count][15] = number_format($over_three_sixty_days,2);
                    $report[$row_count][16] = number_format($unallocated,2);
                    $report[$row_count][17] = number_format($invoice_total,2);


                }
                else if($report_detail == 720)
                {

                    $report[$row_count][0] = $count;
                    $report[$row_count][1] = $creditor_name;
                    $report[$row_count][2] = number_format($this_month,2);
                    $report[$row_count][3] = number_format($three_months,2);
                    $report[$row_count][4] = number_format($six_months,2);
                    $report[$row_count][5] = number_format($nine_months,2);
                    $report[$row_count][6] = number_format($one_twenty_days,2);
                    $report[$row_count][7] = number_format($one_fifty_days,2);
                    $report[$row_count][8] = number_format($one_eighty_days,2);
                    $report[$row_count][9] = number_format($two_ten_days,2);
                    $report[$row_count][10] = number_format($two_fourty_days,2);
                    $report[$row_count][11] = number_format($two_seventy_days,2);
                    $report[$row_count][12] = number_format($three_hundred_days,2);
                    $report[$row_count][13] = number_format($three_thirty_days,2);
                    $report[$row_count][14] = number_format($three_sixty_days,2);
                    $report[$row_count][15] = number_format($three_ninety_days,2);
                    $report[$row_count][16] = number_format($four_twenty_days,2);
                    $report[$row_count][17] = number_format($four_fifty_days,2);
                    $report[$row_count][18] = number_format($four_eighty_days,2);
                    $report[$row_count][19] = number_format($five_ten_days,2);
                    $report[$row_count][20] = number_format($five_fourty_days,2);
                    $report[$row_count][21] = number_format($five_seventy_days,2);
                    $report[$row_count][22] = number_format($six_hundred_days,2);
                    $report[$row_count][23] = number_format($six_thirty_days,2);
                    $report[$row_count][24] = number_format($six_sixty_days,2);
                    $report[$row_count][25] = number_format($six_ninety_days,2);
                    $report[$row_count][26] = number_format($seven_twenty_days,2);
                    $report[$row_count][27] = number_format($over_seven_twenty_days,2);
                    $report[$row_count][28] = number_format($unallocated,2);
                    $report[$row_count][29] = number_format($invoice_total,2);
            
          }



        }
        
        //display the patient data


        
          
        
        
      }


      if($report_detail == 180)
        {

          $report[$row_count][0] = '';
          $report[$row_count][1] = 'Total';
          $report[$row_count][2] = number_format($total_this_month,2);
          $report[$row_count][3] = number_format($total_three_months,2);
          $report[$row_count][4] = number_format($total_six_months,2);
          $report[$row_count][5] = number_format($total_nine_months,2);
          $report[$row_count][6] = number_format($total_one_twenty_days,2);
          $report[$row_count][7] = number_format($total_one_fifty_days,2);
          $report[$row_count][8] = number_format($total_one_eighty_days,2);
          $report[$row_count][9] = number_format($total_over_one_eighty_days,2);
          $report[$row_count][10] = number_format($total_unallocated,2);
          $report[$row_count][11] = number_format($total_balance,2);
        
         
        }
        else if($report_detail == 360)
        {



           $report[$row_count][0] = '';
            $report[$row_count][1] = 'Total';
            $report[$row_count][2] = number_format($total_this_month,2);
            $report[$row_count][3] = number_format($total_three_months,2);
            $report[$row_count][4] = number_format($total_six_months,2);
            $report[$row_count][5] = number_format($total_nine_months,2);
            $report[$row_count][6] = number_format($total_one_twenty_days,2);
            $report[$row_count][7] = number_format($total_one_fifty_days,2);
            $report[$row_count][8] = number_format($total_one_eighty_days,2);
            $report[$row_count][9] = number_format($total_two_ten_days,2);
            $report[$row_count][10] = number_format($total_two_fourty_days,2);
            $report[$row_count][11] = number_format($total_two_seventy_days,2);
            $report[$row_count][12] = number_format($total_three_hundred_days,2);
            $report[$row_count][13] = number_format($total_three_thirty_days,2);
            $report[$row_count][14] = number_format($total_three_sixty_days,2);
            $report[$row_count][15] = number_format($total_over_three_sixty_days,2);
            $report[$row_count][16] = number_format($total_unallocated,2);
            $report[$row_count][17] = number_format($total_balance,2);





         
        }
        else if($report_detail == 720)
        {


           $report[$row_count][0] = '';
            $report[$row_count][1] = 'Total';
            $report[$row_count][2] = number_format($total_this_month,2);
            $report[$row_count][3] = number_format($total_three_months,2);
            $report[$row_count][4] = number_format($total_six_months,2);
            $report[$row_count][5] = number_format($total_nine_months,2);
            $report[$row_count][6] = number_format($total_one_twenty_days,2);
            $report[$row_count][7] = number_format($total_one_fifty_days,2);
            $report[$row_count][8] = number_format($total_one_eighty_days,2);
            $report[$row_count][9] = number_format($total_two_ten_days,2);
            $report[$row_count][10] = number_format($total_two_fourty_days,2);
            $report[$row_count][11] = number_format($total_two_seventy_days,2);
            $report[$row_count][12] = number_format($total_three_hundred_days,2);
            $report[$row_count][13] = number_format($total_three_thirty_days,2);
            $report[$row_count][14] = number_format($total_three_sixty_days,2);
            $report[$row_count][15] = number_format($total_three_ninety_days,2);
            $report[$row_count][16] = number_format($total_four_twenty_days,2);
            $report[$row_count][17] = number_format($total_four_fity_days,2);
            $report[$row_count][18] = number_format($total_four_eighty_days,2);
            $report[$row_count][19] = number_format($total_five_ten_days,2);
            $report[$row_count][20] = number_format($total_five_fourty_days,2);
            $report[$row_count][21] = number_format($total_five_seventy_days,2);
            $report[$row_count][22] = number_format($total_six_hundred_days,2);
            $report[$row_count][23] = number_format($total_six_thirty_days,2);
            $report[$row_count][24] = number_format($total_six_sixty_days,2);
            $report[$row_count][25] = number_format($total_six_ninety_days,2);
            $report[$row_count][26] = number_format($total_seven_twenty_days,2);
            $report[$row_count][27] = number_format($total_over_seven_twenty_days,2);
            $report[$row_count][28] = number_format($total_unallocated,2);
            $report[$row_count][29] = number_format($total_balance,2);
         
        }
    }
    
    //create the excel document
    $this->excel->addArray ( $report );
    $this->excel->generateXML ($title);
  }





  public function calculate_creditors_aging_report($report_config=null)
  {

      // $creditor_transaction = $report_config['creditor_transaction'];

     if (isset($report_config['creditor_transaction'])) 
     {
         $creditor_transaction = $report_config['creditor_transaction']->company_configuration_particulars;
        $code_transaction = $creditor_transaction;
      }
      else
        $code_transaction = 'invoiceDate';


      $date_searched_from = $this->session->userdata('date_creditors_batches');
      $date_searched = $this->session->userdata('date_creditors_batches_to');
      $report_detail = $this->session->userdata('report_detail');

      if(empty($date_searched))
      {
        $date_searched = date('Y-m-d');
      }

      $report_type = $this->session->userdata('report_creditor_type');

      if($report_type)
      {
        $date_add = " AND data.$code_transaction <= '".date('Y-m-d')."' ";
      }
      else
      {


        if(!empty($date_searched) AND !empty($date_searched_from))
        {
          $date_add = " AND data.$code_transaction >= '".$date_searched_from."' AND data.$code_transaction <= '".$date_searched."' ";
        }
        else if(!empty($date_searched) AND empty($date_searched_from))
        {
          $date_add = " AND data.$code_transaction <= '".$date_searched."' ";
        }
        else if(empty($date_searched) AND !empty($date_searched_from))
        {
          
          $date_add = " AND data.$code_transaction <= '".$date_searched_from."' ";
        }

      }

      // var_dump($date_add);die();
      $add = '';  
    


      $sql = "CREATE OR REPLACE VIEW v_creditor_ledger_aging AS
            SELECT 

            *,
            creditor.creditor_name AS payable

            FROM 
            (

            SELECT
              `creditor`.`creditor_id` AS `transactionId`,
              '' AS `referenceId`,
              '' AS `payingFor`,
              '' AS `referenceCode`,
              '' AS `transactionCode`,
              '' AS `patient_id`,
                `creditor`.`creditor_id` AS `recepientId`,
              '' AS `accountParentId`,
              '' AS `accountsclassfication`,
              '' AS `accountId`,
              '' AS `accountName`,
              '' AS `transactionName`,
              CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
                0 AS `department_id`,
              `creditor`.`opening_balance` AS `dr_amount`,
              '0' AS `cr_amount`,
              `creditor`.`start_date` AS `invoiceDate`,
              `creditor`.`start_date` AS `transactionDate`,
              `creditor`.`start_date` AS `createdAt`,
              `creditor`.`start_date` AS `referenceDate`,
              `creditor`.`creditor_status` AS `status`,
              'Expense' AS `transactionCategory`,
              'Creditor Opening Balance' AS `transactionClassification`,
              '' AS `transactionTable`,
              'creditor' AS `referenceTable`
            FROM
            creditor
            WHERE debit_id = 2

            UNION ALL

            SELECT
              `creditor`.`creditor_id` AS `transactionId`,
              '' AS `referenceId`,
              '' AS `payingFor`,
              '' AS `referenceCode`,
              '' AS `transactionCode`,
              '' AS `patient_id`,
              `creditor`.`creditor_id` AS `recepientId`,
              '' AS `accountParentId`,
              '' AS `accountsclassfication`,
              '' AS `accountId`,
              '' AS `accountName`,
              '' AS `transactionName`,
              CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
              0 AS `department_id`,
              '0' AS `dr_amount`,
              `creditor`.`opening_balance` AS `cr_amount`,
              `creditor`.`start_date` AS `invoiceDate`,
              `creditor`.`start_date` AS `transactionDate`,
              `creditor`.`start_date` AS `createdAt`,
              `creditor`.`start_date` AS `referenceDate`,
              `creditor`.`creditor_status` AS `status`,
              'Expense' AS `transactionCategory`,
              'Creditor Opening Balance' AS `transactionClassification`,
              '' AS `transactionTable`,
              'creditor' AS `referenceTable`
            FROM
            creditor
            WHERE debit_id = 1

            UNION ALL

            SELECT
              `creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
              `creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
              '' AS `payingFor`,
              `creditor_invoice`.`invoice_number` AS `referenceCode`,
              `creditor_invoice`.`document_number` AS `transactionCode`,
              '' AS `patient_id`,
              `creditor_invoice`.`creditor_id` AS `recepientId`,
              `account`.`parent_account` AS `accountParentId`,
              `account_type`.`account_type_name` AS `accountsclassfication`,
              `creditor_invoice_item`.`account_to_id` AS `accountId`,
              `account`.`account_name` AS `accountName`,
              `creditor_invoice_item`.`item_description` AS `transactionName`,
              `creditor_invoice_item`.`item_description` AS `transactionDescription`,
              
                '0' AS `department_id`,
              `creditor_invoice_item`.`total_amount` AS `dr_amount`,
              '0' AS `cr_amount`,
               `creditor_invoice`.`transaction_date` AS `invoiceDate`,
              `creditor_invoice`.`transaction_date` AS `transactionDate`,
              `creditor_invoice`.`created` AS `createdAt`,
              `creditor_invoice`.`transaction_date` AS `referenceDate`,
              `creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
              'Expense' AS `transactionCategory`,
              'Creditors Invoices' AS `transactionClassification`,
              'creditor_invoice_item' AS `transactionTable`,
              'creditor_invoice' AS `referenceTable`
            FROM
              (
                (
                  (
                    `creditor_invoice_item`,creditor_invoice,creditor
                    
                  )
                  JOIN account ON(
                    (
                      account.account_id = creditor_invoice_item.account_to_id
                    )
                  )
                )
                JOIN `account_type` ON(
                  (
                    account_type.account_type_id = account.account_type_id
                  )
                )
              )
            WHERE 
            creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
            AND creditor_invoice.creditor_invoice_status = 1
            AND creditor.creditor_id = creditor_invoice.creditor_id 
            AND creditor_invoice.transaction_date >= creditor.start_date


            UNION ALL

            SELECT
              `creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
              `creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
              `creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
              `creditor_credit_note`.`invoice_number` AS `referenceCode`,
              `creditor_credit_note`.`document_number` AS `transactionCode`,
              '' AS `patient_id`,
              `creditor_credit_note`.`creditor_id` AS `recepientId`,
              `account`.`parent_account` AS `accountParentId`,
              `account_type`.`account_type_name` AS `accountsclassfication`,
              `creditor_credit_note`.`account_from_id` AS `accountId`,
              `account`.`account_name` AS `accountName`,
              `creditor_credit_note_item`.`description` AS `transactionName`,
              `creditor_credit_note_item`.`description` AS `transactionDescription`,
              0 AS `department_id`,
              0 AS `dr_amount`,
              `creditor_credit_note_item`.`credit_note_amount` AS `cr_amount`,
               `creditor_invoice`.`transaction_date` AS `invoiceDate`,
              `creditor_credit_note`.`transaction_date` AS `transactionDate`,
              `creditor_credit_note`.`created` AS `createdAt`,
              `creditor_invoice`.`transaction_date` AS `referenceDate`,
              `creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
              'Expense Payment' AS `transactionCategory`,
              'Creditors Credit Notes' AS `transactionClassification`,
              'creditor_credit_note' AS `transactionTable`,
              'creditor_credit_note_item' AS `referenceTable`
            FROM
              (
                (
                  (
                    `creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
                    
                  )
                  JOIN account ON(
                    (
                      account.account_id = creditor_credit_note.account_from_id
                    )
                  )
                  
                )
                JOIN `account_type` ON(
                  (
                    account_type.account_type_id = account.account_type_id
                  )
                )
              )
            WHERE 
              creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
              AND creditor_credit_note.creditor_credit_note_status = 1
              AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
              AND creditor_invoice.creditor_invoice_status = 1
              AND creditor.creditor_id = creditor_invoice.creditor_id 
              AND creditor_invoice.transaction_date >= creditor.start_date




              UNION ALL

                SELECT
                `order_supplier`.`order_supplier_id` AS `transactionId`,
                `orders`.`order_id` AS `referenceId`,
                '' AS `payingFor`,
                `orders`.`supplier_invoice_number` AS `referenceCode`,
                '' AS `transactionCode`,
                '' AS `patient_id`,
                `orders`.`supplier_id` AS `recepientId`,
                `account`.`parent_account` AS `accountParentId`,
                `account_type`.`account_type_name` AS `accountsclassfication`,
                `orders`.`account_id` AS `accountId`,
                `account`.`account_name` AS `accountName`,
                'Drug Purchase' AS `transactionName`,
                CONCAT('Purchase of supplies') AS `transactionDescription`,
                0 AS `department_id`,
                ((SUM(`order_supplier`.`less_vat`) + orders.vat_added) - orders.discount_added) AS `dr_amount`,
                '0' AS `cr_amount`,
               `orders`.`supplier_invoice_date` AS `invoiceDate`,
                `orders`.`supplier_invoice_date` AS `transactionDate`,
                `orders`.`created` AS `createdAt`,
                `orders`.`supplier_invoice_date` AS `referenceDate`,
                `orders`.`order_approval_status` AS `status`,
                'Purchases' AS `transactionCategory`,
                'Supplies Invoices' AS `transactionClassification`,
                'order_supplier' AS `transactionTable`,
                'orders' AS `referenceTable`
              FROM
                (
                  (
                    (
                      `order_supplier`
                      JOIN `orders` ON(
                        (
                          orders.order_id = order_supplier.order_id AND orders.order_delete = 0
                        )
                      )
                    )
                    JOIN account ON(
                      (
                        account.account_id = orders.account_id
                      )
                    )


                    JOIN order_item ON(
                      (
                        order_item.order_item_id = order_supplier.order_item_id
                      )
                    )
                  JOIN product ON(
                      (
                        product.product_id = order_item.product_id
                      )
                    )
                  )
                  JOIN `account_type` ON(
                    (
                      account_type.account_type_id = account.account_type_id
                    )
                  )
                )

                WHERE orders.is_store = 0 AND orders.supplier_id > 0 and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
                GROUP BY order_supplier.order_id

              UNION ALL

                SELECT
                `order_supplier`.`order_supplier_id` AS `transactionId`,
                `orders`.`order_id` AS `referenceId`,
                '' AS `payingFor`,
                `orders`.`supplier_invoice_number` AS `referenceCode`,
                `orders`.`reference_number`  AS `transactionCode`,
                '' AS `patient_id`,
                `orders`.`supplier_id` AS `recepientId`,
                `account`.`parent_account` AS `accountParentId`,
                `account_type`.`account_type_name` AS `accountsclassfication`,
                `orders`.`account_id` AS `accountId`,
                `account`.`account_name` AS `accountName`,
                'Credit' AS `transactionName`,
                CONCAT('Credit note of ',' ',`orders`.`reference_number`) AS `transactionDescription`,
                   0 AS `department_id`,
                '0' AS `dr_amount`,
                SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
               `orders`.`supplier_invoice_date` AS `invoiceDate`,
                `orders`.`supplier_invoice_date` AS `transactionDate`,
                `orders`.`created` AS `createdAt`,
                `orders`.`supplier_invoice_date` AS `referenceDate`,
                `orders`.`order_approval_status` AS `status`,
                'Income' AS `transactionCategory`,
                'Supplies Credit Note' AS `transactionClassification`,
                'order_supplier' AS `transactionTable`,
                'orders' AS `referenceTable`

              FROM
                (
                  (
                    (
                      `order_supplier`
                      JOIN `orders` ON(
                        (
                          orders.order_id = order_supplier.order_id AND orders.order_delete = 0
                        )
                      )
                    )
                    JOIN account ON(
                      (
                        account.account_id = orders.account_id
                      )
                    )


                    JOIN order_item ON(
                      (
                        order_item.order_item_id = order_supplier.order_item_id
                      )
                    )
                  JOIN product ON(
                      (
                        product.product_id = order_item.product_id
                      )
                    )
                  )
                  JOIN `account_type` ON(
                    (
                      account_type.account_type_id = account.account_type_id
                    )
                  )
                )

                WHERE orders.is_store = 3 AND orders.supplier_id > 0 and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
                GROUP BY order_supplier.order_id

                UNION ALL
                -- credit invoice payments

                  SELECT
                  `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                  `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                  `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                  `creditor_payment`.`reference_number` AS `referenceCode`,
                  `creditor_payment`.`document_number` AS `transactionCode`,
                  '' AS `patient_id`,
                    `creditor_payment`.`creditor_id` AS `recepientId`,
                  `account`.`parent_account` AS `accountParentId`,
                  `account_type`.`account_type_name` AS `accountsclassfication`,
                  `creditor_payment`.`account_from_id` AS `accountId`,
                  `account`.`account_name` AS `accountName`,
                  `creditor_payment_item`.`description` AS `transactionName`,
                  CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`)  AS `transactionDescription`,
                      0 AS `department_id`,
                  0 AS `dr_amount`,
                  `creditor_payment_item`.`amount_paid` AS `cr_amount`,
                  `creditor_payment`.`transaction_date` AS `invoiceDate`,
                  `creditor_payment`.`transaction_date` AS `transactionDate`,
                  `creditor_payment`.`created` AS `createdAt`,
                  `creditor_invoice`.`transaction_date` AS `referenceDate`,
                  `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                  'Expense Payment' AS `transactionCategory`,
                  'Creditors Invoices Payments' AS `transactionClassification`,
                  'creditor_payment' AS `transactionTable`,
                  'creditor_payment_item' AS `referenceTable`
                FROM
                  (
                    (
                      (
                        `creditor_payment_item`,creditor_payment,creditor_invoice,creditor
                        
                      )
                      JOIN account ON(
                        (
                          account.account_id = creditor_payment.account_from_id
                        )
                      )
                    )
                    JOIN `account_type` ON(
                      (
                        account_type.account_type_id = account.account_type_id
                      )
                    )
                    
                  )
                  WHERE creditor_payment_item.invoice_type = 0 
                  AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
                  AND creditor_payment.creditor_payment_status = 1
                  AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id 
                  AND creditor_invoice.creditor_invoice_status = 1  
                  AND creditor.creditor_id = creditor_invoice.creditor_id 
                  AND creditor_invoice.transaction_date >= creditor.start_date
                  
              UNION ALL

              SELECT
                  `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                  `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                  `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                  `creditor_payment`.`reference_number` AS `referenceCode`,
                  `creditor_payment`.`document_number` AS `transactionCode`,
                  '' AS `patient_id`,
                  `creditor_payment`.`creditor_id` AS `recepientId`,
                  `account`.`parent_account` AS `accountParentId`,
                  `account_type`.`account_type_name` AS `accountsclassfication`,
                  `creditor_payment`.`account_from_id` AS `accountId`,
                  `account`.`account_name` AS `accountName`,
                  `creditor_payment_item`.`description` AS `transactionName`,
                  CONCAT('Payment for invoice of ',' ',`orders`.`supplier_invoice_number`)  AS `transactionDescription`,
                      0 AS `department_id`,
                  0 AS `dr_amount`,
                  `creditor_payment_item`.`amount_paid` AS `cr_amount`,
                  `creditor_payment`.`transaction_date` AS `invoiceDate`,
                  `creditor_payment`.`transaction_date` AS `transactionDate`,
                  `creditor_payment`.`created` AS `createdAt`,
                  `orders`.`supplier_invoice_date` AS `referenceDate`,
                  `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                  'Expense Payment' AS `transactionCategory`,
                  'Creditors Invoices Payments' AS `transactionClassification`,
                  'creditor_payment' AS `transactionTable`,
                  'creditor_payment_item' AS `referenceTable`
                FROM
                  `creditor_payment_item`,creditor_payment,account,account_type,orders
                        
                WHERE creditor_payment_item.invoice_type = 1
                  AND orders.order_id = creditor_payment_item.creditor_invoice_id
                  AND account_type.account_type_id = account.account_type_id
                  AND account.account_id = creditor_payment.account_from_id
                  AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
                  AND creditor_payment.creditor_payment_status = 1

                  UNION ALL

                  SELECT
                  `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                  `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                  `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                  `creditor_payment`.`reference_number` AS `referenceCode`,
                  `creditor_payment`.`document_number` AS `transactionCode`,
                  '' AS `patient_id`,
                  `creditor_payment`.`creditor_id` AS `recepientId`,
                  `account`.`parent_account` AS `accountParentId`,
                  `account_type`.`account_type_name` AS `accountsclassfication`,
                  `creditor_payment`.`account_from_id` AS `accountId`,
                  `account`.`account_name` AS `accountName`,
                  `creditor_payment_item`.`description` AS `transactionName`,
                  CONCAT('Payment of opening balance')  AS `transactionDescription`,
                      0 AS `department_id`,
                  0 AS `dr_amount`,
                  `creditor_payment_item`.`amount_paid` AS `cr_amount`,
                  `creditor_payment`.`transaction_date` AS `invoiceDate`,
                  `creditor_payment`.`transaction_date` AS `transactionDate`,
                  `creditor_payment`.`created` AS `createdAt`,
                  `creditor`.`start_date` AS `referenceDate`,
                  `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                  'Expense Payment' AS `transactionCategory`,
                  'Creditors Invoices Payments' AS `transactionClassification`,
                  'creditor_payment' AS `transactionTable`,
                  'creditor_payment_item' AS `referenceTable`
                FROM
                  (
                    (
                      (
                        `creditor_payment_item`,creditor_payment,creditor
                        
                      )
                      JOIN account ON(
                        (
                          account.account_id = creditor_payment.account_from_id
                        )
                      )
                    )
                    JOIN `account_type` ON(
                      (
                        account_type.account_type_id = account.account_type_id
                      )
                    )
                    
                  )
                  WHERE creditor_payment_item.invoice_type = 2 
                  AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
                  AND creditor_payment.creditor_payment_status = 1
                  AND creditor.creditor_id = creditor_payment_item.creditor_id
                  AND creditor_payment.transaction_date >= creditor.start_date

                  UNION ALL

                  SELECT
                  `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                  `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                  `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                  `creditor_payment`.`reference_number` AS `referenceCode`,
                  `creditor_payment`.`document_number` AS `transactionCode`,
                  '' AS `patient_id`,
                  `creditor_payment`.`creditor_id` AS `recepientId`,
                  `account`.`parent_account` AS `accountParentId`,
                  `account_type`.`account_type_name` AS `accountsclassfication`,
                  `creditor_payment`.`account_from_id` AS `accountId`,
                  `account`.`account_name` AS `accountName`,
                  `creditor_payment_item`.`description` AS `transactionName`,
                  CONCAT('Payment on account')  AS `transactionDescription`,
                      0 AS `department_id`,
                  0 AS `dr_amount`,
                  `creditor_payment_item`.`amount_paid` AS `cr_amount`,
                  `creditor_payment`.`transaction_date` AS `invoiceDate`,
                  `creditor_payment`.`transaction_date` AS `transactionDate`,
                  `creditor_payment`.`created` AS `createdAt`,
                  `creditor_payment`.`created` AS `referenceDate`,
                  `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                  'Expense Payment' AS `transactionCategory`,
                  'Creditors Invoices Payments' AS `transactionClassification`,
                  'creditor_payment' AS `transactionTable`,
                  'creditor_payment_item' AS `referenceTable`
                FROM
                  (
                    (
                      (
                        `creditor_payment_item`,creditor_payment,creditor
                        
                      )
                      JOIN account ON(
                        (
                          account.account_id = creditor_payment.account_from_id
                        )
                      )
                    )
                    JOIN `account_type` ON(
                      (
                        account_type.account_type_id = account.account_type_id
                      )
                    )
                    
                  )
                  WHERE creditor_payment_item.invoice_type = 3 AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
                    AND creditor_payment.creditor_payment_status = 1 
                    AND creditor.creditor_id = creditor_payment.creditor_id AND creditor_payment.transaction_date >= creditor.start_date
                  ) AS data,creditor WHERE data.recepientId = creditor.creditor_id ".$date_add;



      $this->db->query($sql);
  }
  public function get_payables_aging($report_config = null)
  {

    if (isset($report_config['creditor_transaction'])) 
   {
       $creditor_transaction = $report_config['creditor_transaction']->company_configuration_particulars;
      $code_transaction = $creditor_transaction;
    }
    else
      $code_transaction = 'transactionDate';



    //retrieve all users
    $creditor_search =  $this->session->userdata('creditor_search');
    $report_detail = $this->session->userdata('report_creditor_type');

    $date_searched_from = $this->session->userdata('date_creditors_batches');
    $date_searched = $this->session->userdata('date_creditors_batches_to');

    if(empty($date_searched))
    {
      $date_searched = date('Y-m-d');
    }

    if(!empty($creditor_search))
    {
      $where = $creditor_search;
    }
    else
    {
      $where = 'recepientId > 0';
    }

    if(empty($date_searched))
    {
      $date_searched = date('Y-m-d');
    }

    $report_type = $this->session->userdata('report_type');
    $date_add = '';
    if($report_type)
    {
      $date_add = " AND data.$code_transaction <= '".date('Y-m-d')."' ";
    }
    else
    {


      if(!empty($date_searched) AND !empty($date_searched_from))
      {
        $date_add = " AND data.$code_transaction >= '".$date_searched_from."' AND data.$code_transaction <= '".$date_searched."' ";
      }
      else if(!empty($date_searched) AND empty($date_searched_from))
      {
        $date_add = " AND data.$code_transaction <= '".$date_searched."' ";
      }
      else if(empty($date_searched) AND !empty($date_searched_from))
      {
        
        $date_add = " AND data.$code_transaction <= '".$date_searched_from."' ";
      }

    }

    $add = '';  


    $sql_payable = "CREATE OR REPLACE VIEW v_creditor_ledger_aging  AS
                      SELECT 
                      *
                      FROM
                        (
                          SELECT
                            `creditor`.`creditor_id` AS `transactionId`,
                            '' AS `referenceId`,
                            '' AS `payingFor`,
                            '' AS `referenceCode`,
                            '' AS `transactionCode`,
                            '' AS `patient_id`,
                              `creditor`.`creditor_id` AS `recepientId`,
                            '' AS `accountParentId`,
                            '' AS `accountsclassfication`,
                            '' AS `accountId`,
                            '' AS `accountName`,
                            '' AS `transactionName`,
                            CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
                              0 AS `department_id`,
                            `creditor`.`opening_balance` AS `dr_amount`,
                            '0' AS `cr_amount`,
                            `creditor`.`start_date` AS `invoiceDate`,
                            `creditor`.`start_date` AS `transactionDate`,
                            `creditor`.`start_date` AS `createdAt`,
                            `creditor`.`start_date` AS `referenceDate`,
                            `creditor`.`creditor_status` AS `status`,
                            'Expense' AS `transactionCategory`,
                            'Creditor Opening Balance' AS `transactionClassification`,
                            '' AS `transactionTable`,
                            'creditor' AS `referenceTable`
                          FROM
                          creditor
                          WHERE debit_id = 2
                          AND creditor.creditor_status = 0

                          UNION ALL

                          SELECT
                            `creditor`.`creditor_id` AS `transactionId`,
                            '' AS `referenceId`,
                            '' AS `payingFor`,
                            '' AS `referenceCode`,
                            '' AS `transactionCode`,
                            '' AS `patient_id`,
                            `creditor`.`creditor_id` AS `recepientId`,
                            '' AS `accountParentId`,
                            '' AS `accountsclassfication`,
                            '' AS `accountId`,
                            '' AS `accountName`,
                            '' AS `transactionName`,
                            CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
                            0 AS `department_id`,
                            0 AS `dr_amount`,
                            `creditor`.`opening_balance` AS `cr_amount`,
                            `creditor`.`start_date` AS `invoiceDate`,
                            `creditor`.`start_date` AS `transactionDate`,
                            `creditor`.`start_date` AS `createdAt`,
                            `creditor`.`start_date` AS `referenceDate`,
                            `creditor`.`creditor_status` AS `status`,
                            'Expense' AS `transactionCategory`,
                            'Creditor Opening Balance' AS `transactionClassification`,
                            '' AS `transactionTable`,
                            'creditor' AS `referenceTable`
                          FROM
                          creditor
                          WHERE debit_id = 1
                          AND creditor.creditor_status = 0


                          UNION ALL

                          SELECT
                            `creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
                            `creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
                            '' AS `payingFor`,
                            `creditor_invoice`.`invoice_number` AS `referenceCode`,
                            `creditor_invoice`.`document_number` AS `transactionCode`,
                            '' AS `patient_id`,
                            `creditor_invoice`.`creditor_id` AS `recepientId`,
                            `account`.`parent_account` AS `accountParentId`,
                            `account_type`.`account_type_name` AS `accountsclassfication`,
                            `creditor_invoice_item`.`account_to_id` AS `accountId`,
                            `account`.`account_name` AS `accountName`,
                            `creditor_invoice_item`.`item_description` AS `transactionName`,
                            `creditor_invoice_item`.`item_description` AS `transactionDescription`,
                            
                              '0' AS `department_id`,
                            SUM(`creditor_invoice_item`.`total_amount`) AS `dr_amount`,
                            '0' AS `cr_amount`,
                            `creditor_invoice`.`transaction_date` AS `invoiceDate`,
                            `creditor_invoice`.`transaction_date` AS `transactionDate`,
                            `creditor_invoice`.`created` AS `createdAt`,
                            `creditor_invoice`.`transaction_date` AS `referenceDate`,
                            `creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
                            'Expense' AS `transactionCategory`,
                            'Creditors Invoices' AS `transactionClassification`,
                            'creditor_invoice_item' AS `transactionTable`,
                            'creditor_invoice' AS `referenceTable`
                          FROM
                            `creditor_invoice_item`,creditor_invoice,creditor,account,account_type      
                               
                          WHERE 
                          creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
                          AND creditor_invoice.creditor_invoice_status = 1
                          AND account.account_id = creditor_invoice_item.account_to_id
                          AND account_type.account_type_id = account.account_type_id
                          AND creditor.creditor_id = creditor_invoice.creditor_id 
                          AND creditor_invoice.transaction_date >= creditor.start_date
                          AND creditor.creditor_status = 0
                          GROUP BY `creditor_invoice`.`creditor_invoice_id`




                          UNION ALL

                          SELECT
                            `creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
                            `creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
                            `creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
                            `creditor_credit_note`.`invoice_number` AS `referenceCode`,
                            `creditor_credit_note`.`document_number` AS `transactionCode`,
                            '' AS `patient_id`,
                            `creditor_credit_note`.`creditor_id` AS `recepientId`,
                            `account`.`parent_account` AS `accountParentId`,
                            `account_type`.`account_type_name` AS `accountsclassfication`,
                            `creditor_credit_note`.`account_from_id` AS `accountId`,
                            `account`.`account_name` AS `accountName`,
                            `creditor_credit_note_item`.`description` AS `transactionName`,
                            `creditor_credit_note_item`.`description` AS `transactionDescription`,
                            0 AS `department_id`,
                            0 AS `dr_amount`,
                            `creditor_credit_note_item`.`credit_note_amount` AS `cr_amount`,
                            `creditor_invoice`.`transaction_date` AS `invoiceDate`,
                            `creditor_credit_note`.`transaction_date` AS `transactionDate`,
                            `creditor_credit_note`.`created` AS `createdAt`,
                            `creditor_invoice`.`transaction_date` AS `referenceDate`,
                            `creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
                            'Expense Payment' AS `transactionCategory`,
                            'Creditors Credit Notes' AS `transactionClassification`,
                            'creditor_credit_note' AS `transactionTable`,
                            'creditor_credit_note_item' AS `referenceTable`
                          FROM
                             `creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor,account,account_type
                          WHERE 
                            creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
                            AND creditor_credit_note.creditor_credit_note_status = 1
                            AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
                            AND creditor_invoice.creditor_invoice_status = 1
                            AND creditor.creditor_id = creditor_invoice.creditor_id 
                            AND account_type.account_type_id = account.account_type_id
                            AND account.account_id = creditor_credit_note.account_from_id
                            AND creditor_invoice.transaction_date >= creditor.start_date
                            AND creditor.creditor_status = 0

                            UNION ALL


                          SELECT
                            `creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
                            `creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
                            `creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
                            `creditor_credit_note`.`invoice_number` AS `referenceCode`,
                            `creditor_credit_note`.`document_number` AS `transactionCode`,
                            '' AS `patient_id`,
                            `creditor_credit_note`.`creditor_id` AS `recepientId`,
                            `account`.`parent_account` AS `accountParentId`,
                            `account_type`.`account_type_name` AS `accountsclassfication`,
                            `creditor_credit_note`.`account_from_id` AS `accountId`,
                            `account`.`account_name` AS `accountName`,
                            `creditor_credit_note_item`.`description` AS `transactionName`,
                            `creditor_credit_note_item`.`description` AS `transactionDescription`,
                            0 AS `department_id`,
                            0 AS `dr_amount`,
                            `creditor_credit_note_item`.`credit_note_amount` AS `cr_amount`,
                            `creditor_credit_note`.`transaction_date` AS `invoiceDate`,
                            `creditor_credit_note`.`transaction_date` AS `transactionDate`,
                            `creditor_credit_note`.`created` AS `createdAt`,
                            `creditor_credit_note`.`transaction_date` AS `referenceDate`,
                            `creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
                            'Ex' AS `transactionCategory`,
                            'Creditors Credit Notes' AS `transactionClassification`,
                            'creditor_credit_note' AS `transactionTable`,
                            'creditor_credit_note_item' AS `referenceTable`
                          FROM
                             `creditor_credit_note_item`,creditor_credit_note,creditor,account,account_type
                          WHERE 
                            creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
                            AND creditor_credit_note.creditor_credit_note_status = 1
                            AND creditor_credit_note.creditor_invoice_id = 0
                            AND creditor.creditor_id = creditor_credit_note.creditor_id 
                            AND account_type.account_type_id = account.account_type_id
                            AND account.account_id = creditor_credit_note.account_from_id
                            AND creditor_credit_note.transaction_date >= creditor.start_date
                            AND creditor.creditor_status = 0

                            UNION ALL

                            SELECT
                            `order_supplier`.`order_supplier_id` AS `transactionId`,
                            `orders`.`order_id` AS `referenceId`,
                            '' AS `payingFor`,
                            `orders`.`supplier_invoice_number` AS `referenceCode`,
                            '' AS `transactionCode`,
                            '' AS `patient_id`,
                            `orders`.`supplier_id` AS `recepientId`,
                            `account`.`parent_account` AS `accountParentId`,
                            `account_type`.`account_type_name` AS `accountsclassfication`,
                            `orders`.`account_id` AS `accountId`,
                            `account`.`account_name` AS `accountName`,
                            'Drug Purchase' AS `transactionName`,
                            CONCAT('Purchase of supplies') AS `transactionDescription`,
                            0 AS `department_id`,
                            SUM(`order_supplier`.`less_vat`) AS `dr_amount`,
                            '0' AS `cr_amount`,
                            `orders`.`supplier_invoice_date` AS `invoiceDate`,
                            `orders`.`supplier_invoice_date` AS `transactionDate`,
                            `orders`.`created` AS `createdAt`,
                            `orders`.`supplier_invoice_date` AS `referenceDate`,
                            `orders`.`order_approval_status` AS `status`,
                            'Purchases' AS `transactionCategory`,
                            'Supplies Invoices' AS `transactionClassification`,
                            'order_supplier' AS `transactionTable`,
                            'orders' AS `referenceTable`
                          FROM
                           `order_supplier`,orders,account,order_item,product,account_type,creditor
                                  

                          WHERE orders.is_store = 0 AND orders.supplier_id > 0 
                            AND orders.order_id = order_supplier.order_id
                            AND account.account_id = orders.account_id
                            AND order_item.order_item_id = order_supplier.order_item_id
                            AND product.product_id = order_item.product_id
                            AND account_type.account_type_id = account.account_type_id
                            and orders.order_approval_status = 7 
                            AND creditor.creditor_id = orders.supplier_id
                            AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
                            AND creditor.creditor_status = 0
                            AND orders.supplier_invoice_date >= creditor.start_date
                            GROUP BY order_supplier.order_id

                            UNION ALL

                              SELECT
                              `order_supplier`.`order_supplier_id` AS `transactionId`,
                              `orders`.`order_id` AS `referenceId`,
                              '' AS `payingFor`,
                              `orders`.`supplier_invoice_number` AS `referenceCode`,
                              `orders`.`reference_number`  AS `transactionCode`,
                              '' AS `patient_id`,
                              `orders`.`supplier_id` AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `orders`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              'Credit' AS `transactionName`,
                              CONCAT('Credit note of ',' ',`orders`.`reference_number`) AS `transactionDescription`,
                                 0 AS `department_id`,
                              0 AS `dr_amount`,
                              SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
                              `orders`.`supplier_invoice_date` AS `invoiceDate`,
                              `orders`.`supplier_invoice_date` AS `transactionDate`,
                              `orders`.`created` AS `createdAt`,
                              `orders`.`supplier_invoice_date` AS `referenceDate`,
                              `orders`.`order_approval_status` AS `status`,
                              'Income' AS `transactionCategory`,
                              'Supplies Credit Note' AS `transactionClassification`,
                              'order_supplier' AS `transactionTable`,
                              'orders' AS `referenceTable`

                            FROM
                            `order_supplier`,orders,account,order_item,product,account_type,creditor
                             

                              WHERE orders.is_store = 3
                              AND orders.order_id = order_supplier.order_id
                              AND account.account_id = orders.account_id
                              AND order_item.order_item_id = order_supplier.order_item_id
                              AND product.product_id = order_item.product_id
                              AND account_type.account_type_id = account.account_type_id
                               AND orders.supplier_id > 0 and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)

                            AND creditor.creditor_id = orders.supplier_id
                            AND creditor.creditor_status = 0
                            AND orders.supplier_invoice_date >= creditor.start_date
                              GROUP BY order_supplier.order_id

                              UNION ALL
                              -- credit invoice payments

                                SELECT
                                `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                                `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                                `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                                `creditor_payment`.`reference_number` AS `referenceCode`,
                                `creditor_payment`.`document_number` AS `transactionCode`,
                                '' AS `patient_id`,
                                  `creditor_payment`.`creditor_id` AS `recepientId`,
                                `account`.`parent_account` AS `accountParentId`,
                                `account_type`.`account_type_name` AS `accountsclassfication`,
                                `creditor_payment`.`account_from_id` AS `accountId`,
                                `account`.`account_name` AS `accountName`,
                                `creditor_payment_item`.`description` AS `transactionName`,
                                CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`)  AS `transactionDescription`,
                                    0 AS `department_id`,
                                0 AS `dr_amount`,
                                `creditor_payment_item`.`amount_paid` AS `cr_amount`,
                                `creditor_invoice`.`transaction_date` AS `invoiceDate`,
                                `creditor_payment`.`transaction_date` AS `transactionDate`,
                                `creditor_payment`.`created` AS `createdAt`,
                                `creditor_invoice`.`transaction_date` AS `referenceDate`,
                                `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                                'Expense Payment' AS `transactionCategory`,
                                'Creditors Invoices Payments' AS `transactionClassification`,
                                'creditor_payment' AS `transactionTable`,
                                'creditor_payment_item' AS `referenceTable`
                              FROM
                                `creditor_payment_item`,creditor_payment,creditor_invoice,creditor,account,account_type
                              WHERE creditor_payment_item.invoice_type = 0 
                                AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
                                AND creditor_payment.creditor_payment_status = 1
                                AND account.account_id = creditor_payment.account_from_id
                                AND account_type.account_type_id = account.account_type_id
                                AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id 
                                AND creditor_invoice.creditor_invoice_status = 1  
                                AND creditor.creditor_id = creditor_invoice.creditor_id 
                                AND creditor_invoice.transaction_date >= creditor.start_date
                                AND creditor.creditor_status = 0

                            UNION ALL

                            SELECT
                                `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                                `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                                `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                                `creditor_payment`.`reference_number` AS `referenceCode`,
                                `creditor_payment`.`document_number` AS `transactionCode`,
                                '' AS `patient_id`,
                                `creditor_payment`.`creditor_id` AS `recepientId`,
                                `account`.`parent_account` AS `accountParentId`,
                                `account_type`.`account_type_name` AS `accountsclassfication`,
                                `creditor_payment`.`account_from_id` AS `accountId`,
                                `account`.`account_name` AS `accountName`,
                                `creditor_payment_item`.`description` AS `transactionName`,
                                CONCAT('Payment for invoice of ',' ',`orders`.`supplier_invoice_number`)  AS `transactionDescription`,
                                0 AS `department_id`,
                                0 AS `dr_amount`,
                                `creditor_payment_item`.`amount_paid` AS `cr_amount`,
                                `orders`.`supplier_invoice_date` AS `invoiceDate`,
                                `creditor_payment`.`transaction_date` AS `transactionDate`,
                                `creditor_payment`.`created` AS `createdAt`,
                                `orders`.`supplier_invoice_date` AS `referenceDate`,
                                `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                                'Expense Payment' AS `transactionCategory`,
                                'Creditors Invoices Payments' AS `transactionClassification`,
                                'creditor_payment' AS `transactionTable`,
                                'creditor_payment_item' AS `referenceTable`
                              FROM
                                `creditor_payment_item`,creditor_payment,account,account_type,orders,creditor  
                              WHERE 
                                creditor_payment_item.invoice_type = 1
                                AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
                                AND creditor_payment.creditor_payment_status = 1
                                AND  account.account_id = creditor_payment.account_from_id
                                AND account_type.account_type_id = account.account_type_id
                                AND orders.order_id = creditor_payment_item.creditor_invoice_id
                                AND creditor.creditor_id = orders.supplier_id
                                AND creditor.creditor_status = 0
                                AND orders.supplier_invoice_date >= creditor.start_date

                                UNION ALL

                                SELECT
                                `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                                `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                                `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                                `creditor_payment`.`reference_number` AS `referenceCode`,
                                `creditor_payment`.`document_number` AS `transactionCode`,
                                '' AS `patient_id`,
                                `creditor_payment`.`creditor_id` AS `recepientId`,
                                `account`.`parent_account` AS `accountParentId`,
                                `account_type`.`account_type_name` AS `accountsclassfication`,
                                `creditor_payment`.`account_from_id` AS `accountId`,
                                `account`.`account_name` AS `accountName`,
                                `creditor_payment_item`.`description` AS `transactionName`,
                                CONCAT('Payment of opening balance')  AS `transactionDescription`,
                                    0 AS `department_id`,
                                0 AS `dr_amount`,
                                `creditor_payment_item`.`amount_paid` AS `cr_amount`,
                                `creditor`.`start_date` AS `invoiceDate`,
                                `creditor_payment`.`transaction_date` AS `transactionDate`,
                                `creditor_payment`.`created` AS `createdAt`,
                                `creditor`.`start_date` AS `referenceDate`,
                                `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                                'Expense Payment' AS `transactionCategory`,
                                'Creditors Invoices Payments' AS `transactionClassification`,
                                'creditor_payment' AS `transactionTable`,
                                'creditor_payment_item' AS `referenceTable`
                              FROM
                                `creditor_payment_item`,creditor_payment,creditor,account,account_type
                                      
                                   
                              WHERE creditor_payment_item.invoice_type = 2 
                                AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
                                AND creditor_payment.creditor_payment_status = 1
                                AND account.account_id = creditor_payment.account_from_id
                                AND account_type.account_type_id = account.account_type_id
                                AND creditor.creditor_id = creditor_payment_item.creditor_id
                                AND creditor.creditor_status = 0
                                AND creditor_payment.transaction_date >= creditor.start_date

                                UNION ALL

                                SELECT
                                `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                                `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                                `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                                `creditor_payment`.`reference_number` AS `referenceCode`,
                                `creditor_payment`.`document_number` AS `transactionCode`,
                                '' AS `patient_id`,
                                `creditor_payment`.`creditor_id` AS `recepientId`,
                                `account`.`parent_account` AS `accountParentId`,
                                `account_type`.`account_type_name` AS `accountsclassfication`,
                                `creditor_payment`.`account_from_id` AS `accountId`,
                                `account`.`account_name` AS `accountName`,
                                `creditor_payment_item`.`description` AS `transactionName`,
                                CONCAT('Payment on account')  AS `transactionDescription`,
                                    0 AS `department_id`,
                                0 AS `dr_amount`,
                                `creditor_payment_item`.`amount_paid` AS `cr_amount`,
                                `creditor`.`start_date` AS `invoiceDate`,
                                `creditor_payment`.`transaction_date` AS `transactionDate`,
                                `creditor_payment`.`created` AS `createdAt`,
                                `creditor_payment`.`created` AS `referenceDate`,
                                `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                                'Expense Payment' AS `transactionCategory`,
                                'Creditors Invoices Payments' AS `transactionClassification`,
                                'creditor_payment' AS `transactionTable`,
                                'creditor_payment_item' AS `referenceTable`
                              FROM
                                `creditor_payment_item`,creditor_payment,creditor,account,account_type
                              WHERE creditor_payment_item.invoice_type = 3 
                                  AND account.account_id = creditor_payment.account_from_id
                                  AND  account_type.account_type_id = account.account_type_id
                                  AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
                                  AND creditor_payment.creditor_payment_status = 1 
                                  AND creditor.creditor_id = creditor_payment.creditor_id

                                  AND creditor.creditor_status = 0
                                   AND creditor_payment.transaction_date >= creditor.start_date
                                
                            )AS  data WHERE data.transactionId > 0  ".$date_add;


                      $this->db->query($sql_payable);
                      // var_dump("hre");die();
                      $sql_other = "CREATE OR REPLACE VIEW v_creditor_ledger_aging_by_date AS SELECT * FROM v_creditor_ledger_aging ORDER BY $code_transaction ASC";
                      $this->db->query($sql_other);
                         // var_dump("hre");die();

      // $add = ' AND v_creditor_ledger_aging.recepientId = 39';
   
    if($report_detail == 180)
    {

      $sql = "
        
          SELECT 
           *
          FROM 
          (
            SELECT
              v_creditor_ledger_aging.recepientIds AS recepientId,
              '' as receivable,
              2 as branch_id,
              
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) = 0 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `coming_due`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 1 AND 30 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `thirty_days`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 31 AND 60 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `sixty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 61 AND 90 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `ninety_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 91 AND 120 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_twenty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 121 AND 150 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_fifty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 151 AND 180 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_eighty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) > 180 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `over_one_eighty_days`     
              FROM
              `v_creditor_ledger_aging`
              WHERE `v_creditor_ledger_aging`.`recepientId` > 0 ".$add."
              GROUP BY  `v_creditor_ledger_aging`.`recepientId`
             ) AS data ORDER BY data.`receivable` ASC
           ";

    }
    else if($report_detail == 360)
    {
      $sql = "
        
          SELECT 
           *
          FROM 
          (
            SELECT
              v_creditor_ledger_aging.recepientId AS recepientId,
              '' as receivable,
              2 as branch_id,
              
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) = 0 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `coming_due`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 1 AND 30 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `thirty_days`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 31 AND 60 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `sixty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 61 AND 90 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `ninety_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 91 AND 120 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_twenty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 121 AND 150 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_fifty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 151 AND 180 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_eighty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 181 AND 210 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `two_ten_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 211 AND 240 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `two_fourty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 241 AND 270 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `two_seventy_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 271 AND 300 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_hundred_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 301 AND 330 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_thirty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 331 AND 360 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_sixty_days`,
              
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) > 360 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `over_three_sixty_days`     
              FROM
              `v_creditor_ledger_aging`
              WHERE `v_creditor_ledger_aging`.`recepientId` > 0 ".$add."
              GROUP BY  `v_creditor_ledger_aging`.`recepientId`
             ) AS data ORDER BY data.`receivable` ASC";
    }
    else if($report_detail == 720)
    {
      $sql = "
        
          SELECT 
           *
          FROM 
          (
            SELECT
              v_creditor_ledger_aging.recepientId AS recepientId,
              '' as receivable,
              2 as branch_id,
              
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) = 0 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `coming_due`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 1 AND 30 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `thirty_days`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 31 AND 60 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `sixty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 61 AND 90 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `ninety_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 91 AND 120 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_twenty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 121 AND 150 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_fifty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 151 AND 180 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_eighty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 181 AND 210 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `two_ten_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 211 AND 240 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `two_fourty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 241 AND 270 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `two_seventy_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 271 AND 300 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_hundred_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 301 AND 330 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_thirty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 331 AND 360 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_sixty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 361 AND 390 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_ninety_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 391 AND 420 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `four_twenty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 421 AND 450 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `four_fifty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 451 AND 480 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `four_eighty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 481 AND 510 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `five_ten_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 510 AND 540 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `five_fourty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 541 AND 570 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `five_seventy_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 571 AND 600 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `six_hundred_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 601 AND 630 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `six_thirty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 631 AND 660 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `six_sixty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 661 AND 690 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `six_ninety_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 691 AND 720 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `seven_twenty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) > 720 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `over_seven_twenty_days`     
              FROM
              `v_creditor_ledger_aging`
              WHERE `v_creditor_ledger_aging`.`recepientId` > 0 ".$add."
              GROUP BY  `v_creditor_ledger_aging`.`recepientId`
             ) AS data ORDER BY data.`receivable` ASC
           ";
    }
    else
    {

      $sql = "
        
          SELECT 
           *
          FROM 
          (
            SELECT
              v_creditor_ledger_aging.recepientId AS recepientId,
              '' as receivable,
              2 as branch_id,
              
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) = 0 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `coming_due`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 1 AND 30 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `thirty_days`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 31 AND 60 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `sixty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 61 AND 90 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `ninety_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 91 AND 120 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_twenty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 121 AND 150 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_fifty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) BETWEEN 151 AND 180 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_eighty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_creditor_ledger_aging.$code_transaction) > 180 ),(v_creditor_ledger_aging.dr_amount - v_creditor_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `over_one_eighty_days`     
              FROM
              `v_creditor_ledger_aging`
              WHERE `v_creditor_ledger_aging`.`recepientId` > 0 ".$add."
              GROUP BY  `v_creditor_ledger_aging`.`recepientId`
             ) AS data ORDER BY data.`receivable` ASC
           ";

    }
   
    

    $query = $this->db->query($sql);



    // $this->db->from('v_aged_payables');
    // $this->db->select('*');
    // $this->db->where($where);
    // $this->db->order_by('payables','ASC');
    // $query = $this->db->get();

    return $query;
  }

  public function get_all_vendors($report_config=null)
  {

    $branch_id =  $this->session->userdata('branch_id');

    
    $where = '';
    if(!empty($branch_id))
      $where .= ' AND branch_id = '.$branch_id;


    if(isset($report_config))
    {
      if(isset($report_config['unlimit_accounts']))
        $unlimit_accounts = $report_config['unlimit_accounts']->company_configuration_particulars;
      else
        $unlimit_accounts = NULL;



      if($unlimit_accounts == "true")
        $where = '';
      
    }



    $this->db->from('creditor');
    $this->db->select('*');
    $this->db->where('creditor_status = 0'.$where);
    $this->db->order_by('creditor_name','ASC');
    $query = $this->db->get();

    return $query;
  }

    public function get_creditor_name($creditor_id)
  {
    $account_name = '';
    $this->db->select('creditor_name');
    $this->db->where('creditor_id = '.$creditor_id);
    $query = $this->db->get('creditor');

    $account_name = '';
    if($query->num_rows () > 0)
    {
      $account_details = $query->row();
      $account_name = $account_details->creditor_name;
    }


    return $account_name;
  }

    public function get_provider_name($provider_id)
  {
    $account_name = '';
    $this->db->select('provider_name');
    $this->db->where('provider_id = '.$provider_id);
    $query = $this->db->get('provider');

    $account_name = '';
    if($query->num_rows () > 0)
    {
      $account_details = $query->row();
      $account_name = $account_details->provider_name;
    }


    return $account_name;
  }

   public function get_payment_vouchers_items($creditor_id,$creditor_payment_id=0)
  { 
    
    $select ="SELECT
              *
              FROM(
              ";

      $select .="
                  SELECT
                    `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                    `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                    `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                    `creditor_payment`.`reference_number` AS `referenceCode`,
                    `creditor_payment`.`document_number` AS `transactionCode`,
                    '' AS `patient_id`,
                    `creditor_payment`.`creditor_id` AS `recepientId`,
                    '' AS `accountsclassfication`,
                    `creditor_payment`.`account_from_id` AS `accountId`,
                    0 AS `accountFromId`,
                    `creditor_payment_item`.`description` AS `transactionName`,
                    CONCAT('<b>Invoice: </b> ',' ',`creditor_invoice`.`invoice_number`,'<b> Invoice Date: </b>', `creditor_invoice`.`transaction_date`,' <b> Amount Paid :</b>',creditor_payment_item.amount_paid)  AS `transactionDescription`,
                    `creditor_payment_item`.`amount_paid` AS `amount`,
                    `creditor_payment`.`transaction_date` AS `transactionDate`,
                    `creditor_payment`.`created` AS `createdAt`,
                    `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                     creditor.branch_id AS `branch_id`,
                    'Creditor Payment' AS `transactionCategory`,
                    'Creditors Invoices Payments' AS `transactionClassification`,
                    'creditor_payment' AS `transactionTable`,
                    'creditor_payment_item' AS `referenceTable`,
                    CONCAT('open-creditor-payments/',creditor.creditor_id,'/',`creditor_payment`.`creditor_payment_id`) AS link,
                    creditor_payment.recon_id AS recon_id
                  FROM

                    `creditor_payment_item`,creditor_payment,creditor_invoice,creditor


                  WHERE creditor_payment_item.invoice_type = 0
                    AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
                    AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
                    AND creditor_payment.creditor_payment_status = 1
                    AND creditor_invoice.creditor_invoice_status = 1
                    AND creditor_invoice.creditor_id = creditor.creditor_id
                    AND creditor_invoice.transaction_date >= creditor.start_date
                  -- GROUP BY creditor_payment_item.creditor_payment_id



                  UNION ALL

                  SELECT
                    `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                    `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                    `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                    `creditor_payment`.`reference_number` AS `referenceCode`,
                    `creditor_payment`.`document_number` AS `transactionCode`,
                    '' AS `patient_id`,
                    `creditor_payment`.`creditor_id` AS `recepientId`,
                    '' AS `accountsclassfication`,
                    `creditor_payment`.`account_from_id` AS `accountId`,
                    0 AS `accountFromId`,
                    `creditor_payment_item`.`description` AS `transactionName`,
                    CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
                    SUM(`creditor_payment_item`.`amount_paid`) AS `amount`,
                    `creditor_payment`.`transaction_date` AS `transactionDate`,
                    `creditor_payment`.`created` AS `createdAt`,
                    `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                     creditor.branch_id AS `branch_id`,
                    'Creditor Payment' AS `transactionCategory`,
                    'Creditors Opening Balance Payment' AS `transactionClassification`,
                    'creditor_payment' AS `transactionTable`,
                    'creditor_payment_item' AS `referenceTable`,
                    CONCAT('open-creditor-payments/',creditor.creditor_id,'/',`creditor_payment`.`creditor_payment_id`) AS link,
                    creditor_payment.recon_id AS recon_id
                  FROM

                    `creditor_payment_item`,creditor_payment,creditor

                  WHERE creditor_payment_item.invoice_type = 2
                  AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
                  AND creditor.creditor_id = creditor_payment_item.creditor_id
                  AND creditor_payment.transaction_date >= creditor.start_date
                  GROUP BY creditor_payment_item.creditor_payment_id

                UNION ALL

                  SELECT
                    `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                    `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                    `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                    `creditor_payment`.`reference_number` AS `referenceCode`,
                    `creditor_payment`.`document_number` AS `transactionCode`,
                    '' AS `patient_id`,
                    `creditor_payment`.`creditor_id` AS `recepientId`,
                    '' AS `accountsclassfication`,
                    `creditor_payment`.`account_from_id` AS `accountId`,
                    0 AS `accountFromId`,
                    `creditor_payment_item`.`description` AS `transactionName`,
                    CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
                    SUM(`creditor_payment_item`.`amount_paid`) AS `amount`,
                    `creditor_payment`.`transaction_date` AS `transactionDate`,
                    `creditor_payment`.`created` AS `createdAt`,
                    `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                     creditor.branch_id AS `branch_id`,
                    'Creditor Payment' AS `transactionCategory`,
                    'Creditors Invoices Payments' AS `transactionClassification`,
                    'creditor_payment' AS `transactionTable`,
                    'creditor_payment_item' AS `referenceTable`,
                    CONCAT('open-creditor-payments/',creditor.creditor_id,'/',`creditor_payment`.`creditor_payment_id`) AS link,
                    creditor_payment.recon_id AS recon_id
                  FROM

                    `creditor_payment_item`,creditor_payment,creditor

                  WHERE
                    creditor_payment_item.invoice_type = 3
                    AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
                    AND creditor.creditor_id = creditor_payment_item.creditor_id
                    AND creditor_payment.transaction_date >= creditor.start_date
                  GROUP BY creditor_payment_item.creditor_payment_id
            )AS data WHERE data.transactionId > 0 AND data.referenceId = $creditor_payment_id ORDER BY data.transactionDate ASC ";
      $query = $this->db->query($select);
      return $query;
  }



}
?>
