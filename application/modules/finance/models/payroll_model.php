<?php

class payroll_model extends CI_Model
{
  /*
  * Retrieve all provider
  * @param string $table
  *   @param string $where
  *
  */
  public function get_all_payroll($table, $where, $per_page, $page, $order = 'provider_name', $order_method = 'ASC')
  {
    //retrieve all users
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }

	/*
	*	Add a new provider
	*
	*/
  public function get_payroll_list($table, $where, $order)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order,'asc');
		$query = $this->db->get('');

		return $query;
	}

  /*
	*	get a single provider's details
	*	@param int $provider_id
	*
	*/
	public function get_statutory_accounts($account_id)
	{
		//retrieve all users
		$this->db->from('statutory_accounts');
		$this->db->select('*');
		$this->db->where('statutory_account_id = '.$account_id);
		$query = $this->db->get();

		return $query;
	}  
  public function get_all_statutories_details($table, $where, $per_page, $page, $order = 'statutory_name', $order_method = 'ASC')
  {
    //retrieve all users
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    // $this->db->group_by('provider_invoice.provider_invoice_id');
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }


  public function add_invoice_item($statutory_id,$statutory_invoice_id)
	{
		$amount = $this->input->post('unit_price');
		$account_to_id=$this->input->post('account_to_id');
    $item_description = $this->input->post('item_description');
		$quantity=$this->input->post('quantity');
    $vat_amount = $this->input->post('vat_amount');
    $total_amount = $this->input->post('total_amount');
		$tax_type_id=$this->input->post('tax_type_id');


		$service = array(
							'statutory_invoice_id'=>0,
							'unit_price'=> $amount,
							'account_to_id' => $account_to_id,
							'statutory_id' => $statutory_id,
              'item_description'=>$item_description,
							'created_by' => $this->session->userdata('personnel_id'),
							'created' => date('Y-m-d'),
              'total_amount'=>$total_amount,
              'vat_amount'=>$vat_amount,
              'quantity'=>$quantity,
              'vat_type_id'=>$tax_type_id
						);
    if(!empty($statutory_invoice_id))
    {
      $service['statutory_invoice_id'] = $statutory_invoice_id;
      $service['statutory_invoice_item_status'] = 1;
    }
    else
    {
      $service['statutory_invoice_item_status'] = 0;
    }


		$this->db->insert('payroll_invoice_item',$service);
		return TRUE;

	}

  public function confirm_statutory_invoice($statutory_id,$statutory_invoice_id = NULL)
	{
		$amount = $this->input->post('amount');
		$amount_charged = $this->input->post('amount_charged');
		$invoice_date = $this->input->post('invoice_date');
    $vat_charged = $this->input->post('vat_charged');
    $invoice_number = $this->input->post('invoice_number');

		$date_check = explode('-', $invoice_date);
		$month = $date_check[1];
		$year = $date_check[0];


		$document_number = $this->create_invoice_number();

		// var_dump($checked); die();

		$insertarray['transaction_date'] = $invoice_date;
		$insertarray['invoice_year'] = $year;
		$insertarray['invoice_month'] = $month;
		$insertarray['statutory_id'] = $statutory_id;
		$insertarray['document_number'] = $document_number;
    $insertarray['invoice_number'] = strtoupper($invoice_number);
		$insertarray['total_amount'] = $amount_charged;
		$insertarray['vat_charged'] = $vat_charged;
		$insertarray['created_by'] = $this->session->userdata('personnel_id');
		$insertarray['created'] = date('Y-m-d');
		$insertarray['amount'] = $amount;

    if(!empty($statutory_invoice_id))
    {
        $this->db->where('statutory_invoice_id',$statutory_invoice_id);
        if($this->db->update('payroll_invoice', $insertarray))
        {

          $total_visits = sizeof($_POST['payroll_invoice_items']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['payroll_invoice_items'];
              $statutory_invoice_item_id = $visit[$r];
              //check if card is held
              $service = array(
                        'statutory_invoice_id'=>$statutory_invoice_id,
                        'created' =>$invoice_date,
                        'statutory_invoice_item_status'=>1,
                        'year'=>$year,
                        'month'=>$month,
                      );
              $this->db->where('statutory_invoice_item_id',$statutory_invoice_item_id);
              $this->db->update('payroll_invoice_item',$service);
            }
          }

          return TRUE;
        }
    }
    else
    {
      if($this->db->insert('payroll_invoice', $insertarray))
      {

        $statutory_invoice_id = $this->db->insert_id();
        $total_visits = sizeof($_POST['payroll_invoice_items']);
        //check if any checkboxes have been ticked
        if($total_visits > 0)
        {
          for($r = 0; $r < $total_visits; $r++)
          {
            $visit = $_POST['payroll_invoice_items'];
            $statutory_invoice_item_id = $visit[$r];
            //check if card is held
            $service = array(
                      'statutory_invoice_id'=>$statutory_invoice_id,
                      'created' =>$invoice_date,
                      'statutory_invoice_item_status'=>1,
                      'year'=>$year,
                      'month'=>$month,
                    );
            $this->db->where('statutory_invoice_item_id',$statutory_invoice_item_id);
            $this->db->update('payroll_invoice_item',$service);
          }
        }

        return TRUE;
      }
    }
		

	}




  public function create_invoice_number()
	{
		//select product code
		$this->db->where('provider_invoice_id > 0');
		$this->db->from('provider_invoice');
		$this->db->select('MAX(document_number) AS number');
		$this->db->order_by('provider_invoice_id','DESC');
		// $this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			// var_dump($number);die();
			$number++;

		}
		else{
			$number = 1;
		}
		// var_dump($number);die();
		return $number;
	}

  public function get_provider_invoice($provider_id,$limit=null)
	{
		$this->db->where('provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id AND provider_invoice.provider_invoice_status = 1 AND provider_invoice.provider_id = '.$provider_id);
		if($limit)
		{
			$this->db->limit($limit);
		}
		$this->db->group_by('provider_invoice.provider_invoice_id');
		$this->db->order_by('provider_invoice.transaction_date','DESC');
		return $this->db->get('provider_invoice_item,provider_invoice');
	}



  public function get_statutory_invoice_number($provider_id,$limit=null)
  {
    // $this->db->where('v_payroll_invoice_balances.provider_id = '.$provider_id);
    // $this->db->select('*');
    // return $this->db->get('v_payroll_invoice_balances');

    $select_statement = "
                        SELECT
                          data.transactionId AS provider_invoice_id,
                          data.transactionName AS invoice_number,
                          data.transactionDate AS invoice_date,
                          data.transactionClassification AS provider_invoice_type,
                          COALESCE (SUM(data.dr_amount),0) AS dr_amount,
                          COALESCE (SUM(data.cr_amount),0) AS cr_amount,
                          COALESCE (SUM(data.dr_amount),0) - COALESCE (SUM(data.cr_amount),0) AS balance
                        FROM 
                        (
                         

                           SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              '' AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.total_payroll AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'Payroll' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 1
                            AND account.account_id = statutory_accounts.account_id
                            AND account_type.account_type_id = account.account_type_id
                            AND payroll.payroll_status = 1


                            UNION ALL 


                            SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              '' AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.paye AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'PAYE' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 2
                            AND account.account_id = statutory_accounts.account_id
                            AND account_type.account_type_id = account.account_type_id
                            AND payroll.payroll_status = 1



                            UNION ALL 


                            SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              '' AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.nssf AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'PAYE' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 3
                            AND account.account_id = statutory_accounts.account_id
                            AND account_type.account_type_id = account.account_type_id
                            AND payroll.payroll_status = 1


                            UNION ALL




                            SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              '' AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.nhif AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'NHIF' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 4
                            AND account.account_id = statutory_accounts.account_id
                            AND account_type.account_type_id = account.account_type_id
                            AND payroll.payroll_status = 1

                            UNION ALL




                             SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              '' AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.savings AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'Savings' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 5
                            AND account.account_id = statutory_accounts.account_id
                            AND account_type.account_type_id = account.account_type_id
                            AND payroll.payroll_status = 1



                             UNION ALL




                             SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              '' AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.housing_levy AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'Savings' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 6
                            AND account.account_id = statutory_accounts.account_id
                            AND account_type.account_type_id = account.account_type_id
                            AND payroll.payroll_status = 1



                             UNION ALL




                             SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              '' AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
                               CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.nita AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'Savings' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 7
                            AND account.account_id = statutory_accounts.account_id
                            AND account_type.account_type_id = account.account_type_id
                            AND payroll.payroll_status = 1

                            UNION ALL 

                            SELECT
                              `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
                              `payroll_payment`.`payroll_payment_id` AS `referenceId`,
                              `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
                              `payroll_payment`.`reference_number` AS `referenceCode`,
                              `payroll_payment`.`document_number` AS `transactionCode`,
                              '' AS `patient_id`,
                              `statutory_accounts`.`statutory_account_id` AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `payroll_payment`.`account_from_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
                              CONCAT('Payment for invoice of ',' ',`payroll_summary`.`payroll_created_for`)  AS `transactionDescription`,
                                  0 AS `department_id`,
                              0 AS `dr_amount`,
                              `payroll_payment_item`.`amount_paid` AS `cr_amount`,
                              `payroll_payment`.`transaction_date` AS `transactionDate`,
                              `payroll_payment`.`created` AS `createdAt`,
                              `payroll_summary`.`payroll_created_for` AS `referenceDate`,
                              `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
                              'Expense Payment' AS `transactionCategory`,
                              'Creditors Invoices Payments' AS `transactionClassification`,
                              'payroll_payment' AS `transactionTable`,
                              'payroll_payment_item' AS `referenceTable`
                            FROM
                              (
                                (
                                  (
                                    `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts
                                    
                                  )
                                  JOIN account ON(
                                    (
                                      account.account_id = payroll_payment.account_from_id
                                    )
                                  )
                                )
                                JOIN `account_type` ON(
                                  (
                                    account_type.account_type_id = account.account_type_id
                                  )
                                )
                                
                              )
                              WHERE payroll_payment_item.invoice_type = 0 
                              AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
                              AND payroll_payment.payroll_payment_status = 1
                              AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id 
                              AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id


                             



                              UNION ALL 


                              SELECT
                                `payroll_invoice`.`statutory_invoice_id` AS `transactionId`,
                                `payroll_invoice`.`statutory_invoice_id` AS `referenceId`,
                                '' AS `payingFor`,
                                `payroll_invoice`.`invoice_number` AS `referenceCode`,
                                `payroll_invoice`.`document_number` AS `transactionCode`,
                                '' AS `patient_id`,
                                `payroll_invoice`.`statutory_id` AS `recepientId`,
                                `account`.`parent_account` AS `accountParentId`,
                                `account_type`.`account_type_name` AS `accountsclassfication`,
                                `payroll_invoice_item`.`account_to_id` AS `accountId`,
                                `account`.`account_name` AS `accountName`,
                                `payroll_invoice`.`statutory_invoice_id`  AS `transactionName`,
                                `payroll_invoice_item`.`item_description` AS `transactionDescription`,
                                
                                  '0' AS `department_id`,
                                SUM(`payroll_invoice_item`.`total_amount`) AS `dr_amount`,
                                '0' AS `cr_amount`,
                                `payroll_invoice`.`transaction_date` AS `transactionDate`,
                                `payroll_invoice`.`created` AS `createdAt`,
                                `payroll_invoice`.`transaction_date` AS `referenceDate`,
                                `payroll_invoice_item`.`statutory_invoice_item_status` AS `status`,
                                'Expense' AS `transactionCategory`,
                                'Payroll Invoices' AS `transactionClassification`,
                                'payroll_invoice_item' AS `transactionTable`,
                                'payroll_invoice' AS `referenceTable`
                              FROM
                                (
                                  (
                                    (
                                      `payroll_invoice_item`,payroll_invoice,statutory_accounts
                                      
                                    )
                                    JOIN account ON(
                                      (
                                        account.account_id = payroll_invoice_item.account_to_id
                                      )
                                    )
                                  )
                                  JOIN `account_type` ON(
                                    (
                                      account_type.account_type_id = account.account_type_id
                                    )
                                  )
                                )
                              WHERE 
                              payroll_invoice.statutory_invoice_id = payroll_invoice_item.statutory_invoice_id 
                              AND payroll_invoice.statutory_invoice_status = 1
                              AND statutory_accounts.statutory_account_id = payroll_invoice.statutory_id 

                              GROUP BY payroll_invoice_item.statutory_invoice_id

                            
                              UNION ALL


                              SELECT
                                `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
                                `payroll_payment`.`payroll_payment_id` AS `referenceId`,
                                `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
                                `payroll_payment`.`reference_number` AS `referenceCode`,
                                `payroll_payment`.`document_number` AS `transactionCode`,
                                '' AS `patient_id`,
                                `statutory_accounts`.`statutory_account_id` AS `recepientId`,
                                `account`.`parent_account` AS `accountParentId`,
                                `account_type`.`account_type_name` AS `accountsclassfication`,
                                `payroll_payment`.`account_from_id` AS `accountId`,
                                `account`.`account_name` AS `accountName`,
                                `payroll_invoice`.`statutory_invoice_id`  AS `transactionName`,
                                CONCAT('Payment for invoice of ',' ',`payroll_invoice`.`transaction_date`)  AS `transactionDescription`,
                                    0 AS `department_id`,
                                0 AS `dr_amount`,
                                SUM(`payroll_payment_item`.`amount_paid`) AS `cr_amount`,
                                `payroll_payment`.`transaction_date` AS `transactionDate`,
                                `payroll_payment`.`created` AS `createdAt`,
                                `payroll_invoice`.`transaction_date` AS `referenceDate`,
                                `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
                                'Payroll Payment' AS `transactionCategory`,
                                'Payroll Payment' AS `transactionClassification`,
                                'payroll_payment' AS `transactionTable`,
                                'payroll_payment_item' AS `referenceTable`
                              FROM
                                (
                                  (
                                    (
                                      `payroll_payment_item`,payroll_payment,statutory_accounts,payroll_invoice
                                      
                                    )
                                    JOIN account ON(
                                      (
                                        account.account_id = payroll_payment.account_from_id
                                      )
                                    )
                                  )
                                  JOIN `account_type` ON(
                                    (
                                      account_type.account_type_id = account.account_type_id
                                    )
                                  )
                                  
                                )
                                WHERE payroll_payment_item.invoice_type = 1
                                AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
                                AND payroll_payment.payroll_payment_status = 1
                                AND payroll_invoice.statutory_invoice_id = payroll_payment_item.payroll_invoice_id 
                                AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id
                                GROUP BY payroll_invoice.statutory_invoice_id
                         
                          ) AS data,statutory_accounts WHERE data.recepientId = ".$provider_id." AND data.recepientId = statutory_accounts.statutory_account_id  GROUP BY data.transactionName ORDER BY data.transactionDate ASC ";
                          $query = $this->db->query($select_statement);
                  return $query;


  }

  public function add_credit_note_item($provider_id,$provider_credit_note_id)
  {

    $amount = $this->input->post('amount');
		$account_to_id=$this->input->post('account_to_id');
    $description = $this->input->post('description');
		$tax_type_id=$this->input->post('tax_type_id');

    if($tax_type_id == 0)
    {
      $amount = $amount;
      $vat = 0;
    }
    else if($tax_type_id == 1)
    {

      $vat = $amount *0.16;
      $amount = $amount*1.16;
    }
    else if($tax_type_id == 2){

      $vat = $amount*0.05;
      $amount = $amount *1.05;
    }

    // var_dump($amount);die();


		$service = array(
							'provider_id' => $provider_id,
              'account_to_id' => $account_to_id,
              'description'=>$description,
							'created_by' => $this->session->userdata('personnel_id'),
							'created' => date('Y-m-d'),
              'credit_note_amount'=>$amount,
              'credit_note_charged_vat'=>$vat,
              'vat_type_id'=>$tax_type_id
						);

    if(!empty($provider_credit_note_id))
    {
      $service['provider_credit_note_id'] = $provider_credit_note_id;
      $service['provider_credit_note_item_status'] = 1;
    }
    else
    {
       $service['provider_credit_note_item_status'] = 0;
    }

		$this->db->insert('provider_credit_note_item',$service);
		return TRUE;

  }


  public function confirm_provider_credit_note($provider_id,$provider_credit_note_id)
  {
    $amount = $this->input->post('amount');
    $amount_charged = $this->input->post('amount_charged');
    $invoice_date = $this->input->post('credit_note_date');
    $provider_invoice_id = $this->input->post('invoice_id');
    $vat_charged = $this->input->post('vat_charged');
    $invoice_number = $this->input->post('credit_note_number');

    $date_check = explode('-', $invoice_date);
    $month = $date_check[1];
    $year = $date_check[0];


    $document_number = $this->create_credit_note_number();

    // var_dump($checked); die();

    $insertarray['transaction_date'] = $invoice_date;
    $insertarray['invoice_year'] = $year;
    $insertarray['invoice_month'] = $month;
    $insertarray['provider_id'] = $provider_id;
    $insertarray['provider_invoice_id'] = $provider_invoice_id;
    $insertarray['document_number'] = $document_number;
    $insertarray['invoice_number'] = strtoupper($invoice_number);
    $insertarray['total_amount'] = $amount_charged;
    $insertarray['vat_charged'] = $vat_charged;
 
    $insertarray['amount'] = $amount;
    $insertarray['account_from_id'] = 83;


     $total_visits = sizeof($_POST['provider_notes_items']);

     // var_dump($total_visits);die();

     if(!empty($provider_credit_note_id))
     {
        $this->db->where('provider_credit_note_id',$provider_credit_note_id);
        if($this->db->update('provider_credit_note', $insertarray))
        {


          $total_visits = sizeof($_POST['provider_notes_items']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['provider_notes_items'];
              $provider_credit_note_item_id = $visit[$r];
              //check if card is held
              $service = array(
                        'provider_credit_note_id'=>$provider_credit_note_id,
                        'created' =>$invoice_date,
                        'provider_credit_note_item_status'=>1,
                        'provider_invoice_id'=>$provider_invoice_id,
                        'year'=>$year,
                        'month'=>$month,
                      );
              $this->db->where('provider_credit_note_item_id',$provider_credit_note_item_id);
              $this->db->update('provider_credit_note_item',$service);
            }
          }
          return TRUE;
        }
     }
     else
     {

        $insertarray['created_by'] = $this->session->userdata('personnel_id');
        $insertarray['created'] = date('Y-m-d');
        if($this->db->insert('provider_credit_note', $insertarray))
        {
          $provider_credit_note_id = $this->db->insert_id();


          $total_visits = sizeof($_POST['provider_notes_items']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['provider_notes_items'];
              $provider_credit_note_item_id = $visit[$r];
              //check if card is held
              $service = array(
                        'provider_credit_note_id'=>$provider_credit_note_id,
                        'created' =>$invoice_date,
                        'provider_credit_note_item_status'=>1,
                        'provider_invoice_id'=>$provider_invoice_id,
                        'year'=>$year,
                        'month'=>$month,
                      );
              $this->db->where('provider_credit_note_item_id',$provider_credit_note_item_id);
              $this->db->update('provider_credit_note_item',$service);
            }
          }
          return TRUE;
        }
     }

  }

  public function create_credit_note_number()
	{
		//select product code
		$this->db->where('provider_invoice_id > 0');
		$this->db->from('provider_invoice');
		$this->db->select('MAX(document_number) AS number');
		$this->db->order_by('provider_invoice_id','DESC');
		// $this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			// var_dump($number);die();
			$number++;

		}
		else{
			$number = 1;
		}
		// var_dump($number);die();
		return $number;
	}


  public function get_provider_credit_notes($provider_id,$limit=null)
	{
		$this->db->where('provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id AND provider_credit_note.provider_credit_note_status = 1 AND provider_credit_note.provider_id = '.$provider_id);
		if($limit)
		{
			$this->db->limit($limit);
		}
		$this->db->group_by('provider_credit_note.provider_credit_note_id');
		$this->db->order_by('provider_credit_note.transaction_date','DESC');
		return $this->db->get('provider_credit_note_item,provider_credit_note');
	}


  public function get_provider_payments($provider_id,$limit=null)
  {
    $this->db->where('provider_payment.provider_payment_id = provider_payment_item.provider_payment_id AND provider_payment.provider_payment_status = 1 AND provider_payment.provider_id = '.$provider_id);
    if($limit)
    {
      $this->db->limit($limit);
    }
    $this->db->group_by('provider_payment.provider_payment_id');
    $this->db->join('account','account.account_id = provider_payment.account_from_id','left');
    $this->db->order_by('provider_payment.transaction_date','DESC');
    return $this->db->get('provider_payment_item,provider_payment');
  }

  public function add_payment_item($provider_id,$provider_payment_id)
  {

    $amount = $this->input->post('amount_paid');
    $provider_invoice_id = $this->input->post('invoice_id');

    // var_dump($provider_payment_id);die();

    // if(empty($provider_invoice_id))
    // {
    //   $invoice_type = 2;
    // }
    // else
    // {
      $exploded = explode('***', $provider_invoice_id);
      $invoice_id = $exploded[0];
      $invoice_number = $exploded[1];
      $invoice_type = $exploded[2];

    // }

    $service = array(
              'payroll_invoice_id'=>$invoice_id,
              'invoice_number'=>$invoice_number,
              'invoice_type'=>$invoice_type,
              'payroll_payment_item_status' => 0,
              'payroll_payment_id' => 0,
              'payroll_id' => $provider_id,
              'created_by' => $this->session->userdata('personnel_id'),
              'created' => date('Y-m-d'),
              'amount_paid'=>$amount,
            );
            // var_dump($service);die();

    if(!empty($provider_payment_id))
    {
      $service['payroll_payment_id'] = $provider_payment_id;
      $service['payroll_payment_item_status'] = 1;
    }

    $this->db->insert('payroll_payment_item',$service);
    return TRUE;

  }

  public function confirm_payroll_payment($provider_id,$provider_payment_id)
  {

    // var_dump($provider_payment_id);die();
    $amount_paid = $this->input->post('amount_paid');
    $payment_date = $this->input->post('payment_date');
    $reference_number = $this->input->post('reference_number');
    $account_from_id = $this->input->post('account_from_id');

    $date_check = explode('-', $payment_date);
    $month = $date_check[1];
    $year = $date_check[0];


    // var_dump($year);die();

    if(!empty($provider_payment_id))
    {
     

      // $document_number = $this->create_credit_payment_number();

      $insertarray['transaction_date'] = $payment_date;
      $insertarray['payment_year'] = $year;
      $insertarray['payment_month'] = $month;
      $insertarray['payroll_id'] = $provider_id;
      $insertarray['reference_number'] = strtoupper($reference_number);
      $insertarray['total_amount'] = $amount_paid;
      $insertarray['account_from_id'] = $account_from_id;
      $insertarray['created_by'] = $this->session->userdata('personnel_id');
       $this->db->where('payroll_payment_id',$provider_payment_id);

      if($this->db->update('payroll_payment', $insertarray))
      {


        $total_visits = sizeof($_POST['payroll_payments_items']);

        //check if any checkboxes have been ticked
        if($total_visits > 0)
        {
          for($r = 0; $r < $total_visits; $r++)
          {
            $visit = $_POST['payroll_payments_items'];
            $payroll_payment_item_id = $visit[$r];
            //check if card is held
            $service = array(
                      'payroll_payment_id'=>$provider_payment_id,
                      'created' =>$payment_date,
                      'payroll_payment_item_status'=>1,
                      'year'=>$year,
                      'month'=>$month,
                    );
            $this->db->where('payroll_payment_item_id',$provider_payment_item_id);
            $this->db->update('payroll_payment_item',$service);
          }
        }



          return TRUE;
      }

    }

    else
    {
      $document_number = $this->create_credit_payment_number();

      $insertarray['transaction_date'] = $payment_date;
      $insertarray['payment_year'] = $year;
      $insertarray['payment_month'] = $month;
      $insertarray['payroll_id'] = $provider_id;
      $insertarray['document_number'] = $document_number;
      $insertarray['reference_number'] = strtoupper($reference_number);
      $insertarray['total_amount'] = $amount_paid;
      $insertarray['account_from_id'] = $account_from_id;
      $insertarray['created_by'] = $this->session->userdata('personnel_id');
      $insertarray['created'] = date('Y-m-d');

      if($this->db->insert('payroll_payment', $insertarray))
      {
        $provider_payment_id = $this->db->insert_id();


        $total_visits = sizeof($_POST['payroll_payments_items']);

        //check if any checkboxes have been ticked
        if($total_visits > 0)
        {
          for($r = 0; $r < $total_visits; $r++)
          {
            $visit = $_POST['payroll_payments_items'];
            $payroll_payment_item_id = $visit[$r];
            //check if card is held
            $service = array(
                      'payroll_payment_id'=>$provider_payment_id,
                      'created' =>$payment_date,
                      'payroll_payment_item_status'=>1,
                      'year'=>$year,
                      'month'=>$month,
                    );
            $this->db->where('payroll_payment_item_id',$payroll_payment_item_id);
            $this->db->update('payroll_payment_item',$service);
          }
        }



        return TRUE;
      }
    }

    
  }

  public function create_credit_payment_number()
  {
    //select product code
    $this->db->where('provider_payment_id > 0');
    $this->db->from('provider_payment');
    $this->db->select('MAX(document_number) AS number');
    $this->db->order_by('provider_payment_id','DESC');
    // $this->db->limit(1);
    $query = $this->db->get();
    // var_dump($query); die();
    if($query->num_rows() > 0)
    {
      $result = $query->result();
      $number =  $result[0]->number;
      // var_dump($number);die();
      $number++;

    }
    else{
      $number = 1;
    }
    // var_dump($number);die();
    return $number;
  }




  /*
  * Add a new provider
  *
  */
  public function add_provider()
  {
    $provider_type_id = $this->input->post('provider_type_id');

    if(isset($provider_type_id))
    {
      $provider_type_id = 1;
    }
    else
    {
      $provider_type_id = 0;
    }
    $data = array(
      'provider_name'=>$this->input->post('provider_name'),
      'provider_email'=>$this->input->post('provider_email'),
      'provider_phone'=>$this->input->post('provider_phone'),
      'provider_location'=>$this->input->post('provider_location'),
      'provider_building'=>$this->input->post('provider_building'),
      'provider_floor'=>$this->input->post('provider_floor'),
      'provider_address'=>$this->input->post('provider_address'),
      'provider_post_code'=>$this->input->post('provider_post_code'),
      'provider_city'=>$this->input->post('provider_city'),
      'opening_balance'=>$this->input->post('opening_balance'),
      'start_date'=>$this->input->post('provider_account_date'),
      'provider_contact_person_name'=>$this->input->post('provider_contact_person_name'),
      'provider_contact_person_onames'=>$this->input->post('provider_contact_person_onames'),
      'provider_contact_person_phone1'=>$this->input->post('provider_contact_person_phone1'),
      'provider_contact_person_phone2'=>$this->input->post('provider_contact_person_phone2'),
      'provider_contact_person_email'=>$this->input->post('provider_contact_person_email'),
      'provider_description'=>$this->input->post('provider_description'),
      'branch_code'=>$this->session->userdata('branch_code'),
      'created_by'=>$this->session->userdata('provider_id'),
      'debit_id'=>$this->input->post('debit_id'),
      'modified_by'=>$this->session->userdata('provider_id'),
      'provider_type_id'=>$provider_type_id,
      'created'=>date('Y-m-d H:i:s')
    );

    if($this->db->insert('provider', $data))
    {
      return $this->db->insert_id();
    }
    else{
      return FALSE;
    }
  }

  /*
  * Update an existing provider
  * @param string $image_name
  * @param int $provider_id
  *
  */
  public function edit_provider($provider_id)
  {
    $data = array(
      'provider_name'=>$this->input->post('provider_name'),
      'provider_email'=>$this->input->post('provider_email'),
      'provider_phone'=>$this->input->post('provider_phone'),
      'provider_location'=>$this->input->post('provider_location'),
      'provider_building'=>$this->input->post('provider_building'),
      'provider_floor'=>$this->input->post('provider_floor'),
      'provider_address'=>$this->input->post('provider_address'),
      'provider_post_code'=>$this->input->post('provider_post_code'),
      'provider_city'=>$this->input->post('provider_city'),
      'opening_balance'=>$this->input->post('opening_balance'),
      'start_date'=>$this->input->post('provider_account_date'),
      'provider_contact_person_name'=>$this->input->post('provider_contact_person_name'),
      'provider_contact_person_onames'=>$this->input->post('provider_contact_person_onames'),
      'provider_contact_person_phone1'=>$this->input->post('provider_contact_person_phone1'),
      'provider_contact_person_phone2'=>$this->input->post('provider_contact_person_phone2'),
      'provider_contact_person_email'=>$this->input->post('provider_contact_person_email'),
      'provider_description'=>$this->input->post('provider_description'),
      'debit_id'=>$this->input->post('debit_id'),
      'modified_by'=>$this->session->userdata('provider_id'),
    );

    $this->db->where('provider_id', $provider_id);
    if($this->db->update('provider', $data))
    {
      return TRUE;
    }
    else{
      return FALSE;
    }
  }


  /*
  * get a single provider's details
  * @param int $provider_id
  *
  */
  public function get_payroll_account($provider_id)
  {
    //retrieve all users
    $this->db->from('v_payroll_ledger_aging');
    $this->db->select('SUM(dr_amount) AS total_invoice_amount');
    $this->db->where('transactionClassification = "Payroll Invoices" AND recepientId = '.$provider_id);
    $query = $this->db->get();
    $invoices = $query->row();

    $total_invoice_amount = $invoices->total_invoice_amount;


    $this->db->from('v_payroll_ledger_aging');
    $this->db->select('SUM(cr_amount) AS total_paid_amount');
    $this->db->where('transactionClassification = "Payroll Invoices Payments" AND recepientId = '.$provider_id);
    $query = $this->db->get();
    $payments = $query->row();

    $total_paid_amount = $payments->total_paid_amount;


    $response['total_invoice'] = $total_invoice_amount;
    $response['total_paid_amount'] = $total_paid_amount;
    $response['total_credit_note'] = 0;

    return $response;
  }



   /*
  * Retrieve all provider
  * @param string $table
  *   @param string $where
  *
  */
  public function get_all_payroll_details($table, $where, $per_page, $page, $order = 'provider_name', $order_method = 'ASC')
  {
    //retrieve all users
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    // $this->db->group_by('provider_invoice.provider_invoice_id');
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }


  public function get_payroll_invoice_details($payroll_invoice_id)
  {

      $this->db->from('payroll_invoice');
      $this->db->select('*');
      $this->db->where('statutory_invoice_id = '.$payroll_invoice_id);
      $query = $this->db->get();
      return $query;
  }

  public function get_payroll_payment_details($payroll_payment_id)
  {

      $this->db->from('payroll_payment');
      $this->db->select('*');
      $this->db->where('payroll_payment_id = '.$payroll_payment_id);
      $query = $this->db->get();
      return $query;
  }

  public function check_on_account($provider_payment_id)
  {

     $this->db->from('provider_payment_item');
      $this->db->select('*');
      $this->db->where('invoice_type = 3 AND provider_payment_id = '.$provider_payment_id);
      $query = $this->db->get();
      if($query->num_rows() > 0)
      {
          return TRUE;
      }
      else
      {
        return FALSE;
      }


  }

  public function get_provider_credit_note_details($provider_credit_note_id)
  {

      $this->db->from('provider_credit_note');
      $this->db->select('*');
      $this->db->where('provider_credit_note_id = '.$provider_credit_note_id);
      $query = $this->db->get();
      return $query;
  }
  
  public function get_content($table, $where,$select,$group_by=NULL,$limit=NULL)
  {
    $this->db->from($table);
    $this->db->select($select);
    $this->db->where($where);
    if($group_by != NULL)
    {
      $this->db->group_by($group_by);
    }
    $query = $this->db->get('');
    
    return $query;
  }

   

  public function get_statutory($statutory_id)
  {
    //retrieve all users
    $this->db->from('statutory_accounts');
    $this->db->select('*');
    $this->db->where('statutory_account_id = '.$statutory_id);
    $query = $this->db->get();

    return $query;
  }
  public function get_account_name($from_account_id)
  {
    $account_name = '';
    $this->db->select('account_name');
    $this->db->where('account_id = '.$from_account_id);
    $query = $this->db->get('account');

    $account_details = $query->row();
    $account_name = '';
    if($query->num_rows() > 0)
    {
     $account_name = $account_details->account_name;
   }
   

   return $account_name;
 }

 public function get_all_accounts_by_type($account_type_name = NULL)
    {
        
        $this->db->from('account,account_type');
        $this->db->select('*');
        $this->db->where('parent_account <> 0 AND account.account_type_id = account_type.account_type_id AND account_type.account_type_name = "'.$account_type_name.'"');
        $this->db->order_by('parent_account','ASC');
        $query = $this->db->get();

         return $query;

    }
      public function get_payroll_aging_report()
  {
    //retrieve all users
    $statutory_search =  $this->session->userdata('statutory_search');
    $date_statutories_batches_from = $this->session->userdata('date_statutories_batches');
    $date_statutories_batches_to = $date_searched = $this->session->userdata('date_statutories_batches_to');
    $report_type_list =  $this->session->set_userdata('report_statutory_type');
    $report_detail = $this->session->userdata('report_detail');

    if(empty($date_searched))
    {
      $date_searched = date('Y-m-d');
    }

    if(!empty($statutory_search))
    {
      $where = $statutory_search;
    }
    else
    {
      $where = 'recepientId > 0';
    }

   
    $report_type = $this->session->userdata('report_type');
    $date_add = '';
    if($report_type)
    {
      $date_add = " AND data.transactionDate <= '".date('Y-m-d')."' ";
    }
    else
    {


      if(!empty($date_searched) AND !empty($date_searched_from))
      {
        $date_add = " AND data.transactionDate >= '".$date_searched_from."' AND data.transactionDate <= '".$date_searched."' ";
      }
      else if(!empty($date_searched) AND empty($date_searched_from))
      {
        $date_add = " AND data.transactionDate <= '".$date_searched."' ";
      }
      else if(empty($date_searched) AND !empty($date_searched_from))
      {
        
        $date_add = " AND data.transactionDate <= '".$date_searched_from."' ";
      }

    }

    $add = '';  


    $sql_payable = "CREATE OR REPLACE VIEW v_payroll_ledger_aging AS
                      SELECT 
                      *
                      FROM
                        (
                          
                            


                            SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              CONCAT(`account`.`account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for)) AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              CONCAT('Payroll') AS `transactionName`,
                              CONCAT('Payroll') AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.total_payroll AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `invoiceDate`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'Payroll' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 1
                            AND account.account_id = statutory_accounts.account_id
                            AND payroll.payroll_status = 1
                            AND account_type.account_type_id = account.account_type_id


                            UNION ALL 


                            SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              CONCAT(`account`.`account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for)) AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              CONCAT('PAYE') AS `transactionName`,
                              CONCAT('PAYE') AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.paye AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `invoiceDate`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'PAYE' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 2
                            AND account.account_id = statutory_accounts.account_id
                            AND payroll.payroll_status = 1
                            AND account_type.account_type_id = account.account_type_id




                            UNION ALL 


                            SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              CONCAT(`account`.`account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for)) AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              CONCAT('NSSF') AS `transactionName`,
                              CONCAT('NSSF') AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.nssf AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `invoiceDate`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'PAYE' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 3
                            AND account.account_id = statutory_accounts.account_id
                            AND payroll.payroll_status = 1
                            AND account_type.account_type_id = account.account_type_id


                            UNION ALL




                            SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              CONCAT(`account`.`account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for)) AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              CONCAT('NHIF') AS `transactionName`,
                              CONCAT('NHIF') AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.nhif AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `invoiceDate`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'NHIF' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 4
                            AND account.account_id = statutory_accounts.account_id
                            AND payroll.payroll_status = 1
                            AND account_type.account_type_id = account.account_type_id

                            UNION ALL




                            SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              '' AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              CONCAT('Sacco Contributions') AS `transactionName`,
                              CONCAT('Sacco Contributions') AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.savings AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `invoiceDate`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'Sacco Contributions' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 5
                            AND account.account_id = statutory_accounts.account_id
                            AND account_type.account_type_id = account.account_type_id
                            AND payroll.payroll_status = 1

                            UNION ALL




                            SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              '' AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              statutory_accounts.statutory_account_name AS `transactionName`,
                              statutory_accounts.statutory_account_name AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.housing_levy AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `invoiceDate`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'Sacco Contributions' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 6
                            AND account.account_id = statutory_accounts.account_id
                            AND account_type.account_type_id = account.account_type_id
                            AND payroll.payroll_status = 1

                            UNION ALL
                            SELECT
                              payroll_summary.payroll_summary_id AS `transactionId`,
                              payroll_summary.payroll_id AS `referenceId`,
                              '' AS `payingFor`,
                              '' AS `referenceCode`,
                              '' AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `account`.`account_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              statutory_accounts.statutory_account_name AS `transactionName`,
                              statutory_accounts.statutory_account_name AS `transactionDescription`,
                                0 AS `department_id`,
                              payroll_summary.nita AS `dr_amount`,
                              0 AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `invoiceDate`,
                              payroll_summary.payroll_created_for AS `transactionDate`,
                              payroll_summary.payroll_created_for  AS `createdAt`,
                              payroll_summary.payroll_created_for  AS `referenceDate`,
                              `payroll`.`payroll_status` AS `status`,
                              'Sacco Contributions' AS `transactionCategory`,
                              'Payroll Expense' AS `transactionClassification`,
                              'finance_purchase' AS `transactionTable`,
                              '' AS `referenceTable`
                            FROM
                            payroll_summary,payroll,statutory_accounts,account,account_type
                            WHERE payroll.payroll_id = payroll_summary.payroll_id
                            AND statutory_accounts.statutory_account_id = 7
                            AND account.account_id = statutory_accounts.account_id
                            AND account_type.account_type_id = account.account_type_id
                            AND payroll.payroll_status = 1

                            UNION ALL

                            SELECT
                              `payroll_invoice`.`statutory_invoice_id` AS `transactionId`,
                              `payroll_invoice_item`.`statutory_invoice_item_id`  AS `referenceId`,
                              '' AS `payingFor`,
                              `payroll_invoice`.`invoice_number` AS `referenceCode`,
                              `payroll_invoice`.`document_number` AS `transactionCode`,
                              '' AS `patient_id`,
                              `statutory_accounts`.`statutory_account_id` AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `payroll_invoice_item`.`account_to_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                               'Payroll Invoice' AS `transactionName`,
                              `payroll_invoice_item`.`item_description` AS `transactionDescription`,
                              
                                '0' AS `department_id`,
                              SUM(`payroll_invoice_item`.`total_amount`) AS `dr_amount`,
                              '0' AS `cr_amount`,
                              payroll_invoice.transaction_date AS `invoiceDate`,
                              `payroll_invoice`.`transaction_date` AS `transactionDate`,
                              `payroll_invoice`.`created` AS `createdAt`,
                              `payroll_invoice`.`transaction_date` AS `referenceDate`,
                              `payroll_invoice_item`.`statutory_invoice_item_status` AS `status`,
                              'Expense' AS `transactionCategory`,
                              'Payroll Invoices' AS `transactionClassification`,
                              'payroll_invoice_item' AS `transactionTable`,
                              'payroll_invoice' AS `referenceTable`
                            FROM
                              `payroll_invoice_item`,payroll_invoice,statutory_accounts,account,account_type
                            WHERE 
                            payroll_invoice.statutory_invoice_id = payroll_invoice_item.statutory_invoice_id 
                            AND payroll_invoice.statutory_invoice_status = 1
                            AND account_type.account_type_id = account.account_type_id
                            AND account.account_id = payroll_invoice_item.account_to_id
                            AND statutory_accounts.statutory_account_id = payroll_invoice.statutory_id 
                            GROUP BY `payroll_invoice_item`.`statutory_invoice_id` 

                            UNION ALL



                            SELECT
                              `payroll_credit_note_item`.`statutory_credit_note_item_id` AS `transactionId`,
                              `payroll_credit_note`.`statutory_credit_note_id` AS `referenceId`,
                              `payroll_credit_note_item`.`statutory_invoice_id` AS `payingFor`,
                              `payroll_credit_note`.`invoice_number` AS `referenceCode`,
                              `payroll_credit_note`.`document_number` AS `transactionCode`,
                              '' AS `patient_id`,
                              `statutory_accounts`.`statutory_account_id` AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `payroll_credit_note`.`account_from_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              'Credit Note' AS `transactionName`,
                              CONCAT('Payroll Invoice Credit Note',' ',payroll_invoice.invoice_number) AS `transactionDescription`,
                              0 AS `department_id`,
                              0 AS `dr_amount`,
                              `payroll_credit_note_item`.`credit_note_amount` AS `cr_amount`,
                              payroll_invoice.transaction_date AS `invoiceDate`,
                              `payroll_credit_note`.`transaction_date` AS `transactionDate`,
                              `payroll_credit_note`.`created` AS `createdAt`,
                              `payroll_invoice`.`transaction_date` AS `referenceDate`,
                              `payroll_credit_note_item`.`statutory_credit_note_item_status` AS `status`,
                              'Expense Payment' AS `transactionCategory`,
                              'Statutory Credit Notes' AS `transactionClassification`,
                              'payroll_credit_note' AS `transactionTable`,
                              'payroll_credit_note_item' AS `referenceTable`
                            FROM
                              `payroll_credit_note_item`,payroll_credit_note,payroll_invoice,statutory_accounts,account,account_type   
                                  
                            WHERE 
                              payroll_credit_note.statutory_credit_note_id = payroll_credit_note_item.statutory_credit_note_id 
                              AND payroll_credit_note.statutory_credit_note_status = 1
                              AND payroll_invoice.statutory_invoice_id = payroll_credit_note_item.statutory_invoice_id
                              AND payroll_invoice.statutory_invoice_status = 1
                              AND account.account_id = statutory_accounts.account_id
                              AND statutory_accounts.statutory_account_id = payroll_credit_note.statutory_id 
                              AND payroll_credit_note.credit_note_type_id = 1
                              AND account_type.account_type_id = account.account_type_id


                              UNION ALL


                              SELECT
                              `payroll_credit_note_item`.`statutory_credit_note_item_id` AS `transactionId`,
                              `payroll_credit_note`.`statutory_credit_note_id` AS `referenceId`,
                              `payroll_credit_note_item`.`statutory_invoice_id` AS `payingFor`,
                              `payroll_credit_note`.`invoice_number` AS `referenceCode`,
                              `payroll_credit_note`.`document_number` AS `transactionCode`,
                              '' AS `patient_id`,
                              `statutory_accounts`.`statutory_account_id` AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              `payroll_credit_note`.`account_from_id` AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              'Credit Note' AS `transactionName`,
                              CONCAT('Credit Note',statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionDescription`,
                              0 AS `department_id`,
                              0 AS `dr_amount`,
                              `payroll_credit_note_item`.`credit_note_amount` AS `cr_amount`,
                              payroll_summary.payroll_created_for AS `invoiceDate`,
                              `payroll_credit_note`.`transaction_date` AS `transactionDate`,
                              `payroll_credit_note`.`created` AS `createdAt`,
                              `payroll_summary`.`payroll_created_for` AS `referenceDate`,
                              `payroll_credit_note_item`.`statutory_credit_note_item_status` AS `status`,
                              'Expense Payment' AS `transactionCategory`,
                              'Statutory Credit Notes' AS `transactionClassification`,
                              'payroll_credit_note' AS `transactionTable`,
                              'payroll_credit_note_item' AS `referenceTable`
                            FROM
                              `payroll_credit_note_item`,payroll_credit_note,payroll_summary,statutory_accounts,account,account_type,payroll     
                                  
                            WHERE 
                              payroll_credit_note.statutory_credit_note_id = payroll_credit_note_item.statutory_credit_note_id 
                              AND payroll_credit_note.statutory_credit_note_status = 1
                              AND account.account_id = statutory_accounts.account_id
                              AND account_type.account_type_id = account.account_type_id
                              AND payroll_summary.payroll_summary_id = payroll_credit_note_item.statutory_invoice_id
                              AND statutory_accounts.statutory_account_id = payroll_credit_note.statutory_id 
                              AND payroll_credit_note.credit_note_type_id = 0
                              AND payroll.payroll_id = payroll_summary.payroll_id
                              AND payroll.payroll_status = 1



                            UNION ALL
                                -- payroll invoice payments

                            SELECT
                                  `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
                                  `payroll_payment`.`payroll_payment_id` AS `referenceId`,
                                  `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
                                  CONCAT(`statutory_accounts`.`statutory_account_name`,'-',MONTH(payroll_summary.payroll_created_for),'-',YEAR(payroll_summary.payroll_created_for)) AS `referenceCode`,
                                  `payroll_payment`.`document_number` AS `transactionCode`,
                                  '' AS `patient_id`,
                                  `statutory_accounts`.`statutory_account_id` AS `recepientId`,
                                  `account`.`parent_account` AS `accountParentId`,
                                  `account_type`.`account_type_name` AS `accountsclassfication`,
                                  `payroll_payment`.`account_from_id` AS `accountId`,
                                  `account`.`account_name` AS `accountName`,
                                  `payroll_payment_item`.`description` AS `transactionName`,
                                  CONCAT('Payment for invoice of ',' ',`payroll_summary`.`payroll_created_for`)  AS `transactionDescription`,
                                      0 AS `department_id`,
                                  0 AS `dr_amount`,
                                  `payroll_payment_item`.`amount_paid` AS `cr_amount`,
                                  payroll_summary.payroll_created_for AS `invoiceDate`,
                                  `payroll_payment`.`transaction_date` AS `transactionDate`,
                                  `payroll_payment`.`created` AS `createdAt`,
                                  `payroll_summary`.`payroll_created_for` AS `referenceDate`,
                                  `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
                                  'Payroll Payment' AS `transactionCategory`,
                                  'Payroll Payment' AS `transactionClassification`,
                                  'payroll_payment' AS `transactionTable`,
                                  'payroll_payment_item' AS `referenceTable`
                                FROM
                                   `payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts,payroll,account,account_type
                                WHERE payroll_payment_item.invoice_type = 0 
                                  AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
                                  AND payroll.payroll_id = payroll_summary.payroll_id
                                  AND payroll.payroll_status = 1
                                  AND payroll_payment.payroll_payment_status = 1
                                  AND statutory_accounts.statutory_account_id <= 7
                                  AND account.account_id = payroll_payment.account_from_id
                                  AND account_type.account_type_id = account.account_type_id
                                  AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id
                                  AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id


                            

                            UNION ALL


                            SELECT
                                  `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
                                  `payroll_payment`.`payroll_payment_id` AS `referenceId`,
                                  `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
                                  `payroll_payment`.`reference_number` AS `referenceCode`,
                                  `payroll_payment`.`document_number` AS `transactionCode`,
                                  '' AS `patient_id`,
                                  `statutory_accounts`.`statutory_account_id` AS `recepientId`,
                                  `account`.`parent_account` AS `accountParentId`,
                                  `account_type`.`account_type_name` AS `accountsclassfication`,
                                  `payroll_payment`.`account_from_id` AS `accountId`,
                                  `account`.`account_name` AS `accountName`,
                                  `payroll_payment_item`.`description` AS `transactionName`,
                                  CONCAT('Payment for invoice of ',' ',`payroll_invoice`.`transaction_date`)  AS `transactionDescription`,
                                      0 AS `department_id`,
                                  0 AS `dr_amount`,
                                  `payroll_payment_item`.`amount_paid` AS `cr_amount`,
                                  payroll_invoice.transaction_date AS `invoiceDate`,
                                  `payroll_payment`.`transaction_date` AS `transactionDate`,
                                  `payroll_payment`.`created` AS `createdAt`,
                                  `payroll_invoice`.`transaction_date` AS `referenceDate`,
                                  `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
                                  'Payroll Payment' AS `transactionCategory`,
                                  'Payroll Payment' AS `transactionClassification`,
                                  'payroll_payment' AS `transactionTable`,
                                  'payroll_payment_item' AS `referenceTable`
                                FROM
                                `payroll_payment_item`,payroll_payment,statutory_accounts,payroll_invoice,account,account_type
                                 
                                WHERE payroll_payment_item.invoice_type = 1
                                  AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
                                  AND payroll_payment.payroll_payment_status = 1
                                  AND account.account_id = payroll_payment.account_from_id
                                  AND statutory_accounts.statutory_account_id <= 7
                                  AND  account_type.account_type_id = account.account_type_id
                                  AND payroll_invoice.statutory_invoice_id = payroll_payment_item.payroll_invoice_id 
                                  AND statutory_accounts.statutory_account_id = payroll_invoice.statutory_id



                             UNION ALL


                            SELECT
                                  `payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
                                  `payroll_payment`.`payroll_payment_id` AS `referenceId`,
                                  `payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
                                  `payroll_payment`.`reference_number` AS `referenceCode`,
                                  `payroll_payment`.`document_number` AS `transactionCode`,
                                  '' AS `patient_id`,
                                  `statutory_accounts`.`statutory_account_id` AS `recepientId`,
                                  `account`.`parent_account` AS `accountParentId`,
                                  `account_type`.`account_type_name` AS `accountsclassfication`,
                                  `payroll_payment`.`account_from_id` AS `accountId`,
                                  `account`.`account_name` AS `accountName`,
                                  `payroll_payment_item`.`description` AS `transactionName`,
                                  CONCAT('Payment for invoice of ',' ',`statutory_accounts`.`statutory_account_name`)  AS `transactionDescription`,
                                      0 AS `department_id`,
                                  0 AS `dr_amount`,
                                  `payroll_payment_item`.`amount_paid` AS `cr_amount`,
                                  `payroll_payment`.`transaction_date`  AS `invoiceDate`,
                                  `payroll_payment`.`transaction_date` AS `transactionDate`,
                                  `payroll_payment`.`created` AS `createdAt`,
                                  `payroll_payment`.`transaction_date`  AS `referenceDate`,
                                  `payroll_payment_item`.`payroll_payment_item_status` AS `status`,
                                  'Payroll Payment' AS `transactionCategory`,
                                  'Payroll Payment' AS `transactionClassification`,
                                  'payroll_payment' AS `transactionTable`,
                                  'payroll_payment_item' AS `referenceTable`
                                FROM
                                `payroll_payment_item`,payroll_payment,statutory_accounts,account,account_type
                                 
                                WHERE payroll_payment_item.invoice_type = 3
                                  AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id 
                                  AND payroll_payment.payroll_payment_status = 1
                                  AND account.account_id = payroll_payment.account_from_id
                                  AND statutory_accounts.statutory_account_id <= 7
                                  AND  account_type.account_type_id = account.account_type_id
                                  AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id


                            UNION ALL

                            SELECT
                              `journal_entry`.`journal_entry_id` AS `transactionId`,
                              '' AS `referenceId`,
                              '' AS `payingFor`,
                              '' AS `referenceCode`,
                              journal_entry.document_number AS `transactionCode`,
                              '' AS `patient_id`,
                              statutory_accounts.statutory_account_id AS `recepientId`,
                              `account`.`parent_account` AS `accountParentId`,
                              `account_type`.`account_type_name` AS `accountsclassfication`,
                              journal_entry.account_to_id AS `accountId`,
                              `account`.`account_name` AS `accountName`,
                              CONCAT(journal_entry.journal_entry_description)  AS `transactionName`,
                              CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
                                0 AS `department_id`,
                              0 AS `dr_amount`,
                               `journal_entry`.`amount_paid` AS `cr_amount`,
                              `journal_entry`.`payment_date` AS `invoiceDate`,
                              `journal_entry`.`payment_date` AS `transactionDate`,
                              `journal_entry`.`payment_date` AS `createdAt`,
                               `journal_entry`.`payment_date` AS `referenceDate`,
                              `journal_entry`.`journal_entry_status` AS `status`,
                              'Payroll Payment' AS `transactionCategory`,
                              'Payroll Payments' AS `transactionClassification`,
                              'payroll_payment' AS `transactionTable`,
                              'payroll_payment_item' AS `referenceTable`


                            FROM
                              journal_entry,account,account_type,statutory_accounts

                            WHERE journal_entry.journal_entry_deleted = 0 
                            AND account_type.account_type_id = account.account_type_id
                            AND  account.account_id = journal_entry.account_from_id
                            AND statutory_accounts.account_id = journal_entry.account_from_id

                                
                            )AS  data WHERE data.transactionId > 0 ".$date_add;


                      $this->db->query($sql_payable);
                      // var_dump("hre");die();
                      $sql_other = "CREATE OR REPLACE VIEW v_payroll_ledger_aging_by_date AS SELECT * FROM v_payroll_ledger_aging ORDER BY transactionDate ASC;";
                      $this->db->query($sql_other);
                         // var_dump("hre");die();

      // $add = ' AND v_payroll_ledger_aging.recepientId = 39';
    // var_dump($report_detail);die();
    if($report_detail == 180)
    {

      $sql = "
        
          SELECT 
           *
          FROM 
          (
            SELECT
              v_payroll_ledger_aging.recepientId AS recepientId,
              '' as receivable,
              2 as branch_id,
              
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) = 0 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `coming_due`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 1 AND 30 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `thirty_days`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 31 AND 60 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `sixty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 61 AND 90 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `ninety_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 91 AND 120 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_twenty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 121 AND 150 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_fifty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 151 AND 180 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_eighty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) > 180 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `over_one_eighty_days`     
              FROM
              `v_payroll_ledger_aging`
              WHERE `v_payroll_ledger_aging`.`recepientId` > 0 ".$add."
              GROUP BY  `v_payroll_ledger_aging`.`recepientId`
             ) AS data ORDER BY data.`receivable` ASC
           ";

    }
    else if($report_detail == 360)
    {
      $sql = "
        
          SELECT 
           *
          FROM 
          (
            SELECT
              v_payroll_ledger_aging.recepientId AS recepientId,
              '' as receivable,
              2 as branch_id,
              
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) = 0 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `coming_due`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 1 AND 30 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `thirty_days`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 31 AND 60 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `sixty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 61 AND 90 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `ninety_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 91 AND 120 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_twenty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 121 AND 150 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_fifty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 151 AND 180 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_eighty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 181 AND 210 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `two_ten_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 211 AND 240 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `two_fourty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 241 AND 270 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `two_seventy_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 271 AND 300 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_hundred_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 301 AND 330 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_thirty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 331 AND 360 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_sixty_days`,
              
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) > 360 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `over_three_sixty_days`     
              FROM
              `v_payroll_ledger_aging`
              WHERE `v_payroll_ledger_aging`.`recepientId` > 0 ".$add."
              GROUP BY  `v_payroll_ledger_aging`.`recepientId`
             ) AS data ORDER BY data.`receivable` ASC";
    }
    else if($report_detail == 720)
    {
      $sql = "
        
          SELECT 
           *
          FROM 
          (
            SELECT
              v_payroll_ledger_aging.recepientId AS recepientId,
              '' as receivable,
              2 as branch_id,
              
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) = 0 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `coming_due`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 1 AND 30 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `thirty_days`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 31 AND 60 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `sixty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 61 AND 90 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `ninety_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 91 AND 120 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_twenty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 121 AND 150 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_fifty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 151 AND 180 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_eighty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 181 AND 210 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `two_ten_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 211 AND 240 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `two_fourty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 241 AND 270 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `two_seventy_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 271 AND 300 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_hundred_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 301 AND 330 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_thirty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 331 AND 360 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_sixty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 361 AND 390 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `three_ninety_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 391 AND 420 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `four_twenty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 421 AND 450 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `four_fifty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 451 AND 480 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `four_eighty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 481 AND 510 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `five_ten_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 510 AND 540 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `five_fourty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 541 AND 570 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `five_seventy_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 571 AND 600 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `six_hundred_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 601 AND 630 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `six_thirty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 631 AND 660 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `six_sixty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 661 AND 690 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `six_ninety_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 691 AND 720 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `seven_twenty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) > 720 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `over_seven_twenty_days`     
              FROM
              `v_payroll_ledger_aging`
              WHERE `v_payroll_ledger_aging`.`recepientId` > 0 ".$add."
              GROUP BY  `v_payroll_ledger_aging`.`recepientId`
             ) AS data ORDER BY data.`receivable` ASC
           ";
    }
    else
    {

      $sql = "
        
          SELECT 
           *
          FROM 
          (
            SELECT
              v_payroll_ledger_aging.recepientId AS recepientId,
              '' as receivable,
              2 as branch_id,
              
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) = 0 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `coming_due`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 1 AND 30 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `thirty_days`,
                (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 31 AND 60 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `sixty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 61 AND 90 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `ninety_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 91 AND 120 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_twenty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 121 AND 150 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_fifty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) BETWEEN 151 AND 180 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `one_eighty_days`,
              (
                sum(
                  IF(
                     ( DATEDIFF('$date_searched', v_payroll_ledger_aging.transactionDate) > 180 ),(v_payroll_ledger_aging.dr_amount - v_payroll_ledger_aging.cr_amount),0
                    )
                  )
             
                
              ) AS `over_one_eighty_days`     
              FROM
              `v_payroll_ledger_aging`
              WHERE `v_payroll_ledger_aging`.`recepientId` > 0 ".$add."
              GROUP BY  `v_payroll_ledger_aging`.`recepientId`
             ) AS data ORDER BY data.`receivable` ASC
           ";

    }
   
    

    $query = $this->db->query($sql);



    // $this->db->from('v_aged_payables');
    // $this->db->select('*');
    // $this->db->where($where);
    // $this->db->order_by('payables','ASC');
    // $query = $this->db->get();

    return $query;
  }

  public function get_all_statutory_accounts()
  {
    $this->db->from('statutory_accounts');
    $this->db->select('*');
    $this->db->where('statutory_account_id > 0');
    $this->db->order_by('statutory_account_name','ASC');
    $query = $this->db->get();

    return $query;
  }

     public function export_payroll()
  {
    $this->load->library('excel');
    

    
    $report_detail = $this->session->userdata('report_detail');

    // $this->creditors_model->calculate_creditors_aging_report();
    $creditor_query = $this->payroll_model->get_payroll_aging_report();
    $creditor_array = array();
    if($creditor_query->num_rows() > 0)
    {
      foreach ($creditor_query->result() as $key) {
        // code...
        $creditor_array[$key->recepientId] = $key;
      }
    }

    $income_rs = $this->get_all_statutory_accounts();
    
    
    $title = 'Statutory Account List';



    $income_result = '';


    $total_this_month = 0;
    $total_three_months = 0;
    $total_six_months = 0;
    $total_nine_months = 0;
    $total_payments = 0;
    $total_invoices =0;
    $total_balance = 0;
    $total_one_twenty_days =0;
    $total_one_fifty_days =0;
    $total_one_eighty_days =0;

    $total_two_ten_days =0;
    $total_two_fourty_days =0;
    $total_two_seventy_days =0;
    $total_three_hundred_days =0;
    $total_three_thirty_days =0;
    $total_three_sixty_days =0;
    $total_three_ninety_days =0;
    $total_four_twenty_days =0;
    $total_four_fifty_days =0;
    $total_four_eighty_days =0;
    $total_five_ten_days =0;
    $total_five_fourty_days =0;
    $total_five_seventy_days =0;
    $total_six_hundred_days =0;
    $total_six_thirty_days =0;
    $total_six_sixty_days =0;
    $total_six_ninety_days =0;
    $total_seven_twenty_days =0;
    $total_over_seven_twenty_days =0;
    $total_over_three_sixty_days =0;



    $total_one_eighty_days =0;
    $total_over_three_sixty_days =0;
    $total_over_one_eighty_days = 0;
    $total_unallocated  =0;



    $total_income = 0;
    $total_income = 0;
    $total_thirty = 0;
    $total_sixty = 0;
    $total_ninety = 0;
    $total_over_ninety = 0;
    $total_coming_due = 0;
    $grand_total = 0;
    $total_unallocated = 0;
    $grand_dr =0;
    $grand_cr = 0;
    
    if($income_rs->num_rows() > 0)
    {
      $count = 0;
      /*
        -----------------------------------------------------------------------------------------
        Document Header
        -----------------------------------------------------------------------------------------
      */

       
      $row_count = 0;

      if($report_detail == 180)
      {
        $report[$row_count][0] = 'Num';
        $report[$row_count][1] = 'Debtor name';
        $report[$row_count][2] = 'Current';
        $report[$row_count][3] = '30 Days';
        $report[$row_count][4] = '60 Days';
        $report[$row_count][5] = '90 Days';
        $report[$row_count][6] = '120 Days';
        $report[$row_count][7] = '150 Days';
        $report[$row_count][8] = '180 Days';
        $report[$row_count][9] = 'Over 180 Days';
        $report[$row_count][10] = 'Unallocated Funds';
        $report[$row_count][11] = 'Balance';
      }
      else if($report_detail == 360)
      {
        $report[$row_count][0] = 'Num';
        $report[$row_count][1] = 'Debtor name';
        $report[$row_count][2] = 'Current';
        $report[$row_count][3] = '30 Days';
        $report[$row_count][4] = '60 Days';
        $report[$row_count][5] = '90 Days';
        $report[$row_count][6] = '120 Days';
        $report[$row_count][7] = '150 Days';
        $report[$row_count][8] = '180 Days';
        $report[$row_count][9] = '210 Days';
        $report[$row_count][10] = '240 Days';
        $report[$row_count][11] = '270 Days';
        $report[$row_count][12] = '330 Days';
        $report[$row_count][13] = '360 Days';
        $report[$row_count][14] = 'Over 360 Days';
        $report[$row_count][15] = 'Unallocated Funds';
        $report[$row_count][16] = 'Balance';

      }
      else if($report_detail == 720)
      {


        $report[$row_count][0] = 'Num';
        $report[$row_count][1] = 'Debtor name';
        $report[$row_count][2] = 'Current';
        $report[$row_count][3] = '30 Days';
        $report[$row_count][4] = '60 Days';
        $report[$row_count][5] = '90 Days';
        $report[$row_count][6] = '120 Days';
        $report[$row_count][7] = '150 Days';
        $report[$row_count][8] = '180 Days';
        $report[$row_count][9] = '210 Days';
        $report[$row_count][10] = '240 Days';
        $report[$row_count][11] = '270 Days';
        $report[$row_count][12] = '300 Days';
        $report[$row_count][13] = '330 Days';
        $report[$row_count][14] = '360 Days';
        $report[$row_count][15] = '390 Days';
        $report[$row_count][16] = '420 Days';
        $report[$row_count][17] = '450 Days';
        $report[$row_count][18] = '480 Days';
        $report[$row_count][19] = '510 Days';
        $report[$row_count][20] = '540 Days';
        $report[$row_count][21] = '570 Days';
        $report[$row_count][22] = '600 Days';
        $report[$row_count][23] = '630 Days';
        $report[$row_count][24] = '660 Days';
        $report[$row_count][25] = '690 Days';
        $report[$row_count][26] = '720 Days';
        $report[$row_count][27] = 'Over 720 Days';
        $report[$row_count][28] = 'Unallocated Funds';
        $report[$row_count][29] = 'Balance';


      }
      else
      {
         $report[$row_count][0] = 'Num';
        $report[$row_count][1] = 'Debtor name';
        $report[$row_count][2] = 'Current';
        $report[$row_count][3] = '30 Days';
        $report[$row_count][4] = '60 Days';
        $report[$row_count][5] = '90 Days';
        $report[$row_count][6] = '120 Days';
        $report[$row_count][7] = '150 Days';
        $report[$row_count][8] = '180 Days';
        $report[$row_count][9] = 'Over 180 Days';
        $report[$row_count][10] = 'Unallocated Funds';
        $report[$row_count][11] = 'Balance';
      }
     
      //get & display all services
      
      //display all patient data in the leftmost columns
      foreach($income_rs->result() as $key => $value_two)
      {
        $row_count++;
        $total_invoiced = 0;
        # code...
        // $total_amount = $value->total_amount;
        $statutory_account_id = $value_two->statutory_account_id;
        $statutory_account_name = $value_two->statutory_account_name;

        $opening_balance =0;// $value_two->opening_balance;
        // $debit_id = $value_two->debit_id;

        // $opening_date = $value_two->created;

        
          $opening_balance = $opening_balance;  
       

        $unallocated = 0;//$this->company_financial_model->get_unallocated_funds($statutory_account_id);

        if(empty($unallocated))
          $unallocated = 0;
        $total_unallocated += $unallocated;


        $count++;



        if(array_key_exists($statutory_account_id, $creditor_array))
              {
                $this_month = $creditor_array[$statutory_account_id]->coming_due;
                $three_months = $creditor_array[$statutory_account_id]->thirty_days;
                $six_months = $creditor_array[$statutory_account_id]->sixty_days;
                $nine_months = $creditor_array[$statutory_account_id]->ninety_days;
                $one_twenty_days = $creditor_array[$statutory_account_id]->one_twenty_days;
                $one_fifty_days = $creditor_array[$statutory_account_id]->one_fifty_days;
                $one_eighty_days = $creditor_array[$statutory_account_id]->one_eighty_days;
                $over_one_eighty_days = $creditor_array[$statutory_account_id]->over_one_eighty_days;

                if($report_detail == 360)
                {
                  $two_ten_days = $creditor_array[$statutory_account_id]->two_ten_days;
                  $two_fourty_days = $creditor_array[$statutory_account_id]->two_fourty_days;
                  $two_seventy_days = $creditor_array[$statutory_account_id]->two_seventy_days;
                  $three_hundred_days = $creditor_array[$statutory_account_id]->three_hundred_days;
                  $three_thirty_days = $creditor_array[$statutory_account_id]->three_thirty_days;


                  $three_sixty_days = $creditor_array[$statutory_account_id]->three_sixty_days;
                  $over_three_sixty_days = $creditor_array[$statutory_account_id]->over_three_sixty_days;
                }

                if($report_detail == 720)
                {
                  $three_ninety_days = $creditor_array[$statutory_account_id]->three_ninety_days;

                  $four_twenty_days = $creditor_array[$statutory_account_id]->four_twenty_days;
                  $four_fifty_days = $creditor_array[$statutory_account_id]->four_fifty_days;
                  $four_eighty_days = $creditor_array[$statutory_account_id]->four_eighty_days;

                  $five_ten_days = $creditor_array[$statutory_account_id]->five_ten_days;
                  $five_fourty_days = $creditor_array[$statutory_account_id]->five_fourty_days;
                  $five_seventy_days = $creditor_array[$statutory_account_id]->five_seventy_days;
                  $six_hundred_days = $creditor_array[$statutory_account_id]->six_hundred_days;
                  $six_thirty_days = $creditor_array[$statutory_account_id]->six_thirty_days;
                  $six_sixty_days = $creditor_array[$statutory_account_id]->six_sixty_days;
                  $six_ninety_days = $creditor_array[$statutory_account_id]->six_ninety_days;
                  $seven_twenty_days = $creditor_array[$statutory_account_id]->seven_twenty_days;
                  $over_seven_twenty_days = $creditor_array[$statutory_account_id]->over_seven_twenty_days;

                }


                $date = date('Y-m-d');
                  

                if($report_detail == 180)
                {
                  $total_this_month +=$this_month;
                  $total_three_months +=$three_months;
                  $total_six_months +=$six_months;
                  $total_nine_months +=$nine_months;
                  $total_one_twenty_days +=$one_twenty_days;
                  $total_one_fifty_days +=$one_fifty_days;
                  $total_one_eighty_days +=$one_eighty_days;
                  $total_over_one_eighty_days +=$over_one_eighty_days;
                }
                else if($report_detail == 360)
                {
                 $total_this_month +=$this_month;
                  $total_three_months +=$three_months;
                  $total_six_months +=$six_months;
                  $total_nine_months +=$nine_months;
                  $total_one_twenty_days +=$one_twenty_days;
                  $total_one_fifty_days +=$one_fifty_days;
                  $total_one_eighty_days +=$one_eighty_days;
                  $total_two_ten_days += $two_ten_days;
                  $total_two_fourty_days += $two_fourty_days;
                  $total_two_seventy_days += $two_seventy_days;
                  $total_three_hundred_days += $three_hundred_days;
                  $total_three_thirty_days += $three_thirty_days;
                  $total_three_sixty_days += $three_sixty_days;
                  $total_over_three_sixty_days += $over_three_sixty_days;

                }
                else if($report_detail == 720)
                {

                  $total_this_month +=$this_month;
                  $total_three_months +=$three_months;
                  $total_six_months +=$six_months;
                  $total_nine_months +=$nine_months;
                  $total_one_twenty_days +=$one_twenty_days;
                  $total_one_fifty_days +=$one_fifty_days;
                  $total_one_eighty_days +=$one_eighty_days;

                  $total_two_ten_days += $two_ten_days;
                  $total_two_fourty_days += $two_fourty_days;
                  $total_two_seventy_days += $two_seventy_days;
                  $total_three_hundred_days += $three_hundred_days;
                  $total_three_thirty_days += $three_thirty_days;
                  $total_three_sixty_days += $three_sixty_days;
                  $total_three_ninety_days += $three_ninety_days;
                  $total_four_twenty_days += $four_twenty_days;
                  $total_four_fity_days += $four_fity_days;
                  $total_four_eighty_days += $four_eighty_days;
                  $total_five_ten_days += $five_ten_days;
                  $total_five_fourty_days += $five_fourty_days;
                  $total_five_seventy_days += $five_seventy_days;
                  $total_six_hundred_days += $six_hundred_days;
                  $total_six_thirty_days += $six_thirty_days;
                  $total_six_sixty_days += $six_sixty_days;
                  $total_six_ninety_days += $six_ninety_days;
                  $total_seven_twenty_days += $seven_twenty_days;
                  $total_over_seven_twenty_days += $over_seven_twenty_days;


                }

               
             
                // $total_payments += $payments_total;
                // $total_invoices += $invoice_total;

                if($report_detail == 180)
                  $invoice_total = $this_month + $three_months + $six_months + $nine_months + $one_twenty_days + $one_fifty_days + $one_eighty_days + $over_one_eighty_days;
                else if($report_detail == 360)
                 $invoice_total = $this_month + $three_months + $six_months + $nine_months + $one_twenty_days + $one_fifty_days + $one_eighty_days + $two_ten_days + $two_fourty_days + $two_seventy_days + $three_hundred_days + $three_thirty_days + $three_sixty_days + $over_three_sixty_days;
               else if($report_detail == 720)
                 $invoice_total = $this_month + $three_months + $six_months + $nine_months + $one_twenty_days + $one_fifty_days + $one_eighty_days + $two_ten_days + $two_fourty_days + $two_seventy_days + $three_hundred_days + $three_thirty_days + $three_sixty_days + $three_ninety_days + $four_twenty_days + $four_fifty_days + $four_eighty_days + $five_ten_days + $five_fourty_days + $five_seventy_days + $six_hundred_days + $six_thirty_days + $six_sixty_days + $six_ninety_days + $seven_twenty_days + $over_seven_twenty_days;
                


                $total_balance += $invoice_total;


                if($report_detail == 180)
                {


                    $report[$row_count][0] = $count;
                    $report[$row_count][1] = $statutory_account_name;
                    $report[$row_count][2] = number_format($this_month,2);
                    $report[$row_count][3] = number_format($three_months,2);
                    $report[$row_count][4] = number_format($six_months,2);
                    $report[$row_count][5] = number_format($nine_months,2);
                    $report[$row_count][6] = number_format($one_twenty_days,2);
                    $report[$row_count][7] = number_format($one_fifty_days,2);
                    $report[$row_count][8] = number_format($one_eighty_days,2);
                    $report[$row_count][9] = number_format($over_one_eighty_days,2);
                    $report[$row_count][10] = number_format($unallocated,2);
                    $report[$row_count][11] = number_format($invoice_total,2);

        

                }
                else if($report_detail == 360)
                {


                    $report[$row_count][0] = $count;
                    $report[$row_count][1] = $statutory_account_name;
                    $report[$row_count][2] = number_format($this_month,2);
                    $report[$row_count][3] = number_format($three_months,2);
                    $report[$row_count][4] = number_format($six_months,2);
                    $report[$row_count][5] = number_format($nine_months,2);
                    $report[$row_count][6] = number_format($one_twenty_days,2);
                    $report[$row_count][7] = number_format($one_fifty_days,2);
                    $report[$row_count][8] = number_format($one_eighty_days,2);
                    $report[$row_count][9] = number_format($two_ten_days,2);
                    $report[$row_count][10] = number_format($two_fourty_days,2);
                    $report[$row_count][11] = number_format($two_seventy_days,2);
                    $report[$row_count][12] = number_format($three_hundred_days,2);
                    $report[$row_count][13] = number_format($three_thirty_days,2);
                    $report[$row_count][14] = number_format($three_sixty_days,2);
                    $report[$row_count][15] = number_format($over_three_sixty_days,2);
                    $report[$row_count][16] = number_format($unallocated,2);
                    $report[$row_count][17] = number_format($invoice_total,2);


                }
                else if($report_detail == 720)
                {

                    $report[$row_count][0] = $count;
                    $report[$row_count][1] = $statutory_account_name;
                    $report[$row_count][2] = number_format($this_month,2);
                    $report[$row_count][3] = number_format($three_months,2);
                    $report[$row_count][4] = number_format($six_months,2);
                    $report[$row_count][5] = number_format($nine_months,2);
                    $report[$row_count][6] = number_format($one_twenty_days,2);
                    $report[$row_count][7] = number_format($one_fifty_days,2);
                    $report[$row_count][8] = number_format($one_eighty_days,2);
                    $report[$row_count][9] = number_format($two_ten_days,2);
                    $report[$row_count][10] = number_format($two_fourty_days,2);
                    $report[$row_count][11] = number_format($two_seventy_days,2);
                    $report[$row_count][12] = number_format($three_hundred_days,2);
                    $report[$row_count][13] = number_format($three_thirty_days,2);
                    $report[$row_count][14] = number_format($three_sixty_days,2);
                    $report[$row_count][15] = number_format($three_ninety_days,2);
                    $report[$row_count][16] = number_format($four_twenty_days,2);
                    $report[$row_count][17] = number_format($four_fifty_days,2);
                    $report[$row_count][18] = number_format($four_eighty_days,2);
                    $report[$row_count][19] = number_format($five_ten_days,2);
                    $report[$row_count][20] = number_format($five_fourty_days,2);
                    $report[$row_count][21] = number_format($five_seventy_days,2);
                    $report[$row_count][22] = number_format($six_hundred_days,2);
                    $report[$row_count][23] = number_format($six_thirty_days,2);
                    $report[$row_count][24] = number_format($six_sixty_days,2);
                    $report[$row_count][25] = number_format($six_ninety_days,2);
                    $report[$row_count][26] = number_format($seven_twenty_days,2);
                    $report[$row_count][27] = number_format($over_seven_twenty_days,2);
                    $report[$row_count][28] = number_format($unallocated,2);
                    $report[$row_count][29] = number_format($invoice_total,2);
            
          }



        }
        
        //display the patient data


        
          
        
        
      }


      if($report_detail == 180)
        {

          $report[$row_count][0] = '';
          $report[$row_count][1] = 'Total';
          $report[$row_count][2] = number_format($total_this_month,2);
          $report[$row_count][3] = number_format($total_three_months,2);
          $report[$row_count][4] = number_format($total_six_months,2);
          $report[$row_count][5] = number_format($total_nine_months,2);
          $report[$row_count][6] = number_format($total_one_twenty_days,2);
          $report[$row_count][7] = number_format($total_one_fifty_days,2);
          $report[$row_count][8] = number_format($total_one_eighty_days,2);
          $report[$row_count][9] = number_format($total_over_one_eighty_days,2);
          $report[$row_count][10] = number_format($total_unallocated,2);
          $report[$row_count][11] = number_format($total_balance,2);
        
         
        }
        else if($report_detail == 360)
        {



           $report[$row_count][0] = '';
            $report[$row_count][1] = 'Total';
            $report[$row_count][2] = number_format($total_this_month,2);
            $report[$row_count][3] = number_format($total_three_months,2);
            $report[$row_count][4] = number_format($total_six_months,2);
            $report[$row_count][5] = number_format($total_nine_months,2);
            $report[$row_count][6] = number_format($total_one_twenty_days,2);
            $report[$row_count][7] = number_format($total_one_fifty_days,2);
            $report[$row_count][8] = number_format($total_one_eighty_days,2);
            $report[$row_count][9] = number_format($total_two_ten_days,2);
            $report[$row_count][10] = number_format($total_two_fourty_days,2);
            $report[$row_count][11] = number_format($total_two_seventy_days,2);
            $report[$row_count][12] = number_format($total_three_hundred_days,2);
            $report[$row_count][13] = number_format($total_three_thirty_days,2);
            $report[$row_count][14] = number_format($total_three_sixty_days,2);
            $report[$row_count][15] = number_format($total_over_three_sixty_days,2);
            $report[$row_count][16] = number_format($total_unallocated,2);
            $report[$row_count][17] = number_format($total_balance,2);





         
        }
        else if($report_detail == 720)
        {


           $report[$row_count][0] = '';
            $report[$row_count][1] = 'Total';
            $report[$row_count][2] = number_format($total_this_month,2);
            $report[$row_count][3] = number_format($total_three_months,2);
            $report[$row_count][4] = number_format($total_six_months,2);
            $report[$row_count][5] = number_format($total_nine_months,2);
            $report[$row_count][6] = number_format($total_one_twenty_days,2);
            $report[$row_count][7] = number_format($total_one_fifty_days,2);
            $report[$row_count][8] = number_format($total_one_eighty_days,2);
            $report[$row_count][9] = number_format($total_two_ten_days,2);
            $report[$row_count][10] = number_format($total_two_fourty_days,2);
            $report[$row_count][11] = number_format($total_two_seventy_days,2);
            $report[$row_count][12] = number_format($total_three_hundred_days,2);
            $report[$row_count][13] = number_format($total_three_thirty_days,2);
            $report[$row_count][14] = number_format($total_three_sixty_days,2);
            $report[$row_count][15] = number_format($total_three_ninety_days,2);
            $report[$row_count][16] = number_format($total_four_twenty_days,2);
            $report[$row_count][17] = number_format($total_four_fity_days,2);
            $report[$row_count][18] = number_format($total_four_eighty_days,2);
            $report[$row_count][19] = number_format($total_five_ten_days,2);
            $report[$row_count][20] = number_format($total_five_fourty_days,2);
            $report[$row_count][21] = number_format($total_five_seventy_days,2);
            $report[$row_count][22] = number_format($total_six_hundred_days,2);
            $report[$row_count][23] = number_format($total_six_thirty_days,2);
            $report[$row_count][24] = number_format($total_six_sixty_days,2);
            $report[$row_count][25] = number_format($total_six_ninety_days,2);
            $report[$row_count][26] = number_format($total_seven_twenty_days,2);
            $report[$row_count][27] = number_format($total_over_seven_twenty_days,2);
            $report[$row_count][28] = number_format($total_unallocated,2);
            $report[$row_count][29] = number_format($total_balance,2);
         
        }
    }
    
    //create the excel document
    $this->excel->addArray ( $report );
    $this->excel->generateXML ($title);
  }


}
?>
