<?php

class Transfer_model extends CI_Model
{
  /*
  *	Count all items from a table
  *	@param string $table
  * 	@param string $where
  *
  */
  public function count_items($table, $where, $limit = NULL)
  {
    if($limit != NULL)
    {
      $this->db->limit($limit);
    }
    $this->db->from($table);
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  public function get_account_transfer_transactions($table, $where, $config, $page, $order, $order_method)
  {
    //retrieve all accounts
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $this->db->join('finance_transfered', 'finance_transfered.finance_transfer_id = finance_transfer.finance_transfer_id','left');
    $query = $this->db->get('', $config, $page);

    return $query;
  }
  public function transfer_funds()
  {

    $document_number_two = $this->create_transfer_number();
    $document_number = $this->input->post('reference_code');

    $exploded = explode('AT', $document_number);

    $suffix = $exploded[1];
    // $document_number = $this->create_purchases_payment();
    $account = array(
      'account_from_id'=>$this->input->post('account_from_id'),
      'finance_transfer_amount'=>$this->input->post('amount'),
      'transaction_date'=>$this->input->post('transfer_date'),
      'reference_number'=>$this->input->post('reference_number'),
      'created_by'=>$this->session->userdata('personnel_id'),
      'remarks'=>$this->input->post('description'),
      'created'=>date('Y-m-d H:i:s'),
      'reference_code'=>$document_number,
      'suffix'=>$suffix,
      'account_to_id'=>$this->input->post('account_to_id'),
      'last_modified'=>date('Y-m-d H:i:s')
    );
    if($this->db->insert('finance_transfer',$account))
    {
      $finance_transfer_id = $this->db->insert_id();
      $account = array(
        'account_to_id'=>$this->input->post('account_to_id'),
        'finance_transfered_amount'=>$this->input->post('amount'),
        'transaction_date'=>$this->input->post('transfer_date'),
        'created_by'=>$this->session->userdata('personnel_id'),
        'finance_transfer_id'=>$finance_transfer_id,
        'remarks'=>$this->input->post('description'),
        'created'=>date('Y-m-d H:i:s'),
        'last_modified'=>date('Y-m-d H:i:s'),
      );
      if($this->db->insert('finance_transfered',$account))
      {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }

  }

  public function edit_account_transfer_data(){
    $transfer_id = $this->input->post('transfer_id');
    $from_error = false;
    $to_error = false;
    $account_from = array(
      'account_from_id'=>$this->input->post('account_from_id'),
      'finance_transfer_amount'=>$this->input->post('amount'),
      'transaction_date'=>$this->input->post('transfer_date'),
      'reference_number'=>$this->input->post('reference_number'),
      'account_to_id'=>$this->input->post('account_to_id'),
      'remarks'=>$this->input->post('description'),
      'last_modified'=>date('Y-m-d H:i:s'),
    );

    $this->db->where('finance_transfer_id',$transfer_id);
    if($this->db->update('finance_transfer',$account_from))
    {
        //return TRUE;
      $from_error = FALSE;
    }
    else
    {
        //return FALSE;
      $from_error = TRUE;
    }


    $account_to = array(
      'account_to_id'=>$this->input->post('account_to_id'),
      'finance_transfered_amount'=>$this->input->post('amount'),
      'transaction_date'=>$this->input->post('transfer_date'),
      'created_by'=>$this->session->userdata('personnel_id'),
      'remarks'=>$this->input->post('description'),
      'last_modified'=>date('Y-m-d H:i:s'),
    );

    $this->db->where('finance_transfer_id',$transfer_id);
    if($this->db->update('finance_transfered',$account_to))
    {
        //return TRUE;
      $to_error = FALSE;
    }
    else
    {
        //return FALSE;
      $to_error = TRUE;
    }


    if($to_error == FALSE && $from_error == FALSE){
      return TRUE;
    } else {
      return FALSE;
    }




  }

  public function get_transfer_from_details($id)
  {
    $this->db->where('finance_transfer_id',$id);

    $query = $this->db->get('finance_transfer');
    // var_dump($query);
   // die();

    return $query;
  }

  public function get_transfer_to_details($id)
  {
    $this->db->where('finance_transfer_id',$id);

    $query = $this->db->get('finance_transfered');
    // var_dump($query);
   // die();

    return $query;
  }


  public function get_account_name($from_account_id)
  {
    $account_name = '';
    $this->db->select('account_name');
    $this->db->where('account_id = '.$from_account_id);
    $query = $this->db->get('account');

    $account_details = $query->row();
    $account_name = '';
    if($query->num_rows() > 0)
    {
     $account_name = $account_details->account_name;
   }


   return $account_name;
 }


 public function get_journal_details($id)
 {
  $this->db->where('journal_entry_id',$id);

  $query = $this->db->get('journal_entry');
    //var_dump($query);
   // die();

  return $query;

}

public function edit_journal_details(){
  $account_payment_id = $this->input->post('account_id');
  $adjustment_type_id = $this->input->post('adjustment_type_id');
  $account = array(
    'account_to_id'=>$this->input->post('account_to_id'),
    'account_from_id'=>$this->input->post('account_from_id'),
    'amount_paid'=>$this->input->post('amount'),
    'journal_entry_description'=>$this->input->post('description'),
    'payment_date'=>$this->input->post('payment_date'),
  );


  if($adjustment_type_id > 0)
  {



      $invoice_to_id = $this->input->post('invoice_to_id');
      $invoice_from_id = $this->input->post('invoice_from_id');
      if(!empty($invoice_to_id))
      {
        $exploded_invoice_to = explode('...', $invoice_to_id);


        if(is_array($exploded_invoice_to))
        {
           

           $invoice_id = $exploded_invoice_to[0];
           $invoice_number = $exploded_invoice_to[1];
           $invoice_type = $exploded_invoice_to[2];
           $creditor_id = $exploded_invoice_to[3];
           $account_type = $exploded_invoice_to[4];


           $account['account_to_type_id'] = $account_type;
           $account['invoice_to_id'] = $invoice_id;
           $account['invoice_to_number'] = $invoice_number;
           $account['attached_to_account_id'] = $creditor_id;
           $account['invoice_to_type'] = $invoice_type;

        }
      }
      else
      {
         $account['account_to_type_id'] = 0;
         $account['invoice_to_id'] = 0;
         $account['invoice_to_number'] = NUll;
         $account['attached_to_account_id'] = 0;
         $account['invoice_to_type'] = 0;
      }



      if(!empty($invoice_from_id))
      {
        $exploded_invoice_from = explode('...', $invoice_from_id);


        if(is_array($exploded_invoice_from))
        {
           

           $invoice_id = $exploded_invoice_from[0];
           $invoice_number = $exploded_invoice_from[1];
           $invoice_type = $exploded_invoice_from[2];
           $creditor_id = $exploded_invoice_from[3];
           $account_type = $exploded_invoice_from[4];


           $account['account_from_type_id'] = $account_type;
           $account['invoice_from_id'] = $invoice_id;
           $account['invoice_from_number'] = $invoice_number;
           $account['attached_from_account_id'] = $creditor_id;
           $account['invoice_from_type'] = $invoice_type;

        }
      }
      else
      {
          $account['account_from_type_id'] = 0;
           $account['invoice_from_id'] = 0;
           $account['invoice_from_number'] = NULL;
           $account['attached_from_account_id'] = 0;
           $account['invoice_from_type'] = 0;
      }
    }


  $this->db->where('document_number',$account_payment_id);
  if($this->db->update('journal_entry',$account))
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }


}



function create_journal_number()
{
    //select product code
  $preffix = "HA-RT-";
  $this->db->from('journal_entry');
  $this->db->where("journal_entry_id > 0");
  $this->db->select('MAX(document_number) AS number');
    $query = $this->db->get();//echo $query->num_rows();

    if($query->num_rows() > 0)
    {
      $result = $query->result();
      $number =  $result[0]->number;

      $number++;//go to the next number
    }
    else{//start generating receipt numbers
      $number = 1;
    }

    return $number;
  }


  public function add_journal_entry($type_id = 0)
  {

    

   

    $account = array(
      'account_to_id'=>$this->input->post('account_to_id'),
      'account_from_id'=>$this->input->post('account_from_id'),
      'amount_paid'=>$this->input->post('amount'),
      'journal_entry_description'=>$this->input->post('description'),
      'document_number'=>$this->input->post('reference_number'),
      'payment_date'=>$this->input->post('payment_date'),
      'created_by'=>$this->session->userdata('personnel_id'),
      'created'=>date('Y-m-d')
    );

      if($type_id > 0)
      {

          $account['journal_type_id'] = $type_id;

          $invoice_to_id = $this->input->post('invoice_to_id');
          $invoice_from_id = $this->input->post('invoice_from_id');
          if(!empty($invoice_to_id))
          {
            $exploded_invoice_to = explode('...', $invoice_to_id);


            if(is_array($exploded_invoice_to))
            {
               

               $invoice_id = $exploded_invoice_to[0];
               $invoice_number = $exploded_invoice_to[1];
               $invoice_type = $exploded_invoice_to[2];
               $creditor_id = $exploded_invoice_to[3];
               $account_type = $exploded_invoice_to[4];


               $account['account_to_type_id'] = $account_type;
               $account['invoice_to_id'] = $invoice_id;
               $account['invoice_to_number'] = $invoice_number;
               $account['attached_to_account_id'] = $creditor_id;
               $account['invoice_to_type'] = $invoice_type;

            }
          }



          if(!empty($invoice_from_id))
          {
            $exploded_invoice_from = explode('...', $invoice_from_id);


            if(is_array($exploded_invoice_from))
            {
               

               $invoice_id = $exploded_invoice_from[0];
               $invoice_number = $exploded_invoice_from[1];
               $invoice_type = $exploded_invoice_from[2];
               $creditor_id = $exploded_invoice_from[3];
               $account_type = $exploded_invoice_from[4];


               $account['account_from_type_id'] = $account_type;
               $account['invoice_from_id'] = $invoice_id;
               $account['invoice_from_number'] = $invoice_number;
               $account['attached_from_account_id'] = $creditor_id;
               $account['invoice_from_type'] = $invoice_type;

            }
          }
      }
      // var_dump($account); die();
    if($this->db->insert('journal_entry',$account))
    {
      return TRUE;
    }
    else
    {
      return FALSE;
    }
  }

  public function get_account_payments_transactions($table, $where, $config, $page, $order, $order_method)
  {
    //retrieve all accounts
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $query = $this->db->get('', $config, $page);

    return $query;
  }
  public function get_creditor_name($creditor_id)
  {
    $account_name = '';
    $this->db->select('creditor_name');
    $this->db->where('creditor_id = '.$creditor_id);
    $query = $this->db->get('creditor');

    $account_name = '';
    if($query->num_rows () > 0)
    {
      $account_details = $query->row();
      $account_name = $account_details->creditor_name;
    }


    return $account_name;
  }
  public function get_type_variables($table,$where,$select)
  {
      //retrieve all users
    $this->db->from($table);
    $this->db->select($select);
    $this->db->where($where);
    $query = $this->db->get();

    return $query;

  }

  public function add_account_payment()
  {
    $property_beneficiary_data = $this->input->post('property_beneficiary_id');

    $explode = explode('#', $property_beneficiary_data);

    $property_beneficiary_id = $explode[0];
    $property_owner_id = $explode[1];
    $account = array(
      'account_to_id'=>$this->input->post('account_to_id'),
      'account_from_id'=>$this->input->post('account_from_id'),
      'amount_paid'=>$this->input->post('amount'),
      'account_payment_description'=>$this->input->post('description'),
      'account_to_type'=>$this->input->post('account_to_type'),
      'receipt_number'=>$this->input->post('cheque_number'),
      'payment_date'=>$this->input->post('payment_date'),
      'created_by'=>$this->session->userdata('personnel_id'),
      'created'=>date('Y-m-d'),
      'payment_to'=>$property_owner_id,
      'property_beneficiary_id'=>$property_beneficiary_id
    );
      // var_dump($account); die();
    if($this->db->insert('account_payments',$account))
    {

      $account_payment_id = $this->db->insert_id();
      $account_from_id = $this->input->post('account_from_id');
      $transfer_charge = $this->get_transfer_charge($account_from_id);

      if($transfer_charge > 0)
      {
        $account = array(
          'account_to_id'=>32,
          'account_from_id'=>$this->input->post('account_from_id'),
          'amount_paid'=>$transfer_charge,
          'account_payment_description'=>$this->input->post('description'),
          'account_to_type'=>4,
          'receipt_number'=>$this->input->post('cheque_number'),
          'payment_date'=>$this->input->post('payment_date'),
          'created_by'=>$this->session->userdata('personnel_id'),
          'created'=>date('Y-m-d'),
          'parent_payment_id'=>$account_payment_id,
          'property_beneficiary_id'=>0,
          'payment_to'=>10
        );
            // var_dump($account); die();
        if($this->db->insert('account_payments',$account))
        {
          return TRUE;
        }
        else
        {
          return TRUE;
        }
      }
      else
      {
        return TRUE;
      }

    }
    else
    {
      return FALSE;
    }
  }
  public function get_transfer_charge($account_id)
  {

   $transfer_charge = 0;
   $this->db->select('transfer_charge');
   $this->db->where('account_id = '.$account_id);
   $query = $this->db->get('account');

   $account_details = $query->row();
   $transfer_charge = $account_details->transfer_charge;

   return $transfer_charge;

 }
 public function get_owner_name($property_owner_id)
 {
  $account_name = '';
  $this->db->select('property_owner_name');
  $this->db->where('property_owner_id = '.$property_owner_id);
  $query = $this->db->get('property_owners');

  $account_details = $query->row();
  if($query->num_rows() > 0)
  {
    $account_name = $account_details->property_owner_name;
  }


  return $account_name;
}

public function get_beneficiary_name($property_beneficiary_id)
{
  $account_name = '';
  $this->db->select('property_beneficiary_name');
  $this->db->where('property_beneficiary_id = '.$property_beneficiary_id);
  $query = $this->db->get('property_beneficiaries');

  $account_details = $query->row();
  $account_name = $account_details->property_beneficiary_name;

  return $account_name;
}

public function export_direct_payments()
{
 $this->load->library('excel');

      //get all transactions
 $where = 'account_payment_deleted = 0 ';
 $table = 'account_payments';


 $search = $this->session->userdata('search_direct_payments');

 if(!empty($search))
 {
  $where .=$search;
}
$this->db->where($where);
$this->db->order_by('account_payments.payment_date', 'ASC');
$this->db->select('*');
$defaulters_query = $this->db->get($table);

$title = 'Direct Payments';

if($defaulters_query->num_rows() > 0)
{
  $count = 0;
        /*
          -----------------------------------------------------------------------------------------
          Document Header
          -----------------------------------------------------------------------------------------
        */



          $row_count = 0;
          $report[$row_count][0] = '#';
          $report[$row_count][1] = 'Payment Date';
          $report[$row_count][2] = 'Payment From';
          $report[$row_count][3] = 'Transaction Number.';
          $report[$row_count][4] = 'Description';
          $report[$row_count][5] = 'Amount';



        //get & display all services

        //display all patient data in the leftmost columns
          foreach($defaulters_query->result() as $leases_row)
          {

           $account_from_id = $leases_row->account_from_id;
           $account_to_type = $leases_row->account_to_type;
           $account_to_id = $leases_row->account_to_id;
           $receipt_number = $leases_row->receipt_number;
           $account_payment_id = $leases_row->account_payment_id;
           $payment_date = $leases_row->payment_date;
           $created = $leases_row->created;
           $amount_paid = $leases_row->amount_paid;
           $payment_to = $leases_row->payment_to;
           $property_beneficiary_id = $leases_row->property_beneficiary_id;

           $account_from_name = $this->transfer_model->get_account_name($account_from_id);
           if($account_to_type == 1 AND $account_to_id > 0)
           {
            $payment_type = 'Transfer';
            $account_to_name = $this->transfer_model->get_account_name($account_to_id);
          }
          else if($account_to_type == 3 AND $payment_to > 0)
          {
                // doctor payments
            $payment_type = "Landlord Payment";
            $account_to_name = $this->transfer_model->get_owner_name($payment_to);
          }
          else if($account_to_type == 2 AND $account_to_id > 0)
          {
                // creditor
            $payment_type = "Creditor Payment";
            $account_to_name = $this->transfer_model->get_creditor_name($account_to_id);
          }
          else if($account_to_type == 4 AND $account_to_id > 0)
          {
                // expense account
            $payment_type = "Direct Expense Payment";
            $account_to_name = $this->transfer_model->get_account_name($account_to_id);
          }
          else if($account_to_type == 3 AND $property_beneficiary_id > 0)
          {
                // doctor payments
            $payment_type = "Landlord Payment";
            $account_to_name = $this->transfer_model->get_beneficiary_name($property_beneficiary_id);
          }
          else
          {
            $account_to_name ='';
          }



          $row_count++;
          $count++;
              //display the patient data
          $report[$row_count][0] = $row_count;
          $report[$row_count][1] = $payment_date;
          $report[$row_count][3] = $account_from_name;
          $report[$row_count][2] = strtoupper($receipt_number);
          $report[$row_count][4] = $payment_type.' '.$account_to_name;
          $report[$row_count][5] = number_format($amount_paid,2);


        }
      }

      //create the excel document
      $this->excel->addArray ( $report );
      $this->excel->generateXML ($title);
    }

    // public function get_account_name($from_account_id)
    // {
    //   $account_name = '';
    //   $this->db->select('account_name');
    //   $this->db->where('account_id = '.$from_account_id);
    //   $query = $this->db->get('account');

    //   $account_details = $query->row();
    //   $account_name = $account_details->account_name;

    //   return $account_name;
    // }


    public function get_property_name($from_account_id)
    {
      $property_name = '';
      $this->db->select('property_name');
      $this->db->where('property_id = '.$from_account_id);
      $query = $this->db->get('property');

      $account_details = $query->row();
      $property_name = $account_details->property_name;

      return $property_name;
    }

    public function get_property_owner_name($from_account_id)
    {
      $property_name = '';
      $this->db->select('property_owner_name');
      $this->db->where('property_owner_id = '.$from_account_id);
      $query = $this->db->get('property_owners');

      $account_details = $query->row();
      $property_name = $account_details->property_owner_name;

      return $property_name;
    }


    // function create_transfer_number()
    // {
    //   //select product code
    //   $preffix = "HA-RT-";
    //   $this->db->from('landlord_transfer');
    //   $this->db->where("landlord_transfer_id > 0");
    //   $this->db->select('MAX(document_number) AS number');
    //   $query = $this->db->get();//echo $query->num_rows();

    //   if($query->num_rows() > 0)
    //   {
    //     $result = $query->result();
    //     $number =  $result[0]->number;

    //     $number++;//go to the next number
    //   }
    //   else{//start generating receipt numbers
    //     $number = 1;
    //   }

    //   return $number;
    // }


  public function create_transfer_number()
  {
      //select product code

    $this->db->where('finance_transfer_id > 0 ');
    $this->db->from('finance_transfer');
    $this->db->select('finance_transfer_id AS number');
    $this->db->order_by('suffix','ASC');
    // $this->db->limit(1);
    $query = $this->db->get();

    if($query->num_rows() > 0)
    {
      $result = $query->result();
      $number =  $result[0]->number;
      // $finance_transfer_id = $result[0]->finance_transfer_id;

       // var_dump($number); die();

       if(empty($number))
       {

          $number = 1;

       }
        else
        {
            $number++;
        }



    }
    else{//start generating rece$number= 1;ipt numbers
     $number = 1;

    }

    // var_dump($number); die();
    return 'AT'.$number;
  }


  public function add_cheque_sequence()
   {
      $array_add['branch_id'] = $this->session->userdata('branch_id');
      $array_add['bank_id'] = $this->input->post('bank_id');
      $array_add['sequence_start'] = $start = $this->input->post('sequence_start');
      $array_add['sequence_end'] = $end = $this->input->post('sequence_end');
      $array_add['created_by'] = $this->session->userdata('personnel_id');
      $array_add['created_on'] = date('Y-m-d');
      $array_add['cheque_sequence_deleted'] = 0;

      if($this->db->insert('cheque_sequence',$array_add))
      {
         $cheque_sequence_id = $this->db->insert_id();
         for ($i=$start; $i <= $end; $i++) {
           // code...
             $array_cheque_add['cheque_number'] = $i;
             $array_cheque_add['bank_id'] =  $this->input->post('bank_id');
             $array_cheque_add['cheque_status'] =  0;
             $array_cheque_add['allocated_to_type'] =  0;
             $array_cheque_add['cheque_sequence_id'] =  $cheque_sequence_id;
             $array_cheque_add['recon_id'] =  0;

             $this->db->insert('cheques',$array_cheque_add);


         }


      }

      return TRUE;
   }

   public function get_transactions($table, $where, $config, $page, $order, $order_method)
   {
     //retrieve all accounts
     $this->db->from($table);
     $this->db->select('*');
     $this->db->where($where);
     $this->db->order_by($order, $order_method);
     $query = $this->db->get('', $config, $page);

     return $query;
   }

   public function get_all_cheque_series($cheque_sequence_id)
   {

     $select_query = "
                       SELECT

                       *

                       FROM
                       (

                         SELECT
                           creditor_payment.transaction_date AS payment_date,
                           creditor.creditor_name AS paid_to_name,
                           'Creditor Invoice Payment' AS transaction_type,
                           creditor_payment.total_amount AS amount_paid,
                           cheques.cheque_status AS cheque_status,
                           cheques.paid_status AS paid_status,
                           cheques.cheque_number AS cheque_number,
                           cheques.cheque_sequence_id AS cheque_sequence_id
                         FROM
                         cheque_sequence,cheques,account,creditor_payment,creditor
                         WHERE
                         cheque_sequence.cheque_sequence_id = cheques.cheque_sequence_id
                         AND cheque_sequence.cheque_sequence_deleted = 0
                         AND cheque_sequence.bank_id = account.account_id
                         AND cheques.creditor_payment_id = creditor_payment.creditor_payment_id
                         AND creditor.creditor_id = cheques.paid_to


                         UNION ALL


                         SELECT
                           '' AS payment_date,
                           '' AS paid_to_name,
                           'OPEN' AS transaction_type,
                           0 AS amount_paid,
                           cheques.cheque_status AS cheque_status,
                           cheques.paid_status AS paid_status,
                           cheques.cheque_number AS cheque_number,
                           cheques.cheque_sequence_id AS cheque_sequence_id
                         FROM
                         cheque_sequence,cheques
                         WHERE
                         cheque_sequence.cheque_sequence_id = cheques.cheque_sequence_id
                         AND cheque_sequence.cheque_sequence_deleted = 0  AND cheques.paid_status = 0

                       ) AS data WHERE data.cheque_sequence_id = $cheque_sequence_id


                     ";
     $query = $this->db->query($select_query);
     // $this->db->select('cheques.*');
     // $this->db->where('cheque_sequence.cheque_sequence_id = cheques.cheque_sequence_id AND cheque_sequence.cheque_sequence_deleted = 0 AND cheque_sequence.bank_id = account.account_id AND cheque_sequence_id.cheque_sequence_id = '.$cheque_sequence_id);
     // $query = $this->db->get('cheque_sequence,cheques,account');

     return $query;
   }


   public function confirm_parent_account($account_id,$parent_creditor_payable_id,$parent_provider_payable_id,$parent_payroll_payable_id)
   {


      $this->db->where('account_id',$account_id);
      $this->db->select('parent_account');
      $this->db->limit(1);
      $query = $this->db->get('account');

      if($query->num_rows() > 0)
      {
        foreach ($query->result() as $key => $value) {
          // code...
          $parent_account = $value->parent_account;
        }
      }

      if(empty($parent_account))
      {
        return FALSE;
      }
      else
      {

        if($parent_account == $parent_creditor_payable_id)
        {
          // get all creditors unpayed invoices

          return 'creditor';
        }
        else if($parent_account == $parent_provider_payable_id)
        {
          // get all creditors unpayed invoices
          return 'provider';
        }
        else if($parent_account == $parent_payroll_payable_id)
        {
          // get all creditors unpayed invoices
          return 'payroll';
        }
        else
        {
          return FALSE;
        }
      }
   }

  }
  ?>
