<?php

class loans_model extends CI_Model
{
  /*
  * Retrieve all loan
  * @param string $table
  *   @param string $where
  *
  */
  public function get_all_loans($table, $where, $per_page, $page, $order = 'loan_name', $order_method = 'ASC')
  {
    //retrieve all users
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }

	/*
	*	Add a new loan
	*
	*/
  public function get_loans_list($table, $where, $order)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order,'asc');
		$query = $this->db->get('');

		return $query;
	}

  /*
	*	get a single loan's details
	*	@param int $loan_id
	*
	*/
	public function get_loan($loan_id)
	{
		//retrieve all users
		$this->db->from('loan');
		$this->db->select('*');
		$this->db->where('loan_id = '.$loan_id);
		$query = $this->db->get();

		return $query;
	}


  public function add_invoice_item($loan_id,$loan_invoice_id)
	{
		$amount = $this->input->post('unit_price');
		$account_to_id=$this->input->post('account_to_id');
    $item_description = $this->input->post('item_description');
		$quantity=$this->input->post('quantity');
    $vat_amount = $this->input->post('vat_amount');
    $total_amount = $this->input->post('total_amount');
		$tax_type_id=$this->input->post('tax_type_id');


		$service = array(
							'loan_invoice_id'=>0,
							'unit_price'=> $amount,
							'account_to_id' => $account_to_id,
							'loan_id' => $loan_id,
              'item_description'=>$item_description,
							'created_by' => $this->session->userdata('personnel_id'),
							'created' => date('Y-m-d'),
              'total_amount'=>$total_amount,
              'vat_amount'=>$vat_amount,
              'quantity'=>$quantity,
              'vat_type_id'=>$tax_type_id
						);
    if(!empty($loan_invoice_id))
    {
      $service['loan_invoice_id'] = $loan_invoice_id;
      $service['loan_invoice_item_status'] = 1;
    }
    else
    {
      $service['loan_invoice_item_status'] = 0;
    }


		$this->db->insert('loan_invoice_item',$service);
		return TRUE;

	}

  public function edit_invoice_item($loan_id,$loan_invoice_id)
  {
    $amount = $this->input->post('unit_price');
    $account_to_id=$this->input->post('account_to_id');
    $item_description = $this->input->post('item_description');
    $quantity=$this->input->post('quantity');
    $vat_amount = $this->input->post('vat_amount');
    $total_amount = $this->input->post('total_amount');
    $loan_amount = $this->input->post('loan_amount');
    $tax_type_id=$this->input->post('tax_type_id');
     $loan_invoice_item_id=$this->input->post('loan_invoice_item_id');

    if(empty($loan_amount))
      $loan_amount = 0;


    $service = array(
              'loan_invoice_id'=>0,
              'unit_price'=> $amount,
              'account_to_id' => $account_to_id,
              'loan_id' => $loan_id,
              'item_description'=>$item_description,
              'created_by' => $this->session->userdata('personnel_id'),
              'created' => date('Y-m-d'),
              'total_amount'=>$total_amount,
              'vat_amount'=>$vat_amount,
              'quantity'=>$quantity,
              'vat_type_id'=>$tax_type_id
            );
   
    $service['loan_invoice_id'] = $loan_invoice_id;
    $service['loan_invoice_item_status'] = 1;
 

    $this->db->where('loan_invoice_item_id',$loan_invoice_item_id);
    $this->db->update('loan_invoice_item',$service);


    // update also the loan invoice
    $service_loan['loan_amount'] = $loan_amount;
    $service_loan['total_amount'] = $total_amount;
    $this->db->where('loan_invoice_id',$loan_invoice_id);
    $this->db->update('loan_invoice',$service_loan);


    return TRUE;

  }

  public function confirm_loan_invoice($loan_id,$loan_invoice_id = NULL)
	{
		$amount = $this->input->post('amount');
		$amount_charged = $this->input->post('amount_charged');
		$invoice_date = $this->input->post('invoice_date');
    $vat_charged = $this->input->post('vat_charged');
    $invoice_number = $this->input->post('invoice_number');
    $interest_charge = $this->input->post('interest_charge');
     $insurance_charge = $this->input->post('insurance_charge');
    $loan_amount = $this->input->post('loan_amount');

		$date_check = explode('-', $invoice_date);
		$month = $date_check[1];
		$year = $date_check[0];

    $invoice_number = $month.'-'.$year;
		$document_number = $this->create_invoice_number();

		// var_dump($checked); die();

		$insertarray['transaction_date'] = $invoice_date;
		$insertarray['invoice_year'] = $year;
		$insertarray['invoice_month'] = $month;
		$insertarray['loan_id'] = $loan_id;
		$insertarray['document_number'] = $document_number;
    $insertarray['invoice_number'] = strtoupper($invoice_number);
		$insertarray['total_amount'] = $amount_charged;
		$insertarray['vat_charged'] = $vat_charged;
    $insertarray['interest_charge'] = $interest_charge;
    $insertarray['insurance_charge'] = $insurance_charge;
		$insertarray['created_by'] = $this->session->userdata('personnel_id');
		$insertarray['created'] = date('Y-m-d');
		$insertarray['amount'] = $amount;

    if(!empty($loan_invoice_id))
    {
        $this->db->where('loan_invoice_id',$loan_invoice_id);
        if($this->db->update('loan_invoice', $insertarray))
        {

          $total_visits = sizeof($_POST['loan_invoice_items']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['loan_invoice_items'];
              $loan_invoice_item_id = $visit[$r];
              //check if card is held
              $service = array(
                        'loan_invoice_id'=>$loan_invoice_id,
                        'created' =>$invoice_date,
                        'loan_invoice_item_status'=>1,
                        'year'=>$year,
                        'month'=>$month,
                      );
              $this->db->where('loan_invoice_item_id',$loan_invoice_item_id);
              $this->db->update('loan_invoice_item',$service);
            }
          }

          return TRUE;
        }
    }
    else
    {
      if($this->db->insert('loan_invoice', $insertarray))
      {

        $loan_invoice_id = $this->db->insert_id();
        $total_visits = sizeof($_POST['loan_invoice_items']);
        //check if any checkboxes have been ticked
        if($total_visits > 0)
        {
          for($r = 0; $r < $total_visits; $r++)
          {
            $visit = $_POST['loan_invoice_items'];
            $loan_invoice_item_id = $visit[$r];
            //check if card is held
            $service = array(
                      'loan_invoice_id'=>$loan_invoice_id,
                      'created' =>$invoice_date,
                      'loan_invoice_item_status'=>1,
                      'year'=>$year,
                      'month'=>$month,
                    );
            $this->db->where('loan_invoice_item_id',$loan_invoice_item_id);
            $this->db->update('loan_invoice_item',$service);
          }
        }

        return TRUE;
      }
    }
		

	}

  public function create_invoice_number()
	{
		//select product code
		$this->db->where('loan_invoice_id > 0');
		$this->db->from('loan_invoice');
		$this->db->select('MAX(document_number) AS number');
		$this->db->order_by('loan_invoice_id','DESC');
		// $this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			// var_dump($number);die();
			$number++;

		}
		else{
			$number = 1;
		}
		// var_dump($number);die();
		return $number;
	}

  public function get_loan_invoice($loan_id,$limit=null)
	{
		$this->db->where('loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id AND loan_invoice.loan_invoice_status = 1 AND loan_invoice.loan_id = '.$loan_id);
		if($limit)
		{
			$this->db->limit($limit);
		}
		$this->db->group_by('loan_invoice.loan_invoice_id');
		$this->db->order_by('loan_invoice.transaction_date','DESC');
		return $this->db->get('loan_invoice_item,loan_invoice');
	}



  public function get_loan_invoice_number($loan_id,$limit=null)
  {
    // $this->db->where('v_loans_invoice_balances.loan_id = '.$loan_id);
    // $this->db->select('*');
    // return $this->db->get('v_loans_invoice_balances');

    $select_statement = "
                        SELECT
                          data.invoice_id AS loan_invoice_id,
                          data.invoice_number AS invoice_number,
                          data.invoice_date AS invoice_date,
                          data.loan_invoice_type AS loan_invoice_type,
                          COALESCE (SUM(data.dr_amount),0) AS dr_amount,
                          COALESCE (SUM(data.cr_amount),0) AS cr_amount,
                          COALESCE (SUM(data.dr_amount),0) - COALESCE (SUM(data.cr_amount),0) AS balance
                        FROM 
                        (
                         

                            SELECT
                            `loan_invoice`.`loan_id` AS loan_id,
                            `loan_invoice`.`loan_invoice_id` AS invoice_id,
                            `loan_invoice`.`invoice_number` AS invoice_number,
                            `loan_invoice`.`transaction_date` AS invoice_date,
                            'loan Bills' AS loan_invoice_type,
                            COALESCE (SUM(`loan_invoice_item`.`total_amount`),0) AS dr_amount,
                            0 AS cr_amount
                            FROM (`loan_invoice`,loan_invoice_item,loan)
                            WHERE `loan_invoice_item`.`loan_invoice_id` = `loan_invoice`.`loan_invoice_id` AND loan_invoice.loan_invoice_status = 1 AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
                            GROUP BY `loan_invoice`.`loan_invoice_id`

                            UNION ALL 

                            SELECT
                            `loan_invoice`.`loan_id` AS loan_id,
                            `loan_invoice`.`loan_invoice_id` AS invoice_id,
                            `loan_invoice`.`invoice_number` AS invoice_number,
                            `loan_invoice`.`transaction_date` AS invoice_date,
                            'loan Bills Credit Note' AS loan_invoice_type,
                            0 AS dr_amount,
                            COALESCE (SUM(`loan_credit_note_item`.`credit_note_amount`),0) AS cr_amount
                            FROM (`loan_invoice`,loan_credit_note,loan_credit_note_item)
                            WHERE `loan_credit_note_item`.`loan_credit_note_id` = `loan_credit_note`.`loan_credit_note_id`
                            AND `loan_invoice`.`loan_invoice_id` = `loan_credit_note_item`.`loan_invoice_id` AND loan_credit_note.loan_credit_note_status = 1
                            GROUP BY `loan_credit_note_item`.`loan_invoice_id`

                            UNION ALL


                            SELECT
                            `loan_invoice`.`loan_id` AS loan_id,
                            `loan_invoice`.`loan_invoice_id` AS invoice_id,
                            `loan_invoice`.`invoice_number` AS invoice_number,
                            `loan_invoice`.`transaction_date` AS invoice_date,
                            'Bill Payments' AS loan_invoice_type,
                            0 AS dr_amount,
                            COALESCE (SUM(`loan_payment_item`.`amount_paid`),0) AS cr_amount
                            FROM (loan_payment_item,loan_payment,loan_invoice,loan)
                            WHERE `loan_payment_item`.`loan_invoice_id` = `loan_invoice`.`loan_invoice_id` 
                            AND `loan_payment_item`.`loan_payment_id` = `loan_payment`.`loan_payment_id` AND loan_payment_item.invoice_type = 0 AND loan_payment.loan_payment_status = 1 AND `loan_payment_item`.`loan_id` = `loan`.`loan_id` AND loan.account_from_id = loan_payment_item.account_to_id
                            GROUP BY loan_invoice.loan_invoice_id

                            UNION ALL 

                             SELECT
                            `loan`.`loan_id` AS loan_id,
                            `loan`.`loan_id` AS invoice_id,
                            `loan`.`loan_id` AS invoice_number,
                            `loan`.`start_date` AS invoice_date,
                            'Opening Balance' AS loan_invoice_type,
                             COALESCE (SUM(opening_balance),0) AS dr_amount,
                            '0' AS cr_amount
                            FROM (loan)
                            WHERE loan.loan_id > 0
                            GROUP BY loan.loan_id

                           

                          ) AS data,loan WHERE data.loan_id = ".$loan_id." AND data.loan_id = loan.loan_id AND data.invoice_date >= loan.start_date   GROUP BY data.invoice_number ORDER BY data.invoice_date ASC ";
                          $query = $this->db->query($select_statement);
                  return $query;


  }

  public function add_credit_note_item($loan_id,$loan_credit_note_id)
  {

    $amount = $this->input->post('amount');
		$account_to_id=$this->input->post('account_to_id');
    $description = $this->input->post('description');
		$tax_type_id=$this->input->post('tax_type_id');

    if($tax_type_id == 0)
    {
      $amount = $amount;
      $vat = 0;
    }
    else if($tax_type_id == 1)
    {

      $vat = $amount *0.16;
      $amount = $amount*1.16;
    }
    else if($tax_type_id == 2){

      $vat = $amount*0.05;
      $amount = $amount *1.05;
    }

    // var_dump($amount);die();


		$service = array(
							'loan_id' => $loan_id,
              'account_to_id' => $account_to_id,
              'description'=>$description,
							'created_by' => $this->session->userdata('personnel_id'),
							'created' => date('Y-m-d'),
              'credit_note_amount'=>$amount,
              'credit_note_charged_vat'=>$vat,
              'vat_type_id'=>$tax_type_id
						);

    if(!empty($loan_credit_note_id))
    {
      $service['loan_credit_note_id'] = $loan_credit_note_id;
      $service['loan_credit_note_item_status'] = 1;
    }
    else
    {
       $service['loan_credit_note_item_status'] = 0;
    }

		$this->db->insert('loan_credit_note_item',$service);
		return TRUE;

  }


  public function confirm_loan_credit_note($loan_id,$loan_credit_note_id)
  {
    $amount = $this->input->post('amount');
    $amount_charged = $this->input->post('amount_charged');
    $invoice_date = $this->input->post('credit_note_date');
    $loan_invoice_id = $this->input->post('invoice_id');
    $vat_charged = $this->input->post('vat_charged');
    $invoice_number = $this->input->post('credit_note_number');

    $date_check = explode('-', $invoice_date);
    $month = $date_check[1];
    $year = $date_check[0];


    $document_number = $this->create_credit_note_number();

    // var_dump($checked); die();

    $insertarray['transaction_date'] = $invoice_date;
    $insertarray['invoice_year'] = $year;
    $insertarray['invoice_month'] = $month;
    $insertarray['loan_id'] = $loan_id;
    $insertarray['loan_invoice_id'] = $loan_invoice_id;
    $insertarray['document_number'] = $document_number;
    $insertarray['invoice_number'] = strtoupper($invoice_number);
    $insertarray['total_amount'] = $amount_charged;
    $insertarray['vat_charged'] = $vat_charged;
 
    $insertarray['amount'] = $amount;
    $insertarray['account_from_id'] = 83;


     $total_visits = sizeof($_POST['loan_notes_items']);

     // var_dump($total_visits);die();

     if(!empty($loan_credit_note_id))
     {
        $this->db->where('loan_credit_note_id',$loan_credit_note_id);
        if($this->db->update('loan_credit_note', $insertarray))
        {


          $total_visits = sizeof($_POST['loan_notes_items']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['loan_notes_items'];
              $loan_credit_note_item_id = $visit[$r];
              //check if card is held
              $service = array(
                        'loan_credit_note_id'=>$loan_credit_note_id,
                        'created' =>$invoice_date,
                        'loan_credit_note_item_status'=>1,
                        'loan_invoice_id'=>$loan_invoice_id,
                        'year'=>$year,
                        'month'=>$month,
                      );
              $this->db->where('loan_credit_note_item_id',$loan_credit_note_item_id);
              $this->db->update('loan_credit_note_item',$service);
            }
          }
          return TRUE;
        }
     }
     else
     {

        $insertarray['created_by'] = $this->session->userdata('personnel_id');
        $insertarray['created'] = date('Y-m-d');
        if($this->db->insert('loan_credit_note', $insertarray))
        {
          $loan_credit_note_id = $this->db->insert_id();


          $total_visits = sizeof($_POST['loan_notes_items']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['loan_notes_items'];
              $loan_credit_note_item_id = $visit[$r];
              //check if card is held
              $service = array(
                        'loan_credit_note_id'=>$loan_credit_note_id,
                        'created' =>$invoice_date,
                        'loan_credit_note_item_status'=>1,
                        'loan_invoice_id'=>$loan_invoice_id,
                        'year'=>$year,
                        'month'=>$month,
                      );
              $this->db->where('loan_credit_note_item_id',$loan_credit_note_item_id);
              $this->db->update('loan_credit_note_item',$service);
            }
          }
          return TRUE;
        }
     }

  }

  public function create_credit_note_number()
	{
		//select product code
		$this->db->where('loan_invoice_id > 0');
		$this->db->from('loan_invoice');
		$this->db->select('MAX(document_number) AS number');
		$this->db->order_by('loan_invoice_id','DESC');
		// $this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			// var_dump($number);die();
			$number++;

		}
		else{
			$number = 1;
		}
		// var_dump($number);die();
		return $number;
	}


  public function get_loan_credit_notes($loan_id,$limit=null)
	{
		$this->db->where('loan_credit_note.loan_credit_note_id = loan_credit_note_item.loan_credit_note_id AND loan_credit_note.loan_credit_note_status = 1 AND loan_credit_note.loan_id = '.$loan_id);
		if($limit)
		{
			$this->db->limit($limit);
		}
		$this->db->group_by('loan_credit_note.loan_credit_note_id');
		$this->db->order_by('loan_credit_note.transaction_date','DESC');
		return $this->db->get('loan_credit_note_item,loan_credit_note');
	}


  public function get_loan_payments($loan_id,$limit=null)
  {
    $this->db->where('loan_payment.loan_payment_id = loan_payment_item.loan_payment_id AND loan_payment.loan_payment_status = 1 AND loan_payment.loan_id = '.$loan_id);
    if($limit)
    {
      $this->db->limit($limit);
    }
    $this->db->group_by('loan_payment.loan_payment_id');
    $this->db->join('account','account.account_id = loan_payment.account_from_id','left');
    $this->db->order_by('loan_payment.transaction_date','DESC');
    return $this->db->get('loan_payment_item,loan_payment');
  }

  public function add_payment_item($loan_id,$loan_payment_id)
  {

    $amount = $this->input->post('amount_paid');
    $account_to_id = $this->input->post('account_to_id');
    $loan_invoice_id = $this->input->post('invoice_id');



    // if(empty($loan_invoice_id))
    // {
    //   $invoice_type = 2;
    // }
    // else
    // {
      $exploded = explode('.', $loan_invoice_id);
      $invoice_id = $exploded[0];
      $invoice_number = $exploded[1];
      $invoice_type = $exploded[2];

    // }

    $service = array(
              'loan_invoice_id'=>$invoice_id,
              'invoice_number'=>$invoice_number,
              'invoice_type'=>$invoice_type,
              'account_to_id'=>$account_to_id,
              'loan_payment_item_status' => 0,
              'loan_payment_id' => 0,
              'loan_id' => $loan_id,
              'created_by' => $this->session->userdata('personnel_id'),
              'created' => date('Y-m-d'),
              'amount_paid'=>$amount,
            );
            // var_dump($service);die();

    if(!empty($loan_payment_id))
    {
      $service['loan_payment_id'] = $loan_payment_id;
      $service['loan_payment_item_status'] = 1;
    }

    $this->db->insert('loan_payment_item',$service);
    return TRUE;

  }

  public function confirm_loan_payment($loan_id,$loan_payment_id)
  {
    $amount_paid = $this->input->post('amount_paid');
    $payment_date = $this->input->post('payment_date');
    $reference_number = $this->input->post('reference_number');
    $account_from_id = $this->input->post('account_from_id');

    $date_check = explode('-', $payment_date);
    $month = $date_check[1];
    $year = $date_check[0];


    // var_dump($year);die();

    if(!empty($loan_payment_id))
    {
     

      // $document_number = $this->create_credit_payment_number();

      $insertarray['transaction_date'] = $payment_date;
      $insertarray['payment_year'] = $year;
      $insertarray['payment_month'] = $month;
      $insertarray['loan_id'] = $loan_id;
      $insertarray['reference_number'] = strtoupper($reference_number);
      $insertarray['total_amount'] = $amount_paid;
      $insertarray['account_from_id'] = $account_from_id;
      $insertarray['created_by'] = $this->session->userdata('personnel_id');
       $this->db->where('loan_payment_id',$loan_payment_id);

      if($this->db->update('loan_payment', $insertarray))
      {


        $total_visits = sizeof($_POST['loan_payments_items']);

        //check if any checkboxes have been ticked
        if($total_visits > 0)
        {
          for($r = 0; $r < $total_visits; $r++)
          {
            $visit = $_POST['loan_payments_items'];
            $loan_payment_item_id = $visit[$r];
            //check if card is held
            $service = array(
                      'loan_payment_id'=>$loan_payment_id,
                      'created' =>$payment_date,
                      'loan_payment_item_status'=>1,
                      'year'=>$year,
                      'month'=>$month,
                    );
            $this->db->where('loan_payment_item_id',$loan_payment_item_id);
            $this->db->update('loan_payment_item',$service);
          }
        }



          return TRUE;
      }

    }

    else
    {
      $document_number = $this->create_credit_payment_number();

      $insertarray['transaction_date'] = $payment_date;
      $insertarray['payment_year'] = $year;
      $insertarray['payment_month'] = $month;
      $insertarray['loan_id'] = $loan_id;
      $insertarray['document_number'] = $document_number;
      $insertarray['reference_number'] = strtoupper($reference_number);
      $insertarray['total_amount'] = $amount_paid;
      $insertarray['account_from_id'] = $account_from_id;
      $insertarray['created_by'] = $this->session->userdata('personnel_id');
      $insertarray['created'] = date('Y-m-d');

      if($this->db->insert('loan_payment', $insertarray))
      {
        $loan_payment_id = $this->db->insert_id();


        $total_visits = sizeof($_POST['loan_payments_items']);

        //check if any checkboxes have been ticked
        if($total_visits > 0)
        {
          for($r = 0; $r < $total_visits; $r++)
          {
            $visit = $_POST['loan_payments_items'];
            $loan_payment_item_id = $visit[$r];
            //check if card is held
            $service = array(
                      'loan_payment_id'=>$loan_payment_id,
                      'created' =>$payment_date,
                      'loan_payment_item_status'=>1,
                      'year'=>$year,
                      'month'=>$month,
                    );
            $this->db->where('loan_payment_item_id',$loan_payment_item_id);
            $this->db->update('loan_payment_item',$service);
          }
        }



        return TRUE;
      }
    }

    
  }

  public function create_credit_payment_number()
  {
    //select product code
    $this->db->where('loan_payment_id > 0');
    $this->db->from('loan_payment');
    $this->db->select('MAX(document_number) AS number');
    $this->db->order_by('loan_payment_id','DESC');
    // $this->db->limit(1);
    $query = $this->db->get();
    // var_dump($query); die();
    if($query->num_rows() > 0)
    {
      $result = $query->result();
      $number =  $result[0]->number;
      // var_dump($number);die();
      $number++;

    }
    else{
      $number = 1;
    }
    // var_dump($number);die();
    return $number;
  }




  /*
  * Add a new loan
  *
  */
  public function add_loan()
  {
    $loan_type_id = $this->input->post('loan_type_id');

    if(isset($loan_type_id))
    {
      $loan_type_id = 1;
    }
    else
    {
      $loan_type_id = 0;
    }
    $data = array(
      'loan_name'=>$this->input->post('loan_name'),
      'loan_email'=>$this->input->post('loan_email'),
      'loan_phone'=>$this->input->post('loan_phone'),
      'loan_location'=>$this->input->post('loan_location'),
      'loan_building'=>$this->input->post('loan_building'),
      'loan_floor'=>$this->input->post('loan_floor'),
      'loan_address'=>$this->input->post('loan_address'),
      'loan_post_code'=>$this->input->post('loan_post_code'),
      'loan_city'=>$this->input->post('loan_city'),
      'opening_balance'=>$this->input->post('opening_balance'),
      'start_date'=>$this->input->post('loan_account_date'),
      'loan_contact_person_name'=>$this->input->post('loan_contact_person_name'),
      'loan_contact_person_onames'=>$this->input->post('loan_contact_person_onames'),
      'loan_contact_person_phone1'=>$this->input->post('loan_contact_person_phone1'),
      'loan_contact_person_phone2'=>$this->input->post('loan_contact_person_phone2'),
      'loan_contact_person_email'=>$this->input->post('loan_contact_person_email'),
      'loan_description'=>$this->input->post('loan_description'),
      'account_from_id'=>$this->input->post('account_from_id'),
      'account_to_id'=>$this->input->post('account_to_id'),
      'branch_code'=>$this->session->userdata('branch_code'),
      'created_by'=>$this->session->userdata('loan_id'),
      'debit_id'=>$this->input->post('debit_id'),
      'modified_by'=>$this->session->userdata('loan_id'),
      'loan_type_id'=>$loan_type_id,
      'created'=>date('Y-m-d H:i:s')
    );

    if($this->db->insert('loan', $data))
    {
      return $this->db->insert_id();
    }
    else{
      return FALSE;
    }
  }

  /*
  * Update an existing loan
  * @param string $image_name
  * @param int $loan_id
  *
  */
  public function edit_loan($loan_id)
  {
    $data = array(
      'loan_name'=>$this->input->post('loan_name'),
      'loan_email'=>$this->input->post('loan_email'),
      'loan_phone'=>$this->input->post('loan_phone'),
      'loan_location'=>$this->input->post('loan_location'),
      'loan_building'=>$this->input->post('loan_building'),
      'loan_floor'=>$this->input->post('loan_floor'),
      'loan_address'=>$this->input->post('loan_address'),
      'loan_post_code'=>$this->input->post('loan_post_code'),
      'loan_city'=>$this->input->post('loan_city'),
      'opening_balance'=>$this->input->post('opening_balance'),
      'start_date'=>$this->input->post('loan_account_date'),
      'account_from_id'=>$this->input->post('account_from_id'),
      'account_to_id'=>$this->input->post('account_to_id'),
      'loan_contact_person_name'=>$this->input->post('loan_contact_person_name'),
      'loan_contact_person_onames'=>$this->input->post('loan_contact_person_onames'),
      'loan_contact_person_phone1'=>$this->input->post('loan_contact_person_phone1'),
      'loan_contact_person_phone2'=>$this->input->post('loan_contact_person_phone2'),
      'loan_contact_person_email'=>$this->input->post('loan_contact_person_email'),
      'loan_description'=>$this->input->post('loan_description'),
      'debit_id'=>$this->input->post('debit_id'),
      'modified_by'=>$this->session->userdata('loan_id'),
    );

    $this->db->where('loan_id', $loan_id);
    if($this->db->update('loan', $data))
    {
      return TRUE;
    }
    else{
      return FALSE;
    }
  }


  /*
  * get a single loan's details
  * @param int $loan_id
  *
  */
  public function get_loan_account($loan_id)
  {
    //retrieve all users
    $this->db->from('v_general_ledger');
    $this->db->select('SUM(dr_amount) AS total_invoice_amount');
    $this->db->where('transactionClassification = "loans Invoices" AND recepientId = '.$loan_id);
    $query = $this->db->get();
    $invoices = $query->row();

    $total_invoice_amount = $invoices->total_invoice_amount;


    $this->db->from('v_general_ledger');
    $this->db->select('SUM(cr_amount) AS total_paid_amount');
    $this->db->where('transactionClassification = "loans Invoices Payments" AND recepientId = '.$loan_id);
    $query = $this->db->get();
    $payments = $query->row();

    $total_paid_amount = $payments->total_paid_amount;


    $response['total_invoice'] = $total_invoice_amount;
    $response['total_paid_amount'] = $total_paid_amount;
    $response['total_credit_note'] = 0;

    return $response;
  }



   /*
  * Retrieve all loan
  * @param string $table
  *   @param string $where
  *
  */
  public function get_all_loans_details($table, $where, $per_page, $page, $order = 'loan_name', $order_method = 'ASC')
  {
    //retrieve all users
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    // $this->db->group_by('loan_invoice.loan_invoice_id');
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }


  public function get_loan_invoice_details($loan_invoice_id)
  {

      $this->db->from('loan_invoice,loan_invoice_item');
      $this->db->select('*');
      $this->db->where('loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id AND loan_invoice.loan_invoice_id = '.$loan_invoice_id);
      $query = $this->db->get();
      return $query;
  }

  public function get_loan_payment_details($loan_payment_id)
  {

      $this->db->from('loan_payment');
      $this->db->select('*');
      $this->db->where('loan_payment_id = '.$loan_payment_id);
      $query = $this->db->get();
      return $query;
  }

  public function check_on_account($loan_payment_id)
  {

     $this->db->from('loan_payment_item');
      $this->db->select('*');
      $this->db->where('invoice_type = 3 AND loan_payment_id = '.$loan_payment_id);
      $query = $this->db->get();
      if($query->num_rows() > 0)
      {
          return TRUE;
      }
      else
      {
        return FALSE;
      }


  }

  public function get_loan_credit_note_details($loan_credit_note_id)
  {

      $this->db->from('loan_credit_note');
      $this->db->select('*');
      $this->db->where('loan_credit_note_id = '.$loan_credit_note_id);
      $query = $this->db->get();
      return $query;
  }
  
  public function get_content($table, $where,$select,$group_by=NULL,$limit=NULL)
  {
    $this->db->from($table);
    $this->db->select($select);
    $this->db->where($where);
    if($group_by != NULL)
    {
      $this->db->group_by($group_by);
    }
    $query = $this->db->get('');
    
    return $query;
  }

   public function export_loans()
  {
    $this->load->library('excel');
    

    $income_rs = $this->company_financial_model->get_loans_aging_report();
    
    
    $title = 'loans List';

    $income_result = '';
    $total_income = 0;
    $total_income = 0;
    $total_thirty = 0;
    $total_sixty = 0;
    $total_ninety = 0;
    $total_over_ninety = 0;
    $total_coming_due = 0;
    $grand_total = 0;
    $total_unallocated = 0;
    $grand_dr =0;
    $grand_cr = 0;
    
    if($income_rs->num_rows() > 0)
    {
      $count = 0;
      /*
        -----------------------------------------------------------------------------------------
        Document Header
        -----------------------------------------------------------------------------------------
      */


      $row_count = 0;
      $report[$row_count][0] = '#';
      $report[$row_count][1] = 'Doctor';
      $report[$row_count][2] = 'Coming Due';
      $report[$row_count][3] = '1 - 30 Days';
      $report[$row_count][4] = '31 - 60 Days';
      $report[$row_count][5] = '61 - 90 Days';
      $report[$row_count][6] = 'Over 90 Days';
      $report[$row_count][7] = 'Unallocated Funds';
      $report[$row_count][8] = 'Total Debits';
      $report[$row_count][9] = 'Total Credits';
      $report[$row_count][10] = 'Balance';
      //get & display all services
      
      //display all patient data in the leftmost columns
      foreach($income_rs->result() as $value)
      {
        $row_count++;
        $total_invoiced = 0;
        # code...
        // $total_amount = $value->total_amount;
        $payables = $value->payables;
        $thirty_days = $value->thirty_days;
        $sixty_days = $value->sixty_days;
        $ninety_days = $value->ninety_days;
        $over_ninety_days = $value->over_ninety_days;
        $coming_due = $value->coming_due;
        $loan_id = $value->recepientId;
        $Total = $value->Total;
        $total_dr = $value->total_dr;
        $total_cr = $value->total_cr;

        $unallocated = $this->company_financial_model->get_unallocated_funds($loan_id);
        $total_thirty += $thirty_days;
        $total_sixty += $sixty_days;
        $total_ninety += $ninety_days;
        $total_over_ninety += $over_ninety_days;
        $total_coming_due += $coming_due;
        $grand_total += $Total;
        $total_unallocated += $unallocated;
        $grand_dr += $total_dr;
        $grand_cr += $total_cr;

        $balance = $total_dr - $total_cr;
        $Total = $balance;
        $grand_total += $Total;


        $count++;
        
        //display the patient data
        $report[$row_count][0] = $count;
        $report[$row_count][1] = $payables;
        $report[$row_count][2] = number_format($coming_due,2);
        $report[$row_count][3] = number_format($thirty_days,2);
        $report[$row_count][4] = number_format($sixty_days,2);
        $report[$row_count][5] = number_format($ninety_days,2);
        $report[$row_count][6] = number_format($over_ninety_days,2);
        $report[$row_count][7] = number_format($unallocated,2);
        $report[$row_count][8] = number_format($total_dr,2);
        $report[$row_count][9] = number_format($total_cr,2);
        $report[$row_count][10] = number_format($balance,2);
          
        
        
      }
    }
    
    //create the excel document
    $this->excel->addArray ( $report );
    $this->excel->generateXML ($title);
  }

  public function get_loans_aging_report()
  {
    //retrieve all users
    $provider_search =  $this->session->userdata('provider_search');

    if(!empty($provider_search))
    {
      $where = $provider_search;
    }
    else
    {
      $where = 'recepientId > 0';
    }
    $this->db->from('v_aged_providers');
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by('payables','ASC');
    $query = $this->db->get();

    return $query;
  }


   public function get_all_loans_items()
  {
    //retrieve all users
    $provider_search =  $this->session->userdata('provider_search');

    if(!empty($provider_search))
    {
      $where = $provider_search;
    }
    else
    {
      $where = 'loan_id > 0';
    }
    $this->db->from('loan');
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by('loan_name','ASC');
    $query = $this->db->get();

    return $query;
  }


  public function get_unallocated_loans_funds($creditor_id)
  {
    $selected_statement = "
                  SELECT 
                      SUM(cr_amount) AS cr_amount

                  FROM
                  (
                  SELECT
                  `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                  `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                  `creditor_payment`.`reference_number` AS `referenceCode`,
                  `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                  `creditor_payment`.`document_number` AS `transactionCode`,
                  '' AS `patient_id`,
                  `creditor_payment`.`creditor_id` AS `recepientId`,
                  `account`.`parent_account` AS `accountParentId`,
                  `account_type`.`account_type_name` AS `accountsclassfication`,
                  `creditor_payment`.`account_from_id` AS `accountId`,
                  `account`.`account_name` AS `accountName`,
                  `creditor_payment_item`.`description` AS `transactionName`,
                  CONCAT('Payment on account')  AS `transactionDescription`,
                      0 AS `department_id`,
                  0 AS `dr_amount`,
                  `creditor_payment_item`.`amount_paid` AS `cr_amount`,
                  `creditor_payment`.`transaction_date` AS `transactionDate`,
                  `creditor_payment`.`created` AS `createdAt`,
                  `creditor_payment`.`created` AS `referenceDate`,
                  `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                  'Expense Payment' AS `transactionCategory`,
                  'Creditors Invoices Payments' AS `transactionClassification`,
                  'creditor_payment' AS `transactionTable`,
                  'creditor_payment_item' AS `referenceTable`
                FROM
                  (
                    (
                      (
                        `creditor_payment_item`
                        JOIN `creditor_payment` ON(
                          (
                            creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
                          )
                        )
                      )
                      JOIN account ON(
                        (
                          account.account_id = creditor_payment.account_from_id
                        )
                      )
                    )
                    JOIN `account_type` ON(
                      (
                        account_type.account_type_id = account.account_type_id
                      )
                    )
                    JOIN `creditor` ON(
                      (
                        creditor.creditor_id = creditor_payment_item.creditor_id
                      )
                    )
                  )
                  WHERE creditor_payment_item.invoice_type = 3) AS data WHERE data.recepientId = ".$creditor_id;
          $query = $this->db->query($selected_statement);
          $checked = $query->row();

          $dr_amount = $checked->cr_amount;

          return $dr_amount;
    }

}
?>
