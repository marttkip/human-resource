<?php //echo $this->load->view('search/search_loan_account', '', TRUE);?>

<?php

$income_rs = $this->loans_model->get_all_loans_items();
$income_result = '';
$total_income = 0;
$total_income = 0;
$total_thirty = 0;
$total_sixty = 0;
$total_ninety = 0;
$total_over_ninety = 0;
$total_coming_due = 0;
$grand_total = 0;
$total_unallocated = 0;
$grand_dr =0;
$grand_cr = 0;
if($income_rs->num_rows() > 0)
{
	foreach ($income_rs->result() as $key => $value) {
		# code...
		// $total_amount = $value->total_amount;
		$loan_name = $value->loan_name;
		$start_date = $value->start_date;
		$account_from_id = $value->account_from_id;
		$account_to_id = $value->account_to_id;
		$loan_id = $value->loan_id;

		$account_from_name = $this->company_financial_model->get_account_name($account_from_id);
		$account_to_name = $this->company_financial_model->get_account_name($account_to_id);

		$loan_amount = 0;
		$dr_amount = 0;
		$cr_amount = 0;
		$interest_payment = 0;
		$principle_payment = 0;
		$account_rs = $this->company_financial_model->get_loan_statement_units($loan_id);

		if($account_rs->num_rows() > 0)
		{
			foreach ($account_rs->result() as $key => $value) {
				// code...
				$loan_amount = $value->loan_amount;
				$dr_amount = $value->dr_amount;
				$cr_amount = $value->cr_amount;
				$interest_payment = $value->interest_payment;
				$principle_payment = $value->principle_payment;
			}
		}

		if($loan_id == 2){
			// var_dump($loan_amount);die();
		}

		$loan_balance = $loan_amount - $principle_payment;
		$total_coming_due += $loan_amount;
		$total_thirty += $loan_balance;
		$total_sixty += $interest_payment;

		$income_result .='<tr>
							<td class="text-left">'.strtoupper($loan_name).'</td>
							<td class="text-left">'.date('jS M Y',strtotime($start_date)).'</td>
							<td class="text-left">'.strtoupper($account_to_name).'</td>
							<td class="text-left">'.strtoupper($account_from_name).'</td>
							<td class="text-right">'.number_format($loan_amount,2).'</td>
							<td class="text-right">'.number_format($loan_balance,2).'</td>
							<td class="text-right">'.number_format($interest_payment,2).'</td>
							<td class="text-right"><a href="'.site_url().'loan-statement/'.$loan_id.'" class="btn btn-warning btn-xs"> Open Account</a></td>
							<td class="text-right"><a href="'.site_url().'finance/edit-loan/'.$loan_id.'" class="btn btn-success btn-xs"> Edit Account</a></td>
							<td class="text-right"><a href="'.site_url().'finance/delete-loan/'.$loan_id.'" class="btn btn-danger btn-xs" onclick="return confirm(\'Are you sure you want to remove this loan ? \')"> Delete</a></td>
							</tr>';
	}

	$income_result .='<tr>
						<th class="text-left" colspan="4">TOTALS</th>
						<th class="text-right">'.number_format($total_coming_due,2).'</th>
						<th class="text-right">'.number_format($total_thirty,2).'</th>
						<th class="text-right">'.number_format($total_sixty,2).'</th>
					</tr>';

}
?>

<section class="panel">
		<header class="panel-heading">
				<h3 class="panel-title">Loans </h3>
				<div class="widget-tools pull-right" style="margin-top: -25px;">
					<a href="<?php echo site_url();?>finance/export-loans" class="btn btn-sm btn-info" target="_blank" ><i class="fa fa-plus"></i> Export loans Report</a>
                    <a href="<?php echo site_url();?>finance/add-loan" class="btn btn-sm btn-primary" ><i class="fa fa-plus"></i> Add loan</a>
                </div>
		</header>
		<div class="panel-body">
		<?php
      		$search = $this->session->userdata('loan_search');
			if(!empty($search))
			{
				?>
                <a href="<?php echo base_url().'finance/loans/close_loan_loan_search';?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Close Search</a>
                <?php
			}
			?>
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th class="text-left">Loan</th>
					<th class="text-left">Date</th>
					<th class="text-left">Account Debit</th>
					<th class="text-left">Account Credit</th>
					<th class="text-right">Loan Amount</th>
					<th class="text-right">Principal Balance</th>
					<th class="text-right">Interest Repaid</th>
					<th class="text-right">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php echo $income_result?>
				<!-- <tr>
        			<td >Web Development</td>
					<td class="text-right">200,000.00</td>
					<td class="text-right">200,000.00</td>
					<td class="text-right">200,000.00</td>
					<td class="text-right">200,000.00</td>
					<td class="text-right">200,000.00</td>
					<td class="text-right">200,000.00</td>
				</tr> -->

			</tbody>
		</table>
    </div>
</section>
