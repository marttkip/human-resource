<?php
//loan data
$loan_name = set_value('loan_name');
$loan_email = set_value('loan_email');
$loan_phone = set_value('loan_phone');
$loan_location = set_value('loan_location');
$loan_building = set_value('loan_building');
$loan_floor = set_value('loan_floor');
$loan_address = set_value('loan_address');
$loan_post_code = set_value('loan_post_code');
$loan_city = set_value('loan_city');
$start_date = set_value('start_date');
$loan_contact_person_name = set_value('loan_contact_person_name');
$loan_contact_person_onames = set_value('loan_contact_person_onames');
$loan_contact_person_phone1 = set_value('loan_contact_person_phone1');
$loan_contact_person_phone2 = set_value('loan_contact_person_phone2');
$loan_contact_person_email = set_value('loan_contact_person_email');
$loan_description = set_value('loan_description');
$opening_balance  = set_value('opening_balance');
$account_from_id  = set_value('account_from_id');
$account_to_id  = set_value('account_to_id');
$balance_brought_forward = set_value('balance_brought_forward');
?>
           <section class="panel">
                <header class="panel-heading">
                    <h3 class="panel-title"><?php echo $title;?> </h3>
                    <div class="box-tools pull-right">
                        <a href="<?php echo site_url();?>accounting/loans" class="btn btn-sm btn-primary" ><i class="fa fa-arrow-left"></i> Back to loan bills</a>
                    </div>
                </header>

              <div class="panel-body">

                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');

						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';

							$this->session->unset_userdata('success_message');
						}

						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';

							$this->session->unset_userdata('error_message');
						}

						$validation_errors = validation_errors();

						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}

						$validation_errors = validation_errors();

						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>

                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="col-md-12">
	<div class="col-md-6">

        <div class="form-group">
            <label class="col-lg-5 control-label">Loan Name: </label>

            <div class="col-lg-7">
            	<input type="text" class="form-control" name="loan_name" placeholder="Loan Name" value="<?php echo $loan_name;?>">
            </div>
        </div>

       
      <div class="form-group">
        <label class="col-md-5 control-label">Loan Date: </label>

        <div class="col-md-7">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </span>
                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="loan_account_date" placeholder="Transaction date" id="datepicker" value="<?php echo $start_date;?>" autocomplete="off">
            </div>
        </div>
      </div>
        

	</div>

    <div class="col-md-6">

        <div class="form-group">
            <label class="col-lg-5 control-label">Account From: </label>
            <div class="col-lg-7">
            	 <select id="account_from_id" name="account_from_id" class="form-control" required>
                                    
                    <?php
                      $changed = '<option value="">--- Account ---</option>';
                      $accounts = $this->purchases_model->get_all_accounts();
                     if($accounts->num_rows() > 0)
                     {
                         foreach($accounts->result() as $row):
                             // $company_name = $row->company_name;
                             $account_name = $row->account_name;
                             $account_id = $row->account_id;
                             $parent_account = $row->parent_account;

                             if($parent_account != $current_parent)
                             {
                                $account_from_name = $this->transfer_model->get_account_name($parent_account);
                              $changed .= '<optgroup label="'.$account_from_name.'">';
                             }

                             $changed .= "<option value=".$account_id."> ".$account_name."</option>";
                             $current_parent = $parent_account;
                             if($parent_account != $current_parent)
                             {
                              $changed .= '</optgroup>';
                             }

                           
                          
                         endforeach;
                     }
                     echo $changed;
                     ?>
                      
      
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Account To: </label>
            <div class="col-lg-7">
            	 <select id="account_to_id" name="account_to_id" class="form-control" required>
                                    
                    <?php
                      $changed = '<option value="">--- Account ---</option>';
                      $accounts = $this->purchases_model->get_all_accounts();
                     if($accounts->num_rows() > 0)
                     {
                         foreach($accounts->result() as $row):
                             // $company_name = $row->company_name;
                             $account_name = $row->account_name;
                             $account_id = $row->account_id;
                             $parent_account = $row->parent_account;

                             if($parent_account != $current_parent)
                             {
                                $account_from_name = $this->transfer_model->get_account_name($parent_account);
                              $changed .= '<optgroup label="'.$account_from_name.'">';
                             }

                             $changed .= "<option value=".$account_id."> ".$account_name."</option>";
                             $current_parent = $parent_account;
                             if($parent_account != $current_parent)
                             {
                              $changed .= '</optgroup>';
                             }

                           
                          
                         endforeach;
                     }
                     echo $changed;
                     ?>
                      
      
                </select>
            </div>
        </div>


    </div>
</div>

<div class="col-md-12" style="margin-top:10px;margin-bottom:10px;">
	<div class="col-md-12">

        <div class="form-group">
            <label class="col-lg-2 control-label">Description: </label>

            <div class="col-lg-9">
            	<textarea class="form-control" name="loan_description" rows="5"><?php echo $loan_phone;?></textarea>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions text-center">
            <button class="submit btn btn-primary" type="submit">
                Add Loan Detail
            </button>
        </div>
    </div>
</div>
    <?php echo form_close();?>
</div>
</section>
