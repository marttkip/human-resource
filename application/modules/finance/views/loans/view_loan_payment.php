<?php
	  $invoice_where = 'loan_payment_id = '.$loan_payment_id;
      $invoice_table = 'loan_payment_item';
      $invoice_order = 'loan_payment_item_id';

      $invoice_query = $this->loans_model->get_loans_list($invoice_table, $invoice_where, $invoice_order);

      $result_payment ='<table class="table table-bordered table-striped table-condensed">
                          <thead>
                            <tr>
                              <th >#</th>
                              <th >Type</th>
                              <th >Invoice Number</th>
                              <th >Amount Paid</th>
                            </tr>
                          </thead>
                            <tbody>';
      $total_amount = 0;
      $total_vat_amount = 0;
      if($invoice_query->num_rows() > 0)
      {
        $x = 0;

        foreach ($invoice_query->result() as $key => $value) {
          // code...
          $loan_payment_item_id = $value->loan_payment_item_id;
          $invoice_type = $value->invoice_type;
          $loan_id = $value->loan_id;

          if($invoice_type == 0)
          {
            $type = "loan Bill";
            // loan invoice
            $loan_invoice_id = $value->loan_invoice_id;
            $invoice_where = 'loan_invoice.loan_id = '.$loan_id.' AND loan_invoice_id = '.$loan_invoice_id;
            $invoice_table = 'loan_invoice';
            $invoice_order = 'loan_invoice_id';

            $invoice_items = $this->loans_model->get_loans_list($invoice_table, $invoice_where, $invoice_order);
            $invoice_things = $invoice_items->row();

            $account_name = $invoice_things->invoice_number;
          }
          else if($invoice_type == 1)
          {
            $type = "Supplies Invoice";
              // loan invoice
              $loan_invoice_id = $value->loan_invoice_id;
              $invoice_where = 'orders.supplier_id = '.$loan_id.' AND order_id = '.$loan_invoice_id;
              $invoice_table = 'orders';
              $invoice_order = 'order_id';

              $invoice_items = $this->loans_model->get_loans_list($invoice_table, $invoice_where, $invoice_order);
              $invoice_things = $invoice_items->row();
              $account_name = $invoice_things->supplier_invoice_number;

          }

          else if($invoice_type == 2)
          {
            $type = "On opening balance";
              // loan invoice
              $loan_invoice_id = $value->loan_invoice_id;

              $account_name = '';

          }

          else if($invoice_type == 3)
          {
            $type = "On account";
              // loan invoice
              $loan_invoice_id = $value->loan_invoice_id;

              $account_name = '';

          }
          $amount = $value->amount_paid;
          $total_amount += $amount;
          $checkbox_data = array(
                    'name'        => 'loan_payments_items[]',
                    'id'          => 'checkbox'.$loan_payment_item_id,
                    'class'          => 'css-checkbox  lrg ',
                    'checked'=>'checked',
                    'value'       => $loan_payment_item_id
                  );

          $x++;
          $result_payment .= '<tr>
                                  <td>'.$x.'</td>
                                  <td>'.$type.'</td>
                                  <td>'.$account_name.'</td>
                                  <td>'.number_format($amount,2).'</td>
                              </tr>';
        }

        // display button

        $display = TRUE;
      }
      else {
        $display = FALSE;
      }

      $result_payment .='</tbody>
                      </table>';
      ?>

      <?php echo $result_payment;?>