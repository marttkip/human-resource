<?php

$lender = $this->lenders_model->get_lender($lender_id);
// $lender = $lender->row();

//patient details

// var_dump($lender);die();


foreach ($lender->result() as $key => $value) {
	// code...
	$lender_name = $value->lender_name;
	$lender_address = $value->lender_address;
	$lender_phone = $value->lender_phone;

}



$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));

//doctor
//served by
$served_by = '';//$this->accounts_model->get_personnel($this->session->userdata('personnel_id'));


// var_dump($lender_id);die();

$lender_invoice_query = $this->lenders_model->get_lender_invoice_details($lender_invoice_id);

if($lender_invoice_query->num_rows() > 0)
{
	foreach ($lender_invoice_query->result() as $key => $value) {
		// code...
		$lender_invoice_id = $value->lender_invoice_id;
		$lender_invoice_number = $value->invoice_number;
		$lender_invoice_date = $value->transaction_date;

	}
}


 $invoice_where = 'lender_invoice_item.lender_invoice_id = '.$lender_invoice_id.' AND lender_invoice_item_status = 1 AND account.account_id = lender_invoice_item.account_to_id';
$invoice_table = 'lender_invoice_item,account';
$invoice_order = 'lender_invoice_item_id';

$invoice_query = $this->lenders_model->get_lenders_list($invoice_table, $invoice_where, $invoice_order);
// var_dump($invoice_query);die();

$total_amount = 0;
$total_vat_amount = 0;
$item_list = '';
// var_dump($lender_invoice_date); die();




$configuration_rs = $this->site_model->get_company_configuration();

$configuration_array = array();
foreach ($configuration_rs->result() as $key) {
	// code...
	$configuration_array[$key->company_configuration_name] = $key;
}




$payment_method = $configuration_array['payment_method'];
if(isset($configuration_array['payment_method']))
{
	$payment_method = $configuration_array['payment_method']->company_configuration_particulars;
}
else
{
	$payment_method = '';
}


$paybill = $configuration_array['paybill'];
if(isset($configuration_array['paybill']))
{
	$paybill = $configuration_array['paybill']->company_configuration_particulars;
}
else
{
	$paybill = '';
}


$company_billing_name = $configuration_array['company_billing_name'];
if(isset($configuration_array['company_billing_name']))
{
	$company_billing_name = $configuration_array['company_billing_name']->company_configuration_particulars;
}
else
{
	$company_billing_name = '';
}


$bank = $configuration_array['bank'];
$account_number = '';
if(isset($configuration_array['bank']))
{
	$bank_name = $configuration_array['bank']->company_configuration_status;
	$account_number = $configuration_array['bank']->company_configuration_particulars;
}
else
{
	$bank = '';
}



?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Invoice</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
        	body
        	{
        		font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
        	}
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid;}

			.col-md-6 {
			    width: 50%;
			 }

			h3
			{
				font-size: 30px;
			}
			p
			{
				margin: 0 0 0px !important;
			}

			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{ margin:0 auto;}
			.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}

			table.table{
			    border-spacing: 0 !important;
			    font-size:12px;
				margin-top:0px;
				margin-bottom: 10px !important;
				border-collapse: inherit !important;
				font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
			}
			th, td {
			    border: 1px solid #000 !important;
			    padding: 0.5em 1em !important;
			}
			/* the first 'th' within the first 'tr' of the 'thead': */
			thead tr:first-child th:first-child {
			    border-radius: 10px 0 0 0 !important;
			}
			/* the last 'th' within the first 'tr' of the 'thead': */
			thead tr:first-child th:last-child {
			    border-radius: 0 10px 0 0 !important;
			}
			/* the first 'td' within the last 'tr' of the 'tbody': */
			tbody tr:last-child td:first-child {
			    border-radius: 0 0 0 10px !important;
			}
			/* the last 'td' within the last 'tr' of the 'tbody': */
			tbody tr:last-child td:last-child {
			    border-radius: 0 0 10px 10px !important;
			}
			tbody tr:first-child td:first-child {
			    border-radius: 10px 10px 0 0 !important;
			    border-bottom: 0px solid #000 !important;
			}
			.padd
			{
				padding:10px;
			}

		</style>
    </head>
    <body onLoad="window.print();return false;">



    	<div class="padd " >
    		<div class="col-print-12">
	    		<div class="col-print-6" style="margin-bottom: 10px;">
	            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive" style="margin-bottom: 10px;height:80px !important;" />
	            	<!-- <p style="font-size: 11px !important;">Dr. Lorna Sande | Dr. Kinoti M. Dental Surgeons</p> -->

	            	<!-- <p style="font-size: 12px;">Pin. PO51455684R </p> -->
	            </div>
	            <div class="col-print-4" >
	            	<p style="font-size:10px !important;text-align: left;">

	                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
	                    Address :<strong> P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></strong> <br/>
	                    Office Line: <strong> <?php echo $contacts['phone'];?></strong> <br/>
	                    </strong> EMail: <strong><?php echo $contacts['email'];?>.</strong><br/>

	                </p>

	            </div>
	            <div class="col-print-2">
	            	<div  class="center-align">

		                 <h4 ><strong>Invoice</strong></h4>
		            </div>
	            </div>
	        	<!-- -->
	        </div>


	      	<div class="row" >
	      		<div class="col-md-12">
	      			<div class="col-print-5 left-align">
	      				<p style="font-size: 12px;">FOR PROFESSIONAL SERVICES RENDERED </p>
		            	<table class="table">
		            		<tr>
		            			<td><strong>Bill To</strong></td>
		            		</tr>
		            		<tr>

		            			<td>

		            				<?php echo $lender_name; ?> <br>
		            				<?php echo $lender_address.' '.$lender_phone; ?>

		            			</td>
		            		</tr>
		            	</table>
		            </div>
		            <div class="col-print-1">
		            	&nbsp;
		            </div>
		            <div class="col-print-6">
		            	<p style="font-size: 12px;"> &nbsp;</p>
		            	<table class="table">
		            		<tr>
		            			<td>
		            				<span class="pull-left">Date:</span>
		            				<span class="pull-right"> <strong><?php echo $lender_invoice_date; ?> </strong></span>
		            			</td>
		            		</tr>
		            		<tr>

		            			<td style="border-top: 0px solid #000 !important;">

		            					<span class="pull-left"><strong>Invoice No.</strong> </span>
		            					<span class="pull-right" ><strong><?php echo $lender_invoice_number; ?> </strong> </span>


		            			</td>
		            		</tr>
		            	</table>
		            </div>

	      		</div>

	        </div>

	      	<div class="row">
	      		<div class="col-md-12">
	      			<table class="table">
		            		<tr>
		            			<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important; width: 10%;">
			            			<span class="pull-left">Item </span>

			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important; width: 60%;">
			            			<span class="pull-left">Description </span>

			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important;  width: 10%;">
			            			<span class="pull-left">Qty </span>

			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important;  width: 10%;">
			            			<span class="pull-left">Rate </span>

			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 10px !important;border-bottom: 0px solid #000 !important;  width: 10%;">
			            			<span class="pull-left">Amount </span>

			            		</td>
		            		</tr>

		            		<?php
                                $total = 0;
                                $sub_total = 0;


                                // var_dump($item_invoiced_rs);die();
								$total_amount= 0;
								$days = 0;
								$count = 0;
								$item_list = '';
								$description_list = "";
								$quantity_list = "";
								$rate_list = "";
								$amount_list = "";


								if($invoice_query->num_rows() > 0)
								{
									$x = 0;

									foreach ($invoice_query->result() as $key => $value) {
									  // code...
									  $account_name = $value->account_name;
									  $unit_price = $value->unit_price;
									  $lender_invoice_item_id = $value->lender_invoice_item_id;
									  $quantity = $value->quantity;
									  $account_name = $value->account_name;
									  $item_description = $value->item_description;
									  $vat_type_id = $value->vat_type_id;
									  $vat_amount = $value->vat_amount;
									   $lender_id = $value->lender_id;
									  $amount = $value->total_amount;
									  $total_amount += $amount;
									  $total_vat_amount += $vat_amount;
									  if($vat_type_id == 0)
									  {
									    $vat = 'No VAT';
									  }
									  else if($vat_type_id == 1)
									  {
									    $vat = '16 % VAT';
									  }

									  else if($vat_type_id == 2)
									  {
									    $vat = '5 % Withholding TAX';
									  }
									  $checkbox_data = array(
									            'name'        => 'lender_invoice_items[]',
									            'id'          => 'checkbox'.$lender_invoice_item_id,
									            'class'          => 'css-checkbox  lrg ',
									            'checked'=>'checked',
									            'value'       => $lender_invoice_item_id
									          );

									  $x++;
									  $sub_total= $sub_total +($quantity * $unit_price);

									  $item_list .= $account_name.'<br> ';
										$description_list .= $item_description."<br>";
										$quantity_list .= $quantity."<br>";
										$rate_list .= number_format($unit_price,2)."<br>";
										$amount_list .= number_format($quantity * $unit_price,2)."<br>";


									}
									// $total_amount = $total_amount + $sub_total;
								}





                              ?>
		            		<tr>
		            			<td  style="border-radius: 10px 10px 0px 10px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important; height: 200px !important;">
			            			<span class="pull-left"> <?php echo $item_list;?> </span>

			            		</td>
			            		<td  style="border-radius: 10px 10px 0px 0px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important;height: 200px !important;">
			            			<span class="pull-left">  <?php echo $description_list;?>  </span>

			            		</td>
			            		<td  style="border-radius: 10px 10px 0px 0px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important;height: 200px !important;">
			            			<span class="pull-left"> <?php echo $quantity_list;?>  </span>

			            		</td>
			            		<td  style="border-radius: 10px 10px 0px 0px !important;border-bottom: 1px solid #000 !important;border-right: 0px solid #000 !important;height: 200px !important;">
			            			<span class="pull-left"> <?php echo $rate_list;?> </span>

			            		</td>
			            		<td  style="border-radius: 10px 10px 10px 0px !important;border-bottom: 1px solid #000 !important; height: 200px !important;">
			            			<span class="pull-left"> <?php echo $amount_list;?> </span>

			            		</td>
		            		</tr>

		            		<tr>
		            			<td style="border:none !important;"></td>
		            			<td  style="border:none !important;"></td>
		            			<td colspan="3"  style="border:none !important; padding:0px !important;">
		            				<table class="table" style="margin:none !important; padding: none !important;margin-top:0px !important;">
					            		<tr>
					            			<td style="border-radius: 10px 0px 0 0 !important;border-right: 0px solid #000 !important;border-top: 0px solid #000 !important;">
						            			<span class="pull-left"><strong>Total</strong> </span>
						            		</td>
						            		<td style="border-radius: 0px 10px 0 0 !important;border-bottom: 0px solid #000 !important;border-top: 0px solid #000 !important;">
						            			<span class="pull-right" >KES <?php echo number_format($sub_total,2); ?>  </span>
						            		</td>
					            		</tr>

					            		<tr>
					            			<td style="border-right: 0px solid #000 !important;">
						            			<span class="pull-left"><strong>Balance</strong> </span>
						            		</td>
						            		<td style="border-radius: 0px 0px 10px 0 !important;">
						            			<span class="pull-right" >KES <?php echo number_format($sub_total,2); ?> </span>
						            		</td>
					            		</tr>

					            	</table>
		            			</td>
		            		</tr>



		            	</table>
	      		</div>
	      	</div>

					<div class="col-md-12">
						<div class="col-md-12" style="margin-bottom: 0px !important;font-size: 12px !important;">
							 <p>Bill Payable to : <strong><?php echo $company_billing_name;?> </strong></p>
							 <?php

							 if($paybill)
							 {
							 	?>
							 	 <p>Modes of Payment; M-pesa (Pay Till No. <?php echo $paybill?>)</p>
							 	<?php
							 }
							 ?>
							

							<div >


								<div class="col-print-6">
									 <?php

									 if($bank)
									 {
									 	?>
									<div class="col-print-12">
										<div class="col-print-4">
											Bank Name
										</div>
										<div class="col-print-1" style="width:2% !important;">
											:
										</div>
										<div class="col-print-7 pull-left">
											<?php echo $bank_name?>
										</div>
									</div>
									<!-- <div class="col-print-12">
										<div class="col-print-4">
											Bank Branch
										</div>
										<div class="col-print-1" style="width:2% !important;">
											:
										</div>
										<div class="col-print-7 pull-left">
											Upper Hill
										</div>
									</div> -->
									<div class="col-print-12">
										<div class="col-print-4">
											Account Number
										</div>
										<div class="col-print-1" style="width:2% !important;">
											:
										</div>
										<div class="col-print-7 pull-left">
											<?php echo $account_number?>
										</div>
									</div>
								</div>

								<?php
								}
								?>
							</div>
						</div>
					</div>


	      	<div class="col-md-12">
	      		<div class="col-md-12" style="margin-bottom: 0px !important;font-size: 12px !important;">

	      			<div class="col-print-12">

	      			<div class="row" style="font-style:italic; font-size:11px; margin-top: 20px;">
			        	<div class="col-md-12 pull-left">

			                <div class="col-print-4 pull-left">
			                    Customer Name: <span style="text-decoration: underline;"> <?php echo $lender_name;?> </span>
			                </div>

			                <div class="col-print-4 pull-left">
			                  Customer Signature : ................................
			                </div>
			                <div class="col-print-4 pull-left">
			                  Date by: .....................................
			                </div>

			          	</div>
        				</div>
	      			</div>
	      		</div>
	      	</div>
	    </div>


    </body>

</html>
