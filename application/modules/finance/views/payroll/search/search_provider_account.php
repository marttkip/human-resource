<?php
$date_statutories_batches_from = $this->session->userdata('date_statutories_batches');
$date_statutories_batches_to = $this->session->userdata('date_statutories_batches_to');
$report_type_list =  $this->session->set_userdata('report_statutory_type');
$report_detail = $this->session->userdata('report_detail');
?> 
 <section class="panel">
   
    <!-- Widget content -->
    <div class="panel-body">
        <div class="padd">
            <?php

            echo form_open("finance/statutories/search_statutories", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                 <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Account: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="statutory_name" placeholder="Account" autocomplete="off" >
                        </div>
                    </div>
                    <div class="form-group" style="display:none;">
                        <label class="col-md-4 control-label">Report From : </label>
                        
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date from" value="<?php echo $date_statutories_batches_from?>">
                            </div>
                        </div>
                    </div>
                  
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Report To: </label>
                        
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Date To" value="<?php echo $date_statutories_batches_to?>">
                            </div>
                        </div>
                    </div>
                  
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Report? </label>

                        <?php

                        if($report_type_list == 0)
                        {
                            ?>
                             <div class="col-lg-4">
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" type="radio" name="report_type" value="0" checked="checked">
                                        Date
                                    </label>
                                </div>
                            </div>
                            
                            <div class="col-lg-4">
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" type="radio" name="report_type" value="1">
                                        All
                                    </label>
                                </div>
                            </div>
                            <?php

                        }
                        else
                        {
                             ?>
                             <div class="col-lg-4">
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" type="radio" name="report_type" value="0" >
                                        Date
                                    </label>
                                </div>
                            </div>
                            
                            <div class="col-lg-4">
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" type="radio" name="report_type" value="1" checked="checked">
                                        All
                                    </label>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                       
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Interval (days): </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="visit_type_name" placeholder="Interval Period" value="30" readonly >
                        </div>
                    </div>
                  
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Report Detail : </label>
                        
                        <div class="col-md-8">
                           <select class="form-control" name="interval_limit">

                               
                                <?php
                                $report_detail = $this->session->userdata('report_detail');

                                if($report_detail == 180)
                                {
                                    ?>
                                    <option value="180" selected> 180</option>
                                    <!-- <option value="360" > 360</option> -->
                                     <!-- <option value="720" > 720</option> -->
                                    <?php
                                }
                                
                                ?>
                                

                           </select>
                               
                       
                        </div>
                    </div>
                  
                </div>

                <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
                <div class="col-md-1">
                     <div class="center-align">
                        <button type="submit" class="btn btn-info btn-sm">Search</button>
                    </div>
                </div>
                 
            </div>
     
           
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</section>