<?php echo $this->load->view('search/search_creditor_account', '', TRUE);?>

<?php

				$report_detail = $this->session->userdata('report_detail');
// $income_rs = $this->company_financial_model->get_payables_aging_report();
// $income_result = '';
// $total_income = 0;
// $total_income = 0;
// $total_thirty = 0;
// $total_sixty = 0;
// $total_ninety = 0;
// $total_over_ninety = 0;
// $total_coming_due = 0;
// $grand_total = 0;
// $total_unallocated = 0;
// $grand_dr =0;
// $grand_cr = 0;
// if($income_rs->num_rows() > 0)
// {
// 	foreach ($income_rs->result() as $key => $value) {
// 		# code...
// 		// $total_amount = $value->total_amount;
// 		$payables = $value->payables;
// 		$thirty_days = $value->thirty_days;
// 		$sixty_days = $value->sixty_days;
// 		$ninety_days = $value->ninety_days;
// 		$over_ninety_days = $value->over_ninety_days;
// 		$coming_due = $value->coming_due;
// 		$creditor_id = $value->recepientId;
// 		$Total = $value->Total;
// 		$total_dr = $value->total_dr;
// 		$total_cr = $value->total_cr;

// 		$unallocated = $this->company_financial_model->get_unallocated_funds($creditor_id);
// 		$total_thirty += $thirty_days;
// 		$total_sixty += $sixty_days;
// 		$total_ninety += $ninety_days;
// 		$total_over_ninety += $over_ninety_days;
// 		$total_coming_due += $coming_due;
		
// 		$total_unallocated += $unallocated;
// 		$grand_dr += $total_dr;
// 		$grand_cr += $total_cr;
// 		$balance = $total_dr - $total_cr;
// 		$Total = $balance;
// 		$grand_total += $Total;
// 		$income_result .='<tr>
// 							<td class="text-left">'.strtoupper($payables).'</td>
// 							<td class="text-right">'.number_format($coming_due,2).'</td>
// 							<td class="text-right">'.number_format($thirty_days,2).'</td>
// 							<td class="text-right">'.number_format($sixty_days,2).'</td>
// 							<td class="text-right">'.number_format($ninety_days,2).'</td>
// 							<td class="text-right">'.number_format($over_ninety_days,2).'</td>
// 							<td class="text-right">'.number_format($unallocated,2).'</td>
// 							<td class="text-right">'.number_format($total_dr,2).'</td>
// 							<td class="text-right">('.number_format($total_cr,2).')</td>
// 							<td class="text-right">'.number_format($total_dr-$total_cr,2).'</td>
// 							<td class="text-right"><a href="'.site_url().'creditor-statement/'.$creditor_id.'" class="btn btn-warning btn-xs"> Open Account</a></td>
// 							<td class="text-right"><a href="'.site_url().'finance/edit-creditor/'.$creditor_id.'" class="btn btn-success btn-xs"> Edit Account</a></td>
// 							<td class="text-right"><a href="'.site_url().'finance/delete-creditor/'.$creditor_id.'" class="btn btn-danger btn-xs" onclick="return confirm(\'Are you sure you want to remove this creditor ? \')"> Delete</a></td>
// 							</tr>';
// 	}

// 	$income_result .='<tr>
// 						<th class="text-left">TOTALS</th>
// 						<th class="text-right">'.number_format($total_coming_due,2).'</th>
// 						<th class="text-right">'.number_format($total_thirty,2).'</th>
// 						<th class="text-right">'.number_format($total_sixty,2).'</th>
// 						<th class="text-right">'.number_format($total_ninety,2).'</th>
// 						<th class="text-right">'.number_format($total_over_ninety,2).'</th>
// 						<th class="text-right">'.number_format($total_unallocated,2).'</th>
// 						<th class="text-right">'.number_format($grand_dr,2).'</th>
// 						<th class="text-right">('.number_format($grand_cr,2).')</th>
// 						<th class="text-right">'.number_format($grand_total,2).'</th>
// 					</tr>';

// }
?>

<section class="panel">
		<header class="panel-heading">
				<h3 class="panel-title">Payables </h3>
				<div class="widget-tools pull-right" style="margin-top: -25px;">
					<a href="<?php echo site_url();?>finance/export-payables" class="btn btn-sm btn-info" target="_blank" ><i class="fa fa-plus"></i> Export Payables Report</a>
                    <a href="<?php echo site_url();?>finance/add-creditor" class="btn btn-sm btn-primary" ><i class="fa fa-plus"></i> Add Creditor</a>
                </div>
		</header>
		<div class="panel-body">
		<?php
      		$search = $this->session->userdata('creditors_account_search');
			if(!empty($search))
			{
				?>
                <a href="<?php echo base_url().'finance/creditors/close_creditor_creditor_search';?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Close Search</a>
                <?php
			}
			?>


			<?php
					
					$result = '';

					// $report_detail = $this->session->userdata('report_detail');


					$creditor_query = $this->creditors_model->get_payables_aging($report_config);
					$creditor_array = array();
					if($creditor_query->num_rows() > 0)
					{
						foreach ($creditor_query->result() as $key) {
							// code...
							$creditor_array[$key->recepientId] = $key;
						}
					}

					
					// var_dump($report_detail);die();
					if($report_detail == 720)
					{

							$result .= '
										<table class="table table-hover table-bordered" id ="'.$report_detail.' Report">
										  <thead>
											<tr>
											  <th>#</th>
											  <th>Supplier Name</th>
											  <th>Current</th>
											  <th>30 Days</th>
											  <th>60 Days</th>
											  <th> 90 Days</th>
											  <th>120 Days</th>
											  <th>150 days</th>
											  <th>180 days</th>
											  <th>210 days</th>
											  <th>240 days</th>
											  <th>270 days</th>
											  <th>300 days</th>
											  <th>330 days</th>
											  <th>360 days</th>
											  <th>390 days</th>
											  <th>420 days</th>
											  <th>450 days</th>
											  <th>480 days</th>
											  <th>510 days</th>
											  <th>540 days</th>
											  <th>570 days</th>
											  <th>600 days</th>
											  <th>630 days</th>
											  <th>660 days</th>
											  <th>690 days</th>
											  <th>720 days</th>
											  <th>Over 720 days</th>
											  <th>Unallocated Payments</th>
											  <th>Balance</th>
											</tr>
										  </thead>
										  <tbody>
									';

					}

					else if($report_detail == 180)
					{

							$result .= '
										<table class="table table-hover table-bordered " id ="'.$report_detail.' Report">
										  <thead>
											<tr>
											  <th>#</th>
											  <th>Supplier Name</th>
											  <th>Current</th>
											  <th>30 Days</th>
											  <th>60 Days</th>
											  <th> 90 Days</th>
											  <th>120 Days</th>
											  <th>150 days</th>
											  <th>180 days</th>
											  <th>Over 180 days</th>
											  <th>Unallocated Payments</th>
											  <th>Balance</th>
											</tr>
										  </thead>
										  <tbody>
									';

					}
					else if($report_detail == 360)
					{

							$result .= '
										<table class="table table-hover table-bordered " id ="'.$report_detail.' Report">
										  <thead>
											<tr>
											  <th>#</th>
											  <th>Supplier Name</th>
											  <th>Current</th>
											  <th>30 Days</th>
											  <th>60 Days</th>
											  <th> 90 Days</th>
											  <th>120 Days</th>
											  <th>150 days</th>
											  <th>180 days</th>
											  <th>210 days</th>
											  <th>240 days</th>
											  <th>270 days</th>
											  <th>300 days</th>
											  <th>330 days</th>
											  <th>360 days</th>
											  <th>Over 360 days</th>
											  <th>Unallocated Payments</th>
											  <th>Balance</th>
											</tr>
										  </thead>
										  <tbody>
									';

					}
					$count = 0;

								
								
					$total_this_month = 0;
					$total_three_months = 0;
					$total_six_months = 0;
					$total_nine_months = 0;
					$total_payments = 0;
					$total_invoices =0;
					$total_balance = 0;
					$total_one_twenty_days =0;
		            $total_one_fifty_days =0;
		            $total_one_eighty_days =0;

		            $total_two_ten_days =0;
		            $total_two_fourty_days =0;
		            $total_two_seventy_days =0;
		            $total_three_hundred_days =0;
		            $total_three_thirty_days =0;
		            $total_three_sixty_days =0;
		            $total_three_ninety_days =0;
		            $total_four_twenty_days =0;
		            $total_four_fifty_days =0;
		            $total_four_eighty_days =0;
		            $total_five_ten_days =0;
		            $total_five_fourty_days =0;
		            $total_five_seventy_days =0;
		            $total_six_hundred_days =0;
		            $total_six_thirty_days =0;
		            $total_six_sixty_days =0;
		            $total_six_ninety_days =0;
		            $total_seven_twenty_days =0;
		            $total_over_seven_twenty_days =0;
		             $total_over_three_sixty_days =0;



		            $total_one_eighty_days =0;
		            $total_over_three_sixty_days =0;
		            $total_over_one_eighty_days = 0;
		            $total_unallocated  =0;
					// var_dump($query_list->num_rows());die();
					if($query_list->num_rows() > 0)
					{

						foreach ($query_list->result() as $key => $value_two) 
						{
							// code...
							$creditor_id = $value_two->creditor_id;
							$creditor_name = $value_two->creditor_name;

							$opening_balance = $value_two->opening_balance;
							$debit_id = $value_two->debit_id;

							$opening_date = $value_two->created;

							
							if($debit_id == 2)
							{
								$opening_balance = $opening_balance;	
							}
							else
							{
								$opening_balance = -$opening_balance;
							}

							$unallocated = $this->company_financial_model->get_unallocated_funds($creditor_id);

							if($creditor_id == 39){
								// var_dump($unallocated);die();
							}

							if(empty($unallocated))
								$unallocated = 0;
							$total_unallocated += $unallocated;


							if(array_key_exists($creditor_id, $creditor_array))
							{
								$this_month = $creditor_array[$creditor_id]->coming_due;
								$three_months = $creditor_array[$creditor_id]->thirty_days;
								$six_months = $creditor_array[$creditor_id]->sixty_days;
								$nine_months = $creditor_array[$creditor_id]->ninety_days;
								$one_twenty_days = $creditor_array[$creditor_id]->one_twenty_days;
								$one_fifty_days = $creditor_array[$creditor_id]->one_fifty_days;
								$one_eighty_days = $creditor_array[$creditor_id]->one_eighty_days;
								$over_one_eighty_days = $creditor_array[$creditor_id]->over_one_eighty_days;

								if($report_detail == 360)
					         	{
									$two_ten_days = $creditor_array[$creditor_id]->two_ten_days;
									$two_fourty_days = $creditor_array[$creditor_id]->two_fourty_days;
									$two_seventy_days = $creditor_array[$creditor_id]->two_seventy_days;
									$three_hundred_days = $creditor_array[$creditor_id]->three_hundred_days;
									$three_thirty_days = $creditor_array[$creditor_id]->three_thirty_days;


									$three_sixty_days = $creditor_array[$creditor_id]->three_sixty_days;
									$over_three_sixty_days = $creditor_array[$creditor_id]->over_three_sixty_days;

								}

								if($report_detail == 720)
					         	{
								$three_ninety_days = $creditor_array[$creditor_id]->three_ninety_days;


								$four_twenty_days = $creditor_array[$creditor_id]->four_twenty_days;
								$four_fifty_days = $creditor_array[$creditor_id]->four_fifty_days;
								$four_eighty_days = $creditor_array[$creditor_id]->four_eighty_days;

								$five_ten_days = $creditor_array[$creditor_id]->five_ten_days;
								$five_fourty_days = $creditor_array[$creditor_id]->five_fourty_days;
								$five_seventy_days = $creditor_array[$creditor_id]->five_seventy_days;
								$six_hundred_days = $creditor_array[$creditor_id]->six_hundred_days;
								$six_thirty_days = $creditor_array[$creditor_id]->six_thirty_days;
								$six_sixty_days = $creditor_array[$creditor_id]->six_sixty_days;
								$six_ninety_days = $creditor_array[$creditor_id]->six_ninety_days;
								$seven_twenty_days = $creditor_array[$creditor_id]->seven_twenty_days;
								$over_seven_twenty_days = $creditor_array[$creditor_id]->over_seven_twenty_days;
								}



								$date = date('Y-m-d');
					        

							   if($report_detail == 180)
					            {
					            	 $total_this_month +=$this_month;
					            $total_three_months +=$three_months;
					            $total_six_months +=$six_months;
					            $total_nine_months +=$nine_months;
					            $total_one_twenty_days +=$one_twenty_days;
					            $total_one_fifty_days +=$one_fifty_days;
					            $total_one_eighty_days +=$one_eighty_days;
					             $total_over_one_eighty_days +=$over_one_eighty_days;
					         	}
					         	else if($report_detail == 360)
					         	{
					         		 $total_this_month +=$this_month;
						            $total_three_months +=$three_months;
						            $total_six_months +=$six_months;
						            $total_nine_months +=$nine_months;
						            $total_one_twenty_days +=$one_twenty_days;
						            $total_one_fifty_days +=$one_fifty_days;
						            $total_one_eighty_days +=$one_eighty_days;
					         		$total_two_ten_days += $two_ten_days;
						            $total_two_fourty_days += $two_fourty_days;
						            $total_two_seventy_days += $two_seventy_days;
						            $total_three_hundred_days += $three_hundred_days;
						            $total_three_thirty_days += $three_thirty_days;
						            $total_three_sixty_days += $three_sixty_days;
						            $total_over_three_sixty_days += $over_three_sixty_days;

					         	}
					         	else if($report_detail == 720)
					         	{

					         		$total_this_month +=$this_month;
						            $total_three_months +=$three_months;
						            $total_six_months +=$six_months;
						            $total_nine_months +=$nine_months;
						            $total_one_twenty_days +=$one_twenty_days;
						            $total_one_fifty_days +=$one_fifty_days;
						            $total_one_eighty_days +=$one_eighty_days;

					         		$total_two_ten_days += $two_ten_days;
						            $total_two_fourty_days += $two_fourty_days;
						            $total_two_seventy_days += $two_seventy_days;
						            $total_three_hundred_days += $three_hundred_days;
						            $total_three_thirty_days += $three_thirty_days;
						            $total_three_sixty_days += $three_sixty_days;
					         		$total_three_ninety_days += $three_ninety_days;
						            $total_four_twenty_days += $four_twenty_days;
						            $total_four_fity_days += $four_fity_days;
						            $total_four_eighty_days += $four_eighty_days;
						            $total_five_ten_days += $five_ten_days;
						            $total_five_fourty_days += $five_fourty_days;
						            $total_five_seventy_days += $five_seventy_days;
						            $total_six_hundred_days += $six_hundred_days;
						            $total_six_thirty_days += $six_thirty_days;
						            $total_six_sixty_days += $six_sixty_days;
						            $total_six_ninety_days += $six_ninety_days;
						            $total_seven_twenty_days += $seven_twenty_days;
						            $total_over_seven_twenty_days += $over_seven_twenty_days;


					         	}

					           
					         
					            // $total_payments += $payments_total;
					            // $total_invoices += $invoice_total;

					            if($report_detail == 180)
					            {


					            	$invoice_total = $this_month + $three_months + $six_months + $nine_months + $one_twenty_days + $one_fifty_days + $one_eighty_days + $over_one_eighty_days;
					        	}

					        	else if($report_detail == 360)
					            {


					             $invoice_total = $this_month + $three_months + $six_months + $nine_months + $one_twenty_days + $one_fifty_days + $one_eighty_days + $two_ten_days + $two_fourty_days + $two_seventy_days + $three_hundred_days + $three_thirty_days + $three_sixty_days + $over_three_sixty_days;
					            }

					            else if($report_detail == 720)
					            {
					             $invoice_total = $this_month + $three_months + $six_months + $nine_months + $one_twenty_days + $one_fifty_days + $one_eighty_days + $two_ten_days + $two_fourty_days + $two_seventy_days + $three_hundred_days + $three_thirty_days + $three_sixty_days + $three_ninety_days + $four_twenty_days + $four_fifty_days + $four_eighty_days + $five_ten_days + $five_fourty_days + $five_seventy_days + $six_hundred_days + $six_thirty_days + $six_sixty_days + $six_ninety_days + $seven_twenty_days + $over_seven_twenty_days;
					         	}


					            $total_balance += $invoice_total;


					            $count++;
					            if($report_detail == 180)
					            {
									$result .= 
										'
											<tr>
												<td class="text-left">'.$count.'</td>
												<td class="text-left">'.$creditor_name.'</td>
												<td class="text-right">'.number_format($this_month, 2).'</td>
												<td class="text-right">'.number_format($three_months, 2).'</td>
												<td class="text-right">'.number_format($six_months, 2).'</td>
												<td class="text-right">'.number_format($nine_months, 2).'</td>
												<td class="text-right">'.number_format($one_twenty_days, 2).'</td>
												<td class="text-right">'.number_format($one_fifty_days, 2).'</td>
												<td class="text-right">'.number_format($one_eighty_days, 2).'</td>
												<td class="text-right">'.number_format($over_one_eighty_days, 2).'</td>
												<td class="text-right" >'.number_format($unallocated,2).'</td>
												<td class="text-right">'.number_format($invoice_total, 2).'</td>
												<td class="text-right"><a href="'.site_url().'creditor-statement/'.$creditor_id.'" class="btn btn-warning btn-xs"> Open Account</a></td>
												<td class="text-right"><a href="'.site_url().'finance/edit-creditor/'.$creditor_id.'" class="btn btn-success btn-xs"> Edit Account</a></td>
												<td class="text-right"><a href="'.site_url().'finance/delete-creditor/'.$creditor_id.'" class="btn btn-danger btn-xs" onclick="return confirm(\'Are you sure you want to remove this creditor ? \')"> Delete</a></td>
											</tr>
												';

								}
								else if($report_detail == 360)
								{

									$result .= 
										'
											<tr>
												<td class="text-left">'.$count.'</td>
												<td class="text-left">'.$creditor_name.'</td>
												<td class="text-right">'.number_format($this_month, 2).'</td>
												<td class="text-right">'.number_format($three_months, 2).'</td>
												<td class="text-right">'.number_format($six_months, 2).'</td>
												<td class="text-right">'.number_format($nine_months, 2).'</td>
												<td class="text-right">'.number_format($one_twenty_days, 2).'</td>
												<td class="text-right">'.number_format($one_fifty_days, 2).'</td>
												<td class="text-right">'.number_format($one_eighty_days, 2).'</td>
												<td class="text-right">'.number_format($two_ten_days, 2).'</td>
												<td class="text-right">'.number_format($two_fourty_days, 2).'</td>
												<td class="text-right">'.number_format($two_seventy_days, 2).'</td>
												<td class="text-right">'.number_format($three_hundred_days, 2).'</td>
												<td class="text-right">'.number_format($three_thirty_days, 2).'</td>
												<td class="text-right">'.number_format($three_sixty_days, 2).'</td>
												<td class="text-right">'.number_format($over_three_sixty_days, 2).'</td>
												<td class="text-right" >'.number_format($unallocated,2).'</td>
												<td class="text-right">'.number_format($invoice_total, 2).'</td>
												<td class="text-right"><a href="'.site_url().'creditor-statement/'.$creditor_id.'" class="btn btn-warning btn-xs"> Open Account</a></td>
												<td class="text-right"><a href="'.site_url().'finance/edit-creditor/'.$creditor_id.'" class="btn btn-success btn-xs"> Edit Account</a></td>
												<td class="text-right"><a href="'.site_url().'finance/delete-creditor/'.$creditor_id.'" class="btn btn-danger btn-xs" onclick="return confirm(\'Are you sure you want to remove this creditor ? \')"> Delete</a></td>
												</tr>
												';

								}
								else if($report_detail == 720)
								{
									
									$result .= 
										'
											<tr>
												<td class="text-left">'.$count.'</td>
												<td class="text-left">'.$creditor_name.'</td>
												<td class="text-right">'.number_format($this_month, 2).'</td>
												<td class="text-right">'.number_format($three_months, 2).'</td>
												<td class="text-right">'.number_format($six_months, 2).'</td>
												<td class="text-right">'.number_format($nine_months, 2).'</td>
												<td class="text-right">'.number_format($one_twenty_days, 2).'</td>
												<td class="text-right">'.number_format($one_fifty_days, 2).'</td>
												<td class="text-right">'.number_format($one_eighty_days, 2).'</td>
												<td class="text-right">'.number_format($two_ten_days, 2).'</td>
												<td class="text-right">'.number_format($two_fourty_days, 2).'</td>
												<td class="text-right">'.number_format($two_seventy_days, 2).'</td>
												<td class="text-right">'.number_format($three_hundred_days, 2).'</td>
												<td class="text-right">'.number_format($three_thirty_days, 2).'</td>
												<td class="text-right">'.number_format($three_sixty_days, 2).'</td>
												<td class="text-right">'.number_format($three_ninety_days, 2).'</td>
												<td class="text-right">'.number_format($four_twenty_days, 2).'</td>
												<td class="text-right">'.number_format($four_fifty_days, 2).'</td>
												<td class="text-right">'.number_format($four_eighty_days, 2).'</td>
												<td class="text-right">'.number_format($five_ten_days, 2).'</td>
												<td class="text-right">'.number_format($five_fourty_days, 2).'</td>
												<td class="text-right">'.number_format($five_seventy_days, 2).'</td>
												<td class="text-right">'.number_format($six_hundred_days, 2).'</td>
												<td class="text-right">'.number_format($six_thirty_days, 2).'</td>
												<td class="text-right">'.number_format($six_sixty_days, 2).'</td>
												<td class="text-right">'.number_format($six_ninety_days, 2).'</td>
												<td class="text-right">'.number_format($seven_twenty_days, 2).'</td>
												<td class="text-right">'.number_format($over_seven_twenty_days, 2).'</td>
												<td class="text-right" >'.number_format($unallocated,2).'</td>
												<td class="text-right">'.number_format($invoice_total, 2).'</td>
												<td class="text-right"><a href="'.site_url().'creditor-statement/'.$creditor_id.'" class="btn btn-warning btn-xs"> Open Account</a></td>
												<td class="text-right"><a href="'.site_url().'finance/edit-creditor/'.$creditor_id.'" class="btn btn-success btn-xs"> Edit Account</a></td>
												<td class="text-right"><a href="'.site_url().'finance/delete-creditor/'.$creditor_id.'" class="btn btn-danger btn-xs" onclick="return confirm(\'Are you sure you want to remove this creditor ? \')"> Delete</a></td>
												</tr>
												';
								}



							}
						

						




						}


						if($report_detail == 180)
						{
						
							$result .= '<tr>
											<th class="text-right" ></th>
											<th class="text-right" >TOTAL</th>
											<th class="text-right">'.number_format($total_this_month, 2).'</th>
											<th class="text-right">'.number_format($total_three_months, 2).'</th>
											<th class="text-right">'.number_format($total_six_months, 2).'</th>
											<th class="text-right">'.number_format($total_nine_months, 2).'</th>
											<th class="text-right">'.number_format($total_one_twenty_days, 2).'</th>
											<th class="text-right">'.number_format($total_one_fifty_days, 2).'</th>
											<th class="text-right">'.number_format($total_one_eighty_days, 2).'</th>
											<th class="text-right">'.number_format($total_over_one_eighty_days, 2).'</th>
											<th class="text-right">'.number_format($total_unallocated, 2).'</th>
											<th class="text-right">'.number_format($total_balance,2).'</th>
										</tr>
										';
						}
						else if($report_detail == 360)
						{

							$result .= '<tr>
											<th class="text-right" ></th>
											<th class="text-right">TOTAL</th>

											<th class="text-right">'.number_format($total_this_month, 2).'</th>
											<th class="text-right">'.number_format($total_three_months, 2).'</th>
											<th class="text-right">'.number_format($total_six_months, 2).'</th>
											<th class="text-right">'.number_format($total_nine_months, 2).'</th>
											<th class="text-right">'.number_format($total_one_twenty_days, 2).'</th>
											<th class="text-right">'.number_format($total_one_fifty_days, 2).'</th>
											<th class="text-right">'.number_format($total_one_eighty_days, 2).'</th>
											<th class="text-right">'.number_format($total_two_ten_days, 2).'</th>
											<th class="text-right">'.number_format($total_two_fourty_days, 2).'</th>
											<th class="text-right">'.number_format($total_two_seventy_days, 2).'</th>
											<th class="text-right">'.number_format($total_three_hundred_days, 2).'</th>
											<th class="text-right">'.number_format($total_three_thirty_days, 2).'</th>
											<th class="text-right">'.number_format($total_three_sixty_days, 2).'</th>
											<th class="text-right">'.number_format($total_over_three_sixty_days, 2).'</th>
											<th class="text-right">'.number_format($total_unallocated, 2).'</th>
											<th class="text-right">'.number_format($total_balance,2).'</th>
										</tr>
										';
						}
						else if($report_detail == 720)
						{

							$result .= '<tr>
											<th class="text-right" ></th>
											<th class="text-right" >TOTAL</th>
											<th class="text-right">'.number_format($total_this_month, 2).'</th>
											<th class="text-right">'.number_format($total_three_months, 2).'</th>
											<th class="text-right">'.number_format($total_six_months, 2).'</th>
											<th class="text-right">'.number_format($total_nine_months, 2).'</th>
											<th class="text-right">'.number_format($total_one_twenty_days, 2).'</th>
											<th class="text-right">'.number_format($total_one_fifty_days, 2).'</th>
											<th class="text-right">'.number_format($total_one_eighty_days, 2).'</th>
											<th class="text-right">'.number_format($total_two_ten_days, 2).'</th>
											<th class="text-right">'.number_format($total_two_fourty_days, 2).'</th>
											<th class="text-right">'.number_format($total_two_seventy_days, 2).'</th>
											<th class="text-right">'.number_format($total_three_hundred_days, 2).'</th>
											<th class="text-right">'.number_format($total_three_thirty_days, 2).'</th>
											<th class="text-right">'.number_format($total_three_sixty_days, 2).'</th>
											<th class="text-right">'.number_format($total_three_ninety_days, 2).'</th>
											<th class="text-right">'.number_format($total_four_twenty_days, 2).'</th>
											<th class="text-right">'.number_format($total_four_fity_days, 2).'</th>
											<th class="text-right">'.number_format($total_four_eighty_days, 2).'</th>
											<th class="text-right">'.number_format($total_five_ten_days, 2).'</th>
											<th class="text-right">'.number_format($total_five_fourty_days, 2).'</th>
											<th class="text-right">'.number_format($total_five_seventy_days, 2).'</th>
											<th class="text-right">'.number_format($total_six_hundred_days, 2).'</th>
											<th class="text-right">'.number_format($total_six_thirty_days, 2).'</th>
											<th class="text-right">'.number_format($total_six_sixty_days, 2).'</th>
											<th class="text-right">'.number_format($total_six_ninety_days, 2).'</th>
											<th class="text-right">'.number_format($total_seven_twenty_days, 2).'</th>
											<th class="text-right">'.number_format($total_over_seven_twenty_days, 2).'</th>
											<th class="text-right">'.number_format($total_unallocated, 2).'</th>
											<th class="text-right">'.number_format($total_balance,2).'</th>
										</tr>
										';
						}
						$result .= 
							'
										  </tbody>
										</table>
							';
					}
						
					echo $result;
			?>
   
    </div>
</section>
