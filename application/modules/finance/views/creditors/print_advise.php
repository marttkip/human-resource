
<?php


if($payment_voucher_details->num_rows() > 0)
{
    foreach ($payment_voucher_details->result() as $key => $value) {
        // code...

        $creditor_payment_id = $value->creditor_payment_id;
        $payment_voucher_amount = $value->total_amount;
        $created = $value->created;
        $created_by = $value->created_by;
        $creditor_id = $value->creditor_id;
        // $creditor_type = $value->creditor_type;
        $payment_voucher_number = $value->reference_number;
        // $approval_status = $value->approval_status;
        $confirm_number = $value->document_number;
    }
}


// if($creditor_type == 1)
$name = $this->creditors_model->get_creditor_name($creditor_id);
// else if($creditor_type == 2)
// $name = $this->creditors_model->get_provider_name($creditor_id);


$amount_in_words = $this->accounts_model->convert_number($payment_voucher_amount);



?>

	<html lang="en">

    <head>
          <title>Payment Advise</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:9px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 60%;
             }
        
            h3
            {
                font-size: 30px;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 10px;}
            .title-img{float:left; padding-left:10px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {  
                            width:20%; float:right; 
                            /*background-color: green; */
                            color:black;
                            height: 73px !important;
                            padding: 20px;
                            word-spacing: 3px;
                            margin-top: 5px;
                            /*margin: 1px;*/
                            border-radius: 5px;
                        }
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

            table.table{
                border-spacing: 0 !important;
                font-size:11px;
                margin-top:8px;
                border-collapse: inherit !important;
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
            }
            /*th, td {
                border: 2px solid #000 !important;
                padding: 0.5em 1em !important;
            }
            thead tr:first-child th:first-child {
                border-radius: 20px 0 0 0 !important;
            }
            thead tr:first-child th:last-child {
                border-radius: 0 20px 0 0 !important;
            }
            tbody tr:last-child td:first-child {
                border-radius: 0 0 0 20px !important;
            }
            tbody tr:last-child td:last-child {
                border-radius: 0 0 20px 20px !important;
            }
            tbody tr:first-child td:first-child {
                border-radius: 20px 20px 0 0 !important;
                border-bottom: 0px solid #000 !important;
            }*/
            .padd
            {
                padding:50px;
            }
            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
                padding: 5px !important;
            }
            
        </style>
    </head>
    <body class="receipt_spacing">
        
        <div class="col-md-12" >
            <div class="col-print-4">
                    <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" style=" height: 100px; "alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
                 
                    
            </div>
            <div class="col-print-5">
                <strong>
                        <h6>
                        <?php echo $contacts['company_name'];?><br/>
                        P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                        <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                        E-mail: <?php echo $contacts['email'];?>.<br/>
                         Tel : <?php echo $contacts['phone'];?><br/>
                     </h6>
                </strong>
            </div>
    
            <div class="col-print-3">
                    <div class="col-print-12">
                        <h6>Cheque No. : <?php echo $confirm_number?></h6>
                    </div>
                    <div class="col-print-12">
                        <h6>Payment No. : <?php echo $payment_voucher_number?></h6>
                    </div>

                    <div class="col-print-12">
                        <h6>Date. : <?php echo date('jS M Y',strtotime($created))?></h6>
                    </div>

            </div>
        </div>
        <div class="col-md-12" style="border-top: 2px solid grey;">
            <div >
                <h4>Cash/Cheque Payment Voucher</h4>
            </div>
        </div>



            <div class="col-md-12">

                <div class="col-md-12">
                    
                    <table class="table table-hover table-bordered ">
                        <thead>
                            <tr style="border:2px solid black">
                              <td colspan="3" style="border:1px solid black">Payment to the order of : <?php echo $name?></td>
                            </tr>
                            <tr>
                              <td colspan="3" style="border:1px solid black">Amount (In Figures) : <?php echo $payment_voucher_amount?></td>
                            </tr>
                            <tr>
                              <td colspan="3" style="border:1px solid black">Amount (In Words) : <?php echo $amount_in_words?></td>
                            </tr>
                            
                            <tr>
                              <th width="60%" style="border:1px solid black">DESCRIPTION</th>
                              <th width="20%" style="text-align:right;border:1px solid black">AMOUNT</th>                       
                              <th width="20%" style="border:1px solid black">OTHER INFO</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $payment_voucher_rs = $this->creditors_model->get_payment_vouchers_items($creditor_id,$creditor_payment_id);
                                $result_voucher_items = '';
                                $total_voucher = 0;
                                if($payment_voucher_rs->num_rows() > 0)
                                {
                                    foreach ($payment_voucher_rs->result() as $key => $value) {
                                        // code...

                                        $invoice_number = $value->transactionDescription;
                                        $invoice_date = $value->transactionDate;
                                        $payment_voucher_item_amount = $value->amount;
                                        // $invoice_amount = $value->invoice_amount;
                                        // $expense_account = $value->expense_account;


                                        
                                       	$description = $invoice_number;
                                       


                                        $total_voucher += $payment_voucher_item_amount;

                                        $result_voucher_items .= '<tr>
                                                                        <td style="border:1px solid black">'.$description.'</td>
                                                                        <td style="border:1px solid black;text-align:right">'.number_format($payment_voucher_item_amount,2).'</td>
                                                                        <td style="border:1px solid black"></td>
                                                                    </tr>';
                                    }

                                     $result_voucher_items .= '<tr>
                                                                    <th style="border:1px solid black;">Total</th>
                                                                    <th style="text-align:right;border:1px solid black;">'.number_format($total_voucher,2).'</th>
                                                                    <th></th>
                                                                </tr>';
                                }

                                echo $result_voucher_items;
                            ?>
                           
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-12" style="font-style:italic; font-size:11px; margin-top: 20px;padding: 20px;">
                <div class="col-md-12">
                    <div class="col-print-6">
                        <div class="col-print-12" style="margin-top: 15px;">
                            <div class="col-print-3">Paid By:</div><div  class="col-print-9">.................................................................................</div>
                        </div>

                        <div class="col-print-12" style="margin-top: 15px;">
                            <div class="col-print-3">Sign:</div><div  class="col-print-9">.................................................................................</div>
                        </div>
                        <div class="col-print-12" style="margin-top: 15px;">
                          <div class="col-print-3">Received By:</div><div  class="col-print-9">.................................................................................</div>
                        </div>
                        <div class="col-print-12" style="margin-top: 15px;">
                          <div class="col-print-3">Sign:</div><div  class="col-print-9">.................................................................................</div>
                        </div>
                    </div>

                    <div class="col-print-6">
                        <div class="col-print-12" style="margin-top: 15px;">
                            <div class="col-print-3">Approved By:</div><div  class="col-print-9">.................................................................................</div>
                        </div>

                        <div class="col-print-12" style="margin-top: 15px;">
                            <div class="col-print-3">Sign:</div><div  class="col-print-9">.................................................................................</div>
                        </div>
                    </div>
                    

                </div>
            </div>



       
    </body>
    
</html> 