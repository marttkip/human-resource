<?php



if($account_from_id > 0)
{
    $next_purchase_number = $this->purchases_model->create_purchase_number($location_id);
    // var_dump($next_purchase_number);die();
    $web_name = $this->site_model->create_web_name($account_name_title);

    $transacting_account_name = $this->purchases_model->get_account_name($account_from_id);

    $petty_cash_date_from = $this->session->userdata('petty_cash_visit_date_from');
    $petty_cash_date_to = $this->session->userdata('petty_cash_visit_date_to');
    ?>


     <script type="text/javascript">

            $(function() {
                $("#skill_input").autocomplete({
                  source: "<?php echo base_url('finance/purchases/autocompleteData'); ?>",
                  select: function( event, ui ) {
                      event.preventDefault();
                      $("#skill_input").val(ui.item.value);
                  }

                });
            });
    </script>
    <div class="col-md-5">
        <div class="col-md-12">
          <section class="panel panel-info" style="margin-bottom: 5px !important;">
              <header class="panel-heading">
                  <h3 class="panel-title">Search <?php echo $transacting_account_name?></h3>
              </header>
              <div class="panel-body">

                   <div class="row">

                        <?php echo form_open("finance/purchases/search_petty_cash", array("class" => "form-horizontal"));?>

                         <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
                        <div class="col-md-12">
                            <div class="col-md-6">
                               <div class="form-group">
                                   <label class="col-md-4 control-label">Date From: </label>

                                   <div class="col-md-8">
                                       <div class="input-group">
                                           <span class="input-group-addon">
                                               <i class="fa fa-calendar"></i>
                                           </span>
                                           <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Transaction date" value="<?php echo $petty_cash_date_from?>" id="datepicker" autocomplete="off" required>
                                       </div>
                                   </div>
                               </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Date To: </label>

                                    <div class="col-md-8">
                                         <div class="input-group">
                                             <span class="input-group-addon">
                                                 <i class="fa fa-calendar"></i>
                                             </span>
                                             <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Transaction date" value="<?php echo $petty_cash_date_to?>" id="datepicker1" autocomplete="off" required>
                                         </div>
                                     </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 " style="margin-top:10px">
                                <div class="center-align">
                                    <button type="submit" class="btn btn-sm btn-info">Search record</button>
                                    <a href="<?php echo site_url().'print-petty-cash/'.$web_name?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print Statement</a>

                                </div>



                            </div>
                        </div>


                   <?php echo form_close();?>

                   <?php
                    $search = $this->session->userdata('search_petty_cash');
                    if(!empty($search))
                    {
                        ?>
                       <!--  <div class="center-align">
                             <a href="<?php echo base_url().'finance/purchases/close_petty_cash_search/'.$location_id;?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Close Search</a>
                        </div> -->

                        <?php
                    }
                   ?>

               </div>

                </div>
            </section>
        </div>
        <div class="col-md-12">
          <section class="panel panel-info" style="margin-top: 0em !important;margin-bottom: 5px;">
              <header class="panel-heading">
                  <h3 class="panel-title">Record Transaction </h3>
              </header>
              <div class="panel-body">


                 <?php echo form_open("finance/purchases/record_petty_cash", array("class" => "form-horizontal"));?>
                   <div class="row">
                     <div class="col-md-12">
                     <div class="col-md-6">
                       <input type="hidden" class="form-control" name="account_from_id" placeholder="Account" value="<?php echo $account_from_id?>" required/>
                       <input type="hidden" class="form-control" name="location_id" placeholder="Location" value="<?php echo $location_id?>" required/>
                       <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
                           <div class="form-group">
                               <label class="col-md-4 control-label">Transaction date: </label>

                               <div class="col-md-8">
                                   <div class="input-group">
                                       <span class="input-group-addon">
                                           <i class="fa fa-calendar"></i>
                                       </span>
                                       <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="transaction_date" placeholder="Transaction date" value="<?php echo date('Y-m-d');?>" id="datepicker2" required>
                                   </div>
                               </div>
                           </div>
                           <div class="form-group">
                               <label class="col-md-4 control-label">Transaction No *</label>

                               <div class="col-md-8">
                                   <input type="text" class="form-control" value="<?php echo $next_purchase_number;?>" name="transaction_number" placeholder="Transaction Number" required/>
                               </div>
                           </div>
                           <div class="form-group">
                               <label class="col-md-4 control-label">Department *</label>

                               <div class="col-md-8">
                                   <select class="form-control" name="department_id" id="department_id" >
                                       <option value="0">-- Select a department --</option>
                                       <?php
                                       if($departments->num_rows() > 0)
                                       {
                                           foreach($departments->result() as $res)
                                           {
                                               $department_id = $res->department_id;
                                               $department_name = $res->department_name;
                                               ?>
                                               <option value="<?php echo $department_id;?>"><?php echo $department_name;?></option>
                                               <?php
                                           }
                                       }
                                       ?>
                                   </select>
                               </div>
                           </div>

                           <div class="form-group" >
                               <label class="col-md-4 control-label">Expense Account *</label>

                               <div class="col-md-8">
                                   <select class="form-control select2" name="account_to_id" id="account_to_id" required>
                                     <option value="0">--- select an expense account - ---</option>
                                     <?php
                                     if($expense_accounts->num_rows() > 0)
                                     {
                                       foreach ($expense_accounts->result() as $key => $value) {
                                         // code...
                                         $account_id = $value->account_id;
                                         $account_name = $value->account_name;
                                         echo '<option value="'.$account_id.'"> '.$account_name.'</option>';
                                       }
                                     }
                                     ?>
                                   </select>
                               </div>
                           </div>


                     </div>
                     <div class="col-md-6">
                       <div class="form-group">
                           <label class="col-md-4 control-label">Type *</label>

                           <div class="col-md-8">
                               <div class="radio">
                                   <label>
                                       <input  type="radio" checked value="0" name="transaction_type_id" id="account_to_type" onclick="get_transaction_type_list(this.value)">
                                       Pure Expense
                                   </label>
                                  <!--  <label>
                                       <input  type="radio"  value="1" name="transaction_type_id" id="account_to_type" onclick="get_transaction_type_list(this.value)">
                                       Expense Linked to a creditor
                                   </label> -->
                               </div>
                           </div>
                       </div>
                       <div class="form-group" style="display:none;" id="creditor_div">
                           <label class="col-md-4 control-label">Creditor*</label>

                           <div class="col-md-8">
                               <select class="form-control" name="creditor_id" id="creditor_id">
                                 <option value="0">--- select a creditor - ---</option>
                                 <?php
                                 if($creditors->num_rows() > 0)
                                 {
                                   foreach ($creditors->result() as $key => $value) {
                                     // code...
                                     $creditor_id = $value->creditor_id;
                                     $creditor_name = $value->creditor_name;
                                     echo '<option value="'.$creditor_id.'"> '.$creditor_name.'</option>';
                                   }
                                 }
                                 ?>
                               </select>
                           </div>
                       </div>


                               <div class="form-group">
                                   <label class="col-md-4 control-label">Description *</label>

                                   <div class="col-md-8">
                                       <textarea class="form-control" name="description" required></textarea>
                                   </div>
                               </div>

                               <div class="form-group">
                                   <label class="col-md-4 control-label">Amount *</label>

                                   <div class="col-md-8">
                                       <input type="text" class="form-control" name="transacted_amount" placeholder="Amount" required/>
                                   </div>
                               </div>

                               <div class="form-group">
                                   <label class="col-md-4 control-label">Recepient *</label>

                                   <div class="col-md-8">
                                     <input type="text" id="skill_input" name="recipient_name"  class="form-control" placeholder="Start typing..." required>

                                   </div>
                               </div>




                            </div>
                     </div>



                   </div>
                   <div class="row">
                       <div class="col-md-12">
                           <div class="alert alert-warning alert-sm">NB: Please note Petty Cash account has to have a + Figure at the Balance for any transactions be submit.</div>
                       </div>
                   </div>
                   <div class="row" style="margin-top:5px;">
                         <div class="col-md-12">
                             <div class="text-center">
                                 <button type="submit" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to add this record ? ')">Save record</button>
                             </div>
                         </div>
                     </div>
                   <?php echo form_close();?>


                </div>

          </section>
        </div>
        <div class="col-md-12">
            <?php
            $query_list = $this->company_financial_model->get_account_transactions_ledgers(NULL,NULL,0,0,2,$petty_cash_date_from, $petty_cash_date_to,4,$account_from_id);
            $grouped_array_checked = array();
            foreach ($query_list->result() as $element_two) {
                $grouped_array_checked[$element_two->accountId] = $element_two;
            }

            $opening_balance_cr_amount =  $grouped_array_checked[$account_from_id]->cr_amount;
            $opening_balance_dr_amount =  $grouped_array_checked[$account_from_id]->dr_amount;

            $opening_balance = $opening_balance_cr_amount - $opening_balance_dr_amount;

            ?>

        </div>
    </div>
    <div class="col-md-7">

        <div class="col-md-12">


    			<?php

    			$error = $this->session->userdata('error_message');
    			$success = $this->session->userdata('success_message');

    			if(!empty($error))
    			{
    				echo '<div class="alert alert-danger">'.$error.'</div>';
    				$this->session->unset_userdata('error_message');
    			}

    			if(!empty($success))
    			{
    				echo '<div class="alert alert-success">'.$success.'</div>';
    				$this->session->unset_userdata('success_message');
    			}

    			$result =  '';

    			// echo $result;
      $result = '';
      $count = 0;
      $balance = 0;
      // $opening_balance_query = $this->purchases_model->get_account_opening_balance($account_from_id,$location_id);

      $cr_amount = 0;
      $dr_amount = 0;
      $total_dr_amount = 0;
      $total_cr_amount = 0;
      $result .= '<tbody>';
      // if($opening_balance_query->num_rows() > 0)
      // {
        // $row = $opening_balance_query->row();
        $cr_amount = $opening_balance_cr_amount;
        $dr_amount = $opening_balance_dr_amount;
        $balance += $dr_amount;
        $balance -=  $cr_amount;

        $total_dr_amount += $dr_amount;
        $total_cr_amount += $cr_amount;

        $opening_balance = $dr_amount  - $cr_amount;
        // var_dump(expression)
        if($opening_balance < 0 AND $dr_amount > $cr_amount)
        {
          $dr_amount -=$opening_balance;
          $cr_amount = 0;
        }
        else if($opening_balance < 0 AND $dr_amount < $cr_amount)
        {
          $cr_amount -=$opening_balance;
          $dr_amount = 0;
        }
        else if($opening_balance > 0 AND $dr_amount > $cr_amount)
        {
          $dr_amount =$opening_balance;
          $cr_amount =0;
        }
        else if($opening_balance == 0 AND $dr_amount == $cr_amount)
        {
          $dr_amount =0;
          $cr_amount =0;
        }





        $result .='
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Balance B/F</td>
                    <td >'.number_format($dr_amount,2).' </td>
                    <td >'.number_format($cr_amount,2).' </td>
                    <td >'.number_format($balance,2).' </td>

                  </tr>
                  ';



      $query_purchases = $this->company_financial_model->get_account_transactions_ledgers(NULL,NULL,0,0,1,$petty_cash_date_from, $petty_cash_date_to,4,$account_from_id);
      $authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
      if($query_purchases->num_rows() > 0)
      {
        foreach ($query_purchases->result() as $key => $value) {
          // code...
          $transactionClassification = $value->transactionClassification;

          $document_number = '';
          $transaction_number = '';
          $finance_purchase_description = '';
          $finance_purchase_amount = 0 ;
           $referenceId = $value->payingFor;
          $transactionCategory = $value->transactionCategory;
          if($transactionClassification == 'Purchase Payment' AND $referenceId > 0 )
          {
            // get purchase details
            $detail = $this->purchases_model->get_purchases_details($referenceId);
            $row = $detail->row();
            $document_number = $row->document_number;
            $transaction_number = $row->transaction_number;
            $finance_purchase_description = $row->finance_purchase_description;

          }


          $referenceId = $value->payingFor;
          $document_number =$transaction_number = $value->referenceCode;
          $finance_purchase_description = $value->transactionName;
          $transactionTable = $value->transactionTable;
          $f_p_id = $value->transactionId;



          if($transactionClassification == "Expense" AND $dr_amount > 0 AND $cr_amount == 0)
          {
            $cr_amount = $value->cr_amount;
            $dr_amount = $value->dr_amount;

          }
          else
          {
            $cr_amount = $value->cr_amount;
            $dr_amount = $value->dr_amount;
          }



          $transaction_date = $value->transactionDate;

          $creditor_name = '';//$value->creditor_name;
          $creditor_id = 0;//$value->creditor_id;
          $account_name = '';//$value->account_name;
          $finance_purchase_id = '';//$value->finance_purchase_id;



           if($cr_amount > 0 AND ($transactionTable == 'finance_purchase') AND ($transaction_date == date('Y-m-d') OR $authorize_invoice_changes))
           {
            $button_del = '<a onclick="edit_petty_cash('.$f_p_id.','.$location_id.')"   class="btn btn-xs btn-success fa fa-pencil"></a>
                          <a href="'.site_url().'delete-petty-cash-record/'.$f_p_id.'/'.$location_id.'" onclick="return confirm(\'Are you sure you want to delete this record ?\')"  class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>';
          }
          else
          {
            $button_del = '';
          }



           $transaction_date = date('jS M Y',strtotime($transaction_date));

          $total_dr_amount += $dr_amount;
          $total_cr_amount += $cr_amount;
          $balance += $dr_amount;
          $balance -=  $cr_amount;
          if($balance < 0)
          {
            $color = 'class="danger"';
          }
          else
          {
            $color = 'class=""';
          }

          $count++;
          $result .='
                    <tr>
                      <td>'.$count.'</td>
                      <td>'.$transaction_date.'</td>
                      <td>'.$transactionCategory.'</td>
                      <td>'.$transaction_number.'</td>
                      <td>'.$finance_purchase_description.' '.$creditor_name.'</td>
                      <td>'.number_format($dr_amount,2).' </td>
                      <td>'.number_format($cr_amount,2).'</td>
                      <td '.$color.'>'.number_format($balance,2).'</td>
                      <td >'.$button_del.'</td>

                    </tr>
                    ';
        }


      }

      $result .='
                </tbody>
                <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Balance</th>
                    <th>'.number_format($total_dr_amount,2).'</th>
                    <th>'.number_format($total_cr_amount,2).'</th>
                    <th><strong>KES '.number_format($balance,2).' </strong></th>

                </tfoot>
                ';


    ?>
    <section class="panel">
        <header class="panel-heading">
            <h3 class="panel-title"><?php echo $transacting_account_name?>  <?php echo $search_title;?></h3>
        </header>
        <div class="panel-body" style="height: 80vh;overflow-y: scroll;padding:0px">
          <?php
          $error = $this->session->userdata('error_message');
          $success = $this->session->userdata('success_message');

          if(!empty($error))
          {
            var_dump($error);die();
            echo '<div class="alert alert-warning">'.$error.'</div>';
            $this->session->unset_userdata('error_message');
          }

          if(!empty($success))
          {
            echo '<div class="alert alert-success">'.$success.'</div>';
            $this->session->unset_userdata('success_message');
          }
          ?>

            <table class="table table-condensed table-striped table-bordered table-linked">
    		 	<thead>
    				<tr>
                      <th style="width: 2%;">#</th>
    				  <th style="width: 10%;">Date</th>
                      <th style="width: 20%;">Category</th>
    				  <th style="width: 10%;">Ref Number</th>
    				  <th style="width: 20%;">Description</th>
    				  <th style="width: 10%;">Debit</th>
                      <th style="width: 10%;">Credit</th>
    				  <th style="width: 10%;">Bal</th>
                      <th style="width: 8%;"></th>
    				</tr>
    			 </thead>
    		  	<tbody>
                    <?php echo $result;?>
    		  	</tbody>
    		</table>

        </div>

    </section>
    </div>
    </div>

     <script type="text/javascript">

            function get_transaction_type_list(type)
            {
                // var type = getRadioCheckedValue(radio_name);
                // $("#charge_to_id").customselect()="";
                // alert(radio_name);
                var myTarget1 = document.getElementById("creditor_div");

                if(type == 1)
                {


                    myTarget1.style.display = 'block';
                    $('#creditor_id').addClass('select2');
                }
                else {
                  myTarget1.style.display = 'none';
                }
                var url = "<?php echo site_url();?>accounting/petty_cash/get_list_type_petty_cash/1";
                // alert(url);
                //get department services
                $.get( url, function( data )
                {
                    $( "#charge_to_id" ).html( data );
                    // $(".custom-select").customselect();
                });

            }

            function getRadioCheckedValue(radio_name)
            {
               var oRadio = document.forms[0].elements[radio_name];

               for(var i = 0; i < oRadio.length; i++)
               {
                  if(oRadio[i].checked)
                  {
                     return oRadio[i].value;
                  }
               }

               return '';
            }

            function display_payment_model(modal_id)
          	{
          		$('#modal-defaults'+modal_id).modal('show');
          		$('#datepicker1'+modal_id).datepicker({
        	      autoclose: true,
        	      format: 'yyyy-mm-dd',
        	    })
          	}


            function check_payment_type(payment_type_id){

              var myTarget1 = document.getElementById("cheque_div");

              var myTarget2 = document.getElementById("mpesa_div");

              var myTarget3 = document.getElementById("insuarance_div");

              if(payment_type_id == 1)
              {
                // this is a check

                myTarget1.style.display = 'block';
                myTarget2.style.display = 'none';
                myTarget3.style.display = 'none';
              }
              else if(payment_type_id == 2)
              {
                myTarget1.style.display = 'none';
                myTarget2.style.display = 'none';
                myTarget3.style.display = 'none';
              }
              else if(payment_type_id == 3)
              {
                myTarget1.style.display = 'none';
                myTarget2.style.display = 'none';
                myTarget3.style.display = 'block';
              }
              else if(payment_type_id == 4)
              {
                myTarget1.style.display = 'none';
                myTarget2.style.display = 'none';
                myTarget3.style.display = 'none';
              }
              else if(payment_type_id == 5)
              {
                myTarget1.style.display = 'none';
                myTarget2.style.display = 'block';
                myTarget3.style.display = 'none';
              }
              else
              {
                myTarget2.style.display = 'none';
                myTarget3.style.display = 'block';
              }

            }

            function edit_petty_cash(account_payment_id,location_id)
            {

                document.getElementById("sidebar-right").style.display = "block";
                // document.getElementById("existing-sidebar-div").style.display = "none";

                var config_url = $('#config_url').val();
                var data_url = config_url+"finance/purchases/edit_petty_cash/"+account_payment_id;
                //window.alert(data_url);
                $.ajax({
                type:'POST',
                url: data_url,
                data:{account_payment_id: account_payment_id},
                dataType: 'text',
                success:function(data){

                    document.getElementById("current-sidebar-div").style.display = "block";
                    $("#current-sidebar-div").html(data);
                    $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd'
                    });


                },
                error: function(xhr, status, error) {
                //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                alert(error);
                }

                });
            }

            function close_side_bar()
            {
                // $('html').removeClass('sidebar-right-opened');
                document.getElementById("sidebar-right").style.display = "none";
                document.getElementById("current-sidebar-div").style.display = "none";
                document.getElementById("existing-sidebar-div").style.display = "none";
                tinymce.remove();
            }

            $(document).on("submit","form#edit-direct-payment",function(e)
            {
                // alert('dasdajksdhakjh');
                e.preventDefault();
                // myApp.showIndicator();

                var form_data = new FormData(this);

                // alert(form_data);

                var config_url = $('#config_url').val();

                 var url = config_url+"finance/purchases/edit_petty_cash_data";
                $.ajax({
                type:'POST',
                url: url,
                data:form_data,
                dataType: 'text',
               processData: false,
               contentType: false,
                success:function(data)
                {
                  // alert(data);
                  var data = jQuery.parseJSON(data);

                    if(data.message == "success")
                    {

                      var location_id = data.location_id;

                      var redirect_url = $('#redirect_url').val();


                      if(location_id == 2)
                      {
                        window.location.href = config_url+"accounting/unbanked-cash";
                      }
                      else
                      {
                        window.location.href = config_url+"accounting/petty-cash";
                      }


                    }
                    else
                    {
                        alert('Please ensure you have added included all the items');
                    }

                },
                error: function(xhr, status, error) {
                alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

                }
                });


            });




        </script>
<?php
}
else
{
    ?>
    <div class="alert alert-warning alert-lg">NOTE: Please note that the </div>
    <?php
}?>
