<?php
 $document_number_two = $this->transfer_model->create_journal_number();

 // var_dump($type_id);die();

 if($type_id <= 3)
 {
    $required = 'required';
 }
 else
 {
    $required = '';
 }
?>
<div class="row">
  <div class="col-md-12">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?php echo $title?>
            </h2>
        </header>
        <div class="panel-body">
            <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Account to Debit (Money To)</label>
                            <div class="col-lg-8">
                                <select id="account_from_id" name="account_from_id" class="form-control" onchange="get_accounty_type_list(this.value,<?php echo $attached_account_id?>,<?php echo $type_id?>)" required>
                                    
                                    <?php
                                        $changed = '<option value="">--- Account ---</option>';
                                     if($accounts->num_rows() > 0)
                                     {
                                         foreach($accounts->result() as $row):
                                             // $company_name = $row->company_name;
                                             $account_name = $row->account_name;
                                             $account_id = $row->account_id;
                                             $parent_account = $row->parent_account;

                                             if($parent_account != $current_parent)
                                             {
                                                  $account_from_name = $this->transfer_model->get_account_name($parent_account);
                                                $changed .= '<optgroup label="'.$account_from_name.'">';
                                             }

                                             $changed .= "<option value=".$account_id."> ".$account_name."</option>";
                                             $current_parent = $parent_account;
                                             if($parent_account != $current_parent)
                                             {
                                                $changed .= '</optgroup>';
                                             }

                                             
                                            
                                         endforeach;
                                     }
                                     echo $changed;
                                     ?>
                                      
                                        
                                </select>
                            </div>
                        </div>

                        <div id="supplier_invoices_to" style="display:none;">
                             <div class="form-group">
                                 <label class="col-md-4 control-label">Attached Invoice: </label>

                                 <div class="col-md-8">
                                    <select class="form-control custom-select" id="invoice_to_id" name="invoice_to_id" <?php echo $required?>>
                                        
                                    </select>
                                 </div>
                             </div>
                        </div>
                        <input type="hidden" name="type_id" id="type_id" value="<?php echo $type_id?>">
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Reference No *</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="reference_number" placeholder="Reference Number" value="<?php echo $document_number_two;?>" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Transfer date: </label>

                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                    </span>
                                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" placeholder="Transfer Date" value="<?php echo date('Y-m-d');?>" id="datepicker2" required>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">

                        <!-- Activate checkbox -->


                        <div class="form-group">
                            <label class="col-lg-4 control-label">Account to Credit: (Money From) </label>

                            <div class="col-lg-8">
                                <select name="account_to_id" class="form-control select2" id="charge_to_id" onchange="check_for_invoice(this.value,2)" required>
                                  <?php
                                        $changed = '';
                                     if($other_accounts->num_rows() > 0)
                                     {
                                         foreach($other_accounts->result() as $row):
                                             // $company_name = $row->company_name;
                                             $account_name = $row->account_name;
                                             $account_id = $row->account_id;
                                             $parent_account = $row->parent_account;


                                             if($parent_account != $current_parent AND  $type_id >= 4)
                                             {
                                                $account_from_name = $this->transfer_model->get_account_name($parent_account);
                                                $changed .= '<optgroup label="'.$account_from_name.'">';
                                             }



                                             if($account_from_id == $account_id AND $type_id <= 3)
                                             {
                                                 $changed .= '<option value="'.$account_id.'" selected> '.$account_name.'</option>';
                                             }
                                             else if($type_id >= 4)
                                             {
                                                 $changed .= '<option value="'.$account_id.'" > '.$account_name.'</option>';
                                             }
                                             

                                              $current_parent = $parent_account;
                                             if($parent_account != $current_parent AND  $type_id >= 4)
                                             {
                                                $changed .= '</optgroup>';
                                             }

                                             
                                            
                                         endforeach;
                                     }
                                     echo $changed;
                                     ?>

                                </select>
                            </div>
                        </div>

                        <div id="supplier_invoices_from" style="display:none;">
                             <div class="form-group">
                                 <label class="col-md-4 control-label">Attached Invoice: </label>

                                 <div class="col-md-8">
                                    <select class="form-control custom-select" id="invoice_from_id" name="invoice_from_id">
                                        
                                    </select>
                                 </div>
                             </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Amount *</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="amount" placeholder="Amount" value="<?php echo set_value('amount');?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Description *</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" name="description" placeholder="Transfer Description" required="required"></textarea>
                            </div>
                        </div>
                        <div class="form-actions center-align">
                            <button class="submit btn btn-primary btn-sm" type="submit" onclick="return confirm('Are you sure you want to perform this entry ? ')">
                                Transfer
                            </button>
                        </div>
                    </div>

                </div>
            <?php echo form_close();?>
        </div>
      </section>
  </div>
</div>


 <section class="panel">
    <header class="panel-heading">
          <h4 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h4>
          <div class="widget-icons pull-right">
               
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd">
            <?php
            $error = $this->session->userdata('error_message');
            $success = $this->session->userdata('success_message');
            
            if(!empty($error))
            {
                echo '<div class="alert alert-danger">'.$error.'</div>';
                $this->session->unset_userdata('error_message');
            }
            
            if(!empty($success))
            {
                echo '<div class="alert alert-success">'.$success.'</div>';
                $this->session->unset_userdata('success_message');
            }
            $search = $this->session->userdata('search_journal');
            if(!empty($search))
            {
              ?>
              <a href="<?php echo base_url().'finance/transfer/close_journal_search';?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Close Search</a>
              <?php
            }
            
            ?>
                        
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover table-bordered ">
                        <thead>
                            <tr>
                              <th>#</th>   
                              <th>Date</th>      
                              <th>Document No.</th>              
                              <th>Debited Account</th>
                              <th>Credited Account</th>
                              <th>Transacted Amount</th> 
                              <th>Entry Description</th>
                              <th colspan="2">Actions</th>                     
                            </tr>
                         </thead>
                        <tbody>
                            <?php

                                $all_accounts_rs = $this->site_model->get_accounts_names();


                                $array_list = array();

                                foreach ($all_accounts_rs->result() as $element_five) {
                                    // code...
                                    $array_list[$element_five->creditor_id][$element_five->billing_type] = $element_five;
                                }
                                $result = '';
                                // var_dump($query); die();
                               if($query->num_rows() > 0)
                               {
                                 $x=$page;
                                    foreach ($query->result() as $key => $value) {
                                        # code...
                                        $account_from_id = $value->account_from_id;
                                        $account_to_type = $value->account_to_type;
                                        $account_to_id = $value->account_to_id;
                                        $receipt_number = $value->document_number;
                                        $journal_entry_id = $value->journal_entry_id;
                                         $payment_date = $value->payment_date;
                                         $created = $value->created;
                                        $amount_paid = $value->amount_paid;
                                        $payment_to = $value->payment_to;
                                        $account_to_type_id = $value->account_to_type_id;
                                        $invoice_to_number = $value->invoice_to_number;
                                        $attached_to_account_id = $value->attached_to_account_id;

                                        $account_from_type_id = $value->account_from_type_id;
                                        $invoice_to_number = $value->invoice_to_number;
                                        $attached_from_account_id = $value->attached_from_account_id;




                                        $description= '';
                                        if(!empty($attached_to_account_id))
                                        {
                                            $name = '';
                                            
                                            $name = $array_list[$attached_to_account_id][$account_to_type_id]->creditor_name;
                                            
                                            $description = ' '.$invoice_to_number.' - '.$name;
                                        }


                                        if(!empty($attached_from_account_id))
                                        {
                                            $name = '';
                                            
                                            $name = $array_list[$attached_from_account_id][$account_from_type_id]->creditor_name;
                                            



                                            $description .= ' '.$invoice_to_number.' - '.$name;
                                        }

                                        $payment_to = $value->payment_to;
                                        $journal_entry_description = $value->journal_entry_description;

                                        $account_from_name = $this->transfer_model->get_account_name($account_from_id);
                                        $account_to_name = $this->transfer_model->get_account_name($account_to_id);


                                        $edit_invoice = '
                                        <td>
                                          <a class="btn btn-xs btn-primary fa fa-pen"></a>
                                        </td>
                                        ';
                                        


                                        // if($created == date('Y-m-d'))
                                        // {
                                            $add_invoice = '
                                            <td>
                                          <a onclick="edit_journal_entry('.$journal_entry_id.','.$attached_account_id.','.$type_id.')" class="btn btn-xs btn-primary fa fa-pencil"></a>
                                        </td>
                                            <td><a href="'.site_url().'delete-journal-entry/'.$journal_entry_id.'/'.$attached_account_id.'/'.$type_id.'" class="btn btn-xs btn-danger fa fa-trash" onclick="return confirm(\'Do you really want delete this entry?\');"></a></td>';
                                        // }
                                        // else
                                        // {
                                        //     $add_invoice = '';
                                        // }

                                        $x++;

                                        $result .= '<tr>
                                                        <td>'.$x.'</td>
                                                        <td>'.$payment_date.'</td>
                                                        <td>'.strtoupper($receipt_number).'</td>
                                                        <td>'.$account_from_name.'</td>
                                                        <td>'.$account_to_name.'</td>
                                                        <td>'.number_format($amount_paid,2).'</td>
                                                        <td>'.$journal_entry_description.' '.$description.'</td>
                                                        '.$add_invoice.'
                                                    </tr>';

                                    }
                               }
                               echo $result;
                            ?>
                        </tbody>
                    </table>
                </div>
                
            </div>
            <div class="widget-foot">
                                
                <?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
    </div>
</section>



<script type="text/javascript">

    $(document).ready(function(){
               
        document.getElementById("supplier_invoices_to").style.display = "none"; 
        document.getElementById("supplier_invoices_from").style.display = "none"; 
    });
    function get_accounty_type_list(radio_name,attached_account_id,type_id) {
        var type = radio_name;
        // $("#charge_to_id").customselect()="";
        //alert(radio_name);
        // var url = "<?php echo site_url();?>finance/transfer/get_other_accounts/" + type;
        // alert(url);
        //get department services
        // $.get(url, function(data) {
        //     $("#charge_to_id").html(data);
            // $(".custom-select").customselect();

            check_for_invoice(type,type_id,attached_account_id);


        // });

    }


    function get_accounty_edit_type_list(radio_name,journal_entry_id) {
        var type = radio_name;
        // $("#charge_to_id").customselect()="";
        //alert(radio_name);
        var url = "<?php echo site_url();?>finance/transfer/get_other_accounts/" + type;
        // alert(url);
        //get department services
        $.get(url, function(data) {
            $("#charge_to_id").html(data);
            // $(".custom-select").customselect();

            check_for_invoice_edit(type,1,journal_entry_id);


        });

    }
    function check_for_invoice(account_id,type,attached_account_id=0)
    {

            
            var config_url = $('#config_url').val();    

            var url = config_url+"finance/transfer/confirm_for_invoices/"+account_id+"/"+type+"/"+attached_account_id;
            $.ajax({
            type:'POST',
            url: url,
            // data:form_data,
            dataType: 'text',
            // processData: false,
            // contentType: false,
            success:function(data)
            {
              var data = jQuery.parseJSON(data);

                if(data.message == 'success')
                {

                
                    if(type == 1)
                    {

                        document.getElementById("supplier_invoices_to").style.display = "block"; 
                        // document.getElementById("supplier_invoices_from").style.display = "none"; 
                         $( "#invoice_to_id" ).html( data.result );

                         $("#invoice_to_id").customselect();



                    }
                    else if(type == 2)
                    {
                        document.getElementById("supplier_invoices_to").style.display = "block"; 
                        // document.getElementById("supplier_invoices_from").style.display = "none"; 
                         $( "#invoice_to_id" ).html( data.result );

                         $("#invoice_to_id").customselect();
                    }

                    else if(type == 3)
                    {
                        document.getElementById("supplier_invoices_to").style.display = "block"; 
                        // document.getElementById("supplier_invoices_from").style.display = "none"; 
                         $( "#invoice_to_id" ).html( data.result );

                         $("#invoice_to_id").customselect();
                    }

                    
                }
                else
                {
                    document.getElementById("supplier_invoices_to").style.display = "none"; 
                    document.getElementById("supplier_invoices_from").style.display = "none"; 
                }
               
               

            },
            error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

            }
            });
             
    }
    function getRadioCheckedValue(radio_name) {
        var oRadio = document.forms[0].elements[radio_name];

        for (var i = 0; i < oRadio.length; i++) {
            if (oRadio[i].checked) {
                return oRadio[i].value;
            }
        }

        return '';
    }


    function edit_journal_entry(journal_entry_id,attached_account_id,type_id){
      console.log(journal_entry_id);
      document.getElementById("sidebar-right").style.display = "block"; 
      var config_url = $('#config_url').val();

      var data_url = config_url+"finance/transfer/edit_journal_adjustments/"+journal_entry_id;

      $.ajax({
            type:'POST',
            url: data_url,
            data:{journal_entry_id: journal_entry_id},
            dataType: 'text',
            success:function(data){

                document.getElementById("current-sidebar-div").style.display = "block"; 
                $("#current-sidebar-div").html(data);


                get_journals_particulars(journal_entry_id,attached_account_id,type_id);
            },
            error: function(xhr, status, error) {
            //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            alert(error);
            }

      });



    }

    function get_journals_particulars(journal_entry_id,attached_account_id,type_id)
    {

        document.getElementById("supplier_edit_invoices_to").style.display = "none"; 
        // document.getElementById("supplier_edit_invoices_from").style.display = "none"; 

        var config_url = $('#config_url').val();

        var data_url = config_url+"finance/transfer/journal_particulars/"+journal_entry_id;

          $.ajax({
                type:'POST',
                url: data_url,
                data:{journal_entry_id: journal_entry_id},
                dataType: 'text',
                success:function(data){
                    var data = jQuery.parseJSON(data);


                    var account_to_type_id = data.account_to_type_id;
                    var account_to_id = data.account_to_id;
                    var account_from_id = data.account_from_id;

                    if(account_to_type_id > 0)
                    {
                        check_for_invoice_edit(account_from_id,type_id,attached_account_id,journal_entry_id);
                    }


                    var account_from_type_id = data.account_from_type_id;

                    if(account_from_type_id > 0)
                    {
                         check_for_invoice_edit(account_to_id,type_id,attached_account_id,journal_entry_id);
                    }

                    // if(type_id >= 4)
                    // {
                    //     // alert(account_to_id);
                    //     var attached_account_id = account_to_id;
                    //     check_for_invoice_edit(account_from_id,type_id,attached_account_id,journal_entry_id);
                    // }

                    
                },
                error: function(xhr, status, error) {
                //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                alert(error);
                }

          });

          $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd'
                    });
    }

     function close_side_bar()
        {
            // $('html').removeClass('sidebar-right-opened');
            document.getElementById("sidebar-right").style.display = "none"; 
            document.getElementById("current-sidebar-div").style.display = "none"; 
            document.getElementById("existing-sidebar-div").style.display = "none"; 
            tinymce.remove();
        }


   $(document).on("submit","form#edit-journal-data",function(e)
      {
            // alert('dasdajksdhakjh');
            e.preventDefault();
            // myApp.showIndicator();
            
            var form_data = new FormData(this);

            // alert(form_data);

            var config_url = $('#config_url').val();   
            var adjustment_type_id = $('#adjustment_type_id').val();   
            var attached_account_id = $('#attached_account_id').val();    


            var url = config_url+"finance/transfer/edit_journal_data";
            $.ajax({
            type:'POST',
            url: url,
            data:form_data,
            dataType: 'text',
            processData: false,
            contentType: false,
            success:function(data)
            {
              var data = jQuery.parseJSON(data);

                if(data.message == "success")
                {
                  
                  var redirect_url = $('#redirect_url').val();
                  if(attached_account_id > 0)
                  {
                    if(adjustment_type_id == 1)
                    {
                        window.location.href = config_url+'creditor-adjustments/'+attached_account_id;
                    }
                    else if(adjustment_type_id == 2)
                    {
                        window.location.href = config_url+'provider-adjustments/'+attached_account_id;
                    }
                     else if(adjustment_type_id == 3)
                    {
                        window.location.href = config_url+'statutory-adjustments/'+attached_account_id;
                    }
                  }
                  else
                  {
                    // window.location.href = config_url+'accounting/journal-entry';
                    if(adjustment_type_id == 4)
                    {
                        window.location.href = config_url+'journals/director-and-shareholders-journals';
                    }
                    else if(adjustment_type_id == 5)
                    {
                        window.location.href = config_url+'journals/providers-payable-journals';
                    }
                    else if(adjustment_type_id == 6)
                    {
                        window.location.href = config_url+'journals/creditors-payable-journals';
                    }
                    else if(adjustment_type_id == 7)
                    {
                        window.location.href = config_url+'journals/payroll-payable-journals';
                    }

                  }
                
                }
                else
                {
                    alert('Please ensure you have added included all the items');
                }

            },
            error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

            }
            });
             
            
        });
    function check_for_invoice_edit(account_id,type,attached_account_id,journal_entry_id)
    {

            
            var config_url = $('#config_url').val();    

            var url = config_url+"finance/transfer/confirm_for_invoices/"+account_id+"/"+type+"/"+attached_account_id+"/"+journal_entry_id;
            $.ajax({
            type:'POST',
            url: url,
            // data:form_data,
            dataType: 'text',
            // processData: false,
            // contentType: false,
            success:function(data)
            {
              var data = jQuery.parseJSON(data);

                if(data.message == 'success')
                {

                
                    if(type == 1)
                    {

                        document.getElementById("supplier_edit_invoices_to").style.display = "block"; 
                        // document.getElementById("supplier_invoices_from").style.display = "none"; 
                         $( "#invoice_edit_to_id" ).html( data.result );

                         $("#invoice_edit_to_id").customselect();



                    }
                    else if(type == 2)
                    {
                       document.getElementById("supplier_edit_invoices_to").style.display = "block"; 
                        // document.getElementById("supplier_invoices_from").style.display = "none"; 
                         $( "#invoice_edit_to_id" ).html( data.result );

                         $("#invoice_edit_to_id").customselect();
                    }
                    else if(type == 3)
                    {
                        document.getElementById("supplier_edit_invoices_to").style.display = "block"; 
                        // document.getElementById("supplier_invoices_from").style.display = "none"; 
                         $( "#invoice_edit_to_id" ).html( data.result );

                         $("#invoice_edit_to_id").customselect();
                        // document.getElementById("supplier_edit_invoices_from").style.display = "block"; 
                        //  $( "#invoice_edit_from_id" ).html( data.result );

                        //  $("#invoice_edit_from_id").customselect();
                    }

                    
                }
                else
                {
                    // document.getElementById("supplier_invoices_to").style.display = "none"; 
                    // document.getElementById("supplier_invoices_from").style.display = "none"; 
                }
               
               

            },
            error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

            }
            });
             
    }

</script>
