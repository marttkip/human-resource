<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_model extends CI_Model {

    public function insert_bank($data) {
        $this->db->insert('bank', $data);
        return $this->db->insert_id(); // returns the inserted bank's ID
    }
}
?>
