<style type="text/css">
/* Style for the sidebar */
.nano {
    background-color: #f8f9fa; /* Background color for the sidebar */
    border-right: 1px solid #dee2e6; /* Border on the right side */
    width: 250px; /* Width of the sidebar */
    height: 100vh; /* Full height */
}

/* Style for the sidebar content */
.nano-content {
    padding: 0; /* Remove padding */
    overflow: hidden; /* Prevent scrollbar overflow */
}

/* Style for the navigation menu */
.nav-main {
    list-style: none; /* Remove default list styles */
    padding: 0; /* Remove default padding */
    margin: 0; /* Remove default margin */
}

/* Style for the menu items */
.nav-main > li {
    position: relative; /* Set position relative for dropdown functionality */
}

/* Style for the menu links */
.nav-main a {
    display: flex; /* Use flexbox for layout */
    align-items: center; /* Center items vertically */
    color: #212529; /* Text color */
    text-decoration: none; /* Remove underline */
    padding: 10px 15px; /* Padding around text */
    font-size: 16px; /* Font size */
    background-color: #f8f9fa; /* Background color */
}

/* Style for the icons */
.nav-main a i {
    margin-right: 10px; /* Space between icon and text */
    color: #6c757d; /* Icon color */
}

/* Style for the submenu items */
.nav-main .submenu {
    display: none; /* Hide submenu by default */
    position: absolute; /* Position it absolutely */
    background-color: #fff; /* Background color */
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.15); /* Shadow effect */
    border-radius: 4px; /* Rounded corners */
    top: 100%; /* Position it below the parent */
    left: 0; /* Align to the left of the parent */
    z-index: 1000; /* Ensure it's on top */
}

/* Show the submenu on hover */
.nav-main .dropdown:hover .submenu {
    display: block; /* Show the submenu on hover */
}

/* Style for submenu links */
.nav-main .submenu a {
    display: flex; /* Use flexbox for layout */
    align-items: center; /* Center items vertically */
    padding: 10px 15px; /* Padding around text */
    font-size: 14px; /* Font size */
    color: #212529; /* Text color */
}

/* Style for submenu icons */
.nav-main .submenu a i {
    margin-right: 10px; /* Space between icon and text */
    color: #6c757d; /* Icon color */
}

/* Style for submenu on hover */
.nav-main .submenu a:hover {
    background-color: #e2e6ea; /* Background color on hover */
    color: #0056b3; /* Text color on hover */
}

/* Style for caret icon */
.nav-main .dropdown .caret {
    margin-left: 10px; /* Space between text and caret */
}

/* Optional: Active link style */
.nav-main a.active {
    background-color: #007bff; /* Background color for active link */
    color: #fff; /* Text color for active link */
}

</style>