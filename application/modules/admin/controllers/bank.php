<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
class Bank extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('excel');
        $this->load->model('Bank_model');
        $this->load->model('site/site_model');
        $this->load->model('admin/admin_model');
        $this->load->model('admin/sections_model');
        $this->load->model('Bank_branch_model');
    }

    public function upload() {

    	$v_data['title'] = $data['title'] = $this->site_model->display_page_title();
		
		$data['content'] = $this->load->view('bank/bank_import', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);


        // $this->load->view('upload_form');
    }

    public function process_upload() {
        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'xlsx|xls';
        $config['max_size'] = 2048;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('bank/bank_import', $error);
        } else {
            $data = $this->upload->data();
            $file_path = './assets/uploads/' . $data['file_name'];
            $this->load_excel_data($file_path);
        }
    }

   private function load_excel_data($file_path) {
    // Load the spreadsheet
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file_path);
    $combined_data = [];

    // Read data from all three sheets and combine
    for ($i = 0; $i < 3; $i++) {
        // Get the sheet data and ensure values are read as strings to preserve leading zeros
        $sheet_data = $spreadsheet->getSheet($i)->toArray(null, true, true, true);
        array_shift($sheet_data); // Remove header row
        
        foreach ($sheet_data as &$row) {
            // Convert all cell values to strings to preserve leading zeros
            $row = array_map('strval', $row);
        }
        
        $combined_data = array_merge($combined_data, $sheet_data);
    }

    // Insert bank and branch data
    $bank_map = [];
    foreach ($combined_data as $row) {
        // Ensure we have at least 4 columns
        if (count($row) < 4) {
            continue;
        }

        list($bank_name, $bank_code, $branch_code, $branch_name) = array_values($row);

        // Check if any of the values are NULL or empty
        if (empty($bank_name) || empty($bank_code) || empty($branch_code) || empty($branch_name)) {
            // Skip the current iteration if data is NULL or empty
            continue;
        }

        if (!isset($bank_map[$bank_name])) {
            $bank_data = [
                'bank_name' => $bank_name,
                'bank_code' => $bank_code
            ];
            $bank_id = $this->Bank_model->insert_bank($bank_data);
            $bank_map[$bank_name] = $bank_id;
        } else {
            $bank_id = $bank_map[$bank_name];
        }

        $branch_data = [
            'bank_id' => $bank_id,
            'branch_code' => $branch_code,
            'branch_name' => $branch_name
        ];
        $this->Bank_branch_model->insert_branch($branch_data);
    }

    echo "Data imported successfully!";
}

}
?>
