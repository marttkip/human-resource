
<?php

$this->db->where('account_staging_delete = 0');
$accounts_config_rs = $this->db->get('account_staging');

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;


	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];
$cost_of_goods_id = $session_account['cost_of_goods_id'];
$expense_account_id = $session_account['expense_account_id'];
$other_income_account_id = $session_account['other_income_account_id'];

error_reporting(0);
// echo "<br>I'm at: ". getcwd();
include_once "application/modules/financials/controllers/financial_report.php";
$financial_class = new Financial_Report(array("detailedPnl"=>true,
			"date" => $this->session->userdata('date_to_general_ledger')));

$date_from = $this->session->userdata('date_from_general_ledger');
$date_to = $this->session->userdata('date_to_general_ledger');
$arrDone = array();

$start = strtotime($date_from);
$end = strtotime($date_to);


$current = $start;
$ret = array();

while( $current<=$end ){

    $date_checked = date("Y-m-d",$current);
    $next = date('Y-M-01', $current) . "+1 month";

    $current = strtotime($next);

    $ret[] = $date_checked;
}

// $args = array("Income Account","Expense Account");
$pnl_accounts_rs = $this->company_financial_model->get_accounts_by_type("Income Account");
$expense_accounts_rs = $this->company_financial_model->get_accounts_by_type("Expense Account");



// echo "<pre>";
// print_r($pnl_accounts_rs->result());
// echo "</pre>";

// echo "<br>Printing final";
// echo "<pre>";
// print_r($financial_class->arrdataDetailed());
// echo "</pre>";
// $all_transacted_rs = $this->company_financial_model->get_account_transactions_ledgers_new(array("for_pnl" => true));

// foreach($all_transacted_rs->result() as $item){
// 	if(!in_array($item->transactionCategory,$arrDone))
// 	{
// 		var_dump($item);
// 	}

// }
// echo "</pre>";


$statement = $this->session->userdata('income_statement_title_search');

// var_dump($statement);die();

if(!empty($statement))
{
	$checked = $statement;
}
else {
	$checked = 'Reporting period: '.date('M j, Y', strtotime(date('Y-01-01'))).' to ' .date('M j, Y', strtotime(date('Y-m-d')));
}


// $closing_stock =  $this->company_financial_model->get_opening_stock_value();


?>

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$general_ledger_search_title =	$this->session->userdata('general_ledger_search_title');


	if(!empty($general_ledger_search_title))
	{
		$search_title = $general_ledger_search_title;
	}
	else
	{
		$search_title = 'ALL TIME REPORT';
	}

?>


<div class="col-md-4">
	<?php echo $this->load->view('search/search_profit_and_loss','', true);?>

	<div class="text-center">
		<h3 class="box-title">Income Statement</h3>
		<h5 class="box-title"> <?php echo $search_title?></h5>
		<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
	</div>

	<div class="form-group">
	<?php
			$search_status = $this->session->userdata('income_statement_search');
			$search_title = 'INCOME STATEMENT '.$search_title;
			if(!empty($search_status))
			{
				echo '<a href="'.site_url().'financials/company_financial/close_income_statement_search" class="btn btn-sm btn-warning">Close search</a>';
			}
			?>
	</div>
	<div class="col-md-12" style="margin-bottom:20px">
		<div class="col-md-12">
			<div class="form-group">
				  <a href="<?php echo site_url().'print-detailed-income-statement'?>" target="_blank" class="btn btn-md btn-warning col-md-12"><i class="fa fa-print"></i> Print Detailed P&L</a>
			</div>
		</div>

	</div>
	<br>
	<div class="col-md-12">
		<div class="form-group">
		      <a href="<?php echo site_url().'company-financials/profit-and-loss'?>"  class="btn btn-md btn-default col-md-12"><i class="fa fa-file"></i> Income Statement</a>
		</div>
		<!-- <div class="form-group">
		      <a href="<?php echo site_url().'company-financials/balance-sheet'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Balance Sheet</a>
		</div>
		<div class="form-group">
		      <a href="<?php echo site_url().'company-financials/trial-balance'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Trial Balance</a>
		</div>
		<div class="form-group">
		      <a href="<?php echo site_url().'company-financials/general-ledger'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> General Ledger</a>
		</div> -->
	</div>

</div>
<div class="col-md-8">

	<section class="panel">



			<!-- /.box-header -->
			<div class="panel-body" style="height:85vh;overflow-y:scroll;padding: 0px !important;">

				<table class="table table-condensed table-striped table-bordered table-linked" id="testTable">
					<thead>

						<th style="z-index: 2;background: #cb6d51;color: #fff !important;text-align: left;">NAME</th>

						<?php
						if(is_array($ret))
						{

							$total_count = count($ret);

							for ($i=0; $i < $total_count; $i++) {
								// code...

								$marked_date = $ret[$i];

								$date_explode = date("M Y",strtotime($marked_date));

								echo "<th style='z-index: 2;background: #cb6d51;color: #fff !important;text-align: right;'>".strtoupper($date_explode)."</th>";
							}

						}
						?>

					</thead>
					<tbody>
						<tr>
							<th class="text-left" colspan="12" style="background-color:#3c8dbc;color:#fff;">INCOMES</th>
						</tr>
						<?php

						$total_count = count($ret);
						$list = '';
						$grand_balance[$year][$month]["amount"] = 0;
						foreach ($pnl_accounts_rs->result() as $key => $value) {
							// code...

							$account_id = $value->account_id;
							$account_name = $value->account_name;
							$account_type_name = $value->account_type_name;

							if(array_key_exists($account_id,$financial_class->arrdataDetailed()["incomes"]) AND is_array($financial_class->arrdataDetailed()["incomes"][$account_id]))
							{


								$list .= '<tr>
											<td class="text-left">'.strtoupper($account_name).'</td>
											';

								for ($i=0; $i < $total_count; $i++) {
								// code...

									$marked_date = $ret[$i];


									$date_list = strtotime($marked_date.'-01');
									$year = date("Y",strtotime($marked_date));
									$month = (int)date("m",strtotime($marked_date));
									// $marker =

									$item = $financial_class->arrdataDetailed()["incomes"][$account_id][$year][$month]["amount"];

									$list .="<td style='text-align: right;'><a onclick='get_account_items(".$account_id.",".$date_list.",\"incomes\")'>".number_format($item,2)."</a></td>";
									$grand_balance[$year][$month]["amount"] += $item;

								}

								$list .= '
										</td>';
							}


						}
						echo $list;
						?>
						<tr>
							<th class="text-left" colspan="1" style="background-color:#3c8dbc;color:#fff;">TOTALS</th>
							<?php

							for ($i=0; $i < $total_count; $i++) {

									$marked_date = $ret[$i];

									$year = date("Y",strtotime($marked_date));
									$month = (int)date("m",strtotime($marked_date));

									$amount = $grand_balance[$year][$month]["amount"];
								?>
									<th class="text-left" style="background-color:#3c8dbc;color:#fff;border-top:2px solid #000;text-align: right;"><?php echo number_format($amount,2)?></th>
								<?php

							}
							?>

						</tr>
						<tr>
							<th class="text-left" colspan="12" style="background-color:#3c8dbc;color:#fff;">EXPENSES</th>
						</tr>
						<?php
						$list_two = "";

						$grand_expense[$year][$month]["amount"] = 0;
						foreach ($expense_accounts_rs->result() as $key => $value) {
							// code...

							$account_id = $value->account_id;
							$account_name = $value->account_name;
							$account_type_name = $value->account_type_name;
							if(array_key_exists($account_id,$financial_class->arrdataDetailed()["expenses"]) AND is_array($financial_class->arrdataDetailed()["expenses"][$account_id]))
							{


								$list_two .= '<tr>
											<td class="text-left">'.strtoupper($account_name).'</td>
											';

								for ($i=0; $i < $total_count; $i++) {
								// code...

									$marked_date = $ret[$i];

									$date_list = strtotime($marked_date.'-01');

									$year = date("Y",strtotime($marked_date));
									$month = (int)date("m",strtotime($marked_date));


									$item = $financial_class->arrdataDetailed()["expenses"][$account_id][$year][$month]["amount"];

									$grand_expense[$year][$month]["amount"] += $item;

									$list_two .="<td style='text-align: right;'><a onclick='get_account_items(".$account_id.",".$date_list.",\"expenses\")'>".number_format($item,2)."</a></td>";


								}

								$list_two .= '
										</td>';
							}


						}
						echo $list_two;
						?>

						<tr>
							<th class="text-left" colspan="1" style="background-color:#3c8dbc;color:#fff;">TOTALS</th>
							<?php

							for ($i=0; $i < $total_count; $i++) {

									$marked_date = $ret[$i];

									$year = date("Y",strtotime($marked_date));
									$month = (int)date("m",strtotime($marked_date));

									$amount = $grand_expense[$year][$month]["amount"];
								?>
									<th class="text-left" style="background-color:#3c8dbc;color:#fff;border-top:2px solid #000;text-align: right;"><?php echo number_format($amount,2)?></th>
								<?php

							}
							?>

						</tr>


						<tfoot>
							<tr>
			        			<th class="text-left"><strong>NET PROFIT / LOSS</strong></th>
			        			<?php

										for ($i=0; $i < $total_count; $i++) {

													$marked_date = $ret[$i];

													$year = date("Y",strtotime($marked_date));
													$month = (int)date("m",strtotime($marked_date));
													$income_amount = $grand_balance[$year][$month]["amount"];
													$expense_amount = $grand_expense[$year][$month]["amount"];

													$profit_or_loss = $income_amount - $expense_amount;
												?>
													<th class="text-left"  style="z-index: 2;background: #cb6d51;color: #fff !important;text-align: right;border-top:2px solid #000;"><?php echo number_format($profit_or_loss,2)?></th>
												<?php

											}
										?>
							</tr>
						</tfoot>
					</tbody>
				</table>



				</table>

	    	</div>
	</section>
</div>

<script type="text/javascript">

	function get_account_items(account_id,marked_date,transaction)
	{
		// alert(transaction);
			document.getElementById("sidebar-right").style.display = "block";
			var config_url = $('#config_url').val();
			var data_url = config_url+"financials/company_financial/account_transactions_list/"+account_id+"/"+marked_date;

			$.ajax({
			type:'POST',
			url: data_url,
			data:{appointment_id: 1,transaction:transaction},
			dataType: 'text',
			success:function(data){

					document.getElementById("current-sidebar-div").style.display = "block";
					$("#current-sidebar-div").html(data);

				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});

	}


</script>
