<?php

// $date_from = $this->session->userdata('date_from_general_ledger');
    $date_to = $this->session->userdata('date_to_general_ledger');
    $add = '';
    if(!empty($date_from) OR !empty($date_to))
    {

        $date_from = $date_from;
        $date_to = $date_to;

    }

    else
    {

        $date_from = date('Y-m-01');
        $date_to = date('Y-m-d');
        

    }

    $shareholder_id = $this->session->userdata('shareholder_id');
?>

<section class="panel">
		<header class="panel-heading">
				<h5 class="pull-left"><i class="icon-reorder"></i>Search </h5>
				<div class="clearfix"></div>
		</header>
		<!-- /.box-header -->
		<div class="panel-body">
			<div class="col-md-12">
	
            <?php
            echo form_open("financials/company_financial/search_income_statement", array("class" => "form-horizontal"));
            ?>
            <div class="col-md-12">
                

           
                    <div class="form-group" style="display:none;">
                        <label class="col-md-4 control-label">Date From: </label>

                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Visit Date" value="<?php echo $date_from?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">


               		<div class="form-group">
                      <label class="col-md-4 control-label">Shareholders: </label>

                      <div class="col-md-8">
                          	<select name="shareholder_id" class="form-control">

                          		<?php

                          		if($shareholder_id)
                          		{

                          		}
                          		else
                          		{
                          			?>
                          			<option value="">----Select a Shareholder----</option>
                          			<?php

                          		}
                          		?>
								
								<?php
									$doctor = $this->reception_model->get_all_personnel_doctors();					
									if($doctor->num_rows() > 0){

										foreach($doctor->result() as $row => $close):
											$fname = $close->personnel_fname;
											$onames = $close->personnel_onames;
											$personnel_id = $close->personnel_id;
											
											if($personnel_id == $shareholder_id)
											{
												echo "<option value='".$personnel_id."' selected='selected'>".$onames." ".$fname."</option>";
											}
											
											else
											{
												echo "<option value='".$personnel_id."'>".$onames." ".$fname."</option>";
											}
										endforeach;
									}
								?>
							</select>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-4 control-label">Date To: </label>

                      <div class="col-md-8">
                          <div class="input-group">
                              <span class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                              </span>
                              <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Visit Date" value="<?php echo $date_to;?>" autocomplete="off" required>
                          </div>
                      </div>
                  </div>

               
                	 <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-4">
                        	<div class="center-align">
                           		<button type="submit" class="btn btn-info">Search</button>
            				</div>
                        </div>
                    </div>
            </div>



            <?php
            echo form_close();
            ?>
                		
	</div>
  </div>
</section>
