
<!--end reports -->

<section class="panel ">
    <header class="panel-heading">

        <h2 class="panel-title"><?php echo strtoupper($title);?></h2>
        
        <a onclick="add_new_staging_account()" class="btn btn-sm btn-success pull-right" style="margin-top: -25px;"><i class="fa fa-cancel" ></i> Add New Staging Account</a>
    </header>

    <div class="panel-body">


		
		<div id="view-staging-accounts"></div>
		

  	</div>
</section>
   

<script type="text/javascript">


	$(document).ready(function(){

		get_all_staging_accounts();
		
	});

	function get_all_staging_accounts()
	{

		var config_url = $('#config_url').val();
	    var data_url = config_url+"financials/company_financial/view_staging_accounts";
	      // window.alert(data_url);
		$.ajax({
		  type:'POST',
		  url: data_url,
		  data:{visit_id: 1},
		  dataType: 'text',
		  success:function(data){
		  
		   	$("#view-staging-accounts").html(data);

		  },
		  error: function(xhr, status, error) {
		  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		  alert(error);
		  }

		});

	}

	function update_staging_accounts(account_staging_id)
	{
		 var config_url = $('#config_url').val();



		 var account_id = $('#account_id'+account_staging_id).val();
		var has_children = $('#has_children'+account_staging_id).val();
		var bs_account_position = $('#bs_account_position'+account_staging_id).val();
		var bs_account_status = $('#bs_account_status'+account_staging_id).val();
		var bs_account_balance_report = $('#bs_account_balance_report'+account_staging_id).val();
		var bs_account_type_id = $('#bs_account_type_id'+account_staging_id).val();
		var reference_name = $('#reference_name'+account_staging_id).val();
		var account_staging_name = $('#account_staging_name'+account_staging_id).val();
		var bs_account_income_status = $('#bs_account_income_status'+account_staging_id).val();
	
                    
        var url = config_url+"financials/company_financial/update_staging_accounts/"+account_staging_id;
    
        $.ajax({
        type:'POST',
        url: url,
        data:{has_children: has_children,bs_account_position: bs_account_position,bs_account_status: bs_account_status, bs_account_balance_report: bs_account_balance_report, reference_name: reference_name,account_staging_name:account_staging_name,bs_account_type_id: bs_account_type_id,bs_account_income_status: bs_account_income_status,account_id: account_id},
        dataType: 'text',
        success:function(data){
          // var data = jQuery.parseJSON(data);
            
           	// get_all_staging_accounts();
         

        },
        error: function(xhr, status, error) {
        alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

        }
        });
	}

	function remove_staging_account(account_staging_id)
	{

		var res = confirm('Are you sure you want to remove the staging account ?');

		if(res)
		{


			var config_url = $('#config_url').val();
		    var data_url = config_url+"financials/company_financial/remove_staging_account/"+account_staging_id;
		      // window.alert(data_url);
			$.ajax({
			  type:'POST',
			  url: data_url,
			  data:{visit_id: 1},
			  dataType: 'text',
			  success:function(data){
			  
			   	get_all_staging_accounts();

			  },
			  error: function(xhr, status, error) {
			  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			  alert(error);
			  }

			});

		}

	}

	function add_new_staging_account()
	{	

		open_sidebar();

		var config_url = $('#config_url').val();
	      var data_url = config_url+"financials/company_financial/add_staging_account";
	      // window.alert(data_url);
	      $.ajax({
	      type:'POST',
	      url: data_url,
	      data:{visit_id: 1},
	      dataType: 'text',
	      success:function(data){
	     
	      	 $("#sidebar-div").html(data);

	      },
	      error: function(xhr, status, error) {
	      //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	      alert(error);
	      }

	      });

	}

	$(document).on("submit","form#add-staging-account",function(e)
	{
		// alert("sahjahsjagsajh");

		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		// alert('shjsagjagj')
		var config_url = $('#config_url').val();	

		 var url = config_url+"financials/company_financial/save_new_account";
		 // var values = $('#visit_type_id').val();
		 
	       $.ajax({
	       type:'POST',
	       url: url,
	       data:form_data,
	       dataType: 'text',
	       processData: false,
	       contentType: false,
	       success:function(data){
	          var data = jQuery.parseJSON(data);
	        
	          	if(data.message == "success")
				{
					close_side_bar();
	    			get_all_staging_accounts();
				}
				else
				{
					alert('Please ensure you have added included all the items');
				}

		
	       
	       },
	       error: function(xhr, status, error) {
	       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	       
	       }
	       });
		 
		
	   
		
	});
</script>