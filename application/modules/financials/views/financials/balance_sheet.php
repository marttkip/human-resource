<?php
$date_from = $this->session->userdata('date_from_general_ledger');
$explode = explode('-', $date_from);
$current_year = $explode[0];
$previous_year_earnings = $explode[0] -1;
$projected_account_id = $account_id;
	$date_from = $this->session->userdata('date_from_general_ledger');
	$date_to = $this->session->userdata('date_to_general_ledger');
	$add = '';
	if(!empty($date_from) OR !empty($date_to))
	{

		$date_from = $date_from;
		$date_to = $date_to;

	}

	else
	{

		$date_from = date('Y-m-01');
		$date_to = date('Y-m-d');
		

	}

	$search_title = $this->session->userdata('general_ledger_search_title');

?>
<?php

$grand_income = 0;
$income_result = '';


error_reporting(0);
// echo "<br>I'm at: ". getcwd();
include_once "application/modules/financials/controllers/financial_report.php";

// include_once ""
$financial_class = new Financial_Report(array("balance_sheet"=>true,
			"date" => $this->session->userdata('date_to_general_ledger')));
// $financial_class = new Financial_Report(array("date" => $this->session->userdata('date_to_general_ledger')));



// echo "<br>Printing final";

// echo "<pre>";
// // print_r($financial_class->arrAssetTypes());
// print_r($financial_class->arrData());
// echo "</pre>";



$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;

		
	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];
$cost_of_goods_id = $session_account['cost_of_goods_id'];
$expense_account_id = $session_account['expense_account_id'];
$other_income_account_id = $session_account['other_income_account_id'];
$bank_id = $session_account['bank_id'];
$capital_account_id  = $session_account['capital_account_id'];
$loans_account_id  = $session_account['loans_account_id'];
$acc_depreciation_id = $session_account['acc_depreciation_id'];
$asset_depreciation_id = $session_account['asset_depreciation_id'];

// all income accounts




$search = $this->session->userdata('balance_sheet_title_search');

if(!empty($search))
{
	$balance_sheet_search = ucfirst($search);
}
else {

	
	$balance_sheet_search = 'Reporting as for: '.date("Y-01-01").' to '.date("Y-m-d");
}

$total_current_assets = $total_fixed+$total_income+$accounts_receivable;
?>

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$general_ledger_search_title =	$this->session->userdata('general_ledger_search_title');


	if(!empty($general_ledger_search_title))
	{
		$search_title = $general_ledger_search_title;
	}
	else 
	{
		$search_title = 'ALL TIME REPORT';
	}

?>


<style>
	td .match
	{
		border-top: #000 2px solid !important;
	}
</style>

<div class="col-md-4">

	<?php echo $this->load->view('search/search_balance_sheet','', true);?>


	<div class="text-center">
		<h4 class="box-title">Balance Sheet</h4>
		<h5 class="box-title"> <?php echo $search_title?> </h5>
		<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
	</div>
	<?php
	$search_title = 'BALANCE SHEET '.$search_title;
	?>
		<div class="col-md-12">
			<div class="col-md-12" style="margin-bottom:20px">
			
				<div class="form-group">
					<a href="<?php echo site_url().'print-balance-sheet'?>" target="_blank" class="btn btn-md btn-warning col-md-12"><i class="fa fa-print"></i> Print Balance Sheet</a>
				</div>
		
			
		</div>
		<br>
		<div class="col-md-12">
			
		    <div class="form-group">
			      <a href="<?php echo site_url().'company-financials/profit-and-loss'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Income Statement</a>
			</div>
			<div class="form-group">
			      <a href="<?php echo site_url().'company-financials/trial-balance'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Trial Balance</a>
			</div>
			<div class="form-group">
			      <a href="<?php echo site_url().'company-financials/general-ledger'?>"  class="btn btn-md btn-warning col-md-12"><i class="fa fa-file"></i> General Ledger</a>
			</div>
			
  		</div>
	</div>

   

	
</div>
<div class="col-md-8">
	

	<section class="panel">
		<div class="panel-body" style="height:85vh;overflow-y:scroll;padding:0px;">
    	<!-- <h4 class="box-title">Asssets</h4> -->
    	<!-- <h4 class="box-title">Fixed Assets</h4> -->
    	<table class="table  table-striped table-condensed table-bordered" id="<?php echo $search_title?>">
    		<thead>

				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">ASSETS</th>
				</tr>
				
				<tr>
					<th class="text-left" colspan="2" >CURRENT ASSETS</th>
				</tr>
				
			</thead>

			<tbody>

				<!-- ASSETS -->

				<?php


					
					// code...

					$total_current_assets = 0;
					//Current Assets
					foreach($financial_class->arrAssetTypes()["c"] as $asset)
					//foreach ($financial_class->arrData()["assets"] as $key => $value) {
						if(array_key_exists($asset, $financial_class->arrData()["assets"])){
							$value = $financial_class->arrData()["assets"][$asset];

							$name = $value['name'];
							$amount = $value['amount'];

							$section = 'assets';
							$total_current_assets += $amount;
							echo "<tr>
											<td class='text-left'>".strtoupper($name)."</td>
											<td class='text-right'><a onclick='get_account_items(".$asset.",\"".$section."\")'>".number_format($amount,2)."</a></td>
										</tr>";

					}
					echo '<tr>
								<th style="padding-left:20px !important; ">TOTAL CURRENT ASSETS</th>
								<th class="text-right" style="border-top: 2px solid #000;">'.number_format($total_current_assets,2).'</th>
							</tr>';

				echo '<tr>
						<th class="text-left" colspan="2" >FIXED ASSETS</th>
					</tr>';

					$total_fixed_assets = 0;
					//Fixed Assets
					foreach($financial_class->arrAssetTypes()["f"] as $asset)
						if(array_key_exists($asset, $financial_class->arrData()["assets"])){
							$value = $financial_class->arrData()["assets"][$asset];

							$name = $value['name'];
							$amount = $value['amount'];

							$section = 'assets';
							$total_fixed_assets += $amount;
							echo "<tr>
											<td class='text-left'>".strtoupper($name)."</td>
											<td class='text-right'><a onclick='get_account_items(".$asset.",\"".$section."\")'>".number_format($amount,2)."</a></td>
										</tr>";

					}


					echo '<tr>
								<th style="padding-left:20px !important; ">TOTAL FIXED ASSETS</th>
								<th class="text-right" style="border-top: 2px solid #000;">'.number_format($total_fixed_assets,2).'</th>
							</tr>';
				$total_assets = $total_current_assets + $total_fixed_assets;

				echo '<tr>
								<th style="">TOTAL  ASSETS</th>
								<th class="text-right" style="border-top: 2px solid #000;">'.number_format($total_assets ,2).'</th>
							</tr>';
				?>

				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">EQUITY</th>
				</tr>
				
				<?php

				$total_equity = 0;
				foreach ($financial_class->arrData()["equities"] as $key => $value) {

						$name = $value['name'];
						$amount = $value['amount'];

						$section = 'equities';
						$total_equity += $amount;
						echo "<tr>
										<td class='text-left'>".strtoupper($name)."</td>
										<td class='text-right'><a onclick='get_account_items(".$key.",\"".$section."\")'>".number_format($amount,2)."</a></td>
									</tr>";

				}
				$section = 'equities';
				$total_equity += $financial_class->getProfit();
				echo "<tr>
						<td class='text-left'>PROFIT</td>
						<td class='text-right'><a onclick='get_account_items(".$accounts_receivable_id.",\"".$section."\")' >".number_format($financial_class->getProfit(),2)."</a></td>
					</tr>";
				echo '<tr>
							<th style="">TOTAL EQUITIES</th>
							<th class="text-right" style="border-top: 2px solid #000;">'.number_format($total_equity,2).'</th>
						</tr>';
				?>
				
				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">LIABILITIES</th>
				</tr>
				
				<?php

				$total_liability = 0;
				foreach ($financial_class->arrData()["liabilities"] as $key => $value) {

						$name = $value['name'];
						$amount = $value['amount'];

						$section = 'equities';
						$total_liability += $amount;
						echo "<tr>
										<td class='text-left'>".strtoupper($name)."</td>
										<td class='text-right'><a onclick='get_account_items(".$key.",\"".$section."\")' >".number_format($amount,2)."</a></td>
									</tr>";

				}

				echo '<tr>
							<th style="">TOTAL LIABILITY</th>
							<th class="text-right" style="border-top: 2px solid #000;">'.number_format($total_liability,2).'</th>
						</tr>';


				$totals = $total_equity + $total_liability;


				echo '<tr style="color:'.(round($totals,2) == round($total_assets,2) ? "limegreen" : "red").'">
							<th style="">TOTAL EQUITIES & LIABILITIES</th>
							<th class="text-right" style="border-top: 2px solid #000;">'.number_format($totals,2).'</th>
						</tr>';
				?>


				
			</tbody>

			
				
		
		</table>
		
    </div>

	</section>
</div>


<script type="text/javascript">
		var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};


function get_account_items(account_id,transaction)
{
	// alert(transaction);
		document.getElementById("sidebar-right").style.display = "block"; 
		var config_url = $('#config_url').val();
		var data_url = config_url+"financials/company_financial/transactions_list/"+account_id;
	
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1,transaction:transaction},
		dataType: 'text',
		success:function(data){

				document.getElementById("current-sidebar-div").style.display = "block"; 
				$("#current-sidebar-div").html(data);

			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});

}


</script>

