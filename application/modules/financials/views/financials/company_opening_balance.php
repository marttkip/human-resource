<?php
$account_rs = $this->company_financial_model->get_all_opening_balances();

$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;


	}
}
$retained_earnings_id = $session_account['retained_earnings_id'];
?>
<!--end reports -->
<div class="col-md-12">
	<div class="col-md-4">
		<section class="panel ">
            <header class="panel-heading">
                <h2 class="panel-title"><?php echo strtoupper($title);?></h2>
               
            </header>

            <div class="panel-body">
            	<?php
            	$reserved_accounts = $this->company_financial_model->get_reserved_accounts();

            	

            	?>

            	<?php
			    echo form_open("financials/company_financial/update_opening_balance", array("class" => "form-horizontal"));
			    ?>

			       
			        <div class="form-group" >
			            <label class="col-md-4 control-label">Account Name (Account Debited): </label>

			            <div class="col-md-8">

			            	<select class="form-control  select2" name="account_id" id="account_id" onchange="get_account_opening_balances(this.value)" required>
		                          <option value="">--- select an account - ---</option>
		                          <?php
		                          if($reserved_accounts->num_rows() > 0)
		                          {
		                            foreach ($reserved_accounts->result() as $key => $value) {
		                              // code...
		                              $account_id = $value->account_id;
		                              $account_name = $value->account_name;
		                              $parent_account = $value->parent_account;

		                              if($parent_account == 0)
		                              	$account_name = '<i>'.$account_name.'</i>';
		                              else
		                              	$account_name = '<span style="margin-left: 10px;"></span> - '.$account_name;


		                              echo '<option value="'.$account_id.'"> '.$account_name.'</option>';
		                            }
		                          }
		                          ?>
	                        </select>
			               
			            </div>
			        </div>
			        <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
			        <br>
			        <span class="col-md-12" id="items" style="display:none;margin-top: 5px;">
			        	
			        	<div class="form-group">
			              <label class="col-md-4 control-label">Opening Amount: </label>

			              <div class="col-md-8">
			                  <input type="text" class="form-control" name="balance" id="balance" placeholder="Amount"  autocomplete="off" required>
			              </div>
			          </div>
			          <div class="form-group" >
				            <label class="col-md-4 control-label">Account From Name (Credited Account):  </label>

				            <div class="col-md-8">

				            	<select class="form-control  select2" name="account_from_id" id="account_from_id"  >
			                          <option value="0">--- no account ---</option>
			                          <?php
			                          if($reserved_accounts->num_rows() > 0)
			                          {
			                            foreach ($reserved_accounts->result() as $key => $value) {
			                              // code...
			                              $account_id = $value->account_id;
			                              $account_name = $value->account_name;
			                              if($account_id == $retained_earnings_id)
			                              echo '<option value="'.$account_id.'"> '.$account_name.'</option>';
			                            }
			                          }
			                          ?>
		                        </select>
				               
				            </div>
				        </div>
			
			          <div class="form-group">
			              <label class="col-md-4 control-label">Date: </label>

			              <div class="col-md-8">
			                  <div class="input-group">
			                      <span class="input-group-addon">
			                          <i class="fa fa-calendar"></i>
			                      </span>
			                      <input data-format="yyyy-MM-dd" type="text"  class="form-control" name="opening_balance_date" id="opening_balance_date" placeholder="Date" value="" autocomplete="off" required>
			                  </div>
			              </div>
			          </div>

			        <div class="form-group">
			                <div class="col-lg-8 col-lg-offset-4">
			                	<div class="center-align">
			                   		<button type="submit" class="btn btn-info" onclick="return confirm('Are you sure you want to update the opening balance ? ')">UPDATE ACCOUNT</button>
			    				</div>
			                </div>
		         	</div>

			     </span>



			    <?php
			    echo form_close();
			    ?>
    

            </div>
        </section>
	</div>
    <div class="col-md-8">

        <section class="panel ">
            <header class="panel-heading">

                <h2 class="panel-title"><?php echo strtoupper($title);?></h2>
                <!-- <a href="<?php echo site_url();?>company-financials/balance-sheet"  class="btn btn-sm btn-info pull-right" style="margin-top:-25px;margin-left:5px" > Back to Balance Sheet </a> -->
               
            </header>

            <div class="panel-body">


			<?php
				if(!empty($ledger_search))
				{
					?>
	                <a href="<?php echo base_url().'accounting/petty_cash/close_expense_ledger';?>" class="btn btn-sm btn-danger"><i class="fa fa-cancel"></i> Close Search</a>
	                <?php
				}
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');

				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}

				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}

				// echo $result;

				$count = 0;
				$items = '';
				if($account_rs->num_rows() > 0)
				{
					foreach ($account_rs->result() as $key => $value) {
						// code...
						$account_name = $value->account_name;
						$balance = $value->balance;
						$opening_balance_date = $value->opening_balance_date;
						$account_from_id = $value->account_from_id;

						$account_credited_name = '';
						if($account_from_id)
							$account_credited_name = $this->company_financial_model->get_account_name($account_from_id);
						
						$count++;
						$items .= '<tr>
										<td>'.$count.'</td>
										<td>'.date('jS M Y',strtotime($opening_balance_date)).'</td>
										<td>'.ucwords(strtolower($account_name)).'</td>
										<td>'.number_format($balance,0).'</td>
										<td>'.ucwords(strtolower($account_credited_name)).'</td>
									</tr>';
					}
				}
			?>			
				<table class="table table-hover table-bordered ">
				 	<thead>
						<tr>
						  <th></th>
						  <th>DATE</th>
						  <th>ACCOUNT DEBITED</th>
			              <th>AMOUNT</th>
			              <th>ACCOUNT CREDITED</th>
						</tr>
					 </thead>
				  	<tbody>
	          			<?php echo $items;?>
					</tbody>
				</table>

          	</div>
		</section>
    </div>
   
</div>
<script type="text/javascript">
	
	function get_account_opening_balances(account_id)
	{


        var url = "<?php echo site_url();?>financials/company_financial/account_opening_balance/"+account_id;  
        $('#items').css('display', 'none');

        $.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data);
			if(data.message == "success")
			{
				// $("#sidebar-detail").html(data.result);
				$('#items').css('display', 'block');

				document.getElementById("balance").value = data.balance;
				document.getElementById("opening_balance_date").value = data.opening_balance_date;
			
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
       


	}
</script>