
<?php

$this->db->where('account_staging_delete = 0');
$accounts_config_rs = $this->db->get('account_staging');

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;


	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];
$cost_of_goods_id = $session_account['cost_of_goods_id'];
$expense_account_id = $session_account['expense_account_id'];
$other_income_account_id = $session_account['other_income_account_id'];

// error_reporting(0);
// echo "<br>I'm at: ". getcwd();
include_once "application/modules/financials/controllers/financial_report.php";
$financial_class = new Financial_Report(array("doctorspnl"=>true,
			"date" => $this->session->userdata('date_to_general_ledger')));


$arrDone = array();



// echo "<br>Printing final";
// echo "<pre>";
// print_r($financial_class->arrData());
// echo "</pre>";
// $all_transacted_rs = $this->company_financial_model->get_account_transactions_ledgers_new(array("doctorspnl" => true));

// foreach($all_transacted_rs->result() as $item){
// 	if(!in_array($item->transactionCategory,$arrDone))
// 	{
// 		var_dump($item);
// 	}

// }
// echo "</pre>";


$statement = $this->session->userdata('income_statement_title_search');

// var_dump($statement);die();

if(!empty($statement))
{
	$checked = $statement;
}
else {
	$checked = 'Reporting period: '.date('M j, Y', strtotime(date('Y-01-01'))).' to ' .date('M j, Y', strtotime(date('Y-m-d')));
}


// $closing_stock =  $this->company_financial_model->get_opening_stock_value();


?>

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$general_ledger_search_title =	$this->session->userdata('general_ledger_search_title');


	if(!empty($general_ledger_search_title))
	{
		$shareholder_id = $this->session->userdata('shareholder_id');
		$search_title = '';
		$staff_name = '';
		if(!empty($shareholder_id))
		{
			$staff_name = $this->reception_model->get_personnel_name_by_id($shareholder_id);
			$search_title = $staff_name.' ';
		}

		$search_title .= $general_ledger_search_title;
	}
	else
	{
		$search_title = 'ALL TIME REPORT';
	}



$shareholder_id = $this->session->userdata('shareholder_id');
if(!empty($shareholder_id))
{

	$salary_list = $this->company_financial_model->get_shareholder_salaries($shareholder_id);

  $net_salary = 0;
	$net_paye = 0;
	$net_nssf = 0;
	$net_nhif = 0;
	$net_gross = 0;
  $payroll_list = '';
  if($salary_list->num_rows() > 0)
  {
  	foreach ($salary_list->result() as $key => $value) {
  		// code...

  		$payroll_net = $value->payroll_net;
			$gross = $value->gross;
			$nssf = $value->nssf;
			$nhif = $value->nhif;
			$paye = $value->paye;
  		$payroll_created = $value->payroll_created;

  		$month = date('M',strtotime($payroll_created));

  		$net_salary += $payroll_net;
			$net_nhif += $nhif;
			$net_nssf += $nssf;
			$net_paye += $paye;
			$net_gross += $gross;

  		$payroll_list .='<tr>
												<td class="text-left" style="border: 1px solid #000;">'.strtoupper($month).'</td>
				                <td class="text-right" style="border: 1px solid #000;">'.number_format($gross,2).'</td>
				                <td class="text-right" style="border: 1px solid #000;background-color:orange;color:#000;">100%</td>
				                <td class="text-right" style="border: 1px solid #000;">'.number_format($gross,2).'</td>
												<td class="text-right" style="border: 1px solid #000;">'.number_format($gross,2).'</td>
											</tr>';

  	}


  	$payroll_list .='<tr>
											<th class="text-left" style="border: 1px solid #000;"></th>
				              <th class="text-right" style="border: 1px solid #000;"></th>
				              <th class="text-right" style="border: 1px solid #000;"></th>
				              <th class="text-right" style="border: 1px solid #000;"></th>
											<th class="text-right" style="border-top:0.5px solid black;border-bottom:2px solid black;">'.number_format($net_gross,2).'</th>
										</tr>';
  }
}




?>


<div class="col-md-4">
	<?php echo $this->load->view('search/search_doctors_profit_and_loss','', true);?>

	<div class="text-center">
		<h3 class="box-title">Income Statement</h3>
		<h5 class="box-title"> <?php echo $search_title?></h5>
		<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
	</div>

	<div class="form-group">
	<?php
			$search_status = $this->session->userdata('income_statement_search');
			$search_title = 'INCOME STATEMENT '.$search_title;
			if(!empty($search_status))
			{
				echo '<a href="'.site_url().'financials/company_financial/close_income_statement_search" class="btn btn-sm btn-warning">Close search</a>';
			}
			?>
	</div>
	<div class="col-md-12" style="margin-bottom:20px">
		<div class="col-md-12">
			<?php
			if(!empty($shareholder_id))
			{


				?>
					<div class="form-group">
						  <a href="<?php echo site_url().'print-doctors-income-statement'?>" target="_blank" class="btn btn-md btn-warning col-md-12"><i class="fa fa-print"></i> Print <?php echo $staff_name;?> P&L</a>
					</div>
				<?php
			}
			?>
		</div>

	</div>
	<br>
	<div class="col-md-12">
		<div class="form-group">
		      <a href="<?php echo site_url().'company-financials/monthly-detail'?>"  class="btn btn-md btn-default col-md-12"><i class="fa fa-file"></i> Monthly Detail</a>
		</div>
		<!-- <div class="form-group">
		      <a href="<?php echo site_url().'company-financials/balance-sheet'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Balance Sheet</a>
		</div>
		<div class="form-group">
		      <a href="<?php echo site_url().'company-financials/trial-balance'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Trial Balance</a>
		</div>
		<div class="form-group">
		      <a href="<?php echo site_url().'company-financials/general-ledger'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> General Ledger</a>
		</div> -->
	</div>

</div>
<div class="col-md-8">

	<section class="panel">

			<?php
			 $shareholder_id = $this->session->userdata('shareholder_id');


			 if($shareholder_id)
			 {
			?>
			<div class="panel-body" style="height:80vh;overflow-y:scroll;padding: 0px !important;">

		    	<table class="table table-striped  table-striped table-condensed table-linked" id="<?php echo $search_title?>">
		    		<thead>
		    			<tr>
		        			<th class="text-left" style="width:20%">ACCOUNT</th>
							    <th class="text-right" style="width:20%;text-align:right !important;"></th>
                  <th class="text-right" style="width:20%;text-align:right !important;">Percentage/Ratio Shared</th>
                  <th class="text-right" style="width:20%;text-align:right !important;">Invoiced Amount</th>
                  <th class="text-right" style="width:20%;text-align:right !important;">Received Amount</th>
						</tr>
					</thead>

					<tbody>
						<tr>
							<th class="text-left" colspan="5" style="background-color:#3c8dbc;color:#fff;">INCOME</th>
						</tr>

						<?php



							// code...

								$total_income = 0;
								foreach ($financial_class->arrData()["incomes"] as $key => $value) {

										$name = $value['name'];
										$amount = $value['amount'];


										$total_income += $amount;


								}

								$total_paid = 0;
								foreach ($financial_class->arrData()["Patients Payments"] as $key => $value) {

										$name = $value['name'];
										$amount = $value['amount'];


										$total_paid += $amount;


								}
								$total_invoiced = $total_income;
								$total_income = $total_paid;
								echo '<tr>
														<td class="text-left">INCOME</td>
                            <td class="text-right"></td>
                            <td class="text-right"></td>
                            <td class="text-right">'.number_format($total_invoiced,2).'</td>
														<td class="text-right">'.number_format($total_paid,2).'</td>
													</tr>';
								$franchaise_fee = 0.05 * $total_income;


								$total_income = $total_income - $franchaise_fee;

                echo '<tr>
                        <td class="text-left">LESS</td>
                        <td class="text-right"></td>
                        <td class="text-right">Franchaise Fee @5%</td>
                        <td class="text-right"> ( '.number_format($franchaise_fee,2).' )</td>
                        <td class="text-right"> '.number_format($total_income,2).'</td>
                      </tr>';

                $administration_fee = $franchaise_fee;


								$total_income = $total_income - $administration_fee;
                echo '<tr>
                        <td class="text-left"></td>
                        <td class="text-right"></td>
                        <td class="text-right">Administration Fee @5%</td>
                        <td class="text-right">( '.number_format($administration_fee,2).' )</td>
                        <td class="text-right">'.number_format($total_income,2).'</td>
                      </tr>';
								echo '<tr>
													<th class="text-left">TOTAL INCOME</th>
                          <th class="text-right"></th>
                          <th class="text-right"></th>
                          <th class="text-right"></th>
													<th class="text-right">'.number_format($total_income,2).'</th>
												</tr>';

						?>
						<tr>
							<th class="text-left" colspan="5" style="background-color:#3c8dbc;color:#fff;">SHARED EXPENSES</th>
						</tr>

						<?php

						$total_expenses = 0;
							foreach ($financial_class->arrData()["expenses"] as $key => $value) {

									$name = $value['name'];
									$amount = $value['amount'];

									$expense_name = strtoupper($name);


									if($expense_name == "SALARY")
										$amount -= $net_salary;
									else if($expense_name == "PAYE")
										$amount -= $net_paye;
									else if($expense_name == "NHIF")
										$amount -= $net_nhif;
									else if($expense_name == "NSSF")
										$amount -= $net_nssf;



									$share = $this->company_financial_model->get_expense_share($key);
									$share_value = $amount * ($share/100);
									$total_expenses += $share_value;


									echo '<tr>
													<td class="text-left">'.strtoupper($name).'</td>
                          <td class="text-right">'.number_format($amount,2).'</td>
                          <td class="text-right warning"><div class="col-md-11"><input class="form-control" size="1" type="text" id="share'.$key.'" name="share'.$key.'" onkeyup="update_share_value('.$key.')" value="'.$share.'" style="text-align:right;"></div><div class="col-md-1">%</div></td>
                          <td class="text-right">'.number_format($share_value,2).'</td>
													<td class="text-right">'.number_format($share_value,2).'</td>
												</tr>';

							}

							echo '<tr>
												<th class="text-left">TOTAL EXPENSES</th>
                        <th class="text-right"></th>
                        <th class="text-right"></th>
                        <th class="text-right"></th>
												<th class="text-right">'.number_format($total_expenses,2).'</th>
											</tr>';

							$profitorloss = $total_income - $total_expenses;
						?>
						<tr>
							<th class="text-left" colspan="5" style="background-color:#3c8dbc;color:#fff;">GROSS SALARY AS PER MONTH</th>
						</tr>
            <?php

            	echo $payroll_list;




            	echo '<tr>
												<th class="text-left" colspan="4">Total Cost of Shared Expenses Shared and Payments Received</th>
												<th class="text-right">'.number_format($net_gross+$total_expenses,2).'</th>
											</tr>';

						$profitorloss -= $net_payroll;
            ?>


					</tbody>
					<tfoot>
						<tr>
              <th class="text-left"><strong>PROFIT / NET DUE</strong></th>
              <th class="text-right"><strong></strong></th>
              <th class="text-right"><strong></strong></th>
              <th class="text-right"><strong></strong></th>
							<th class="text-right"><?php echo number_format($profitorloss,2)?></th>
						</tr>
					</tfoot>
				</table>

	    	</div>
	    <?php
	    	}
	    ?>
	</section>
</div>

<script type="text/javascript">
		var xport = {
  _fallbacktoCSV: true,
  toXLS: function(tableId, filename) {
    this._filename = (typeof filename == 'undefined') ? tableId : filename;

    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }

    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it

      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};


function update_share_value(expense_account_id)
{

	  var share = $("input#share"+expense_account_id).val();





      var config_url = $('#config_url').val();


      var url = config_url+"financials/company_financial/update_share_value/"+expense_account_id;

      $.ajax({
				      type:'POST',
				      url: url,
				      data:{share: share,expense_account_id: expense_account_id},
				      dataType: 'text',
				      success:function(data){
				        var data = jQuery.parseJSON(data);

				           $(".scheme_name").val(data.scheme_name);
				           $(".member_number").val(data.member_number);
				           $(".principal_member").val(data.principal_member);
				           $(".relation").val(data.relation);

				           $("#visit_type_id").val(visit_type_id);
				           $("#visit_type_id2").val(visit_type_id);


				      },
				      error: function(xhr, status, error) {
				      alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

				      }
      });
}

</script>
