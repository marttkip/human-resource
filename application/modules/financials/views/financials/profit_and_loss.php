
<?php

$this->db->where('account_staging_delete = 0');
$accounts_config_rs = $this->db->get('account_staging');

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;

		
	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];
$cost_of_goods_id = $session_account['cost_of_goods_id'];
$expense_account_id = $session_account['expense_account_id'];
$other_income_account_id = $session_account['other_income_account_id'];

// error_reporting(0);
// echo "<br>I'm at: ". getcwd();
include_once "application/modules/financials/controllers/financial_report.php";
$financial_class = new Financial_Report(array("pnl"=>true,
			"date" => $this->session->userdata('date_to_general_ledger')));


$arrDone = array();



// echo "<br>Printing final";
// echo "<pre>";
// print_r($financial_class->arrData());
// echo "</pre>";
// $all_transacted_rs = $this->company_financial_model->get_account_transactions_ledgers_new(array("for_pnl" => true));

// foreach($all_transacted_rs->result() as $item){
// 	if(!in_array($item->transactionCategory,$arrDone))
// 	{
// 		var_dump($item);
// 	}
	
// }
// echo "</pre>";


$statement = $this->session->userdata('income_statement_title_search');

// var_dump($statement);die();

if(!empty($statement))
{
	$checked = $statement;
}
else {
	$checked = 'Reporting period: '.date('M j, Y', strtotime(date('Y-01-01'))).' to ' .date('M j, Y', strtotime(date('Y-m-d')));
}


// $closing_stock =  $this->company_financial_model->get_opening_stock_value();


?>

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$general_ledger_search_title =	$this->session->userdata('general_ledger_search_title');


	if(!empty($general_ledger_search_title))
	{
		$search_title = $general_ledger_search_title;
	}
	else 
	{
		$search_title = 'ALL TIME REPORT';
	}

?>


<div class="col-md-4">
	<?php echo $this->load->view('search/search_profit_and_loss','', true);?>

	<div class="text-center">
		<h3 class="box-title">Income Statement</h3>
		<h5 class="box-title"> <?php echo $search_title?></h5>
		<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
	</div>

	<div class="form-group">
	<?php 
			$search_status = $this->session->userdata('income_statement_search');
			$search_title = 'INCOME STATEMENT '.$search_title;
			if(!empty($search_status))
			{
				echo '<a href="'.site_url().'financials/company_financial/close_income_statement_search" class="btn btn-sm btn-warning">Close search</a>';
			}
			?>
	</div>
	<div class="col-md-12" style="margin-bottom:20px">
		<div class="col-md-12">
		
			<div class="form-group">
				  <a href="<?php echo site_url().'print-income-statement'?>" target="_blank" class="btn btn-md btn-warning col-md-12"><i class="fa fa-print"></i> Print P&L</a>
			</div>
		</div>
		
	</div>
	<br>
	<div class="col-md-12">
		<!-- <div class="col-md-6"> -->
				<div class="form-group">
			      <a href="<?php echo site_url().'company-financials/monthly-detail'?>"  class="btn btn-md btn-default col-md-12"><i class="fa fa-file"></i> Monthly Detail</a>
				</div>
		<!-- </div>
		<div class="col-md-6">
				<div class="form-group">
			      <a href="<?php echo site_url().'company-financials/cashflow-profit-and-loss'?>"  class="btn btn-md btn-default col-md-12"><i class="fa fa-file"></i> Cashflow P n L</a>
				</div>
		</div> -->
		
		<div class="form-group">
		      <a href="<?php echo site_url().'company-financials/balance-sheet'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Balance Sheet</a>
		</div>
		<div class="form-group">
		      <a href="<?php echo site_url().'company-financials/trial-balance'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Trial Balance</a>
		</div>
		<div class="form-group">
		      <a href="<?php echo site_url().'company-financials/general-ledger'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> General Ledger</a>
		</div>
	</div>
	
</div>
<div class="col-md-8">

	<section class="panel">
			
			
				
			<!-- /.box-header -->
			<div class="panel-body" style="height:80vh;overflow-y:scroll;padding: 0px !important;">

		    	<table class="table table-striped  table-striped table-condensed table-linked" id="<?php echo $search_title?>">
		    		<thead>
		    			<tr>
		        			<th class="text-left">ACCOUNT</th>
							<th class="text-right">BALANCE</th>
						</tr>
					</thead>
						
					<tbody>
						<tr>
							<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">INCOME</th>
						</tr>

						<?php


						
							// code...

								$total_income = 0;
								foreach ($financial_class->arrData()["incomes"] as $key => $value) {

										$name = $value['name'];
										$amount = $value['amount'];


										$total_income += $amount;
										echo '<tr>
														<td class="text-left">'.strtoupper($name).'</td>
														<td class="text-right">'.number_format($amount,2).'</td>
													</tr>';

								}

								echo '<tr>
													<th class="text-left">TOTAL INCOME</th>
													<th class="text-right">'.number_format($total_income,2).'</th>
												</tr>';
						
						?>

						<tr>
							<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">COST OF GOODS SOLD</th>
						</tr>

						<?php
							echo '<tr>
											<td class="text-left">OPENING STOCK</td>
											<td class="text-right">0</td>
										</tr>
										<tr>
											<td class="text-left">PURCHASES</td>
											<td class="text-right">0</td>
										</tr>
										<tr>
											<td class="text-left">OTHER ADDITIONS</td>
											<td class="text-right">0</td>
										</tr>
										<tr>
											<td class="text-left">RETURN OUTWARDS</td>
											<td class="text-right">0</td>
										</tr>
										<tr>
											<td class="text-left">OTHER DEDUCTIONS</td>
											<td class="text-right">0</td>
										</tr>
										<tr>
											<td class="text-left">CLOSING STOCK</td>
											<td class="text-right">0</td>
										</tr>
										<tr>
											<td class="text-left">TOTAL GOODS SOLD</td>
											<td class="text-right">0</td>
										</tr>

										<tr>
											<th class="text-left">GROSS PROFIT (Total Income - Total Goods Sold)</th>
											<th class="text-right">0</th>
										</tr>';


						?>


						<tr>
							<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">OPERATING EXPENSE</th>
						</tr>

						<?php

						$total_expenses = 0;
							foreach ($financial_class->arrData()["expenses"] as $key => $value) {

									$name = $value['name'];
									$amount = $value['amount'];


									$total_expenses += $amount;
									echo '<tr>
													<td class="text-left">'.strtoupper($name).'</td>
													<td class="text-right">'.number_format($amount,2).'</td>
												</tr>';

							}

							echo '<tr>
												<th class="text-left">TOTAL EXPENSES</th>
												<th class="text-right">'.number_format($total_expenses,2).'</th>
											</tr>';

							$profitorloss = $total_income - $total_expenses;
						?>

						
					</tbody>
					<tfoot>
						<tr>
		        	<th class="text-left"><strong>NET PROFIT / LOSS</strong></th>
							<th class="text-right"><?php echo number_format($profitorloss,2)?></th>
						</tr>
					</tfoot>
				</table>

	    	</div>
	</section>
</div>

<script type="text/javascript">
		var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};


</script>
