
<?php

$this->db->where('account_staging_delete = 0');
$accounts_config_rs = $this->db->get('account_staging');

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;

		
	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];
$cost_of_goods_id = $session_account['cost_of_goods_id'];
$expense_account_id = $session_account['expense_account_id'];
$other_income_account_id = $session_account['other_income_account_id'];

// error_reporting(0);
// echo "<br>I'm at: ". getcwd();
include_once "application/modules/financials/controllers/financial_report.php";
$financial_class = new Financial_Report(array("pnl"=>true,
			"date" => $this->session->userdata('date_to_general_ledger')));


$arrDone = array();



$statement = $this->session->userdata('income_statement_title_search');

// var_dump($statement);die();

if(!empty($statement))
{
	$checked = $statement;
}
else {
	$checked = 'Reporting period: '.date('M j, Y', strtotime(date('Y-01-01'))).' to ' .date('M j, Y', strtotime(date('Y-m-d')));
}


$closing_stock =  $this->company_financial_model->get_opening_stock_value();


?>

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$general_ledger_search_title =	$this->session->userdata('general_ledger_search_title');


	if(!empty($general_ledger_search_title))
	{
		$search_title = $general_ledger_search_title;
	}
	else 
	{
		$search_title = 'ALL TIME REPORT';
	}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | P & L</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
       <script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script>

		<script src="<?php echo base_url()."assets/"?>table-resources/table2excel/jquery.table2excel.min.js"></script>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:9px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
						.center-align{margin:0 auto; text-align:center;}

						.receipt_bottom_border{border-bottom: #888888 medium solid;}
						.row .col-md-12 table {
							/*border:solid #000 !important;*/
							/*border-width:1px 0 0 1px !important;*/
							font-size:12px;
							margin-top:10px;
						}
						.col-md-6 {
						    width: 50%;
						 }
						.row .col-md-12 th, .row .col-md-12 td {
							/*border:solid #000 !important;*/
							/*border-width:0 1px 1px 0 !important;*/
						}
						.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
						{
							 /*padding: 2px;*/
							 padding: 5px;
						}
						h3
						{
							font-size: 30px;
						}
						.col-print-1 {width:8%;  float:left;}
						.col-print-2 {width:16%; float:left;}
						.col-print-3 {width:25%; float:left;}
						.col-print-4 {width:33%; float:left;}
						.col-print-5 {width:42%; float:left;}
						.col-print-6 {width:50%; float:left;}
						.col-print-7 {width:58%; float:left;}
						.col-print-8 {width:66%; float:left;}
						.col-print-9 {width:75%; float:left;}
						.col-print-10{width:83%; float:left;}
						.col-print-11{width:92%; float:left;}
						.col-print-12{width:100%; float:left;}


						
			            

						.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
						.title-img{float:left; padding-left:30px;}
						img.logo{ margin:0 auto;}
            
        </style>
        

		<script language="javascript">
		    function doc_keyUp(e) {
		        // this would test for whichever key is 40 and the ctrl key at the same time
		        if (e.keyCode === 69) {
		            alert("Your Excel Document is being prepared and should start downloading in a few. Don't hit E again until it does.");
		            // call your function to do the thing
		            downloadExcel();
		        }
		    }
		// register the handler 
		    document.addEventListener('keyup', doc_keyUp, false);
		    function downloadExcel() {
		    	// alert("sasa")
		        $("#testTable").table2excel({
		            exclude: ".noExl",
		            name: '<?php echo $search_title?>',
		            filename: "<?php echo $search_title?>.xls",
		            fileext: ".xls",
		            exclude_img: true,
		            exclude_links: true,
		            exclude_inputs: true
		        });
		    }
		</script>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>

      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align" style="padding: 5px;">
            	<strong>PROFIT AND LOSS STATEMENT</strong><br>

            	<?php
            	// $search_title = $this->session->userdata('balance_sheet_title_search');

      			// 	 if(empty($search_title))
      			// 	 {
      			// 	 	$search_title = "";
      			// 	 }
      			// 	 else
      			// 	 {
      			// 	 	$search_title =$search_title;
      			// 	 }
				//  echo $search_title;
            	?>

            </div>
        </div>

    	<div class="col-md-12">
        	<table class="table table-striped  table-striped table-condensed table-linked" id="testTable">
	    		<thead>
	    			<tr>
	        			<th class="text-left" style="border: 1px solid #000;">ACCOUNT</th>
						<th class="text-right" style="border: 1px solid #000;">BALANCE</th>
					</tr>
				</thead>
					
				<tbody>
					<tr>
						<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;border: 1px solid #000;">INCOME</th>
					</tr>

					<?php


					
						// code...

							$total_income = 0;
							foreach ($financial_class->arrData()["incomes"] as $key => $value) {

									$name = $value['name'];
									$amount = $value['amount'];


									$total_income += $amount;
									echo '<tr>
													<td class="text-left" style="border: 1px solid #000;">'.strtoupper($name).'</td>
													<td class="text-right" style="border: 1px solid #000;">'.number_format($amount,2).'</td>
												</tr>';

							}

							echo '<tr>
												<th class="text-left" style="border: 1px solid #000;">TOTAL INCOME</th>
												<th class="text-right" style="border: 1px solid #000;">'.number_format($total_income,2).'</th>
											</tr>';
					
					?>

						<tr>
							<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">COST OF GOODS SOLD</th>
						</tr>

						<?php
							echo '<tr>
											<td class="text-left" style="border: 1px solid #000;">OPENING STOCK</td>
											<td class="text-right" style="border: 1px solid #000;">0</td>
										</tr>
										<tr>
											<td class="text-left" style="border: 1px solid #000;">PURCHASES</td>
											<td class="text-right" style="border: 1px solid #000;">0</td>
										</tr>
										<tr>
											<td class="text-left" style="border: 1px solid #000;">OTHER ADDITIONS</td>
											<td class="text-right" style="border: 1px solid #000;">0</td>
										</tr>
										<tr>
											<td class="text-left" style="border: 1px solid #000;">RETURN OUTWARDS</td>
											<td class="text-right" style="border: 1px solid #000;">0</td>
										</tr>
										<tr>
											<td class="text-left" style="border: 1px solid #000;">OtdER DEDUCTIONS</td>
											<td class="text-right" style="border: 1px solid #000;">0</td>
										</tr>
										<tr>
											<td class="text-left" style="border: 1px solid #000;">CLOSING STOCK</td>
											<td class="text-right" style="border: 1px solid #000;">0</td>
										</tr>
										<tr>
											<td class="text-left" style="border: 1px solid #000;">TOTAL GOODS SOLD</td>
											<td class="text-right" style="border: 1px solid #000;">0</td>
										</tr>

										<tr>
											<th class="text-left" style="border: 1px solid #000;">GROSS PROFIT (Total Income - Total Goods Sold)</th>
											<th class="text-right" style="border: 1px solid #000;">0</th>
										</tr>';


						?>
					<tr>
						<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;border: 1px solid #000;">OPERATING EXPENSE</th>
					</tr>

					<?php

					$total_expenses = 0;
						foreach ($financial_class->arrData()["expenses"] as $key => $value) {

								$name = $value['name'];
								$amount = $value['amount'];


								$total_expenses += $amount;
								echo '<tr>
												<td class="text-left" style="border: 1px solid #000;">'.strtoupper($name).'</td>
												<td class="text-right" style="border: 1px solid #000;">'.number_format($amount,2).'</td>
											</tr>';

						}

						echo '<tr>
											<th class="text-left" style="border: 1px solid #000;">TOTAL EXPENSES</th>
											<th class="text-right" style="border: 1px solid #000;">'.number_format($total_expenses,2).'</th>
										</tr>';

						$profitorloss = $total_income - $total_expenses;
					?>

					
				</tbody>
				<tfoot>
					<tr>
	        			<th class="text-left" style="border: 1px solid #000;"><strong>NET PROFIT / LOSS</strong></th>
						<th class="text-right" style="border: 1px solid #000;"><?php echo number_format($profitorloss,2)?></th>
					</tr>
				</tfoot>
			</table>

        </div>

    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-sm-12">
                <div class="col-sm-10 pull-left">
                    <strong>Prepared by: </strong>
                </div>
                <div class="col-sm-2 pull-right">
                    <?php echo date('jS M Y H:i a'); ?>
                </div>
            </div>

        </div>
    </body>

</html>

<script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,
  toXLS: function(tableId, filename) {
    this._filename = (typeof filename == 'undefined') ? tableId : filename;

    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }

    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it

      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>
