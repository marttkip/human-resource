<?php echo $this->load->view('search/search_balance_sheet','', true);?>


// p & l 


<?php

$grand_income = 0;
$income_result = '';



$revenue_rs = $this->ledgers_model->get_all_services();


if($revenue_rs->num_rows() > 0)
{
	$balance = 0;
	$total_amount = 0;
	// var_dump($revenue_rs->result());die();
	foreach ($revenue_rs->result() as $key => $value5) {
		# code...
		// get all transactions
		$service_id = $value5->service_id;
		$department_id = $value5->department_id;
		$service_name = $value5->service_name;



		// update the service Details
		$revenue_items_rs = $this->ledgers_model->get_receivables_transactions_per_service($service_id,1);


		if($revenue_items_rs->num_rows() > 0)
		{
			$balance = 0;
			$total_amount = 0;
			// var_dump($revenue_items_rs->result());die();
			foreach ($revenue_items_rs->result() as $key => $value6) 
			{

				// $dr_amount = $value6->dr_amount;
				$dr_amount = $value6->dr_amount;
				$cr_amount = $value6->cr_amount;
				
				$total_income = $dr_amount-$cr_amount;
				$grand_income += $total_income;

				$income_result .='<tr>
									<td class="text-left">'.strtoupper($service_name).'</td>
									<td class="text-right"><a href="'.site_url().'company-financials/services-bills/'.$department_id.'" >'.number_format($total_income,2).'</a></td>
									</tr>';

			

			}
			
			
		}
	}

}
$income_result .='<tr>
						<td class="text-left"><b>INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income,2).'</b></td>
					</tr>';


$other_income_id = $this->company_financial_model->get_parent_account_id('Other Incomes');

$other_account_rs = $this->ledgers_model->get_all_child_accounts($other_income_id);

$grand_other_income = 0;

if($other_account_rs->num_rows() > 0)
{
	$balance = 0;
	$total_amount = 0;
 	foreach ($other_account_rs->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		

		$expense_of_account_rs = $this->ledgers_model->get_account_transactions($account_id);

			// var_dump($parent_account_id);die();
		if($expense_of_account_rs->num_rows() > 0)
		{
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_other_income = $cr_amount;
				$grand_other_income += $total_other_income;

				$income_result .='<tr>
									<td class="text-left">'.strtoupper($account_name).'</td>
									<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_other_income,2).'</a></td>
									</tr>';
			

			}
			
		}
	}
}


$income_result .='<tr>
						<td class="text-left"><b>OTHER INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_other_income,2).'</b></td>
					</tr>';

$income_result .='<tr>
						<td class="text-left"><b>TOTAL INCOME</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_income+$grand_other_income,2).'</b></td>
					</tr>';


$salary = $this->company_financial_model->get_salary_expenses();
// $nssf = $this->company_financial_model->get_statutories(1);
// $nhif = $this->company_financial_model->get_statutories(2);
// $paye_amount = $this->company_financial_model->get_statutories(3);
$relief =0;// $this->company_financial_model->get_statutories(4);
$loans = 0;//$this->company_financial_model->get_statutories(5);

// $paye = $paye_amount - $relief;

$salary -= $relief;
$other_deductions = $salary;// - ($nssf+$nhif+$paye_amount+$relief);

// $total_operational_amount += $salary+$nssf+$nhif+$paye_amount;
$total_operational_amount += $salary;
// $operation_result .= $non_pharm;
$operation_result .='<tr>
						<td class="text-left"><b>Total Operation Cost</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($total_operational_amount,2).'</b></td>
					</tr>';

// get cost of goods
$parent_account_id = $this->company_financial_model->get_parent_account_id('Cost of Goods');

$account_rs = $this->ledgers_model->get_all_child_accounts($parent_account_id);

// var_dump($account_rs->result());die();
$goods_result ='';
$grand_goods = 0;
if($account_rs->num_rows() > 0)
{
 	foreach ($account_rs->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;


		$expense_of_account_rs = $this->ledgers_model->get_expense_account_transactions($account_id,1);

			// var_dump($parent_account_id);die();
		if($expense_of_account_rs->num_rows() > 0)
		{
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
			

				

				$total_goods = $dr_amount-$cr_amount;
				$grand_goods += $total_goods;
				// $grand_balance += $balance;
				$goods_result .='<tr>
									<td class="text-left">'.strtoupper($account_name).'</td>
									<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_goods,2).'</a></td>
									</tr>';
			

			}
			

		}
	}
}
$goods_result .='<tr>
						<td class="text-left"><b>TOTAL GOODS SOLD</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_goods,2).'</b></td>
					</tr>';


$parent_account_id2 = $this->company_financial_model->get_parent_account_id('Expense Accounts');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($parent_account_id2);

$grand_expense = 0;
$operation_result ='';
$total_operational_amount = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_expense_account_transactions($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_expense = $dr_amount-$cr_amount;
				$grand_expense += $total_expense;

				
				$operation_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_expense,2).'</a></td>
									</tr>';

			}
			

		}
	}
}


$parent_account_id2 = $this->company_financial_model->get_parent_account_id('Payroll');
$account_rs2 = $this->ledgers_model->get_all_child_accounts($parent_account_id2);

$grand_expense = 0;
if($account_rs2->num_rows() > 0)
{
 	foreach ($account_rs2->result() as $key => $value) 
 	{
 		# code...

 		$account_name = $value->account_name;
 		$account_id = $value->account_id;
 		 
		$expense_of_account_rs = $this->ledgers_model->get_expense_account_transactions($account_id,1);
		// var_dump($account_rs2);die();

		if($expense_of_account_rs->num_rows() > 0)
		{

			
			$balance = 0;
			$total_amount = 0;
			// var_dump($expense_of_account_rs->result());die();
			foreach ($expense_of_account_rs->result() as $key => $value4) {
				# code...
				// get all transactions

				$dr_amount = $value4->dr_amount;
				$cr_amount = $value4->cr_amount;
				$accountName = $value4->accountName;

				$total_expense = $dr_amount-$cr_amount;
				$grand_expense += $total_expense;

				
				$operation_result .='<tr>
										<td class="text-left">'.strtoupper($account_name).'</td>
										<td class="text-right"><a href="'.site_url().'accounting/expense-ledger/'.$account_id.'" >'.number_format($total_expense,2).'</a></td>
									</tr>';

			}
			

		}
	}
}

$operation_result .='<tr>
						<td class="text-left"><b>TOTAL OPERATING EXPENSE</b></td>
						<td class="text-right" style="border-top:#3c8dbc solid 2px;"><b>'.number_format($grand_expense,2).'</b></td>
					</tr>';

$statement = $this->session->userdata('income_statement_title_search');

// var_dump($statement);die();

$total_profit = $grand_income - $grand_goods - $grand_expense;


?>
<?php
//  pnnda
$bank_balances_rs = $this->company_financial_model->get_account_value_by_type('Current Assets');
$bank_balances_result = '';
$total_income = 0;
// var_dump($bank_balances_rs);die();

if($bank_balances_rs->num_rows() > 0)
{
	foreach ($bank_balances_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->accountName;
		$account_id = $value->account_id;
		$total_income += $total_amount;
		$bank_balances_result .='<tr>
							<td class="text-left">'.strtoupper($transactionName).'</td>
							<td class="text-right">
							<a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_amount,2).'</a>
							</td>
							</tr>';
	}
	$bank_balances_result .='<tr>
							<td class="text-left"><b>TOTAL BANK BALANCE</b></td>
							<td class="text-right"><b class="match">'.number_format($total_income,2).'</b></td>
							</tr>';
}



$bank_balances_rs = $this->company_financial_model->get_account_value_by_type('Bank');
$cash_in_bank = '';
$total_income = 0;
// var_dump($bank_balances_rs);die();

if($bank_balances_rs->num_rows() > 0)
{
	foreach ($bank_balances_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->accountName;
		$account_id = $value->account_id;
		$total_income += $total_amount;
		$cash_in_bank .='<tr>
							<td class="text-left">'.strtoupper($transactionName).'</td>
							<td class="text-right">
							<a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_amount,2).'</a>
							</td>
							</tr>';
	}
	$cash_in_bank .='<tr>
							<td class="text-left"><b>TOTAL BANK BALANCE</b></td>
							<td class="text-right"><b class="match">'.number_format($total_income,2).'</b></td>
							</tr>';
}



$bank_balances_rs = $this->company_financial_model->get_account_share_capital_by_type('Capital');
$share_capital_list = '';
$total_share_capital = 0;
// var_dump($bank_balances_rs);die();

if($bank_balances_rs->num_rows() > 0)
{
	foreach ($bank_balances_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->accountName;
		$account_id = $value->account_id;
		$total_share_capital += $total_amount;
		$share_capital_list .='<tr>
							<td class="text-left">'.strtoupper($transactionName).'</td>
							<td class="text-right">
							<a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_amount,2).'</a>
							</td>
							</tr>';
	}
	
}




$fixed_asset_rs = $this->company_financial_model->get_asset_categories();
$fixed_asset_result = '';
$total_fixed = 0;
// var_dump($fixed_asset_rs);die();

if($fixed_asset_rs->num_rows() > 0)
{
	foreach ($fixed_asset_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->asset_value;
		$asset_category_name = $value->asset_category_name;
		$asset_category_id = $value->asset_category_id;
		$total_fixed += $total_amount;
		$fixed_asset_result .='<tr>
								<td class="text-left">'.strtoupper($asset_category_name).'</td>
								<td class="text-right">
								'.number_format($total_amount,2).'
								</td>
								</tr>';
	}



}

$capital_earned_rs = $this->company_financial_model->get_account_value_by_type('Asset');


// var_dump($capital_earned_rs);die();

if($capital_earned_rs->num_rows() > 0)
{
	foreach ($capital_earned_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->accountName;
		$account_id = $value->account_id;
		

		$debit = 0;
		$credit = 0;
		if($total_amount > 0)
		{
			$debit = $total_amount;
			$total_debits += $debit;
		}
		else
		{
			$credit = -$total_amount;
			$total_credits += $credit;
		}

		if($transactionName == "Opening Balance")
		{
			$total_income += $total_amount;
			$bank_balances_result .='<tr>
							<td class="text-left">'.strtoupper($transactionName).'</td>
							<td class="text-right">
							<a href="'.site_url().'account-transactions/'.$account_id.'" >'.number_format($total_amount,2).'</a>
							</td>
							</tr>';

		}
		else
		{

			$total_fixed += $total_amount;
			$fixed_asset_result .='<tr>
									<td class="text-left">'.strtoupper($transactionName).'</td>
									<td class="text-right">
									'.number_format($total_amount,2).'
									</td>
								</tr>';

		}
		
	}

}



$accounts_receivable = $this->company_financial_model->get_accounts_receivables();
$accounts_payable = $this->company_financial_model->get_accounts_payable();
$accounts_providers = $this->company_financial_model->get_accounts_providers();
$payroll_liability = $this->company_financial_model->get_payroll_liability();
$cash_on_hand = $this->company_financial_model->get_cash_on_hand();
$wht_payable = $this->company_financial_model->get_total_wht_tax();
$vat_payable = $this->company_financial_model->get_total_vat_tax();

$total_assets = $accounts_receivable+$total_income;

$total_liability = $accounts_payable + $wht_payable +$wht_payable + $accounts_providers + $payroll_liability;
$current_year_earnings = $total_assets + $total_fixed - $total_liability;

$search = $this->session->userdata('balance_sheet_title_search');

if(!empty($search))
{
	$balance_sheet_search = ucfirst($search);
}
else {
	$balance_sheet_search = 'Reporting as of: '.date('M j, Y', strtotime(date('Y-m-d')));
}

$total_current_assets = $total_fixed+$total_income+$accounts_receivable;
?>


<style>
	td .match
	{
		border-top: #000 2px solid !important;
	}
</style>
<div class="text-center">
	<h3 class="box-title">Balance Sheet</h3>
	<h5 class="box-title"> <?php echo $balance_sheet_search?> </h5>
	<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
</div>

<div class="col-md-2">
	
</div>
<div class="col-md-8">
	

<section class="panel">
		<div class="panel-body">
    	<h3 class="box-title">Asssets</h3>
    	<!-- <h4 class="box-title">Fixed Assets</h4> -->
    	<table class="table  table-striped table-condensed" style="margin-left: 10px">

    		<thead>
				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">Fixed Asset</th>
				</tr>
				<tr>
        			<th class="text-left">Account</th>
					<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
					<?php
					echo $fixed_asset_result;

					?>
					
					

			</tbody>
		</table>
		<table class="table  table-striped table-condensed" style="margin-left: 10px">
			<thead>

				<tr>
					<th class="text-left" colspan="" >TOTAL FIXED ASSETS</th>
					<th class="text-right" colspan="" ><?php echo number_format($total_fixed,2)?></th>
				</tr>
			</thead>
		</table>
		<table class="table  table-striped table-condensed" style="margin-left: 10px">
			<thead>

				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">Current Assets</th>
				</tr>
			</thead>
		</table>
		<table class="table  table-striped table-condensed" style="margin-left: 10px">
			<thead>
				<tr>
					<th class="text-left" colspan="2" >Other Current Assets</th>
				</tr>
				
			</thead>
			<tbody>
				<?php echo $bank_balances_result?>
			</tbody>
			<thead>

				<tr>
					<th class="text-left" colspan="2" >Cash in at Bank and in hand</th>
				</tr>
				
			</thead>

			<tbody>
				<?php echo $cash_in_bank?>
			</tbody>

			
			<tbody>
				<tr>
					<td class="text-left">ACCOUNTS RECEIVABLES</td>
					<td class="text-right"><?php echo number_format($accounts_receivable,2)?></td>
				</tr>
				
			</tbody>
		</table>
		<table class="table  table-striped table-condensed" style="margin-left: 10px">
			<thead>

				<tr>
					<th class="text-left" colspan="" >TOTAL CURRENT ASSETS</th>
					<th class="text-right" colspan="" ><?php echo number_format($total_fixed+$total_income+$accounts_receivable,2);?></th>
				</tr>
			</thead>
		</table>


		<table class="table  table-striped table-condensed" style="margin-left: 10px">
			<thead>

				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">Current Liabilities</th>
				</tr>
			</thead>
		</table>
		<table class="table  table-striped table-condensed" style="margin-left: 10px">
			
			<tbody>
				<tr>
					<td class="text-left">ACCOUNTS PAYABLES</td>
					<td class="text-right"><a href="<?php echo site_url().'accounts-payables'?>" ><?php echo number_format($accounts_payable,2)?></a> </td>
				</tr>
				<tr>
					<td class="text-left">PROVIDERS</td>
					<td class="text-right"><?php echo number_format($accounts_providers,2)?> </td>
				</tr>
				<tr>
					<td class="text-left">PAYROLL LIABILITY</td>
					<td class="text-right"><?php echo number_format($payroll_liability,2)?> </td>
				</tr>
				<tr>
					 <th class="text-left">TOTAL CURRENT LIABILITIES</th>
					<td class="text-right"><b class="match"><?php echo number_format($total_liability,2);?></b></td>
				</tr>
			</tbody>
			
		</table>
		
		<table class="table  table-striped table-condensed" >
			<thead>

				
				<tr>
					<th class="text-left" colspan="" >NET ASSETS = (CURRENT ASSETS - CURRENT LIABILITIES)</th>
					<th class="text-right" colspan="" ><?php echo number_format($total_current_assets - $total_liability,2);?></th>
				</tr>
			</thead>
		</table>
		
		
		<h3 class="box-title">Capital and Reserves</h3>
    	<table class="table  table-striped table-condensed" style="margin-left: 10px">
			<thead>
				<tr>
        			<th class="text-left">Account</th>
							<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
				<tr>
        			<td class="text-left">Share Capital Account</td>
							<td class="text-right"><?php echo number_format($total_share_capital,2)?></td>
				</tr>
				<tr>
        			<td class="text-left">Retained Earnings</td>
					<td class="text-right"><?php echo number_format($total_profit,2)?></td>
				</tr>
				<tr>
							<td class="text-left">Total Share Holder Funds</td>
					<td class="text-right"><?php echo number_format($total_share_capital+$total_profit,2)?></td>
				</tr>
				<tr>
					<td class="text-left">Total Share Holder Funds plus Liability</td>
					<td class="text-right"><?php echo number_format($total_share_capital+$total_profit+$total_liability,2)?></td>
				</tr>
				
			</tbody>
		</table>
    </div>
</section>
</div>
<div class="col-md-2">
	
</div>
