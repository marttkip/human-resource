<?php

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];
$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];

$result = "	
			UNION ALL

			SELECT
				`creditor`.`creditor_id` AS `transactionId`,
				'' AS `referenceId`,
				'' AS `payingFor`,
				'' AS `referenceCode`,
				'' AS `transactionCode`,
				'' AS `patient_id`,
			    `creditor`.`creditor_id` AS `recepientId`,
				`account`.`parent_account` AS `accountParentId`,
		  		`account_type`.`account_type_name` AS `accountsclassfication`,
				account.account_id AS `accountId`,
				account.account_name AS `accountName`,
				'' AS `transactionName`,
				CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
				0 AS `dr_amount`,
				`creditor`.`opening_balance` AS `cr_amount`,
				`creditor`.`start_date` AS `transactionDate`,
				`creditor`.`start_date` AS `createdAt`,
				`creditor`.`creditor_status` AS `status`,
				creditor.branch_id AS `branch_id`,
				'Expense' AS `transactionCategory`,
				'Creditor Opening Balance' AS `transactionClassification`,
				'' AS `transactionTable`,
				'creditor' AS `referenceTable`
			FROM
			creditor,account,account_type
			WHERE debit_id = 1 AND account.account_id = ".$supplier_opening_balance_id." AND account.account_type_id = account_type.account_type_id

			UNION ALL

			SELECT
				`creditor`.`creditor_id` AS `transactionId`,
				'' AS `referenceId`,
				'' AS `payingFor`,
				'' AS `referenceCode`,
				'' AS `transactionCode`,
				'' AS `patient_id`,
			    `creditor`.`creditor_id` AS `recepientId`,
				`account`.`parent_account` AS `accountParentId`,
		  		`account_type`.`account_type_name` AS `accountsclassfication`,
				account.account_id AS `accountId`,
				account.account_name AS `accountName`,
				'' AS `transactionName`,
				CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
				`creditor`.`opening_balance` AS `dr_amount`,
				'0' AS `cr_amount`,
				`creditor`.`start_date` AS `transactionDate`,
				`creditor`.`start_date` AS `createdAt`,
				`creditor`.`creditor_status` AS `status`,
				 creditor.branch_id AS `branch_id`,
				'Expense' AS `transactionCategory`,
				'Creditor Opening Balance' AS `transactionClassification`,
				'' AS `transactionTable`,
				'creditor' AS `referenceTable`
			FROM
			creditor,account,account_type
			WHERE debit_id = 2 AND account.account_id = ".$supplier_opening_balance_id." AND account.account_type_id = account_type.account_type_id


			UNION ALL


			SELECT
				`creditor`.`creditor_id` AS `transactionId`,
				'' AS `referenceId`,
				'' AS `payingFor`,
				'' AS `referenceCode`,
				'' AS `transactionCode`,
				'' AS `patient_id`,
			    `creditor`.`creditor_id` AS `recepientId`,
				`account`.`parent_account` AS `accountParentId`,
		  		`account_type`.`account_type_name` AS `accountsclassfication`,
				account.account_id AS `accountId`,
				account.account_name AS `accountName`,
				'' AS `transactionName`,
				CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
				`creditor`.`opening_balance` AS `dr_amount`,
				0 AS `cr_amount`,
				`creditor`.`start_date` AS `transactionDate`,
				`creditor`.`start_date` AS `createdAt`,
				`creditor`.`creditor_status` AS `status`,
				creditor.branch_id AS `branch_id`,
				'Expense' AS `transactionCategory`,
				'Creditor Opening Balance' AS `transactionClassification`,
				'' AS `transactionTable`,
				'creditor' AS `referenceTable`
			FROM
			creditor,account,account_type
			WHERE debit_id = 1 AND account.account_id = ".$accounts_payable_id." AND account.account_type_id = account_type.account_type_id

			UNION ALL

			SELECT
				`creditor`.`creditor_id` AS `transactionId`,
				'' AS `referenceId`,
				'' AS `payingFor`,
				'' AS `referenceCode`,
				'' AS `transactionCode`,
				'' AS `patient_id`,
			    `creditor`.`creditor_id` AS `recepientId`,
				`account`.`parent_account` AS `accountParentId`,
		  		`account_type`.`account_type_name` AS `accountsclassfication`,
				account.account_id AS `accountId`,
				account.account_name AS `accountName`,
				'' AS `transactionName`,
				CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
				0 AS `dr_amount`,
				`creditor`.`opening_balance`  AS `cr_amount`,
				`creditor`.`start_date` AS `transactionDate`,
				`creditor`.`start_date` AS `createdAt`,
				`creditor`.`creditor_status` AS `status`,
				 creditor.branch_id AS `branch_id`,
				'Expense' AS `transactionCategory`,
				'Creditor Opening Balance' AS `transactionClassification`,
				'' AS `transactionTable`,
				'creditor' AS `referenceTable`
			FROM
			creditor,account,account_type
			WHERE debit_id = 2 AND account.account_id = ".$accounts_payable_id." AND account.account_type_id = account_type.account_type_id
		

			UNION ALL


			SELECT
				`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
				`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
				'' AS `payingFor`,
				`creditor_invoice`.`invoice_number` AS `referenceCode`,
				`creditor_invoice`.`document_number` AS `transactionCode`,
				'' AS `patient_id`,
			  `creditor_invoice`.`creditor_id` AS `recepientId`,
				`account`.`parent_account` AS `accountParentId`,
				`account_type`.`account_type_name` AS `accountsclassfication`,
				`creditor_invoice_item`.`account_to_id` AS `accountId`,
				`account`.`account_name` AS `accountName`,
				`creditor_invoice_item`.`item_description` AS `transactionName`,
				CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
				`creditor_invoice_item`.`total_amount` AS `dr_amount`,
				'0' AS `cr_amount`,
				`creditor_invoice`.`transaction_date` AS `transactionDate`,
				`creditor_invoice`.`created` AS `createdAt`,
				`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
				'creditor.branch_id' AS `branch_id`,
				'Cost of Goods' AS `transactionCategory`,
				'Creditors Invoices' AS `transactionClassification`,
				'creditor_invoice_item' AS `transactionTable`,
				'creditor_invoice' AS `referenceTable`
			FROM
				(
					(
						(
							`creditor_invoice_item`,creditor_invoice,creditor
							
						)
						JOIN account ON(
							(
								account.account_id = creditor_invoice_item.account_to_id
							)
						)
					)
					JOIN `account_type` ON(
						(
							account_type.account_type_id = account.account_type_id
						)
					)
				)
			WHERE 

			creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
			AND creditor_invoice.creditor_invoice_status = 1
			AND creditor.creditor_id = creditor_invoice.creditor_id 
			AND creditor_invoice.transaction_date >= creditor.start_date

		UNION ALL

		  SELECT
			`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
			`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
			`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
			`creditor_credit_note`.`invoice_number` AS `referenceCode`,
			`creditor_credit_note`.`document_number` AS `transactionCode`,
			'' AS `patient_id`,
		  	`creditor_credit_note`.`creditor_id` AS `recepientId`,
			`account`.`parent_account` AS `accountParentId`,
			`account_type`.`account_type_name` AS `accountsclassfication`,
			`creditor_credit_note`.`account_from_id` AS `accountId`,
			`account`.`account_name` AS `accountName`,
			`creditor_credit_note_item`.`description` AS `transactionName`,
			`creditor_credit_note_item`.`description` AS `transactionDescription`,
			0 AS `dr_amount`,
			`creditor_credit_note_item`.`credit_note_amount` AS `cr_amount`,
			`creditor_credit_note`.`transaction_date` AS `transactionDate`,
			`creditor_credit_note`.`created` AS `createdAt`,
			`creditor_invoice`.`transaction_date` AS `referenceDate`,
			`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
			'Supplier Credit Note' AS `transactionCategory`,
			'Creditors Credit Notes' AS `transactionClassification`,
			'creditor_credit_note' AS `transactionTable`,
			'creditor_credit_note_item' AS `referenceTable`
		FROM
			(
				(
					(
						`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
						
					)
					JOIN account ON(
						(
							account.account_id = creditor_credit_note.account_from_id
						)
					)
					
				)
				JOIN `account_type` ON(
					(
						account_type.account_type_id = account.account_type_id
					)
				)
			)
		WHERE 
			creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
			AND creditor_credit_note.creditor_credit_note_status = 1
			AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
			AND creditor_invoice.creditor_invoice_status = 1
			AND creditor.creditor_id = creditor_invoice.creditor_id 
			AND creditor_invoice.transaction_date >= creditor.start_date


		UNION ALL


		SELECT
				`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
				`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
				'' AS `payingFor`,
				`creditor_invoice`.`invoice_number` AS `referenceCode`,
				`creditor_invoice`.`document_number` AS `transactionCode`,
				'' AS `patient_id`,
			  `creditor_invoice`.`creditor_id` AS `recepientId`,
				`account`.`parent_account` AS `accountParentId`,
				`account_type`.`account_type_name` AS `accountsclassfication`,
				`account`.`account_id` AS `accountId`,
				`account`.`account_name` AS `accountName`,
				`creditor_invoice_item`.`item_description` AS `transactionName`,
				CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
				0 AS `dr_amount`,
				`creditor_invoice_item`.`total_amount` AS `cr_amount`,
				`creditor_invoice`.`transaction_date` AS `transactionDate`,
				`creditor_invoice`.`created` AS `createdAt`,
				`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
				'creditor.branch_id' AS `branch_id`,
				'Cost of Goods' AS `transactionCategory`,
				'Creditors Invoices' AS `transactionClassification`,
				'creditor_invoice_item' AS `transactionTable`,
				'creditor_invoice' AS `referenceTable`
			FROM
				(
					(
						(
							`creditor_invoice_item`,creditor_invoice,creditor
							
						)
						JOIN account ON(
							(
								account.account_id = ".$accounts_payable_id."
							)
						)
					)
					JOIN `account_type` ON(
						(
							account_type.account_type_id = account.account_type_id
						)
					)
				)
			WHERE 

			creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id 
			AND creditor_invoice.creditor_invoice_status = 1
			AND creditor.creditor_id = creditor_invoice.creditor_id 
			AND creditor_invoice.transaction_date >= creditor.start_date

		UNION ALL

		  SELECT
			`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
			`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
			`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
			`creditor_credit_note`.`invoice_number` AS `referenceCode`,
			`creditor_credit_note`.`document_number` AS `transactionCode`,
			'' AS `patient_id`,
		  	`creditor_credit_note`.`creditor_id` AS `recepientId`,
			`account`.`parent_account` AS `accountParentId`,
			`account_type`.`account_type_name` AS `accountsclassfication`,
			`account`.`account_id` AS `accountId`,
			`account`.`account_name` AS `accountName`,
			`creditor_credit_note_item`.`description` AS `transactionName`,
			`creditor_credit_note_item`.`description` AS `transactionDescription`,
			`creditor_credit_note_item`.`credit_note_amount` AS `dr_amount`,
			0 AS `cr_amount`,
			`creditor_credit_note`.`transaction_date` AS `transactionDate`,
			`creditor_credit_note`.`created` AS `createdAt`,
			`creditor_invoice`.`transaction_date` AS `referenceDate`,
			`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
			'Supplier Credit Note' AS `transactionCategory`,
			'Creditors Credit Notes' AS `transactionClassification`,
			'creditor_credit_note' AS `transactionTable`,
			'creditor_credit_note_item' AS `referenceTable`
		FROM
			(
				(
					(
						`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
						
					)
					JOIN account ON(
						(
							account.account_id = ".$accounts_payable_id."
						)
					)
					
				)
				JOIN `account_type` ON(
					(
						account_type.account_type_id = account.account_type_id
					)
				)
			)
		WHERE 
			creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id 
			AND creditor_credit_note.creditor_credit_note_status = 1
			AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
			AND creditor_invoice.creditor_invoice_status = 1
			AND creditor.creditor_id = creditor_invoice.creditor_id 
			AND creditor_invoice.transaction_date >= creditor.start_date



		UNION ALL


		SELECT
			`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
			`creditor_payment`.`creditor_payment_id` AS `referenceId`,
			`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
			`creditor_payment`.`reference_number` AS `referenceCode`,
			`creditor_payment`.`document_number` AS `transactionCode`,
			'' AS `patient_id`,
		  	`creditor_payment`.`creditor_id` AS `recepientId`,
			`account`.`parent_account` AS `accountParentId`,
			`account_type`.`account_type_name` AS `accountsclassfication`,
			`creditor_payment`.`account_from_id` AS `accountId`,
			`account`.`account_name` AS `accountName`,
			`creditor_payment_item`.`description` AS `transactionName`,
			CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
			0 AS `dr_amount`,
			`creditor_payment_item`.`amount_paid` AS `cr_amount`,
			`creditor_payment`.`transaction_date` AS `transactionDate`,
			`creditor_payment`.`created` AS `createdAt`,
			`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
			'creditor_payment.branch_id' AS `branch_id`,
			'Creditor Payment' AS `transactionCategory`,
			'Creditors Invoices Payments' AS `transactionClassification`,
			'creditor_payment' AS `transactionTable`,
			'creditor_payment_item' AS `referenceTable`
		FROM
			(
				(
					(
						`creditor_payment_item`,creditor_payment,creditor_invoice,creditor
						
					)
					JOIN account ON(
						(
							account.account_id = creditor_payment.account_from_id
						)
					)
				)
				JOIN `account_type` ON(
					(
						account_type.account_type_id = account.account_type_id
					)
				)
			
			)
			WHERE creditor_payment_item.invoice_type = 0 
			AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
			AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
			AND creditor_payment.creditor_payment_status = 1
			AND creditor_invoice.creditor_invoice_status = 1
			AND creditor_invoice.creditor_id = creditor.creditor_id
			AND creditor_payment.transaction_date >= creditor.start_date

		UNION ALL


		SELECT
			`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
			`creditor_payment`.`creditor_payment_id` AS `referenceId`,
			`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
			`creditor_payment`.`reference_number` AS `referenceCode`,
			`creditor_payment`.`document_number` AS `transactionCode`,
			'' AS `patient_id`,
		  	`creditor_payment`.`creditor_id` AS `recepientId`,
			`account`.`parent_account` AS `accountParentId`,
			`account_type`.`account_type_name` AS `accountsclassfication`,
			`account`.`account_id` AS `accountId`,
			`account`.`account_name` AS `accountName`,
			`creditor_payment_item`.`description` AS `transactionName`,
			CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
			`creditor_payment_item`.`amount_paid` AS `dr_amount`,
			0 AS `cr_amount`,
			`creditor_payment`.`transaction_date` AS `transactionDate`,
			`creditor_payment`.`created` AS `createdAt`,
			`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
			'creditor_payment.branch_id' AS `branch_id`,
			'Creditor Payment' AS `transactionCategory`,
			'Creditors Invoices Payments' AS `transactionClassification`,
			'creditor_payment' AS `transactionTable`,
			'creditor_payment_item' AS `referenceTable`
		FROM
			(
				(
					(
						`creditor_payment_item`,creditor_payment,creditor_invoice,creditor
						
					)
					JOIN account ON(
						(
							account.account_id = ".$accounts_payable_id."
						)
					)
				)
				JOIN `account_type` ON(
					(
						account_type.account_type_id = account.account_type_id
					)
				)
			
			)
			WHERE creditor_payment_item.invoice_type = 0 
			AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
			AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
			AND creditor_payment.creditor_payment_status = 1
			AND creditor_invoice.creditor_invoice_status = 1
			AND creditor_invoice.creditor_id = creditor.creditor_id
			AND creditor_payment.transaction_date >= creditor.start_date

		UNION ALL

			SELECT
				`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
				`creditor_payment`.`creditor_payment_id` AS `referenceId`,
				`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
				`creditor_payment`.`reference_number` AS `referenceCode`,
				`creditor_payment`.`document_number` AS `transactionCode`,
				'' AS `patient_id`,
				`creditor_payment`.`creditor_id` AS `recepientId`,
				`account`.`parent_account` AS `accountParentId`,
				`account_type`.`account_type_name` AS `accountsclassfication`,
				`creditor_payment`.`account_from_id` AS `accountId`,
				`account`.`account_name` AS `accountName`,
				`creditor_payment_item`.`description` AS `transactionName`,
				CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
				0 AS `dr_amount`,
				`creditor_payment_item`.`amount_paid` AS `cr_amount`,
				`creditor_payment`.`transaction_date` AS `transactionDate`,
				`creditor_payment`.`created` AS `createdAt`,
				`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
				'creditor_payment.branch_id' AS `branch_id`,
				'Creditor Payment' AS `transactionCategory`,
				'Creditors Invoices Payments' AS `transactionClassification`,
				'creditor_payment' AS `transactionTable`,
				'creditor_payment_item' AS `referenceTable`
				FROM
				(
					(
						(
							`creditor_payment_item`,creditor_payment,creditor
							
						)
						JOIN account ON(
							(
								account.account_id = creditor_payment.account_from_id
							)
						)
					)
					JOIN `account_type` ON(
						(
							account_type.account_type_id = account.account_type_id
						)
					)

				)
				WHERE creditor_payment_item.invoice_type = 2 
				AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
				AND creditor.creditor_id = creditor_payment_item.creditor_id
				AND creditor_payment.transaction_date >= creditor.start_date

		UNION ALL

			SELECT
				`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
				`creditor_payment`.`creditor_payment_id` AS `referenceId`,
				`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
				`creditor_payment`.`reference_number` AS `referenceCode`,
				`creditor_payment`.`document_number` AS `transactionCode`,
				'' AS `patient_id`,
				`creditor_payment`.`creditor_id` AS `recepientId`,
				`account`.`parent_account` AS `accountParentId`,
				`account_type`.`account_type_name` AS `accountsclassfication`,
				`account`.`account_id` AS `accountId`,
				`account`.`account_name` AS `accountName`,
				`creditor_payment_item`.`description` AS `transactionName`,
				CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
				`creditor_payment_item`.`amount_paid` AS `dr_amount`,
				0 AS `cr_amount`,
				`creditor_payment`.`transaction_date` AS `transactionDate`,
				`creditor_payment`.`created` AS `createdAt`,
				`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
				'creditor_payment.branch_id' AS `branch_id`,
				'Creditor Payment' AS `transactionCategory`,
				'Creditors Invoices Payments' AS `transactionClassification`,
				'creditor_payment' AS `transactionTable`,
				'creditor_payment_item' AS `referenceTable`
				FROM
				(
					(
						(
							`creditor_payment_item`,creditor_payment,creditor
							
						)
						JOIN account ON(
							(
								account.account_id = ".$accounts_payable_id."
							)
						)
					)
					JOIN `account_type` ON(
						(
							account_type.account_type_id = account.account_type_id
						)
					)

				)
				WHERE creditor_payment_item.invoice_type = 2 
				AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
				AND creditor.creditor_id = creditor_payment_item.creditor_id
				AND creditor_payment.transaction_date >= creditor.start_date

			UNION ALL

				SELECT
					`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
					`creditor_payment`.`creditor_payment_id` AS `referenceId`,
					`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_payment`.`reference_number` AS `referenceCode`,
					`creditor_payment`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
					`creditor_payment`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`creditor_payment`.`account_from_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_payment_item`.`description` AS `transactionName`,
					CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
					0 AS `dr_amount`,
					`creditor_payment_item`.`amount_paid` AS `cr_amount`,
					`creditor_payment`.`transaction_date` AS `transactionDate`,
					`creditor_payment`.`created` AS `createdAt`,
					`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
					'creditor_payment.branch_id' AS `branch_id`,
					'Creditor Payment' AS `transactionCategory`,
					'Creditors Invoices Payments' AS `transactionClassification`,
					'creditor_payment' AS `transactionTable`,
					'creditor_payment_item' AS `referenceTable`
					FROM
					(
						(
							(
								`creditor_payment_item`,creditor_payment,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = creditor_payment.account_from_id
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
						
					)
					WHERE creditor_payment_item.invoice_type = 3
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
					AND creditor.creditor_id = creditor_payment_item.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date
			UNION ALL

				SELECT
					`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
					`creditor_payment`.`creditor_payment_id` AS `referenceId`,
					`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
					`creditor_payment`.`reference_number` AS `referenceCode`,
					`creditor_payment`.`document_number` AS `transactionCode`,
					'' AS `patient_id`,
					`creditor_payment`.`creditor_id` AS `recepientId`,
					`account`.`parent_account` AS `accountParentId`,
					`account_type`.`account_type_name` AS `accountsclassfication`,
					`account`.`account_id` AS `accountId`,
					`account`.`account_name` AS `accountName`,
					`creditor_payment_item`.`description` AS `transactionName`,
					CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
					`creditor_payment_item`.`amount_paid` AS `dr_amount`,
					0 AS `cr_amount`,
					`creditor_payment`.`transaction_date` AS `transactionDate`,
					`creditor_payment`.`created` AS `createdAt`,
					`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
					'creditor_payment.branch_id' AS `branch_id`,
					'Creditor Payment' AS `transactionCategory`,
					'Creditors Invoices Payments' AS `transactionClassification`,
					'creditor_payment' AS `transactionTable`,
					'creditor_payment_item' AS `referenceTable`
					FROM
					(
						(
							(
								`creditor_payment_item`,creditor_payment,creditor
								
							)
							JOIN account ON(
								(
									account.account_id = ".$accounts_payable_id."
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)
						
					)
					WHERE creditor_payment_item.invoice_type = 3
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
					AND creditor.creditor_id = creditor_payment_item.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date


				UNION ALL

					SELECT
						`order_supplier`.`order_supplier_id` AS `transactionId`,
						`orders`.`order_id` AS `referenceId`,
						'' AS `payingFor`,
						`orders`.`supplier_invoice_number` AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					 	`orders`.`supplier_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						'Drug Purchase' AS `transactionName`,
						CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
						SUM(`order_supplier`.`less_vat`) AS `dr_amount`,
						0 AS `cr_amount`,
						`orders`.`supplier_invoice_date` AS `transactionDate`,
						`orders`.`created` AS `createdAt`,
						`orders`.`supplier_invoice_date` AS `referenceDate`,
						`orders`.`order_approval_status` AS `status`,
						'Purchases' AS `transactionCategory`,
						'Supplies Invoices' AS `transactionClassification`,
						'order_supplier' AS `transactionTable`,
						'orders' AS `referenceTable`
					FROM
						`order_supplier`,orders,account,order_item,product,account_type,creditor
								

					WHERE orders.is_store = 0 
						AND orders.supplier_id = creditor.creditor_id
						AND orders.order_id = order_supplier.order_id
						AND order_item.order_item_id = order_supplier.order_item_id
						AND product.product_id = order_item.product_id
						AND account_type.account_type_id = account.account_type_id
						AND account.account_id = orders.account_id
						AND orders.order_approval_status = 7 
						AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
						AND orders.supplier_invoice_date >= creditor.start_date
						GROUP BY order_supplier.order_id

					UNION ALL



					SELECT
						`order_supplier`.`order_supplier_id` AS `transactionId`,
						`orders`.`order_id` AS `referenceId`,
						'' AS `payingFor`,
						`orders`.`supplier_invoice_number` AS `referenceCode`,
						'' AS `transactionCode`,
						'' AS `patient_id`,
					 	`orders`.`supplier_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						'Drug Purchase' AS `transactionName`,
						CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
						0 AS `dr_amount`,
						SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
						`orders`.`supplier_invoice_date` AS `transactionDate`,
						`orders`.`created` AS `createdAt`,
						`orders`.`supplier_invoice_date` AS `referenceDate`,
						`orders`.`order_approval_status` AS `status`,
						'Purchases' AS `transactionCategory`,
						'Supplies Invoices' AS `transactionClassification`,
						'order_supplier' AS `transactionTable`,
						'orders' AS `referenceTable`
					FROM
						`order_supplier`,orders,account,order_item,product,account_type,creditor
								

					WHERE orders.is_store = 0 
						AND orders.supplier_id = creditor.creditor_id
						AND orders.order_id = order_supplier.order_id
						AND order_item.order_item_id = order_supplier.order_item_id
						AND product.product_id = order_item.product_id
						AND account_type.account_type_id = account.account_type_id
						AND account.account_id = ".$accounts_payable_id."
						AND orders.order_approval_status = 7 
						AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
						AND orders.supplier_invoice_date >= creditor.start_date
						GROUP BY order_supplier.order_id

					UNION ALL

					SELECT
						`order_supplier`.`order_supplier_id` AS `transactionId`,
						`orders`.`order_id` AS `referenceId`,
						'' AS `payingFor`,
						`orders`.`supplier_invoice_number` AS `referenceCode`,
						`orders`.`reference_number`  AS `transactionCode`,
						'' AS `patient_id`,
					  `orders`.`supplier_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`orders`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						'Credit' AS `transactionName`,
						CONCAT('Credit note of ',' ',`orders`.`reference_number`) AS `transactionDescription`,
						'0' AS `dr_amount`,
						SUM(`order_supplier`.`less_vat`) AS `cr_amount`,
						`orders`.`supplier_invoice_date` AS `transactionDate`,
						`orders`.`created` AS `createdAt`,
						`orders`.`supplier_invoice_date` AS `referenceDate`,
						`orders`.`order_approval_status` AS `status`,
						'Income' AS `transactionCategory`,
						'Supplies Credit Note' AS `transactionClassification`,
						'order_supplier' AS `transactionTable`,
						'orders' AS `referenceTable`

					FROM
						`order_supplier`,orders,order_item,product,account,account_type,creditor
						
						WHERE orders.is_store = 3
						AND orders.order_id = order_supplier.order_id
						AND account.account_id = orders.account_id
						AND order_item.order_item_id = order_supplier.order_item_id
						AND product.product_id = order_item.product_id
						AND account_type.account_type_id = account.account_type_id
						 AND orders.supplier_id = creditor.creditor_id and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
						 AND orders.supplier_invoice_date >= creditor.start_date
						GROUP BY order_supplier.order_id

					UNION ALL

					SELECT
						`order_supplier`.`order_supplier_id` AS `transactionId`,
						`orders`.`order_id` AS `referenceId`,
						'' AS `payingFor`,
						`orders`.`supplier_invoice_number` AS `referenceCode`,
						`orders`.`reference_number`  AS `transactionCode`,
						'' AS `patient_id`,
					  `orders`.`supplier_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						`account`.`account_id` AS `accountId`,
						`account`.`account_name` AS `accountName`,
						'Credit' AS `transactionName`,
						CONCAT('Credit note of ',' ',`orders`.`supplier_invoice_number`) AS `transactionDescription`,
						SUM(`order_supplier`.`less_vat`)  AS `dr_amount`,
						0 AS `cr_amount`,
						`orders`.`supplier_invoice_date` AS `transactionDate`,
						`orders`.`created` AS `createdAt`,
						`orders`.`supplier_invoice_date` AS `referenceDate`,
						`orders`.`order_approval_status` AS `status`,
						'Income' AS `transactionCategory`,
						'Supplies Credit Note' AS `transactionClassification`,
						'order_supplier' AS `transactionTable`,
						'orders' AS `referenceTable`

					FROM
						`order_supplier`,orders,account,order_item,product,account_type,creditor
									

						WHERE orders.is_store = 3
						AND account.account_id = ".$accounts_payable_id."
						AND account_type.account_type_id = account.account_type_id
						AND product.product_id = order_item.product_id
						AND orders.order_id = order_supplier.order_id
						AND order_item.order_item_id = order_supplier.order_item_id
						AND orders.supplier_id = creditor.creditor_id and orders.order_approval_status = 7 AND order_supplier.order_item_id IN (select order_item.order_item_id FROM order_item)
						AND orders.supplier_invoice_date >= creditor.start_date
						GROUP BY order_supplier.order_id



					UNION ALL

					 SELECT
						`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
						`creditor_payment`.`creditor_payment_id` AS `referenceId`,
						`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
						`creditor_payment`.`reference_number` AS `referenceCode`,
						`creditor_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`creditor_payment`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						creditor_payment.account_from_id AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
						0 AS `dr_amount`,
						`creditor_payment_item`.`amount_paid` AS `cr_amount`,
						`creditor_payment`.`transaction_date` AS `transactionDate`,
						`creditor_payment`.`created` AS `createdAt`,
						`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
						'creditor_payment.branch_id' AS `branch_id`,
						'Creditor Payment' AS `transactionCategory`,
						'Creditors Invoices Payments' AS `transactionClassification`,
						'creditor_payment' AS `transactionTable`,
						'creditor_payment_item' AS `referenceTable`
						FROM
						(
							(
								(
									`creditor_payment_item`,creditor_payment,orders,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = creditor_payment.account_from_id
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)
							
						)
					WHERE creditor_payment_item.invoice_type = 1 
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
					AND creditor_payment.creditor_payment_status = 1 
					AND orders.order_id = creditor_payment_item.creditor_invoice_id
					AND orders.supplier_id = creditor.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date

					UNION ALL

					SELECT
						`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
						`creditor_payment`.`creditor_payment_id` AS `referenceId`,
						`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
						`creditor_payment`.`reference_number` AS `referenceCode`,
						`creditor_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`creditor_payment`.`creditor_id` AS `recepientId`,
						`account`.`parent_account` AS `accountParentId`,
						`account_type`.`account_type_name` AS `accountsclassfication`,
						account.account_id AS `accountId`,
						`account`.`account_name` AS `accountName`,
						`creditor_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
						`creditor_payment_item`.`amount_paid` AS `dr_amount`,
						0 AS `cr_amount`,
						`creditor_payment`.`transaction_date` AS `transactionDate`,
						`creditor_payment`.`created` AS `createdAt`,
						`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
						'creditor_payment.branch_id' AS `branch_id`,
						'Creditor Payment' AS `transactionCategory`,
						'Creditors Invoices Payments' AS `transactionClassification`,
						'creditor_payment' AS `transactionTable`,
						'creditor_payment_item' AS `referenceTable`
						FROM
						(
							(
								(
									`creditor_payment_item`,creditor_payment,orders,creditor
									
								)
								JOIN account ON(
									(
										account.account_id = ".$accounts_payable_id."
									)
								)
							)
							JOIN `account_type` ON(
								(
									account_type.account_type_id = account.account_type_id
								)
							)
							
						)
					WHERE creditor_payment_item.invoice_type = 1 
					AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id  
					AND creditor_payment.creditor_payment_status = 1 
					AND orders.order_id = creditor_payment_item.creditor_invoice_id
					AND orders.supplier_id = creditor.creditor_id
					AND creditor_payment.transaction_date >= creditor.start_date

				UNION ALL

					-- bank reconcilliation


		SELECT

			bank_reconcilliation.recon_id AS `transactionId`,
			'' AS `referenceId`,
			'' AS `payingFor`,
			'' AS `referenceCode`,
			'' AS `transactionCode`,
			'' AS `patient_id`,
		  	'' AS `recepientId`,
			account_type.account_type_id AS `accountParentId`,
			account_type.account_type_name AS `accountsclassfication`,
			`account`.`account_id` AS `accountId`,
			account.account_name AS `accountName`,
			'' AS `transactionName`,
			CONCAT('Interest ', account.account_name ) AS `transactionDescription`,
			SUM(bank_reconcilliation.interest_earned) AS `dr_amount`,
			0 AS `cr_amount`,
			bank_reconcilliation.interest_date AS `transactionDate`,
			bank_reconcilliation.interest_date AS `createdAt`,
			bank_reconcilliation.recon_status AS `status`,
			2 AS `branch_id`,
			'Deposit' AS `transactionCategory`,
			'Payments' AS `transactionClassification`,
			'account_payments' AS `transactionTable`,
			'' AS `referenceTable`
		FROM
			bank_reconcilliation,account,account_type
			WHERE bank_reconcilliation.recon_status = 2
			AND bank_reconcilliation.account_id = account.account_id
			AND account.account_type_id = account_type.account_type_id
		GROUP BY bank_reconcilliation.recon_id



		UNION ALL

		SELECT

			bank_reconcilliation.recon_id AS `transactionId`,
			'' AS `referenceId`,
			'' AS `payingFor`,
			'' AS `referenceCode`,
			'' AS `transactionCode`,
			'' AS `patient_id`,
		  	'' AS `recepientId`,
			account_type.account_type_id AS `accountParentId`,
			account_type.account_type_name AS `accountsclassfication`,
			`account`.`account_id` AS `accountId`,
			account.account_name AS `accountName`,
			'' AS `transactionName`,
			CONCAT('Interest ', account.account_name ) AS `transactionDescription`,
			0 AS `dr_amount`,
			SUM(bank_reconcilliation.interest_earned) AS `cr_amount`,
			bank_reconcilliation.interest_date AS `transactionDate`,
			bank_reconcilliation.interest_date AS `createdAt`,
			bank_reconcilliation.recon_status AS `status`,
			2 AS `branch_id`,
			'Deposit' AS `transactionCategory`,
			'Payments' AS `transactionClassification`,
			'account_payments' AS `transactionTable`,
			'' AS `referenceTable`
		FROM
			bank_reconcilliation,account,account_type
			WHERE bank_reconcilliation.recon_status = 2
			AND bank_reconcilliation.interest_account_id = account.account_id
			AND account.account_type_id = account_type.account_type_id
		GROUP BY bank_reconcilliation.recon_id";


echo $result;
?>