
<?php
//unset the sessions set\
$search = $this->session->userdata('accounts_search');
$search_title = $this->session->userdata('accounts_search_title');//echo $account;die();

$account_ledger_search = $this->session->userdata('account_ledger_search');
$search_title  = '';
$operation_result = '';
if($account_ledger_search == 1)
{

	$account = $this->session->userdata('account_id');
	$search_title = $this->session->userdata('search_title');
	$account_date_from = $this->session->userdata('account_date_from');
    $account_date_to = $this->session->userdata('account_date_to');
	if(!empty($account_date_from) AND !empty($account_date_to))
	{
		$search_title .= ' FROM PERIOD BETWEEN '.$account_date_from.'  AND '.$account_date_to.'';
	}
	else if(!empty($account_date_from) AND empty($account_date_to))
	{
		$search_title .= ' FOR "'.$account_date_from.'"';
	}
	else if(empty($account_date_from) AND !empty($account_date_to))
	{
		$search_title .= ' FOR "'.$account_date_to.'"';
	}
	else
	{
		$search_title .= '';
	}
	// $opening_bal = $this->petty_cash_model->get_account_opening_bal($account);


	$operation_rs = $this->ledgers_model->get_account_ledger($account);
	// var_dump($operation_rs);die();
	$operation_result = '<tbody>';
	$total_operational_amount = 0;
	$balance = 0;

	$total_operational_amount = $balance;
	$total_dr_amount = 0;
	$total_cr_amount = 0;
	if($operation_rs->num_rows() > 0)
	{
		foreach ($operation_rs->result() as $key => $value) {
			# code...
			$dr_amount = $value->dr_amount;
			$cr_amount = $value->cr_amount;
			$transactionName = $value->accountName;
			$account_id = $value->accountId;
			$transactionDescription = $value->transactionDescription;


			$transactionCategory = $value->transactionCategory;
			$transactionName = $value->transactionName;
			$transactionDate = $value->transactionDate;
			$transactionCode = $value->transactionCode;
			$referenceCode = $value->referenceCode;

			if(empty($transactionCode))
			{
				$referenceCode = $transactionCode;
			}
			$total_operational_amount += $dr_amount;
			
			$accountsclassfication = $value->accountsclassfication;

			if($accountsclassfication == "Equity")
			{
				$cr_amount = $cr_amount;
				$total_operational_amount -= $cr_amount;
			}
			else
			{
				$total_operational_amount -= $cr_amount;
			}


			$total_dr_amount += $dr_amount;
			$total_cr_amount += $cr_amount;
			$operation_result .='<tr>
	                          <td class="text-left">'.strtoupper($transactionDate).'</td>
	            							<td class="text-left">'.strtoupper($transactionDescription).'</td>

	                          <td class="text-left">'.strtoupper($transactionCode).'</td>
	            							<td class="text-right">'.number_format($dr_amount,2).'</td>
	                          <td class="text-right">'.number_format($cr_amount,2).'</td>
	                          <td class="text-right">'.number_format($total_operational_amount,2).'</td>
	            							</tr>';
		}


		$operation_result .='
							</tbody>
							<tfoot>
											<tr>
	                         

	                          <th class="text-left" colspan="2"></th>
	                           <th class="text-left">TOTAL</th>
	            			  <th class="text-right">'.number_format($total_dr_amount,2).'</th>
	                          <th class="text-right">'.number_format($total_cr_amount,2).'</th>
	                          <th class="text-right">'.number_format($total_dr_amount - $total_cr_amount,2).'</th>
	            			</tr>
	            </tfoot>';

	}
}


?>

<!--end reports -->
<div class="row">
	<div class="col-md-12">
	<div class="col-md-4">
		<?php echo $this->load->view('search_ledgers', '', TRUE);?>

		<?php
			if(!empty($account_ledger_search))
			{
				?>
				<div class="row">
					<div class="col-md-12">
						<div class="pull-left">
							 <a href="<?php echo base_url().'financials/ledgers/close_search_ledger';?>" class="btn btn-sm btn-danger"><i class="fa fa-cancel"></i> Close Search</a>
						</div>
						<div class="pull-right">
							<a class="btn btn-sm btn-success" target="_blank" onclick="javascript:xport.toCSV('<?php echo $search_title?>');"><i class="fa fa-export"></i> Export Statement</a>
							<a href="<?php echo base_url().'print-account-ledger';?>" class="btn btn-sm btn-warning" target="_blank"><i class="fa fa-print"></i> Print Statement</a>
						</div>
					</div>
				</div>
				<br>
               

                
                <?php
			}
		?>
	</div>
    <div class="col-md-8">

        <section class="panel panel-primary">
            <header class="panel-heading">
                
                <h6 class="panel-title center-align"><?php echo strtoupper($search_title);?></h6>
            </header>
            
            <div class="panel-body" style="height:80vh !important;overflow-y: scroll;padding: 0px;">
               
              
		


				<table class="table table-hover table-hover table-bordered  table-linked" id="<?php echo $search_title?>">
				 	<thead>
						<tr>
						  <th>Transaction Date</th>
						  <th>Description</th>
						  <th>Voucher</th>
						  <th>Debit</th>
              <th>Credit</th>
              <th>Arrears</th>
						</tr>
					 </thead>
				  	
              			<?php echo $operation_result;?>
				
					</table>
          	</div>
		</section>
    </div>
    
</div>
</div>

<script type="text/javascript">
	

	
	$(document).on("change","select#transaction_type_id",function(e)
	{
		var transaction_type_id = $(this).val();
		
		if(transaction_type_id == '1')
		{
			// deposit
			$('#from_account_div').css('display', 'block');
			$('#account_to_div').css('display', 'block');
			// $('#consultation').css('display', 'block');
		}
		else if(transaction_type_id == '2')
		{
			// expenditure
			$('#from_account_div').css('display', 'block');
			$('#account_to_div').css('display', 'none');
			// $('#consultation').css('display', 'block');
		}
		else
		{
			$('#from_account_div').css('display', 'none');
			$('#account_to_div').css('display', 'none');
		}
		
		
	});
</script>

<script type="text/javascript">
		var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};


</script>