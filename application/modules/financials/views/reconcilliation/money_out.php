<?php 

$money_out_rs = $this->reconcilliation_model->get_money_out($recon_id);


$result = '';
if($money_out_rs->num_rows() > 0)
{
	foreach ($money_out_rs->result() as $key => $value) {
		# code...
		$payment_date = $value->payment_date;
		$cheque = $value->cheque;
		$payee = $value->payee;
		$amount = $value->amount;
		$type = $value->type;
		$transactionid = $value->transactionid;
		$recon_idd = $value->recon_id;
		$recepientId = $value->recepientId;

		if($recon_idd > 0)
		{
			$marked = 'checked';
		}
		else
		{
			$marked ='';
		}


		$link = '';
		if($type == 1)
		{
			// creditor
			$link = 'open-creditor-payments/'.$recepientId.'/'.$transactionid;
			
			
		}
		else if($type == 2)
		{
			// account payments
			$link = 'open-account-payments/'.$transactionid;
			
		}
		else if($type == 3)
		{
			// transfers

			$link = 'open-transfers/'.$transactionid;
		}
		else if($type == 4)
		{
			// payroll payment

			$link = 'open-payroll-payment/'.$transactionid;
		}

		else if($type == 5)
		{
			// payroll payment

			$link = 'open-journals/'.$transactionid;
		}

		// else if($type == 6)
		// {
		// 	// payroll payment

		// 	$link = 'petty-cash/'.$transactionid;
		// }



		 $checkbox_data = array(
                    'name'        => 'money_out_items[]',
                    'id'          => 'checkbox'.$transactionid,
                    'class'          => 'css-checkbox  lrg ',
                    'checked'=>$marked,
                    'value'       => $transactionid,
                    'onclick'=>'mark_transaction_out('.$transactionid.','.$recon_id.','.$type.')'
                  );
		$result .= '<tr>	
						 <td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$transactionid.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
						<td>'.$payment_date.'</td>
						<td>'.$cheque.'</td>
						<td>'.$payee.'</td>
						<td><a href="'.site_url().$link.'">'.number_format($amount,2).'</a></td>


					</tr>';
		

	}

}



echo $result;

?>