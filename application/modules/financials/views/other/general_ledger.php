

<?php
	$date_from = $this->session->userdata('date_from_general_ledger');
	$date_to = $this->session->userdata('date_to_general_ledger');
	$add = '';
	if(!empty($date_from) OR !empty($date_to))
	{

		$date_from = $date_from;
		$date_to = $date_to;

	}

	else
	{

		$date_from = date('Y-m-01');
		$date_to = date('Y-m-d');
		

	}

	$search_title = $this->session->userdata('general_ledger_search_title');



$this->db->where('account_staging_delete = 0');
$accounts_config_rs = $this->db->get('account_staging');

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;

		
	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];
$cost_of_goods_id = $session_account['cost_of_goods_id'];
$expense_account_id = $session_account['expense_account_id'];
$other_income_account_id = $session_account['other_income_account_id'];

error_reporting(0);
// echo "<br>I'm at: ". getcwd();
include_once "application/modules/financials/controllers/financial_report.php";
// if($opening_tb_status)
// {
// 	$opening_tb_status = TRUE;
// 	$title = 'Opening TB';
// 	$financial_class = new Financial_Report(array('opening_tb'=>$opening_tb_status,'trial_balance'=>false,
// 			"date" => $this->session->userdata('date_to_general_ledger')));
// }
// else
// {
	$opening_tb_status = FALSE;
		$title = 'Trial Balance';

		$financial_class = new Financial_Report(array('opening_tb'=>$opening_tb_status,'trial_balance'=>true,
			"date" => $this->session->userdata('date_to_general_ledger')));
// }

$date_from = $this->session->userdata('date_from_general_ledger');
$date_to = $this->session->userdata('date_to_general_ledger');
$arrDone = array();


$fixed_asset_result = '';
?>



<div class="row">
	<div class="col-md-4">
		<section class="panel">
				<header class="panel-heading">
						<h5 class="pull-left"><i class="icon-reorder"></i>Search </h5>
						<div class="clearfix"></div>
				</header>
				<!-- /.box-header -->
				<div class="panel-body">
					<?php echo form_open("financials/company_financial/search_general_ledger", array("class" => "form-horizontal"));?>
		      		<div class="col-md-12">
		      			<div class="form-group" style="display: none;">
							<label class="col-md-4 control-label">Date From: </label>

							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
									</span>
									<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date From" value="<?php echo $date_from?>" autocomplete="off" >
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Date To: </label>

							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
									</span>
									<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder=" Date to" value="<?php echo $date_to?>" autocomplete="off" required>
								</div>
							</div>
						</div>
						<div class="form-group">
			                <div class="col-lg-8 col-lg-offset-2">
			                	<div class="center-align">
			                   		<button type="submit" class="btn btn-info">Search</button>
			    				</div>
			                </div>
			            </div>
		      		</div>
		      		<?php echo form_close();?>
		      	</div>
		    </section>

		<div class="text-center">
			<h3 class="box-title">General Ledger</h3>
			<h5 class="box-title"><?php echo $search_title?></h5>
			<h6 class="box-title">Created <?php echo date('jS M Y');?></h6>


			

		</div>
		<hr>
		<?php
		$search_title = 'GENERAL LEDGER '.$search_title;
		?>
		<div class="col-md-12">
			<div class="col-md-12" style="margin-bottom:20px">
				
				<div class="col-md-12">
					<div class="form-group">
						<a href="<?php echo site_url().'print-general-ledger'?>" target="_blank" class="btn btn-md btn-warning col-md-12"><i class="fa fa-print"></i> Print G L</a>
					</div>
				</div>
				
			</div>
			<br>
			<div class="col-md-12">
				
				<div class="form-group">
				      <a href="<?php echo site_url().'company-financials/profit-and-loss'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> Income Statement</a>
				</div>
				<div class="form-group">
				      <a href="<?php echo site_url().'company-financials/balance-sheet'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Balance Sheet</a>
				</div>
				<div class="form-group">
				      <a href="<?php echo site_url().'company-financials/trial-balance'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Trial Balance</a>
				</div>
			</div>
			
		</div>
	</div>


	<div class="col-md-8">
		
			<div class="col-md-12" style="margin-top:9px;">
			    <div class="panel-body" style="height:87vh;overflow-y:scroll;padding:0px !important;">
			    	<table class="table  table-striped table-condensed table-linked" id="<?php echo $search_title?>">
						<thead>
							<tr>
								<th >Num.</th>
			        			<th >DATE</th>
								<th >CATEGORY</th>
								<th >SERIAL</th>
								<th >DESCRIPTION</th>
								<th class="center-align">DEBIT</th>
								<th class="center-align">CREDIT</th>
								<th class="center-align">AMOUNT</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$counter = 0;


							
							foreach($financial_class->arrData() as $section => $data)
							{
								// code...

									// echo "<tr>
									// 			<th colspan='3'>".ucfirst($section)."</th>
									// 		</tr>";
									// foreach ($data as $key => $value) {


							

										echo "<tr>";
										echo "<td style='text-align:left;border:1px #000 solid;' class='warning' colspan='8'>".strtoupper($section)."</td>";
										echo "</tr>";

										$item = $financial_class->arrdataAllList()[$section];
										$grand_dr_total = $grand_cr_total = $grand_total = 0;
										$keys = array_keys($item); 
										foreach ($keys as $key) {
											// code...

											$account_id = $key;
											$account_name = $this->company_financial_model->get_account_name($key);
											

											echo "<tr>";
											echo "<td style='text-align:left;border:1px #000 solid;' class='primary' colspan='8'>$account_name</td>";
											echo "</tr>";
											$dr_total = $cr_total = $total = 0;
											$item = $financial_class->arrdataAllList()[$section][$account_id];
											foreach($item as $key=> $data)
											{
												$counter++;
												//$total += $data["amount"];
												$debcred = $data["debcred"];

												$link = $data["link"];
												$referenceId = $data["referenceId"];
												$link_debit = '';
												if($debcred == "debit")
												{

													if(!empty($link))
														$link_debit = "<a href='".site_url()."".$link."' target='_blank'>".number_format($data["amount"],2)."</a>";
													else
														$link_debit = number_format($data["amount"],2);
												}

												$link_credit = '';
												if($debcred == "credit")
												{

													if(!empty($link))
														$link_credit = "<a href='".site_url()."".$link."' target='_blank'>".number_format($data["amount"],2)."</a>";
													else
														$link_credit = number_format($data["amount"],2);
												}




																			
												// if($new_day)
												// 	$day = new DateTime($date);
												
												echo "<tr>";
												echo "<td style='text-align:center;border:1px #000 solid;'>$counter</td>";
												echo "<td style='border:1px #000 solid;'>".date('jS M Y',strtotime($data["date"]))."</td>";
												echo "<td style='border:1px #000 solid;'>".$data["cat"]."</td>";
												echo "<td style='border:1px #000 solid;'>".$key."</td>";
												echo "<td style='border:1px #000 solid;'>".$data["desc"]."</td>";

												if($debcred == "debit"){
													$dr_total = $grand_dr_total += $data["amount"];
													$grand_total = $total += $data["amount"];
													echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid;'>".$link_debit."</td>";
													echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid;'></td>";
												}
												else if($debcred == "credit"){
													$grand_cr_total = $cr_total += $data["amount"];
													$grand_total = $total -= $data["amount"];
													echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid;'></td>";
													echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid;'>".$link_credit."</td>";
												}
												else{
													echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid; color:red'>Error ".$data["amount"]."</td>";
													echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid; color:red'>Error</td>";
												}
												echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid;'>".number_format($total,2)."</td>";
												echo "</tr>";
											
											}

											echo '<tr >
													<th style="z-index: 2;background: #cb6d51;color: #fff !important;text-align: left;border:1px solid #000;" colspan="5">TOTAL '.strtoupper($account_name).'</th>
													
													<th style="z-index: 2;background: #cb6d51;color: #fff !important;text-align: right;border:2px solid #000;">'.number_format($dr_total,2).'</th>
													<th style="z-index: 2;background: #cb6d51;color: #fff !important;text-align: right;border:2px solid #000;">'.number_format($cr_total,2).'</th>
													<th style="z-index: 2;background: #cb6d51;color: #fff !important;text-align: right;border:2px solid #000;">'.number_format($total,2).'</th>
												</tr>';





										}

										echo '<tr >
													<th style="z-index: 2;background: lightblue;color: #000 !important;text-align: left;border:1px solid #000;" colspan="5">TOTAL '.strtoupper($section).'</th>
													
													<th style="z-index: 2;background: lightblue;color: #000 !important;text-align: right;border:2px solid #000;">'.number_format($grand_dr_total,2).'</th>
													<th style="z-index: 2;background: lightblue;color: #000 !important;text-align: right;border:2px solid #000;">'.number_format($grand_cr_total,2).'</th>
													<th style="z-index: 2;background: lightblue;color: #000 !important;text-align: right;border:2px solid #000;">'.number_format($grand_total,2).'</th>
												</tr>';
									// }
							}
							
					
						?>
						</tbody>
					</table>

			    </div>
			</div>
			
		
	</div>

</div>
<script type="text/javascript">
		var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};


</script>