<?php

                
    $date_from = $this->session->userdata('date_from_general_ledger');
    $date_to = $this->session->userdata('date_to_general_ledger');
    $add = '';
    if(!empty($date_from) OR !empty($date_to))
    {

        $date_from = $date_from;
        $date_to = $date_to;

    }

    else
    {

        $date_from = date('Y-m-01');
        $date_to = date('Y-m-d');
        

    }

    $search_title = $this->session->userdata('general_ledger_search_title');



$this->db->where('account_staging_delete = 0');
$accounts_config_rs = $this->db->get('account_staging');

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
    foreach ($accounts_config_rs->result() as $key => $value) {
        // code...
        $staing_account_id = $value->account_id;
        $reference_name = $value->reference_name;

        $session_account[$reference_name] = $staing_account_id;

        
    }
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];
$cost_of_goods_id = $session_account['cost_of_goods_id'];
$expense_account_id = $session_account['expense_account_id'];
$other_income_account_id = $session_account['other_income_account_id'];

error_reporting(0);
// echo "<br>I'm at: ". getcwd();
include_once "application/modules/financials/controllers/financial_report.php";
// if($opening_tb_status)
// {
//  $opening_tb_status = TRUE;
//  $title = 'Opening TB';
//  $financial_class = new Financial_Report(array('opening_tb'=>$opening_tb_status,'trial_balance'=>false,
//          "date" => $this->session->userdata('date_to_general_ledger')));
// }
// else
// {
    $opening_tb_status = FALSE;
        $title = 'Trial Balance';

        $financial_class = new Financial_Report(array('opening_tb'=>$opening_tb_status,'trial_balance'=>true,
            "date" => $this->session->userdata('date_to_general_ledger')));
// }

$date_from = $this->session->userdata('date_from_general_ledger');
$date_to = $this->session->userdata('date_to_general_ledger');
$arrDone = array();


$fixed_asset_result = '';
 
    ?>






<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | BALANCE SHEET</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script>

        <script src="<?php echo base_url()."assets/"?>table-resources/table2excel/jquery.table2excel.min.js"></script>
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
                font-size:9px;                
            }
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
                        .center-align{margin:0 auto; text-align:center;}

                        .receipt_bottom_border{border-bottom: #888888 medium solid;}
                        .row .col-md-12 table {
                            /*border:solid #000 !important;*/
                            /*border-width:1px 0 0 1px !important;*/
                            font-size:12px;
                            margin-top:10px;
                        }
                        .col-md-6 {
                            width: 50%;
                         }
                        .row .col-md-12 th, .row .col-md-12 td {
                            /*border:solid #000 !important;*/
                            /*border-width:0 1px 1px 0 !important;*/
                        }
                        .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
                        {
                             /*padding: 2px;*/
                             padding: 5px;
                        }
                        h3
                        {
                            font-size: 30px;
                        }
                        .col-print-1 {width:8%;  float:left;}
                        .col-print-2 {width:16%; float:left;}
                        .col-print-3 {width:25%; float:left;}
                        .col-print-4 {width:33%; float:left;}
                        .col-print-5 {width:42%; float:left;}
                        .col-print-6 {width:50%; float:left;}
                        .col-print-7 {width:58%; float:left;}
                        .col-print-8 {width:66%; float:left;}
                        .col-print-9 {width:75%; float:left;}
                        .col-print-10{width:83%; float:left;}
                        .col-print-11{width:92%; float:left;}
                        .col-print-12{width:100%; float:left;}


                        
                        

                        .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
                        .title-img{float:left; padding-left:30px;}
                        img.logo{ margin:0 auto;}
            
        </style>
        

        <script language="javascript">
            function doc_keyUp(e) {
                // this would test for whichever key is 40 and the ctrl key at the same time
                if (e.keyCode === 69) {
                    alert("Your Excel Document is being prepared and should start downloading in a few. Don't hit E again until it does.");
                    // call your function to do the thing
                    downloadExcel();
                }
            }
        // register the handler 
            document.addEventListener('keyup', doc_keyUp, false);
            function downloadExcel() {
                // alert("sasa")
                $("#testTable").table2excel({
                    exclude: ".noExl",
                    name: '<?php echo $search_title?>',
                    filename: "<?php echo $search_title?>.xls",
                    fileext: ".xls",
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true
                });
            }
        </script>
    </head>
    <body class="receipt_spacing">
        <div class="row">
            <div class="col-xs-12">
                <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 center-align receipt_bottom_border">
                <strong>
                    <?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>

      <div class="row receipt_bottom_border" >
            <div class="col-md-12 center-align" style="padding: 5px;">
                <strong>GENERAL LEDGER</strong><br>

               <?php
                // $search_title = $this->session->userdata('income_statement_title_search');

                     if(empty($search_title))
                     {
                        $search_title = "";
                     }
                     else
                     {
                        $search_title =$search_title;
                     }
                 echo $search_title;
                ?>


            </div>
        </div>

        <div class="row">
            <div style="margin: auto;">
                <div class="col-md-12"> 
                    
                        <table class="table  table-striped table-condensed table-linked" id="testTable">
                        <thead>
                            <tr>
                                <th style="text-align:left;border:1px #000 solid;" >Num.</th>
                                <th style="text-align:left;border:1px #000 solid;" >DATE</th>
                                <th style="text-align:left;border:1px #000 solid;" >CATEGORY</th>
                                <th style="text-align:left;border:1px #000 solid;" >SERIAL</th>
                                <th style="text-align:left;border:1px #000 solid;" >DESCRIPTION</th>
                                <th style="text-align:left;border:1px #000 solid;" class="center-align">DEBIT</th>
                                <th style="text-align:left;border:1px #000 solid;" class="center-align">CREDIT</th>
                                <th style="text-align:left;border:1px #000 solid;" class="center-align">AMOUNT</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $counter = 0;


                            
                            foreach($financial_class->arrData() as $section => $data)
                            {
                                // code...

                                    // echo "<tr>
                                    //          <th colspan='3'>".ucfirst($section)."</th>
                                    //      </tr>";
                                    // foreach ($data as $key => $value) {


                            

                                        echo "<tr>";
                                        echo "<td style='text-align:left;border:1px #000 solid;' class='warning' colspan='8'>".strtoupper($section)."</td>";
                                        echo "</tr>";

                                        $item = $financial_class->arrdataAllList()[$section];
                                        $grand_dr_total = $grand_cr_total = $grand_total = 0;
                                        $keys = array_keys($item); 
                                        foreach ($keys as $key) {
                                            // code...

                                            $account_id = $key;
                                            $account_name = $this->company_financial_model->get_account_name($key);
                                            

                                            echo "<tr>";
                                            echo "<td style='text-align:left;border:1px #000 solid;' class='primary' colspan='8'>$account_name</td>";
                                            echo "</tr>";
                                            $dr_total = $cr_total = $total = 0;
                                            $item = $financial_class->arrdataAllList()[$section][$account_id];
                                            foreach($item as $key=> $data)
                                            {
                                                $counter++;
                                                //$total += $data["amount"];
                                                $debcred = $data["debcred"];

                                                $link = $data["link"];
                                                $referenceId = $data["referenceId"];
                                                $link_debit = '';
                                                if($debcred == "debit")
                                                {

                                                    if(!empty($link))
                                                        $link_debit = "".number_format($data["amount"],2)."";
                                                    else
                                                        $link_debit = number_format($data["amount"],2);
                                                }

                                                $link_credit = '';
                                                if($debcred == "credit")
                                                {

                                                    if(!empty($link))
                                                        $link_credit = "".number_format($data["amount"],2)."";
                                                    else
                                                        $link_credit = number_format($data["amount"],2);
                                                }




                                                                            
                                                // if($new_day)
                                                //  $day = new DateTime($date);
                                                
                                                echo "<tr>";
                                                echo "<td style='text-align:center;border:1px #000 solid;'>$counter</td>";
                                                echo "<td style='border:1px #000 solid;'>".date('jS M Y',strtotime($data["date"]))."</td>";
                                                echo "<td style='border:1px #000 solid;'>".$data["cat"]."</td>";
                                                echo "<td style='border:1px #000 solid;'>".$key."</td>";
                                                echo "<td style='border:1px #000 solid;'>".$data["desc"]."</td>";

                                                if($debcred == "debit"){
                                                    $dr_total = $grand_dr_total += $data["amount"];
                                                    $grand_total = $total += $data["amount"];
                                                    echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid;'>".$link_debit."</td>";
                                                    echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid;'></td>";
                                                }
                                                else if($debcred == "credit"){
                                                    $grand_cr_total = $cr_total += $data["amount"];
                                                    $grand_total = $total -= $data["amount"];
                                                    echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid;'></td>";
                                                    echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid;'>".$link_credit."</td>";
                                                }
                                                else{
                                                    echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid; color:red'>Error ".$data["amount"]."</td>";
                                                    echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid; color:red'>Error</td>";
                                                }
                                                echo "<td style='text-align:right;margin-right:20px;border:1px #000 solid;'>".number_format($total,2)."</td>";
                                                echo "</tr>";
                                            
                                            }

                                            echo '<tr >
                                                    <th style="z-index: 2;background: #cb6d51;color: #fff !important;text-align: left;border:1px solid #000;" colspan="5">TOTAL '.strtoupper($account_name).'</th>
                                                    
                                                    <th style="z-index: 2;background: #cb6d51;color: #fff !important;text-align: right;border:2px solid #000;">'.number_format($dr_total,2).'</th>
                                                    <th style="z-index: 2;background: #cb6d51;color: #fff !important;text-align: right;border:2px solid #000;">'.number_format($cr_total,2).'</th>
                                                    <th style="z-index: 2;background: #cb6d51;color: #fff !important;text-align: right;border:2px solid #000;">'.number_format($total,2).'</th>
                                                </tr>';





                                        }

                                        echo '<tr >
                                                    <th style="z-index: 2;background: lightblue;color: #000 !important;text-align: left;border:1px solid #000;" colspan="5">TOTAL '.strtoupper($section).'</th>
                                                   
                                                    <th style="z-index: 2;background: lightblue;color: #000 !important;text-align: right;border:2px solid #000;">'.number_format($grand_dr_total,2).'</th>
                                                    <th style="z-index: 2;background: lightblue;color: #000 !important;text-align: right;border:2px solid #000;">'.number_format($grand_cr_total,2).'</th>
                                                    <th style="z-index: 2;background: lightblue;color: #000 !important;text-align: right;border:2px solid #000;">'.number_format($grand_total,2).'</th>
                                                </tr>';
                                    // }
                            }
                            
                    
                        ?>
                        </tbody>
                    </table>
      
                </div>

            </div>
        </div>

        <div class="row" style="font-style:italic; font-size:11px;">
            <div class="col-sm-12">
                <div class="col-sm-10 pull-left">
                    <strong>Prepared by: </strong>
                </div>
                <div class="col-sm-2 pull-right">
                    <?php echo date('jS M Y H:i a'); ?>
                </div>
            </div>

        </div>
    </body>

</html>
