<input type="hidden" name="budget_year" id="budget_year" value="<?php echo $budget_year;?>">

<?php
$date_from = $this->session->userdata('date_from_general_ledger');
$explode = explode('-', $date_from);
$current_year = $explode[0];
$previous_year = $explode[0] -1;

// var_dump(expression)
$fixed_asset_result = '<tbody>';
$total_debits = 0;
$total_credits = 0;

$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

$patients = array();

if($accounts_config_rs->num_rows() > 0)
{
	foreach ($accounts_config_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$reference_name = $value->reference_name;

		$session_account[$reference_name] = $staing_account_id;

		
	}
}

$providers_liability_id = $session_account['providers_liability_id'];
$providers_wht_id = $session_account['providers_wht_id'];
$payroll_liability_id = $session_account['payroll_liability_id'];
$accounts_payable_id = $session_account['accounts_payable_id'];
$fixed_assets_id = $session_account['fixed_assets_id'];
$accounts_receivable_id = $session_account['accounts_receivable_id'];
$suppliers_wht_id = $session_account['suppliers_wht_id'];
$income_account_id = $session_account['income_account_id'];


// $all_transacted_rs = $this->ledgers_model->get_account_ledger_by_accounts(1);

$all_transacted_rs = $this->company_financial_model->get_account_transactions_ledgers(NULL,NULL,1,0,NULL,NULL, NULL,1);


$arr = json_decode(json_encode ( $all_transacted_rs->result() ) , true);

$grouped_array = array();
foreach ($all_transacted_rs->result() as $element) {
	
    $grouped_array[$element->accountId][] = $element;
}


// var_dump($grouped_array);die();
$account_report_rs = $this->company_financial_model->get_staging_accounts(1);


$arrOrphans = array();
if($account_report_rs->num_rows() > 0)
{

	foreach ($account_report_rs->result() as $key => $value) {
		// code...
		$account_id = $value->account_id;

		$has_children = $value->has_children;


		if($has_children)
		{
			// check if account has children
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($account_id);
				

			if($children_account->num_rows() > 0)
			{	

				foreach ($children_account->result() as $key_two => $value_two) {
					// code...
					$child_account_id = $value_two->account_id;
					$arrOrphans[$child_account_id] =  $child_account_id;
				}

				
			}
			else
			{
				$arrOrphans[$value->account_id] =  $value->account_id;
			}
		}
		else
		{
			$arrOrphans[$value->account_id] =  $value->account_id;
		}
		

		
	}

}

$arrBranches = array();
foreach($arrOrphans as $serial){
	$arrSerial = explode(".", $serial);
	$new_serial = "";
	foreach($arrSerial as $part){
		$new_serial .= (strlen($new_serial)>0?".":"") . $part;

		if(!in_array($new_serial, $arrBranches))
			array_push($arrBranches, $new_serial);
	}
}

// $query_one = $this->ledgers_model->get_account_ledger_by_accounts_opening_balance($arrBranches,$current_year);

$query_one = $this->company_financial_model->get_account_transactions_ledgers($arrBranches,$current_year,0,0,NULL,NULL, NULL,0);
$grouped_array_old = array();
foreach ($query_one->result() as $element_two) {
    $grouped_array_old[$element_two->accountId] = $element_two;
}

// var_dump($arrBranches);die();





// all income accounts

$account_report_rs = $this->company_financial_model->get_staging_accounts(2);

$arrOrphansIncome = array();
if($account_report_rs->num_rows() > 0)
{

	foreach ($account_report_rs->result() as $key => $value) {
		// code...
		$account_id = $value->account_id;
		$has_children = $value->has_children;


		if($has_children)
		{
			// check if account has children
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($account_id);
				

			if($children_account->num_rows() > 0)
			{	

				foreach ($children_account->result() as $key_two => $value_two) {
					// code...
					$child_account_id = $value_two->account_id;
					$arrOrphansIncome[$child_account_id] =  $child_account_id;
				}

				
			}
			else
			{
				$arrOrphansIncome[$value->account_id] =  $value->account_id;
			}

		}
		else
		{
			$arrOrphansIncome[$value->account_id] =  $value->account_id;
		}

		

		
	}

}

$arrBranchesIncome = array();
foreach($arrOrphansIncome as $serialIncome){
	$arrSerial = explode(".", $serialIncome);
	$new_serial = "";
	foreach($arrSerial as $part){
		$new_serial .= (strlen($new_serial)>0?".":"") . $part;

		if(!in_array($new_serial, $arrBranchesIncome))
			array_push($arrBranchesIncome, $new_serial);
	}
}

// var_dump($arrBranchesIncome);die();
// $query_three = $this->ledgers_model->get_account_ledger_by_accounts_opening_periods($arrBranchesIncome,$previous_year);

$query_three = $this->company_financial_model->get_account_transactions_ledgers($arrBranchesIncome,$previous_year,0,0,NULL,NULL, NULL,2);


$grouped_array_income = array();
foreach ($query_three->result() as $element_three) {
    $grouped_array_income[$element_three->accountId][] = $element_three;
}


// var_dump($arrBranchesIncome);die();



// all fixed assets accounts

$account_report_rs = $this->company_financial_model->get_staging_accounts(3);

$arrOrphansFixed = array();
if($account_report_rs->num_rows() > 0)
{

	foreach ($account_report_rs->result() as $key => $value) {
		// code...
		$account_id = $value->account_id;
		$has_children = $value->has_children;


		if($has_children)
		{

			// check if account has children
			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($account_id);
				

			if($children_account->num_rows() > 0)
			{	

				foreach ($children_account->result() as $key_two => $value_two) {
					// code...
					$child_account_id = $value_two->account_id;
					$arrOrphansFixed[$child_account_id] =  $child_account_id;
				}

				
			}
			else
			{
				$arrOrphansFixed[$value->account_id] =  $value->account_id;
			}
		}
		else
		{
			$arrOrphansFixed[$value->account_id] =  $value->account_id;
		}

		
	}

}



$arrBranchesFixed = array();
foreach($arrOrphansFixed as $serialBranches){
	$arrSerial = explode(".", $serialBranches);
	$new_serial = "";
	foreach($arrSerial as $part){
		$new_serial .= (strlen($new_serial)>0?".":"") . $part;

		if(!in_array($new_serial, $arrBranchesFixed))
			array_push($arrBranchesFixed, $new_serial);
	}
}

$query_four = $this->ledgers_model->get_transactions_fixed_assets($arrBranchesFixed,1);

$grouped_array_fixed = array();
$grouped_array_category = array();
foreach ($query_four->result() as $element_five) {
    $grouped_array_fixed[$element_five->patient_id][] = $element_five;
    // $grouped_array_category[$element_five->category_id][] = $element_five;
}


// var_dump($grouped_array_fixed);die();


// end of fixed assets



$this->db->where('bs_account_status = 1 AND bs_account_type_id > 0  AND account_staging_delete = 0');
$this->db->order_by('bs_account_position','ASC');
$accounts_staging_rs = $this->db->get('account_staging');
// var_dump($accounts_staging_rs->result());die();
if($accounts_staging_rs->num_rows() > 0)
{
	foreach ($accounts_staging_rs->result() as $key => $value) {
		// code...
		$staing_account_id = $value->account_id;
		$account_staging_name = $value->account_staging_name;
		$bs_account_type_id = $value->bs_account_type_id;
		$has_children = $value->has_children;
		$bs_account_balance_report = $value->bs_account_balance_report;

	
		if($bs_account_type_id == 1 AND $has_children == 0 AND $bs_account_balance_report == 0)
		{
			$debit = 0;
			$credit = 0;
			$first_array = count($grouped_array[$staing_account_id]);

			if($first_array > 0)
			{

				for ($i=0; $i < $first_array; $i++) { 


			  		$dr_amount = $grouped_array[$staing_account_id][$i]->dr_amount;
				    $cr_amount = $grouped_array[$staing_account_id][$i]->cr_amount;
				    $accounts_receivable = $dr_amount - $cr_amount;

				    // $credit =0;
					if($accounts_receivable > 0)
					{
						$debit = $accounts_receivable;
						$total_debits += $debit;
					}
					else
					{
						$credit = -$accounts_receivable;
						$total_credits += $credit;
					}


					
					$fixed_asset_result .='<tr>
												<td class="text-left"><strong>'.strtoupper($account_staging_name).'</strong></td>
												<td class="text-center">
													<a href="'.site_url().'account-transactions/'.$staing_account_id.'" > '.number_format($debit,2).' </a>
												</td>
												<td class="text-center">
													<a href="'.site_url().'account-transactions/'.$staing_account_id.'" >'.number_format($credit,2).'</a>
												</td>
											</tr>';

				}

				
			}

			// unset($grouped_array);

		}



		else if($bs_account_type_id == 1 AND $has_children == 0 AND $bs_account_balance_report == 1)
		{

			// get the account opening balance
		 	$second_array = count($grouped_array_old[$staing_account_id]);
			$opening_balance = 0;
			$debit = 0;
			$credit = 0;
			$accounts_receivable =  0;
			if($second_array > 0)
			{

				$debit_opening = 0;
				$credit_opening = 0;
				// for ($z=0; $z < $second_array; $z++) { 

					$opening_dr_amount = $grouped_array_old[$staing_account_id]->dr_amount;
				    $opening_cr_amount = $grouped_array_old[$staing_account_id]->cr_amount;
				    $opening_balance = $opening_dr_amount - $opening_cr_amount;

				    if($opening_balance > 0)
				    {
				    	$debit_opening = $opening_balance;
						// $total_debits += $debit_opening;
					

						$opening = number_format($debit_opening,2);
				    }
				    else
					{
						$credit_opening = -$opening_balance;
						// $total_credits += $credit_opening;
						$opening = "(".number_format($credit_opening,2).")";

					}

					// $fixed_asset_result .='<tr>
					// 							<td class="text-right"><i>Opening Balance '.strtoupper($account_name).'</i></td>
					// 							<td class="text-center" colspan="2">
					// 								<i>'.$opening.'</i>
					// 							</td>
					// 						</tr>';

				// }
			}
			// $accounts_receivable += $opening_balance;

			$accounts_receivable = $opening_balance;
			
			$first_array = count($grouped_array[$staing_account_id]);

			if($staing_account_id == 280)
			{
				// var_dump($first_array);die();
			}

			if($first_array > 0)
			{

				for ($i=0; $i < $first_array; $i++) { 


			  		$dr_amount = $grouped_array[$staing_account_id][$i]->dr_amount;
				    $cr_amount = $grouped_array[$staing_account_id][$i]->cr_amount;
				    $accounts_receivable = $dr_amount - $cr_amount;
				    $accounts_receivable += $opening_balance;
				    

					
					

				}

				
			}

			if($accounts_receivable > 0)
			{
				$debit = $accounts_receivable;
				$total_debits += $debit;
			}
			else
			{
				$credit = -$accounts_receivable;
				$total_credits += $credit;
			}

			$fixed_asset_result .='<tr>
										<td class="text-left"><strong>'.strtoupper($account_staging_name).'</strong></td>
										<td class="text-center">
											<a href="'.site_url().'account-transactions/'.$staing_account_id.'" > '.number_format($debit,2).' </a>
										</td>
										<td class="text-center">
											<a href="'.site_url().'account-transactions/'.$staing_account_id.'" >'.number_format($credit,2).'</a>
										</td>
									</tr>';

			// unset($grouped_array);

			

		}


		else if($bs_account_type_id == 2 AND $has_children == 1 AND $bs_account_balance_report == 0)
		{

			// var_dump($staing_account_id);die();

			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($staing_account_id);
			

			if($children_account->num_rows() > 0)
			{	
				$fixed_asset_result .='<tr>
											<th class="text-left" colspan="3">'.strtoupper($account_staging_name).' </th>
											
										</tr>';
				foreach ($children_account->result() as $key => $value) {
					# code...
					$account_name = $value->account_name;
					$account_id = $value->account_id;


					$first_array = count($grouped_array[$account_id]);
					$debit = 0;
					$credit = 0;
					if($first_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
						    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;


							if($total_amount > 0)
							{
								$debit = $total_amount;
								$total_debits += $debit;
							}
							else
							{
								$credit = -$total_amount;
								$total_credits += $credit;
							}







							$fixed_asset_result .='<tr>
														<td class="text-left">'.strtoupper($account_name).' </td>
														<td class="text-center">
															<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
														</td>
														<td class="text-center">
															<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
														</td>
													</tr>';


						}

					}

				}
			}

		}


		// banks

		else if($bs_account_type_id == 3 AND ($has_children == 1 OR $has_children == 0) AND $bs_account_balance_report == 1)
		{


			

			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($staing_account_id);
			
		

			if($children_account->num_rows() > 0)
			{	
				$fixed_asset_result .='<tr>
											<th class="text-left" colspan="3">'.strtoupper($account_staging_name).' </th>
											
										</tr>';
				foreach ($children_account->result() as $key => $value) {
					# code...
					$account_name = $value->account_name;
					$account_id = $value->account_id;


					// get the account opening balance
				 	$second_array = count($grouped_array_old[$account_id]);
					$opening_balance = 0;
					if($second_array > 0)
					{

						$debit_opening = 0;
						$credit_opening = 0;
						// for ($z=0; $z < $second_array; $z++) { 

							$opening_dr_amount = $grouped_array_old[$account_id]->dr_amount;
						    $opening_cr_amount = $grouped_array_old[$account_id]->cr_amount;
						    $opening_balance = $opening_dr_amount - $opening_cr_amount;

						    if($opening_balance > 0)
						    {
						    	$debit_opening = $opening_balance;
								// $total_debits += $debit_opening;
							

								$opening = number_format($debit_opening,2);
						    }
						    else
							{
								$credit_opening = -$opening_balance;
								// $total_credits += $credit_opening;
								$opening = "(".number_format($credit_opening,2).")";

							}


						// }
					}

					$first_array = count($grouped_array[$account_id]);
					$debit = 0;
					$credit = 0;
					$total_amount = 0;
					if($first_array > 0 OR $second_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
						    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;


						}

					    $total_amount += $opening_balance;

					   	// get the account opening balance



						if($total_amount > 0)
						{
							$debit = $total_amount;
							$total_debits += $debit;
						}
						else
						{
							$credit = -$total_amount;
							$total_credits += $credit;
						}

						$fixed_asset_result .='<tr>
														<td class="text-left" style="padding-left:15px !important;">'.strtoupper($account_name).'s </td>
														<td class="text-center">
															<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
														</td>
														<td class="text-center">
															<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
														</td>
													</tr>';


					}


				}
			}
			else
			{
				// var_dump($staing_account_id);die();

				$second_array = count($grouped_array_old[$staing_account_id]);
				$opening_balance = 0;
				if($second_array > 0)
				{

					$debit_opening = 0;
					$credit_opening = 0;

						$opening_dr_amount = $grouped_array_old[$staing_account_id]->dr_amount;
					    $opening_cr_amount = $grouped_array_old[$staing_account_id]->cr_amount;
					    $opening_balance = $opening_dr_amount - $opening_cr_amount;

					    if($opening_balance > 0)
					    {
					    	$debit_opening = $opening_balance;
							// $total_debits += $debit_opening;
						

							$opening = number_format($debit_opening,2);
					    }
					    else
						{
							$credit_opening = -$opening_balance;
							// $total_credits += $credit_opening;
							$opening = "(".number_format($credit_opening,2).")";

						}


				}

				$first_array = count($grouped_array[$staing_account_id]);
				$debit = 0;
				$credit = 0;
				$total_amount = 0;
				if($first_array > 0 OR $second_array > 0)
				{

					for ($i=0; $i < $first_array; $i++) { 


				  		$dr_amount = $grouped_array[$staing_account_id][$i]->dr_amount;
					    $cr_amount = $grouped_array[$staing_account_id][$i]->cr_amount;
					    $total_amount = $dr_amount - $cr_amount;

					    // $debit = $dr_amount;
					    // $credit = $cr_amount;

					    // if($account_id == 271)
					    // {
					    // 	var_dump($total_amount);
					    // }
					}

				    $total_amount += $opening_balance;

				   	// get the account opening balance



					if($total_amount > 0)
					{
						$debit = $total_amount;
						$total_debits += $debit;
					}
					else
					{
						$credit = -$total_amount;
						$total_credits += $credit;
					}

				}

				$fixed_asset_result .='<tr>
												<td class="text-left">'.strtoupper($account_staging_name).'  </td>
												<td class="text-center">
													<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
												</td>
												<td class="text-center">
													<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
												</td>
											</tr>';
			}

		}



		// all p and l accounts

		
		else if($bs_account_type_id == 3 AND ($has_children == 1 OR $has_children == 0) AND $bs_account_balance_report == 2)
		{

			

			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($staing_account_id);
			

			if($children_account->num_rows() > 0)
			{	
				$fixed_asset_result .='<tr>
											<th class="text-left" colspan="3">'.strtoupper($account_staging_name).'</th>
											
										</tr>';
				foreach ($children_account->result() as $key => $value) {
					# code...
					$account_name = $value->account_name;
					$account_id = $value->account_id;


					// get the account opening balance
				 	$second_array = count($grouped_array_income[$account_id]);
					$opening_balance = 0;
					if($second_array > 0)
					{

						$debit_opening = 0;
						$credit_opening = 0;
						for ($z=0; $z < $second_array; $z++) { 

							$opening_dr_amount = $grouped_array_income[$account_id][$z]->dr_amount;
						    $opening_cr_amount = $grouped_array_income[$account_id][$z]->cr_amount;
						    $opening_balance = $opening_dr_amount - $opening_cr_amount;

						    if($opening_balance > 0)
						    {
						    	$debit_opening = $opening_balance;
								// $total_debits += $debit_opening;
							

								$opening = number_format($debit_opening,2);
						    }
						    else
							{
								$credit_opening = -$opening_balance;
								// $total_credits += $credit_opening;
								$opening = "(".number_format($credit_opening,2).")";

							}


						}
					}

					$first_array = count($grouped_array[$account_id]);
					$debit = 0;
					$credit = 0;
					$total_amount = 0;
					if($first_array > 0 OR $second_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
						    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;

						    // $debit = $dr_amount;
						    // $credit = $cr_amount;

						    // if($account_id == 271)
						    // {
						    // 	var_dump($total_amount);
						    // }
						}

					    $total_amount += $opening_balance;

					   	// get the account opening balance



						if($total_amount > 0)
						{
							$debit = $total_amount;
							$total_debits += $debit;
						}
						else
						{
							$credit = -$total_amount;
							$total_credits += $credit;
						}

						$fixed_asset_result .='<tr>
													<td class="text-left" style="padding-left:15px !important;">'.strtoupper($account_name).'  </td>
													<td class="text-center">
														<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
													</td>
													<td class="text-center">
														<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
													</td>
												</tr>';


					}


				}
			}
			else
			{
				// var_dump($staing_account_id);die();
				// var_dump($grouped_array_old[144]);die();
				// get the account opening balance
				 	$second_array = count($grouped_array_income[$staing_account_id]);
					$opening_balance = 0;
					if($second_array > 0)
					{

						$debit_opening = 0;
						$credit_opening = 0;
						for ($z=0; $z < $second_array; $z++) { 

							$opening_dr_amount = $grouped_array_income[$staing_account_id][$z]->dr_amount;
						    $opening_cr_amount = $grouped_array_income[$staing_account_id][$z]->cr_amount;
						    $opening_balance = $opening_dr_amount - $opening_cr_amount;

						    if($opening_balance > 0)
						    {
						    	$debit_opening = $opening_balance;
								// $total_debits += $debit_opening;
							

								$opening = number_format($debit_opening,2);
						    }
						    else
							{
								$credit_opening = -$opening_balance;
								// $total_credits += $credit_opening;
								$opening = "(".number_format($credit_opening,2).")";

							}


						}
					}

					$first_array = count($grouped_array[$staing_account_id]);
					$debit = 0;
					$credit = 0;
					$total_amount = 0;
					if($first_array > 0 OR $second_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$staing_account_id][$i]->dr_amount;
						    $cr_amount = $grouped_array[$staing_account_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;

						    // $debit = $dr_amount;
						    // $credit = $cr_amount;

						    // if($account_id == 271)
						    // {
						    // 	var_dump($total_amount);
						    // }
						}

					    $total_amount += $opening_balance;

					   	// get the account opening balance



						if($total_amount > 0)
						{
							$debit = $total_amount;
							$total_debits += $debit;
						}
						else
						{
							$credit = -$total_amount;
							$total_credits += $credit;
						}

						$fixed_asset_result .='<tr>
													<td class="text-left">'.strtoupper($account_staging_name).'  </td>
													<td class="text-center">
														<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
													</td>
													<td class="text-center">
														<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
													</td>
												</tr>';


					}
			}

		}

		else if($bs_account_type_id == 5 AND $has_children == 0 AND $bs_account_balance_report == 0)
		{
			// var_dump($previous_year);die();
			$profit = $this->company_financial_model->get_retained_earning($previous_year,1);
			$debit = 0;
			$credit = 0;

			// var_dump($profit);die();
			if($profit < 0)
			{
				$debit = -$profit;
				$total_debits += $debit;
			}
			else
			{
				$credit = $profit;
				$total_credits += $credit;
			}

			$fixed_asset_result .='<tr>
										<td class="text-left">'.strtoupper($account_staging_name).' </td>
										<td class="text-center">
											<a href="">'.number_format($debit,2).'</a>
										</td>
										<td class="text-center">
											<a href="">'.number_format($credit,2).'</a>
										</td>
									</tr>';
		}
		// type == 3

		// loans

		// banks

		else if($bs_account_type_id == 4 AND $has_children == 1 AND $bs_account_balance_report == 1)
		{

			

			$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($staing_account_id);
			

			if($children_account->num_rows() > 0)
			{	
				$fixed_asset_result .='<tr>
											<th class="text-left" colspan="3">'.strtoupper($account_staging_name).' </th>
											
										</tr>';
				foreach ($children_account->result() as $key => $value) {
					# code...
					$account_name = $value->account_name;
					$account_id = $value->account_id;


					// get the account opening balance
				 	$second_array = count($grouped_array_old[$account_id]);
					$opening_balance = 0;
					if($second_array > 0)
					{

						$debit_opening = 0;
						$credit_opening = 0;
						// for ($z=0; $z < $second_array; $z++) { 

							$opening_dr_amount = $grouped_array_old[$account_id]->dr_amount;
						    $opening_cr_amount = $grouped_array_old[$account_id]->cr_amount;
						    $opening_balance = $opening_dr_amount - $opening_cr_amount;

						    if($opening_balance > 0)
						    {
						    	$debit_opening = $opening_balance;
								// $total_debits += $debit_opening;
							

								$opening = number_format($debit_opening,2);
						    }
						    else
							{
								$credit_opening = -$opening_balance;
								// $total_credits += $credit_opening;
								$opening = "(".number_format($credit_opening,2).")";

							}

					}

					$first_array = count($grouped_array[$account_id]);
					$debit = 0;
					$credit = 0;
					$total_amount = 0;
					if($first_array > 0 OR $second_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array[$account_id][$i]->dr_amount;
						    $cr_amount = $grouped_array[$account_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;


						}

					    $total_amount += $opening_balance;

					   	// get the account opening balance



						if($total_amount > 0)
						{
							$debit = $total_amount;
							$total_debits += $debit;
						}
						else
						{
							$credit = -$total_amount;
							$total_credits += $credit;
						}

						$fixed_asset_result .='<tr>
														<td class="text-left">'.strtoupper($account_name).' </td>
														<td class="text-center">
															<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
														</td>
														<td class="text-center">
															<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
														</td>
													</tr>';


					}


				}
			}

		}

		else if($bs_account_type_id == 6 AND $has_children == 1 AND $bs_account_balance_report == 3)
		{


			

			$children_account = $this->company_financial_model->get_assets_child_accounts_by_parent_id($staing_account_id);
			
			

			if($children_account->num_rows() > 0)
			{	
				$fixed_asset_result .='<tr>
											<th class="text-left" colspan="3">'.strtoupper($account_staging_name).' </th>
											
										</tr>';
				foreach ($children_account->result() as $key => $value) {
					# code...
					$child_account_name = $value->account_name;
					$child_account_id = $value->account_id;
					$asset_name = $value->asset_name;
					$asset_value = $value->asset_value;
					$asset_id = $value->asset_id;
					$child_account_name .= ' : '.$asset_name;


					// get the account opening balance
				 	$second_array = count($grouped_array_old[$account_id]);
					$opening_balance = 0;
					if($second_array > 0)
					{

						$debit_opening = 0;
						$credit_opening = 0;
						// for ($z=0; $z < $second_array; $z++) { 

							$opening_dr_amount = $grouped_array_old[$account_id]->dr_amount;
						    $opening_cr_amount = $grouped_array_old[$account_id]->cr_amount;
						    $opening_balance = $opening_dr_amount - $opening_cr_amount;

						    if($opening_balance > 0)
						    {
						    	$debit_opening = $opening_balance;
								// $total_debits += $debit_opening;
							

								$opening = number_format($debit_opening,2);
						    }
						    else
							{
								$credit_opening = -$opening_balance;
								// $total_credits += $credit_opening;
								$opening = "(".number_format($credit_opening,2).")";

							}


						// }
					}
					$account_amount += $opening_balance;

					$first_array = count($grouped_array_fixed[$asset_id]);
					$debit = 0;
					$credit = 0;
					$total_amount = 0;
					// $total_amount += $opening_balance;
					if($first_array > 0 OR $second_array > 0)
					{

						for ($i=0; $i < $first_array; $i++) { 


					  		$dr_amount = $grouped_array_fixed[$asset_id][$i]->dr_amount;
						    $cr_amount = $grouped_array_fixed[$asset_id][$i]->cr_amount;
						    $total_amount = $dr_amount - $cr_amount;


						}

						if($total_amount > 0)
						{
							$debit = $total_amount;
							$total_debits += $debit;
						}
						else
						{
							$credit = -$total_amount;
							$total_credits += $credit;
						}

					}

					$fixed_asset_result .='<tr>
												<td class="text-left" style="padding-left:15px !important;">'.strtoupper($child_account_name).' </td>
												<td class="text-center">
													<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($debit,2).'</a>
												</td>
												<td class="text-center">
													<a href="'.site_url().'account-transactions/'.$account_id.'">'.number_format($credit,2).'</a>
												</td>
											</tr>';
				}
			}

		}



		
	}
}

$fixed_asset_result .='
						</tbody>
						<tfoot>
						<tr>
								<th class="text-left">SUBTOTAL</th>
								<th class="text-center">
								'.number_format($total_debits,2).'
								</th>
								<th class="text-center">
								'.number_format($total_credits,2).'
								</th>
							</tr>
							<tr>
								<th class="text-left">SUBTOTAL</th>
								<th class="text-center" colspan="2">
								'.number_format($total_debits-$total_credits,2).'
								</th>
							</tr>';
$fixed_asset_result .='
						</tfoot>';


$date_from = $this->session->userdata('date_from_general_ledger');
$date_to = $this->session->userdata('date_to_general_ledger');
$add = '';
if(!empty($date_from) OR !empty($date_to))
{

	$date_from = $date_from;
	$date_to = $date_to;

}

else
{

	$date_from = date('Y-m-01');
	$date_to = date('Y-m-d');
	

}

?>
<div class="row">
	
		<div class="col-md-4">
			<section class="panel">
				<header class="panel-heading">
						<h5 class="pull-left"><i class="icon-reorder"></i>Search </h5>
						<div class="clearfix"></div>
				</header>
				<!-- /.box-header -->
				<div class="panel-body">
					<?php echo form_open("financials/budget/search_trial_balance", array("class" => "form-horizontal"));?>
		      		<div class="col-md-12">
		      			<div class="form-group">
							<label class="col-md-4 control-label">Date From: </label>

							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
									</span>
									<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date From" value="<?php echo $date_from?>" autocomplete="off" required>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Date To: </label>

							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
									</span>
									<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder=" Date to" value="<?php echo $date_to?>" autocomplete="off" required>
								</div>
							</div>
						</div>
						<div class="form-group">
			                <div class="col-lg-8 col-lg-offset-2">
			                	<div class="center-align">
			                   		<button type="submit" class="btn btn-info">Search</button>
			    				</div>
			                </div>
			            </div>
		      		</div>
		      		<?php echo form_close();?>
		      	</div>
		    </section>




			<div class="text-center">
				<h4 class="box-title">Trial Balance</h4>
				<h5 class="box-title"> <?php echo $this->session->userdata('general_ledger_search_title');?> </h5>
				<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
			</div>
			<?php 
			$search_title = $this->session->userdata('general_ledger_search_title');
			$search_title = 'TRIAL BALANCE '.$search_title;
			?>

			<div class="col-md-12">

				<div class="col-md-12" style="margin-bottom:20px">
					<div class="col-md-6">
						<div class="form-group">
							  <a onclick="javascript:xport.toCSV('<?php echo $search_title?>');" target="_blank" class="btn btn-md btn-success col-md-12"><i class="fa fa-print"></i> EXPORT TRIAL BALANCE</a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<a href="<?php echo site_url().'print-trial-balance'?>" target="_blank" class="btn btn-md btn-warning col-md-12"><i class="fa fa-print"></i> Print Trail Balance</a>
						</div>
					</div>
					
				</div>
				<br>
				<div class="col-md-12">
					
				    <div class="form-group">
					      <a href="<?php echo site_url().'company-financials/profit-and-loss'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Income Statement</a>
					</div>
					<div class="form-group">
					      <a href="<?php echo site_url().'company-financials/balance-sheet'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Balance Sheet</a>
					</div>
					<div class="form-group">
					      <a href="<?php echo site_url().'company-financials/general-ledger'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> General Ledger</a>
					</div>
			  	</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="panel-body" style="height:85vh;overflow-y: scroll;padding:0px;">
				<table class="table  table-striped table-condensed table-linked" id="<?php echo $search_title?>">

		    		<thead>
						<tr>
		        			<th class="text-left">ACCOUNT NAME</th>
							<th class="text-center">DEBIT</th>
							<th class="text-center">CREDIT</th>
						</tr>
					</thead>
					
						<?php
						echo $fixed_asset_result;

						?>
					
				</table>
			</div>
		</div>
		
		
	
	
</div>
<script type="text/javascript">
	$(function() {
		// var budget_year = <?php echo $budget_year?>;
			var budget_year = document.getElementById("budget_year").value;

		// alert(budget_year);
		get_year_budget(budget_year);
		
	});

	function get_year_budget(budget_year)
	{
		var config_url = $('#config_url').val();
	 	var url = config_url+"financials/budget/get_year_budget_summary/"+budget_year;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#budget-table").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}
			$("#title-div").html('REPORT FOR '+budget_year);

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
	}
	function add_budget_item(budget_year)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"financials/budget/add_budget_item/"+budget_year;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});



			$('.timepicker').timepicker({
			    timeFormat: 'h:mm p',
			    interval: 60,
			    minTime: '10',
			    maxTime: '6:00pm',
			    defaultTime: '11',
			    startTime: '10:00',
			    dynamic: false,
			    dropdown: true,
			    scrollbar: true
			});
			// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
	}
	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}

	$(document).on("submit","form#confirm-budget-item",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		var budget_year = $('#budget_year').val();	
		var budget_month = $('#budget_month').val();	
		var account_id = $('#account_id').val();	
		var config_url = $('#config_url').val();

		var url = config_url+"financials/budget/confirm_budget_item/"+budget_year;
		 
		 // alert(url);
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.status == "success")
			{
				
				
				// close_side_bar();
				get_budget_items(budget_year,budget_month,account_id);
				get_year_budget(budget_year);


				
			}
			else
			{
				alert(data.message);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
		 
		
	   
		
	});
	function delete_budget_item(budget_item_id,budget_year,month,account_id)
	{

		var res = confirm('Are you sure you want to delete this entry ?');


		if(res)
		{
			var config_url = $('#config_url').val();
			var data_url = config_url+"financials/budget/delete_budget_item/"+budget_item_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){
				

				 get_budget_items(budget_year,month,account_id);
				 get_year_budget(budget_year);
				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});
		}
		

	}

	function edit_budget_item(month,account_id,budget_year)
	{
		close_side_bar();
		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"financials/budget/add_budget_item/"+budget_year+"/"+month+"/"+account_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			 get_budget_items(budget_year,month,account_id);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});

	}

	function get_budget_items(budget_year,month,account_id)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"financials/budget/get_budget_list/"+budget_year+"/"+month+"/"+account_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#visit-payment-div").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}
			// $("#title-div").html('REPORT FOR '+budget_year);

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
	}

		var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>