<input type="hidden" name="budget_year" id="budget_year" value="<?php echo $budget_year;?>">

<?php
error_reporting(0);
// echo "<br>I'm at: ". getcwd();
include_once "application/modules/financials/controllers/financial_report.php";

// include_once ""
$date_to = $this->session->userdata('date_to_general_ledger');

if($opening_tb_status)
{
	$opening_tb_status = TRUE;
	$title = 'Opening TB';
	$link = 'print-opening-tb';
	$financial_class = new Financial_Report(array('opening_tb'=>$opening_tb_status,'trial_balance'=>false,
			"date" => $this->session->userdata('date_to_general_ledger')));
}
else
{
	$opening_tb_status = FALSE;
		$title = 'Trial Balance';
			$link = 'print-trial-balance';
		$financial_class = new Financial_Report(array('opening_tb'=>$opening_tb_status,'trial_balance'=>true,
			"date" => $this->session->userdata('date_to_general_ledger')));
}

// echo "sajshakhsa";
// $financial_class->printData();

// echo "<br>Printing final";
// echo "<pre>";
// print_r($financial_class->printData());
// echo "</pre>";


// $arrDone = array("Creditor Invoices","Petty Cash","Transfer","Payroll Invoice","Creditor Invoices Payments","Creditor Payment","Receivable Invoice","Receivable Payment");

// $arrDone = array();



// echo "<br>Printing final";
// echo "<pre>";
// $all_transacted_rs = $this->company_financial_model->get_account_transactions_ledgers_new(NULL,NULL,0,0,NULL,NULL, NULL,NULL);

// foreach($all_transacted_rs->result() as $item){
// 	if(!in_array($item->transactionCategory,$arrDone))
// 	{
// 		var_dump($item);
// 	}

// }
// echo "</pre>";

?>


<div class="col-md-12">

		<div class="col-md-4">
			<section class="panel">
				<header class="panel-heading">
						<h5 class="pull-left"><i class="icon-reorder"></i>Search </h5>
						<div class="clearfix"></div>
				</header>
				<!-- /.box-header -->
				<div class="panel-body">
					<?php echo form_open("financials/budget/search_trial_balance", array("class" => "form-horizontal"));?>
		      		<div class="col-md-12">
		      			<div class="form-group" style="display: none;">
							<label class="col-md-4 control-label">Date From: </label>

							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
									</span>
									<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date From" value="<?php echo $date_from?>" autocomplete="off" >
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Date To: </label>

							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
									</span>
									<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder=" Date to" value="<?php echo $date_to?>" autocomplete="off" required>
								</div>
							</div>
						</div>
						<div class="form-group">
			                <div class="col-lg-8 col-lg-offset-2">
			                	<div class="center-align">
			                   		<button type="submit" class="btn btn-info">Search</button>
			    				</div>
			                </div>
			            </div>
		      		</div>
		      		<?php echo form_close();?>
		      	</div>
		    </section>




			<div class="text-center">
				<h4 class="box-title"><?php echo $title?></h4>
				<h5 class="box-title"> <?php echo $this->session->userdata('general_ledger_search_title');?> </h5>
				<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
			</div>
			<?php
			$search_title = $this->session->userdata('general_ledger_search_title');
			$search_title =  $title.' '.$search_title;
			?>

			<div class="col-md-12">

				<div class="col-md-12" style="margin-bottom:20px">

					<div class="col-md-12">
						<div class="form-group">
							<a href="<?php echo site_url().$link?>" target="_blank" class="btn btn-md btn-warning col-md-12"><i class="fa fa-print"></i> Print <?php echo $title?></a>
						</div>
					</div>

				</div>
				<br>
				<div class="col-md-12">
					<?php
					if($opening_tb_status)
					{
						?>
						<div class="form-group">
						      <a href="<?php echo site_url().'company-financials/trial-balance'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> Trial Balance</a>
						</div>
						<?php
					}
					else
					{
						?>
						<div class="form-group">
						      <a href="<?php echo site_url().'company-financials/opening-tb'?>"  class="btn btn-md btn-success col-md-12"><i class="fa fa-file"></i> Opening TB</a>
						</div>
						<?php
					}
					?>


				  <div class="form-group">
					      <a href="<?php echo site_url().'company-financials/profit-and-loss'?>"  class="btn btn-md btn-info col-md-12"><i class="fa fa-file"></i> Income Statement</a>
					</div>
					<div class="form-group">
					      <a href="<?php echo site_url().'company-financials/balance-sheet'?>"  class="btn btn-md btn-primary col-md-12"><i class="fa fa-file"></i> Balance Sheet</a>
					</div>
					<div class="form-group">
					      <a href="<?php echo site_url().'company-financials/general-ledger'?>"  class="btn btn-md btn-warning col-md-12"><i class="fa fa-file"></i> General Ledger</a>
					</div>

			  	</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="panel-body" style="height:85vh;overflow-y: scroll;padding:0px;">
				<table class="table  table-striped table-condensed table-linked" id="<?php echo $search_title?>">

		    		<thead>
						<tr>
		        			<th class="text-left">ACCOUNT NAME</th>
							<th style="text-align: right !important;">DEBIT</th>
							<th style="text-align: right !important;">CREDIT</th>
						</tr>
					</thead>
						<tbody>
						<?php
						// $array_data = $financial_class->arrData();

						$total_debits = 0;
						$total_credits = 0;


						foreach($financial_class->arrData() as $section => $data)
						{
							// code...

								echo "<tr>
											<th colspan='3'>".ucfirst($section)."</th>
										</tr>";
								foreach ($data as $key => $value) {
									// code...
									$name = $value['name'];
									$account_id = $key;
									$amount = $value['amount'];

									$amount = round($amount,2);
									// echo "<br>";
									// print_r($value['name']);
									// echo "<br>";
									// echo "<br> $section - $name;";

									if($amount != 0)
									{


										if($section == "expenses" OR $section == "assets")
										{
											echo "<tr >
														<td style='padding-left:15px !important;'>".ucwords(strtolower($name))."</td>
														<td style='text-align: right !important;'><a onclick='get_account_items(".$account_id.",\"".$section."\")'>".number_format($amount,2)."</a></td>
														<td style='text-align: right !important;'></td>
													</tr>";

											$total_debits += $amount;
										}
										else if($section == "equities" OR $section == "incomes" OR $section == "liabilities")
										{

											echo "<tr>
														<td style='padding-left:15px !important;'>".ucwords(strtolower($name))."</td>
														<td style='text-align: right !important;'></td>
														<td style='text-align: right !important;'><a onclick='get_account_items(".$account_id.",\"".$section."\")'>".number_format($amount,2)."</a></td>
													</tr>";
											$total_credits += $amount;
										}
									}
								}
						}

						echo "</tbody>
								<tfoot>
								<tr>
									<th>TOTALS</th>
									<th style='text-align: right !important;'>".number_format($total_debits,2)."</th>
									<th style='text-align: right !important;'>".number_format($total_credits,2)."</th>
								</tr>
								</tfoot>";

						$bal = $total_debits-$total_credits;





						?>

				</table>
			</div>
		</div>




</div>
<script type="text/javascript">
	$(function() {
		// var budget_year = <?php echo $budget_year?>;
			var budget_year = document.getElementById("budget_year").value;

		// alert(budget_year);
		get_year_budget(budget_year);

	});

	function get_year_budget(budget_year)
	{
		var config_url = $('#config_url').val();
	 	var url = config_url+"financials/budget/get_year_budget_summary/"+budget_year;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#budget-table").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}
			$("#title-div").html('REPORT FOR '+budget_year);

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
	}
	function add_budget_item(budget_year)
	{

		document.getElementById("sidebar-right").style.display = "block";
		document.getElementById("existing-sidebar-div").style.display = "none";

		var config_url = $('#config_url').val();
		var data_url = config_url+"financials/budget/add_budget_item/"+budget_year;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block";
			$("#current-sidebar-div").html(data);

			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});



			$('.timepicker').timepicker({
			    timeFormat: 'h:mm p',
			    interval: 60,
			    minTime: '10',
			    maxTime: '6:00pm',
			    defaultTime: '11',
			    startTime: '10:00',
			    dynamic: false,
			    dropdown: true,
			    scrollbar: true
			});
			// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
	}
	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none";
		document.getElementById("current-sidebar-div").style.display = "none";
		document.getElementById("existing-sidebar-div").style.display = "none";
		tinymce.remove();
	}

	$(document).on("submit","form#confirm-budget-item",function(e)
	{
		e.preventDefault();

		var form_data = new FormData(this);

		// alert(form_data);
		var budget_year = $('#budget_year').val();
		var budget_month = $('#budget_month').val();
		var account_id = $('#account_id').val();
		var config_url = $('#config_url').val();

		var url = config_url+"financials/budget/confirm_budget_item/"+budget_year;

		 // alert(url);
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
	   success:function(data){
	      var data = jQuery.parseJSON(data);

	      	if(data.status == "success")
			{


				// close_side_bar();
				get_budget_items(budget_year,budget_month,account_id);
				get_year_budget(budget_year);



			}
			else
			{
				alert(data.message);
			}

	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

	   }
	   });




	});
	function delete_budget_item(budget_item_id,budget_year,month,account_id)
	{

		var res = confirm('Are you sure you want to delete this entry ?');


		if(res)
		{
			var config_url = $('#config_url').val();
			var data_url = config_url+"financials/budget/delete_budget_item/"+budget_item_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){


				 get_budget_items(budget_year,month,account_id);
				 get_year_budget(budget_year);
				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});
		}


	}

	function edit_budget_item(month,account_id,budget_year)
	{
		close_side_bar();
		document.getElementById("sidebar-right").style.display = "block";
		document.getElementById("existing-sidebar-div").style.display = "none";

		var config_url = $('#config_url').val();
		var data_url = config_url+"financials/budget/add_budget_item/"+budget_year+"/"+month+"/"+account_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block";
			$("#current-sidebar-div").html(data);

			 get_budget_items(budget_year,month,account_id);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});

	}

	function get_budget_items(budget_year,month,account_id)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"financials/budget/get_budget_list/"+budget_year+"/"+month+"/"+account_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#visit-payment-div").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}
			// $("#title-div").html('REPORT FOR '+budget_year);

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
	}

		var xport = {
  _fallbacktoCSV: true,
  toXLS: function(tableId, filename) {
    this._filename = (typeof filename == 'undefined') ? tableId : filename;

    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }

    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it

      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

function get_account_items(account_id,transaction)
	{
		// alert(transaction);
			document.getElementById("sidebar-right").style.display = "block";
			var config_url = $('#config_url').val();
			var data_url = config_url+"financials/company_financial/transactions_list/"+account_id;

			$.ajax({
			type:'POST',
			url: data_url,
			data:{appointment_id: 1,transaction:transaction},
			dataType: 'text',
			success:function(data){

					document.getElementById("current-sidebar-div").style.display = "block";
					$("#current-sidebar-div").html(data);

				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});

	}

</script>
