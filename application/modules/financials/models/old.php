<?php

class Company_financial_model extends CI_Model
{

	public function get_all_service_types()
	{
		$this->db->select('*');
		$this->db->where('service_delete = 0 AND service.service_status = 1');
		$query = $this->db->get('service');

		return $query;
	}

	public function get_service_invoice_total($service_id, $date = NULL)
	{
		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_payments_add =  ' AND (payment_created >= \''.$date_from.'\' AND payment_created <= \''.$date_to.'\') ';
				$search_invoice_add =  ' AND (visit_charge.date >= \''.$date_from.'\' AND visit_charge.date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_payments_add = ' AND payments.payment_created = \''.$date_from.'\'';
				$search_invoice_add = ' AND visit_charge.date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_payments_add = ' AND payments.payment_created = \''.$date_to.'\'';
				$search_invoice_add = ' AND visit_charge.date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';
			$search_payments_add = '';

		}


		$table = 'visit_charge, service_charge,service';

		$where = 'visit_charge.visit_charge_delete = 0 AND visit_charge.service_charge_id = service_charge.service_charge_id AND service.service_id = service_charge.service_id AND service.service_status = 1 AND service_charge.service_id = '.$service_id;


		$where .= $search_invoice_add;

		$this->db->select('SUM(visit_charge_units*visit_charge_amount) AS service_total');
		$this->db->where($where);
		$query = $this->db->get($table);

		$result = $query->row();
		$total = $result->service_total;;

		if($total == NULL)
		{
			$total = 0;
		}

		return $total;
	}

	public function get_service_payments_total($service_id, $date = NULL)
	{
		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_payments_add =  ' AND (payment_created >= \''.$date_from.'\' AND payment_created <= \''.$date_to.'\') ';
				$search_invoice_add =  ' AND (visit_charge.date >= \''.$date_from.'\' AND visit_charge.date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_payments_add = ' AND payments.payment_created = \''.$date_from.'\'';
				$search_invoice_add = ' AND visit_charge.date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_payments_add = ' AND payments.payment_created = \''.$date_to.'\'';
				$search_invoice_add = ' AND visit_charge.date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';
			$search_payments_add = '';

		}

		$table = 'payments';


		$where = 'cancel = 0 AND payment_service_id = '.$service_id;

		$where .= $search_payments_add;
		$this->db->select('SUM(amount_paid) AS paid_amount');
		$this->db->where($where);
		$query = $this->db->get($table);

		$result = $query->row();
		$total = $result->paid_amount;;

		if($total == NULL)
		{
			$total = 0;
		}

		return $total;
	}

	public function get_total_purchases()
	{
		if($date == NULL)
		{
			$date = date('Y-m-d');
		}

		$table = 'order_supplier';


		$where = 'invoice_number <> ""';


		$this->db->select('selling_unit_price,pack_size,quantity_received');
		$this->db->where($where);
		$query = $this->db->get($table);

		$result = $query->row();
		$total_value = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$selling_unit_price = $value->selling_unit_price;
				$pack_size = $value->pack_size;
				$quantity_received = $value->quantity_received;

				$total_units = $pack_size * $quantity_received;

				$total_value += $total_units * $pack_size;
			}
		}

		return $total_value;
	}

	public function get_income_value_new($transactionCategory)
	{
		//retrieve all users

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  '  AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = '  AND transaction_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$branch_id = $this->session->userdata('branch_id');

		$search_status = $this->session->userdata('trail_budget_year');


		if($search_status)
		{
			$search_invoice_add = ' AND YEAR(transaction_date) = \''.$search_status.'\'';
		}
		else
		{
			$search_invoice_add = ' AND YEAR(transaction_date) = \''.date('Y').'\'';
		}

		$select_statement = "
							SELECT
								data.service_name AS service_name,
								data.parent_service AS department_id,
								account.account_name AS account_name ,
								SUM(dr_amount) AS dr_amount,
								SUM(cr_amount) AS cr_amount
								FROM (SELECT
									visit_invoice.visit_invoice_id AS transaction_id,
									visit.visit_id AS reference_id,
									visit.invoice_number AS reference_code,
									visit.patient_id AS patient_id,
									service.service_id AS parent_service,
									service.department_id AS department_id,
									visit_charge.service_charge_id AS child_service,
									visit.personnel_id AS personnel_id,
									visit.visit_type AS payment_type,
									service.account_id AS account_id,
									'' AS payment_method_name,
									service.service_name AS service_name,
									departments.department_name AS department_name,
									service_charge.service_charge_name AS transaction_name,
									CONCAT( 'Charged for ', service.service_name, ' : ', service_charge.service_charge_name ) AS transaction_description,
									(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS dr_amount,
									'0' AS cr_amount,
									visit_invoice.created AS transaction_date,
									visit_charge.visit_charge_timestamp AS created_at,
									visit_charge.charged AS `status`,
									'Patient' AS party,
									'Revenue' AS transactionCategory,
									'Invoice Patients' AS transactionClassification,
									'visit_charge' AS transactionTable,
									'visit' AS referenceTable
									FROM
										visit_charge,visit,service_charge,service,departments,visit_invoice

									WHERE
										visit.visit_delete = 0
										AND visit_charge.charged = 1
										AND visit_charge.visit_charge_delete = 0
										AND visit.visit_id = visit_charge.visit_id
										AND service_charge.service_charge_id = visit_charge.service_charge_id
										AND service.service_id = service_charge.service_id
										AND departments.department_id = service.department_id
										AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
										AND visit_invoice.visit_invoice_delete = 0
										AND visit.branch_id = $branch_id

									UNION ALL

									SELECT
										payments.payment_id AS transaction_id,
										visit.visit_id AS reference_id,
										visit_invoice.visit_invoice_number AS reference_code,
										visit.patient_id AS patient_id,
										'' AS parent_service,
										'' AS department_id,
										'' AS child_service,
										'' AS personnel_id,
										visit.visit_type AS payment_type,
										'' AS account_id,
										'' AS payment_method_name,
										'' AS service_name,
										'' AS department_name,
										CONCAT( 'Credit Note', ' : ', visit.visit_id) AS transaction_name,
										CONCAT( 'Credit Note for ', visit_invoice.visit_invoice_number ) AS transaction_description,
										'0' AS dr_amount,
										payment_item.payment_item_amount AS cr_amount,
										payments.payment_created AS transaction_date,
										DATE(payments.time) AS created_at,
										payments.payment_status AS `status`,
										'Patient' AS party,
										'Credit Notes' AS transactionCategory,
										'Credit Note Patients' AS transactionClassification,
										'payments' AS transactionTable,
										'visit' AS referenceTable
									FROM
										payments,payment_item,visit_invoice
									LEFT JOIN visit ON visit.visit_id = visit_invoice.visit_id AND visit.branch_id = $branch_id
									LEFT JOIN visit_type ON visit.visit_type = visit_type.visit_type_id
										WHERE payments.cancel = 0
										AND payments.payment_type = 1
										AND payments.payment_id = payment_item.payment_id
										AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id




									) AS data,account WHERE department_id > 0  ".$search_invoice_add." AND account.account_id = 134  GROUP BY data.department_id";

		$query = $this->db->query($select_statement);

		$result = $query->row();

		$product_value  = array();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$item['name'] = $value->service_name;
				$item['department_id'] = $value->department_id;
				$item['account_name'] = $value->account_name;
				$item['value'] = $value->dr_amount - $value->cr_amount;

				array_push($product_value, $item);
			}
		}

		// var_dump($product_value);
		return  $product_value;
	}

	public function get_income_value($transactionCategory)
	{
		//retrieve all users

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$this->db->from('v_all_invoice_payments,service');
		$this->db->select('SUM(cr_amount) AS total_amount,service.service_name AS parent_service,service.service_id');
		$this->db->where('service.service_id = v_all_invoice_payments.parent_service  AND  transactionCategory = "'.$transactionCategory.'" '.$search_invoice_add);
		$this->db->group_by('parent_service');
		$query = $this->db->get();

		return $query;
	}

	public function get_operational_cost_value($transactionCategory)
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{

				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}


		$search_status = $this->session->userdata('trail_budget_year');


		if($search_status)
		{
			$search_invoice_add = ' AND YEAR(transactionDate) = \''.$search_status.'\'';
		}
		else
		{
			$search_invoice_add = ' AND YEAR(transactionDate) = \''.date('Y').'\'';
		}
		//retrieve all users
		$this->db->from('v_expenses_ledger');
		$this->db->select('SUM(dr_amount) AS total_amount,accountName,accountId');
		$this->db->where('accountId > 0 AND accountsclassfication = "'.$transactionCategory.'"');
		// $this->db->where('accountId > 0');
		$this->db->group_by('v_expenses_ledger.accountId');
		$query = $this->db->get();

		return $query;
	}

	public function get_operational_cost_value_by_classification($accountsclassification)
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{

				$search_invoice_add =  ' AND (referenceDate >= \''.$date_from.'\' AND referenceDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND referenceDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND referenceDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('SUM(dr_amount) AS total_amount,accountName,accountId');
		$this->db->where('accountId > 0 AND accountsclassfication = "'.$accountsclassification.'" '.$search_invoice_add);
		$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}


	public function get_cog_value($transactionCategory)
	{
		//retrieve all users
		$this->db->from('v_all_invoice_payments');
		$this->db->select('SUM(dr_amount) AS total_amount,accountName');
		$this->db->where('transactionCategory = "'.$transactionCategory.'" AND (patient_id IS NULL OR patient_id = 0)');
		$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}


	public function get_payables_aging_report()
	{
		//retrieve all users
		$creditor_search =  $this->session->userdata('creditor_search');

		if(!empty($creditor_search))
		{
			$where = $creditor_search;
		}
		else
		{
			$where = 'recepientId > 0';
		}
		$this->db->from('v_aged_payables');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('payables','ASC');
		$query = $this->db->get();

		return $query;
	}


		public function get_payables_aging_report_by_creditor($creditor_id)
		{
			//retrieve all users
			$this->db->from('v_aged_payables');
			$this->db->select('*');
			$this->db->where('recepientId ='.$creditor_id);
			// $this->db->group_by('accountId');
			$query = $this->db->get();

			return $query;
		}

	public function get_receivables_aging_report()
	{
		//retrieve all users
		$this->db->from('v_aged_receivables');
		$this->db->select('*');
		// $this->db->where('');
		// $this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}



	public function get_account_value()
	{

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users

		$this->db->from('v_account_ledger_by_date,account');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount,accountName,account_id');
		$this->db->where('accountParentId = 2 AND v_account_ledger_by_date.accountId = account.account_id AND account.account_status = 1 AND account.paying_account = 0  '.$search_invoice_add);
		$this->db->group_by('v_account_ledger_by_date.accountId');
		$query = $this->db->get();

		return $query;

	}


	public function get_accounts_receivables()
	{


		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transaction_date >= \''.date("Y-01-01").'\' AND transaction_date <= \''.date("Y-m-d").'\') ';

		}
		$search_status = $this->session->userdata('trail_budget_year');


		if($search_status)
		{
			$search_invoice_add = ' AND YEAR(transaction_date) = \''.$search_status.'\'';
		}
		else
		{
			$search_invoice_add = ' AND YEAR(transaction_date) = \''.date('Y').'\'';
		}

		//retrieve all users
		$this->db->from('v_transactions');
		$this->db->select('SUM(dr_amount) - SUM(cr_amount) AS total_amount');
		$this->db->where('v_transactions.transaction_id > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;

		return $total_invoices_balance ;

	}

	public function get_cash_on_hand()
	{

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transaction_date >= \''.date("Y-01-01").'\' AND transaction_date <= \''.date("Y-m-d").'\') ';

		}
		$this->db->from('v_transactions');
		$this->db->select('SUM(cr_amount) AS total_amount');
		$this->db->where('(party = "Patient" AND payment_method_name ="Cash") '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_addold =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_addold = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_addold = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_addold =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}

		$this->db->from('v_general_ledger_by_date');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount');
		$this->db->where('(
							(v_general_ledger_by_date.transactionClassification = "Purchase Payment" AND v_general_ledger_by_date.accountName = "Cash Account")
									OR (v_general_ledger_by_date.transactionCategory = "Transfer" AND  v_general_ledger_by_date.accountName = "Cash Account")
									OR (v_general_ledger_by_date.transactionCategory = "Expense Payment" AND  v_general_ledger_by_date.accountName = "Cash Account")) '.$search_invoice_addold);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row2 = $query->row();
		$total_transfer_balance = $query_row2->total_amount;

		return $total_invoices_balance - $total_transfer_balance;
	}
	public function get_accounts_payable()
	{
		//retrieve all users

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			// $search_invoice_add =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}

		$search_status = $this->session->userdata('trail_budget_year');



		if($search_status)
		{
			$search_invoice_add = ' AND YEAR(transactionDate) = \''.$search_status.'\'';
		}
		else
		{
			$search_invoice_add = ' AND YEAR(transactionDate) = \''.date('Y').'\'';
		}




		$this->db->from('v_creditor_ledger_aging');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount');
		$this->db->where('transactionId > 0  '.$search_invoice_add);
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;



		return $total_invoices_balance;
	}


	public function get_accounts_providers()
	{
		//retrieve all users

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			// $search_invoice_add =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}

		$search_status = $this->session->userdata('trail_budget_year');


		if($search_status)
		{
			$search_invoice_add = ' AND YEAR(transactionDate) = \''.$search_status.'\'';
		}
		else
		{
			$search_invoice_add = ' AND YEAR(transactionDate) = \''.date('Y').'\'';
		}



		$this->db->from('v_provider_ledger_aging');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount');
		$this->db->where('transactionId > 0  '.$search_invoice_add);
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;



		return $total_invoices_balance;
	}




	public function get_payroll_liability()
	{
		//retrieve all users

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			// $search_invoice_add =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}


		$search_status = $this->session->userdata('trail_budget_year');


		if($search_status)
		{
			$search_invoice_add = ' AND YEAR(transactionDate) = \''.$search_status.'\'';
		}
		else
		{
			$search_invoice_add = ' AND YEAR(transactionDate) = \''.date('Y').'\'';
		}




		$this->db->from('v_payroll_ledger_aging');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount');
		$this->db->where('transactionId > 0  '.$search_invoice_add);
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;



		return $total_invoices_balance;
	}


	public function get_total_wht_tax()
	{
		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND created = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (created >= \''.date("Y-01-01").'\' AND created <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('creditor_invoice_item');
		$this->db->select('SUM(vat_amount) AS total_amount');
		$this->db->where('vat_type_id = 2 AND vat_amount > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND created = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (created >= \''.date("Y-01-01").'\' AND created <= \''.date("Y-m-d").'\') ';

		}

		//retrieve all users
		$this->db->from('creditor_credit_note_item');
		$this->db->select('SUM(credit_note_charged_vat) AS total_amount');
		$this->db->where('vat_type_id = 2 AND credit_note_charged_vat > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query2 = $this->db->get();
		$query_row2 = $query2->row();
		$total_credit_note_balance = $query_row2->total_amount;

		return $total_invoices_balance - $total_credit_note_balance;

	}

	public function get_total_vat_tax()
	{

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND created = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (created >= \''.date("Y-01-01").'\' AND created <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('creditor_invoice_item');
		$this->db->select('SUM(vat_amount) AS total_amount');
		$this->db->where('vat_type_id = 1 AND vat_amount > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND created = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (created >= \''.date("Y-01-01").'\' AND created <= \''.date("Y-m-d").'\') ';

		}

		//retrieve all users
		$this->db->from('creditor_credit_note_item');
		$this->db->select('SUM(credit_note_charged_vat) AS total_amount');
		$this->db->where('vat_type_id = 1 AND credit_note_charged_vat > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query2 = $this->db->get();
		$query_row2 = $query2->row();
		$total_credit_note_balance = $query_row2->total_amount;

		// var_dump($total_credit_note_balance);die();
		return $total_invoices_balance - $total_credit_note_balance;

	}


	public function get_tax_total_wht_tax()
	{
		$search_status = $this->session->userdata('tax_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_tax');
			$date_to = $this->session->userdata('date_to_tax');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND created = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (created >= \''.date("Y-01-01").'\' AND created <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('creditor_invoice_item');
		$this->db->select('SUM(vat_amount) AS total_amount');
		$this->db->where('vat_type_id = 2 AND vat_amount > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		return $total_invoices_balance;

	}


	public function get_tax_total_vat_tax()
	{

		$search_status = $this->session->userdata('tax_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_tax');
			$date_to = $this->session->userdata('date_to_tax');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND created = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (created >= \''.date("Y-01-01").'\' AND created <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('creditor_invoice_item');
		$this->db->select('SUM(vat_amount) AS total_amount');
		$this->db->where('vat_type_id = 1 AND vat_amount > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;




		return $total_invoices_balance;

	}
	public function get_all_vendors()
	{
		$this->db->from('creditor');
		$this->db->select('*');
		$this->db->where('creditor_status = 0');
		$this->db->order_by('creditor_name','ASC');
		$query = $this->db->get();

		return $query;
	}

	public function get_creditor_expenses($creditor_id)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('v_creditor_ledger_aging');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount');
		$this->db->where('recepientId = '.$creditor_id.''.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		return $total_invoices_balance ;
	}


	public function get_creditor_period_invoices_expenses($creditor_id)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			// $search_invoice_add =  ' AND (transaction_date >= \''.date("Y-01-01").'\' AND transaction_date <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('v_creditor_ledger_aging');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount');
		$this->db->where('recepientId = '.$creditor_id.''.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		return $total_invoices_balance ;
	}



	public function get_all_visit_types()
	{
		$this->db->from('visit_type');
		$this->db->select('*');
		$this->db->where('visit_type_id > 0');
		$query = $this->db->get();

		return $query;
	}

	public function get_receivable_balances($visit_type_id)
	{
		$search_status = $this->session->userdata('customer_income_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_customer_income');
			$date_to = $this->session->userdata('date_to_customer_income');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (invoice_date >= \''.$date_from.'\' AND invoice_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND invoice_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND invoice_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = ' AND invoice_date = \''.date('Y-m-d').'\'';

		}

		//retrieve all users
		$this->db->from('v_transactions');
		$this->db->select('SUM(dr_amount) AS dr_amount,SUM(cr_amount) AS cr_amount');
		$this->db->where('invoice_date >= "2018-03-01" AND payment_type = '.$visit_type_id.'   '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		// $total_invoices_balance = $query_row->total_amount;
		$payments = $query_row->cr_amount;
		$invoices = $query_row->dr_amount;
		$checked['invoice'] = $invoices;
		$checked['payments'] = $payments;


		return $checked;

	}

	public function get_total_receivable_wht_tax()
	{
		//retrieve all users
		$this->db->from('lease_invoice,invoice');
		$this->db->select('SUM(tax_amount) AS total_amount');
		$this->db->where('tax_amount > 0 AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		return $total_invoices_balance;

	}

	public function get_opening_stock()
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$start_date = date('Y-m-01', strtotime($date_from));
				$search_invoice_add =  '  AND transactionDate < \''.$start_date.'\' ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Product Opening Stock" OR  v_product_stock.transactionClassification = "Supplier Purchases" OR  v_product_stock.transactionClassification = "Product Addition" OR v_product_stock.transactionClassification = "Supplier Credit Note" OR v_product_stock.transactionClassification = "Product Deductions" OR v_product_stock.transactionClassification = "Drug Sales") AND ( category_id = 2 OR category_id = 3)  '.$search_invoice_add;
		$this->db->select('SUM(dr_amount) - SUM(cr_amount)  AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}



		return  $starting_value;
	}



	public function get_product_balance_brought_forward()
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$start_date = date('Y-m-01', strtotime($date_from));
				$search_invoice_add =  '  AND transactionDate < \''.$start_date.'\' ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Product Opening Stock" OR  v_product_stock.transactionClassification = "Supplier Purchases" OR  v_product_stock.transactionClassification = "Product Addition" OR v_product_stock.transactionClassification = "Supplier Credit Note" OR v_product_stock.transactionClassification = "Product Deductions" OR v_product_stock.transactionClassification = "Drug Sales")  '.$search_invoice_add;
		$this->db->select('SUM(dr_amount) AS dr_amount, SUM(cr_amount)  AS cr_amount, SUM(dr_quantity) AS dr_quantity,SUM(cr_quantity) AS cr_quantity');
		$this->db->where($where);
		$query = $this->db->get($table);



		return  $query;
	}

	public function get_product_purchases_new($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		// $table = 'v_product_stock';
		// $where = '(v_product_stock.transactionClassification = "Product Opening Stock" OR  v_product_stock.transactionClassification = "Supplier Purchases" )  AND ( category_id = 2 OR category_id = 3) '.$search_invoice_add;
		// $this->db->select('SUM(dr_amount) - SUM(cr_amount)  AS starting_value');
		// $this->db->where($where);
		$select_statement = "
							SELECT
								data.transactionCategory,
								SUM(data.dr_amount) AS dr_amount,
								SUM(data.cr_amount) AS cr_amount
							FROM
							(
								SELECT
								  `store_product`.`store_product_id` AS `transactionId`,
								  `product`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  `store_product`.`owning_store_id` AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  concat(
								    'Opening Balance of', ' ', `product`.`product_name`
								  ) AS `transactionDescription`,
								  `store_product`.`store_quantity` AS `dr_quantity`,
								  '0' AS `cr_quantity`,
								  (
								    `product`.`product_unitprice` * `store_product`.`store_quantity`
								  ) AS `dr_amount`,
								  '0' AS `cr_amount`,
								  `store_product`.`created` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Purchases' AS `transactionCategory`,
								  'Product Opening Stock' AS `transactionClassification`,
								  'store_product' AS `transactionTable`,
								  'product' AS `referenceTable`
								from
								  (
								    (
								      `store_product`
								      join `product` on(
								        (
								          (
								            `product`.`product_id` = `store_product`.`product_id`
								          )
								          and (`product`.`product_deleted` = 0)
								        )
								      )
								    )
								    join `store` on(
								      (
								        `store`.`store_id` = `store_product`.`owning_store_id`
								      )
								    )
								  )
								union all
								select
								  `order_supplier`.`order_supplier_id` AS `transactionId`,
								  `product`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  `orders`.`store_id` AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  concat(
								    'Purchase of', ' ', `product`.`product_name`
								  ) AS `transactionDescription`,
								  (
								    `order_supplier`.`quantity_received` * `order_supplier`.`pack_size`
								  ) AS `dr_quantity`,
								  '0' AS `cr_quantity`,
								  `order_supplier`.`total_amount` AS `dr_amount`,
								  '0' AS `cr_amount`,
								  `orders`.`supplier_invoice_date` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Purchases' AS `transactionCategory`,
								  'Supplier Purchases' AS `transactionClassification`,
								  'order_item' AS `transactionTable`,
								  'orders' AS `referenceTable`
								from
								  (
								    (
								      (
								        (
								          `order_item`
								          join `order_supplier`
								        )
								        join `product`
								      )
								      join `orders`
								    )
								    join `store` on(
								      (
								        `store`.`store_id` = `orders`.`store_id`
								      )
								    )
								  )
								where
								  (
								    (
								      `order_item`.`order_item_id` = `order_supplier`.`order_item_id`
								    )
								    and (
								      `order_item`.`product_id` = `product`.`product_id`
								    )
								    and (
								      `orders`.`order_id` = `order_item`.`order_id`
								    )
								    and (`product`.`product_deleted` = 0)
								    and (`orders`.`supplier_id` > 0)
								    and (`orders`.`is_store` < 2)
								    and (
								      `orders`.`order_approval_status` = 7
								    )
								    and (`product`.`product_id` <> 0)
								  )
								union all
								select
								  `product_purchase`.`purchase_id` AS `transactionId`,
								  `product_purchase`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  `product_purchase`.`store_id` AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  `product_purchase`.`purchase_description` AS `transactionDescription`,
								  (
								    `product_purchase`.`purchase_quantity` * `product_purchase`.`purchase_pack_size`
								  ) AS `dr_quantity`,
								  '0' AS `cr_quantity`,
								  (
								    `product`.`product_unitprice` * (
								      `product_purchase`.`purchase_quantity` * `product_purchase`.`purchase_pack_size`
								    )
								  ) AS `dr_amount`,
								  '0' AS `cr_amount`,
								  `product_purchase`.`purchase_date` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Additions' AS `transactionCategory`,
								  'Product Addition' AS `transactionClassification`,
								  'product_purchase' AS `transactionTable`,
								  'product' AS `referenceTable`
								from
								  (
								    (
								      `product_purchase`
								      join `product`
								    )
								    join `store` on(
								      (
								        `store`.`store_id` = `product_purchase`.`store_id`
								      )
								    )
								  )
								where
								  (
								    (
								      `product`.`product_id` = `product_purchase`.`product_id`
								    )
								    and (`product`.`product_deleted` = 0)
								  )
								union all
								select
								  `product_deductions_stock`.`product_deductions_stock_id` AS `transactionId`,
								  `product_deductions_stock`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  `product_deductions_stock`.`store_id` AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  `product_deductions_stock`.`deduction_description` AS `transactionDescription`,
								  '0' AS `dr_quantity`,
								  (
								    `product_deductions_stock`.`product_deductions_stock_quantity` * `product_deductions_stock`.`product_deductions_stock_pack_size`
								  ) AS `cr_quantity`,
								  '0' AS `dr_amount`,
								  (
								    `product`.`product_unitprice` * (
								      `product_deductions_stock`.`product_deductions_stock_quantity` * `product_deductions_stock`.`product_deductions_stock_pack_size`
								    )
								  ) AS `cr_amount`,
								  `product_deductions_stock`.`product_deductions_stock_date` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Deductions' AS `transactionCategory`,
								  'Product Deductions' AS `transactionClassification`,
								  'product_deductions_stock' AS `transactionTable`,
								  'product' AS `referenceTable`
								from
								  (
								    (
								      `product_deductions_stock`
								      join `product`
								    )
								    join `store` on(
								      (
								        `store`.`store_id` = `product_deductions_stock`.`store_id` AND store.store_parent = 0
								      )
								    )
								  )
								where
								  (
								    (
								      `product`.`product_id` = `product_deductions_stock`.`product_id`
								    )
								    and (`product`.`product_deleted` = 0) AND (product.category_id < 2 OR product.category_id > 2)
								  )
								union all
								select
								  `order_supplier`.`order_supplier_id` AS `transactionId`,
								  `product`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  `orders`.`store_id` AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  concat(
								    'Credit note of', ' ', `product`.`product_name`
								  ) AS `transactionDescription`,
								  '0' AS `dr_quantity`,
								  (
								    `order_supplier`.`quantity_received` * `order_supplier`.`pack_size`
								  ) AS `cr_quantity`,
								  '0' AS `dr_amount`,
								  `order_supplier`.`total_amount` AS `cr_amount`,
								  `orders`.`supplier_invoice_date` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Return Outwards' AS `transactionCategory`,
								  'Supplier Credit Note' AS `transactionClassification`,
								  'order_item' AS `transactionTable`,
								  'orders' AS `referenceTable`
								from
								  (
								    (
								      (
								        (
								          `order_item`
								          join `order_supplier`
								        )
								        join `product`
								      )
								      join `orders`
								    )
								    join `store` on(
								      (
								        `store`.`store_id` = `orders`.`store_id`
								      )
								    )
								  )
								where
								  (
								    (
								      `order_item`.`order_item_id` = `order_supplier`.`order_item_id`
								    )
								    and (
								      `order_item`.`product_id` = `product`.`product_id`
								    )
								    and (
								      `orders`.`order_id` = `order_item`.`order_id`
								    )
								    and (`product`.`product_deleted` = 0)
								    and (`orders`.`supplier_id` > 0)
								    and (
								      `orders`.`order_approval_status` = 7
								    )
								    and (`orders`.`is_store` = 3)
								  )
								union all
								select
								  `visit_charge`.`visit_charge_id` AS `transactionId`,
								  `product`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  visit_charge.store_id AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  concat(
								    'Product Sale', ' ', `product`.`product_name`
								  ) AS `transactionDescription`,
								  '0' AS `dr_quantity`,
								  `visit_charge`.`visit_charge_units` AS `cr_quantity`,
								  '0' AS `dr_amount`,
								  (
								    `visit_charge`.`visit_charge_units` * `visit_charge`.`buying_price`
								  ) AS `cr_amount`,
								  `visit_charge`.`date` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Sales' AS `transactionCategory`,
								  'Drug Sales' AS `transactionClassification`,
								  'visit_charge' AS `transactionTable`,
								  'product' AS `referenceTable`
								from
								  (
								    (
								      `visit_charge`
								      join `product`
								    )
								    join `store` on(
								      (`store`.`store_id` = visit_charge.store_id)
								    )
								  )
								where
								  (
								    (`visit_charge`.`charged` = 1)
								    and (
								      `visit_charge`.`visit_charge_delete` = 0
								    )
								    and (
								      `product`.`product_id` = `visit_charge`.`product_id`
								    )
								    and (`product`.`product_deleted` = 0)
								  )
								union all
								select
								  `product_deductions`.`product_deductions_id` AS `transactionId`,
								  `product`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  `store`.`store_id` AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  concat(
								    'Product Transfered', ' ', `product`.`product_name`
								  ) AS `transactionDescription`,
								  '0' AS `dr_quantity`,
								  (
								    `product_deductions`.`quantity_given` * `product_deductions`.`pack_size`
								  ) AS `cr_quantity`,
								  '0' AS `dr_amount`,
								  (
								    `product`.`product_unitprice` * (
								      `product_deductions`.`quantity_given` * `product_deductions`.`pack_size`
								    )
								  ) AS `cr_amount`,
								  `product_deductions`.`search_date` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Deductions' AS `transactionCategory`,
								  'Drug Transfer' AS `transactionClassification`,
								  'product_deductions' AS `transactionTable`,
								  'product' AS `referenceTable`
								from
								  (
								    (
								      (
								        `product_deductions`
								        join `product`
								      )
								      join `orders`
								    )
								    join `store` on(
								      (
								        `orders`.`store_id` = `store`.`store_id`
								      )
								    )
								  )
								where
								  (
								    (
								      `product_deductions`.`order_id` = `orders`.`order_id`
								    )
								    and (
								      `product_deductions`.`product_id` = `product`.`product_id`
								    )
								    and (`product`.`product_deleted` = 0)
								    and (`orders`.`supplier_id` > 0)
								    and (`orders`.`is_store` = 2)
								    and (
								      `orders`.`order_approval_status` = 7
								    )
								  )
							) AS data WHERE ".$search_invoice_add."  GROUP BY data.transactionCategory";
		$query = $this->db->query($select_statement);

		// var_dump($query->result());die();
		$result = $query->row();

		$product_value  = array();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$item['name'] = $value->transactionCategory;
				$item['value'] = $value->dr_amount - $value->cr_amount;

				array_push($product_value, $item);
			}
		}

		// var_dump($product_value);
		return  $product_value;
	}

	public function get_opening_stock_value()
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$start_date = date('Y-m-01', strtotime($date_from));
				$search_invoice_add =  '  transactionDate < \''.$start_date.'\' ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' transactionDate < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' transactionDate < \''.$date_to.'\'';
			}
		}
		else
		{
			// $start_date = $this->company_financial_model->get_inventory_start_date();

			$search_invoice_add = ' transactionDate < \''.$start_date.'\'';

		}
			$select_statement = "
							SELECT
								SUM(data.dr_amount) AS dr_amount,
								SUM(data.cr_amount) AS cr_amount
							FROM
							(
								SELECT
								  `store_product`.`store_product_id` AS `transactionId`,
								  `product`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  `store_product`.`owning_store_id` AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  concat(
								    'Opening Balance of', ' ', `product`.`product_name`
								  ) AS `transactionDescription`,
								  `store_product`.`store_quantity` AS `dr_quantity`,
								  '0' AS `cr_quantity`,
								  (
								    `product`.`product_unitprice` * `store_product`.`store_quantity`
								  ) AS `dr_amount`,
								  '0' AS `cr_amount`,
								  `store_product`.`created` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Purchases' AS `transactionCategory`,
								  'Product Opening Stock' AS `transactionClassification`,
								  'store_product' AS `transactionTable`,
								  'product' AS `referenceTable`
								from
								  (
								    (
								      `store_product`
								      join `product` on(
								        (
								          (
								            `product`.`product_id` = `store_product`.`product_id`
								          )
								          and (`product`.`product_deleted` = 0)
								        )
								      )
								    )
								    join `store` on(
								      (
								        `store`.`store_id` = `store_product`.`owning_store_id`
								      )
								    )
								  )
								union all
								select
								  `order_supplier`.`order_supplier_id` AS `transactionId`,
								  `product`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  `orders`.`store_id` AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  concat(
								    'Purchase of', ' ', `product`.`product_name`
								  ) AS `transactionDescription`,
								  (
								    `order_supplier`.`quantity_received` * `order_supplier`.`pack_size`
								  ) AS `dr_quantity`,
								  '0' AS `cr_quantity`,
								  `order_supplier`.`total_amount` AS `dr_amount`,
								  '0' AS `cr_amount`,
								  `orders`.`supplier_invoice_date` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Purchases' AS `transactionCategory`,
								  'Supplier Purchases' AS `transactionClassification`,
								  'order_item' AS `transactionTable`,
								  'orders' AS `referenceTable`
								from
								  (
								    (
								      (
								        (
								          `order_item`
								          join `order_supplier`
								        )
								        join `product`
								      )
								      join `orders`
								    )
								    join `store` on(
								      (
								        `store`.`store_id` = `orders`.`store_id`
								      )
								    )
								  )
								where
								  (
								    (
								      `order_item`.`order_item_id` = `order_supplier`.`order_item_id`
								    )
								    and (
								      `order_item`.`product_id` = `product`.`product_id`
								    )
								    and (
								      `orders`.`order_id` = `order_item`.`order_id`
								    )
								    and (`product`.`product_deleted` = 0)
								    and (`orders`.`supplier_id` > 0)
								    and (`orders`.`is_store` < 2)
								    and (
								      `orders`.`order_approval_status` = 7
								    )
								    and (`product`.`product_id` <> 0)
								  )
								union all
								select
								  `product_purchase`.`purchase_id` AS `transactionId`,
								  `product_purchase`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  `product_purchase`.`store_id` AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  `product_purchase`.`purchase_description` AS `transactionDescription`,
								  (
								    `product_purchase`.`purchase_quantity` * `product_purchase`.`purchase_pack_size`
								  ) AS `dr_quantity`,
								  '0' AS `cr_quantity`,
								  (
								    `product`.`product_unitprice` * (
								      `product_purchase`.`purchase_quantity` * `product_purchase`.`purchase_pack_size`
								    )
								  ) AS `dr_amount`,
								  '0' AS `cr_amount`,
								  `product_purchase`.`purchase_date` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Additions' AS `transactionCategory`,
								  'Product Addition' AS `transactionClassification`,
								  'product_purchase' AS `transactionTable`,
								  'product' AS `referenceTable`
								from
								  (
								    (
								      `product_purchase`
								      join `product`
								    )
								    join `store` on(
								      (
								        `store`.`store_id` = `product_purchase`.`store_id`
								      )
								    )
								  )
								where
								  (
								    (
								      `product`.`product_id` = `product_purchase`.`product_id`
								    )
								    and (`product`.`product_deleted` = 0)
								  )
								union all
								select
								  `product_deductions_stock`.`product_deductions_stock_id` AS `transactionId`,
								  `product_deductions_stock`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  `product_deductions_stock`.`store_id` AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  `product_deductions_stock`.`deduction_description` AS `transactionDescription`,
								  '0' AS `dr_quantity`,
								  (
								    `product_deductions_stock`.`product_deductions_stock_quantity` * `product_deductions_stock`.`product_deductions_stock_pack_size`
								  ) AS `cr_quantity`,
								  '0' AS `dr_amount`,
								  (
								    `product`.`product_unitprice` * (
								      `product_deductions_stock`.`product_deductions_stock_quantity` * `product_deductions_stock`.`product_deductions_stock_pack_size`
								    )
								  ) AS `cr_amount`,
								  `product_deductions_stock`.`product_deductions_stock_date` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Deductions' AS `transactionCategory`,
								  'Product Deductions' AS `transactionClassification`,
								  'product_deductions_stock' AS `transactionTable`,
								  'product' AS `referenceTable`
								from
								  (
								    (
								      `product_deductions_stock`
								      join `product`
								    )
								    join `store` on(
								      (
								        `store`.`store_id` = `product_deductions_stock`.`store_id`
								      )
								    )
								  )
								where
								  (
								    (
								      `product`.`product_id` = `product_deductions_stock`.`product_id`
								    )
								    and (`product`.`product_deleted` = 0)
								  )
								union all
								select
								  `order_supplier`.`order_supplier_id` AS `transactionId`,
								  `product`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  `orders`.`store_id` AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  concat(
								    'Credit note of', ' ', `product`.`product_name`
								  ) AS `transactionDescription`,
								  '0' AS `dr_quantity`,
								  (
								    `order_supplier`.`quantity_received` * `order_supplier`.`pack_size`
								  ) AS `cr_quantity`,
								  '0' AS `dr_amount`,
								  `order_supplier`.`total_amount` AS `cr_amount`,
								  `orders`.`supplier_invoice_date` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Return Outwards' AS `transactionCategory`,
								  'Supplier Credit Note' AS `transactionClassification`,
								  'order_item' AS `transactionTable`,
								  'orders' AS `referenceTable`
								from
								  (
								    (
								      (
								        (
								          `order_item`
								          join `order_supplier`
								        )
								        join `product`
								      )
								      join `orders`
								    )
								    join `store` on(
								      (
								        `store`.`store_id` = `orders`.`store_id`
								      )
								    )
								  )
								where
								  (
								    (
								      `order_item`.`order_item_id` = `order_supplier`.`order_item_id`
								    )
								    and (
								      `order_item`.`product_id` = `product`.`product_id`
								    )
								    and (
								      `orders`.`order_id` = `order_item`.`order_id`
								    )
								    and (`product`.`product_deleted` = 0)
								    and (`orders`.`supplier_id` > 0)
								    and (
								      `orders`.`order_approval_status` = 7
								    )
								    and (`orders`.`is_store` = 3)
								  )
								union all
								select
								  `visit_charge`.`visit_charge_id` AS `transactionId`,
								  `product`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  visit_charge.store_id AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  concat(
								    'Product Sale', ' ', `product`.`product_name`
								  ) AS `transactionDescription`,
								  '0' AS `dr_quantity`,
								  `visit_charge`.`visit_charge_units` AS `cr_quantity`,
								  '0' AS `dr_amount`,
								  (
								    `visit_charge`.`visit_charge_units` * `visit_charge`.`buying_price`
								  ) AS `cr_amount`,
								  `visit_charge`.`date` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Sales' AS `transactionCategory`,
								  'Drug Sales' AS `transactionClassification`,
								  'visit_charge' AS `transactionTable`,
								  'product' AS `referenceTable`
								from
								  (
								    (
								      `visit_charge`
								      join `product`
								    )
								    join `store` on(
								      (`store`.`store_id` = visit_charge.store_id)
								    )
								  )
								where
								  (
								    (`visit_charge`.`charged` = 1)
								    and (
								      `visit_charge`.`visit_charge_delete` = 0
								    )
								    and (
								      `product`.`product_id` = `visit_charge`.`product_id`
								    )
								    and (`product`.`product_deleted` = 0)
								  )
								union all
								select
								  `product_deductions`.`product_deductions_id` AS `transactionId`,
								  `product`.`product_id` AS `product_id`,
								  `product`.`category_id` AS `category_id`,
								  `store`.`store_id` AS `store_id`,
								  '' AS `receiving_store`,
								  `product`.`product_name` AS `product_name`,
								  `store`.`store_name` AS `store_name`,
								  concat(
								    'Product Transfered', ' ', `product`.`product_name`
								  ) AS `transactionDescription`,
								  '0' AS `dr_quantity`,
								  (
								    `product_deductions`.`quantity_given` * `product_deductions`.`pack_size`
								  ) AS `cr_quantity`,
								  '0' AS `dr_amount`,
								  (
								    `product`.`product_unitprice` * (
								      `product_deductions`.`quantity_given` * `product_deductions`.`pack_size`
								    )
								  ) AS `cr_amount`,
								  `product_deductions`.`search_date` AS `transactionDate`,
								  `product`.`product_status` AS `status`,
								  `product`.`product_deleted` AS `product_deleted`,
								  'Deductions' AS `transactionCategory`,
								  'Drug Transfer' AS `transactionClassification`,
								  'product_deductions' AS `transactionTable`,
								  'product' AS `referenceTable`
								from
								  (
								    (
								      (
								        `product_deductions`
								        join `product`
								      )
								      join `orders`
								    )
								    join `store` on(
								      (
								        `orders`.`store_id` = `store`.`store_id`
								      )
								    )
								  )
								where
								  (
								    (
								      `product_deductions`.`order_id` = `orders`.`order_id`
								    )
								    and (
								      `product_deductions`.`product_id` = `product`.`product_id`
								    )
								    and (`product`.`product_deleted` = 0)
								    and (`orders`.`supplier_id` > 0)
								    and (`orders`.`is_store` = 2)
								    and (
								      `orders`.`order_approval_status` = 7
								    )
								  )
							) AS data WHERE ".$search_invoice_add."";
		$query = $this->db->query($select_statement);

		// var_dump($query->result());die();
		$result = $query->row();

		$dr_amount = $result->dr_amount;
		$cr_amount = $result->cr_amount;

		$starting_value = $dr_amount - $cr_amount;
		return  $starting_value;
	}

	public function get_product_purchases($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Product Opening Stock" OR  v_product_stock.transactionClassification = "Supplier Purchases" )  AND ( category_id = 2 OR category_id = 3) '.$search_invoice_add;
		$this->db->select('SUM(dr_amount) - SUM(cr_amount)  AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}
		return  $starting_value;
	}



	public function get_product_other_purchases($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Product Addition" ) AND ( category_id = 2 OR category_id = 3)  '.$search_invoice_add;
		$this->db->select('SUM(dr_amount)  AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}
		return  $starting_value;
	}


	public function get_product_return_outwards($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Supplier Credit Note" ) AND ( category_id = 2 OR category_id = 3)  '.$search_invoice_add;
		$this->db->select('SUM(cr_amount)  AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}
		return  $starting_value;
	}


	public function get_total_other_deductions($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Product Deductions" OR v_product_stock.transactionClassification = "Drug Transfer" ) AND ( category_id = 2 OR category_id = 3)  '.$search_invoice_add;
		$this->db->select('SUM(cr_amount)  AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}
		return  $starting_value;
	}
	public function get_product_sales($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = 'v_product_stock';
		$where = '(v_product_stock.transactionClassification = "Drug Sales") AND ( category_id = 2 OR category_id = 3)   '.$search_invoice_add;
		$this->db->select('SUM(cr_amount)  AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}
		return  $starting_value;
	}


	public function get_closing_stock_old()
	{

		$table = 'product,store_product';
		$where = 'product.product_status = 1 AND product.product_deleted = 0 AND product.product_id = store_product.product_id';
		$this->db->select('SUM((store_product.store_quantity * product.product_unitprice)) AS starting_value');
		$this->db->where($where);
		$query = $this->db->get($table);


		$result = $query->row();

		$starting_value  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$starting_value = $value->starting_value;
			}
		}

		$inventory_start_date = $this->company_financial_model->get_inventory_start_date();
		$sales_value = $this->company_financial_model->get_prev_drug_units_sold_value($inventory_start_date);
		$procurred_amount = $this->company_financial_model->get_prev_total_purchases();

		return ($procurred_amount + $starting_value) - $sales_value;
	}


	public function get_stock_value()
	{
		$inventory_start_date = $this->company_financial_model->get_inventory_start_date();

		$sales_value = $this->company_financial_model->get_drug_units_sold_value($inventory_start_date);
		// $procurred_amount = $this->company_financial_model->get_total_purchases();

		return $sales_value;
	}

	public function get_all_fixed_categories()
	{
		$this->db->select('*');
		$this->db->where('asset_category_id > 0');
		$query = $this->db->get('asset_category');

		return $query;
	}

	public function get_visit_details()
	{
		$this->db->from('visit_type');
		$this->db->select('*');
		$this->db->where('visit_type_status = 1');
		$query = $this->db->get();

		return $query;
	}

	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	public function get_visit_type_invoice($visit_type_id)
	{
		//retrieve all users

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_payments_add =  ' AND (payment_created >= \''.$date_from.'\' AND payment_created <= \''.$date_to.'\') ';
				$search_invoice_add =  ' AND (visit_charge.date >= \''.$date_from.'\' AND visit_charge.date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_payments_add = 'AND payments.payment_created = \''.$date_from.'\'';
				$search_invoice_add = 'AND visit_charge.date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_payments_add = 'AND payments.payment_created = \''.$date_to.'\'';
				$search_invoice_add = 'AND visit_charge.date = \''.$date_to.'\'';
			}
				// var_dump($search_payments_add); die();
		}


		$this->db->from('visit,payments');
		$this->db->select('SUM(amount_paid) AS total_payments');
		$this->db->where('visit.visit_delete = 0 AND payments.cancel = 0 AND visit.visit_id = payments.visit_id AND visit.visit_type = '.$visit_type_id.$search_payments_add);
		$query = $this->db->get('');
		$invoice_amount = 0;
		$payment_amount = 0;
		$balance_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$payment_amount =$value->total_payments;

			}
		}

		$this->db->from('visit,visit_charge');
		$this->db->select('SUM(visit_charge_amount) AS total_invoice');
		$this->db->where('visit.visit_delete = 0 AND visit_charge.visit_charge_delete = 0 AND visit.visit_id = visit_charge.visit_id AND visit.visit_type = '.$visit_type_id.$search_invoice_add);
		$visit_charge_query = $this->db->get('');
		$invoice_amount = 0;
		if($visit_charge_query->num_rows() > 0)
		{
			foreach ($visit_charge_query->result() as $key => $value_charge) {
				# code...
				$invoice_amount =$value_charge->total_invoice;

			}
		}

		$balance_amount = $invoice_amount - $payment_amount;

		$response['invoice_total'] = $invoice_amount;
		$response['payments_value']= $payment_amount;
		$response['balance'] = $balance_amount;
		return $response;
	}



	public function get_prev_drug_units_sold_value($inventory_start_date, $product_id=NULL, $start_date = NULL, $end_date = NULL, $branch_code = NULL)
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (visit_charge.date >= \''.$date_from.'\' AND visit_charge.date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND visit_charge.date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND visit_charge.date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = "visit_charge, service_charge,product";
		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.charged = 1 AND service_charge.product_id > 0 AND visit_charge.visit_charge_delete = 0 AND product.product_id = service_charge.product_id AND product.product_deleted = 0 '.$search_invoice_add;



		$items = "SUM((visit_charge.visit_charge_units * visit_charge.visit_charge_amount)) AS amount";
		$order = "date";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		$total_sold = 0;
		if(count($result) > 0)
		{
			foreach ($result as $key) {
				# code...
				$amount = $key->amount;

				$total_sold =$amount;
			}
		}
		return $total_sold;
	}

	public function get_inventory_start_date()
	{
		$this->db->where('branch_code', $this->session->userdata('branch_code'));
		$query = $this->db->get('branch');

		$inventory_start_date = '';
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$inventory_start_date = $row->inventory_start_date;
		}

		return $inventory_start_date;
	}

	public function get_total_purchases_old($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (orders.supplier_invoice_date >= \''.$date_from.'\' AND orders.supplier_invoice_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND orders.supplier_invoice_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND orders.supplier_invoice_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		// $table = 'v_general_ledger';
		// $where = '(transactionCategory = "Expense" AND accountName="Catering Expenses") '.$search_invoice_add;
		// $this->db->select('SUM(dr_amount) AS total_amount');
		// $this->db->where($where);
		// $query = $this->db->get($table);
		// $row = $query->row();
		//
		// $total_value = $row->total_amount;

		$table = 'order_supplier,orders,order_item';
		$where = 'invoice_number <> "" AND orders.order_id = order_supplier.order_id AND orders.supplier_id > 0 AND orders.order_approval_status = 7 AND order_supplier.order_item_id = order_item.order_item_id AND orders.supplier_invoice_date <> "0000-00-00" '.$search_invoice_add;
		$this->db->select('SUM(order_supplier.less_vat) AS total_amount');
		$this->db->where($where);
		$query = $this->db->get($table);
		$row = $query->row();

		$total_value = $row->total_amount;

		return $total_value;
	}

	public function get_prev_total_purchases($start_date=null)
	{


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');


			if(!empty($date_from))
			{
				$search_invoice_add = ' AND orders.supplier_invoice_date < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND orders.supplier_invoice_date < \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = 'order_supplier,orders,order_item';
		$where = 'invoice_number <> "" AND orders.order_id = order_supplier.order_id AND orders.supplier_id > 0 AND orders.order_approval_status = 7 AND order_supplier.order_item_id = order_item.order_item_id AND orders.supplier_invoice_date <> "0000-00-00" '.$search_invoice_add;
		$this->db->select('SUM(order_supplier.less_vat) AS total_amount');
		$this->db->where($where);
		$query = $this->db->get($table);
		$row = $query->row();

		$total_value = $row->total_amount;

		return $total_value;
	}

	public function get_drug_units_sold_value($inventory_start_date, $product_id=NULL, $start_date = NULL, $end_date = NULL, $branch_code = NULL)
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (visit_charge.date >= \''.$date_from.'\' AND visit_charge.date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND visit_charge.date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND visit_charge.date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$table = "visit_charge, service_charge,product";
		$where = 'visit_charge.service_charge_id = service_charge.service_charge_id AND visit_charge.charged = 1 AND service_charge.product_id > 0 AND visit_charge.visit_charge_delete = 0 AND product.product_id = service_charge.product_id AND product.product_deleted = 0 '.$search_invoice_add;



		$items = "SUM((visit_charge.visit_charge_units * visit_charge.visit_charge_amount)) AS amount";
		$order = "date";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		$total_sold = 0;
		if(count($result) > 0)
		{
			foreach ($result as $key) {
				# code...
				$amount = $key->amount;

				$total_sold =$amount;
			}
		}
		return $total_sold;
	}

	/*
	*	Retrieve all creditor
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_service_bills($table, $where, $per_page, $page, $order = 'v_transactions_by_date.transaction_date', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->join('personnel','personnel.personnel_id = v_transactions_by_date.personnel_id','left');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}

	function export_services_bills($department_id)
	{
		$this->load->library('excel');

		// var_dump($department_id);die();
		$where = 'visit_charge.visit_id = visit.visit_id AND visit.patient_id = patients.patient_id AND visit.visit_type = visit_type.visit_type_id AND visit_charge.service_charge_id = service_charge.service_charge_id AND visit.visit_delete = 0 AND visit_charge.visit_charge_delete = 0 AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id AND visit_invoice.visit_invoice_delete = 0 AND visit_charge.charged = 1 AND service_charge.service_id = service.service_id AND service.department_id ='.$department_id;

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$where .=  ' AND (visit_invoice.created >= \''.$date_from.'\' AND visit_invoice.created <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$where .= ' AND visit_invoice.created = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$where .= ' AND visit_invoice.created = \''.$date_to.'\'';
			}
		}
		else
		{
			$where .= '';

		}

		$branch_id = $this->session->userdata('branch_id');


		if(!empty($branch_id))
		{
			$where .= ' AND visit.branch_id = '.$branch_id;
		}

		$this->db->where($where);
		$this->db->select('*');
		$this->db->join('personnel','visit_charge.personnel_id = personnel.personnel_id','left');
		$table = 'visit_charge,visit,patients,service_charge,visit_type,service,visit_invoice';
		$visits_query = $this->db->get($table);

		$title = 'Service Bill Export '.date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$col_count = 0;

		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Invoice Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Name';
			$col_count++;
			$report[$row_count][$col_count] = 'Category';
			$col_count++;
			$report[$row_count][$col_count] = 'Service';
			$col_count++;
			$current_column = $col_count ;

			$report[$row_count][$current_column] = 'Provider';
			$current_column++;
			$report[$row_count][$current_column] = 'Units';
			$current_column++;
			$report[$row_count][$current_column] = 'Charge Amount';
			$current_column++;
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
					$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));

				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}

				$visit_id = $row->visit_id;
				$date = $row->date;
				$invoice_date = date('jS M Y',strtotime($row->date));
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type;
				$patient_number = $row->patient_number;
				$visit_type = $row->visit_type;
				$service_name = $row->service_name;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				// $rejected_amount = $row->amount_rejected;
				$invoice_number = $row->invoice_number;
				$visit_charge_amount = $row->visit_charge_amount;
				$visit_charge_units = $row->visit_charge_units;
				$visit_type_name = $row->visit_type_name;
				$personnel = $row->personnel_fname.' '.$row->personnel_onames;


				$count++;

				//display services charged to patient
				$total_invoiced2 = 0;


				//display the patient data
				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $invoice_date;
				$col_count++;
				$report[$row_count][$col_count] = $row->patient_surname.' '.$row->patient_othernames;
				$col_count++;
				$report[$row_count][$col_count] = $visit_type_name;
				$col_count++;
				$report[$row_count][$col_count] = $service_name;
				$col_count++;
				$current_column = $col_count;


				$report[$row_count][$current_column] = $personnel;
				$current_column++;
				$report[$row_count][$current_column] = $visit_charge_units;
				$current_column++;
				$report[$row_count][$current_column] = round($visit_charge_amount);
				$current_column++;
				$report[$row_count][$current_column] = round($visit_charge_amount * $visit_charge_units);
				$current_column++;

			}
		}

		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	public function get_account_name($from_account_id)
	{
		$account_name = '';
		$this->db->select('account_name');
		$this->db->where('account_id = '.$from_account_id);
		$query = $this->db->get('account');

		$account_details = $query->row();
		$account_name = $account_details->account_name;

		return $account_name;
	}


	public function get_service_ledger($service_id)
	{
		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transaction_date >= \''.$date_from.'\' AND transaction_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transaction_date = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		//retrieve all users
		$this->db->from('v_transactions_by_date');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount,v_transactions_by_date.*,patients.patient_surname,patients.patient_othernames,patients.patient_number');
		$this->db->group_by('v_transactions_by_date.reference_id');
		$this->db->join('patients','patients.patient_id = v_transactions_by_date.patient_id','LEFT');
		$this->db->where('(transactionCategory = "Revenue" OR transactionCategory = "Credit Note") AND child_service = '.$service_id.'  '.$search_invoice_add);
		$query = $this->db->get();

		return $query;
	}

	public function get_expense_ledger($account_id)
	{
		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		//retrieve all users
		$this->db->from('v_expenses_ledger');
		$this->db->select('dr_amount AS total_amount,v_expenses_ledger.*');
		$this->db->where('accountId = '.$account_id.'  '.$search_invoice_add);
		$query = $this->db->get();

		return $query;
	}



	public function get_accounts_ledger($account_id)
	{
		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('v_account_ledger_by_date');
		$this->db->select('v_account_ledger_by_date.*');
		$this->db->where('accountId = '.$account_id.'
						'.$search_invoice_add);
		$this->db->order_by('v_account_ledger_by_date.transactionDate','ASC');
		$query = $this->db->get();

		return $query;
	}


	public function get_account_opening_balance($account_id)
	{

		$this->db->from('account');
		$this->db->select('account.account_opening_balance AS total_amount');
		$this->db->where('account_id = '.$account_id);

		$query = $this->db->get();
		$balance = $query->row();

		$total_amount = $balance->total_amount;
		return $total_amount;
	}

	public function get_creditor_statement_balance($creditor_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate < \''.$date_from.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_to.'\'';
			}
		}
		else
		{

			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_creditor_ledger_aging_by_date,creditor');
		$this->db->select('SUM(`dr_amount`) AS dr_amount,SUM(cr_amount) AS cr_amount');
		$this->db->where('creditor.creditor_id = v_creditor_ledger_aging_by_date.recepientId AND v_creditor_ledger_aging_by_date.transactionDate >= creditor.start_date AND recepientId = '.$creditor_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}


	public function get_creditor_statement($creditor_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{

			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_creditor_ledger_aging_by_date,creditor');
		$this->db->select('*');
		$this->db->where('creditor.creditor_id = v_creditor_ledger_aging_by_date.recepientId AND v_creditor_ledger_aging_by_date.transactionDate >= creditor.start_date AND recepientId = '.$creditor_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}

	/*
	*	get a single creditor's details
	*	@param int $creditor_id
	*
	*/
	public function get_creditor($creditor_id)
	{
		//retrieve all users
		$this->db->from('creditor');
		$this->db->select('*');
		$this->db->where('creditor_id = '.$creditor_id);
		$query = $this->db->get();

		return $query;
	}

	/*
	*	Retrieve all creditor
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_income_statement_views($table, $where, $per_page, $page, $order = 'v_product_stock.transactionDate', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		// $this->db->join('personnel','personnel.personnel_id = visit_charge.provider_id','left');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}


	public function get_salary_expenses()
	{
		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (payroll_created_for >= \''.$date_from.'\' AND payroll_created_for <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND payroll_created_for = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND payroll_created_for = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		//retrieve all users
		$this->db->from('v_payroll');
		$this->db->select('SUM(total_additions) AS total_amount');
		$this->db->where('payroll_id > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$total_amount = 0;
		$query = $this->db->get();
		if($query->num_rows() > 0 )
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		return $total_amount;
	}


	public function get_statutories($statutory_id)
	{
		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (payroll_created_for >= \''.$date_from.'\' AND payroll_created_for <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND payroll_created_for = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND payroll_created_for = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		if($statutory_id == 1)
		{
			$column = 'total_nssf';
		}
		else if($statutory_id == 2)
		{
			$column = 'total_nhif';
		}
		else if($statutory_id == 3)
		{
			$column = 'total_paye';
		}
		else if($statutory_id == 4)
		{
			$column = 'total_relief';
		}
		else if($statutory_id == 5)
		{
			$column = 'total_loans';
		}
		//retrieve all users
		$this->db->from('v_payroll');
		$this->db->select('SUM('.$column.') AS total_amount');
		$this->db->where('payroll_id > 0 '.$search_invoice_add);
		// $this->db->group_by('accountId');
		$total_amount = 0;
		$query = $this->db->get();
		if($query->num_rows() > 0 )
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		return $total_amount;
	}


	// get non pharm purchases


	public function get_non_pharm_purchases($start_date=null)
	{
		if($start_date == NULL)
		{
			$start_date = date('Y-m-d');
		}


		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		// $table = 'v_product_stock_non_pharm';
		// $where = '(v_product_stock_non_pharm.category_id < 2 OR v_product_stock_non_pharm.category_id > 3) AND v_product_stock_non_pharm.transactionClassification ="Product Addition"  '.$search_invoice_add;
		// $this->db->select('SUM(dr_amount) - SUM(cr_amount)  AS starting_value,category_name,v_product_stock_non_pharm.category_id');
		// $this->db->group_by('v_product_stock_non_pharm.category_id');
		// $this->db->join('category','v_product_stock_non_pharm.category_id = category.category_id','left');

		$select_statement = "
							  SELECT
								`product_deductions_stock`.`product_deductions_stock_id` AS transactionId,
								`product_deductions_stock`.`product_id` AS `product_id`,
								`product`.`category_id` AS `category_id`,
								 product_deductions_stock.store_id AS `store_id`,
								 '' AS `receiving_store`,
								 product.product_name AS `product_name`,
								store.store_name AS `store_name`,
								 product_deductions_stock.deduction_description AS `transactionDescription`,
								 '0' AS `dr_quantity`,
								 (product_deductions_stock_quantity * product_deductions_stock_pack_size) AS  `cr_quantity`,
								'0' AS `dr_amount`,
								 (`product`.`product_unitprice` * (product_deductions_stock_quantity * product_deductions_stock_pack_size)) AS `cr_amount`,
								`product_deductions_stock`.`product_deductions_stock_date` AS `transactionDate`,
								`product`.`product_status` AS `status`,
								`product`.`product_deleted` AS `product_deleted`,
								category.category_name AS `transactionCategory`,
								'Product Deductions' AS `transactionClassification`,
								'product_deductions_stock' AS `transactionTable`,
								'product' AS `referenceTable`
								FROM (`product_deductions_stock`,product,store,category)
								WHERE product.product_id = product_deductions_stock.product_id AND product.product_deleted = 0
								AND store.store_id = product_deductions_stock.store_id
								AND product.category_id = category.category_id
								AND (product.category_id < 2 OR product.category_id > 2)
								AND store.store_parent > 0
								GROUP BY category.category_id
							  ";



		// $this->db->where($where);
		$query = $this->db->query($select_statement);


		// $result = $query->row();

		// $starting_value  =0 ;
		// if($query->num_rows() > 0)
		// {
		// 	foreach ($query->result() as $key => $value) {
		// 		# code...
		// 		$starting_value = $value->starting_value;
		// 	}
		// }
		return  $query;
	}


	public function export_stock_report($report_id,$category_id)
	{

	    $search_status = $this->session->userdata('income_statement_search');
	    $search_payments_add = '';
	    $search_invoice_add = '';
	    if($search_status == 1)
	    {
	        $stock_search  = $this->session->userdata('stock_report_id#'.$report_id);
	        // var_dump($stock_search);die();

			if(!empty($stock_search) AND $stock_search == $report_id)
			{
		    	// $exploded = explode('#', $stock_search);

		    	$export_report_id = $report_id;

		      	$date_from = $this->session->userdata('date_from_stock'.$export_report_id);
				$date_to = $this->session->userdata('date_to_stock'.$export_report_id);
				// var_dump($date_from);die();
				if(!empty($date_from) AND !empty($date_to))
				{
					$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
				}
				else if(!empty($date_from))
				{
					$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
				}
				else if(!empty($date_to))
				{
					$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
				}

		  }
	      else
	      {
	      	$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
	      }

	    }
	    else
	    {
	    	// $stock_search  = $this->session->userdata('stock_report_id#'.$report_id);
	    	 $stock_search  = $this->session->userdata('stock_report_id#'.$report_id);
	        // var_dump($stock_search);die();

				if(!empty($stock_search) AND $stock_search == $report_id)
				{
			    	// $exploded = explode('#', $stock_search);

			    	$export_report_id = $report_id;

			      	$date_from = $this->session->userdata('date_from_stock'.$export_report_id);
					$date_to = $this->session->userdata('date_to_stock'.$export_report_id);
					// var_dump($date_from);die();
					if(!empty($date_from) AND !empty($date_to))
					{
						$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
					}
					else if(!empty($date_from))
					{
						$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
					}
					else if(!empty($date_to))
					{
						$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
					}

			  }
			else
			{
				$search_invoice_add = '';
			}


    	}

    	$this->load->library('excel');
    	if($report_id == 1)
    	{
    		$table = 'v_product_stock';
    		$where = '(v_product_stock.transactionClassification = "Product Opening Stock" OR  v_product_stock.transactionClassification = "Supplier Purchases") AND ( category_id = 2 OR category_id = 3)  '.$search_invoice_add;
    		$report_title = 'Purchases Report';
    	}
    	else if($report_id == 2)
    	{
    		$table = 'v_product_stock';
    		$where = '(v_product_stock.transactionClassification = "Product Opening Stock" OR  v_product_stock.transactionClassification = "Supplier Purchases") AND ( category_id = 2 OR category_id = 3)  '.$search_invoice_add;
    		$report_title = 'Purchases Report';
    	}

    	else if($report_id == 3)
    	{
    		$table = 'v_product_stock';
			$where = '(v_product_stock.transactionClassification = "Product Addition") AND ( category_id = 2 OR category_id = 3)  '.$search_invoice_add;
    		$report_title = 'Other Additions Report';
    	}
    	else if($report_id == 4)
    	{
    		$table = 'v_product_stock';
    		$where = '(v_product_stock.transactionClassification = "Supplier Credit Note" ) AND ( category_id = 2 OR category_id = 3)  '.$search_invoice_add;
    		$report_title = 'Return Outwards';
    	}
    	else if($report_id == 5)
    	{
    		$table = 'v_product_stock';
			$where = '(v_product_stock.transactionClassification = "Product Deductions" OR v_product_stock.transactionClassification = "Drug Transfer") AND ( category_id = 2 OR category_id = 3)  '.$search_invoice_add;
    		$report_title = 'Other Deductions';
    	}

    	else if($report_id == 7)
    	{
    		$table = 'v_product_stock_non_pharm';
		    $where = '(v_product_stock_non_pharm.category_id < 2 OR v_product_stock_non_pharm.category_id > 3) AND v_product_stock_non_pharm.transactionClassification ="Product Addition" AND v_product_stock_non_pharm.category_id = '.$category_id.'   '.$search_invoice_add;


		    $this->db->where('category_id',$category_id);
		    $query_cat = $this->db->get('category');

		    $row = $query_cat->row();
		    $category_name = $row->category_name;


    		$report_title = $category_name;
    	}
    	$this->db->where($where);
		$this->db->order_by('transactionDate', 'ASC');
		$this->db->select('*');
		$visits_query = $this->db->get($table);

		$title = $report_title.' Export '.date('jS M Y',strtotime(date($date_from))).' - '.date('jS M Y',strtotime(date($date_to)));
		$col_count = 0;

		if($visits_query->num_rows() > 0)
		{
			$count = 0;

			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Transaction Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Product';
			$col_count++;
			$report[$row_count][$col_count] = 'Affected Store';
			$col_count++;
			$report[$row_count][$col_count] = 'Description';
			$col_count++;
			$report[$row_count][$col_count] = 'Quantity';
			$col_count++;
			$report[$row_count][$col_count] = 'Value';
			$col_count++;
			$total_dr_quantity = 0;
			$total_dr_amount = 0;
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;

				$transaction_id = $row->transactionId;
				$product_id = $row->product_id;
				$store_id = $row->store_id;
				$product_name = $row->product_name;
				$store_name = $row->store_name;
				$transactionDescription = $row->transactionDescription;
				$dr_quantity = $row->dr_quantity;
				$cr_quantity = $row->cr_quantity;
				$dr_amount = $row->dr_amount;
				$cr_amount = $row->cr_amount;
				$transactionDate = $row->transactionDate;
				$transactionDate = date('F j, Y', strtotime($transactionDate));

				$total_dr_quantity += $dr_quantity;
				$total_dr_amount += $dr_amount;

				$count++;
				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $transactionDate;
				$col_count++;
				$report[$row_count][$col_count] = $product_name;
				$col_count++;
				$report[$row_count][$col_count] = $store_name;
				$col_count++;
				$report[$row_count][$col_count] = $transactionDescription;
				$col_count++;
				$report[$row_count][$col_count] = $dr_quantity;
				$col_count++;
				$report[$row_count][$col_count] = number_format($dr_amount,2);
				$col_count++;



			}

		}

		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	public function export_current_stock_report($report_id,$category_id)
	{

	    $search_status = $this->session->userdata('income_statement_search');
	    $search_payments_add = '';
	    $search_invoice_add = '';
	    if($search_status == 1)
	    {
	        $stock_search  = $this->session->userdata('stock_report_id#'.$report_id);
	        // var_dump($stock_search);die();

			if(!empty($stock_search) AND $stock_search == $report_id)
			{
		    	// $exploded = explode('#', $stock_search);

		    	$export_report_id = $report_id;

		      	$date_from = $this->session->userdata('date_from_stock'.$export_report_id);
				$date_to = $this->session->userdata('date_to_stock'.$export_report_id);
				// var_dump($date_from);die();
				if(!empty($date_from) AND !empty($date_to))
				{
					$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
				}
				else if(!empty($date_from))
				{
					$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
				}
				else if(!empty($date_to))
				{
					$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
				}

		  }
	      else
	      {
	      	$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
	      }

	    }
	    else
	    {
	    	// $stock_search  = $this->session->userdata('stock_report_id#'.$report_id);
	    	 $stock_search  = $this->session->userdata('stock_report_id#'.$report_id);
	        // var_dump($stock_search);die();

				if(!empty($stock_search) AND $stock_search == $report_id)
				{
			    	// $exploded = explode('#', $stock_search);

			    	$export_report_id = $report_id;

			      	$date_from = $this->session->userdata('date_from_stock'.$export_report_id);
					$date_to = $this->session->userdata('date_to_stock'.$export_report_id);
					// var_dump($date_from);die();
					if(!empty($date_from) AND !empty($date_to))
					{
						$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
					}
					else if(!empty($date_from))
					{
						$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
					}
					else if(!empty($date_to))
					{
						$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
					}

			  }
			else
			{
				$search_invoice_add = '';
			}


    	}

    	$this->load->library('excel');

    	$table = 'v_product_stock';
    	$where = '(v_product_stock.transactionClassification = "Product Opening Stock" OR  v_product_stock.transactionClassification = "Supplier Purchases" OR  v_product_stock.transactionClassification = "Product Addition" OR v_product_stock.transactionClassification = "Supplier Credit Note" OR v_product_stock.transactionClassification = "Product Deductions" OR v_product_stock.transactionClassification = "Drug Sales" OR v_product_stock.transactionClassification = "Drug Transfer") AND ( category_id = 2 OR category_id = 3)  '.$search_invoice_add;
    	$this->db->where($where);
		$this->db->order_by('transactionDate', 'ASC');
		$this->db->select('*');
		$visits_query = $this->db->get($table);

		$report_title = 'Current Stock';

		$title = $report_title.' Export '.date('jS M Y',strtotime(date($date_from))).' - '.date('jS M Y',strtotime(date($date_to)));
		$col_count = 0;

		if($visits_query->num_rows() > 0)
		{
			$count = 0;

			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Transaction Date';
			$col_count++;
			$report[$row_count][$col_count] = 'Product';
			$col_count++;
			$report[$row_count][$col_count] = 'Affected Store';
			$col_count++;
			$report[$row_count][$col_count] = 'Description';
			$col_count++;
			$report[$row_count][$col_count] = 'Dr QTY';
			$col_count++;
			$report[$row_count][$col_count] = 'Cr QTY';

			$col_count++;
			$report[$row_count][$col_count] = 'Dr Value';
			$col_count++;
			$report[$row_count][$col_count] = 'Cr Value';

			$col_count++;
			// $total_dr_quantity = 0;
			// $total_dr_amount = 0;

			$total_dr_quantity = 0;
			$total_dr_amount = 0;
			$total_cr_quantity = 0;
			$total_cr_amount = 0;
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
				$total_invoiced = 0;

				$transaction_id = $row->transactionId;
				$product_id = $row->product_id;
				$store_id = $row->store_id;
				$product_name = $row->product_name;
				$store_name = $row->store_name;
				$transactionDescription = $row->transactionDescription;
				$dr_quantity = $row->dr_quantity;
				$cr_quantity = $row->cr_quantity;
				$dr_amount = $row->dr_amount;
				$cr_amount = $row->cr_amount;
				$transactionDate = $row->transactionDate;
				$transactionDate = date('F j, Y', strtotime($transactionDate));




				$total_dr_quantity += $dr_quantity;
				$total_dr_amount += $dr_amount;
				$total_cr_quantity += $cr_quantity;
				$total_cr_amount += $cr_amount;


				$count++;
				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $transactionDate;
				$col_count++;
				$report[$row_count][$col_count] = $product_name;
				$col_count++;
				$report[$row_count][$col_count] = $store_name;
				$col_count++;
				$report[$row_count][$col_count] = $transactionDescription;
				$col_count++;
				$report[$row_count][$col_count] = $dr_quantity;
				$col_count++;
				$report[$row_count][$col_count] = $cr_quantity;
				$col_count++;
				$report[$row_count][$col_count] = number_format($dr_amount,2);
				$col_count++;
				$report[$row_count][$col_count] = number_format($cr_amount,2);
				$col_count++;



			}

		}

		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function get_all_stock_expense_statement_views($table, $where, $per_page, $page, $order = 'product_deductions_stock_date', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select("*");
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		// $this->db->join('personnel','personnel.personnel_id = visit_charge.provider_id','left');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}


	public function get_product_expense_balance_brought_forward()
	{

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$start_date = date('Y-m-01', strtotime($date_from));
				$search_invoice_add =  '   transactionDate < \''.$start_date.'\' ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = '  transactionDate < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = '  transactionDate < \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}
		// $table = 'v_product_stock';
		// $where = '(v_product_stock.transactionClassification = "Product Opening Stock" OR  v_product_stock.transactionClassification = "Supplier Purchases" OR  v_product_stock.transactionClassification = "Product Addition" OR v_product_stock.transactionClassification = "Supplier Credit Note" OR v_product_stock.transactionClassification = "Product Deductions" OR v_product_stock.transactionClassification = "Drug Sales")  '.$search_invoice_add;
		// $this->db->select('SUM(dr_amount) AS dr_amount, SUM(cr_amount)  AS cr_amount, SUM(dr_quantity) AS dr_quantity,SUM(cr_quantity) AS cr_quantity');


		$select_statement = "SELECT
								SUM(data.dr_amount) AS dr_amount,
								SUM(data.cr_amount) AS cr_amount,
								SUM(data.dr_quantity) AS dr_quantity,
								SUM(data.cr_quantity) AS cr_quantity
							   	FROM
								(
									SELECT
									`product_deductions_stock`.`product_deductions_stock_id` AS transactionId,
									`product_deductions_stock`.`product_id` AS `product_id`,
									`product`.`category_id` AS `category_id`,
									 product_deductions_stock.store_id AS `store_id`,
									 '' AS `receiving_store`,
									 product.product_name AS `product_name`,
									store.store_name AS `store_name`,
									 product_deductions_stock.deduction_description AS `transactionDescription`,
									 '0' AS `dr_quantity`,
									 (product_deductions_stock_quantity * product_deductions_stock_pack_size) AS  `cr_quantity`,
									'0' AS `dr_amount`,
									 (`product`.`product_unitprice` * (product_deductions_stock_quantity * product_deductions_stock_pack_size)) AS `cr_amount`,
									`product_deductions_stock`.`product_deductions_stock_date` AS `transactionDate`,
									`product`.`product_status` AS `status`,
									`product`.`product_deleted` AS `product_deleted`,
									`category`.`category_name` AS `transactionCategory`,
									'Product Deductions' AS `transactionClassification`,
									'product_deductions_stock' AS `transactionTable`,
									'product' AS `referenceTable`
									FROM (`product_deductions_stock`,product,store,category)
									WHERE product.product_id = product_deductions_stock.product_id AND product.product_deleted = 0
									AND store.store_id = product_deductions_stock.store_id
									AND product.category_id = category.category_id
									AND (product.category_id < 2 OR product.category_id > 2)
									AND store.store_parent > 0
								) AS data WHERE ".$search_invoice_add;;
		// $this->db->where($where);
		$query = $this->db->query($select_statement);



		return  $query;
	}


  public function get_unallocated_funds($creditor_id)
  {
    $selected_statement = "
                  SELECT
                      SUM(cr_amount) AS cr_amount

                  FROM
                  (
                  SELECT
                  `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                  `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                  `creditor_payment`.`reference_number` AS `referenceCode`,
                  `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                  `creditor_payment`.`document_number` AS `transactionCode`,
                  '' AS `patient_id`,
                  `creditor_payment`.`creditor_id` AS `recepientId`,
                  `account`.`parent_account` AS `accountParentId`,
                  `account_type`.`account_type_name` AS `accountsclassfication`,
                  `creditor_payment`.`account_from_id` AS `accountId`,
                  `account`.`account_name` AS `accountName`,
                  `creditor_payment_item`.`description` AS `transactionName`,
                  CONCAT('Payment on account')  AS `transactionDescription`,
                      0 AS `department_id`,
                  0 AS `dr_amount`,
                  `creditor_payment_item`.`amount_paid` AS `cr_amount`,
                  `creditor_payment`.`transaction_date` AS `transactionDate`,
                  `creditor_payment`.`created` AS `createdAt`,
                  `creditor_payment`.`created` AS `referenceDate`,
                  `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                  'Expense Payment' AS `transactionCategory`,
                  'Creditors Invoices Payments' AS `transactionClassification`,
                  'creditor_payment' AS `transactionTable`,
                  'creditor_payment_item' AS `referenceTable`
                FROM
                  (
                    (
                      (
                        `creditor_payment_item`
                        JOIN `creditor_payment` ON(
                          (
                            creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
                          )
                        )
                      )
                      JOIN account ON(
                        (
                          account.account_id = creditor_payment.account_from_id
                        )
                      )
                    )
                    JOIN `account_type` ON(
                      (
                        account_type.account_type_id = account.account_type_id
                      )
                    )
                    JOIN `creditor` ON(
                      (
                        creditor.creditor_id = creditor_payment_item.creditor_id
                      )
                    )
                  )
                  WHERE creditor_payment_item.invoice_type = 3) AS data WHERE data.recepientId = ".$creditor_id;
          $query = $this->db->query($selected_statement);
          $checked = $query->row();

          $dr_amount = $checked->cr_amount;

          return $dr_amount;
  	}


  	public function get_asset_categories()
	{
		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');


		}
		else
		{
			$date_to = date('Y-m-d');

		}




		$select_sql = 'SELECT
						  asset_amortization.amortizationDate,
						  SUM(asset_amortization.endBalance) as asset_value,
						  assets_details.asset_description,
						  assets_details.asset_category_id,
						  asset_category.asset_category_name
						FROM
						  asset_amortization
						  LEFT JOIN assets_details
						    ON asset_amortization.asset_id = assets_details.asset_id
						  LEFT JOIN asset_category
						    ON assets_details.asset_category_id = asset_category.asset_category_id
						WHERE  YEAR (
						    asset_amortization.amortizationDate
						  ) = YEAR("'.$date_to.'")
						GROUP BY assets_details.asset_category_id
						ORDER BY asset_category.asset_category_name ASC';
		$query = $this->db->query($select_sql);

		return $query;
	}

	public function get_creditor_amount_paid($creditor_invoice_id,$creditor_id)
	{
		 $this->db->where('creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1 AND (creditor_payment_item.invoice_type = 0 OR creditor_payment_item.invoice_type = 4) AND creditor_payment_item.creditor_invoice_id ='.$creditor_invoice_id.' AND creditor_payment_item.creditor_id = '.$creditor_id);
		 $this->db->select('SUM(creditor_payment_item.amount_paid) AS total_paid');
		 $query = $this->db->get('creditor_payment,creditor_payment_item');


		 $total_paid = 0;
		 if($query->num_rows() > 0)
		 {
		  foreach ($query->result() as $key => $value) {
		    # code...
		    $total_paid = $value->total_paid;
		  }
		 }
		 return $total_paid;
	}

	public function get_all_cash_accounts($table, $where, $config, $page, $order, $order_method)
	{
		//retrieve all accounts
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);

		return $query;
	}

	public function deactivate_account($account_id)
	{
		$this->db->where('account_id = '.$account_id);
		if($this->db->update('account',array('account_status'=>0)))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function activate_account($account_id)
	{
		$this->db->where('account_id = '.$account_id);
		if($this->db->update('account',array('account_status'=>1)))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_account($account_id)
	{
		$this->db->select('*');
		$this->db->where('account_id = '.$account_id);
		$query = $this->db->get('account');

		return $query->row();
	}

	public function update_account($account_id)
		{
			$account_data = array(
						'account_name'=>$this->input->post('account_name'),
						'account_type_id'=>$this->input->post('account_type_id'),
						'parent_account'=>$this->input->post('parent_account'),
						'account_opening_balance'=>$this->input->post('account_balance'),
						'paying_account'=>$this->input->post('paying_account'),
						'account_code'=>$this->input->post('account_code'),
						'start_date'=>$this->input->post('start_date')
						);
			$this->db->where('account_id = '.$account_id);
			if($this->db->update('account', $account_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		public function add_account()
		{
			$account = array(
						'account_name'=>$this->input->post('account_name'),
						'account_opening_balance'=>$this->input->post('account_balance'),
						'parent_account'=>$this->input->post('parent_account'),
						'account_type_id'=>$this->input->post('account_type_id'),
	                    'account_status'=>$this->input->post('account_status'),
	                    'paying_account'=>$this->input->post('paying_account'),
	                    'account_code'=>$this->input->post('account_code'),
						'start_date'=>$this->input->post('start_date')
						);
			if($this->db->insert('account',$account))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}

		public function get_account_id($account_name)
		{
			$account_id = 0;

			$this->db->select('account_id');
			$this->db->where('account_name = "'.$account_name.'"');
			$query = $this->db->get('account');

			$bal = $query->row();
			$account_id = $bal->account_id;
			// var_dump($account_id); die();
			return $account_id;

		}

		public function get_parent_account_id($account_name)
		{
			$account_id = 0;

			$this->db->select('account_id');
			$this->db->where('account_name = "'.$account_name.'"');
			$query = $this->db->get('account');

			$bal = $query->row();
			$account_id = $bal->account_id;
			// var_dump($account_id); die();
			return $account_id;

		}
		public function get_parent_account_by_id($account_id)
		{
			// $account_id = 0;

			$this->db->select('parent_account');
			$this->db->where('account_id = "'.$account_id.'"');
			$query = $this->db->get('account');

			$bal = $query->row();
			$account_id = $bal->parent_account;
			// var_dump($account_id); die();
			return $account_id;

		}
		public function get_account_opening_bal($account)
		{
			$opening_bal = 0;

			$this->db->select('account_opening_balance');
			$this->db->where('account_id = '.$account);
			$query = $this->db->get('account');

			$bal = $query->row();
			$opening_bal = $bal->account_opening_balance;

			return $opening_bal;

		}
		public function get_total_opening_bal()
		{
			$opening_bal = 0;

			$this->db->select('SUM(account_opening_balance) AS total_opening_bal');
			$query = $this->db->get('account');

			$bal = $query->row();
			$opening_bal = $bal->total_opening_bal;

			return $opening_bal;
		}
		 public function get_parent_accounts()
		{
			//retrieve all users
			$this->db->from('account');
			$this->db->select('*');
			$this->db->where('parent_account = 0');
			$query = $this->db->get();

			return $query;

	    }
	    public function get_type()
		{
			//retrieve all users
			$this->db->from('account_type');
			$this->db->select('*');
			$this->db->where('account_type_id > 0 ');
			$query = $this->db->get();

			return $query;

	    }

	    public function get_parent_account($parent_account)
	    {
	    	$this->db->from('account');
			$this->db->select('*');
			$this->db->where('account_id = '.$parent_account);
			$query = $this->db->get();
			$account_name = '';
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$account_name = $value->account_name;
				}
			}

			return $account_name;
	    }

	public function get_account_value_by_type($account_type_name)
	{

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			// $search_invoice_add =  ' AND transactionDate <= \''.date("Y-m-d").'\' ';

		}

		$search_status = $this->session->userdata('trail_budget_year');


		if($search_status)
		{
			$search_invoice_add = ' AND YEAR(transactionDate) = \''.$search_status.'\'';
		}
		else
		{
			$search_invoice_add = ' AND YEAR(transactionDate) = \''.date('Y').'\'';
		}



		//retrieve all users

		$this->db->from('v_account_ledger_by_date,account');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount,accountName,account_id');
		$this->db->where('v_account_ledger_by_date.accountId = account.account_id
			 AND v_account_ledger_by_date.accountsclassfication = "'.$account_type_name.'" '.$search_invoice_add);
		$this->db->group_by('v_account_ledger_by_date.accountId');
		$query = $this->db->get();

		return $query;

	}


	public function get_account_share_capital_by_type($account_type_name)
	{

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				// $search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
				$search_invoice_add =  ' AND (transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users

		$this->db->from('v_account_ledger_by_date,account');
		$this->db->select('(SUM(cr_amount) - SUM(dr_amount)) AS total_amount,accountName,account_id');
		$this->db->where('v_account_ledger_by_date.accountId = account.account_id
			 AND v_account_ledger_by_date.accountsclassfication = "'.$account_type_name.'" '.$search_invoice_add);
		$this->db->group_by('v_account_ledger_by_date.accountId');
		$query = $this->db->get();

		return $query;

	}

	public function get_payroll_list($table, $where, $per_page, $page, $order = 'v_payroll.payroll_created_for', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}


	public function export_salary()
	{

		$this->load->library('excel');


		$where = 'payroll_id > 0';

		$search_status = $this->session->userdata('income_statement_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_income_statement');
			$date_to = $this->session->userdata('date_to_income_statement');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (payroll_created_for >= \''.$date_from.'\' AND payroll_created_for <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND payroll_created_for = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND payroll_created_for = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add = '';

		}

		$where .= $search_invoice_add;
		//retrieve all users

		$table = 'v_payroll';


		$this->db->where($where);
		$this->db->select('*');
		$visits_query = $this->db->get($table);

		$title = 'Payroll Export '.date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$col_count = 0;

		if($visits_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/
			$row_count = 0;
			$report[$row_count][$col_count] = '#';
			$col_count++;
			$report[$row_count][$col_count] = 'Period Payroll';
			$col_count++;
			$report[$row_count][$col_count] = 'Amount';
			$col_count++;
			//display all patient data in the leftmost columns
			foreach($visits_query->result() as $row)
			{
				$row_count++;
					$period = date('M Y',strtotime($row->payroll_created_for));
				$total_payroll = $row->total_payroll;

				$total_payroll_amount += $total_payroll;
				$count++;


				//display the patient data
				$report[$row_count][$col_count] = $count;
				$col_count++;
				$report[$row_count][$col_count] = $period;
				$col_count++;
				$report[$row_count][$col_count] = round($total_payroll);
				$col_count++;




			}
		}

		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);


	}


	public function get_provider($provider_id)
	{
		//retrieve all users
		$this->db->from('provider');
		$this->db->select('*');
		$this->db->where('provider_id = '.$provider_id);
		$query = $this->db->get();

		return $query;
	}

	public function get_provider_statement_balance($provider_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate < \''.$date_from.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_to.'\'';
			}
		}
		else
		{

			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_provider_ledger_aging_by_date,provider');
		$this->db->select('SUM(`dr_amount`) AS dr_amount,SUM(cr_amount) AS cr_amount');
		$this->db->where('provider.provider_id = v_provider_ledger_aging_by_date.recepientId AND v_provider_ledger_aging_by_date.transactionDate >= provider.start_date AND recepientId = '.$provider_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}


	public function get_provider_statement($provider_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{

			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_provider_ledger_aging_by_date,provider');
		$this->db->select('*');
		$this->db->where('provider.provider_id = v_provider_ledger_aging_by_date.recepientId AND v_provider_ledger_aging_by_date.transactionDate >= provider.start_date AND recepientId = '.$provider_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}





	public function get_payroll_statement($creditor_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{

			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_payroll_ledger_aging_by_date,statutory_accounts');
		$this->db->select('*');
		$this->db->where('statutory_accounts.statutory_account_id = v_payroll_ledger_aging_by_date.recepientId AND v_payroll_ledger_aging_by_date.recepientId = '.$creditor_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}

	public function get_providers_aging_report()
	{
		//retrieve all users
		$provider_search =  $this->session->userdata('provider_search');

		if(!empty($provider_search))
		{
			$where = $provider_search;
		}
		else
		{
			$where = 'recepientId > 0';
		}
		$this->db->from('v_aged_providers');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('payables','ASC');
		$query = $this->db->get();

		return $query;
	}

	public function get_payroll_aging_report()
	{
		//retrieve all users
		$provider_search =  $this->session->userdata('provider_search');

		if(!empty($provider_search))
		{
			$where = $provider_search;
		}
		else
		{
			$where = 'recepientId > 0';
		}
		$this->db->from('v_aged_statutories');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('payables','ASC');
		$query = $this->db->get();

		return $query;
	}

	public function get_provider_amount_paid($provider_invoice_id,$provider_id)
	{
		 $this->db->where('provider_payment.provider_payment_id = provider_payment_item.provider_payment_id AND provider_payment.provider_payment_status = 1 AND provider_payment_item.invoice_type = 0 AND provider_payment_item.provider_invoice_id ='.$provider_invoice_id.' AND provider_payment_item.provider_id = '.$provider_id);
		 $this->db->select('SUM(provider_payment_item.amount_paid) AS total_paid');
		 $query = $this->db->get('provider_payment,provider_payment_item');


		 $total_paid = 0;
		 if($query->num_rows() > 0)
		 {
		  foreach ($query->result() as $key => $value) {
		    # code...
		    $total_paid = $value->total_paid;
		  }
		 }
		 return $total_paid;
	}

	public function get_payroll_amount_paid($payroll_invoice_id,$statutory_id,$type=NULL)
	{


		if(!empty($type))
		{
			$add = ' AND payroll_payment_item.invoice_type = '.$type;
		}
		else
		{
			$add = '';
		}
		 $this->db->where('payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id AND payroll_payment.payroll_payment_status = 1 AND payroll_payment_item.payroll_invoice_id ='.$payroll_invoice_id.' AND payroll_payment_item.payroll_id = '.$statutory_id.$add);
		 $this->db->select('SUM(payroll_payment_item.amount_paid) AS total_paid');
		 $query = $this->db->get('payroll_payment,payroll_payment_item');


		 $total_paid = 0;
		 if($query->num_rows() > 0)
		 {
		  foreach ($query->result() as $key => $value) {
		    # code...
		    $total_paid = $value->total_paid;
		  }
		 }
		 return $total_paid;
	}

	public function get_payroll_credit_note($payroll_invoice_id,$statutory_id,$type=NULL)
	{


		if(!empty($type))
		{
			$add = ' AND payroll_credit_note.credit_note_type_id = '.$type;
		}
		else
		{
			$add = '';
		}
		 $this->db->where('payroll_credit_note.statutory_credit_note_id = payroll_credit_note_item.statutory_credit_note_id AND payroll_credit_note.statutory_credit_note_status = 1 AND payroll_credit_note_item.statutory_invoice_id ='.$payroll_invoice_id.' AND payroll_credit_note_item.statutory_id = '.$statutory_id.$add);
		 $this->db->select('SUM(payroll_credit_note_item.credit_note_amount) AS total_paid');
		 $query = $this->db->get('payroll_credit_note,payroll_credit_note_item');


		 $total_paid = 0;
		 if($query->num_rows() > 0)
		 {
		  foreach ($query->result() as $key => $value) {
		    # code...
		    $total_paid = $value->total_paid;
		  }
		 }
		 return $total_paid;
	}

	public function get_all_patients_invoices($table, $where)
	{
		//retrieve all users
		$this->db->from($table);

		$this->db->select('v_transactions_by_date.*, patients.*, payment_method.*, personnel.personnel_fname, personnel.personnel_onames,visit_invoice.visit_invoice_number,branch.branch_name,branch.branch_code,visit.visit_id,visit_invoice.visit_invoice_id');
		$this->db->join('patients', 'patients.patient_id = v_transactions_by_date.patient_id', 'left');
		$this->db->join('payment_method', 'payment_method.payment_method_id = v_transactions_by_date.payment_method_id', 'left');
		$this->db->join('payments', 'payments.payment_id = v_transactions_by_date.transaction_id', 'left');
		$this->db->join('visit', 'visit.visit_id = visit_invoice.visit_id', 'left');
		$this->db->join('personnel', 'visit.personnel_id = personnel.personnel_id', 'left');
		$this->db->join('branch', 'branch.branch_id = v_transactions_by_date.branch_id', 'left');

		$this->db->where($where);
		$this->db->order_by('v_transactions_by_date.created_at','DESC');


		$query = $this->db->get('');

		return $query;
	}
	public function get_parent_accounts_by_type($account_type_name)
	{
		$this->db->where('account.account_type_id = account_type.account_type_id AND account_type.account_type_name = "'.$account_type_name.'" AND account.parent_account = 0 AND account.account_deleted = 0');
		$query = $this->db->get('account,account_type');


		return $query;
	}

	public function get_child_accounts($parent_account_name)
    {
    	$this->db->from('account');
		$this->db->select('*');
		$this->db->where('account_name = "'.$parent_account_name.'" AND account.account_status = 1');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$account_id = $value->account_id;
			}
			//retrieve all users
			$this->db->from('account');
			$this->db->select('*');
			$this->db->where('parent_account = '.$account_id.' AND account.account_status = 1');
			$query = $this->db->get();

			return $query;


		}
		else
		{
			return FALSE;
		}

    }

	public function get_total_expense_amount($account_id)
    {
    	$search_status = $this->session->userdata('balance_sheet_search');
		$search_add = '';
		$search_payment_add ='';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_add =  ' AND (created >= \''.$date_from.'\' AND created <= \''.$date_to.'\') ';
				$search_payment_add =  ' AND (payment_date >= \''.$date_from.'\' AND payment_date <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_add = ' AND created = \''.$date_from.'\'';
				$search_payment_add = ' AND payment_date = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_add = ' AND created = \''.$date_to.'\'';
				$search_payment_add = ' AND payment_date = \''.$date_to.'\'';
			}
		}


    	$this->db->from('account_invoices');
		$this->db->select('SUM(invoice_amount) AS total_paid');
		$this->db->where('account_invoice_deleted = 0 AND account_to_id = '.$account_id.''.$search_add);
		$query = $this->db->get();
		$total_paid  =0 ;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_paid = $value->total_paid;
			}
		}
		return $total_paid;
    }


	public function get_child_accounts_by_parent_id($parent_id)
	{
		$this->db->where('account.parent_account = '.$parent_id.' AND account.account_deleted = 0');
		$query = $this->db->get('account');


		return $query;
	}


	public function get_assets_child_accounts_by_parent_id($parent_id)
	{
		$this->db->select('account.*,assets_details.*,asset_category.asset_category_name');
		$this->db->where('account.parent_account = '.$parent_id.' AND account.account_deleted = 0 AND account.account_id = asset_category.account_id AND assets_details.asset_category_id = asset_category.asset_category_id');
		$query = $this->db->get('account,asset_category,assets_details');


		return $query;
	}



	public function get_account_value_by_account_id($account_id)
	{

		$search_status = $this->session->userdata('balance_sheet_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_balance_sheet');
			$date_to = $this->session->userdata('date_to_balance_sheet');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users

		$this->db->from('v_account_ledger_by_date,account');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount,accountName,account_id');
		$this->db->where('v_account_ledger_by_date.accountId = '.$account_id.' AND v_account_ledger_by_date.accountId = account.account_id AND account.account_status = 1  '.$search_invoice_add);
		$this->db->group_by('v_account_ledger_by_date.accountId');
		$query = $this->db->get();

		$total_amount = 0;
		if($query->num_rows() > 0 )
		{
			foreach($query->result() AS $key => $value)
			{
				$total_amount = $value->total_amount;
			}
		}



		return $total_amount;

	}
	public function get_all_account_types()
	{
		$this->db->where('account_type_id > 0');
		$query = $this->db->get('account_type');
		return $query;
	}

	public function get_statutory($statutory_id)
	{
		//retrieve all users
		$this->db->from('statutory_accounts');
		$this->db->select('*');
		$this->db->where('statutory_account_id = '.$statutory_id);
		$query = $this->db->get();

		return $query;
	}

  	public function get_profit_and_loss()
	{
		$services_result = $this->company_financial_model->get_all_service_types();
		$service_result = '';
		$total_service_invoice = 0;
		$total_service_payment = 0;
		$total_service_balance = 0;
		$total_payments = $this->get_total_payments_collected();


		$total_purchases = $this->company_financial_model->get_total_purchases();
		$total_stock_value = $this->company_financial_model->get_stock_value();
		$total_expenses = $this->company_financial_model->get_total_expenses();
		$total_profit = $total_stock_value+$total_payments + $total_purchases - $total_expenses;

		return $total_profit;
	}

	public function get_all_staging_accounts()
	{

		$this->db->from('account_staging');
		$this->db->select('account_staging.*,account.account_name');
		$this->db->join('account','account_staging.account_id = account.account_id','LEFT');
		$this->db->where('account_staging_delete = 0');
		$query = $this->db->get();

		return $query;

	}

	public function get_all_accounts($account_id = NULL)
	{
	  if(!empty($account_id))
	  {
	    $add = ' AND account_id <> '.$account_id;
	  }
	  else
	  {
	    $add ='';
	  }
	  $this->db->from('account');
	  $this->db->select('*');
	  $this->db->where('parent_account <> 0 AND account_deleted = 0'.$add);
	  $this->db->order_by('parent_account','ASC');
	  $query = $this->db->get();

	   return $query;

	}


	public function get_staging_accounts($report_status = 0)
	{

		if(!empty($report_status))
		{
			$add = ' AND account_staging.bs_account_balance_report = '.$report_status;
		}
		else
		{
			$add =  '';
		}

		$this->db->from('account_staging');
		$this->db->select('account_staging.*');
		$this->db->where('account_staging_delete = 0'.$add);
		$query = $this->db->get();

		return $query;

	}


	// loans


	public function get_loan($loan_id)
	{
		//retrieve all users
		$this->db->from('loan');
		$this->db->select('*');
		$this->db->where('loan_id = '.$loan_id);
		$query = $this->db->get();

		return $query;
	}

	public function get_loan_statement_balance($loan_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate < \''.$date_from.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_to.'\'';
			}
		}
		else
		{

			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_loan_ledger_aging_by_date,loan');
		$this->db->select('SUM(`dr_amount`) AS dr_amount,SUM(cr_amount) AS cr_amount');
		$this->db->where('loan.loan_id = v_loan_ledger_aging_by_date.recepientId AND v_loan_ledger_aging_by_date.transactionDate >= loan.start_date AND recepientId = '.$loan_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}


	public function get_loan_statement($provider_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{

			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_loan_ledger_aging_by_date,loan');
		$this->db->select('*');
		$this->db->where('loan.loan_id = v_loan_ledger_aging_by_date.recepientId AND v_loan_ledger_aging_by_date.transactionDate >= loan.start_date AND recepientId = '.$provider_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}

	public function get_loan_statement_units($loan_id,$limit=null)
	{
	    // $this->db->where('v_loans_invoice_balances.loan_id = '.$loan_id);
	    // $this->db->select('*');
	    // return $this->db->get('v_loans_invoice_balances');

	    $select_statement = "
	                        SELECT
	                          data.invoice_id AS loan_invoice_id,
	                          data.invoice_number AS invoice_number,
	                          data.invoice_date AS invoice_date,
	                          data.loan_invoice_type AS loan_invoice_type,
	                          COALESCE (SUM(data.loan_amount),0) AS loan_amount,
	                          COALESCE (SUM(data.dr_amount),0) AS dr_amount,
	                          COALESCE (SUM(data.principle_payment),0) AS principle_payment,
	                          COALESCE (SUM(data.interest_payment),0) AS interest_payment,
	                          COALESCE (SUM(data.cr_amount),0) AS cr_amount,
	                          COALESCE (SUM(data.dr_amount),0) AS balance
	                        FROM
	                        (


	                            SELECT
	                            `loan_invoice`.`loan_id` AS loan_id,
	                            `loan_invoice`.`loan_invoice_id` AS invoice_id,
	                            `loan_invoice`.`invoice_number` AS invoice_number,
	                            `loan_invoice`.`transaction_date` AS invoice_date,
	                            'loan Bills' AS loan_invoice_type,
	                            COALESCE (SUM(`loan_invoice_item`.`total_amount`),0) AS loan_amount,
	                            COALESCE (SUM(`loan_invoice_item`.`total_amount`),0) AS dr_amount,
	                            0 AS principle_payment,
	                            0 AS interest_payment,
	                            0 AS cr_amount
	                            FROM (`loan_invoice`,loan_invoice_item,loan)
	                            WHERE `loan_invoice_item`.`loan_invoice_id` = `loan_invoice`.`loan_invoice_id` AND loan_invoice.loan_invoice_status = 1 AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
	                             GROUP BY `loan_invoice`.`loan_id`

	                            UNION ALL

	                            SELECT
	                            `loan_invoice`.`loan_id` AS loan_id,
	                            `loan_invoice`.`loan_invoice_id` AS invoice_id,
	                            `loan_invoice`.`invoice_number` AS invoice_number,
	                            `loan_invoice`.`transaction_date` AS invoice_date,
	                            'loan Bills Credit Note' AS loan_invoice_type,
	                            0 AS loan_amount,
	                            0 AS dr_amount,
	                            0 AS principle_payment,
	                            0 AS interest_payment,
	                            COALESCE (SUM(`loan_credit_note_item`.`credit_note_amount`),0) AS cr_amount
	                            FROM (`loan_invoice`,loan_credit_note,loan_credit_note_item)
	                            WHERE `loan_credit_note_item`.`loan_credit_note_id` = `loan_credit_note`.`loan_credit_note_id`
	                            AND `loan_invoice`.`loan_invoice_id` = `loan_credit_note_item`.`loan_invoice_id` AND loan_credit_note.loan_credit_note_status = 1
	                             GROUP BY `loan_invoice`.`loan_id`
	                            UNION ALL


	                            SELECT
	                            `loan_invoice`.`loan_id` AS loan_id,
	                            `loan_invoice`.`loan_invoice_id` AS invoice_id,
	                            `loan_invoice`.`invoice_number` AS invoice_number,
	                            `loan_invoice`.`transaction_date` AS invoice_date,
	                            'Bill Payments' AS loan_invoice_type,
	                            0 AS loan_amount,
	                            0 AS dr_amount,
	                            COALESCE (SUM(`loan_payment_item`.`amount_paid`),0) AS principle_payment,
	                            0 AS interest_payment,
	                            0 AS cr_amount
	                            FROM (loan_payment_item,loan_payment,loan_invoice,loan)
	                            WHERE `loan_payment_item`.`loan_invoice_id` = `loan_invoice`.`loan_invoice_id`
	                            AND `loan_payment_item`.`loan_payment_id` = `loan_payment`.`loan_payment_id` AND loan_payment_item.invoice_type = 0 AND loan_payment.loan_payment_status = 1 AND `loan_payment_item`.`loan_id` = `loan`.`loan_id` AND loan.account_from_id = loan_payment_item.account_to_id
	                             GROUP BY `loan_invoice`.`loan_id`


	                             UNION ALL


	                            SELECT
	                            `loan_invoice`.`loan_id` AS loan_id,
	                            `loan_invoice`.`loan_invoice_id` AS invoice_id,
	                            `loan_payment`.`reference_number` AS invoice_number,
	                            `loan_invoice`.`transaction_date` AS invoice_date,
	                            'Bill Payments' AS loan_invoice_type,
	                            0 AS loan_amount,
	                            0 AS dr_amount,
	                            0 AS principle_payment,
	                            COALESCE (SUM(`loan_payment_item`.`amount_paid`),0) AS interest_payment,
	                            0 AS cr_amount
	                            FROM (loan_payment_item,loan_payment,loan_invoice,loan)
	                            WHERE `loan_payment_item`.`loan_invoice_id` = `loan_invoice`.`loan_invoice_id`
	                            AND `loan_payment_item`.`loan_payment_id` = `loan_payment`.`loan_payment_id` AND loan_payment_item.invoice_type = 0 AND loan_payment.loan_payment_status = 1 AND `loan_payment_item`.`loan_id` = `loan`.`loan_id` AND loan.account_from_id <> loan_payment_item.account_to_id
	                            GROUP BY `loan_invoice`.`loan_id`





	                          ) AS data,loan WHERE data.loan_id = ".$loan_id." AND data.loan_id = loan.loan_id AND data.invoice_date >= loan.start_date   GROUP BY data.invoice_id ORDER BY data.invoice_date ASC ";
	                          $query = $this->db->query($select_statement);
	                  return $query;


	  }

	public function update_retained_earnings()
	{
	  	$end_month = '12';
		$end_day = '31';

		$account_report_rs = $this->company_financial_model->get_staging_accounts(2);

		$arrOrphansIncome = array();
		if($account_report_rs->num_rows() > 0)
		{

			foreach ($account_report_rs->result() as $key => $value) {
				// code...
				$account_id = $value->account_id;
				$has_children = $value->has_children;


				if($has_children)
				{

					// check if account has children
					$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($account_id);


					if($children_account->num_rows() > 0)
					{

						foreach ($children_account->result() as $key_two => $value_two) {
							// code...
							$child_account_id = $value_two->account_id;
							$arrOrphansIncome[$child_account_id] =  $child_account_id;
						}


					}
					else
					{
						$arrOrphansIncome[$value->account_id] =  $value->account_id;
					}
				}
				else
				{
					$arrOrphansIncome[$value->account_id] =  $value->account_id;
				}


			}

		}

		$arrBranchesIncome = array();
		foreach($arrOrphansIncome as $serialIncome){
			$arrSerial = explode(".", $serialIncome);
			$new_serial = "";
			foreach($arrSerial as $part){
				$new_serial .= (strlen($new_serial)>0?".":"") . $part;

				if(!in_array($new_serial, $arrBranchesIncome))
					array_push($arrBranchesIncome, $new_serial);
			}
		}

		$max_rs = $this->ledgers_model->get_transactions_profit_and_loss($arrBranchesIncome);

		// var_dump($max_rs->result());die();

		$grouped_array_years = array();

		if($max_rs->num_rows() > 0)
		{
			foreach ($max_rs->result() as $key => $value_four) {
				// code...
				$max_transaction_date = $value_four->max_transaction_date;
				$min_transaction_date = $value_four->min_transaction_date;
			}

			if(!empty($min_transaction_date))
			{
				// explode(delimiter, string)

				$explode_min = explode('-', $min_transaction_date);
				$loop_min = $explode_min[0];
				$year_explode = $explode_min[0] -1;
				$month_explode = $explode_min[1];

				$loop_years = date('Y') - $loop_min;

					// var_dump($year_explode);die();
				$year_start = $year_explode;
				for ($k=0; $k <= $loop_years; $k++)
				{
					// var_dump($k);die();
					// code...
					$year_start = $year_explode + $k;
					// $start_year = $i-1;
					$start_date = $year_start.'-'.$end_month.'-'.$end_day;
					$next_date = $year_start+1;
					$end_date = $next_date.'-'.$end_month.'-'.$end_day;


					$query_four = $this->ledgers_model->get_transactions_profit_and_loss($arrBranchesIncome,1,$start_date,$end_date);


					foreach ($query_four->result() as $element_four) {
					    $grouped_array_years[$element_four->accountId][] = $element_four;
					}
					var_dump($year_start);

					$this->db->where('bs_account_status = 1 AND bs_account_type_id > 0  AND account_staging_delete = 0 AND bs_account_income_status > 0');
					$this->db->order_by('bs_account_position','ASC');
					$accounts_staging_rs = $this->db->get('account_staging');
					// var_dump($accounts_staging_rs->result());die();
					$total_income = 0;
					$total_debits = 0;
					$total_credits = 0;
					if($accounts_staging_rs->num_rows() > 0)
					{
						foreach ($accounts_staging_rs->result() as $key => $value) {
							// code...
							$staing_account_id = $value->account_id;
							$account_staging_name = $value->account_staging_name;
							$bs_account_type_id = $value->bs_account_type_id;
							$has_children = $value->has_children;
							$bs_account_balance_report = $value->bs_account_balance_report;
							$bs_account_income_status = $value->bs_account_income_status;

							if($bs_account_type_id == 3 AND $has_children == 1 AND $bs_account_balance_report == 2)
							{



								$children_account = $this->company_financial_model->get_child_accounts_by_parent_id($staing_account_id);


								if($children_account->num_rows() > 0)
								{

									foreach ($children_account->result() as $key => $value) {
										# code...
										$account_name = $value->account_name;
										$account_id = $value->account_id;


										// get the account opening balance


										$first_array = count($grouped_array_years[$account_id]);
										$debit = 0;
										$credit = 0;
										$total_amount = 0;
										if($first_array > 0 OR $second_array > 0)
										{

											for ($i=0; $i < $first_array; $i++) {


										  		$dr_amount = $grouped_array_years[$account_id][$i]->dr_amount;
											    $cr_amount = $grouped_array_years[$account_id][$i]->cr_amount;
											    $total_amount = $dr_amount - $cr_amount;

											}
										   	// get the account opening balance

											if($total_amount > 0)
											{
												$debit = $total_amount;
												$total_debits += $debit;
											}
											else
											{
												$credit = -$total_amount;
												$total_credits += $credit;
											}




										}
									}
								}

							}
						}
					}




					if(empty($total_debits))
					{
						$total_debits = 0;
					}

					if(empty($total_credits))
					{
						$total_credits = 0;
					}

					// check if transactions exisits

					$start_date = date('Y-m-d', strtotime('+1 day', strtotime($start_date)));
					// var_dump($start_date);die();
					$explode_year = explode('-',$start_date);
					$earnings_year = $explode_year[0];

					$this->db->where('year = "'.$earnings_year.'"');
					$query = $this->db->get('retained_earnings');
					// var_dump($query);die();

					if($query->num_rows() > 0)
					{
						$array_update_earnings['year'] = $earnings_year;
						$array_update_earnings['start_date'] = $start_date;
						$array_update_earnings['end_date'] = $end_date;
						$array_update_earnings['total_debits'] = $total_debits;
						$array_update_earnings['total_credits'] = $total_credits;

						$array_update_earnings['profit'] = $total_credits - $total_debits;
						$this->db->where('year = "'.$earnings_year.'"');
						$this->db->update('retained_earnings',$array_update_earnings);
					}
					else
					{
						$array_insert_earnings['year'] = $earnings_year;
						$array_insert_earnings['start_date'] = $start_date;
						$array_insert_earnings['end_date'] = $end_date;
						$array_insert_earnings['total_debits'] = $total_debits;
						$array_insert_earnings['total_credits'] = $total_credits;

						$array_insert_earnings['profit'] = $total_credits - $total_debits;
						// var_dump($array_insert_earnings);die();
						$this->db->insert('retained_earnings',$array_insert_earnings);
					}

				}
			}
		}
	}


	public function get_retained_earning($year=NULL,$linked_item=NULL)
	{


		if(!empty($year) AND $linked_item == NULL)
		{
			$this->db->where('year',$year);
			$query = $this->db->get('retained_earnings');

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					// code...
					$profit = $value->profit;
				}
			}
			return $profit;
		}
		else if(!empty($year) AND $linked_item == NULL)
		{
			$this->db->select('SUM(profit) AS total_profit');
			$query_changes = $this->db->get('retained_earnings');

			if($query_changes->num_rows() > 0)
			{
				foreach ($query_changes->result() as $key => $value_two) {
					// code...
					$profit = $value_two->total_profit;
				}
			}
			if(empty($profit))
			{
				$profit = 0;
			}
			return $profit;
		}
		else if(!empty($year) AND $linked_item == 1)
		{

			$this->db->where('year <= "'.$year.'"');
			$this->db->select('SUM(profit) AS total_profit');
			$query_changes = $this->db->get('retained_earnings');

			if($query_changes->num_rows() > 0)
			{
				foreach ($query_changes->result() as $key => $value_two) {
					// code...
					$profit = $value_two->total_profit;
				}
			}
			if(empty($profit))
			{
				$profit = 0;
			}
			return $profit;

		}



	}
	public function get_account_transactions_ledgers_new($args=NULL)
	{
		// echo "<pre>" . print_r($args) . "</pre>";
		$end_month = $this->session->userdata('end_of_year_month');
		$end_day = $this->session->userdata('end_of_year_day');

		$this->db->where('branch_id',2);
		$query = $this->db->get('branch');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$end_month = $value->end_of_year_month;
				$end_day = $value->end_of_year_day;
			}
		}



		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();

		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;


			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];
		$provider_opening_balance_id = $session_account['provider_opening_balance_id'];
		$loan_liability_account_id = $session_account['loan_liability_account_id'];
		$suppliers_deductions_id = $session_account['suppliers_deductions_id'];
		$suppliers_purchase_id = $session_account['suppliers_purchase_id'];



		$add = '';




		$date_from = $args["dateFrom"];
		$date_to = $args["dateTo"];
    	$add =  ' AND DATE(data.transactionDate) >= "'.$date_from.'" AND DATE(data.transactionDate) <= "'.$date_to.'" ';
    	// $add =  ' AND DATE(data.transactionDate) >= "2010-01-01" AND DATE(data.transactionDate) <= "2022-12-31" ';

		$item = '*';
		$add_item = '';


		$include_wht = false;
		$include_asset_depreciations = false;
		$include_assets_bills = true;



		$include_loan_invoices = true;
		$include_loan_payments = true;

		$include_receivable_refunds = true;
		$include_receivable_payments = true;
		$include_receivable_journals = true;
		$include_income = true;
		$include_income_credit_note = true;
		$include_journals = true;
		$include_batch_overpayments = true;

		
		$include_customer_income = true;
		$include_customer_payment = true;


		$include_inventory = true;



		$include_bank_recons = true;
		$include_expenses = true;


		$include_petty_cash = true;


		$include_transfers = true;
		$include_account_opening_balances = true;

		$include_direct_payments = true;


		$include_provider_invoices = true;
		$include_provider_payments = true;
		$include_provider_credit_notes = true;
		$include_provider_opening_balance = true;

		$include_creditor_invoices = true;
		$include_creditor_credit_note = true;
		$include_creditor_opening_balance = true;
		$include_creditor_payments = true;

		$include_payroll_invoice = true;
		$include_payroll_payments = true;
		$include_payroll_liabilities = true;
		$include_payroll_liabilities_payments = true;



		$configuration_rs = $this->site_model->get_company_configuration();

		$configuration_array = array();
		foreach ($configuration_rs->result() as $key) {
			// code...
			$configuration_array[$key->company_configuration_name] = $key;
		}

		if(isset($configuration_array['session_pnl']))
			$session_pnl = $configuration_array['session_pnl']->company_configuration_status;
		else
			$session_pnl = false;

		
		// var_dump($session_pnl);die();
		// $session_pnl = true;//$this->session->userdata('session_pnl');
		$include_income_with_collections = false;
		if(isset($session_pnl) AND $session_pnl == 'true')
		{

			$include_income = false;

			$include_receivable_payments = false;
			$include_receivable_non_cash_payments = true;
			$include_income_with_collections = true;
			
		}
		else
		{
			
			$include_income = true;

			$include_receivable_payments = true;
			$include_receivable_non_cash_payments = false;
			$include_income_with_collections = false;
		}


		$forOpeningTB = (is_array($args) and array_key_exists("forOpeningTB",$args) and $args["forOpeningTB"] == true);
		$forBalanceSheet = (is_array($args) and array_key_exists("balance_sheet",$args) and $args["balance_sheet"] == true);
		$for_pnl = (is_array($args) and array_key_exists("for_pnl", $args) and $args["for_pnl"] == true);
		$for_doctors_pnl = (is_array($args) and array_key_exists("for_doctors_pnl", $args) and $args["for_doctors_pnl"] == true);
		$forDetailedPnl = (is_array($args) and array_key_exists("forDetailedPnl", $args) and $args["forDetailedPnl"] == true);
		$for_cashflow_pnl = (is_array($args) and array_key_exists("forCashFlowPnl", $args) and $args["forCashFlowPnl"] == true);
		// echo "<br>Data being sought: " . ($for_pnl ? "For Pnl" : "Other data") . ", Opening TB: " . ($forOpeningTB ? "T" : "F") . ", Bal" . ($forBalanceSheet ? "T" : "F"). ", Doctors" . ($for_doctors_pnl ? "T" : "F");
		// echo "<pre>Arguments of data to provide";
		// var_dump($for_cashflow_pnl);die();
		// echo "</pre>";

		if($forOpeningTB){

			$include_wht = false;
			$include_asset_depreciations = false;
			$include_assets_bills = false;

			$include_loan_invoices = false;
			$include_loan_payments = false;

			$include_receivable_payments = false;
			$include_receivable_journals = false;
			$include_income = false;
			$include_income_credit_note = false;
			$include_journals = false;
			$include_income_with_collections = false;
			$include_receivable_non_cash_payments = false;
			$include_batch_overpayments = false;


			$include_customer_income = false;
			$include_customer_payment = false;


			$include_bank_recons = false;
			$include_expenses = false;


			$include_petty_cash = false;


			$include_transfers = false;

			$include_direct_payments = false;


			$include_provider_invoices = false;
			$include_provider_payments = false;
			$include_provider_credit_notes = false;

			$include_creditor_invoices = false;
			$include_creditor_credit_note = false;
			$include_creditor_payments = false;

			$include_payroll_invoice = false;
			$include_payroll_payments = false;






		}
		else if($forBalanceSheet){

			// loans



		}

		else if($for_pnl OR $for_doctors_pnl OR $for_cashflow_pnl)
		{
			// (is_array($args) ? var_dump($args): "");
			$include_bank_recons = false;
			$include_account_opening_balances = false;
			$include_transfers = false;
			$include_provider_payments = false;
			$include_provider_opening_balance = false;
			$include_creditor_opening_balance = false;
			$include_creditor_payments = false;
			$include_payroll_payments = false;
			$include_loan_invoices = false;
			// $include_loan_payments = false;

			if($for_doctors_pnl)
			{

			}
			else
			{
				$include_receivable_payments = false;
				$include_batch_overpayments = false;
				$include_journals = false;
				
			}

			// var_dump($args);die();


		}
		else if($forDetailedPnl)
		{
			$include_bank_recons = false;
			$include_account_opening_balances = false;
			$include_transfers = false;
			$include_provider_payments = false;
			$include_provider_opening_balance = false;
			$include_creditor_opening_balance = false;
			$include_creditor_payments = false;
			$include_payroll_payments = false;
			$include_loan_invoices = false;
			// $include_loan_payments = false;
			$include_receivable_payments = false;
			$include_batch_overpayments = false;
			$include_journals = false;

			$reportingPeriod = $args['reportingPeriod'];



			$item = '*,YEAR(data.transactionDate) AS year_start,MONTH(data.transactionDate) AS month_start';

			if(!empty($reportingPeriod)){
				$reportingPeriod = date("Y-m-d",$reportingPeriod);

				$year = date("Y",strtotime($reportingPeriod));
				$month = date("m",strtotime($reportingPeriod));

				$add =  ' AND YEAR(data.transactionDate) = "'.$year.'" AND MONTH(data.transactionDate) = "'.$month.'" ';
			}
			else
				$add = '';



		}

		// var_dump($for_doctors_pnl);die();
		$shareholder_id = 1;//$this->session->userdata('shareholder_id');
		if(empty($shareholder_id))
		{
			$shareholder_id = 0;
		}
		if($for_doctors_pnl)
		{

			$add = ' AND data.shareholder_id = 1';
		}

		$select  = "
					SELECT
						".$item.",
						account.parent_account AS `accountParentId`,
						account_type.account_type_id AS account_type_id,
						account_type.account_type_name AS account_type_name,
						account.account_name AS accountName
					FROM

					(

					";



					$select .="


								SELECT
									`creditor_opening_balance`.`creditor_opening_balance_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									".$accounts_payable_id." AS `accountId`,
									0 AS `accountFromId`,
									opening_balance_date AS `transactionName`,
									opening_balance_date AS `transactionDescription`,
									`creditor_opening_balance`.`balance` AS `amount`,
									`creditor_opening_balance`.`opening_balance_date` AS `transactionDate`,
									`creditor_opening_balance`.`opening_balance_date` AS `createdAt`,
									 1 AS `status`,
									 2 AS `branch_id`,
									 ".$shareholder_id." AS `shareholder_id`,
									'Creditor Opening Balance' AS `transactionCategory`,
									'Opening Balance' AS `transactionClassification`,
									'' AS `transactionTable`,
									'account' AS `referenceTable`,
									'' AS link
								FROM
								creditor_opening_balance

							WHERE

							 	creditor_opening_balance.balance <> 0";

					$select .="
								UNION ALL

								SELECT
									`provider_opening_balance`.`provider_opening_balance_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									".$providers_liability_id." AS `accountId`,
									0 AS `accountFromId`,
									opening_balance_date AS `transactionName`,
									opening_balance_date AS `transactionDescription`,
									`provider_opening_balance`.`balance` AS `amount`,
									`provider_opening_balance`.`opening_balance_date` AS `transactionDate`,
									`provider_opening_balance`.`opening_balance_date` AS `createdAt`,
									 1 AS `status`,
									 2 AS `branch_id`,
									 ".$shareholder_id." AS `shareholder_id`,
									'Provider Opening Balance' AS `transactionCategory`,
									'Opening Balance' AS `transactionClassification`,
									'' AS `transactionTable`,
									'account' AS `referenceTable`,
									'' AS link
								FROM
								provider_opening_balance

							WHERE

							 	provider_opening_balance.balance <> 0";

					$select .="
								UNION ALL

								SELECT
									`loan_opening_balance`.`loan_opening_balance_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									".$loan_liability_account_id." AS `accountId`,
									0 AS `accountFromId`,
									opening_balance_date AS `transactionName`,
									opening_balance_date AS `transactionDescription`,
									`loan_opening_balance`.`balance` AS `amount`,
									`loan_opening_balance`.`opening_balance_date` AS `transactionDate`,
									`loan_opening_balance`.`opening_balance_date` AS `createdAt`,
									 1 AS `status`,
									 2 AS `branch_id`,
									 ".$shareholder_id." AS `shareholder_id`,
									'Loan Opening Balance' AS `transactionCategory`,
									'Opening Balance' AS `transactionClassification`,
									'' AS `transactionTable`,
									'account' AS `referenceTable`,
									'' AS link
								FROM
								loan_opening_balance

							WHERE

							 	loan_opening_balance.balance <> 0";

					$select .="
								UNION ALL

								SELECT
									`payroll_opening_balance`.`payroll_opening_balance_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									".$payroll_liability_id." AS `accountId`,
									0 AS `accountFromId`,
									opening_balance_date AS `transactionName`,
									opening_balance_date AS `transactionDescription`,
									`payroll_opening_balance`.`balance` AS `amount`,
									`payroll_opening_balance`.`opening_balance_date` AS `transactionDate`,
									`payroll_opening_balance`.`opening_balance_date` AS `createdAt`,
									 1 AS `status`,
									 2 AS `branch_id`,
									 ".$shareholder_id." AS `shareholder_id`,
									'Payroll Opening Balance' AS `transactionCategory`,
									'Opening Balance' AS `transactionClassification`,
									'' AS `transactionTable`,
									'account' AS `referenceTable`,
									'' AS link
								FROM
								payroll_opening_balance

							WHERE

							 	payroll_opening_balance.balance <> 0";


					$select .="
								UNION ALL

								SELECT
									`account_opening_balance`.`account_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									account_opening_balance.account_id AS `accountId`,
									0 AS `accountFromId`,
									opening_balance_date AS `transactionName`,
									opening_balance_date AS `transactionDescription`,
									`account_opening_balance`.`balance` AS `amount`,
									`account_opening_balance`.`opening_balance_date` AS `transactionDate`,
									`account_opening_balance`.`opening_balance_date` AS `createdAt`,
									 1 AS `status`,
									 2 AS `branch_id`,
									 ".$shareholder_id." AS `shareholder_id`,
									'Account Opening Balance' AS `transactionCategory`,
									'Opening Balance' AS `transactionClassification`,
									'' AS `transactionTable`,
									'account' AS `referenceTable`,
									'' AS link
								FROM
								account_opening_balance

							WHERE

							 	account_opening_balance.balance <> 0";





					$select .="
								UNION ALL

								SELECT
								`account`.`account_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
								'' AS `recepientId`,
								'' AS `accountsclassfication`,
								account.account_id AS `accountId`,
								0 AS `accountFromId`,
								CONCAT('Opening Balance as from',' ',`account`.`start_date`) AS `transactionName`,
								CONCAT('Opening Balance as from',' ',' ',`account`.`start_date`) AS `transactionDescription`,
								`account`.`account_opening_balance` AS `amount`,
								`account`.`start_date` AS `transactionDate`,
								`account`.`start_date` AS `createdAt`,
								`account`.`account_status` AS `status`,
								branch_id AS `branch_id`,
								".$shareholder_id." AS `shareholder_id`,
								'Fund Accounts' AS `transactionCategory`,
								'Account Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'account' AS `referenceTable`,
								'' AS link
								FROM
								account

							WHERE

							 	account.account_opening_balance <> 0 AND account.parent_account <> 0";


				if($include_transfers)
				{

					$select .="
						UNION ALL


						SELECT
							`finance_transfer`.`finance_transfer_id` AS `transactionId`,
							`finance_transfer`.`finance_transfer_id` AS `referenceId`,
							'' AS `payingFor`,
							`finance_transfer`.`reference_number` AS `referenceCode`,
							`finance_transfer`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							`finance_transfer`.`account_to_id` AS `accountId`,
							`finance_transfer`.`account_from_id` AS `accountFromId`,
							`finance_transfer`.`remarks` AS `transactionName`,
							CONCAT('Amount Received ',' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
							`finance_transfer`.`finance_transfer_amount` AS `amount`,
							`finance_transfer`.`transaction_date` AS `transactionDate`,
							`finance_transfer`.`created` AS `createdAt`,
							`finance_transfer`.`finance_transfer_status` AS `status`,
							`finance_transfer`.`branch_id` AS `branch_id`,
							".$shareholder_id." AS `shareholder_id`,
							'Transfer' AS `transactionCategory`,
							'Transfer' AS `transactionClassification`,
							'finance_transfer' AS `transactionTable`,
							'finance_transfered' AS `referenceTable`,
							CONCAT('open-transfers/',`finance_transfer`.`finance_transfer_id`) AS link
						FROM
						`finance_transfer`
						WHERE  finance_transfer.finance_transfer_deleted = 0
						AND finance_transfer.finance_transfer_status = 1 ";
				}

				if($include_petty_cash)
				{


					$select .="

						UNION ALL


						SELECT
							`finance_purchase`.`finance_purchase_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							finance_purchase.creditor_id AS `recepientId`,
							'' AS `accountsclassfication`,
							`finance_purchase`.`account_to_id` AS `accountId`,
							`finance_purchase`.`account_from_id` AS `accountFromId`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`) AS `transactionDescription`,
							`finance_purchase`.`finance_purchase_amount`  AS `amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase`.`finance_purchase_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							".$shareholder_id." AS `shareholder_id`,
							'Petty Cash' AS `transactionCategory`,
							'Expense' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'finance_purchase_payment' AS `referenceTable`,
							CONCAT('open-purchases/',`finance_purchase`.`transaction_date`,'/',`finance_purchase`.`finance_purchase_id`) AS link

						FROM
							finance_purchase
						WHERE
							finance_purchase.finance_purchase_deleted = 0
							AND finance_purchase.account_to_id > 0 ";
					}

					if($include_creditor_opening_balance)
					{

						$select .="
								UNION ALL

								SELECT
									`creditor`.`creditor_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									`creditor`.`creditor_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									".$supplier_opening_balance_id." AS `accountId`,
									0 AS `accountFromId`,
									'' AS `transactionName`,
									CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
									`creditor`.`opening_balance` AS `amount`,
									`creditor`.`start_date` AS `transactionDate`,
									`creditor`.`start_date` AS `createdAt`,
									`creditor`.`creditor_status` AS `status`,
									creditor.branch_id AS `branch_id`,
									".$shareholder_id." AS `shareholder_id`,
									'Creditor Opening Balance' AS `transactionCategory`,
									'Creditor Opening Balance' AS `transactionClassification`,
									'Creditor' AS `transactionTable`,
									'creditor' AS `referenceTable`,
									'' AS link
								FROM
								creditor
								WHERE creditor_status = 0 ";

					}

					if($include_creditor_invoices)
					{
						$select .="

								UNION ALL


								SELECT
									`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
									`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`creditor_invoice`.`invoice_number` AS `referenceCode`,
									`creditor_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
									`creditor_invoice`.`creditor_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									`creditor_invoice_item`.`account_to_id` AS `accountId`,
									'0' AS `accountFromId`,
									`creditor_invoice_item`.`item_description` AS `transactionName`,
									CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
									`creditor_invoice_item`.`total_amount` AS `amount`,
									`creditor_invoice`.`transaction_date` AS `transactionDate`,
									`creditor_invoice`.`created` AS `createdAt`,
									`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
									 creditor.branch_id AS `branch_id`,
									 ".$shareholder_id." AS `shareholder_id`,
									'Creditor Invoices' AS `transactionCategory`,
									'Expense' AS `transactionClassification`,
									'creditor_invoice_item' AS `transactionTable`,
									'creditor_invoice' AS `referenceTable`,
									CONCAT('search-creditor-bill/',creditor.creditor_id) AS link
								FROM
									`creditor_invoice_item`,creditor_invoice,creditor

								WHERE
									creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id
									AND creditor_invoice.creditor_invoice_status = 1
									AND creditor.creditor_id = creditor_invoice.creditor_id
									AND creditor.creditor_status = 0
									AND creditor_invoice.transaction_date >= creditor.start_date";
					}


					if($include_creditor_credit_note)
					{

						$select .="

							UNION ALL

							SELECT
								`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
								`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
								`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_credit_note`.`invoice_number` AS `referenceCode`,
								`creditor_credit_note`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_credit_note`.`creditor_id` AS `recepientId`,
								'' AS `accountsclassfication`,
								`creditor_credit_note_item`.`account_to_id` AS `accountId`,
								0 AS `accountFromId`,
								`creditor_credit_note_item`.`description` AS `transactionName`,
								`creditor_credit_note_item`.`description` AS `transactionDescription`,
								`creditor_credit_note_item`.`credit_note_amount` AS `amount`,
								`creditor_credit_note`.`transaction_date` AS `transactionDate`,
								`creditor_credit_note`.`created` AS `createdAt`,
								`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
								 creditor.branch_id AS `branch_id`,
								 ".$shareholder_id." AS `shareholder_id`,
								'Creditor Credit Note' AS `transactionCategory`,
								'Expense' AS `transactionClassification`,
								'creditor_credit_note' AS `transactionTable`,
								'creditor_credit_note_item' AS `referenceTable`,
								CONCAT('search-creditor-credit-notes/',`creditor`.`creditor_id`) AS link
							FROM
								`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
							WHERE
								creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id
								AND creditor_credit_note.creditor_credit_note_status = 1
								AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
								AND creditor_invoice.creditor_invoice_status = 1
								AND creditor.creditor_id = creditor_invoice.creditor_id
								AND creditor.creditor_status = 0
								AND creditor_invoice.transaction_date >= creditor.start_date";
					}

					if($include_creditor_payments)
					{

						$select .="
							UNION ALL


							SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								'' AS `accountsclassfication`,
								`creditor_payment`.`account_from_id` AS `accountId`,
								0 AS `accountFromId`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								SUM(`creditor_payment_item`.`amount_paid`) AS `amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								 creditor.branch_id AS `branch_id`,
								 ".$shareholder_id." AS `shareholder_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`,
								CONCAT('open-creditor-payments/',creditor.creditor_id,'/',`creditor_payment`.`creditor_payment_id`) AS link
							FROM

								`creditor_payment_item`,creditor_payment,creditor_invoice,creditor


							WHERE creditor_payment_item.invoice_type = 0
								AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
								AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
								AND creditor_payment.creditor_payment_status = 1
								AND creditor_invoice.creditor_invoice_status = 1
								AND creditor_invoice.creditor_id = creditor.creditor_id
								AND creditor_invoice.transaction_date >= creditor.start_date
								AND creditor.creditor_status = 0
							GROUP BY creditor_payment_item.creditor_payment_id



							UNION ALL

							SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								'' AS `accountsclassfication`,
								`creditor_payment`.`account_from_id` AS `accountId`,
								0 AS `accountFromId`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								SUM(`creditor_payment_item`.`amount_paid`) AS `amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								 creditor.branch_id AS `branch_id`,
								 ".$shareholder_id." AS `shareholder_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Opening Balance Payment' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`,
								CONCAT('open-creditor-payments/',creditor.creditor_id,'/',`creditor_payment`.`creditor_payment_id`) AS link
							FROM

								`creditor_payment_item`,creditor_payment,creditor

							WHERE creditor_payment_item.invoice_type = 2
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date
							AND creditor.creditor_status = 0
							GROUP BY creditor_payment_item.creditor_payment_id

						UNION ALL

							SELECT
								`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
								`creditor_payment`.`creditor_payment_id` AS `referenceId`,
								`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
								`creditor_payment`.`reference_number` AS `referenceCode`,
								`creditor_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`creditor_payment`.`creditor_id` AS `recepientId`,
								'' AS `accountsclassfication`,
								`creditor_payment`.`account_from_id` AS `accountId`,
								0 AS `accountFromId`,
								`creditor_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
								SUM(`creditor_payment_item`.`amount_paid`) AS `amount`,
								`creditor_payment`.`transaction_date` AS `transactionDate`,
								`creditor_payment`.`created` AS `createdAt`,
								`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
								 creditor.branch_id AS `branch_id`,
								 ".$shareholder_id." AS `shareholder_id`,
								'Creditor Payment' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'creditor_payment' AS `transactionTable`,
								'creditor_payment_item' AS `referenceTable`,
								CONCAT('open-creditor-payments/',creditor.creditor_id,'/',`creditor_payment`.`creditor_payment_id`) AS link
							FROM

								`creditor_payment_item`,creditor_payment,creditor

							WHERE
								creditor_payment_item.invoice_type = 3
								AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
								AND creditor.creditor_id = creditor_payment_item.creditor_id
								AND creditor_payment.transaction_date >= creditor.start_date
								AND creditor.creditor_status = 0
							GROUP BY creditor_payment_item.creditor_payment_id";
					}

					if($include_creditor_invoices)
					{


						$select .="

							UNION ALL

								SELECT
									`orders`.`order_id` AS `transactionId`,
									`orders`.`order_id` AS `referenceId`,
									'' AS `payingFor`,
									`orders`.`supplier_invoice_number` AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									`orders`.`supplier_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									`orders`.`account_id` AS `accountId`,
									0 AS `accountFromId`,
									'Supplier Invoice Purchases' AS `transactionName`,
									CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
									`orders`.`total_order_amount` AS `amount`,
									`orders`.`supplier_invoice_date` AS `transactionDate`,
									`orders`.`created` AS `createdAt`,
									`orders`.`order_approval_status` AS `status`,
									 creditor.branch_id AS `branch_id`,
									 ".$shareholder_id." AS `shareholder_id`,
									'Creditor Invoices' AS `transactionCategory`,
									'Creditor Inventory Expense' AS `transactionClassification`,
									'order_supplier' AS `transactionTable`,
									'orders' AS `referenceTable`,
									CONCAT('accounts-payables/supplier-invoice-detail/',`orders`.`order_id`)  AS link
								FROM
									orders,creditor
								WHERE orders.is_store = 0
									AND orders.supplier_id = creditor.creditor_id
									AND orders.order_approval_status = 7
									AND orders.supplier_invoice_date >= creditor.start_date
									AND creditor.creditor_status = 0";
					}

				if($include_creditor_credit_note)
				{

					$select .="
					UNION ALL

						SELECT
							`orders`.`order_id` AS `transactionId`,
							`orders`.`order_id` AS `referenceId`,
							'' AS `payingFor`,
							`orders`.`supplier_invoice_number` AS `referenceCode`,
							`orders`.`reference_number`  AS `transactionCode`,
							'' AS `patient_id`,
							`orders`.`supplier_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							`orders`.`account_id` AS `accountId`,
							0 AS `accountFromId`,
							'Credit' AS `transactionName`,
							CONCAT('Credit note of ',' ',`orders`.`reference_number`) AS `transactionDescription`,
							SUM(`orders`.`total_order_amount`) AS `amount`,
							`orders`.`supplier_invoice_date` AS `transactionDate`,
							`orders`.`created` AS `createdAt`,
							`orders`.`order_approval_status` AS `status`,
							 creditor.branch_id AS `branch_id`,
							 ".$shareholder_id." AS `shareholder_id`,
							'Creditor Credit Note' AS `transactionCategory`,
							'Creditor Inventory Credit Note' AS `transactionClassification`,
							'order_supplier' AS `transactionTable`,
							'orders' AS `referenceTable`,
							CONCAT('procurement/credit-note-detail/',`orders`.`order_id`) AS link

						FROM
						orders,creditor

						WHERE orders.is_store = 3
						AND orders.supplier_id = creditor.creditor_id
						AND orders.order_approval_status = 7
						AND orders.supplier_invoice_date >= creditor.start_date
						AND creditor.creditor_status = 0";
				}
				if($include_creditor_payments)
				{

					$select .="

					UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							creditor_payment.account_from_id AS `accountId`,
							0 AS `accountFromId`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							`creditor_payment_item`.`amount_paid` AS `amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							 creditor.branch_id AS `branch_id`,
							 ".$shareholder_id." AS `shareholder_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`,
							CONCAT('open-creditor-payments/',creditor.creditor_id,'/',`creditor_payment`.`creditor_payment_id`) AS link
						FROM

						`creditor_payment_item`,creditor_payment,orders,creditor

						WHERE creditor_payment_item.invoice_type = 1
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
							AND creditor_payment.creditor_payment_status = 1
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date
							AND creditor.creditor_status = 0";
				}

				if($include_bank_recons)
				{


					$select .="
							UNION ALL

								SELECT

									bank_reconcilliation.recon_id AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									`bank_reconcilliation`.`expense_account_id` AS `accountId`,
									`bank_reconcilliation`.`account_id` AS `accountFromId`,
									'' AS `transactionName`,
									CONCAT('Bank Charges ') AS `transactionDescription`,
									bank_reconcilliation.service_charged  AS `amount`,
									bank_reconcilliation.charged_date AS `transactionDate`,
									bank_reconcilliation.charged_date AS `createdAt`,
									bank_reconcilliation.recon_status AS `status`,
									bank_reconcilliation.branch_id AS `branch_id`,
									".$shareholder_id." AS `shareholder_id`,
									'Expense' AS `transactionCategory`,
									'Bank Reconcilliation' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`,
									'' AS link
								FROM
									bank_reconcilliation
								WHERE
									bank_reconcilliation.recon_status = 2";
				}


				if($include_journals)
				{

					$select .="

					UNION ALL

						SELECT
							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							journal_entry.journal_type_id AS `accountsclassfication`,
							journal_entry.account_to_id AS `accountId`,
							journal_entry.account_from_id AS `accountFromId`,
							CONCAT(journal_entry.journal_entry_description)  AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							`journal_entry`.`amount_paid` AS `amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							journal_entry.branch_id AS `branch_id`,
							".$shareholder_id." AS `shareholder_id`,
							'Journal Credit' AS `transactionCategory`,
							'Journal Entry' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`,
							CONCAT('open-journal/',`journal_entry`.`journal_entry_id`) AS link
						FROM
							journal_entry

						WHERE journal_entry.journal_entry_deleted = 0";
				}


				if($include_customer_income)
				{
					$select .= "

								UNION ALL

								SELECT
									`lender_invoice_item`.`lender_invoice_item_id` AS `transactionId`,
									`lender_invoice`.`lender_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`lender_invoice`.`invoice_number` AS `referenceCode`,
									`lender_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
									`lender_invoice`.`lender_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									`lender_invoice_item`.`account_to_id` AS `accountId`,
									'0' AS `accountFromId`,
									`lender_invoice_item`.`item_description` AS `transactionName`,
									CONCAT(`lender_invoice_item`.`item_description`,' ',lender_invoice.invoice_number) AS `transactionDescription`,
									`lender_invoice_item`.`total_amount` AS `amount`,
									`lender_invoice`.`transaction_date` AS `transactionDate`,
									`lender_invoice`.`created` AS `createdAt`,
									`lender_invoice_item`.`lender_invoice_item_status` AS `status`,
									 lender.branch_id AS `branch_id`,
									 ".$shareholder_id." AS `shareholder_id`,
									'Receivable Invoice' AS `transactionCategory`,
									'Income' AS `transactionClassification`,
									'lender_invoice_item' AS `transactionTable`,
									'lender_invoice' AS `referenceTable`,
									'' AS link
								FROM
									`lender_invoice_item`,lender_invoice,lender

								WHERE
									lender_invoice.lender_invoice_id = lender_invoice_item.lender_invoice_id
									AND lender_invoice.lender_invoice_status = 1
									AND lender.lender_id = lender_invoice.lender_id
									AND lender_invoice.transaction_date >= lender.start_date
						UNION ALL



						SELECT
							`lender_payment_item`.`lender_payment_item_id` AS `transactionId`,
							`lender_payment`.`lender_payment_id` AS `referenceId`,
							`lender_payment_item`.`lender_invoice_id` AS `payingFor`,
							`lender_payment`.`reference_number` AS `referenceCode`,
							`lender_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`lender_payment`.`lender_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							`lender_payment`.`account_from_id` AS `accountId`,
							0 AS `accountFromId`,
							`lender_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment for invoice of ',' ',`lender_invoice`.`invoice_number`,' Ref. ', `lender_payment`.`reference_number`)  AS `transactionDescription`,
							`lender_payment_item`.`amount_paid` AS `amount`,
							`lender_payment`.`transaction_date` AS `transactionDate`,
							`lender_payment`.`created` AS `createdAt`,
							`lender_payment_item`.`lender_payment_item_status` AS `status`,
							 lender.branch_id AS `branch_id`,
							 1 AS `shareholder_id`,
							'Receivable Payment' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'lender_payment' AS `transactionTable`,
							'lender_payment_item' AS `referenceTable`,
									'' AS link
						FROM

							`lender_payment_item`,lender_payment,lender_invoice,lender


						WHERE lender_payment_item.invoice_type = 0
							AND lender_payment.lender_payment_id = lender_payment_item.lender_payment_id
							AND lender_invoice.lender_invoice_id = lender_payment_item.lender_invoice_id
							AND lender_payment.lender_payment_status = 1
							AND lender_invoice.lender_invoice_status = 1
							AND lender_invoice.lender_id = lender.lender_id
							AND lender_invoice.transaction_date >= lender.start_date";
				}


				if($include_income)
				{

					$select .="

					UNION ALL


						SELECT

							visit_charge.visit_charge_id  AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_invoice.visit_invoice_number AS referenceCode,
							'' AS transactionCode,
							visit_invoice.patient_id AS patientId,
							'' AS recepientId,
							'' AS `accountsclassfication`,
							visit_charge.account_id AS `accountId`,
							0 AS `accountFromId`,
							visit_charge.charge_name AS transactionName,
							CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number ) AS transactionDescription,
							visit_charge.visit_charge_amount*visit_charge.visit_charge_units  AS amount,
							visit_invoice.created AS `transactionDate`,
							visit_invoice.created AS `createdAt`,
							1 AS `status`,
							visit_invoice.branch_id AS `branch_id`,
							visit_invoice.dentist_id AS `shareholder_id`,
							'Receivable Invoice' AS `transactionCategory`,
							'Income' AS `transactionClassification`,
							'visit_invoice' AS `transactionTable`,
							'visit_charge' AS `referenceTable`,
							CONCAT('search-debtors-report/',`visit_invoice`.`visit_invoice_id`) AS link

						FROM
							visit_charge,visit_invoice
						WHERE
							visit_charge.charged = 1
							AND visit_invoice.visit_invoice_delete = 0
							AND visit_charge.visit_charge_delete = 0
							AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id";
				}

				if($include_income_with_collections)
				{

					$select .="

					UNION ALL


						SELECT

							visit_charge.visit_charge_id  AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_invoice.visit_invoice_number AS referenceCode,
							'' AS transactionCode,
							visit_invoice.patient_id AS patientId,
							'' AS recepientId,
							'' AS `accountsclassfication`,
							visit_charge.account_id AS `accountId`,
							0 AS `accountFromId`,
							visit_charge.charge_name AS transactionName,
							CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number ) AS transactionDescription,
							visit_charge.visit_charge_amount*visit_charge.visit_charge_units  AS amount,
							visit_invoice.created AS `transactionDate`,
							visit_invoice.created AS `createdAt`,
							1 AS `status`,
							visit_invoice.branch_id AS `branch_id`,
							visit_invoice.dentist_id AS `shareholder_id`,
							'Receivable Invoice' AS `transactionCategory`,
							'Income' AS `transactionClassification`,
							'visit_invoice' AS `transactionTable`,
							'visit_charge' AS `referenceTable`,
							CONCAT('search-debtors-report/',`visit_invoice`.`visit_invoice_id`) AS link

						FROM
							visit_charge,visit_invoice
						WHERE
							visit_charge.charged = 1
							AND visit_invoice.visit_invoice_delete = 0
							AND visit_charge.visit_charge_delete = 0
							AND visit_invoice.bill_to > 1
							AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id

					UNION ALL


						SELECT

							payment_distribution.id  AS transactionId,
							visit_invoice.visit_invoice_id AS referenceId,
							'' AS `payingFor`,
							visit_invoice.visit_invoice_number AS referenceCode,
							'' AS transactionCode,
							visit_invoice.patient_id AS patientId,
							'' AS recepientId,
							'' AS `accountsclassfication`,
							visit_charge.account_id AS `accountId`,
							0 AS `accountFromId`,
							visit_charge.charge_name AS transactionName,
							CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number ) AS transactionDescription,
							payment_distribution.amount  AS amount,
							payments.payment_date AS `transactionDate`,
							visit_invoice.created AS `createdAt`,
							1 AS `status`,
							visit_invoice.branch_id AS `branch_id`,
							visit_invoice.dentist_id AS `shareholder_id`,
							'Receivable Invoice' AS `transactionCategory`,
							'Income' AS `transactionClassification`,
							'visit_invoice' AS `transactionTable`,
							'visit_charge' AS `referenceTable`,
							CONCAT('search-debtors-report/',`visit_invoice`.`visit_invoice_id`) AS link

						FROM
							visit_charge,visit_invoice,payment_distribution,payments,payment_item
						WHERE
							visit_charge.charged = 1
							AND visit_invoice.visit_invoice_delete = 0
							AND visit_charge.visit_charge_delete = 0
							AND visit_invoice.bill_to = 1
							AND visit_charge.visit_charge_id = payment_distribution.visit_charge_id
							AND payment_distribution.status = 0
							AND payments.payment_id = payment_item.payment_id
							AND payment_item.payment_item_id = payment_distribution.payment_item_id
							AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id";
				}
				if($include_income_credit_note)
				{
					$select .="

					UNION ALL

						SELECT
							visit_credit_note_item.visit_credit_note_item_id AS transactionId,
							visit_credit_note.visit_credit_note_id AS referenceId,
							visit_invoice.visit_invoice_id AS `payingFor`,
							visit_credit_note.visit_cr_note_number AS referenceCode,
							visit_credit_note.visit_cr_note_number AS transactionCode,
							visit_invoice.patient_id AS patient_id,
							'' AS recepientId,
							'' AS `accountsclassfication`,
							visit_credit_note_item.account_id AS `accountId`,
							0 AS `accountFromId`,
							visit_credit_note_item.charge_name AS transactionName,
							CONCAT('<strong>Credit Note for </strong>  Invoice Number: ', visit_invoice.visit_invoice_number,'<strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transactionDescription,

							visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units AS amount,
							visit_invoice.created AS `transactionDate`,
							visit_credit_note.created AS `createdAt`,
							1 AS `status`,
							visit_invoice.branch_id AS `branch_id`,
							visit_invoice.dentist_id AS `shareholder_id`,
							'Receivable Credit Note' AS `transactionCategory`,
							'Credit Note' AS `transactionClassification`,
							'visit_invoice' AS `transactionTable`,
							'visit_credit_note' AS `referenceTable`,
									'' AS link
						FROM
							visit_credit_note_item,visit_credit_note,visit_invoice
						WHERE
							visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0
							AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id
							AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id";
				}

				if($include_receivable_non_cash_payments)
				{
					$select .=	" UNION ALL


									SELECT

										payment_item.payment_item_id AS `transactionId`,
										payments.payment_id AS `referenceId`,
										'' AS `payingFor`,
										payments.confirm_number AS `referenceCode`,
										payments.confirm_number AS `transactionCode`,
										payments.patient_id AS `patient_id`,
										'' AS `recepientId`,
										'' AS `accountsclassfication`,
										`payment_method`.`account_id` AS `accountId`,
										0 AS `accountFromId`,
										CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number,' ',payments.transaction_code) AS `transactionName`,
										CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
										payment_item.payment_item_amount AS `amount`,
										payments.payment_date AS `transactionDate`,
										payments.payment_date AS `createdAt`,
										`payments`.`cancel` AS `status`,
										visit_invoice.branch_id AS `branch_id`,
										visit_invoice.dentist_id AS `shareholder_id`,
										'Receivable Payment' AS `transactionCategory`,
										'Payments' AS `transactionClassification`,
										'account_payments' AS `transactionTable`,
										'' AS `referenceTable`,
										CONCAT('open-payments/',payments.payment_id) AS link
									FROM
										payments,payment_item,payment_method,visit_invoice
									WHERE
										payments.cancel = 0 AND payments.payment_type = 1
										AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id
										AND payment_item.invoice_type = 1
										AND payments.payment_id = payment_item.payment_id
										AND visit_invoice.bill_to <> 1
										AND payments.payment_method_id = payment_method.payment_method_id
										AND payments.payment_service_id  <= 1

								UNION ALL


									SELECT

										payment_distribution.id AS `transactionId`,
										payments.payment_id AS `referenceId`,
										'' AS `payingFor`,
										payments.confirm_number AS `referenceCode`,
										payments.confirm_number AS `transactionCode`,
										payments.patient_id AS `patient_id`,
										'' AS `recepientId`,
										'' AS `accountsclassfication`,
										`payment_method`.`account_id` AS `accountId`,
										0 AS `accountFromId`,
										CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number,' ',payments.transaction_code) AS `transactionName`,
										CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
										payment_distribution.amount AS `amount`,
										payments.payment_date AS `transactionDate`,
										payments.payment_date AS `createdAt`,
										`payments`.`cancel` AS `status`,
										visit_invoice.branch_id AS `branch_id`,
										visit_invoice.dentist_id AS `shareholder_id`,
										'Receivable Payment' AS `transactionCategory`,
										'Payments' AS `transactionClassification`,
										'account_payments' AS `transactionTable`,
										'' AS `referenceTable`,
										CONCAT('open-payments/',payments.payment_id) AS link
									FROM
										payments,payment_item,payment_method,visit_invoice,payment_distribution,visit_charge
									WHERE
										payments.cancel = 0 AND payments.payment_type = 1
										AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id
										AND payment_item.invoice_type = 1
										AND payments.payment_id = payment_item.payment_id
										AND visit_invoice.bill_to = 1
										AND visit_invoice.visit_invoice_delete = 0
										AND payment_distribution.payment_item_id = payment_item.payment_item_id
										AND visit_charge.charged = 1 AND visit_charge.visit_charge_id = payment_distribution.visit_charge_id
										AND visit_charge.visit_charge_delete = 0 AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id
										AND payments.payment_method_id = payment_method.payment_method_id
										AND payments.payment_service_id  <= 1


					
								";
				}
				if($include_receivable_payments)
				{

					$select .=	" UNION ALL


							SELECT

								payment_item.payment_item_id AS `transactionId`,
								payments.payment_id AS `referenceId`,
								'' AS `payingFor`,
								payments.confirm_number AS `referenceCode`,
								payments.confirm_number AS `transactionCode`,
								payments.patient_id AS `patient_id`,
								'' AS `recepientId`,
								'' AS `accountsclassfication`,
								`payment_method`.`account_id` AS `accountId`,
								0 AS `accountFromId`,
								CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number,' ',payments.transaction_code) AS `transactionName`,
								CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
								payment_item.payment_item_amount AS `amount`,
								payments.payment_date AS `transactionDate`,
								payments.payment_date AS `createdAt`,
								`payments`.`cancel` AS `status`,
								visit_invoice.branch_id AS `branch_id`,
								visit_invoice.dentist_id AS `shareholder_id`,
								'Receivable Payment' AS `transactionCategory`,
								'Payments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`,
								CONCAT('open-payments/',payments.payment_id) AS link
							FROM
								payments,payment_item,payment_method,visit_invoice
							WHERE
								payments.cancel = 0 AND payments.payment_type = 1
								AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id
								AND payment_item.invoice_type = 1
								AND payments.payment_id = payment_item.payment_id
								AND payments.payment_method_id = payment_method.payment_method_id
								AND payments.payment_service_id  <= 1


					
								";
				}


				if($include_receivable_refunds)
				{

					$select .= " UNION ALL


								SELECT

									payment_item.payment_item_id  AS `transactionId`,
									payments.payment_id AS `referenceId`,
									'' AS `payingFor`,
									payments.confirm_number AS `referenceCode`,
									payments.confirm_number AS `transactionCode`,
									payments.patient_id AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									`payments`.`payment_method_id` AS `accountId`,
									0 AS `accountFromId`,
									CONCAT( 'Patient Payment Refund:') AS `transactionName`,
									CONCAT( 'Patient Payment: ') AS `transactionDescription`,
									payment_item.payment_item_amount AS `amount`,
									payments.payment_date AS `transactionDate`,
									payments.payment_date AS `createdAt`,
									`payments`.`cancel` AS `status`,
									visit_invoice.branch_id AS `branch_id`,
									visit_invoice.dentist_id AS `shareholder_id`,
									'Receivable Refund' AS `transactionCategory`,
									'Payments' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`,
									CONCAT('open-payments/',payments.payment_id) AS link
								FROM
									payments,payment_item,visit_invoice
								WHERE
									payments.cancel = 0 AND payments.payment_type > 1
									AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id
									AND payment_item.invoice_type = 1
									AND payments.payment_id = payment_item.payment_id
									AND payments.payment_service_id  > 0";

				}


				if($include_receivable_journals)
				{


					$select .=		"	UNION ALL

									SELECT


										patients_journals.patient_journal_id AS transactionId,
										patients_journals.patient_journal_id AS referenceId,
										'' AS `payingFor`,
										'' AS referenceCode,
										'' AS `transactionCode`,
										patients_journals.patient_id AS patient_id,
										patients_journals.patient_id AS `recepientId`,
										'' AS `accountsclassfication`,
										patients_journals.account_to_id AS `accountId`,
										patients_journals.account_from_id AS `accountFromId`,
										CONCAT('Patient Journals') AS `transactionName`,
										CONCAT('Patient Journals') AS `transactionDescription`,
										patients_journals.journal_amount AS `amount`,
										patients_journals.journal_date AS `transactionDate`,
										patients_journals.journal_date  AS `createdAt`,
										1 AS `status`,
										patients_journals.branch_id AS `branch_id`,
										".$shareholder_id." AS `shareholder_id`,
										'Patients Journals' AS `transactionCategory`,
										'Patient Journal' AS `transactionClassification`,
										'account_payments' AS `transactionTable`,
										'' AS `referenceTable`,
										'' AS link
									FROM
										patients_journals
									WHERE

									patients_journals.visit_invoice_id > 0";
				}


				if($include_provider_opening_balance)
				{

					$select .="



							UNION ALL

								SELECT
									`provider`.`provider_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									`provider`.`provider_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									".$provider_opening_balance_id." AS `accountId`,
									0 AS `accountFromId`,
									'' AS `transactionName`,
									CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
									`provider`.`opening_balance` AS `amount`,
									`provider`.`start_date` AS `transactionDate`,
									`provider`.`start_date` AS `createdAt`,
									`provider`.`provider_status` AS `status`,
									provider.branch_id AS `branch_id`,
									".$shareholder_id." AS `shareholder_id`,
									'Provider Opening Balance' AS `transactionCategory`,
									'Provider Opening Balance' AS `transactionClassification`,
									'' AS `transactionTable`,
									'provider' AS `referenceTable`,
									'' AS link
								FROM
								provider WHERE  provider.provider_status = 0";

				}

				if($include_provider_invoices)
				{

					$select .="
							UNION ALL

								SELECT
									`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
									`provider_invoice`.`provider_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`provider_invoice`.`invoice_number` AS `referenceCode`,
									`provider_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
									`provider_invoice`.`provider_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									`provider_invoice_item`.`account_to_id` AS `accountId`,
									0 AS `accountFromId`,
									CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionName`,
									CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
									`provider_invoice_item`.`total_amount` AS `amount`,
									`provider_invoice`.`transaction_date` AS `transactionDate`,
									`provider_invoice`.`created` AS `createdAt`,
									`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
									provider.branch_id AS `branch_id`,
									".$shareholder_id." AS `shareholder_id`,
									'Provider Invoices' AS `transactionCategory`,
									'Expense' AS `transactionClassification`,
									'provider_invoice_item' AS `transactionTable`,
									'provider_invoice' AS `referenceTable`,
									CONCAT('search-provider-bill/',`provider`.`provider_id`) AS link
								FROM
									`provider_invoice_item`,provider_invoice,provider

								WHERE
									provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id
									AND provider_invoice.provider_invoice_status = 1
									AND provider.provider_id = provider_invoice.provider_id
									AND provider.provider_status = 0
									AND provider_invoice.transaction_date >= provider.start_date";
				}

				if($include_provider_credit_notes)
				{

					$select .="


							UNION ALL

							SELECT
								`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
								`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
								`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
								`provider_credit_note`.`invoice_number` AS `referenceCode`,
								`provider_credit_note`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`provider_credit_note`.`provider_id` AS `recepientId`,
								'' AS `accountsclassfication`,
								`provider_credit_note_item`.`account_to_id` AS `accountId`,
								0 AS `accountFromId`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionDescription`,
								`provider_credit_note_item`.`credit_note_amount` AS `amount`,
								`provider_credit_note`.`transaction_date` AS `transactionDate`,
								`provider_credit_note`.`created` AS `createdAt`,
								`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
								provider.branch_id AS `branch_id`,
								".$shareholder_id." AS `shareholder_id`,
								'Provider Credit Note' AS `transactionCategory`,
								'Credit Note' AS `transactionClassification`,
								'provider_credit_note' AS `transactionTable`,
								'provider_credit_note_item' AS `referenceTable`,
								CONCAT('search-provider-credit-notes/',`provider_credit_note`.`provider_credit_note_id`) AS link
							FROM
								`provider_credit_note_item`,provider_credit_note,provider_invoice,provider

							WHERE
							provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id
							AND provider_credit_note.provider_credit_note_status = 1
							AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
							AND provider_invoice.provider_invoice_status = 1
							AND provider.provider_id = provider_invoice.provider_id
							AND provider.provider_status = 0
							AND provider_invoice.transaction_date >= provider.start_date";
				}


				if($include_provider_payments)
				{

					$select .="
							UNION ALL

							SELECT
								`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
								`provider_payment`.`provider_payment_id` AS `referenceId`,
								`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
								`provider_payment`.`reference_number` AS `referenceCode`,
								`provider_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`provider_payment`.`provider_id` AS `recepientId`,
								'' AS `accountsclassfication`,
								`provider_payment`.`account_from_id` AS `accountId`,
								0 AS `accountFromId`,
								`provider_payment_item`.`description` AS `transactionName`,
								CONCAT('Payment on account')  AS `transactionDescription`,
								`provider_payment_item`.`amount_paid` AS `amount`,
								`provider_payment`.`transaction_date` AS `transactionDate`,
								`provider_payment`.`created` AS `createdAt`,
								`provider_payment_item`.`provider_payment_item_status` AS `status`,
								provider.branch_id AS `branch_id`,
								".$shareholder_id." AS `shareholder_id`,
								'Provider Invoices Payments' AS `transactionCategory`,
								'Creditors Invoices Payments' AS `transactionClassification`,
								'provider_payment' AS `transactionTable`,
								'provider_payment_item' AS `referenceTable`,
								CONCAT('open-provider-payments/',provider.provider_id,'/',`provider_payment`.`provider_payment_id`) AS link
							FROM
								`provider_payment_item`,provider_payment,provider

							WHERE
								provider_payment_item.invoice_type <> 1 AND
									provider_payment.provider_payment_id = provider_payment_item.provider_payment_id
									AND provider_payment.provider_payment_status = 1
									AND provider.provider_id = provider_payment.provider_id
									AND provider.provider_status = 0
									AND provider_payment.transaction_date >= provider.start_date
							";
				}


				if($include_wht)
				{


					$select .="


							UNION ALL


							SELECT
								`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
								`provider_invoice`.`provider_invoice_id` AS `referenceId`,
								'' AS `payingFor`,
								`provider_invoice`.`invoice_number` AS `referenceCode`,
								`provider_invoice`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`provider_invoice`.`provider_id` AS `recepientId`,
								'' AS `accountsclassfication`,
								".$providers_wht_id." AS `accountId`,
								0 AS `accountFromId`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionName`,
								CONCAT(`provider`.`provider_name`,' WHT ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
								`provider_invoice_item`.`vat_amount` AS `amount`,
								`provider_invoice`.`transaction_date` AS `transactionDate`,
								`provider_invoice`.`created` AS `createdAt`,
								`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
								provider.branch_id AS `branch_id`,
								".$shareholder_id." AS `shareholder_id`,
								'Provider WHT' AS `transactionCategory`,
								'Taxes' AS `transactionClassification`,
								'provider_invoice_item' AS `transactionTable`,
								'provider_invoice' AS `referenceTable`,
								CONCAT('search-provider-bill/',`provider`.`provider_id`) AS link
							FROM
								`provider_invoice_item`,provider_invoice,provider

							WHERE
								provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id
								AND provider_invoice.provider_invoice_status = 1
								AND provider.provider_id = provider_invoice.provider_id
								AND provider.provider_status = 0
								AND provider_invoice.transaction_date >= provider.start_date
								AND provider_invoice_item.vat_amount > 0";
				}


				if($include_assets_bills)
				{

					$select .="

							UNION ALL


							SELECT
								asset_category.asset_category_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								assets_details.asset_serial_no AS `referenceCode`,
								assets_details.asset_serial_no AS `transactionCode`,
								assets_details.asset_id AS `patient_id`,
									assets_details.supplier_id AS `recepientId`,
								'' AS `accountsclassfication`,
								asset_category.account_id AS `accountId`,
								0 AS `accountFromId`,
								CONCAT(assets_details.asset_name) AS `transactionName`,
								CONCAT(assets_details.asset_name) AS `transactionDescription`,
								(assets_details.asset_value) AS `amount`,
								assets_details.asset_pd_period AS `transactionDate`,
								assets_details.asset_pd_period  AS `createdAt`,
								1 AS `status`,
								assets_details.branch_id AS `branch_id`,
								".$shareholder_id." AS `shareholder_id`,
								'FIXED ASSET' AS `transactionCategory`,
								'Expense' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`,
									'' AS link
							FROM
								assets_details,asset_category
							WHERE
								assets_details.asset_category_id = asset_category.asset_category_id
								AND assets_details.bill_asset = 1 ";
				}

				if($include_loan_invoices)
				{

					$select .= "
								UNION ALL

								SELECT
									`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
									`loan_invoice`.`loan_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`loan_invoice`.`invoice_number` AS `referenceCode`,
									`loan_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
										`loan_invoice`.`loan_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									`loan`.`account_to_id` AS `accountId`,
									`loan`.`account_from_id` AS `accountFromId`,
									`loan_invoice_item`.`item_description` AS `transactionName`,
									`loan_invoice_item`.`item_description` AS `transactionDescription`,
									 `loan_invoice_item`.`total_amount` AS `amount`,
									`loan_invoice`.`transaction_date` AS `transactionDate`,
									`loan_invoice`.`created` AS `createdAt`,
									`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
									 loan.branch_id AS `branch_id`,
									 ".$shareholder_id." AS `shareholder_id`,
									'Loans Expense' AS `transactionCategory`,
									'Loans' AS `transactionClassification`,
									'loan_invoice_item' AS `transactionTable`,
									'loan_invoice' AS `referenceTable`,
									'' AS link
								FROM
									`loan_invoice_item`,loan_invoice,loan

								WHERE
									loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id
									AND loan_invoice.loan_invoice_status = 1
									AND loan.loan_id = loan_invoice.loan_id
									AND loan_invoice.transaction_date >= loan.start_date";
				}



				if($include_loan_payments)
				{
					$select .= "
						UNION ALL

						SELECT
							`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
							`loan_payment`.`loan_payment_id` AS `referenceId`,
							`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
							`loan_payment`.`reference_number` AS `referenceCode`,
							`loan_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
								`loan_payment`.`loan_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							`loan_payment`.`account_from_id` AS `accountId`,
							0 AS `accountFromId`,
							CONCAT('Payment from ') AS `transactionName`,
							CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
							`loan_payment_item`.`amount_paid` AS `amount`,
							`loan_payment`.`transaction_date` AS `transactionDate`,
							`loan_payment`.`created` AS `createdAt`,
							`loan_payment_item`.`loan_payment_item_status` AS `status`,
							loan.branch_id AS `branch_id`,
							".$shareholder_id." AS `shareholder_id`,
							'Loan Payment' AS `transactionCategory`,
							'Loans Invoices Payments' AS `transactionClassification`,
							'loan_payment' AS `transactionTable`,
							'loan_payment_item' AS `referenceTable`,
									'' AS link
						FROM
							`loan_payment_item`,loan_payment,loan_invoice,loan

						WHERE loan_payment_item.invoice_type = 0
							AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
							AND loan_payment.loan_payment_status = 1
							AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
							AND loan_invoice.loan_invoice_status = 1
							AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
							AND loan.account_from_id = loan_payment_item.account_to_id




					UNION ALL

					SELECT
							`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
							`loan_payment`.`loan_payment_id` AS `referenceId`,
							`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
							`loan_payment`.`reference_number` AS `referenceCode`,
							`loan_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`loan_payment`.`loan_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							loan_payment_item.account_to_id AS `accountId`,
							loan_payment.account_from_id AS `accountFromId`,
							CONCAT('Payment ') AS `transactionName`,
							CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
							`loan_payment_item`.`amount_paid` AS `amount`,
							`loan_payment`.`transaction_date` AS `transactionDate`,
							`loan_payment`.`created` AS `createdAt`,
							`loan_payment_item`.`loan_payment_item_status` AS `status`,
							loan.branch_id AS `branch_id`,
							".$shareholder_id." AS `shareholder_id`,
							'Loan Interest Payment' AS `transactionCategory`,
							'Interest Expense' AS `transactionClassification`,
							'loan_payment' AS `transactionTable`,
							'loan_payment_item' AS `referenceTable`,
									'' AS link
						FROM
							`loan_payment_item`,loan_payment,loan_invoice,loan

							WHERE loan_payment_item.invoice_type = 0
							AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
							AND loan_payment.loan_payment_status = 1
							AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
							AND loan_invoice.loan_invoice_status = 1
							AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
							AND loan.account_from_id <> loan_payment_item.account_to_id";
				}

				if($include_asset_depreciations)
				{

					$select .="
						UNION ALL


						SELECT
							asset_category.asset_category_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							assets_details.asset_serial_no AS `referenceCode`,
							assets_details.asset_serial_no AS `transactionCode`,
							assets_details.asset_id AS `patient_id`,
								assets_details.supplier_id AS `recepientId`,
							'' AS `accountsclassfication`,
							asset_amortization.expense_account_id AS `accountId`,
							0 AS `accountFromId`,
							CONCAT(assets_details.asset_name) AS `transactionName`,
							CONCAT(assets_details.asset_name) AS `transactionDescription`,
							(asset_amortization.interest_amount) AS `amount`,
							asset_amortization.date_approved AS `transactionDate`,
							asset_amortization.date_approved  AS `createdAt`,
							1 AS `status`,
							assets_details.branch_id AS `branch_id`,
							".$shareholder_id." AS `shareholder_id`,
							'Asset Depreciation' AS `transactionCategory`,
							'Asset Depreciation' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`,
									'' AS link
						FROM
							assets_details,asset_category,asset_amortization
						WHERE
							assets_details.asset_category_id = asset_category.asset_category_id
							AND asset_amortization.expense_account_id > 0
							AND asset_amortization.asset_id = assets_details.asset_id
							AND asset_amortization.bill_status = 1";
				}

				if($include_batch_overpayments)
				{




					$select .="

							UNION ALL

							SELECT
								batch_receipts.batch_receipt_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								batch_receipts.receipt_number AS `referenceCode`,
								batch_receipts.receipt_number AS `transactionCode`,
								batch_unallocations.unallocated_payment_id AS `patient_id`,
								0 AS `recepientId`,
								'' AS `accountsclassfication`,
								batch_receipts.bank_id AS `accountId`,
								0 AS `accountFromId`,
								CONCAT('BATCH OVERPAYMENT :',' : ',batch_unallocations.reason) AS `transactionName`,
								CONCAT('BATCH OVERPAYMENT :',' : ',batch_unallocations.reason) AS `transactionDescription`,
								batch_unallocations.amount_paid AS `amount`,
								batch_receipts.payment_date AS `transactionDate`,
								batch_receipts.payment_date  AS `createdAt`,
								1 AS `status`,
								batch_receipts.branch_id AS `branch_id`,
								".$shareholder_id." AS `shareholder_id`,
								'Prepayments' AS `transactionCategory`,
								'Insuance Overpayments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`,
								CONCAT('debtors/view_batch_items/',batch_receipts.batch_receipt_id,'/',batch_receipts.insurance_id) AS link
							FROM
									batch_unallocations,batch_receipts

							WHERE
								batch_receipts.batch_receipt_id = batch_unallocations.batch_receipt_id
								AND batch_unallocations.unallocated_payment_delete = 0
								AND  batch_unallocations.allocation_type_id = 0";
				}

				if($include_payroll_invoice)
				{

					$select .=	"

								UNION ALL

								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									statutory_accounts.account_id AS `accountId`,
									0 AS `accountFromId`,
									CONCAT('Payroll') AS `transactionName`,
									CONCAT('Payroll') AS `transactionDescription`,
									payroll_summary.total_payroll AS `amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									payroll.branch_id AS branch_id,
									".$shareholder_id." AS `shareholder_id`,
									'Payroll Invoice' AS `transactionCategory`,
									'Expense' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`,
									CONCAT('statutory-statement/',`statutory_accounts`.`statutory_account_id`) AS link
								FROM
									payroll_summary,payroll,statutory_accounts
								WHERE
									payroll.payroll_id = payroll_summary.payroll_id
									AND statutory_accounts.statutory_account_id = 1

								UNION ALL


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									statutory_accounts.account_id AS `accountId`,
									0 AS `accountFromId`,
									CONCAT('PAYE') AS `transactionName`,
									CONCAT('PAYE') AS `transactionDescription`,
									payroll_summary.paye AS `amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									payroll.branch_id AS branch_id,
									".$shareholder_id." AS `shareholder_id`,
									'Payroll Invoice' AS `transactionCategory`,
									'Expense' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`,
									CONCAT('statutory-statement/',`statutory_accounts`.`statutory_account_id`) AS link
								FROM
									payroll_summary,payroll,statutory_accounts
								WHERE
									payroll.payroll_id = payroll_summary.payroll_id
									AND statutory_accounts.statutory_account_id = 2

								UNION ALL


								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									statutory_accounts.account_id AS `accountId`,
									0 AS `accountFromId`,
									CONCAT('NSSF') AS `transactionName`,
									CONCAT('NSSF') AS `transactionDescription`,
									payroll_summary.nssf AS `amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									payroll.branch_id AS branch_id,
									".$shareholder_id." AS `shareholder_id`,
									'Payroll Invoice' AS `transactionCategory`,
									'Expense' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'' AS `referenceTable`,
									CONCAT('statutory-statement/',`statutory_accounts`.`statutory_account_id`) AS link
								FROM
									payroll_summary,payroll,statutory_accounts
								WHERE
									payroll.payroll_id = payroll_summary.payroll_id
									AND statutory_accounts.statutory_account_id = 3



								UNION ALL




								SELECT
									payroll_summary.payroll_summary_id AS `transactionId`,
									payroll_summary.payroll_id AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									statutory_accounts.account_id AS `accountId`,
									0 AS `accountFromId`,
									CONCAT('NHIF') AS `transactionName`,
									CONCAT('NHIF') AS `transactionDescription`,
									payroll_summary.nhif AS `amount`,
									payroll_summary.payroll_created_for AS `transactionDate`,
									payroll_summary.payroll_created_for  AS `createdAt`,
									`payroll`.`payroll_status` AS `status`,
									payroll.branch_id AS branch_id,
									".$shareholder_id." AS `shareholder_id`,
									'Payroll Invoice' AS `transactionCategory`,
									'Expense' AS `transactionClassification`,
									'Payroll' AS `transactionTable`,
									'' AS `referenceTable`,
									CONCAT('statutory-statement/',`statutory_accounts`.`statutory_account_id`) AS link
								FROM
									payroll_summary,payroll,statutory_accounts
								WHERE
									payroll.payroll_id = payroll_summary.payroll_id
									AND statutory_accounts.statutory_account_id = 4


								";
				}

				if($include_payroll_liabilities)
				{
					$select .= " UNION ALL




									SELECT
										payroll_summary.payroll_summary_id AS `transactionId`,
										payroll_summary.payroll_id AS `referenceId`,
										'' AS `payingFor`,
										'' AS `referenceCode`,
										'' AS `transactionCode`,
										'' AS `patient_id`,
										'' AS `recepientId`,
										'' AS `accountsclassfication`,
										statutory_accounts.account_id AS `accountId`,
										0 AS `accountFromId`,
										CONCAT('Savings') AS `transactionName`,
										CONCAT('Savings') AS `transactionDescription`,
										payroll_summary.savings AS `amount`,
										payroll_summary.payroll_created_for AS `transactionDate`,
										payroll_summary.payroll_created_for  AS `createdAt`,
										`payroll`.`payroll_status` AS `status`,
										payroll.branch_id AS branch_id,
										".$shareholder_id." AS `shareholder_id`,
										'Payroll Deductions' AS `transactionCategory`,
										'Expense' AS `transactionClassification`,
										'Payroll' AS `transactionTable`,
										'' AS `referenceTable`,
										CONCAT('statutory-statement/',`statutory_accounts`.`statutory_account_id`) AS link
									FROM
										payroll_summary,payroll,statutory_accounts
									WHERE
										payroll.payroll_id = payroll_summary.payroll_id
										AND statutory_accounts.statutory_account_id = 5";
				}


				if($include_payroll_liabilities_payments)
				{
					$select .="

							UNION ALL

							SELECT
								`payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
								`payroll_payment`.`payroll_payment_id` AS `referenceId`,
								`payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
								`payroll_payment`.`reference_number` AS `referenceCode`,
								`payroll_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`statutory_accounts`.`statutory_account_id` AS `recepientId`,
								'' AS `accountsclassfication`,
								`payroll_payment`.`account_from_id` AS `accountId`,
								0 AS `accountFromId`,
								CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
								CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
								`payroll_payment_item`.`amount_paid` AS `amount`,
								`payroll_payment`.`transaction_date` AS `transactionDate`,
								`payroll_payment`.`created` AS `createdAt`,
								`payroll_payment_item`.`payroll_payment_item_status` AS `status`,
								2 AS `branch_id`,
								".$shareholder_id." AS `shareholder_id`,
								'Payroll Deductions Payment' AS `transactionCategory`,
								'Payroll Deductions Payment' AS `transactionClassification`,
								'payroll_payment' AS `transactionTable`,
								'payroll_payment_item' AS `referenceTable`,
								CONCAT('search-payroll-payments/',`statutory_accounts`.`statutory_account_id`) AS link
							FROM
								`payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts

							WHERE payroll_payment_item.invoice_type = 0
							AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id
							AND payroll_payment.payroll_payment_status = 1
							AND statutory_accounts.statutory_account_id = 5
							AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id
							AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id";
				}


				if($include_payroll_payments)
				{
					$select .="

							UNION ALL

							SELECT
								`payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
								`payroll_payment`.`payroll_payment_id` AS `referenceId`,
								`payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
								`payroll_payment`.`reference_number` AS `referenceCode`,
								`payroll_payment`.`document_number` AS `transactionCode`,
								'' AS `patient_id`,
								`statutory_accounts`.`statutory_account_id` AS `recepientId`,
								'' AS `accountsclassfication`,
								`payroll_payment`.`account_from_id` AS `accountId`,
								0 AS `accountFromId`,
								CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
								CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
								`payroll_payment_item`.`amount_paid` AS `amount`,
								`payroll_payment`.`transaction_date` AS `transactionDate`,
								`payroll_payment`.`created` AS `createdAt`,
								`payroll_payment_item`.`payroll_payment_item_status` AS `status`,
								2 AS `branch_id`,
								".$shareholder_id." AS `shareholder_id`,
								'Payroll Payment' AS `transactionCategory`,
								'Payroll Payments' AS `transactionClassification`,
								'payroll_payment' AS `transactionTable`,
								'payroll_payment_item' AS `referenceTable`,
								CONCAT('search-payroll-payments/',`statutory_accounts`.`statutory_account_id`) AS link
							FROM
								`payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts

							WHERE payroll_payment_item.invoice_type = 0
							AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id
							AND payroll_payment.payroll_payment_status = 1
							AND statutory_accounts.statutory_account_id < 5
							AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id
							AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id";
				}

				if($include_direct_payments)
				{
					$select .= "

							UNION ALL

							SELECT
								`account_payments`.`account_payment_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								account_payments.receipt_number AS `transactionCode`,
								'' AS `patient_id`,
								'' AS `recepientId`,
								'' AS `accountsclassfication`,
								account_payments.account_to_id AS `accountId`,
								account_payments.account_from_id AS `accountFromId`,
								CONCAT(account_payments.account_payment_description) AS `transactionName`,
								CONCAT(account_payments.account_payment_description) AS `transactionDescription`,
								`account_payments`.`amount_paid` AS `amount`,
								`account_payments`.`payment_date` AS `transactionDate`,
								`account_payments`.`payment_date` AS `createdAt`,
								`account_payments`.`account_payment_status` AS `status`,
								account_payments.branch_id AS `branch_id`,
								".$shareholder_id." AS `shareholder_id`,
								'Journal Credit' AS `transactionCategory`,
								'Journal' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'account' AS `referenceTable`,
								CONCAT('search-direct-purchases/',`account_payments`.`account_payment_id`) AS link
							FROM
								account_payments

							WHERE account_payments.account_payment_deleted = 0 ";
				}

				if($include_inventory)
				{
					$select .= "

								UNION ALL

								SELECT
									`product_deductions_stock`.`product_deductions_stock_id` AS transactionId,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									'' AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,

									".$suppliers_deductions_id." AS `accountId`,
									".$suppliers_purchase_id." AS `accountFromId`,
									CONCAT(product.product_name) AS `transactionName`,
									CONCAT(product.product_name) AS `transactionDescription`,
									( product_deductions_stock_quantity * buying_price ) AS `amount`,
									DATE(`product_deductions_stock`.`product_deductions_stock_date`) AS `transactionDate`,
									DATE(`product_deductions_stock`.`product_deductions_stock_date`) AS `createdAt`,
									`product_deductions_stock`.`product_deductions_status` AS `status`,
									2 AS `branch_id`,
									".$shareholder_id." AS `shareholder_id`,
									'Inventory Deductions' AS `transactionCategory`,
									'Deductions' AS `transactionClassification`,
									'product' AS `transactionTable`,
									'product_deductions_stock' AS `referenceTable`,
									'' AS link
								FROM
									( `product_deductions_stock`, product, store ) 
								WHERE
									product.product_id = product_deductions_stock.product_id 
									AND product.product_deleted = 0 
									AND store.store_id = product_deductions_stock.store_id 
									AND product_deductions_stock.deduction_status_id = 1";
				}

					$select .="


				) AS data,account,account_type WHERE
					data.transactionId > 0
					AND data.accountId = account.account_id
					AND  account.account_type_id = account_type.account_type_id
					".$add." ".$add_item."
					ORDER BY data.transactionDate ASC

					";


	  $query = $this->db->query($select);


	   return $query;

	}
	public function calculatePayableOpeningBal($args=null)
	{

		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();

		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;


			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];
		$provider_opening_balance_id = $session_account['provider_opening_balance_id'];
		$loans_liability_id = $session_account['loan_liability_account_id'];

		if(is_array($args))
		{

			$startYear = $args['startYear'];
			$currentYear = $args['currentYear'];
			$financialEndMonth = $args['financialEndMonth'];
			$transaction_type = $args['type'];
			$payable_id = $args['payable_id'];

			if($financialEndMonth < 10 ? "0".$financialEndMonth:$financialEndMonth);

			for ($i=$startYear; $i <= $currentYear; $i++) {
				// code...


				$start_date = $startYear.'-01-01';
				$end_period = $i.'-'.$financialEndMonth.'-31';

				$add = ' AND data.transactionDate >= "'.$start_date.'" AND  data.transactionDate <= "'.$end_period.'" ';

				$creditor = true;
				$provider = true;
				$loan = true;
				$payroll = true;
				// $transaction_type = "creditor";

				if($transaction_type == "creditor")
				{
					$provider = false;
					$loan = false;
					$payroll = false;
				}
				else if($transaction_type == "provider"){
					$creditor = false;
					$loan = false;
					$payroll = false;
				}
				else if($transaction_type == "loans"){
					$creditor = false;
					$provider = false;
					$payroll = false;
				}
				else if($transaction_type == "payroll"){
					$creditor = false;
					$loan = false;
					$provider = false;
				}





				$select  = "
							SELECT
								data.recepientId AS recepientId,
								COALESCE (SUM(data.amount),0) AS balance,
								data.transactionDate AS endDate,
								data.referenceTable AS referenceTable
							FROM

							(

							";

							if($creditor)
							{


								$select .="


										SELECT
											`creditor`.`creditor_id` AS `transactionId`,
											'' AS `referenceId`,
											'' AS `payingFor`,
											'' AS `referenceCode`,
											'' AS `transactionCode`,
											'' AS `patient_id`,
											`creditor`.`creditor_id` AS `recepientId`,
											'' AS `accountsclassfication`,
											".$accounts_payable_id." AS `accountId`,
											0 AS `accountFromId`,
											'' AS `transactionName`,
											CONCAT('Opening Balance from',' ',`creditor`.`start_date`) AS `transactionDescription`,
											`creditor`.`opening_balance` AS `amount`,
											`creditor`.`start_date` AS `transactionDate`,
											`creditor`.`start_date` AS `createdAt`,
											`creditor`.`creditor_status` AS `status`,
											creditor.branch_id AS `branch_id`,
											'Creditor Opening Balance' AS `transactionCategory`,
											'Creditor Opening Balance' AS `transactionClassification`,
											'Creditor' AS `transactionTable`,
											'creditor' AS `referenceTable`
										FROM
										creditor
										WHERE creditor_status = 0 ";


								$select .="

										UNION ALL


										SELECT
											`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
											`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
											'' AS `payingFor`,
											`creditor_invoice`.`invoice_number` AS `referenceCode`,
											`creditor_invoice`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
											`creditor_invoice`.`creditor_id` AS `recepientId`,
											'' AS `accountsclassfication`,
											`creditor_invoice_item`.`account_to_id` AS `accountId`,
											'0' AS `accountFromId`,
											`creditor_invoice_item`.`item_description` AS `transactionName`,
											CONCAT(`creditor_invoice_item`.`item_description`,' ',creditor_invoice.invoice_number) AS `transactionDescription`,
											`creditor_invoice_item`.`total_amount` AS `amount`,
											`creditor_invoice`.`transaction_date` AS `transactionDate`,
											`creditor_invoice`.`created` AS `createdAt`,
											`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
											 creditor.branch_id AS `branch_id`,
											'Creditor Invoices' AS `transactionCategory`,
											'Expense' AS `transactionClassification`,
											'creditor_invoice_item' AS `transactionTable`,
											'creditor' AS `referenceTable`
										FROM
											`creditor_invoice_item`,creditor_invoice,creditor

										WHERE
											creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id
											AND creditor_invoice.creditor_invoice_status = 1
											AND creditor.creditor_id = creditor_invoice.creditor_id
											AND creditor_invoice.transaction_date >= creditor.start_date";

								$select .="

									UNION ALL

									SELECT
										`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
										`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
										`creditor_credit_note_item`.`creditor_invoice_id` AS `payingFor`,
										`creditor_credit_note`.`invoice_number` AS `referenceCode`,
										`creditor_credit_note`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
										`creditor`.`creditor_id` AS `recepientId`,
										'' AS `accountsclassfication`,
										`creditor_credit_note_item`.`account_to_id` AS `accountId`,
										0 AS `accountFromId`,
										`creditor_credit_note_item`.`description` AS `transactionName`,
										`creditor_credit_note_item`.`description` AS `transactionDescription`,
										-`creditor_credit_note_item`.`credit_note_amount` AS `amount`,
										`creditor_credit_note`.`transaction_date` AS `transactionDate`,
										`creditor_credit_note`.`created` AS `createdAt`,
										`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
										 creditor.branch_id AS `branch_id`,
										'Creditor Credit Note' AS `transactionCategory`,
										'Expense' AS `transactionClassification`,
										'creditor_credit_note' AS `transactionTable`,
										'creditor' AS `referenceTable`
									FROM
										`creditor_credit_note_item`,creditor_credit_note,creditor_invoice,creditor
									WHERE
										creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id
										AND creditor_credit_note.creditor_credit_note_status = 1
										AND creditor_invoice.creditor_invoice_id = creditor_credit_note_item.creditor_invoice_id
										AND creditor_invoice.creditor_invoice_status = 1
										AND creditor.creditor_id = creditor_invoice.creditor_id
										AND creditor_invoice.transaction_date >= creditor.start_date";


								$select .="
											UNION ALL


											SELECT
												`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
												`creditor_payment`.`creditor_payment_id` AS `referenceId`,
												`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
												`creditor_payment`.`reference_number` AS `referenceCode`,
												`creditor_payment`.`document_number` AS `transactionCode`,
												'' AS `patient_id`,
												`creditor`.`creditor_id` AS `recepientId`,
												'' AS `accountsclassfication`,
												`creditor_payment`.`account_from_id` AS `accountId`,
												0 AS `accountFromId`,
												`creditor_payment_item`.`description` AS `transactionName`,
												CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
												-`creditor_payment_item`.`amount_paid` AS `amount`,
												`creditor_payment`.`transaction_date` AS `transactionDate`,
												`creditor_payment`.`created` AS `createdAt`,
												`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
												 creditor.branch_id AS `branch_id`,
												'Creditor Payment' AS `transactionCategory`,
												'Creditors Invoices Payments' AS `transactionClassification`,
												'creditor_payment' AS `transactionTable`,
												'creditor' AS `referenceTable`
											FROM

												`creditor_payment_item`,creditor_payment,creditor_invoice,creditor


											WHERE creditor_payment_item.invoice_type = 0
												AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
												AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
												AND creditor_payment.creditor_payment_status = 1
												AND creditor_invoice.creditor_invoice_status = 1
												AND creditor_invoice.creditor_id = creditor.creditor_id
												AND creditor_invoice.transaction_date >= creditor.start_date




											UNION ALL

											SELECT
												`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
												`creditor_payment`.`creditor_payment_id` AS `referenceId`,
												`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
												`creditor_payment`.`reference_number` AS `referenceCode`,
												`creditor_payment`.`document_number` AS `transactionCode`,
												'' AS `patient_id`,
												`creditor`.`creditor_id` AS `recepientId`,
												'' AS `accountsclassfication`,
												`creditor_payment`.`account_from_id` AS `accountId`,
												0 AS `accountFromId`,
												`creditor_payment_item`.`description` AS `transactionName`,
												CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
												-`creditor_payment_item`.`amount_paid` AS `amount`,
												`creditor_payment`.`transaction_date` AS `transactionDate`,
												`creditor_payment`.`created` AS `createdAt`,
												`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
												 creditor.branch_id AS `branch_id`,
												'Creditor Payment' AS `transactionCategory`,
												'Creditors Opening Balance Payment' AS `transactionClassification`,
												'creditor_payment' AS `transactionTable`,
												'creditor' AS `referenceTable`
											FROM

												`creditor_payment_item`,creditor_payment,creditor

											WHERE creditor_payment_item.invoice_type = 2
											AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
											AND creditor.creditor_id = creditor_payment_item.creditor_id
											AND creditor_payment.transaction_date >= creditor.start_date


										UNION ALL

											SELECT
												`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
												`creditor_payment`.`creditor_payment_id` AS `referenceId`,
												`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
												`creditor_payment`.`reference_number` AS `referenceCode`,
												`creditor_payment`.`document_number` AS `transactionCode`,
												'' AS `patient_id`,
												`creditor`.`creditor_id` AS `recepientId`,
												'' AS `accountsclassfication`,
												`creditor_payment`.`account_from_id` AS `accountId`,
												0 AS `accountFromId`,
												`creditor_payment_item`.`description` AS `transactionName`,
												CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
												-`creditor_payment_item`.`amount_paid` AS `amount`,
												`creditor_payment`.`transaction_date` AS `transactionDate`,
												`creditor_payment`.`created` AS `createdAt`,
												`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
												 creditor.branch_id AS `branch_id`,
												'Creditor Payment' AS `transactionCategory`,
												'Creditors Invoices Payments' AS `transactionClassification`,
												'creditor_payment' AS `transactionTable`,
												'creditor' AS `referenceTable`
											FROM

												`creditor_payment_item`,creditor_payment,creditor

											WHERE
												creditor_payment_item.invoice_type = 3
												AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
												AND creditor.creditor_id = creditor_payment_item.creditor_id
												AND creditor_payment.transaction_date >= creditor.start_date";



									$select .="

												UNION ALL

													SELECT
														`orders`.`order_id` AS `transactionId`,
														`orders`.`order_id` AS `referenceId`,
														'' AS `payingFor`,
														`orders`.`supplier_invoice_number` AS `referenceCode`,
														'' AS `transactionCode`,
														'' AS `patient_id`,
														`orders`.`supplier_id` AS `recepientId`,
														'' AS `accountsclassfication`,
														`orders`.`account_id` AS `accountId`,
														0 AS `accountFromId`,
														'Supplier Invoice Purchases' AS `transactionName`,
														CONCAT('Purchase of supplies',' ',orders.supplier_invoice_number) AS `transactionDescription`,
														SUM(`orders`.`total_order_amount`) AS `amount`,
														`orders`.`supplier_invoice_date` AS `transactionDate`,
														`orders`.`created` AS `createdAt`,
														`orders`.`order_approval_status` AS `status`,
														 creditor.branch_id AS `branch_id`,
														'Creditor Invoices' AS `transactionCategory`,
														'Creditor Inventory Expense' AS `transactionClassification`,
														'order_supplier' AS `transactionTable`,
														'creditor' AS `referenceTable`
													FROM
														orders,creditor
													WHERE orders.is_store = 0
														AND orders.supplier_id = creditor.creditor_id
														AND orders.order_approval_status = 7
														AND orders.supplier_invoice_date >= creditor.start_date";


												$select .="
															UNION ALL

															SELECT
																`orders`.`order_id` AS `transactionId`,
																`orders`.`order_id` AS `referenceId`,
																'' AS `payingFor`,
																`orders`.`supplier_invoice_number` AS `referenceCode`,
																`orders`.`reference_number`  AS `transactionCode`,
																'' AS `patient_id`,
																`orders`.`supplier_id` AS `recepientId`,
																'' AS `accountsclassfication`,
																`orders`.`account_id` AS `accountId`,
																0 AS `accountFromId`,
																'Credit' AS `transactionName`,
																CONCAT('Credit note of ',' ',`orders`.`reference_number`) AS `transactionDescription`,
																-SUM(`orders`.`total_order_amount`) AS `amount`,
																`orders`.`supplier_invoice_date` AS `transactionDate`,
																`orders`.`created` AS `createdAt`,
																`orders`.`order_approval_status` AS `status`,
																 creditor.branch_id AS `branch_id`,
																'Creditor Credit Note' AS `transactionCategory`,
																'Creditor Inventory Credit Note' AS `transactionClassification`,
																'order_supplier' AS `transactionTable`,
																'creditor' AS `referenceTable`

															FROM
															orders,creditor

															WHERE orders.is_store = 3
															AND orders.supplier_id = creditor.creditor_id
															AND orders.order_approval_status = 7
															AND orders.supplier_invoice_date >= creditor.start_date";

											$select .="

													UNION ALL

														SELECT
															`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
															`creditor_payment`.`creditor_payment_id` AS `referenceId`,
															`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
															`creditor_payment`.`reference_number` AS `referenceCode`,
															`creditor_payment`.`document_number` AS `transactionCode`,
															'' AS `patient_id`,
															`creditor`.`creditor_id` AS `recepientId`,
															'' AS `accountsclassfication`,
															creditor_payment.account_from_id AS `accountId`,
															0 AS `accountFromId`,
															`creditor_payment_item`.`description` AS `transactionName`,
															CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
															-`creditor_payment_item`.`amount_paid` AS `amount`,
															`creditor_payment`.`transaction_date` AS `transactionDate`,
															`creditor_payment`.`created` AS `createdAt`,
															`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
															 creditor.branch_id AS `branch_id`,
															'Creditor Payment' AS `transactionCategory`,
															'Creditors Invoices Payments' AS `transactionClassification`,
															'creditor_payment' AS `transactionTable`,
															'creditor' AS `referenceTable`
														FROM

														`creditor_payment_item`,creditor_payment,orders,creditor

														WHERE creditor_payment_item.invoice_type = 1
															AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
															AND creditor_payment.creditor_payment_status = 1
															AND orders.order_id = creditor_payment_item.creditor_invoice_id
															AND orders.supplier_id = creditor.creditor_id
															AND creditor_payment.transaction_date >= creditor.start_date";
							}
							if($provider)
							{

								$select .="


										UNION ALL

										SELECT
											`provider`.`provider_id` AS `transactionId`,
											'' AS `referenceId`,
											'' AS `payingFor`,
											'' AS `referenceCode`,
											'' AS `transactionCode`,
											'' AS `patient_id`,
											`provider`.`provider_id` AS `recepientId`,
											'' AS `accountsclassfication`,
											".$providers_liability_id." AS `accountId`,
											0 AS `accountFromId`,
											'' AS `transactionName`,
											CONCAT('Opening Balance from',' ',`provider`.`start_date`) AS `transactionDescription`,
											`provider`.`opening_balance` AS `amount`,
											`provider`.`start_date` AS `transactionDate`,
											`provider`.`start_date` AS `createdAt`,
											`provider`.`provider_status` AS `status`,
											provider.branch_id AS `branch_id`,
											'Provider Opening Balance' AS `transactionCategory`,
											'Provider Opening Balance' AS `transactionClassification`,
											'' AS `transactionTable`,
											'provider' AS `referenceTable`
										FROM
										provider";

									$select .="

												UNION ALL

												SELECT
													`provider_invoice_item`.`provider_invoice_item_id` AS `transactionId`,
													`provider_invoice`.`provider_invoice_id` AS `referenceId`,
													'' AS `payingFor`,
													`provider_invoice`.`invoice_number` AS `referenceCode`,
													`provider_invoice`.`document_number` AS `transactionCode`,
													'' AS `patient_id`,
													`provider_invoice`.`provider_id` AS `recepientId`,
													'' AS `accountsclassfication`,
													`provider_invoice_item`.`account_to_id` AS `accountId`,
													0 AS `accountFromId`,
													CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionName`,
													CONCAT(`provider`.`provider_name`,' ',`provider_invoice_item`.`item_description`) AS `transactionDescription`,
													`provider_invoice_item`.`total_amount` AS `amount`,
													`provider_invoice`.`transaction_date` AS `transactionDate`,
													`provider_invoice`.`created` AS `createdAt`,
													`provider_invoice_item`.`provider_invoice_item_status` AS `status`,
													provider.branch_id AS `branch_id`,
													'Provider Invoices' AS `transactionCategory`,
													'Expense' AS `transactionClassification`,
													'provider_invoice_item' AS `transactionTable`,
													'provider' AS `referenceTable`
												FROM
													`provider_invoice_item`,provider_invoice,provider

												WHERE
													provider_invoice.provider_invoice_id = provider_invoice_item.provider_invoice_id
													AND provider_invoice.provider_invoice_status = 1
													AND provider.provider_id = provider_invoice.provider_id
													AND provider_invoice.transaction_date >= provider.start_date";


									$select .="


											UNION ALL

											SELECT
												`provider_credit_note_item`.`provider_credit_note_item_id` AS `transactionId`,
												`provider_credit_note`.`provider_credit_note_id` AS `referenceId`,
												`provider_credit_note_item`.`provider_invoice_id` AS `payingFor`,
												`provider_credit_note`.`invoice_number` AS `referenceCode`,
												`provider_credit_note`.`document_number` AS `transactionCode`,
												'' AS `patient_id`,
												`provider_credit_note`.`provider_id` AS `recepientId`,
												'' AS `accountsclassfication`,
												`provider_credit_note_item`.`account_to_id` AS `accountId`,
												0 AS `accountFromId`,
												CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionName`,
												CONCAT(`provider`.`provider_name`,' ',`provider_credit_note_item`.`description`) AS `transactionDescription`,
												-`provider_credit_note_item`.`credit_note_amount` AS `amount`,
												`provider_credit_note`.`transaction_date` AS `transactionDate`,
												`provider_credit_note`.`created` AS `createdAt`,
												`provider_credit_note_item`.`provider_credit_note_item_status` AS `status`,
												provider.branch_id AS `branch_id`,
												'Provider Credit Note' AS `transactionCategory`,
												'Credit Note' AS `transactionClassification`,
												'provider_credit_note' AS `transactionTable`,
												'provider' AS `referenceTable`
											FROM
												`provider_credit_note_item`,provider_credit_note,provider_invoice,provider

											WHERE
											provider_credit_note.provider_credit_note_id = provider_credit_note_item.provider_credit_note_id
											AND provider_credit_note.provider_credit_note_status = 1
											AND provider_invoice.provider_invoice_id = provider_credit_note_item.provider_invoice_id
											AND provider_invoice.provider_invoice_status = 1
											AND provider.provider_id = provider_invoice.provider_id
											AND provider_invoice.transaction_date >= provider.start_date";

									$select .="
											UNION ALL

											SELECT
												`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
												`provider_payment`.`provider_payment_id` AS `referenceId`,
												`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
												`provider_payment`.`reference_number` AS `referenceCode`,
												`provider_payment`.`document_number` AS `transactionCode`,
												'' AS `patient_id`,
												`provider_payment`.`provider_id` AS `recepientId`,
												'' AS `accountsclassfication`,
												`provider_payment`.`account_from_id` AS `accountId`,
												0 AS `accountFromId`,
												`provider_payment_item`.`description` AS `transactionName`,
												CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
												-`provider_payment_item`.`amount_paid` AS `amount`,
												`provider_payment`.`transaction_date` AS `transactionDate`,
												`provider_payment`.`created` AS `createdAt`,
												`provider_payment_item`.`provider_payment_item_status` AS `status`,
												provider.branch_id AS `branch_id`,
												'Provider Invoices Payments' AS `transactionCategory`,
												'Creditor Invoices Payments' AS `transactionClassification`,
												'provider_payment' AS `transactionTable`,
												'provider' AS `referenceTable`
											FROM
												`provider_payment_item`,provider_payment,provider_invoice,provider

											WHERE provider_payment.provider_payment_id = provider_payment_item.provider_payment_id
												AND provider_payment.provider_payment_status = 1
												AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id
												AND provider_invoice.provider_invoice_status = 1
												AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date
											";



							}


							if($loan)
							{


								$select .= "
											UNION ALL

											SELECT
												`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
												`loan_invoice`.`loan_invoice_id` AS `referenceId`,
												'' AS `payingFor`,
												`loan_invoice`.`invoice_number` AS `referenceCode`,
												`loan_invoice`.`document_number` AS `transactionCode`,
												'' AS `patient_id`,
												`loan`.`loan_id`  AS `recepientId`,
												'' AS `accountsclassfication`,
												`loan`.`account_to_id` AS `accountId`,
												`loan`.`account_from_id` AS `accountFromId`,
												`loan_invoice_item`.`item_description` AS `transactionName`,
												`loan_invoice_item`.`item_description` AS `transactionDescription`,
												 `loan_invoice_item`.`total_amount` AS `amount`,
												`loan_invoice`.`transaction_date` AS `transactionDate`,
												`loan_invoice`.`created` AS `createdAt`,
												`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
												 loan.branch_id AS `branch_id`,
												'Loans Expense' AS `transactionCategory`,
												'Loans' AS `transactionClassification`,
												'loan_invoice_item' AS `transactionTable`,
												'loan' AS `referenceTable`
											FROM
												`loan_invoice_item`,loan_invoice,loan

											WHERE
												loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id
												AND loan_invoice.loan_invoice_status = 1
												AND loan.loan_id = loan_invoice.loan_id
												AND loan_invoice.transaction_date >= loan.start_date";

									$select .= "
										UNION ALL

										SELECT
											`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
											`loan_payment`.`loan_payment_id` AS `referenceId`,
											`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
											`loan_payment`.`reference_number` AS `referenceCode`,
											`loan_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
											`loan`.`loan_id` AS `recepientId`,
											'' AS `accountsclassfication`,
											`loan_payment`.`account_from_id` AS `accountId`,
											0 AS `accountFromId`,
											CONCAT('Payment from ') AS `transactionName`,
											CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
											-`loan_payment_item`.`amount_paid` AS `amount`,
											`loan_payment`.`transaction_date` AS `transactionDate`,
											`loan_payment`.`created` AS `createdAt`,
											`loan_payment_item`.`loan_payment_item_status` AS `status`,
											loan.branch_id AS `branch_id`,
											'Loan Payment' AS `transactionCategory`,
											'Loans Invoices Payments' AS `transactionClassification`,
											'loan_payment' AS `transactionTable`,
											'loan' AS `referenceTable`
										FROM
											`loan_payment_item`,loan_payment,loan_invoice,loan

										WHERE loan_payment_item.invoice_type = 0
											AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
											AND loan_payment.loan_payment_status = 1
											AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
											AND loan_invoice.loan_invoice_status = 1
											AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
											AND loan.account_from_id = loan_payment_item.account_to_id




									UNION ALL

									SELECT
											`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
											`loan_payment`.`loan_payment_id` AS `referenceId`,
											`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
											`loan_payment`.`reference_number` AS `referenceCode`,
											`loan_payment`.`document_number` AS `transactionCode`,
											'' AS `patient_id`,
											`loan`.`loan_id` AS `recepientId`,
											'' AS `accountsclassfication`,
											loan_payment_item.account_to_id AS `accountId`,
											loan_payment.account_from_id AS `accountFromId`,
											CONCAT('Payment ') AS `transactionName`,
											CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
											-`loan_payment_item`.`amount_paid` AS `amount`,
											`loan_payment`.`transaction_date` AS `transactionDate`,
											`loan_payment`.`created` AS `createdAt`,
											`loan_payment_item`.`loan_payment_item_status` AS `status`,
											loan.branch_id AS `branch_id`,
											'Loan Payment' AS `transactionCategory`,
											'Interest Expense' AS `transactionClassification`,
											'loan_payment' AS `transactionTable`,
											'loan' AS `referenceTable`
										FROM
											`loan_payment_item`,loan_payment,loan_invoice,loan

											WHERE loan_payment_item.invoice_type = 0
											AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
											AND loan_payment.loan_payment_status = 1
											AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
											AND loan_invoice.loan_invoice_status = 1
											AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
											AND loan.account_from_id <> loan_payment_item.account_to_id";


							}


							if($payroll)
							{

								$select .=	"
											UNION ALL


											SELECT
												payroll_summary.payroll_summary_id AS `transactionId`,
												payroll_summary.payroll_id AS `referenceId`,
												'' AS `payingFor`,
												'' AS `referenceCode`,
												'' AS `transactionCode`,
												'' AS `patient_id`,
												`statutory_accounts`.`account_id` AS `recepientId`,
												'' AS `accountsclassfication`,
												statutory_accounts.account_id AS `accountId`,
												0 AS `accountFromId`,
												CONCAT('Payroll') AS `transactionName`,
												CONCAT('Payroll') AS `transactionDescription`,
												payroll_summary.total_payroll AS `amount`,
												payroll_summary.payroll_created_for AS `transactionDate`,
												payroll_summary.payroll_created_for  AS `createdAt`,
												`payroll`.`payroll_status` AS `status`,
												payroll.branch_id AS branch_id,
												'Payroll Invoice' AS `transactionCategory`,
												'Expense' AS `transactionClassification`,
												'finance_purchase' AS `transactionTable`,
												'payroll' AS `referenceTable`
											FROM
												payroll_summary,payroll,statutory_accounts
											WHERE
												payroll.payroll_id = payroll_summary.payroll_id
												AND statutory_accounts.statutory_account_id = 1

											UNION ALL


											SELECT
												payroll_summary.payroll_summary_id AS `transactionId`,
												payroll_summary.payroll_id AS `referenceId`,
												'' AS `payingFor`,
												'' AS `referenceCode`,
												'' AS `transactionCode`,
												'' AS `patient_id`,
												`statutory_accounts`.`account_id` AS `recepientId`,
												'' AS `accountsclassfication`,
												statutory_accounts.account_id AS `accountId`,
												0 AS `accountFromId`,
												CONCAT('PAYE') AS `transactionName`,
												CONCAT('PAYE') AS `transactionDescription`,
												payroll_summary.paye AS `amount`,
												payroll_summary.payroll_created_for AS `transactionDate`,
												payroll_summary.payroll_created_for  AS `createdAt`,
												`payroll`.`payroll_status` AS `status`,
												payroll.branch_id AS branch_id,
												'Payroll Invoice' AS `transactionCategory`,
												'Expense' AS `transactionClassification`,
												'finance_purchase' AS `transactionTable`,
												'payroll' AS `referenceTable`
											FROM
												payroll_summary,payroll,statutory_accounts
											WHERE
												payroll.payroll_id = payroll_summary.payroll_id
												AND statutory_accounts.statutory_account_id = 2

											UNION ALL


											SELECT
												payroll_summary.payroll_summary_id AS `transactionId`,
												payroll_summary.payroll_id AS `referenceId`,
												'' AS `payingFor`,
												'' AS `referenceCode`,
												'' AS `transactionCode`,
												'' AS `patient_id`,
												`statutory_accounts`.`account_id` AS `recepientId`,
												'' AS `accountsclassfication`,
												statutory_accounts.account_id AS `accountId`,
												0 AS `accountFromId`,
												CONCAT('NSSF') AS `transactionName`,
												CONCAT('NSSF') AS `transactionDescription`,
												payroll_summary.nssf AS `amount`,
												payroll_summary.payroll_created_for AS `transactionDate`,
												payroll_summary.payroll_created_for  AS `createdAt`,
												`payroll`.`payroll_status` AS `status`,
												payroll.branch_id AS branch_id,
												'Payroll Invoice' AS `transactionCategory`,
												'Expense' AS `transactionClassification`,
												'finance_purchase' AS `transactionTable`,
												'payroll' AS `referenceTable`
											FROM
												payroll_summary,payroll,statutory_accounts
											WHERE
												payroll.payroll_id = payroll_summary.payroll_id
												AND statutory_accounts.statutory_account_id = 3



											UNION ALL




											SELECT
												payroll_summary.payroll_summary_id AS `transactionId`,
												payroll_summary.payroll_id AS `referenceId`,
												'' AS `payingFor`,
												'' AS `referenceCode`,
												'' AS `transactionCode`,
												'' AS `patient_id`,
												`statutory_accounts`.`account_id` AS `recepientId`,
												'' AS `accountsclassfication`,
												statutory_accounts.account_id AS `accountId`,
												0 AS `accountFromId`,
												CONCAT('NHIF') AS `transactionName`,
												CONCAT('NHIF') AS `transactionDescription`,
												payroll_summary.nhif AS `amount`,
												payroll_summary.payroll_created_for AS `transactionDate`,
												payroll_summary.payroll_created_for  AS `createdAt`,
												`payroll`.`payroll_status` AS `status`,
												payroll.branch_id AS branch_id,
												'Payroll Invoice' AS `transactionCategory`,
												'Expense' AS `transactionClassification`,
												'Payroll' AS `transactionTable`,
												'payroll' AS `referenceTable`
											FROM
												payroll_summary,payroll,statutory_accounts
											WHERE
												payroll.payroll_id = payroll_summary.payroll_id
												AND statutory_accounts.statutory_account_id = 4";

									$select .="

											UNION ALL

											SELECT
												`payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
												`payroll_payment`.`payroll_payment_id` AS `referenceId`,
												`payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
												`payroll_payment`.`reference_number` AS `referenceCode`,
												`payroll_payment`.`document_number` AS `transactionCode`,
												'' AS `patient_id`,
												`statutory_accounts`.`account_id` AS `recepientId`,
												'' AS `accountsclassfication`,
												`statutory_accounts`.`account_id` AS `accountId`,
												0 AS `accountFromId`,
												CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
												CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
												-`payroll_payment_item`.`amount_paid` AS `amount`,
												`payroll_payment`.`transaction_date` AS `transactionDate`,
												`payroll_payment`.`created` AS `createdAt`,
												`payroll_payment_item`.`payroll_payment_item_status` AS `status`,
												2 AS `branch_id`,
												'Payroll Payment' AS `transactionCategory`,
												'Payroll Payments' AS `transactionClassification`,
												'payroll_payment' AS `transactionTable`,
												'payroll' AS `referenceTable`
											FROM
												`payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts

											WHERE payroll_payment_item.invoice_type = 0
											AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id
											AND payroll_payment.payroll_payment_status = 1
											AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id
											AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id";


							}



					$select .="
							) AS data WHERE data.transactionId > 0 ".$add." GROUP BY referenceTable";

				$query =$this->db->query($select);

				// echo "<pre>";
				// print_r($query->result());
				// echo "</pre>";

				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						// code...
						$balance =0;
						$recepientId = $value->recepientId;
						$balance = $value->balance;
						$referenceTable = $value->referenceTable;

						$date_tomorrow = date("Y-m-d", strtotime("+1 day", strtotime($end_period)));



						// opening balance

						if($referenceTable == "creditor")
						{
							$sql = "insert ignore into creditor_opening_balance (account_id, opening_balance_date, balance)
								values (".$accounts_payable_id.",'".$date_tomorrow."',".$balance.")
								on duplicate key update balance = ".$balance."";

							$this->db->query($sql);
						}
						else if($referenceTable == "provider")
						{

							$sql = "insert ignore into provider_opening_balance (account_id, opening_balance_date, balance)
								values (".$providers_liability_id.",'".$date_tomorrow."',".$balance.")
								on duplicate key update balance = ".$balance."";

							$this->db->query($sql);
						}


						else if($referenceTable == "loan")
						{

							$sql = "insert ignore into loan_opening_balance (account_id, opening_balance_date, balance)
								values (".$loans_liability_id.",'".$date_tomorrow."',".$balance.")
								on duplicate key update balance = ".$balance."";

							$this->db->query($sql);
						}
						else if($referenceTable == "payroll")
						{

							$sql = "insert ignore into payroll_opening_balance (account_id, opening_balance_date, balance)
								values (".$payroll_liability_id.",'".$date_tomorrow."',".$balance.")
								on duplicate key update balance = ".$balance."";

							$this->db->query($sql);
						}
					}
				}
			}
		}

	}



	public function get_account_balance($account_id)
	{


		$query_list = $this->company_financial_model->get_account_transactions_ledgers(NULL,NULL,0,0,2,date('Y-m-d'), date('Y-m-d'),4,$account_id);
        $grouped_array_checked = array();
        foreach ($query_list->result() as $element_two) {
            $grouped_array_checked[$element_two->accountId] = $element_two;
        }

        $opening_balance_cr_amount =  $grouped_array_checked[$account_id]->cr_amount;
        $opening_balance_dr_amount =  $grouped_array_checked[$account_id]->dr_amount;

        $opening_balance = $opening_balance_dr_amount - $opening_balance_cr_amount;


        return $opening_balance;
	}


	public function get_accounts_list()
	{
		$this->db->select('account.account_id,account.account_name,account_type.account_type_id,account_type.account_type_name');
		$this->db->where('account.account_type_id = account_type.account_type_id');
		$query_list = $this->db->get('account,account_type');

		return $query_list;
	}

	public function get_payments_split()
	{

	}

	public function calculateBalances($args=null)
	{

			$currentYear = date('Y');


			$startYear = $args['startYear'];
			$currentYear = $args['currentYear'];
			$financialEndMonth = $args['financialEndMonth'];
			// $accountId = $args['accountId'];

			$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

			$patients = array();

			if($accounts_config_rs->num_rows() > 0)
			{
				foreach ($accounts_config_rs->result() as $key => $value) {
					// code...
					$staing_account_id = $value->account_id;
					$reference_name = $value->reference_name;

					$session_account[$reference_name] = $staing_account_id;





				}
			}

			$providers_liability_id = $session_account['providers_liability_id'];
			$providers_wht_id = $session_account['providers_wht_id'];
			$payroll_liability_id = $session_account['payroll_liability_id'];
			$accounts_payable_id = $session_account['accounts_payable_id'];
			$fixed_assets_id = $session_account['fixed_assets_id'];
			$accounts_receivable_id = $session_account['accounts_receivable_id'];
			$suppliers_wht_id = $session_account['suppliers_wht_id'];
			$income_account_id = $session_account['income_account_id'];
			$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
			$bank_opening_balance_id = $session_account['bank_opening_balance_id'];
			$provider_opening_balance_id = $session_account['provider_opening_balance_id'];



			if($financialEndMonth < 10 ? "0".$financialEndMonth:$financialEndMonth);

			for ($i=$startYear; $i <= $currentYear; $i++) {
				// code...


				$start_date = $i.'-01-01';
				$end_period = $i.'-'.$financialEndMonth.'-31';

				// $add = ' AND data.transactionDate >= "'.$start_date.'" AND  data.transactionDate <= "'.$end_period.'" ';

				$start_date = $i.'-01-01';
				$end_period = $i.'-'.$financialEndMonth.'-31';

				$financial_class = new Financial_Report(array("cal_opening_balance"=>true,
					"date" => $end_period));

				$date_tomorrow = date("Y-m-d", strtotime("+1 day", strtotime($end_period)));

				// echo "<br>Printing final $i";
				// echo "<pre>";

				// print_r($financial_class->arrAssetsData());

				foreach ($financial_class->arrAssetsData()["assets"] as $key => $value) {
					$account_id = $key;
					$name = $value['name'];
					$amount = $value['amount'];

					// echo "<br>Printing final $end_period : $i $name $amount";



					if($account_id != $accounts_receivable_id AND $amount <> 0)
					{

						$sql = "insert ignore into account_opening_balance (account_id,opening_balance_date, balance)
						values (".$account_id.",'".$date_tomorrow."',".$amount.")
						on duplicate key update balance = ".$amount.",status = 0";

						$this->db->query($sql);
					}


				}
					// echo "</pre>";


				// foreach ($financial_class->arrAssetsData()["equities"] as $key => $value) {
				// 	$account_id = $key;
				// 	$name = $value['name'];
				// 	$amount = $value['amount'];



				// 	if($account_id != $accounts_receivable_id AND $amount <> 0)
				// 	{

				// 		$sql = "insert ignore into account_opening_balance (account_id,opening_balance_date, balance)
				// 		values (".$account_id.",'".$date_tomorrow."',".$amount.")
				// 		on duplicate key update balance = ".$amount."";
				// 		$this->db->query($sql);
				// 	}


				// }


				// echo "<br>Printing final $i";
				// echo "<pre>";

				// print_r($financial_class->arrAssetsData()["assets"][38]);
				// echo "</pre>";


			}

	}
	public function calculateRetainedEarnings($args=null)
	{
		include_once "application/modules/financials/controllers/financial_report.php";




		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();

		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;





			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];
		$provider_opening_balance_id = $session_account['provider_opening_balance_id'];

		$startYear = $args['startYear'];
		$retained_earnings_id = $session_account['retained_earnings_id'];

		$this->db->where('account_id ='.$retained_earnings_id.' AND YEAR(opening_balance_date) = "'.$startYear.'" ');
		$query = $this->db->get('account_opening_balance');
		$opening_balance = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$opening_balance = $value->balance;
			}
		}


		if(is_array($args))
		{

			$startYear = $args['startYear'];
			$currentYear = $args['currentYear'];
			$financialEndMonth = $args['financialEndMonth'];
			$accountId = $args['accountId'];

			if($financialEndMonth < 10 ? "0".$financialEndMonth:$financialEndMonth);

			for ($i=$startYear; $i <= $currentYear; $i++) {
				// code...


				$start_date = $startYear.'-01-01';
				$end_period = $i.'-'.$financialEndMonth.'-31';
				// var_dump($end_period);die();
				$financial_class = new Financial_Report(array("pnl"=>true,
					"date" => $end_period));
				// echo "<br>Printing final";
				// echo "<pre>";
				// print_r($financial_class->arrData());
				// echo "</pre>";

				$total_income = 0;
				foreach ($financial_class->arrData()["incomes"] as $key => $value) {

						$name = $value['name'];
						$amount = $value['amount'];


						$total_income += $amount;


				}


				$total_expenses = 0;
				foreach ($financial_class->arrData()["expenses"] as $key => $value) {

						$name = $value['name'];
						$amount = $value['amount'];


						$total_expenses += $amount;


				}



				$profitorloss = $total_income + $opening_balance - $total_expenses;


				$date_tomorrow = date("Y-m-d", strtotime("+1 day", strtotime($end_period)));

				if(empty($profitorloss))
				{
					$profitorloss = 0;
				}
				// opening balance

				$sql = "insert ignore into account_opening_balance (account_id,opening_balance_date, balance)
						values (".$accountId.",'".$date_tomorrow."',".$profitorloss.")
						on duplicate key update balance = ".$profitorloss.",status = 0";

				$this->db->query($sql);



			}
		}
	}
	public function calculateAccountOpeningBalances($args=null)
	{

		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();

		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;


			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];
		$provider_opening_balance_id = $session_account['provider_opening_balance_id'];


		$date_from = $args["dateFrom"];
		$date_to = $args["dateTo"];
		$add =  ' AND DATE(data.transactionDate) >= "'.$date_from.'" AND DATE(data.transactionDate) <= "'.$date_to.'" ';

		// $add =  ' AND DATE(data.transactionDate) >= "2022-01-01" AND DATE(data.transactionDate) <= "2022-12-01" AND data.account_id = 38 ';







		$select  = "
					SELECT
						*
					FROM

					(

					";


					$select .="
								SELECT
								`account`.`account_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								'' AS `transactionCode`,
								'' AS `patient_id`,
								'' AS `recepientId`,
								'' AS `accountsclassfication`,
								account.account_id AS `accountId`,
								0 AS `accountFromId`,
								CONCAT('Opening Balance as from',' ',`account`.`start_date`) AS `transactionName`,
								CONCAT('Opening Balance as from',' ',' ',`account`.`start_date`) AS `transactionDescription`,
								`account`.`account_opening_balance` AS `amount`,
								`account`.`start_date` AS `transactionDate`,
								`account`.`start_date` AS `createdAt`,
								`account`.`account_status` AS `status`,
								branch_id AS `branch_id`,
								'Fund Accounts' AS `transactionCategory`,
								'Account Opening Balance' AS `transactionClassification`,
								'' AS `transactionTable`,
								'account' AS `referenceTable`
								FROM
								account

							WHERE

							 	account.account_opening_balance <> 0 AND account.parent_account <> 0";
					$select .="
								UNION ALL


								SELECT
									`finance_transfer`.`finance_transfer_id` AS `transactionId`,
									`finance_transfer`.`finance_transfer_id` AS `referenceId`,
									'' AS `payingFor`,
									`finance_transfer`.`reference_number` AS `referenceCode`,
									`finance_transfer`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									`finance_transfer`.`account_to_id` AS `accountId`,
									`finance_transfer`.`account_from_id` AS `accountFromId`,
									`finance_transfer`.`remarks` AS `transactionName`,
									CONCAT('Amount Received ',' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
									`finance_transfer`.`finance_transfer_amount` AS `amount`,
									`finance_transfer`.`transaction_date` AS `transactionDate`,
									`finance_transfer`.`created` AS `createdAt`,
									`finance_transfer`.`finance_transfer_status` AS `status`,
									`finance_transfer`.`branch_id` AS `branch_id`,
									'Transfer' AS `transactionCategory`,
									'Transfer' AS `transactionClassification`,
									'finance_transfer' AS `transactionTable`,
									'finance_transfered' AS `referenceTable`
								FROM
								`finance_transfer`
								WHERE  finance_transfer.finance_transfer_deleted = 0
								AND finance_transfer.finance_transfer_status = 1 ";



					$select .="
								UNION ALL

								SELECT
									`finance_purchase`.`finance_purchase_id` AS `transactionId`,
									'' AS `referenceId`,
									`finance_purchase`.`finance_purchase_id` AS `payingFor`,
									`finance_purchase`.`transaction_number` AS `referenceCode`,
									`finance_purchase`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
									finance_purchase.creditor_id AS `recepientId`,
									'' AS `accountsclassfication`,
									`finance_purchase`.`account_to_id` AS `accountId`,
									`finance_purchase`.`account_from_id` AS `accountFromId`,
									`finance_purchase`.`finance_purchase_description` AS `transactionName`,
									CONCAT( `finance_purchase`.`finance_purchase_description`) AS `transactionDescription`,
									`finance_purchase`.`finance_purchase_amount`  AS `amount`,
									`finance_purchase`.`transaction_date` AS `transactionDate`,
									`finance_purchase`.`created` AS `createdAt`,
									`finance_purchase`.`finance_purchase_status` AS `status`,
									`finance_purchase`.`branch_id` AS `branch_id`,
									'Petty Cash' AS `transactionCategory`,
									'Expense' AS `transactionClassification`,
									'finance_purchase' AS `transactionTable`,
									'finance_purchase_payment' AS `referenceTable`
								FROM
									finance_purchase
								WHERE
									finance_purchase.finance_purchase_deleted = 0
									AND finance_purchase.account_to_id > 0 ";
					$select .= "

							UNION ALL

							SELECT
								`account_payments`.`account_payment_id` AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								'' AS `referenceCode`,
								account_payments.receipt_number AS `transactionCode`,
								'' AS `patient_id`,
								'' AS `recepientId`,
								'' AS `accountsclassfication`,
								account_payments.account_to_id AS `accountId`,
								account_payments.account_from_id AS `accountFromId`,
								CONCAT(account_payments.account_payment_description) AS `transactionName`,
								CONCAT(account_payments.account_payment_description) AS `transactionDescription`,
								`account_payments`.`amount_paid` AS `amount`,
								`account_payments`.`payment_date` AS `transactionDate`,
								`account_payments`.`payment_date` AS `createdAt`,
								`account_payments`.`account_payment_status` AS `status`,
								account_payments.branch_id AS `branch_id`,
								'Journal Credit' AS `transactionCategory`,
								'Journal' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'account' AS `referenceTable`
							FROM
								account_payments

							WHERE account_payments.account_payment_deleted = 0 ";



					$select .="
								UNION ALL


								SELECT
									`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
									`creditor_payment`.`creditor_payment_id` AS `referenceId`,
									`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
									`creditor_payment`.`reference_number` AS `referenceCode`,
									`creditor_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
									`creditor_payment`.`creditor_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									`creditor_payment`.`account_from_id` AS `accountId`,
									0 AS `accountFromId`,
									`creditor_payment_item`.`description` AS `transactionName`,
									CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
									`creditor_payment_item`.`amount_paid` AS `amount`,
									`creditor_payment`.`transaction_date` AS `transactionDate`,
									`creditor_payment`.`created` AS `createdAt`,
									`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
									 creditor.branch_id AS `branch_id`,
									'Creditor Payment' AS `transactionCategory`,
									'Creditors Invoices Payments' AS `transactionClassification`,
									'creditor_payment' AS `transactionTable`,
									'creditor' AS `referenceTable`
								FROM

									`creditor_payment_item`,creditor_payment,creditor_invoice,creditor


								WHERE creditor_payment_item.invoice_type = 0
									AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
									AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
									AND creditor_payment.creditor_payment_status = 1
									AND creditor_invoice.creditor_invoice_status = 1
									AND creditor_invoice.creditor_id = creditor.creditor_id
									AND creditor_invoice.transaction_date >= creditor.start_date




								UNION ALL

								SELECT
									`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
									`creditor_payment`.`creditor_payment_id` AS `referenceId`,
									`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
									`creditor_payment`.`reference_number` AS `referenceCode`,
									`creditor_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
									`creditor_payment`.`creditor_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									`creditor_payment`.`account_from_id` AS `accountId`,
									0 AS `accountFromId`,
									`creditor_payment_item`.`description` AS `transactionName`,
									CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
									`creditor_payment_item`.`amount_paid` AS `amount`,
									`creditor_payment`.`transaction_date` AS `transactionDate`,
									`creditor_payment`.`created` AS `createdAt`,
									`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
									 creditor.branch_id AS `branch_id`,
									'Creditor Payment' AS `transactionCategory`,
									'Creditors Opening Balance Payment' AS `transactionClassification`,
									'creditor_payment' AS `transactionTable`,
									'creditor' AS `referenceTable`
								FROM

									`creditor_payment_item`,creditor_payment,creditor

								WHERE creditor_payment_item.invoice_type = 2
								AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
								AND creditor.creditor_id = creditor_payment_item.creditor_id
								AND creditor_payment.transaction_date >= creditor.start_date


							UNION ALL

								SELECT
									`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
									`creditor_payment`.`creditor_payment_id` AS `referenceId`,
									`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
									`creditor_payment`.`reference_number` AS `referenceCode`,
									`creditor_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
									`creditor_payment`.`creditor_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									`creditor_payment`.`account_from_id` AS `accountId`,
									0 AS `accountFromId`,
									`creditor_payment_item`.`description` AS `transactionName`,
									CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
									`creditor_payment_item`.`amount_paid` AS `amount`,
									`creditor_payment`.`transaction_date` AS `transactionDate`,
									`creditor_payment`.`created` AS `createdAt`,
									`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
									 creditor.branch_id AS `branch_id`,
									'Creditor Payment' AS `transactionCategory`,
									'Creditors Invoices Payments' AS `transactionClassification`,
									'creditor_payment' AS `transactionTable`,
									'creditor' AS `referenceTable`
								FROM

									`creditor_payment_item`,creditor_payment,creditor

								WHERE
									creditor_payment_item.invoice_type = 3
									AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
									AND creditor.creditor_id = creditor_payment_item.creditor_id
									AND creditor_payment.transaction_date >= creditor.start_date";

						$select .="

								UNION ALL

									SELECT
										`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
										`creditor_payment`.`creditor_payment_id` AS `referenceId`,
										`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
										`creditor_payment`.`reference_number` AS `referenceCode`,
										`creditor_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
										`creditor_payment`.`creditor_id` AS `recepientId`,
										'' AS `accountsclassfication`,
										creditor_payment.account_from_id AS `accountId`,
										0 AS `accountFromId`,
										`creditor_payment_item`.`description` AS `transactionName`,
										CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
										`creditor_payment_item`.`amount_paid` AS `amount`,
										`creditor_payment`.`transaction_date` AS `transactionDate`,
										`creditor_payment`.`created` AS `createdAt`,
										`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
										 creditor.branch_id AS `branch_id`,
										'Creditor Payment' AS `transactionCategory`,
										'Creditors Invoices Payments' AS `transactionClassification`,
										'creditor_payment' AS `transactionTable`,
										'creditor' AS `referenceTable`
									FROM

									`creditor_payment_item`,creditor_payment,orders,creditor

									WHERE creditor_payment_item.invoice_type = 1
										AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
										AND creditor_payment.creditor_payment_status = 1
										AND orders.order_id = creditor_payment_item.creditor_invoice_id
										AND orders.supplier_id = creditor.creditor_id
										AND creditor_payment.transaction_date >= creditor.start_date";


						$select .="
									UNION ALL

									SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
										`provider_payment`.`provider_id` AS `recepientId`,
										'' AS `accountsclassfication`,
										`provider_payment`.`account_from_id` AS `accountId`,
										0 AS `accountFromId`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
										`provider_payment_item`.`amount_paid` AS `amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										provider.branch_id AS `branch_id`,
										'Provider Invoices Payments' AS `transactionCategory`,
										'Creditor Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider' AS `referenceTable`
									FROM
										`provider_payment_item`,provider_payment,provider_invoice,provider

									WHERE provider_payment_item.invoice_type = 0
										AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id
										AND provider_payment.provider_payment_status = 1
										AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id
										AND provider_invoice.provider_invoice_status = 1
										AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date



									UNION ALL

									SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
										`provider_payment`.`provider_id` AS `recepientId`,
										'' AS `accountsclassfication`,
										`provider_payment`.`account_from_id` AS `accountId`,
										0 AS `accountFromId`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Payment of opening balance')  AS `transactionDescription`,
										`provider_payment_item`.`amount_paid`  AS `amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										provider.branch_id AS `branch_id`,
										'Provider Invoices Payments' AS `transactionCategory`,
										'Creditors Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider' AS `referenceTable`
									FROM
										`provider_payment_item`,provider_payment,provider

									WHERE provider_payment_item.invoice_type = 2
										AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id
										AND provider_payment.provider_payment_status = 1
										AND provider.provider_id = provider_payment_item.provider_id
										AND provider_payment.transaction_date >= provider.start_date



									UNION ALL

									SELECT
										`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
										`provider_payment`.`provider_payment_id` AS `referenceId`,
										`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
										`provider_payment`.`reference_number` AS `referenceCode`,
										`provider_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
									  `provider_payment`.`provider_id` AS `recepientId`,
										'' AS `accountsclassfication`,
										`provider_payment`.`account_from_id` AS `accountId`,
										0 AS `accountFromId`,
										`provider_payment_item`.`description` AS `transactionName`,
										CONCAT('Payment on account')  AS `transactionDescription`,
										`provider_payment_item`.`amount_paid` AS `amount`,
										`provider_payment`.`transaction_date` AS `transactionDate`,
										`provider_payment`.`created` AS `createdAt`,
										`provider_payment_item`.`provider_payment_item_status` AS `status`,
										provider.branch_id AS `branch_id`,
										'Provider Invoices Payments' AS `transactionCategory`,
										'Creditors Invoices Payments' AS `transactionClassification`,
										'provider_payment' AS `transactionTable`,
										'provider' AS `referenceTable`
									FROM
										`provider_payment_item`,provider_payment,provider

									WHERE
											provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id
											AND provider_payment.provider_payment_status = 1
											AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date";
					$select .= "
								UNION ALL

								SELECT
									`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
									`loan_invoice`.`loan_invoice_id` AS `referenceId`,
									'' AS `payingFor`,
									`loan_invoice`.`invoice_number` AS `referenceCode`,
									`loan_invoice`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
										`loan_invoice`.`loan_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									`loan`.`account_to_id` AS `accountId`,
									`loan`.`account_from_id` AS `accountFromId`,
									`loan_invoice_item`.`item_description` AS `transactionName`,
									`loan_invoice_item`.`item_description` AS `transactionDescription`,
									 `loan_invoice_item`.`total_amount` AS `amount`,
									`loan_invoice`.`transaction_date` AS `transactionDate`,
									`loan_invoice`.`created` AS `createdAt`,
									`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
									 loan.branch_id AS `branch_id`,
									'Loans Expense' AS `transactionCategory`,
									'Loans' AS `transactionClassification`,
									'loan_invoice_item' AS `transactionTable`,
									'loan_invoice' AS `referenceTable`
								FROM
									`loan_invoice_item`,loan_invoice,loan

								WHERE
									loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id
									AND loan_invoice.loan_invoice_status = 1
									AND loan.loan_id = loan_invoice.loan_id
									AND loan_invoice.transaction_date >= loan.start_date";



					$select .= "
								UNION ALL

								SELECT
									`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
									`loan_payment`.`loan_payment_id` AS `referenceId`,
									`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
									`loan_payment`.`reference_number` AS `referenceCode`,
									`loan_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
									`loan`.`loan_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									`loan_payment`.`account_from_id` AS `accountId`,
									0 AS `accountFromId`,
									CONCAT('Payment from ') AS `transactionName`,
									CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
									`loan_payment_item`.`amount_paid` AS `amount`,
									`loan_payment`.`transaction_date` AS `transactionDate`,
									`loan_payment`.`created` AS `createdAt`,
									`loan_payment_item`.`loan_payment_item_status` AS `status`,
									loan.branch_id AS `branch_id`,
									'Loan Payment' AS `transactionCategory`,
									'Loans Invoices Payments' AS `transactionClassification`,
									'loan_payment' AS `transactionTable`,
									'loan' AS `referenceTable`
								FROM
									`loan_payment_item`,loan_payment,loan_invoice,loan

								WHERE loan_payment_item.invoice_type = 0
									AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
									AND loan_payment.loan_payment_status = 1
									AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
									AND loan_invoice.loan_invoice_status = 1
									AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
									AND loan.account_from_id = loan_payment_item.account_to_id

								UNION ALL

								SELECT
										`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
										`loan_payment`.`loan_payment_id` AS `referenceId`,
										`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
										`loan_payment`.`reference_number` AS `referenceCode`,
										`loan_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
										`loan_payment`.`loan_id` AS `recepientId`,
										'' AS `accountsclassfication`,
										loan_payment_item.account_to_id AS `accountId`,
										loan_payment.account_from_id AS `accountFromId`,
										CONCAT('Payment ') AS `transactionName`,
										CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
										`loan_payment_item`.`amount_paid` AS `amount`,
										`loan_payment`.`transaction_date` AS `transactionDate`,
										`loan_payment`.`created` AS `createdAt`,
										`loan_payment_item`.`loan_payment_item_status` AS `status`,
										loan.branch_id AS `branch_id`,
										'Loan Payment' AS `transactionCategory`,
										'Interest Expense' AS `transactionClassification`,
										'loan_payment' AS `transactionTable`,
										'loan_payment_item' AS `referenceTable`
									FROM
										`loan_payment_item`,loan_payment,loan_invoice,loan

										WHERE loan_payment_item.invoice_type = 0
										AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
										AND loan_payment.loan_payment_status = 1
										AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
										AND loan_invoice.loan_invoice_status = 1
										AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
										AND loan.account_from_id <> loan_payment_item.account_to_id";





						$select .="

									UNION ALL

									SELECT
										`payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
										`payroll_payment`.`payroll_payment_id` AS `referenceId`,
										`payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
										`payroll_payment`.`reference_number` AS `referenceCode`,
										`payroll_payment`.`document_number` AS `transactionCode`,
										'' AS `patient_id`,
										`statutory_accounts`.`account_id` AS `recepientId`,
										'' AS `accountsclassfication`,
										`payroll_payment`.`account_from_id` AS `accountId`,
										0 AS `accountFromId`,
										CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
										CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
										`payroll_payment_item`.`amount_paid` AS `amount`,
										`payroll_payment`.`transaction_date` AS `transactionDate`,
										`payroll_payment`.`created` AS `createdAt`,
										`payroll_payment_item`.`payroll_payment_item_status` AS `status`,
										2 AS `branch_id`,
										'Payroll Payment' AS `transactionCategory`,
										'Payroll Payments' AS `transactionClassification`,
										'payroll_payment' AS `transactionTable`,
										'payroll' AS `referenceTable`
									FROM
										`payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts

									WHERE payroll_payment_item.invoice_type = 0
									AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id
									AND payroll_payment.payroll_payment_status = 1
									AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id
									AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id";

						$select .=	"
										UNION ALL


										SELECT

											payments.payment_id AS `transactionId`,
											'' AS `referenceId`,
											'' AS `payingFor`,
											payments.confirm_number AS `referenceCode`,
											payments.confirm_number AS `transactionCode`,
											payments.patient_id AS `patient_id`,
											'' AS `recepientId`,
											'' AS `accountsclassfication`,
											`payment_method`.`account_id` AS `accountId`,
											0 AS `accountFromId`,
											'' AS `transactionName`,
											CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
											payment_item.payment_item_amount AS `amount`,
											payments.payment_date AS `transactionDate`,
											payments.payment_date AS `createdAt`,
											`payments`.`cancel` AS `status`,
											visit_invoice.branch_id AS `branch_id`,
											'Receivable Payment' AS `transactionCategory`,
											'Payments' AS `transactionClassification`,
											'account_payments' AS `transactionTable`,
											'' AS `referenceTable`
										FROM
											payments,payment_item,payment_method,visit_invoice
										WHERE
											payments.cancel = 0 AND payments.payment_type = 1
											AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id
											AND payment_item.invoice_type = 1
											AND payments.payment_id = payment_item.payment_id
											AND payments.payment_method_id = payment_method.payment_method_id


									UNION ALL


										SELECT

											payments.payment_id AS `transactionId`,
											'' AS `referenceId`,
											'' AS `payingFor`,
											payments.confirm_number AS `referenceCode`,
											payments.confirm_number AS `transactionCode`,
											payments.patient_id AS `patient_id`,
											'' AS `recepientId`,
											'' AS `accountsclassfication`,
											`payment_method`.`account_id` AS `accountId`,
											0 AS `accountFromId`,
											'' AS `transactionName`,
											CONCAT('Payment On Account') AS `transactionDescription`,
											(payment_item.payment_item_amount) AS `amount`,
											payments.payment_date AS `transactionDate`,
											payments.payment_date AS `createdAt`,
											`payments`.`cancel` AS `status`,
											payments.branch_id AS `branch_id`,
											'Receivable Payment' AS `transactionCategory`,
											'Prepayments' AS `transactionClassification`,
											'account_payments' AS `transactionTable`,
											'' AS `referenceTable`
										FROM
											payments,payment_item,payment_method
										WHERE
											payments.cancel = 0
											AND payments.payment_type = 1
											AND payment_item.invoice_type = 3
											AND payments.payment_id = payment_item.payment_id
											AND payments.payment_method_id = payment_method.payment_method_id";
					$select .="

								UNION ALL

								SELECT
									batch_receipts.batch_receipt_id AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									batch_receipts.receipt_number AS `referenceCode`,
									batch_receipts.receipt_number AS `transactionCode`,
									batch_unallocations.unallocated_payment_id AS `patient_id`,
									0 AS `recepientId`,
									'' AS `accountsclassfication`,
									batch_receipts.bank_id AS `accountId`,
									0 AS `accountFromId`,
									CONCAT('BATCH OVERPAYMENT :',' : ',batch_unallocations.reason) AS `transactionName`,
									CONCAT('BATCH OVERPAYMENT :',' : ',batch_unallocations.reason) AS `transactionDescription`,
									batch_unallocations.amount_paid AS `amount`,
									batch_receipts.payment_date AS `transactionDate`,
									batch_receipts.payment_date  AS `createdAt`,
									1 AS `status`,
									batch_receipts.branch_id AS `branch_id`,
									'Prepayments' AS `transactionCategory`,
									'Insuance Overpayments' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'' AS `referenceTable`
								FROM
										batch_unallocations,batch_receipts

								WHERE
									batch_receipts.batch_receipt_id = batch_unallocations.batch_receipt_id
									AND batch_unallocations.unallocated_payment_delete = 0
									AND  batch_unallocations.allocation_type_id = 0";
					$select .="

							UNION ALL

								SELECT
									`journal_entry`.`journal_entry_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									journal_entry.document_number AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									journal_entry.account_to_id AS `accountId`,
									journal_entry.account_from_id AS `accountFromId`,
									CONCAT(journal_entry.journal_entry_description)  AS `transactionName`,
									CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
									`journal_entry`.`amount_paid` AS `amount`,
									`journal_entry`.`payment_date` AS `transactionDate`,
									`journal_entry`.`payment_date` AS `createdAt`,
									`journal_entry`.`journal_entry_status` AS `status`,
									journal_entry.branch_id AS `branch_id`,
									'Journal Credit' AS `transactionCategory`,
									'Journal Entry' AS `transactionClassification`,
									'journal_entry' AS `transactionTable`,
									'account' AS `referenceTable`
								FROM
									journal_entry

								WHERE journal_entry.journal_entry_deleted = 0";




			$select .="
				) AS data WHERE data.transactionId > 0 ".$add." ";

			$query =$this->db->query($select);


			return $query;



	}

	public function calculateReceivablesOpeningBal($args=null)
	{

		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();

		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;


			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];
		$provider_opening_balance_id = $session_account['provider_opening_balance_id'];


		if(is_array($args))
		{

			$startYear = $args['startYear'];
			$currentYear = $args['currentYear'];
			$financialEndMonth = $args['financialEndMonth'];
			$transaction_type = $args['type'];
			$payable_id = $args['payable_id'];

			if($financialEndMonth < 10 ? "0".$financialEndMonth:$financialEndMonth);

			for ($i=$startYear; $i <= $currentYear; $i++) {
				// code...


				$start_date = $startYear.'-01-01';
				$end_period = $i.'-'.$financialEndMonth.'-31';

				$add = ' AND data.transactionDate >= "'.$start_date.'" AND  data.transactionDate <= "'.$end_period.'" ';




				$select  = "
							SELECT
								data.recepientId AS recepientId,
								COALESCE (SUM(data.amount),0) AS balance,
								data.transactionDate AS endDate,
								data.referenceTable AS referenceTable
							FROM

							(

							";


							$select .="
									SELECT

										visit_invoice.visit_invoice_id AS transactionId,
										visit_charge.visit_charge_id  AS referenceId,
										'' AS `payingFor`,
										visit_invoice.visit_invoice_number AS referenceCode,
										'' AS transactionCode,
										visit_invoice.patient_id AS patientId,
										'' AS recepientId,
										'' AS `accountsclassfication`,
										visit_charge.account_id AS `accountId`,
										0 AS `accountFromId`,
										visit_charge.charge_name AS transactionName,
										CONCAT( '<strong>Invoice </strong>  Invoice Number :  ', visit_invoice.visit_invoice_number ) AS transactionDescription,
										visit_charge.visit_charge_amount*visit_charge.visit_charge_units  AS amount,
										visit_invoice.created AS `transactionDate`,
										visit_invoice.created AS `createdAt`,
										1 AS `status`,
										visit_invoice.branch_id AS `branch_id`,
										'Receivable Invoice' AS `transactionCategory`,
										'Income' AS `transactionClassification`,
										'visit_invoice' AS `transactionTable`,
										'visit_charge' AS `referenceTable`

									FROM
										visit_charge,visit_invoice
									WHERE
										visit_charge.charged = 1
										AND visit_invoice.visit_invoice_delete = 0
										AND visit_charge.visit_charge_delete = 0
										AND visit_invoice.visit_invoice_id = visit_charge.visit_invoice_id";

								$select .="

										UNION ALL

											SELECT
												visit_credit_note.visit_credit_note_id AS transactionId,
												visit_invoice.visit_invoice_id AS referenceId,
												'' AS `payingFor`,
												visit_credit_note.visit_cr_note_number AS referenceCode,
												visit_credit_note.visit_cr_note_number AS transactionCode,
												visit_invoice.patient_id AS patient_id,
												'' AS recepientId,
												'' AS `accountsclassfication`,
												visit_credit_note_item.account_id AS `accountId`,
												0 AS `accountFromId`,
												visit_credit_note_item.charge_name AS transactionName,
												CONCAT('<strong>Credit Note for </strong>  Invoice Number: ', visit_invoice.visit_invoice_number,'<strong>Credit Note No:</strong> ',visit_credit_note.visit_cr_note_number) AS transactionDescription,

												-visit_credit_note_item.visit_cr_note_amount*visit_credit_note_item.visit_cr_note_units AS amount,
												visit_invoice.created AS `transactionDate`,
												visit_credit_note.created AS `createdAt`,
												1 AS `status`,
												visit_invoice.branch_id AS `branch_id`,
												'Receivable Credit Note' AS `transactionCategory`,
												'Credit Note' AS `transactionClassification`,
												'visit_invoice' AS `transactionTable`,
												'visit_credit_note' AS `referenceTable`
											FROM
												visit_credit_note_item,visit_credit_note,visit_invoice
											WHERE
												visit_credit_note.visit_cr_note_delete = 0 AND visit_credit_note_item.visit_cr_note_item_delete = 0
												AND visit_invoice.visit_invoice_id = visit_credit_note.visit_invoice_id
												AND visit_credit_note_item.visit_credit_note_id = visit_credit_note.visit_credit_note_id";


								$select .=	" UNION ALL

												SELECT

													payments.payment_id AS `transactionId`,
													'' AS `referenceId`,
													'' AS `payingFor`,
													payments.confirm_number AS `referenceCode`,
													payments.confirm_number AS `transactionCode`,
													payments.patient_id AS `patient_id`,
													'' AS `recepientId`,
													'' AS `accountsclassfication`,
													`payment_method`.`account_id` AS `accountId`,
													0 AS `accountFromId`,
													'' AS `transactionName`,
													CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
													-payment_item.payment_item_amount AS `amount`,
													payments.payment_date AS `transactionDate`,
													payments.payment_date AS `createdAt`,
													`payments`.`cancel` AS `status`,
													visit_invoice.branch_id AS `branch_id`,
													'Receivable Payment' AS `transactionCategory`,
													'Payments' AS `transactionClassification`,
													'account_payments' AS `transactionTable`,
													'' AS `referenceTable`
												FROM
													payments,payment_item,payment_method,visit_invoice
												WHERE
													payments.cancel = 0 AND payments.payment_type = 1
													AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id
													AND payment_item.invoice_type = 1
													AND payments.payment_id = payment_item.payment_id
													AND payments.payment_method_id = payment_method.payment_method_id


									UNION ALL


										SELECT

											payments.payment_id AS `transactionId`,
											'' AS `referenceId`,
											'' AS `payingFor`,
											payments.confirm_number AS `referenceCode`,
											payments.confirm_number AS `transactionCode`,
											payments.patient_id AS `patient_id`,
											'' AS `recepientId`,
											'' AS `accountsclassfication`,
											`payment_method`.`account_id` AS `accountId`,
											0 AS `accountFromId`,
											'' AS `transactionName`,
											CONCAT('Payment On Account') AS `transactionDescription`,
											-(payment_item.payment_item_amount) AS `amount`,
											payments.payment_date AS `transactionDate`,
											payments.payment_date AS `createdAt`,
											`payments`.`cancel` AS `status`,
											payments.branch_id AS `branch_id`,
											'Receivable Payment' AS `transactionCategory`,
											'Prepayments' AS `transactionClassification`,
											'account_payments' AS `transactionTable`,
											'' AS `referenceTable`
										FROM
											payments,payment_item,payment_method
										WHERE
											payments.cancel = 0
											AND payments.payment_type = 1
											AND payment_item.invoice_type = 3
											AND payments.payment_id = payment_item.payment_id
											AND payments.payment_method_id = payment_method.payment_method_id";

									$select .="
											UNION ALL

												SELECT


													patients_journals.patient_journal_id AS transactionId,
													patients_journals.patient_journal_id AS referenceId,
													'' AS `payingFor`,
													'' AS referenceCode,
													'' AS `transactionCode`,
													patients_journals.patient_id AS patient_id,
													patients_journals.patient_id AS `recepientId`,
													'' AS `accountsclassfication`,
													patients_journals.account_to_id AS `accountId`,
													patients_journals.account_from_id AS `accountFromId`,
													CONCAT('Patient Journals') AS `transactionName`,
													CONCAT('Patient Journals') AS `transactionDescription`,
													patients_journals.journal_amount AS `amount`,
													patients_journals.journal_date AS `transactionDate`,
													patients_journals.journal_date  AS `createdAt`,
													1 AS `status`,
													patients_journals.branch_id AS `branch_id`,
													'Patients Journals' AS `transactionCategory`,
													'Patient Journal' AS `transactionClassification`,
													'account_payments' AS `transactionTable`,
													'' AS `referenceTable`
												FROM
													patients_journals
												WHERE

												patients_journals.visit_invoice_id > 0";

									$select .="

												UNION ALL

												SELECT
													batch_receipts.batch_receipt_id AS `transactionId`,
													'' AS `referenceId`,
													'' AS `payingFor`,
													batch_receipts.receipt_number AS `referenceCode`,
													batch_receipts.receipt_number AS `transactionCode`,
													batch_unallocations.unallocated_payment_id AS `patient_id`,
													0 AS `recepientId`,
													'' AS `accountsclassfication`,
													batch_receipts.bank_id AS `accountId`,
													0 AS `accountFromId`,
													CONCAT('BATCH OVERPAYMENT :',' : ',batch_unallocations.reason) AS `transactionName`,
													CONCAT('BATCH OVERPAYMENT :',' : ',batch_unallocations.reason) AS `transactionDescription`,
													batch_unallocations.amount_paid AS `amount`,
													batch_receipts.payment_date AS `transactionDate`,
													batch_receipts.payment_date  AS `createdAt`,
													1 AS `status`,
													batch_receipts.branch_id AS `branch_id`,
													'Prepayments' AS `transactionCategory`,
													'Insuance Overpayments' AS `transactionClassification`,
													'account_payments' AS `transactionTable`,
													'' AS `referenceTable`
												FROM
														batch_unallocations,batch_receipts

												WHERE
													batch_receipts.batch_receipt_id = batch_unallocations.batch_receipt_id
													AND batch_unallocations.unallocated_payment_delete = 0
													AND  batch_unallocations.allocation_type_id = 0";




					$select .="
							) AS data WHERE data.transactionId ".$add." ";

				$query =$this->db->query($select);

				// echo "<pre>";
				// print_r($query->result());
				// echo "</pre>";

				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						// code...
						$recepientId = $value->recepientId;
						$balance = $value->balance;
						$referenceTable = $value->referenceTable;

						$date_tomorrow = date("Y-m-d", strtotime("+1 day", strtotime($end_period)));


						// opening balance

						if($balance <> 0)
						{

							$sql = "insert ignore into account_opening_balance (account_id, opening_balance_date, balance)
							values (".$accounts_receivable_id.",'".$date_tomorrow."',".$balance.")
							on duplicate key update balance = ".$balance.",status = 0";

							$this->db->query($sql);
						}


					}
				}
			}
		}

	}


	public function get_lender_aging_report()
	{
		//retrieve all users
		$creditor_search =  $this->session->userdata('creditor_search');

		if(!empty($creditor_search))
		{
			$where = $creditor_search;
		}
		else
		{
			$where = 'recepientId > 0';
		}
		$this->db->from('v_aged_lenders');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('payables','ASC');
		$query = $this->db->get();

		return $query;
	}

	public function get_account_transactions_ledgers($array_leads=NULL,$year=NULL,$ledger_type=0,$type=0,$checked=NULL,$date_from=NULL,$date_to = NULL,$view=0,$account_id=null)
	{

		$end_month = $this->session->userdata('end_of_year_month');
		$end_day = $this->session->userdata('end_of_year_day');

		$this->db->where('branch_id',2);
		$query = $this->db->get('branch');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$end_month = $value->end_of_year_month;
				$end_day = $value->end_of_year_day;
			}
		}

		$financial_year_date = $year."-".$end_month."-".$end_day."";



		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();

		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;


			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];
		$provider_opening_balance_id = $session_account['provider_opening_balance_id'];
		// $overpayment_account_id = $session_account['overpayment_account_id'];



		$add = '';



		// opening balances get_account_ledger_by_accounts_opening_balance
		// requires array leads and date from and date to from the ledgers
		if($view == 0)
		{
			$date_from = $this->session->userdata('date_from_general_ledger');
			$date_to = $this->session->userdata('date_to_general_ledger');

			if(!empty($date_from) OR !empty($date_to))
	    	{

	    		$add .=  ' AND DATE(transactionDate) < \''.$date_from.'\' ';


	    	}
	    	else if(!empty($date_from) AND empty($date_to))
	    	{


	    		$add .= '  AND DATE(transactionDate) < \''.$date_from.'\'';


	    	}
	    	else if(empty($date_from) AND !empty($date_to))
	    	{

	    		$add .= ' AND DATE(transactionDate) < \''.$date_to.'\'';

	    	}
	    	else
	    	{
	    		$add .=  '  AND DATE(transactionDate) < \''.date('Y-m-01').'\'';
	    	}



	    	$add_item = 'GROUP BY data.accountId';




			$item = '	data.accountId AS accountId,
						account.account_name AS account_name,
					 	COALESCE (SUM(data.dr_amount),0) AS dr_amount,
					 	COALESCE (SUM(data.cr_amount),0) AS cr_amount';

			$add .= " AND data.accountId in ('".implode("','", $array_leads)."') ";
			$add_item .= ' ORDER BY data.transactionDate ASC';
		}
		// get_account_ledger_by_accounts
		// requires ledger_type and dates

		else if ($view == 1)
		{

			$date_from = $this->session->userdata('date_from_general_ledger');
			$date_to = $this->session->userdata('date_to_general_ledger');
			if(!empty($date_from) OR !empty($date_to))
	    	{

	    			$add =  '  AND DATE(transactionDate) >= \''.$date_from.'\' AND DATE(transactionDate) <= \''.$date_to.'\' ';


	    	}
	    	else if(!empty($date_from) AND empty($date_to))
	    	{


	    			$add = '  AND DATE(transactionDate) = \''.$date_from.'\'';


	    	}
	    	else if(empty($date_from) AND !empty($date_to))
	    	{

	    			$add = '  AND DATE(transactionDate) = \''.$date_to.'\'';

	    	}
	    	else
	    	{


	    			$add = ' AND DATE(transactionDate) >= "'.date('Y-m-01').'" AND DATE(transactionDate) <= "'.date('Y-m-d').'" ';

	    		// $add = '';

	    	}

			// $add = " AND data.transactionDate <= '2021-12-31'";
			if($ledger_type == 1)
	    	{

	    		$add_item = 'GROUP BY data.accountId';
	    	}
	    	else
	    	{
	    		$add_item = '';
	    	}

	    	// $creditors = $this->load->view('financials/ledgers/creditors_sql', $session_account,true);
	    	// var_dump($creditors);die();
	    	if($ledger_type == 1)
	    	{
				$item = '	data.accountId AS accountId,
							account.account_name AS account_name,
						 	COALESCE (SUM(data.dr_amount),0) AS dr_amount,
						 	COALESCE (SUM(data.cr_amount),0) AS cr_amount';
	    	}
	    	else
	    	{
	    		$item = '*';
	    	}
	    	$add_item .= ' ORDER BY data.transactionDate ASC';

		}

		// opening profit and loss on the financial years
		// get_account_ledger_by_accounts_opening_periods
		else if($view == 2)
		{

			$date_from = $this->session->userdata('date_from_general_ledger');
			$date_to = $this->session->userdata('date_to_general_ledger');
			if(!empty($date_from) OR !empty($date_to))
	    	{

	    		$add =  '  AND DATE(transactionDate) > \''.$financial_year_date.'\' AND DATE(transactionDate) < \''.$date_from.'\' ';


	    	}
	    	else if(!empty($date_from) AND empty($date_to))
	    	{


	    		$add = ' AND DATE(transactionDate) > \''.$financial_year_date.'\' AND DATE(transactionDate) < \''.$date_from.'\'';


	    	}
	    	else if(empty($date_from) AND !empty($date_to))
	    	{

	    		$add = ' AND DATE(transactionDate) > \''.$financial_year_date.'\' AND DATE(transactionDate) < \''.$date_to.'\'';

	    	}
	    	else
	    	{
	    		$add =  ' AND DATE(transactionDate) > \''.$financial_year_date.'\' AND DATE(transactionDate) < \''.date('Y-m-01').'\'';
	    	}


	    	$add_item = 'GROUP BY data.accountId';




			$item = '	data.accountId AS accountId,
					 	COALESCE (SUM(data.dr_amount),0) AS dr_amount,
					 	COALESCE (SUM(data.cr_amount),0) AS cr_amount';

			$add .= " AND data.accountId in ('".implode("','", $array_leads)."') ";
			$add_item .= ' ORDER BY data.transactionDate ASC';

		}

		else if($view == 3)
		{
			if(!empty($date_from) OR !empty($date_to))
	    	{

	    		$add =  ' DATE(data.transactionDate) >= \''.$date_from.'\' AND DATE(data.transactionDate) <= \''.$date_to.'\' AND ';


	    	}

	    	if($checked == 1)
	    	{
	    		$item = '	YEAR(data.transactionDate) AS year,
	    					data.accountId AS accountId,
							account.account_name AS account_name,
						 	COALESCE (SUM(data.dr_amount),0) AS dr_amount,
						 	COALESCE (SUM(data.cr_amount),0) AS cr_amount';

				$add_item = 'GROUP BY data.accountId';
	    	}
	    	else
	    	{
	    		$item = '
					 	MAX(data.transactionDate) AS max_transaction_date,
					 	MIN(data.transactionDate) AS min_transaction_date';

				$add_item = '';
	    	}


	    	$add .= " AND data.accountId in ('".implode("','", $array_leads)."') ";
	    	$add_item .= ' ORDER BY data.transactionDate ASC';

		}

		else if($view == 4)
		{






			if($checked == 1)
			{
				if(!empty($date_from))
		    	{

		    		$add =  ' AND DATE(data.transactionDate) > \''.$date_from.'\' AND DATE(data.transactionDate) <= \''.$date_to.'\' ';
		    	}

				$item = '*';
				$branch_id = $this->session->userdata('branch_id');
				$add .= " AND data.accountId = ".$account_id."  ";
				$add_item .= ' ORDER BY data.transactionDate ASC';
			}
			else if($checked == 2)
			{
				if(!empty($date_from) OR !empty($date_to))
		    	{
		    		$add =  ' AND DATE(data.transactionDate) <= \''.$date_from.'\' ';
		    	}
				$item = 'data.accountId AS accountId,
						account.account_name AS account_name,
					 	COALESCE (SUM(data.dr_amount),0) AS dr_amount,
					 	COALESCE (SUM(data.cr_amount),0) AS cr_amount';
				$add_item = 'GROUP BY data.accountId';
				$add_item .= ' ORDER BY data.transactionDate ASC';

			}
			else if($checked == 3)
			{
				if(!empty($date_from) OR !empty($date_to))
		    	{
		    		$add =  ' AND DATE(data.transactionDate) >= \''.$date_from.'\' AND DATE(data.transactionDate) <= \''.$date_to.'\' ';
		    	}
				$item = 'data.accountId AS accountId,
						account.account_name AS account_name,
					 	COALESCE (SUM(data.dr_amount),0) AS dr_amount,
					 	COALESCE (SUM(data.cr_amount),0) AS cr_amount,
					 	MONTH(data.transactionDate) AS month,
					 	MONTH(data.transactionDate) AS year';
				$add_item = 'GROUP BY MONTH(data.transactionDate),YEAR(data.transactionDate)';
				$add_item .= ' ORDER BY data.cr_amount ASC LIMIT 10';
			}
			else
			{
				if(!empty($date_from) OR !empty($date_to))
		    	{
		    		$add =  ' AND DATE(data.transactionDate) >= \''.$date_from.'\' AND DATE(data.transactionDate) <= \''.$date_to.'\' ';
		    	}

				$item = '*';
				$add .= " AND data.accountId = ".$account_id." ";
				$add_item .= ' ORDER BY data.transactionDate ASC';
			}


		}

		// $add ='';
		// $item = '*';
		// $add_item = '';

		$branch_id = $this->session->userdata('branch_id');

		//$add .= ' AND data.branch_id = '.$branch_id;
		//var_dump($checked);die();

		$select  = "
					SELECT
						".$item.",
						account.parent_account AS `accountParentId`,
						account_type.account_type_id AS account_type_id,
						account_type.account_type_name AS account_type_name,
						account.account_name AS accountName
					FROM

					(

						SELECT
							`account`.`account_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							account.account_id AS `accountId`,
							CONCAT('Opening Balance as from',' ',`account`.`start_date`) AS `transactionName`,
							CONCAT('Opening Balance as from',' ',' ',`account`.`start_date`) AS `transactionDescription`,
							`account`.`account_opening_balance` AS `dr_amount`,
							'0' AS `cr_amount`,
							`account`.`start_date` AS `transactionDate`,
							`account`.`start_date` AS `createdAt`,
							`account`.`account_status` AS `status`,
							branch_id AS `branch_id`,
							'Fund Accounts' AS `transactionCategory`,
							'Account Opening Balance' AS `transactionClassification`,
							'' AS `transactionTable`,
							'account' AS `referenceTable`
							FROM
							account

						WHERE

						 	account.account_opening_balance <> 0 AND account.parent_account > 0



						UNION ALL


						SELECT
							`finance_transfer`.`finance_transfer_id` AS `transactionId`,
							`finance_transfer`.`finance_transfer_id` AS `referenceId`,
							'' AS `payingFor`,
							`finance_transfer`.`reference_number` AS `referenceCode`,
							`finance_transfer`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							`finance_transfer`.`account_to_id` AS `accountId`,
							`finance_transfer`.`remarks` AS `transactionName`,
							CONCAT('Amount Received ',' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
							`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
							0 AS `cr_amount`,
							`finance_transfer`.`transaction_date` AS `transactionDate`,
							`finance_transfer`.`created` AS `createdAt`,
							`finance_transfer`.`finance_transfer_status` AS `status`,
							`finance_transfer`.`branch_id` AS `branch_id`,
							'Transfer' AS `transactionCategory`,
							'Transfer' AS `transactionClassification`,
							'finance_transfer' AS `transactionTable`,
							'finance_transfered' AS `referenceTable`
						FROM
						`finance_transfer`
						WHERE  finance_transfer.finance_transfer_deleted = 0
						AND finance_transfer.finance_transfer_status = 1


						UNION ALL

						SELECT
							`finance_transfer`.`finance_transfer_id` AS `transactionId`,
							`finance_transfer`.`finance_transfer_id` AS `referenceId`,
							'' AS `payingFor`,
							`finance_transfer`.`reference_number` AS `referenceCode`,
							`finance_transfer`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							`finance_transfer`.`account_from_id` AS `accountId`,
							`finance_transfer`.`remarks` AS `transactionName`,
							CONCAT(' Amount Transfered ') AS `transactionDescription`,
							0 AS
							`dr_amount`,
							`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
							`finance_transfer`.`transaction_date` AS `transactionDate`,
							`finance_transfer`.`transaction_date` AS `transactionDate`,

							`finance_transfer`.`finance_transfer_status` AS `status`,
							`finance_transfer`.`branch_id` AS `branch_id`,
							'Transfer' AS `transactionCategory`,
							'Transfer' AS `transactionClassification`,
							'finance_transfered' AS `transactionTable`,
							'finance_transfer' AS `referenceTable`
						FROM
						`finance_transfer`
						WHERE
							finance_transfer.finance_transfer_deleted = 0
							AND finance_transfer.finance_transfer_status = 1

						UNION ALL


						SELECT
							`finance_purchase`.`finance_purchase_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							finance_purchase.creditor_id AS `recepientId`,
							'' AS `accountsclassfication`,
							`finance_purchase`.`account_from_id` AS `accountId`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`) AS `transactionDescription`,
							0 AS `dr_amount`,
							`finance_purchase`.`finance_purchase_amount` AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase`.`finance_purchase_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense' AS `transactionCategory`,
							'Expense' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'finance_purchase_payment' AS `referenceTable`
						FROM
						finance_purchase
						WHERE
						finance_purchase.finance_purchase_deleted = 0
						AND finance_purchase.account_to_id > 0

						UNION ALL

						SELECT
							`finance_purchase`.`finance_purchase_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`finance_purchase`.`creditor_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							`finance_purchase`.`account_to_id` AS `accountId`,
							`finance_purchase`.`finance_purchase_description` AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`)  AS `transactionDescription`,
							`finance_purchase`.`finance_purchase_amount` AS `dr_amount`,
							0 AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase`.`finance_purchase_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense' AS `transactionCategory`,
							'Expense' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							finance_purchase
						WHERE
							finance_purchase.finance_purchase_deleted = 0
							AND finance_purchase.account_to_id > 0


						UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							`creditor_payment`.`account_from_id` AS `accountId`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							 creditor.branch_id AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
						FROM

							`creditor_payment_item`,creditor_payment,creditor_invoice,creditor


						WHERE creditor_payment_item.invoice_type = 0
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
							AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
							AND creditor_payment.creditor_payment_status = 1
							AND creditor_invoice.creditor_invoice_status = 1
							AND creditor_invoice.creditor_id = creditor.creditor_id
							AND creditor_invoice.transaction_date >= creditor.start_date




						UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							`creditor_payment`.`account_from_id` AS `accountId`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							 creditor.branch_id AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
						FROM

							`creditor_payment_item`,creditor_payment,creditor

						WHERE creditor_payment_item.invoice_type = 2
						AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
						AND creditor.creditor_id = creditor_payment_item.creditor_id
						AND creditor_payment.transaction_date >= creditor.start_date

						UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							`creditor_payment`.`account_from_id` AS `accountId`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							 creditor.branch_id AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
						FROM

							`creditor_payment_item`,creditor_payment,creditor

						WHERE
							creditor_payment_item.invoice_type = 3
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date





						UNION ALL



						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							creditor_payment.account_from_id AS `accountId`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							 creditor.branch_id AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
						FROM

						`creditor_payment_item`,creditor_payment,orders,creditor

						WHERE creditor_payment_item.invoice_type = 1
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
							AND creditor_payment.creditor_payment_status = 1
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date




						UNION ALL

						SELECT
							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							journal_entry.account_from_id AS `accountId`,
							CONCAT(journal_entry.journal_entry_description)  AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							`journal_entry`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							journal_entry.branch_id AS `branch_id`,
							'Journal Credit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
							journal_entry

						WHERE journal_entry.journal_entry_deleted = 0

						UNION ALL


						SELECT
							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							journal_entry.account_to_id AS `accountId`,
							CONCAT(journal_entry.journal_entry_description)  AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							0 AS `dr_amount`,
							`journal_entry`.`amount_paid` AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							journal_entry.branch_id AS `branch_id`,
							'Journal Debit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
							journal_entry

						WHERE
							journal_entry.journal_entry_deleted = 0

						UNION ALL


						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							`payment_method`.`account_id` AS `accountId`,
							'' AS `transactionName`,
							CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
							payment_item.payment_item_amount AS `dr_amount`,
							0 AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							visit_invoice.branch_id AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							payments,payment_item,payment_method,visit_invoice
						WHERE
							payments.cancel = 0 AND payments.payment_type = 1
							AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id
							AND payment_item.invoice_type = 1
							AND payments.payment_id = payment_item.payment_id
							AND payments.payment_method_id = payment_method.payment_method_id


						UNION ALL


						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							`payment_method`.`account_id` AS `accountId`,
							'' AS `transactionName`,
							CONCAT('Payment On Account') AS `transactionDescription`,
							(payment_item.payment_item_amount) AS `dr_amount`,
							0 AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							payments.branch_id AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							payments,payment_item,payment_method
						WHERE
							payments.cancel = 0
							AND payments.payment_type = 1
							AND payment_item.invoice_type = 3
							AND payments.payment_id = payment_item.payment_id
							AND payments.payment_method_id = payment_method.payment_method_id





					UNION ALL

					SELECT
						`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
						`provider_payment`.`provider_payment_id` AS `referenceId`,
						`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
						`provider_payment`.`reference_number` AS `referenceCode`,
						`provider_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
							`provider_payment`.`provider_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`provider_payment`.`account_from_id` AS `accountId`,
						`provider_payment_item`.`description` AS `transactionName`,
						CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
						0 AS `dr_amount`,
						`provider_payment_item`.`amount_paid` AS `cr_amount`,
						`provider_payment`.`transaction_date` AS `transactionDate`,
						`provider_payment`.`created` AS `createdAt`,
						`provider_payment_item`.`provider_payment_item_status` AS `status`,
						provider.branch_id AS `branch_id`,
						'Expense Payment' AS `transactionCategory`,
						'Providers Invoices Payments' AS `transactionClassification`,
						'provider_payment' AS `transactionTable`,
						'provider_payment_item' AS `referenceTable`
					FROM
						`provider_payment_item`,provider_payment,provider_invoice,provider

					WHERE provider_payment_item.invoice_type = 0
						AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id
						AND provider_payment.provider_payment_status = 1
						AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id
						AND provider_invoice.provider_invoice_status = 1
						AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date


					UNION ALL

					SELECT
						`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
						`provider_payment`.`provider_payment_id` AS `referenceId`,
						`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
						`provider_payment`.`reference_number` AS `referenceCode`,
						`provider_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`provider_payment`.`provider_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`provider_payment`.`account_from_id` AS `accountId`,
						`provider_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment of opening balance')  AS `transactionDescription`,
						0 AS `dr_amount`,
						`provider_payment_item`.`amount_paid` AS `cr_amount`,
						`provider_payment`.`transaction_date` AS `transactionDate`,
						`provider_payment`.`created` AS `createdAt`,
						`provider_payment_item`.`provider_payment_item_status` AS `status`,
						provider.branch_id AS `branch_id`,
						'Expense Payment' AS `transactionCategory`,
						'Providers Invoices Payments' AS `transactionClassification`,
						'provider_payment' AS `transactionTable`,
						'provider_payment_item' AS `referenceTable`
					FROM
						`provider_payment_item`,provider_payment,provider

					WHERE provider_payment_item.invoice_type = 2
						AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id
						AND provider_payment.provider_payment_status = 1
						AND provider.provider_id = provider_payment_item.provider_id
						AND provider_payment.transaction_date >= provider.start_date



					UNION ALL

					SELECT
						`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
						`provider_payment`.`provider_payment_id` AS `referenceId`,
						`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
						`provider_payment`.`reference_number` AS `referenceCode`,
						`provider_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
					  `provider_payment`.`provider_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`provider_payment`.`account_from_id` AS `accountId`,
						`provider_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment on account')  AS `transactionDescription`,
						0 AS `dr_amount`,
						`provider_payment_item`.`amount_paid` AS `cr_amount`,
						`provider_payment`.`transaction_date` AS `transactionDate`,
						`provider_payment`.`created` AS `createdAt`,
						`provider_payment_item`.`provider_payment_item_status` AS `status`,
						provider.branch_id AS `branch_id`,
						'Expense Payment' AS `transactionCategory`,
						'Providers Invoices Payments' AS `transactionClassification`,
						'provider_payment' AS `transactionTable`,
						'provider_payment_item' AS `referenceTable`
					FROM
						`provider_payment_item`,provider_payment,provider

					WHERE
							provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id
							AND provider_payment.provider_payment_status = 1
							AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date


					UNION ALL


					SELECT
						`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
						`loan_invoice`.`loan_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`loan_invoice`.`invoice_number` AS `referenceCode`,
						`loan_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
							`loan_invoice`.`loan_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`loan`.`account_from_id` AS `accountId`,
						`loan_invoice_item`.`item_description` AS `transactionName`,
						`loan_invoice_item`.`item_description` AS `transactionDescription`,
						 0 AS `dr_amount`,
						`loan_invoice_item`.`total_amount` AS `cr_amount`,
						`loan_invoice`.`transaction_date` AS `transactionDate`,
						`loan_invoice`.`created` AS `createdAt`,
						`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
						loan.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Loans' AS `transactionClassification`,
						'loan_invoice_item' AS `transactionTable`,
						'loan_invoice' AS `referenceTable`
					FROM
						`loan_invoice_item`,loan_invoice,loan

					WHERE
						loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id
						AND loan_invoice.loan_invoice_status = 1
						AND loan.loan_id = loan_invoice.loan_id
						AND loan_invoice.transaction_date >= loan.start_date

					UNION ALL

					SELECT
						`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
						`loan_invoice`.`loan_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`loan_invoice`.`invoice_number` AS `referenceCode`,
						`loan_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`loan_invoice`.`loan_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`loan`.`account_to_id` AS `accountId`,
						`loan_invoice_item`.`item_description` AS `transactionName`,
						`loan_invoice_item`.`item_description` AS `transactionDescription`,
						`loan_invoice_item`.`total_amount` AS `dr_amount`,
						'0' AS `cr_amount`,
						`loan_invoice`.`transaction_date` AS `transactionDate`,
						`loan_invoice`.`created` AS `createdAt`,
						`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
						loan.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Loans' AS `transactionClassification`,
						'loan_invoice_item' AS `transactionTable`,
						'loan_invoice' AS `referenceTable`
					FROM
						`loan_invoice_item`,loan_invoice,loan

					WHERE
						loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id
						AND loan_invoice.loan_invoice_status = 1
						AND loan.loan_id = loan_invoice.loan_id
						AND loan_invoice.transaction_date >= loan.start_date


					UNION ALL

					SELECT
						`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
						`loan_payment`.`loan_payment_id` AS `referenceId`,
						`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
						`loan_payment`.`reference_number` AS `referenceCode`,
						`loan_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
							`loan_payment`.`loan_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`loan_payment`.`account_from_id` AS `accountId`,
						CONCAT('Payment from ') AS `transactionName`,
						CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
						0 AS `dr_amount`,
						`loan_payment_item`.`amount_paid` AS `cr_amount`,
						`loan_payment`.`transaction_date` AS `transactionDate`,
						`loan_payment`.`created` AS `createdAt`,
						`loan_payment_item`.`loan_payment_item_status` AS `status`,
						loan.branch_id AS `branch_id`,
						'Expense Payment' AS `transactionCategory`,
						'Loans Invoices Payments' AS `transactionClassification`,
						'loan_payment' AS `transactionTable`,
						'loan_payment_item' AS `referenceTable`
					FROM
						`loan_payment_item`,loan_payment,loan_invoice,loan

					WHERE loan_payment_item.invoice_type = 0
						AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
						AND loan_payment.loan_payment_status = 1
						AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
						AND loan_invoice.loan_invoice_status = 1
						AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
						AND loan.account_from_id = loan_payment_item.account_to_id


					UNION ALL

					SELECT
						`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
						`loan_payment`.`loan_payment_id` AS `referenceId`,
						`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
						`loan_payment`.`reference_number` AS `referenceCode`,
						`loan_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`loan_payment`.`loan_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`loan`.`account_from_id` AS `accountId`,
						CONCAT('Payment from ') AS `transactionName`,
						CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
						`loan_payment_item`.`amount_paid` AS `dr_amount`,
						0 AS `cr_amount`,
						`loan_payment`.`transaction_date` AS `transactionDate`,
						`loan_payment`.`created` AS `createdAt`,
						`loan_payment_item`.`loan_payment_item_status` AS `status`,
						loan.branch_id AS `branch_id`,
						'Expense Payment' AS `transactionCategory`,
						'Loans Invoices Payments' AS `transactionClassification`,
						'loan_payment' AS `transactionTable`,
						'loan_payment_item' AS `referenceTable`
					FROM
						`loan_payment_item`,loan_payment,loan_invoice,loan

					WHERE loan_payment_item.invoice_type = 0
						AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
						AND loan_payment.loan_payment_status = 1
						AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
						AND loan_invoice.loan_invoice_status = 1
						AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
						AND loan.account_from_id = loan_payment_item.account_to_id

					UNION ALL

					SELECT
							`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
							`loan_payment`.`loan_payment_id` AS `referenceId`,
							`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
							`loan_payment`.`reference_number` AS `referenceCode`,
							`loan_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`loan_payment`.`loan_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							loan_payment_item.account_to_id AS `accountId`,
							CONCAT('Payment ') AS `transactionName`,
							CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
							`loan_payment_item`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`loan_payment`.`transaction_date` AS `transactionDate`,
							`loan_payment`.`created` AS `createdAt`,
							`loan_payment_item`.`loan_payment_item_status` AS `status`,
							loan.branch_id AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Loans Invoices Payments' AS `transactionClassification`,
							'loan_payment' AS `transactionTable`,
							'loan_payment_item' AS `referenceTable`
						FROM
							`loan_payment_item`,loan_payment,loan_invoice,loan,account

							WHERE loan_payment_item.invoice_type = 0
							AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
							AND loan_payment.loan_payment_status = 1
							AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
							AND loan_invoice.loan_invoice_status = 1
							AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
							AND loan.account_from_id <> loan_payment_item.account_to_id


						UNION ALL


						SELECT
							`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
							`loan_payment`.`loan_payment_id` AS `referenceId`,
							`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
							`loan_payment`.`reference_number` AS `referenceCode`,
							`loan_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
								`loan_payment`.`loan_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							loan_payment.account_from_id AS `accountId`,
							CONCAT('Payment ') AS `transactionName`,
							CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`loan_payment_item`.`amount_paid` AS `cr_amount`,
							`loan_payment`.`transaction_date` AS `transactionDate`,
							`loan_payment`.`created` AS `createdAt`,
							`loan_payment_item`.`loan_payment_item_status` AS `status`,
							loan.branch_id AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Loans Invoices Payments' AS `transactionClassification`,
							'loan_payment' AS `transactionTable`,
							'loan_payment_item' AS `referenceTable`
						FROM
							`loan_payment_item`,loan_payment,loan_invoice,loan

						WHERE loan_payment_item.invoice_type = 0
							AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
							AND loan_payment.loan_payment_status = 1
							AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
							AND loan_invoice.loan_invoice_status = 1
							AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
							AND loan.account_from_id <> loan_payment_item.account_to_id



						UNION ALL

							SELECT
								batch_receipts.batch_receipt_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								batch_receipts.receipt_number AS `referenceCode`,
								batch_receipts.receipt_number AS `transactionCode`,
								batch_unallocations.unallocated_payment_id AS `patient_id`,
								0 AS `recepientId`,
								'' AS `accountsclassfication`,
								batch_receipts.bank_id AS `accountId`,
								CONCAT('BATCH OVERPAYMENT :',' : ',batch_unallocations.reason) AS `transactionName`,
								CONCAT('BATCH OVERPAYMENT :',' : ',batch_unallocations.reason) AS `transactionDescription`,
								batch_unallocations.amount_paid AS `dr_amount`,
								0 AS `cr_amount`,
								batch_receipts.payment_date AS `transactionDate`,
								batch_receipts.payment_date  AS `createdAt`,
								1 AS `status`,
								batch_receipts.branch_id AS `branch_id`,
								'OVERPAYMENTS' AS `transactionCategory`,
								'Insuance Overpayments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`
							FROM
									batch_unallocations,batch_receipts

							WHERE
								batch_receipts.batch_receipt_id = batch_unallocations.batch_receipt_id
								AND batch_unallocations.unallocated_payment_delete = 0
								AND  batch_unallocations.allocation_type_id = 0





							UNION ALL

								SELECT
									`payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
									`payroll_payment`.`payroll_payment_id` AS `referenceId`,
									`payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
									`payroll_payment`.`reference_number` AS `referenceCode`,
									`payroll_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
									`statutory_accounts`.`statutory_account_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									`payroll_payment`.`account_from_id` AS `accountId`,
									CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
									CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
									0 AS `dr_amount`,
									`payroll_payment_item`.`amount_paid` AS `cr_amount`,
									`payroll_payment`.`transaction_date` AS `transactionDate`,
									`payroll_payment`.`created` AS `createdAt`,
									`payroll_payment_item`.`payroll_payment_item_status` AS `status`,
									2 AS `branch_id`,
									'Payroll Payment' AS `transactionCategory`,
									'Payroll Payments' AS `transactionClassification`,
									'payroll_payment' AS `transactionTable`,
									'payroll_payment_item' AS `referenceTable`
								FROM
									`payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts

								WHERE payroll_payment_item.invoice_type = 0
								AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id
								AND payroll_payment.payroll_payment_status = 1
								AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id
								AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id



							UNION ALL

								SELECT
									`account_payments`.`account_payment_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									account_payments.receipt_number AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									account_payments.account_from_id AS `accountId`,
									CONCAT(account_payments.account_payment_description) AS `transactionName`,
									CONCAT(account_payments.account_payment_description) AS `transactionDescription`,
									0 AS `dr_amount`,
									`account_payments`.`amount_paid` AS `cr_amount`,
									`account_payments`.`payment_date` AS `transactionDate`,
									`account_payments`.`payment_date` AS `createdAt`,
									`account_payments`.`account_payment_status` AS `status`,
									account_payments.branch_id AS `branch_id`,
									'Journal Credit' AS `transactionCategory`,
									'Journal' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'account' AS `referenceTable`
								FROM
									account_payments

								WHERE account_payments.account_payment_deleted = 0

							UNION ALL


								SELECT
									`account_payments`.`account_payment_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									account_payments.receipt_number AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									account_payments.account_to_id AS `accountId`,
									CONCAT(account_payments.account_payment_description) AS `transactionName`,
									CONCAT(account_payments.account_payment_description) AS `transactionDescription`,
									`account_payments`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`account_payments`.`payment_date` AS `transactionDate`,
									`account_payments`.`payment_date` AS `createdAt`,
									`account_payments`.`account_payment_status` AS `status`,
									account_payments.branch_id AS `branch_id`,
									'Journal Debit' AS `transactionCategory`,
									'Journal' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'account' AS `referenceTable`
								FROM
									account_payments

								WHERE
									account_payments.account_payment_deleted = 0


					) AS data,account,account_type WHERE
						data.transactionId > 0
						AND data.accountId = account.account_id
						AND  account.account_type_id = account_type.account_type_id
						".$add." ".$add_item."


					";


	  $query = $this->db->query($select);


	   return $query;

	}
	public function get_account_transactions_ledgers_____($array_leads=NULL,$year=NULL,$ledger_type=0,$type=0,$checked=NULL,$date_from=NULL,$date_to = NULL,$view=0,$account_id=null)
	{

		$end_month = $this->session->userdata('end_of_year_month');
		$end_day = $this->session->userdata('end_of_year_day');

		$this->db->where('branch_id',2);
		$query = $this->db->get('branch');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$end_month = $value->end_of_year_month;
				$end_day = $value->end_of_year_day;
			}
		}

		$financial_year_date = $year."-".$end_month."-".$end_day."";



		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$patients = array();

		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$session_account[$reference_name] = $staing_account_id;


			}
		}

		$providers_liability_id = $session_account['providers_liability_id'];
		$providers_wht_id = $session_account['providers_wht_id'];
		$payroll_liability_id = $session_account['payroll_liability_id'];
		$accounts_payable_id = $session_account['accounts_payable_id'];
		$fixed_assets_id = $session_account['fixed_assets_id'];
		$accounts_receivable_id = $session_account['accounts_receivable_id'];
		$suppliers_wht_id = $session_account['suppliers_wht_id'];
		$income_account_id = $session_account['income_account_id'];
		$supplier_opening_balance_id= $session_account['supplier_opening_balance_id'];
		$bank_opening_balance_id = $session_account['bank_opening_balance_id'];
		$provider_opening_balance_id = $session_account['provider_opening_balance_id'];
		// $overpayment_account_id = $session_account['overpayment_account_id'];



		$add = '';



		// opening balances get_account_ledger_by_accounts_opening_balance
		// requires array leads and date from and date to from the ledgers
		if($view == 0)
		{
			$date_from = $this->session->userdata('date_from_general_ledger');
			$date_to = $this->session->userdata('date_to_general_ledger');

			if(!empty($date_from) OR !empty($date_to))
	    	{

	    		$add .=  ' AND DATE(transactionDate) < \''.$date_from.'\' ';


	    	}
	    	else if(!empty($date_from) AND empty($date_to))
	    	{


	    		$add .= '  AND DATE(transactionDate) < \''.$date_from.'\'';


	    	}
	    	else if(empty($date_from) AND !empty($date_to))
	    	{

	    		$add .= ' AND DATE(transactionDate) < \''.$date_to.'\'';

	    	}
	    	else
	    	{
	    		$add .=  '  AND DATE(transactionDate) < \''.date('Y-m-01').'\'';
	    	}



	    	$add_item = 'GROUP BY data.accountId';




			$item = '	data.accountId AS accountId,
						account.account_name AS account_name,
					 	COALESCE (SUM(data.dr_amount),0) AS dr_amount,
					 	COALESCE (SUM(data.cr_amount),0) AS cr_amount';

			$add .= " AND data.accountId in ('".implode("','", $array_leads)."') ";
			$add_item .= ' ORDER BY data.transactionDate ASC';
		}
		// get_account_ledger_by_accounts
		// requires ledger_type and dates

		else if ($view == 1)
		{

			$date_from = $this->session->userdata('date_from_general_ledger');
			$date_to = $this->session->userdata('date_to_general_ledger');
			if(!empty($date_from) OR !empty($date_to))
	    	{

	    			$add =  '  AND DATE(transactionDate) >= \''.$date_from.'\' AND DATE(transactionDate) <= \''.$date_to.'\' ';


	    	}
	    	else if(!empty($date_from) AND empty($date_to))
	    	{


	    			$add = '  AND DATE(transactionDate) = \''.$date_from.'\'';


	    	}
	    	else if(empty($date_from) AND !empty($date_to))
	    	{

	    			$add = '  AND DATE(transactionDate) = \''.$date_to.'\'';

	    	}
	    	else
	    	{


	    			$add = ' AND DATE(transactionDate) >= "'.date('Y-m-01').'" AND DATE(transactionDate) <= "'.date('Y-m-d').'" ';

	    		// $add = '';

	    	}

			// $add = " AND data.transactionDate <= '2021-12-31'";
			if($ledger_type == 1)
	    	{

	    		$add_item = 'GROUP BY data.accountId';
	    	}
	    	else
	    	{
	    		$add_item = '';
	    	}

	    	// $creditors = $this->load->view('financials/ledgers/creditors_sql', $session_account,true);
	    	// var_dump($creditors);die();
	    	if($ledger_type == 1)
	    	{
				$item = '	data.accountId AS accountId,
							account.account_name AS account_name,
						 	COALESCE (SUM(data.dr_amount),0) AS dr_amount,
						 	COALESCE (SUM(data.cr_amount),0) AS cr_amount';
	    	}
	    	else
	    	{
	    		$item = '*';
	    	}
	    	$add_item .= ' ORDER BY data.transactionDate ASC';

		}

		// opening profit and loss on the financial years
		// get_account_ledger_by_accounts_opening_periods
		else if($view == 2)
		{

			$date_from = $this->session->userdata('date_from_general_ledger');
			$date_to = $this->session->userdata('date_to_general_ledger');
			if(!empty($date_from) OR !empty($date_to))
	    	{

	    		$add =  '  AND DATE(transactionDate) > \''.$financial_year_date.'\' AND DATE(transactionDate) < \''.$date_from.'\' ';


	    	}
	    	else if(!empty($date_from) AND empty($date_to))
	    	{


	    		$add = ' AND DATE(transactionDate) > \''.$financial_year_date.'\' AND DATE(transactionDate) < \''.$date_from.'\'';


	    	}
	    	else if(empty($date_from) AND !empty($date_to))
	    	{

	    		$add = ' AND DATE(transactionDate) > \''.$financial_year_date.'\' AND DATE(transactionDate) < \''.$date_to.'\'';

	    	}
	    	else
	    	{
	    		$add =  ' AND DATE(transactionDate) > \''.$financial_year_date.'\' AND DATE(transactionDate) < \''.date('Y-m-01').'\'';
	    	}


	    	$add_item = 'GROUP BY data.accountId';




			$item = '	data.accountId AS accountId,
					 	COALESCE (SUM(data.dr_amount),0) AS dr_amount,
					 	COALESCE (SUM(data.cr_amount),0) AS cr_amount';

			$add .= " AND data.accountId in ('".implode("','", $array_leads)."') ";
			$add_item .= ' ORDER BY data.transactionDate ASC';

		}

		else if($view == 3)
		{
			if(!empty($date_from) OR !empty($date_to))
	    	{

	    		$add =  ' DATE(data.transactionDate) >= \''.$date_from.'\' AND DATE(data.transactionDate) <= \''.$date_to.'\' AND ';


	    	}

	    	if($checked == 1)
	    	{
	    		$item = '	YEAR(data.transactionDate) AS year,
	    					data.accountId AS accountId,
							account.account_name AS account_name,
						 	COALESCE (SUM(data.dr_amount),0) AS dr_amount,
						 	COALESCE (SUM(data.cr_amount),0) AS cr_amount';

				$add_item = 'GROUP BY data.accountId';
	    	}
	    	else
	    	{
	    		$item = '
					 	MAX(data.transactionDate) AS max_transaction_date,
					 	MIN(data.transactionDate) AS min_transaction_date';

				$add_item = '';
	    	}


	    	$add .= " AND data.accountId in ('".implode("','", $array_leads)."') ";
	    	$add_item .= ' ORDER BY data.transactionDate ASC';

		}

		else if($view == 4)
		{






			if($checked == 1)
			{
				if(!empty($date_from))
		    	{

		    		$add =  ' AND DATE(data.transactionDate) >= \''.$date_from.'\' AND DATE(data.transactionDate) <= \''.$date_to.'\' ';
		    	}

				$item = '*';
				$branch_id = $this->session->userdata('branch_id');
				$add .= " AND data.accountId = ".$account_id."  ";
				$add_item .= ' ORDER BY data.transactionDate ASC';
			}
			else if($checked == 2)
			{
				if(!empty($date_from) OR !empty($date_to))
		    	{
		    		$add =  ' AND DATE(data.transactionDate) <= \''.$date_from.'\' ';
		    	}
				$item = 'data.accountId AS accountId,
						account.account_name AS account_name,
					 	COALESCE (SUM(data.dr_amount),0) AS dr_amount,
					 	COALESCE (SUM(data.cr_amount),0) AS cr_amount';
				$add_item = 'GROUP BY data.accountId';
				$add_item .= ' ORDER BY data.transactionDate ASC';

			}
			else if($checked == 3)
			{
				if(!empty($date_from) OR !empty($date_to))
		    	{
		    		$add =  ' AND DATE(data.transactionDate) >= \''.$date_from.'\' AND DATE(data.transactionDate) <= \''.$date_to.'\' ';
		    	}
				$item = 'data.accountId AS accountId,
						account.account_name AS account_name,
					 	COALESCE (SUM(data.dr_amount),0) AS dr_amount,
					 	COALESCE (SUM(data.cr_amount),0) AS cr_amount,
					 	MONTH(data.transactionDate) AS month,
					 	MONTH(data.transactionDate) AS year';
				$add_item = 'GROUP BY MONTH(data.transactionDate),YEAR(data.transactionDate)';
				$add_item .= ' ORDER BY data.cr_amount ASC LIMIT 10';
			}
			else
			{
				if(!empty($date_from) OR !empty($date_to))
		    	{
		    		$add =  ' AND DATE(data.transactionDate) >= \''.$date_from.'\' AND DATE(data.transactionDate) <= \''.$date_to.'\' ';
		    	}

				$item = '*';
				$add .= " AND data.accountId = ".$account_id." ";
				$add_item .= ' ORDER BY data.transactionDate ASC';
			}


		}

		// $add ='';
		// $item = '*';
		// $add_item = '';

		$branch_id = $this->session->userdata('branch_id');

		//$add .= ' AND data.branch_id = '.$branch_id;
		//var_dump($checked);die();

		$select  = "
					SELECT
						".$item.",
						account.parent_account AS `accountParentId`,
						account_type.account_type_id AS account_type_id,
						account_type.account_type_name AS account_type_name,
						account.account_name AS accountName
					FROM

					(

						SELECT
							`account`.`account_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							'' AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							account.account_id AS `accountId`,
							CONCAT('Opening Balance as from',' ',`account`.`start_date`) AS `transactionName`,
							CONCAT('Opening Balance as from',' ',' ',`account`.`start_date`) AS `transactionDescription`,
							`account`.`account_opening_balance` AS `dr_amount`,
							'0' AS `cr_amount`,
							`account`.`start_date` AS `transactionDate`,
							`account`.`start_date` AS `createdAt`,
							`account`.`account_status` AS `status`,
							branch_id AS `branch_id`,
							'Fund Accounts' AS `transactionCategory`,
							'Account Opening Balance' AS `transactionClassification`,
							'' AS `transactionTable`,
							'account' AS `referenceTable`
							FROM
							account

						WHERE

						 	account.account_opening_balance <> 0 AND account.parent_account > 0



						UNION ALL


						SELECT
							`finance_transfer`.`finance_transfer_id` AS `transactionId`,
							`finance_transfer`.`finance_transfer_id` AS `referenceId`,
							'' AS `payingFor`,
							`finance_transfer`.`reference_number` AS `referenceCode`,
							`finance_transfer`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							`finance_transfer`.`account_to_id` AS `accountId`,
							`finance_transfer`.`remarks` AS `transactionName`,
							CONCAT('Amount Received ',' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
							`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
							0 AS `cr_amount`,
							`finance_transfer`.`transaction_date` AS `transactionDate`,
							`finance_transfer`.`created` AS `createdAt`,
							`finance_transfer`.`finance_transfer_status` AS `status`,
							`finance_transfer`.`branch_id` AS `branch_id`,
							'Transfer' AS `transactionCategory`,
							'Transfer' AS `transactionClassification`,
							'finance_transfer' AS `transactionTable`,
							'finance_transfered' AS `referenceTable`
						FROM
						`finance_transfer`
						WHERE  finance_transfer.finance_transfer_deleted = 0
						AND finance_transfer.finance_transfer_status = 1


						UNION ALL

						SELECT
							`finance_transfer`.`finance_transfer_id` AS `transactionId`,
							`finance_transfer`.`finance_transfer_id` AS `referenceId`,
							'' AS `payingFor`,
							`finance_transfer`.`reference_number` AS `referenceCode`,
							`finance_transfer`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							`finance_transfer`.`account_from_id` AS `accountId`,
							`finance_transfer`.`remarks` AS `transactionName`,
							CONCAT(' Amount Transfered ') AS `transactionDescription`,
							0 AS
							`dr_amount`,
							`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
							`finance_transfer`.`transaction_date` AS `transactionDate`,
							`finance_transfer`.`transaction_date` AS `transactionDate`,

							`finance_transfer`.`finance_transfer_status` AS `status`,
							`finance_transfer`.`branch_id` AS `branch_id`,
							'Transfer' AS `transactionCategory`,
							'Transfer' AS `transactionClassification`,
							'finance_transfered' AS `transactionTable`,
							'finance_transfer' AS `referenceTable`
						FROM
						`finance_transfer`
						WHERE
							finance_transfer.finance_transfer_deleted = 0
							AND finance_transfer.finance_transfer_status = 1

						UNION ALL


						SELECT
							`finance_purchase`.`finance_purchase_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							finance_purchase.creditor_id AS `recepientId`,
							'' AS `accountsclassfication`,
							`finance_purchase`.`account_from_id` AS `accountId`,
							finance_purchase.recipient_name AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`) AS `transactionDescription`,
							0 AS `dr_amount`,
							`finance_purchase`.`finance_purchase_amount` AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase`.`finance_purchase_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense' AS `transactionCategory`,
							'Expense' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'finance_purchase_payment' AS `referenceTable`
						FROM
						finance_purchase
						WHERE
						finance_purchase.finance_purchase_deleted = 0
						AND finance_purchase.account_to_id > 0

						UNION ALL

						SELECT
							`finance_purchase`.`finance_purchase_id` AS `transactionId`,
							'' AS `referenceId`,
							`finance_purchase`.`finance_purchase_id` AS `payingFor`,
							`finance_purchase`.`transaction_number` AS `referenceCode`,
							`finance_purchase`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`finance_purchase`.`creditor_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							`finance_purchase`.`account_to_id` AS `accountId`,
							finance_purchase.recipient_name AS `transactionName`,
							CONCAT( `finance_purchase`.`finance_purchase_description`)  AS `transactionDescription`,
							`finance_purchase`.`finance_purchase_amount` AS `dr_amount`,
							0 AS `cr_amount`,
							`finance_purchase`.`transaction_date` AS `transactionDate`,
							`finance_purchase`.`created` AS `createdAt`,
							`finance_purchase`.`finance_purchase_status` AS `status`,
							`finance_purchase`.`branch_id` AS `branch_id`,
							'Expense' AS `transactionCategory`,
							'Expense' AS `transactionClassification`,
							'finance_purchase' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							finance_purchase
						WHERE
							finance_purchase.finance_purchase_deleted = 0
							AND finance_purchase.account_to_id > 0


						UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							`creditor_payment`.`account_from_id` AS `accountId`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							 creditor.branch_id AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
						FROM

							`creditor_payment_item`,creditor_payment,creditor_invoice,creditor


						WHERE creditor_payment_item.invoice_type = 0
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
							AND creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
							AND creditor_payment.creditor_payment_status = 1
							AND creditor_invoice.creditor_invoice_status = 1
							AND creditor_invoice.creditor_id = creditor.creditor_id
							AND creditor_invoice.transaction_date >= creditor.start_date




						UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							`creditor_payment`.`account_from_id` AS `accountId`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							 creditor.branch_id AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
						FROM

							`creditor_payment_item`,creditor_payment,creditor

						WHERE creditor_payment_item.invoice_type = 2
						AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
						AND creditor.creditor_id = creditor_payment_item.creditor_id
						AND creditor_payment.transaction_date >= creditor.start_date

						UNION ALL

						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							`creditor_payment`.`account_from_id` AS `accountId`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							 creditor.branch_id AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
						FROM

							`creditor_payment_item`,creditor_payment,creditor

						WHERE
							creditor_payment_item.invoice_type = 3
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1
							AND creditor.creditor_id = creditor_payment_item.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date





						UNION ALL



						SELECT
							`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
							`creditor_payment`.`creditor_payment_id` AS `referenceId`,
							`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
							`creditor_payment`.`reference_number` AS `referenceCode`,
							`creditor_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`creditor_payment`.`creditor_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							creditor_payment.account_from_id AS `accountId`,
							`creditor_payment_item`.`description` AS `transactionName`,
							CONCAT('Payment to ', creditor.creditor_name ,'for ',`orders`.`supplier_invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`creditor_payment_item`.`amount_paid` AS `cr_amount`,
							`creditor_payment`.`transaction_date` AS `transactionDate`,
							`creditor_payment`.`created` AS `createdAt`,
							`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
							 creditor.branch_id AS `branch_id`,
							'Creditor Payment' AS `transactionCategory`,
							'Creditors Invoices Payments' AS `transactionClassification`,
							'creditor_payment' AS `transactionTable`,
							'creditor_payment_item' AS `referenceTable`
						FROM

						`creditor_payment_item`,creditor_payment,orders,creditor

						WHERE creditor_payment_item.invoice_type = 1
							AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
							AND creditor_payment.creditor_payment_status = 1
							AND orders.order_id = creditor_payment_item.creditor_invoice_id
							AND orders.supplier_id = creditor.creditor_id
							AND creditor_payment.transaction_date >= creditor.start_date




						UNION ALL

						SELECT
							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							journal_entry.account_from_id AS `accountId`,
							CONCAT(journal_entry.journal_entry_description)  AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							`journal_entry`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							journal_entry.branch_id AS `branch_id`,
							'Journal Credit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
							journal_entry

						WHERE journal_entry.journal_entry_deleted = 0

						UNION ALL


						SELECT
							`journal_entry`.`journal_entry_id` AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							'' AS `referenceCode`,
							journal_entry.document_number AS `transactionCode`,
							'' AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							journal_entry.account_to_id AS `accountId`,
							CONCAT(journal_entry.journal_entry_description)  AS `transactionName`,
							CONCAT(journal_entry.journal_entry_description) AS `transactionDescription`,
							0 AS `dr_amount`,
							`journal_entry`.`amount_paid` AS `cr_amount`,
							`journal_entry`.`payment_date` AS `transactionDate`,
							`journal_entry`.`payment_date` AS `createdAt`,
							`journal_entry`.`journal_entry_status` AS `status`,
							journal_entry.branch_id AS `branch_id`,
							'Journal Debit' AS `transactionCategory`,
							'Journal' AS `transactionClassification`,
							'journal_entry' AS `transactionTable`,
							'account' AS `referenceTable`
						FROM
							journal_entry

						WHERE
							journal_entry.journal_entry_deleted = 0

						UNION ALL


						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							`payment_method`.`account_id` AS `accountId`,
							'' AS `transactionName`,
							CONCAT( 'Patient Payment: ',payment_method.payment_method,' <strong>Receipt No:</strong> ',payments.confirm_number) AS `transactionDescription`,
							payment_item.payment_item_amount AS `dr_amount`,
							0 AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							visit_invoice.branch_id AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							payments,payment_item,payment_method,visit_invoice
						WHERE
							payments.cancel = 0 AND payments.payment_type = 1
							AND visit_invoice.visit_invoice_id = payment_item.visit_invoice_id
							AND payment_item.invoice_type = 1
							AND payments.payment_id = payment_item.payment_id
							AND payments.payment_method_id = payment_method.payment_method_id


						UNION ALL


						SELECT

							payments.payment_id AS `transactionId`,
							'' AS `referenceId`,
							'' AS `payingFor`,
							payments.confirm_number AS `referenceCode`,
							payments.confirm_number AS `transactionCode`,
							payments.patient_id AS `patient_id`,
							'' AS `recepientId`,
							'' AS `accountsclassfication`,
							`payment_method`.`account_id` AS `accountId`,
							'' AS `transactionName`,
							CONCAT('Payment On Account') AS `transactionDescription`,
							(payment_item.payment_item_amount) AS `dr_amount`,
							0 AS `cr_amount`,
							payments.payment_date AS `transactionDate`,
							payments.payment_date AS `createdAt`,
							`payments`.`cancel` AS `status`,
							payments.branch_id AS `branch_id`,
							'Patient Income' AS `transactionCategory`,
							'Payments' AS `transactionClassification`,
							'account_payments' AS `transactionTable`,
							'' AS `referenceTable`
						FROM
							payments,payment_item,payment_method
						WHERE
							payments.cancel = 0
							AND payments.payment_type = 1
							AND payment_item.invoice_type = 3
							AND payments.payment_id = payment_item.payment_id
							AND payments.payment_method_id = payment_method.payment_method_id





					UNION ALL

					SELECT
						`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
						`provider_payment`.`provider_payment_id` AS `referenceId`,
						`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
						`provider_payment`.`reference_number` AS `referenceCode`,
						`provider_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
							`provider_payment`.`provider_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`provider_payment`.`account_from_id` AS `accountId`,
						`provider_payment_item`.`description` AS `transactionName`,
						CONCAT('Provider payment for invoice of ',' ',`provider_invoice`.`invoice_number`)  AS `transactionDescription`,
						0 AS `dr_amount`,
						`provider_payment_item`.`amount_paid` AS `cr_amount`,
						`provider_payment`.`transaction_date` AS `transactionDate`,
						`provider_payment`.`created` AS `createdAt`,
						`provider_payment_item`.`provider_payment_item_status` AS `status`,
						provider.branch_id AS `branch_id`,
						'Expense Payment' AS `transactionCategory`,
						'Providers Invoices Payments' AS `transactionClassification`,
						'provider_payment' AS `transactionTable`,
						'provider_payment_item' AS `referenceTable`
					FROM
						`provider_payment_item`,provider_payment,provider_invoice,provider

					WHERE provider_payment_item.invoice_type = 0
						AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id
						AND provider_payment.provider_payment_status = 1
						AND provider_invoice.provider_invoice_id = provider_payment_item.provider_invoice_id
						AND provider_invoice.provider_invoice_status = 1
						AND provider.provider_id = provider_invoice.provider_id AND provider_invoice.transaction_date >= provider.start_date


					UNION ALL

					SELECT
						`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
						`provider_payment`.`provider_payment_id` AS `referenceId`,
						`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
						`provider_payment`.`reference_number` AS `referenceCode`,
						`provider_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`provider_payment`.`provider_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`provider_payment`.`account_from_id` AS `accountId`,
						`provider_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment of opening balance')  AS `transactionDescription`,
						0 AS `dr_amount`,
						`provider_payment_item`.`amount_paid` AS `cr_amount`,
						`provider_payment`.`transaction_date` AS `transactionDate`,
						`provider_payment`.`created` AS `createdAt`,
						`provider_payment_item`.`provider_payment_item_status` AS `status`,
						provider.branch_id AS `branch_id`,
						'Expense Payment' AS `transactionCategory`,
						'Providers Invoices Payments' AS `transactionClassification`,
						'provider_payment' AS `transactionTable`,
						'provider_payment_item' AS `referenceTable`
					FROM
						`provider_payment_item`,provider_payment,provider

					WHERE provider_payment_item.invoice_type = 2
						AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id
						AND provider_payment.provider_payment_status = 1
						AND provider.provider_id = provider_payment_item.provider_id
						AND provider_payment.transaction_date >= provider.start_date



					UNION ALL

					SELECT
						`provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
						`provider_payment`.`provider_payment_id` AS `referenceId`,
						`provider_payment_item`.`provider_invoice_id` AS `payingFor`,
						`provider_payment`.`reference_number` AS `referenceCode`,
						`provider_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
					  `provider_payment`.`provider_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`provider_payment`.`account_from_id` AS `accountId`,
						`provider_payment_item`.`description` AS `transactionName`,
						CONCAT('Payment on account')  AS `transactionDescription`,
						0 AS `dr_amount`,
						`provider_payment_item`.`amount_paid` AS `cr_amount`,
						`provider_payment`.`transaction_date` AS `transactionDate`,
						`provider_payment`.`created` AS `createdAt`,
						`provider_payment_item`.`provider_payment_item_status` AS `status`,
						provider.branch_id AS `branch_id`,
						'Expense Payment' AS `transactionCategory`,
						'Providers Invoices Payments' AS `transactionClassification`,
						'provider_payment' AS `transactionTable`,
						'provider_payment_item' AS `referenceTable`
					FROM
						`provider_payment_item`,provider_payment,provider

					WHERE
							provider_payment_item.invoice_type = 3 AND provider_payment.provider_payment_id = provider_payment_item.provider_payment_id
							AND provider_payment.provider_payment_status = 1
							AND provider.provider_id = provider_payment.provider_id AND provider_payment.transaction_date >= provider.start_date


					UNION ALL


					SELECT
						`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
						`loan_invoice`.`loan_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`loan_invoice`.`invoice_number` AS `referenceCode`,
						`loan_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
							`loan_invoice`.`loan_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`loan`.`account_from_id` AS `accountId`,
						`loan_invoice_item`.`item_description` AS `transactionName`,
						`loan_invoice_item`.`item_description` AS `transactionDescription`,
						 0 AS `dr_amount`,
						`loan_invoice_item`.`total_amount` AS `cr_amount`,
						`loan_invoice`.`transaction_date` AS `transactionDate`,
						`loan_invoice`.`created` AS `createdAt`,
						`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
						loan.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Loans' AS `transactionClassification`,
						'loan_invoice_item' AS `transactionTable`,
						'loan_invoice' AS `referenceTable`
					FROM
						`loan_invoice_item`,loan_invoice,loan

					WHERE
						loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id
						AND loan_invoice.loan_invoice_status = 1
						AND loan.loan_id = loan_invoice.loan_id
						AND loan_invoice.transaction_date >= loan.start_date

					UNION ALL

					SELECT
						`loan_invoice_item`.`loan_invoice_item_id` AS `transactionId`,
						`loan_invoice`.`loan_invoice_id` AS `referenceId`,
						'' AS `payingFor`,
						`loan_invoice`.`invoice_number` AS `referenceCode`,
						`loan_invoice`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`loan_invoice`.`loan_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`loan`.`account_to_id` AS `accountId`,
						`loan_invoice_item`.`item_description` AS `transactionName`,
						`loan_invoice_item`.`item_description` AS `transactionDescription`,
						`loan_invoice_item`.`total_amount` AS `dr_amount`,
						'0' AS `cr_amount`,
						`loan_invoice`.`transaction_date` AS `transactionDate`,
						`loan_invoice`.`created` AS `createdAt`,
						`loan_invoice_item`.`loan_invoice_item_status` AS `status`,
						loan.branch_id AS `branch_id`,
						'Expense' AS `transactionCategory`,
						'Loans' AS `transactionClassification`,
						'loan_invoice_item' AS `transactionTable`,
						'loan_invoice' AS `referenceTable`
					FROM
						`loan_invoice_item`,loan_invoice,loan

					WHERE
						loan_invoice.loan_invoice_id = loan_invoice_item.loan_invoice_id
						AND loan_invoice.loan_invoice_status = 1
						AND loan.loan_id = loan_invoice.loan_id
						AND loan_invoice.transaction_date >= loan.start_date


					UNION ALL

					SELECT
						`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
						`loan_payment`.`loan_payment_id` AS `referenceId`,
						`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
						`loan_payment`.`reference_number` AS `referenceCode`,
						`loan_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
							`loan_payment`.`loan_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`loan_payment`.`account_from_id` AS `accountId`,
						CONCAT('Payment from ') AS `transactionName`,
						CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
						0 AS `dr_amount`,
						`loan_payment_item`.`amount_paid` AS `cr_amount`,
						`loan_payment`.`transaction_date` AS `transactionDate`,
						`loan_payment`.`created` AS `createdAt`,
						`loan_payment_item`.`loan_payment_item_status` AS `status`,
						loan.branch_id AS `branch_id`,
						'Expense Payment' AS `transactionCategory`,
						'Loans Invoices Payments' AS `transactionClassification`,
						'loan_payment' AS `transactionTable`,
						'loan_payment_item' AS `referenceTable`
					FROM
						`loan_payment_item`,loan_payment,loan_invoice,loan

					WHERE loan_payment_item.invoice_type = 0
						AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
						AND loan_payment.loan_payment_status = 1
						AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
						AND loan_invoice.loan_invoice_status = 1
						AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
						AND loan.account_from_id = loan_payment_item.account_to_id


					UNION ALL

					SELECT
						`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
						`loan_payment`.`loan_payment_id` AS `referenceId`,
						`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
						`loan_payment`.`reference_number` AS `referenceCode`,
						`loan_payment`.`document_number` AS `transactionCode`,
						'' AS `patient_id`,
						`loan_payment`.`loan_id` AS `recepientId`,
						'' AS `accountsclassfication`,
						`loan`.`account_from_id` AS `accountId`,
						CONCAT('Payment from ') AS `transactionName`,
						CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
						`loan_payment_item`.`amount_paid` AS `dr_amount`,
						0 AS `cr_amount`,
						`loan_payment`.`transaction_date` AS `transactionDate`,
						`loan_payment`.`created` AS `createdAt`,
						`loan_payment_item`.`loan_payment_item_status` AS `status`,
						loan.branch_id AS `branch_id`,
						'Expense Payment' AS `transactionCategory`,
						'Loans Invoices Payments' AS `transactionClassification`,
						'loan_payment' AS `transactionTable`,
						'loan_payment_item' AS `referenceTable`
					FROM
						`loan_payment_item`,loan_payment,loan_invoice,loan

					WHERE loan_payment_item.invoice_type = 0
						AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
						AND loan_payment.loan_payment_status = 1
						AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
						AND loan_invoice.loan_invoice_status = 1
						AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
						AND loan.account_from_id = loan_payment_item.account_to_id

					UNION ALL

					SELECT
							`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
							`loan_payment`.`loan_payment_id` AS `referenceId`,
							`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
							`loan_payment`.`reference_number` AS `referenceCode`,
							`loan_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
							`loan_payment`.`loan_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							loan_payment_item.account_to_id AS `accountId`,
							CONCAT('Payment ') AS `transactionName`,
							CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
							`loan_payment_item`.`amount_paid` AS `dr_amount`,
							0 AS `cr_amount`,
							`loan_payment`.`transaction_date` AS `transactionDate`,
							`loan_payment`.`created` AS `createdAt`,
							`loan_payment_item`.`loan_payment_item_status` AS `status`,
							loan.branch_id AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Loans Invoices Payments' AS `transactionClassification`,
							'loan_payment' AS `transactionTable`,
							'loan_payment_item' AS `referenceTable`
						FROM
							`loan_payment_item`,loan_payment,loan_invoice,loan,account

							WHERE loan_payment_item.invoice_type = 0
							AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
							AND loan_payment.loan_payment_status = 1
							AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
							AND loan_invoice.loan_invoice_status = 1
							AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
							AND loan.account_from_id <> loan_payment_item.account_to_id


						UNION ALL


						SELECT
							`loan_payment_item`.`loan_payment_item_id` AS `transactionId`,
							`loan_payment`.`loan_payment_id` AS `referenceId`,
							`loan_payment_item`.`loan_invoice_id` AS `payingFor`,
							`loan_payment`.`reference_number` AS `referenceCode`,
							`loan_payment`.`document_number` AS `transactionCode`,
							'' AS `patient_id`,
								`loan_payment`.`loan_id` AS `recepientId`,
							'' AS `accountsclassfication`,
							loan_payment.account_from_id AS `accountId`,
							CONCAT('Payment ') AS `transactionName`,
							CONCAT('Payment for invoice of ',' ',`loan_invoice`.`invoice_number`)  AS `transactionDescription`,
							0 AS `dr_amount`,
							`loan_payment_item`.`amount_paid` AS `cr_amount`,
							`loan_payment`.`transaction_date` AS `transactionDate`,
							`loan_payment`.`created` AS `createdAt`,
							`loan_payment_item`.`loan_payment_item_status` AS `status`,
							loan.branch_id AS `branch_id`,
							'Expense Payment' AS `transactionCategory`,
							'Loans Invoices Payments' AS `transactionClassification`,
							'loan_payment' AS `transactionTable`,
							'loan_payment_item' AS `referenceTable`
						FROM
							`loan_payment_item`,loan_payment,loan_invoice,loan

						WHERE loan_payment_item.invoice_type = 0
							AND loan_payment.loan_payment_id = loan_payment_item.loan_payment_id
							AND loan_payment.loan_payment_status = 1
							AND loan_invoice.loan_invoice_id = loan_payment_item.loan_invoice_id
							AND loan_invoice.loan_invoice_status = 1
							AND loan.loan_id = loan_invoice.loan_id AND loan_invoice.transaction_date >= loan.start_date
							AND loan.account_from_id <> loan_payment_item.account_to_id



						UNION ALL

							SELECT
								batch_receipts.batch_receipt_id AS `transactionId`,
								'' AS `referenceId`,
								'' AS `payingFor`,
								batch_receipts.receipt_number AS `referenceCode`,
								batch_receipts.receipt_number AS `transactionCode`,
								batch_unallocations.unallocated_payment_id AS `patient_id`,
								0 AS `recepientId`,
								'' AS `accountsclassfication`,
								batch_receipts.bank_id AS `accountId`,
								CONCAT('BATCH OVERPAYMENT :',' : ',batch_unallocations.reason) AS `transactionName`,
								CONCAT('BATCH OVERPAYMENT :',' : ',batch_unallocations.reason) AS `transactionDescription`,
								batch_unallocations.amount_paid AS `dr_amount`,
								0 AS `cr_amount`,
								batch_receipts.payment_date AS `transactionDate`,
								batch_receipts.payment_date  AS `createdAt`,
								1 AS `status`,
								batch_receipts.branch_id AS `branch_id`,
								'OVERPAYMENTS' AS `transactionCategory`,
								'Insuance Overpayments' AS `transactionClassification`,
								'account_payments' AS `transactionTable`,
								'' AS `referenceTable`
							FROM
									batch_unallocations,batch_receipts

							WHERE
								batch_receipts.batch_receipt_id = batch_unallocations.batch_receipt_id
								AND batch_unallocations.unallocated_payment_delete = 0
								AND  batch_unallocations.allocation_type_id = 0





							UNION ALL

								SELECT
									`payroll_payment_item`.`payroll_payment_item_id` AS `transactionId`,
									`payroll_payment`.`payroll_payment_id` AS `referenceId`,
									`payroll_payment_item`.`payroll_invoice_id` AS `payingFor`,
									`payroll_payment`.`reference_number` AS `referenceCode`,
									`payroll_payment`.`document_number` AS `transactionCode`,
									'' AS `patient_id`,
									`statutory_accounts`.`statutory_account_id` AS `recepientId`,
									'' AS `accountsclassfication`,
									`payroll_payment`.`account_from_id` AS `accountId`,
									CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for)) AS `transactionName`,
									CONCAT(statutory_accounts.statutory_account_name,'-',MONTH(payroll_summary.payroll_created_for),' ',YEAR(payroll_summary.payroll_created_for))   AS `transactionDescription`,
									0 AS `dr_amount`,
									`payroll_payment_item`.`amount_paid` AS `cr_amount`,
									`payroll_payment`.`transaction_date` AS `transactionDate`,
									`payroll_payment`.`created` AS `createdAt`,
									`payroll_payment_item`.`payroll_payment_item_status` AS `status`,
									2 AS `branch_id`,
									'Payroll Payment' AS `transactionCategory`,
									'Payroll Payments' AS `transactionClassification`,
									'payroll_payment' AS `transactionTable`,
									'payroll_payment_item' AS `referenceTable`
								FROM
									`payroll_payment_item`,payroll_payment,payroll_summary,statutory_accounts

								WHERE payroll_payment_item.invoice_type = 0
								AND payroll_payment.payroll_payment_id = payroll_payment_item.payroll_payment_id
								AND payroll_payment.payroll_payment_status = 1
								AND payroll_summary.payroll_summary_id = payroll_payment_item.payroll_invoice_id
								AND statutory_accounts.statutory_account_id = payroll_payment_item.payroll_id



							UNION ALL

								SELECT
									`account_payments`.`account_payment_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									account_payments.receipt_number AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									account_payments.account_from_id AS `accountId`,
									CONCAT(account_payments.account_payment_description) AS `transactionName`,
									CONCAT(account_payments.account_payment_description) AS `transactionDescription`,
									0 AS `dr_amount`,
									`account_payments`.`amount_paid` AS `cr_amount`,
									`account_payments`.`payment_date` AS `transactionDate`,
									`account_payments`.`payment_date` AS `createdAt`,
									`account_payments`.`account_payment_status` AS `status`,
									account_payments.branch_id AS `branch_id`,
									'Journal Credit' AS `transactionCategory`,
									'Journal' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'account' AS `referenceTable`
								FROM
									account_payments

								WHERE account_payments.account_payment_deleted = 0

							UNION ALL


								SELECT
									`account_payments`.`account_payment_id` AS `transactionId`,
									'' AS `referenceId`,
									'' AS `payingFor`,
									'' AS `referenceCode`,
									account_payments.receipt_number AS `transactionCode`,
									'' AS `patient_id`,
									'' AS `recepientId`,
									'' AS `accountsclassfication`,
									account_payments.account_to_id AS `accountId`,
									CONCAT(account_payments.account_payment_description) AS `transactionName`,
									CONCAT(account_payments.account_payment_description) AS `transactionDescription`,
									`account_payments`.`amount_paid` AS `dr_amount`,
									0 AS `cr_amount`,
									`account_payments`.`payment_date` AS `transactionDate`,
									`account_payments`.`payment_date` AS `createdAt`,
									`account_payments`.`account_payment_status` AS `status`,
									account_payments.branch_id AS `branch_id`,
									'Journal Debit' AS `transactionCategory`,
									'Journal' AS `transactionClassification`,
									'account_payments' AS `transactionTable`,
									'account' AS `referenceTable`
								FROM
									account_payments

								WHERE
									account_payments.account_payment_deleted = 0


					) AS data,account,account_type WHERE
						data.transactionId > 0
						AND data.accountId = account.account_id
						AND  account.account_type_id = account_type.account_type_id
						".$add." ".$add_item."


					";


	  $query = $this->db->query($select);


	   return $query;

	}

	public function get_customer_statement_balance($lender_id,$start_date=null)
	{

		$search_status = $this->session->userdata('lender_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_lender_expense');
			$date_to = $this->session->userdata('date_to_lender_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate < \''.$date_from.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_to.'\'';
			}
		}
		else
		{

			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_lender_ledger_aging_by_date,lender');
		$this->db->select('SUM(`dr_amount`) AS dr_amount,SUM(cr_amount) AS cr_amount');
		$this->db->where('lender.lender_id = v_lender_ledger_aging_by_date.recepientId AND v_lender_ledger_aging_by_date.transactionDate >= lender.start_date AND recepientId = '.$lender_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}


	/*
	*	get a single creditor's details
	*	@param int $creditor_id
	*
	*/
	public function get_lender($lender_id)
	{
		//retrieve all users
		$this->db->from('lender');
		$this->db->select('*');
		$this->db->where('lender_id = '.$lender_id);
		$query = $this->db->get();

		return $query;
	}


	public function get_customer_statement($lender_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{

			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_lender_ledger_aging_by_date,lender');
		$this->db->select('*');
		$this->db->where('lender.lender_id = v_lender_ledger_aging_by_date.recepientId AND v_lender_ledger_aging_by_date.transactionDate >= lender.start_date AND recepientId = '.$lender_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}

	public function get_reserved_accounts()
	{
		$this->db->where('account.reserved = 1 AND account.account_type_id = account_type.account_type_id AND account.account_deleted = 0');
		$query = $this->db->get('account,account_type');

		return $query;
	}

	public function get_all_opening_balances()
	{
		$this->db->where('account.reserved = 1 AND account.account_type_id = account_type.account_type_id AND account.account_deleted = 0 AND account.account_id = account_opening_balance.account_id AND status = 1');
		$query = $this->db->get('account,account_type,account_opening_balance');

		return $query;
	}


	public function get_accounts_by_type($args)
	{
		 $this->db->select('account.account_id,account.account_name,account_type.account_type_name');
		$this->db->where('account.account_type_id = account_type.account_type_id AND account.account_deleted = 0 AND account_type.account_type_name = "'.$args.'" AND account.parent_account <> 0');
		$query = $this->db->get('account,account_type');

		return $query;
	}

	public function get_expense_share($account_id)
	{
		$shareholder_id = $this->session->userdata('shareholder_id');

		$date_from = $this->session->userdata('date_from_general_ledger');

		$financial_year = date('Y',strtotime($date_from));

		$this->db->where('personnel_id = '.$shareholder_id.' AND expense_account_id = '.$account_id.' AND financial_year = '.$financial_year.' ');
		$query = $this->db->get('personnel_expense_share');
		$share = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$share = $value->share;
			}
		}

		if(empty($share))
		{
			$share = 0;
		}


		return $share;

	}

	public function update_expense_share($account_id)
	{

		$shareholder_id = $this->session->userdata('shareholder_id');

		$date_from = $this->session->userdata('date_from_general_ledger');
		$share = $this->input->post('share');

		$financial_year = date('Y',strtotime($date_from));


		$sql = "insert ignore into personnel_expense_share (personnel_id, share, expense_account_id,financial_year,period)
										values (".$shareholder_id.",".$share.",".$account_id.",".$financial_year.",'".$date_from."')
										on duplicate key update share = ".$share."";

		$this->db->query($sql);

	}


	public function get_shareholder_salaries($shareholder_id)
	{
		$shareholder_id = $this->session->userdata('shareholder_id');

		$date_from = $this->session->userdata('date_from_general_ledger');
		$share = $this->input->post('share');

		$financial_year = date('Y',strtotime($date_from));


		$this->db->where('personnel_id = '.$shareholder_id.' AND YEAR(payroll_created) = "'.$financial_year.'" ');
		$query = $this->db->get('payroll_list');

		return $query;
	}

	public function get_provider_unallocated_funds($provider_id)
	{
	    $selected_statement = "
	                  SELECT
	                      SUM(cr_amount) AS cr_amount

	                  FROM
	                  (
	                  SELECT
	                  `provider_payment_item`.`provider_payment_item_id` AS `transactionId`,
	                  `provider_payment`.`provider_payment_id` AS `referenceId`,
	                  `provider_payment`.`reference_number` AS `referenceCode`,
	                  `provider_payment_item`.`provider_invoice_id` AS `payingFor`,
	                  `provider_payment`.`document_number` AS `transactionCode`,
	                  '' AS `patient_id`,
	                  `provider_payment`.`provider_id` AS `recepientId`,
	                  `account`.`parent_account` AS `accountParentId`,
	                  `account_type`.`account_type_name` AS `accountsclassfication`,
	                  `provider_payment`.`account_from_id` AS `accountId`,
	                  `account`.`account_name` AS `accountName`,
	                  `provider_payment_item`.`description` AS `transactionName`,
	                  CONCAT('Payment on account')  AS `transactionDescription`,
	                      0 AS `department_id`,
	                  0 AS `dr_amount`,
	                  `provider_payment_item`.`amount_paid` AS `cr_amount`,
	                  `provider_payment`.`transaction_date` AS `transactionDate`,
	                  `provider_payment`.`created` AS `createdAt`,
	                  `provider_payment`.`created` AS `referenceDate`,
	                  `provider_payment_item`.`provider_payment_item_status` AS `status`,
	                  'Expense Payment' AS `transactionCategory`,
	                  'providers Invoices Payments' AS `transactionClassification`,
	                  'provider_payment' AS `transactionTable`,
	                  'provider_payment_item' AS `referenceTable`
	                FROM
	                  (
	                    (
	                      (
	                        `provider_payment_item`
	                        JOIN `provider_payment` ON(
	                          (
	                            provider_payment.provider_payment_id = provider_payment_item.provider_payment_id
	                          )
	                        )
	                      )
	                      JOIN account ON(
	                        (
	                          account.account_id = provider_payment.account_from_id
	                        )
	                      )
	                    )
	                    JOIN `account_type` ON(
	                      (
	                        account_type.account_type_id = account.account_type_id
	                      )
	                    )
	                    JOIN `provider` ON(
	                      (
	                        provider.provider_id = provider_payment_item.provider_id
	                      )
	                    )
	                  )
	                  WHERE provider_payment_item.invoice_type = 3) AS data WHERE data.recepientId = ".$provider_id;
	          $query = $this->db->query($selected_statement);
	          $checked = $query->row();

	          $dr_amount = $checked->cr_amount;

	          return $dr_amount;
	  	}
	  	public function get_unallocated_customers_funds($lender_id)
		{
		    $selected_statement = "
		                  SELECT
		                      SUM(cr_amount) AS cr_amount

		                  FROM
		                  (
		                  SELECT
		                  `lender_payment_item`.`lender_payment_item_id` AS `transactionId`,
		                  `lender_payment`.`lender_payment_id` AS `referenceId`,
		                  `lender_payment`.`reference_number` AS `referenceCode`,
		                  `lender_payment_item`.`lender_invoice_id` AS `payingFor`,
		                  `lender_payment`.`document_number` AS `transactionCode`,
		                  '' AS `patient_id`,
		                  `lender_payment`.`lender_id` AS `recepientId`,
		                  `account`.`parent_account` AS `accountParentId`,
		                  `account_type`.`account_type_name` AS `accountsclassfication`,
		                  `lender_payment`.`account_from_id` AS `accountId`,
		                  `account`.`account_name` AS `accountName`,
		                  `lender_payment_item`.`description` AS `transactionName`,
		                  CONCAT('Payment on account')  AS `transactionDescription`,
		                      0 AS `department_id`,
		                  0 AS `dr_amount`,
		                  `lender_payment_item`.`amount_paid` AS `cr_amount`,
		                  `lender_payment`.`transaction_date` AS `transactionDate`,
		                  `lender_payment`.`created` AS `createdAt`,
		                  `lender_payment`.`created` AS `referenceDate`,
		                  `lender_payment_item`.`lender_payment_item_status` AS `status`,
		                  'Expense Payment' AS `transactionCategory`,
		                  'lenders Invoices Payments' AS `transactionClassification`,
		                  'lender_payment' AS `transactionTable`,
		                  'lender_payment_item' AS `referenceTable`
		                FROM
		                  (
		                    (
		                      (
		                        `lender_payment_item`
		                        JOIN `lender_payment` ON(
		                          (
		                            lender_payment.lender_payment_id = lender_payment_item.lender_payment_id AND lender_payment.lender_payment_status = 1
		                          )
		                        )
		                      )
		                      JOIN account ON(
		                        (
		                          account.account_id = lender_payment.account_from_id
		                        )
		                      )
		                    )
		                    JOIN `account_type` ON(
		                      (
		                        account_type.account_type_id = account.account_type_id
		                      )
		                    )
		                    JOIN `lender` ON(
		                      (
		                        lender.lender_id = lender_payment_item.lender_id
		                      )
		                    )
		                  )
		                  WHERE lender_payment_item.invoice_type = 3) AS data WHERE data.recepientId = ".$lender_id;
		          $query = $this->db->query($selected_statement);
		          $checked = $query->row();

		          $dr_amount = $checked->cr_amount;

		          return $dr_amount;
	  	}


}
?>
