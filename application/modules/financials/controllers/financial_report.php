<?php
// if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once "./application/modules/admin/controllers/admin.php";
error_reporting(0);
class Financial_Report extends MX_Controller
{
	private $documents_path;
	private $session_account;
	private $data;
	private $accounts_payable_id;
	private $payroll_liability_id;
	private $accounts_receivable_id;
	private $providers_liability_id;
	private $arrPayblesPayments;
	private $arrReceivablePayment;
	private $supplier_opening_balance_id;
	private $receivables_prepayment_id;
	private $retained_earnings_id;
	private $retained_earnings;
	private $loans_payable_id;
	private $title;
	private $meta;
	private $forOpeningTB;
	private $forBalanceSheet;
	private $for_pnl;
	private $for_doctors_pnl;
	private $for_cashflow_pnl;
	private $arrAssetTypes;
	private $profit;
	private $financialEndMonth;
	private $dateFrom;
	private $dateTo;
	private $forTrialBalance;
	private $cal_opening_balance;
	private $financialStartDate;
	private $startYear;
	private $detailedPnl;
	private $dataDetailed;
	private $datadoctorsPnl;
	private $dataAssets;
	private $dataList;

	private $dataAllDetailed;
	private $dataAllList;

	private $reportingPeriod;
	private $other_payable_id;


	function __construct($args=null)
	{
		$this->accounts_payable_id = 0;
		$this->accounts_receivable_id = 0;
		$this->other_payable_id = 0;
		$this->receivables_prepayment_id = 0;
		$this->supplier_opening_balance_id =0;
		$this->payroll_liability_id = 0;
		$this->providers_liability_id = 0;
		$this->loans_payable_id = 0;
		$this->profit = 0;
		$this->financialEndMonth = 0;
		$this->retained_earnings_id = 0;
		$this->retained_earnings = 0;
		$this->financialStartDate = '';
		$this->startYear = date('Y');
		$this->detailedPnl = false;
		$this->dataAllList = array();
		$this->dataList = array();
		$this->reportingPeriod = null;
		$this->load->model('company_financial_model');
		$this->arrPayblesPayments = array();
		$this->arrReceivablePayment = array();
		$this->title = '';
		$this->meta = array();

		$accounts_config_rs = $this->company_financial_model->get_staging_accounts();

		$this->session_account = array();

		if($accounts_config_rs->num_rows() > 0)
		{
			foreach ($accounts_config_rs->result() as $key => $value) {
				// code...
				$staing_account_id = $value->account_id;
				$reference_name = $value->reference_name;

				$this->session_account[$reference_name] = $staing_account_id;
				if($reference_name == "accounts_payable_id")
					$this->accounts_payable_id = $this->session_account[$reference_name];
				else if($reference_name == "accounts_receivable_id")
					$this->accounts_receivable_id = $this->session_account[$reference_name];
				else if($reference_name == "receivables_prepayment_id")
					$this->receivables_prepayment_id = $this->session_account[$reference_name];
				else if($reference_name == "supplier_opening_balance_id")
					$this->supplier_opening_balance_id = $this->session_account[$reference_name];
				else if($reference_name == "payroll_liability_id")
					$this->payroll_liability_id = $this->session_account[$reference_name];
				else if($reference_name == "providers_liability_id")
					$this->providers_liability_id = $this->session_account[$reference_name];
				else if($reference_name == "loan_liability_account_id")
					$this->loans_payable_id = $this->session_account[$reference_name];
				else if($reference_name == "retained_earnings_id")
					$this->retained_earnings_id = $this->session_account[$reference_name];
				else if($reference_name == "other_payable_id")
					$this->other_payable_id = $this->session_account[$reference_name];

				




			}
		}
		$this->financialStartDate =  $this->financialStartYearPeriod();

		if(!empty($this->financialStartDate))
		{
			$exploded = explode("-", $this->financialStartDate);

			$this->startYear = $exploded[0];
		}



		$this->financialEndMonth = $this->financialPeriod();

		// var_dump($args);die();
		$this->cal_opening_balance = (is_array($args) and array_key_exists("cal_opening_balance",$args) and $args["cal_opening_balance"] == true);
		$this->forOpeningTB = (is_array($args) and array_key_exists("opening_tb",$args) and $args["opening_tb"] == true);
		$this->forTrialBalance = (is_array($args) and array_key_exists("trial_balance",$args) and $args["trial_balance"] == true);
		$this->forBalanceSheet = (is_array($args) and array_key_exists("balance_sheet",$args) and $args["balance_sheet"] == true);
		$this->for_pnl = (is_array($args) and array_key_exists("pnl",$args) and $args["pnl"] == true);
		$this->for_doctors_pnl = (is_array($args) and array_key_exists("doctorspnl",$args) and $args["doctorspnl"] == true);
		$this->for_cashflow_pnl = (is_array($args) and array_key_exists("cashflowpnl",$args) and $args["cashflowpnl"] == true);
		$this->detailedPnl = (is_array($args) and array_key_exists("detailedPnl",$args) and $args["detailedPnl"] == true);

		$this->reportingPeriod = ((is_array($args) and array_key_exists("reportingPeriod",$args))? $args["reportingPeriod"]:null);

		// echo "<pre>" . print_r($args) . "</pre>";


		if($this->cal_opening_balance)
		{


			// echo "<br>Date provided: " .$args["date"];
			$this->dateTo = new DateTime($args["date"]);
			$this->dateFrom = new DateTime($this->dateTo->format("Y-" . ($this->financialEndMonth < 10 ? "0$this->financialEndMonth":$this->financialEndMonth) . "-01"));
			$this->dateFrom->add(new DateInterval("P1M"));
			if($this->dateFrom->format("Ym") > $this->dateTo->format("Ym"))
				$this->dateFrom->sub(new DateInterval("P1Y"));
			// $this->session->set_userdata('date_from_general_ledger',$this->dateFrom->format("Y-m-d"));
			// $this->session->set_userdata('date_to_general_ledgeOR r',$this->dateTo->format("Y-m-d"));



			$this->preloadAssetsOpeningBalance();
		}
		else if($this->forOpeningTB OR $this->forBalanceSheet OR $this->for_pnl OR $this->forTrialBalance OR $this->for_doctors_pnl OR $this->for_cashflow_pnl)
		{




			// echo "<br>Date provided: " .$args["date"];die();
			$this->dateTo = new DateTime($args["date"]);
			$this->dateFrom = new DateTime($this->dateTo->format("Y-" . ($this->financialEndMonth < 10 ? "0$this->financialEndMonth":$this->financialEndMonth) . "-01"));
			$this->dateFrom->add(new DateInterval("P1M"));
			if($this->dateFrom->format("Ym") > $this->dateTo->format("Ym"))
				$this->dateFrom->sub(new DateInterval("P1Y"));
			$this->session->set_userdata('date_from_general_ledger',$this->dateFrom->format("Y-m-d"));
			$this->session->set_userdata('date_to_general_ledger',$this->dateTo->format("Y-m-d"));
			// echo "<br>Report has been set to get data from " . $this->dateFrom->format("M j Y") . " to " . $this->dateTo->format("M j Y");

			$search_title = 'Reporting Period from '.$this->dateFrom->format("M j Y").' TO '.$this->dateTo->format("M j Y").' ';

			$this->session->set_userdata('general_ledger_search_title',$search_title);
			$this->preloadData();
			$this->loadPaymentSplits();
		}
		else if($this->detailedPnl)
		{




			// echo "<br>Date provided: " .$args["date"];die();
			$this->dateTo = new DateTime($args["date"]);
			$this->dateFrom = new DateTime($this->dateTo->format("Y-" . ($this->financialEndMonth < 10 ? "0$this->financialEndMonth":$this->financialEndMonth) . "-01"));
			$this->dateFrom->add(new DateInterval("P1M"));
			if($this->dateFrom->format("Ym") > $this->dateTo->format("Ym"))
				$this->dateFrom->sub(new DateInterval("P1Y"));
			$this->session->set_userdata('date_from_general_ledger',$this->dateFrom->format("Y-m-d"));
			$this->session->set_userdata('date_to_general_ledger',$this->dateTo->format("Y-m-d"));
			// echo "<br>Report has been set to get data from " . $this->dateFrom->format("M j Y") . " to " . $this->dateTo->format("M j Y");

			$search_title = 'Reporting Period from '.$this->dateFrom->format("M j Y").' TO '.$this->dateTo->format("M j Y").' ';

			$this->session->set_userdata('general_ledger_search_title',$search_title);
			$this->preloadDataDetail();
			// $this->loadPaymentSplits();
		}










	}



	function __destruct(){
		if(is_array($this->session_account))
			unset($this->session_account);
		if(isset($this->data))
			unset($this->data);

		if(isset($this->dataDetailed))
			unset($this->dataDetailed);

		if(isset($this->dataList))
			unset($this->dataList);


		if(isset($this->dataAllDetailed))
			unset($this->dataAllDetailed);

		if(isset($this->dataAllList))
			unset($this->dataAllList);

		if(isset($this->dataAssets))
			unset($this->dataAssets);

		if(is_array($this->arrPayblesPayments))
			unset($this->arrPayblesPayments);
		if(is_array($this->arrReceivablePayment))
			unset($this->arrReceivablePayment);
	}

	function financialPeriod()
	{
		$this->db->where('branch_id',2);
		$query = $this->db->get('branch');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$end_month = $value->end_of_year_month;
				$end_day = $value->end_of_year_day;
			}
		}

		// $financial_year_date = $year."-".$end_month."-".$end_day."";

		return $end_month;
	}


	function financialStartYearPeriod()
	{
		$this->db->where('branch_id',2);
		$query = $this->db->get('branch');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$end_month = $value->end_of_year_month;
				$end_day = $value->end_of_year_day;
				$start_financial_period = $value->start_financial_period;
			}
		}

		// $financial_year_date = $year."-".$end_month."-".$end_day."";

		return $start_financial_period;
	}


	function preloadData()
	{
		$this->data = array(
							"equities" => array(),
							"assets" => array($this->accounts_receivable_id => array("name" => "Accounts Receivables",
								"amount" => 0,
								"position" => "debit",
								"subs" => array())),
							"liabilities" => array($this->accounts_payable_id => array("name" => "Accounts Payable",
								"amount" => 0,
								"position" => "credit",
								"subs" => array()),
								$this->receivables_prepayment_id => array("name" => "Receivables Prepayments",
								"amount" => 0,
								"position" => "credit",
								"subs" => array()),
								$this->providers_liability_id => array("name" => "Providers Liability",
								"amount" => 0,
								"position" => "credit",
								"subs" => array()),
								$this->loans_payable_id => array("name" => "Loans Liability",
								"amount" => 0,
								"position" => "credit",
								"subs" => array()),
								$this->payroll_liability_id => array("name" => "Payroll Liability",
								"amount" => 0,
								"position" => "credit",
								"subs" => array())),
							"incomes" => array(),
							"expenses" => array()
						  );



		$all_accounts_rs = $this->company_financial_model->get_accounts_list();
		// echo "<br>Loading accounts";
		if($all_accounts_rs->num_rows() > 0)
		{
			$this->arrAssetTypes = array("c" => array($this->accounts_receivable_id), 'f' => array());
			foreach ($all_accounts_rs->result() as $key => $value) {
				// code...
				$account_id = $value->account_id;
				$account_type_name = strtolower($value->account_type_name);
				$section = ($account_type_name == "bank" ? "assets" : ($account_type_name == "liability" ? "liabilities" : ($account_type_name == "cost of goods" ? "cost of goods" : ($account_type_name == "expense account" ? "expenses" : ($account_type_name == "asset" ? "assets" : ($account_type_name == "depreciation" ? "expenses" : ($account_type_name == "equity" ? "equities" : ($account_type_name == "income account" ? "incomes" : ($account_type_name == "liabilities" ? "liabilities" :"skip" )))))))));

				if($account_type_name == "bank")
					array_push($this->arrAssetTypes["c"], $account_id);
				else if($account_type_name == "asset")
					array_push($this->arrAssetTypes["f"], $account_id);


				$debcred = null;
				if($section == "assets" or $section == "expenses" or $section == "cost of goods" or $section == "accounts receivables")
					$debcred = "debit";
				else if($section == "equity" or $section == "incomes" or $section == "liabilities")
					$debcred = "credit";


				if($section != "skip")
					$this->data[$section][$value->account_id] = array("name" => $value->account_name,
						"amount" => 0,
						"position" => $debcred);
			}
		}

		$all_transacted_rs_data = $this->company_financial_model->get_account_transactions_ledgers_new(array("for_pnl" => $this->for_pnl,
			"forBalanceSheet" => $this->forBalanceSheet,
			"forOpeningTB" => $this->forOpeningTB,
			"forTrialBalance"=> $this->forTrialBalance,
			"for_doctors_pnl"=> $this->for_doctors_pnl,
			"forCashFlowPnl"=>$this->for_cashflow_pnl,
			"dateFrom"=>$this->dateFrom->format("Y-m-d"),
			"dateTo"=>$this->dateTo->format("Y-m-d")));

		// var_dump($all_transacted_rs_data);die();


		// echo "<br>Getting ready to load the payment splits";
		// echo "<pre>";
		// print_r($all_transacted_rs_data->result());
		// echo "</pre>";
		$profit = 0;
		$curr_profit = 0;
		if($all_transacted_rs_data->num_rows() > 0)
		{
			$show_steps = false;
			$arrSummary = array();
			foreach ($all_transacted_rs_data->result() as $key => $value_transaction) {
				// code...
				$transactionId = $value_transaction->transactionId;
				$transactionCategory = $value_transaction->transactionCategory;
				$accountsclassfication = $value_transaction->accountsclassfication;
				$amount = $value_transaction->amount;

				$accountFromId = $value_transaction->accountFromId;
				$accountId = $value_transaction->accountId;


				if($transactionCategory == 'Transfer'){

					if(array_key_exists($accountFromId, $this->data["equities"]) and array_key_exists($accountId, $this->data["assets"]))
						$this->registerTransaction($amount, "assets", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->data["equities"]) and array_key_exists($accountFromId, $this->data["assets"]))
						$this->registerTransaction($amount, "equities", $accountId, "assets", $accountFromId,0,0,$value_transaction);
					else if(array_key_exists($accountFromId, $this->data["assets"]) and array_key_exists($accountId, $this->data["assets"]))
						$this->registerTransaction($amount, "assets", $accountId, "assets", $accountFromId,0,0,$value_transaction);
					else
						$this->registerTransaction($amount, "assets", $accountId, "assets", $accountFromId,0,0,$value_transaction);

				}
				else if($transactionCategory == 'Petty Cash'){

					if(array_key_exists($accountFromId, $this->data["assets"]) and array_key_exists($accountId, $this->data["assets"]))

						$this->registerTransaction($amount, "assets", $accountId, "assets", $accountFromId,0,0,$value_transaction);
					else{
						$this->registerTransaction($amount, "expenses", $accountId, "assets", $accountFromId,0,0,$value_transaction);
						$profit -= $amount;
					}
				}

				else if($transactionCategory == 'Creditor Invoices'){

					if(array_key_exists($accountId, $this->data["assets"])) // when the creditor invoices the company an asset
						$this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->accounts_payable_id,0,1,$value_transaction);
					else
					{
						if(array_key_exists($accountId, $this->data["expenses"])) // when the creditor invoices the company an asset
						{
							$this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
							$profit -= $amount;
						}
						else if(array_key_exists($accountId, $this->data["assets"])) // when the creditor invoices the company an asset
						{
							$this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
						}
						else if ($show_steps and $accountId == 80)
							echo "<br>Asset invoice not captured [$accountId]";
						
					}
				}
				else if($transactionCategory == 'Provider Invoices'){

					if(array_key_exists($accountId, $this->data["assets"])) // when the creditor invoices the company an asset
						$this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->providers_liability_id,0,1,$value_transaction);
					else
					{
						$this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->providers_liability_id,0,0,$value_transaction);
						$profit -= $amount;
					}
				}
				else if($transactionCategory == 'Payroll Invoice')
				{
					
						$profit -= $amount;
						$this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);

					
					
				}
				else if($transactionCategory == 'Payroll Deductions')
				{
					
						$this->registerTransaction($amount, "liabilities", $this->other_payable_id, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);
					
					
				}
				else if($transactionCategory == 'Inventory Deductions')
				{	

						// $profit -= $amount;
						$this->registerTransaction($amount, "expenses", $accountId, "expenses", $accountFromId,0,0,$value_transaction);
					
					
				}
				else if($transactionCategory == 'Creditor Invoices Payments'){
					// $this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
					// array_push($this->arrPayblesPayments,$transactionId);

					if(array_key_exists($accountId, $this->data["assets"])) // when the creditor invoices the company an asset
						$this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->data["equities"])) // when the creditor invoices the company an equity
						$this->registerTransaction($amount, "equities", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);

					array_push($this->arrPayblesPayments,$transactionId);
				}
				else if($transactionCategory == 'Provider Invoices Payments'){

					if(array_key_exists($accountId, $this->data["assets"])) // when the provider invoices the company an asset
						$this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->providers_liability_id,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->data["equities"])) // when the provider invoices the company an asset
						$this->registerTransaction($amount, "equities", $accountId, "liabilities", $this->providers_liability_id,0,1,$value_transaction);
					// array_push($this->arrPayblesPayments,$transactionId);
				}

				else if($transactionCategory == 'Creditor Opening Balance'){
					//$this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->accounts_payable_id);
					// $this->registerTransaction($amount, "liabilities", $this->accounts_payable_id, "none", 0,0,0,$value_transaction);
					$this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
					$profit -= $amount;
				}
				else if($transactionCategory == 'Provider Opening Balance'){
					// $this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->providers_liability_id);
					// $this->registerTransaction($amount, "liabilities", $this->providers_liability_id, "none", 0,0,0,$value_transaction);

					$this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->providers_liability_id,0,0,$value_transaction);
					$profit -= $amount;

				}


				
				else if($transactionCategory == 'Account Opening Balance')
					if(array_key_exists($accountId, $this->data["assets"]) AND $accountFromId == 0) // when the creditor invoices the company an asset
						$this->registerTransaction($amount, "assets", $accountId, "none", 0,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->data["equities"])  AND $accountFromId == 0) // when the creditor invoices the company an asset
						$this->registerTransaction($amount, "equities", $accountId, "none", 0,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->data["liabilities"]) AND $accountFromId == 0) // when the creditor invoices the company an asset
						$this->registerTransaction($amount, "liabilities", $accountId, "none", 0,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->data["assets"]) AND $accountFromId == $this->retained_earnings_id){

						$this->registerTransaction($amount, "assets", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					}
					else
						echo "<br>Account misplaced $accountId";

				else if($transactionCategory == 'Payroll Opening Balance')
					// $this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->providers_liability_id);
					$this->registerTransaction($amount, "liabilities", $this->payroll_liability_id, "none", 0,0,0,$value_transaction);

				else if($transactionCategory == 'Creditor Payment'){
					if(array_key_exists($accountId, $this->data["equities"]))
						$this->registerTransaction($amount, "equities", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->data["assets"]))
						$this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
					else
						echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";

					array_push($this->arrPayblesPayments,$transactionId);
				}
				else if($transactionCategory == 'Receivable Invoice'){
					$profit += $amount;
					$this->registerTransaction($amount,"incomes", $accountId, "assets", $this->accounts_receivable_id,0,0,$value_transaction);
				}
				else if($transactionCategory == 'Receivable Payment'){
					$this->registerTransaction($amount,"assets", $accountId, "assets", $this->accounts_receivable_id,0,0,$value_transaction);
					array_push($this->arrReceivablePayment,$transactionId);
				}
				else if($transactionCategory == 'Receivable Refund'){
					// $profit += $amount;
					$this->registerTransaction($amount, "assets", $this->accounts_receivable_id,"assets", $accountId,0,0,$value_transaction);
					array_push($this->arrReceivablePayment,$transactionId);
				}
				else if($transactionCategory == 'Receivable Credit Note')
				{
					$profit -= $amount;
					$this->registerTransaction($amount,"incomes", $accountId, "assets", $this->accounts_receivable_id,0,1,$value_transaction);
				}
				else if($transactionCategory == 'Payroll Payment'){
					// $this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);
					// array_push($this->arrPayblesPayments,$transactionId);

					if(array_key_exists($accountId, $this->data["assets"])) // when the creditor invoices the company an asset
						$this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);
					else  if(array_key_exists($accountId, $this->data["equities"])) // when the creditor invoices the company an asset
						$this->registerTransaction($amount, "equities", $accountId, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);
					else
						echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
						
					array_push($this->arrPayblesPayments,$transactionId);
				}
				else if($transactionCategory == 'Payroll Deductions Payment'){
					$this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->other_payable_id,0,0,$value_transaction);
					array_push($this->arrPayblesPayments,$transactionId);
				}
				else if($transactionCategory == 'Creditor Credit Note')
				{
					$profit += $amount;
					$this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->accounts_payable_id,0,1,$value_transaction);
				}
				// else if($transactionCategory == 'Payroll Credit Note')
				// {
					// $profit += $amount;
					// $this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->payroll_liability_id,0,1,$value_transaction);
				// }
				else if($transactionCategory == 'Provider Credit Note')
				{
					$profit += $amount;
					$this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->providers_liability_id,0,1,$value_transaction);
				}
				else if($transactionCategory == 'Prepayments'){
					$this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->receivables_prepayment_id,0,0,$value_transaction);
				}
				else if($transactionCategory == 'Journal Credit'){
				
					// echo "<pre>";
					// print_r($value_transaction);
					// echo "</pre>";
					if(array_key_exists($accountFromId, $this->data["assets"]) and array_key_exists($accountId, $this->data["expenses"])){
						// var_dump($this->other_payable_id);die();
						// record the invoice expense inc
						// $this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->other_payable_id,0,0,$value_transaction);
						// $profit -= $amount;
						// pay for the expense from the bank
						// $this->registerTransaction($amount, "assets", $accountFromId, "liabilities", $this->other_payable_id,0,0,$value_transaction);


						if(array_key_exists($accountFromId, $this->data["assets"]) and array_key_exists($accountId, $this->data["assets"]))
							$this->registerTransaction($amount, "assets", $accountId, "assets", $accountFromId,0,0,$value_transaction);

						else if(array_key_exists($accountId, $this->data["expenses"]) and array_key_exists($accountFromId, $this->data["assets"])){
							$this->registerTransaction($amount, "expenses", $accountId, "assets", $accountFromId,0,0,$value_transaction);
							$profit -= $amount;
						}
						else
							echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
					}
					



					// if(array_key_exists($accountFromId, $this->data["equities"]) and array_key_exists($accountId, $this->data["assets"]))
					// 	$this->registerTransaction($amount, "assets", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					else if(array_key_exists($accountFromId, $this->data["equities"]) and array_key_exists($accountId, $this->data["assets"]))
					{
						$this->registerTransaction($amount, "equities", $accountFromId, "assets", $accountId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountFromId, $this->data["expenses"]) and array_key_exists($accountId, $this->data["assets"])){
						$this->registerTransaction($amount, "expenses", $accountFromId, "assets", $accountId,0,0,$value_transaction);
						$profit -= $amount;
					}
					else if(array_key_exists($accountFromId, $this->data["expenses"]) and array_key_exists($accountId, $this->data["incomes"])){
						$profit -= $amount;
						$this->registerTransaction($amount, "expenses", $accountFromId, "incomes", $accountId,0,1,$value_transaction);
						
					}
					else if(array_key_exists($accountFromId, $this->data["assets"]) and array_key_exists($accountId, $this->data["incomes"])){
						$this->registerTransaction($amount, "assets", $accountFromId, "incomes", $accountId,0,1,$value_transaction);
						$profit += $amount;
					}
					else if(array_key_exists($accountFromId, $this->data["assets"]) and array_key_exists($accountId, $this->data["assets"]))
					{

						$this->registerTransaction($amount, "assets", $accountFromId, "assets", $accountId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountFromId, $this->data["assets"]) and array_key_exists($accountId, $this->data["liabilities"]))
					{
					
						$this->registerTransaction($amount, "assets", $accountFromId, "liabilities", $accountId,0,1,$value_transaction);
					}

					else if(array_key_exists($accountId, $this->data["equities"]) and array_key_exists($accountFromId, $this->data["assets"])) 
					{
						$this->registerTransaction($amount, "assets", $accountFromId, "equities", $accountId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountId, $this->data["equities"]) and array_key_exists($accountFromId, $this->data["expenses"])) 
					{
						$this->registerTransaction($amount, "expenses", $accountFromId, "equities", $accountId,0,0,$value_transaction);
						$profit -= $amount;
					}
					else if(array_key_exists($accountId, $this->data["equities"]) and array_key_exists($accountFromId, $this->data["equities"])) 
					{
						$this->registerTransaction($amount, "equities", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					}

					else if(array_key_exists($accountFromId, $this->data["incomes"]) and array_key_exists($accountId, $this->data["assets"])){
						$profit += $amount;
						$this->registerTransaction($amount, "incomes", $accountFromId, "assets", $accountId,0,1,$value_transaction);
					
					}
					// else if(array_key_exists($accountId, $this->data["equities"]) and array_key_exists($accountFromId, $this->data["assets"])) 
					// {
					// 	$this->registerTransaction($amount, "equities", $accountFromId, "equities", $accountId,0,0,$value_transaction);
					// }
					else if(array_key_exists($accountFromId, $this->data["liabilities"])){

						//***** Mar 23, 2023: When moving values from one payable to another. ...e.g. when ...
						if($accountsclassfication == 1) // ** add the creditors journals
							$this->registerTransaction($amount, "liabilities", $accountFromId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
						else if($accountsclassfication == 2) // ** add the providers journals
							$this->registerTransaction($amount, "liabilities", $accountFromId, "liabilities", $this->providers_liability_id,0,0,$value_transaction);
						else if($accountsclassfication == 3) // ** add the payroll journals
							$this->registerTransaction($amount, "liabilities", $accountFromId, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);
						//***** May 1, 2023: When moving values from one payable from an asset. ...e.g. when ... when paying off a particular liability
						else if($accountsclassfication == 5) // ** add the providers liabilities
							$this->registerTransaction($amount, "liabilities", $accountFromId, "assets", $accountId,0,0,$value_transaction);
						else if(array_key_exists($accountId, $this->data["assets"]) AND array_key_exists($accountFromId, $this->data["liabilities"])){
							//***** April 30, 2023: When moving values from one payable to an asset. ...e.g. when ... recovering money from a staff salary
						
							if($accountsclassfication != 3) // ** add the payroll journals
								$this->registerTransaction($amount, "liabilities", $accountFromId, "assets", $accountId,0,0,$value_transaction);
							else
								echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
						}
						else
							echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";

					}
					else if(array_key_exists($accountFromId, $this->data["assets"]) AND array_key_exists($accountId, $this->data["liabilities"])){
						//***** April 30, 2023: When moving values from one payable to an asset. ...e.g. when ... recovering money from a staff salary
						if($accountsclassfication == 3) // ** add the payroll journals
							$this->registerTransaction($amount, "assets", $accountFromId, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);
						else
							echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
					}
					else if(array_key_exists($accountFromId, $this->data["equities"]) AND array_key_exists($accountId, $this->data["expenses"])){
							
						$profit += $amount;
						$this->registerTransaction($amount, "equities", $accountFromId, "expenses", $accountId,0,0,$value_transaction);
						
						
					}
					else if(array_key_exists($accountId, $this->data["assets"]) AND array_key_exists($accountFromId, $this->data["liabilities"])){

						echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
						
					}
					else
						echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";

						
				}
				else if($transactionCategory == "Patients Journals")
				{
					$profit += $amount;
					$this->registerTransaction($amount, "expenses", $accountFromId,"assets", $this->accounts_receivable_id,0,1,$value_transaction);
				}
				else if($transactionCategory == "Direct Payments")
				{
					// $profit += $amount;
					if(array_key_exists($accountId, $this->data["assets"]) and array_key_exists($accountFromId, $this->data["equities"])) 
					{
						$this->registerTransaction($amount, "assets", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountId, $this->data["expenses"]) and array_key_exists($accountFromId, $this->data["equities"])) 
					{
						$profit -= $amount;
						$this->registerTransaction($amount, "expenses", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountId, $this->data["expenses"]) and array_key_exists($accountFromId, $this->data["assets"])) 
					{
					
						$profit -= $amount;
						$this->registerTransaction($amount, "expenses", $accountId, "assets", $accountFromId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountId, $this->data["assets"]) and array_key_exists($accountFromId, $this->data["assets"])) 
					{
						$this->registerTransaction($amount, "assets", $accountId, "assets", $accountFromId,0,0,$value_transaction);
					}
					
					else
						echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
				}

				// else if($transactionCategory == "Balances Brought Forward"){
				// 	// echo "<br>Adding Bal. B/f: $accountId";
				// 	if(array_key_exists($accountId, $this->data["assets"]))
				// 		$this->registerTransaction($amount, "assets", $accountId, "none", 0);
				// 	else if(array_key_exists($accountId, $this->data["equities"]))
				// 		$this->registerTransaction($amount, "equities", $accountId, "none", 0);
				// }

				else if($transactionCategory == 'Loans Expense')
				{
					// $profit -= $amount;
					$this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->loans_payable_id,0,1,$value_transaction);
				}
				else if($transactionCategory == 'Loan Opening Balance'){
					// $this->registerTransaction($amount, "expenses", $accountId, "liabilities", $this->providers_liability_id);
					$this->registerTransaction($amount, "liabilities", $this->loans_payable_id, "equities", $this->retained_earnings_id,0,0,$value_transaction);

				}

				else if($transactionCategory == 'Loan Payment'){


					if(array_key_exists($accountId, $this->data["assets"])) {
						// var_dump($this->loans_payable_id);die();
						$this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->loans_payable_id,0,0,$value_transaction);
						// $profit += $amount;
					}
					else if(array_key_exists($accountId, $this->data["equities"])) {
						// var_dump($this->loans_payable_id);die();
						$this->registerTransaction($amount, "assets", $accountId, "liabilities", $this->loans_payable_id,0,0,$value_transaction);
						// $profit += $amount;
					}
					else {
						// var_dump($accountId);die();
						echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
					}
				}else if($transactionCategory == 'Loan Interest Payment'){
						$this->registerTransaction($amount, "expenses", $accountId, "assets", $accountFromId,0,0,$value_transaction);
						$profit -= $amount;
			
				}else if($transactionCategory == 'Bank Charges Expense'){
						$this->registerTransaction($amount, "expenses", $accountId, "assets", $accountFromId,0,0,$value_transaction);
						$profit -= $amount;
			
				}else if($transactionCategory == 'Bank Interest Earned'){
						$this->registerTransaction($amount, "assets", $accountFromId, "incomes", $accountId,0,1,$value_transaction);
						$profit += $amount;
			
				}else
					echo "<bR>  accounts: $accountId : $transactionCategory";


				if($show_steps and $curr_profit <> $profit){
					//echo "<br>Changed at $transactionCategory [$profit]: " . ($profit - $curr_profit);

					$account_name = $this->company_financial_model->get_account_name($accountId);
					$curr_profit = $profit;
					if(!array_key_exists($transactionCategory, $arrSummary))
						$arrSummary[$transactionCategory] = array("t" => 0, "s" => array());
					if(!array_key_exists($accountId, $arrSummary[$transactionCategory]["s"]))
						$arrSummary[$transactionCategory]["s"][$accountId] = array("n" => $account_name, "s" => 0);
					$arrSummary[$transactionCategory]["t"] += $amount;
					$arrSummary[$transactionCategory]["s"][$accountId]["s"] += $amount;

					if($accountId == 80){
						echo "<br>Sth: ".$value_transaction->referenceTable." Invoice id ".$value_transaction->referenceId;
					}
				}
			}

			if($show_steps){
				echo "<pre>";
				print_r($arrSummary);
				echo "</pre>";
				unset($arrSummary);
			}

		}

		//Some house-keeping
		$arrReserverd = array($this->accounts_payable_id,$this->accounts_receivable_id,$this->providers_liability_id,$this->payroll_liability_id,$this->retained_earnings_id);
		foreach($this->data as $section => $data)
			foreach($data as $key => $values)
				if(!in_array($key, $arrReserverd))
					if($values["amount"] == 0)
						unset($this->data[$section][$key]);
		unset($arrReserverd);
		$this->profit = $profit;
		// var_dump($profit);die();
		//Check if to update next year's opening balances
		if($this->dateTo->format("m") == 12){
			// error_reporting(E_ALL);
			$next_year = $this->dateFrom->format("Y")+1;
			$date_tomorrow = $next_year."-01-01";
			$arrOk = array("assets", "liabilities", "equities");

			$sql = "UPDATE account_opening_balance
					SET status = 0
					WHERE opening_balance_date = '$date_tomorrow'";
						// echo "<br>Next update: $sql";
						$this->db->query($sql);

			foreach($this->data as $section => $section_data)
			{
				if(in_array($section, $arrOk))
					foreach($section_data as $account_id => $part){
						$balance = $part["amount"];
						// echo "<br>Checking $section >> " . strtolower(trim($part["name"])); 
						if($section == "equities" and strtolower(trim($part["name"])) == "retained earnings"){
							// echo "<br>ADding the profit of KES. " . number_format($this->profit)." ".$part["name"];
							$balance += $this->profit;
						}

						$sql = "insert ignore into account_opening_balance (account_id,opening_balance_date, balance)
						values (".$account_id.",'".$date_tomorrow."',".$balance.")
						on duplicate key update balance = ".$balance.",status = 1";
						// echo "<br>Next update: $sql";
						$this->db->query($sql);

					}
				

			}

				unset($arrOk);
		}

		// var_dump($this->profit);die();

	}




	function loadPaymentSplits(){
		// echo "<br>Getting ready to load the payment splits";
		// echo "<pre>";
		// print_r($this->arrPayblesPayments);
		// echo "</pre>";


		$sql = $this->company_financial_model->get_payments_split();
		//loop through sql and call registerTransaction(..)
	}
	function registerTransaction($amount, $incSection, $incId, $decSection, $decId, $subacc = 0,$credits=0,$transaction_detail){

		// $this->registerTransactionDetail($amount, $incSection, $incId, $decSection, $decId, $subacc,$credits,$transaction_detail);


		$transactionId = $transaction_detail->transactionId;
		$transactionCategory = $transaction_detail->transactionCategory;
		$accountsclassfication = $transaction_detail->accountsclassfication;
		$transactionDate = $transaction_detail->transactionDate;
		$transactionDescription = $transaction_detail->transactionDescription;
		$amount = $transaction_detail->amount;

		$accountFromId = $transaction_detail->accountFromId;
		$accountId = $transaction_detail->accountId;
		$month = $transaction_detail->month_start;
		$year = $transaction_detail->year_start;
		$referenceCode = $transaction_detail->referenceCode;
		$transactionName = $transaction_detail->transactionName;
		$link = $transaction_detail->link;
		$referenceId = $transaction_detail->referenceId;

		$count = $count + 1;
		$date = $transactionDate;
		$patient = "";
		$serial = $referenceCode.'*'.$transactionId.'-'.$incId;

		// var_dump($amount);die();
		if($transactionCategory == "Receivable Payment")
		{
			$this->data['Patients Payments'][$incId]["amount"] += $amount;
		}


		$debcred = null;
		if($incSection == "assets" or $incSection == "expenses" or $incSection == "accounts receivables")
			$debcred = "debit";
		else if($incSection == "equity" or $incSection == "incomes" or $incSection == "liabilities")
			$debcred = "credit";


			$toDecrease = (($incSection == "assets" and $decSection == "liabilities" and $decId == $this->accounts_payable_id)
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->payroll_liability_id )
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->loans_payable_id)
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->providers_liability_id)
						OR ($incSection == "assets" and $decSection == "assets" and $incId == $this->accounts_receivable_id)
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->other_payable_id)
						OR ($incSection == "liabilities" and $decSection == "liabilities" and $decId == $this->payroll_liability_id)
						OR ($incSection == "equities" and $decSection == "assets")
						OR ($incSection == "assets" and $decSection == "incomes")

						OR ($incSection == "assets" and $decSection == "liabilities" AND $credits === 1)
						OR ($incSection == "expenses" and $decSection == "incomes" AND $credits === 1) // income paying off an expense
						OR ($incSection == "liabilities" and $decSection == "assets" )
						OR ($incSection == "equities" and $decSection == "expenses")

					);
		if($incSection != "none")
		{
			if(array_key_exists($incId, $this->data[$incSection]))
				if($credits){
					$this->data[$incSection][$incId]["amount"] += ($amount * ($toDecrease ? 1:-1));
					$serial = (strlen($serial) == 0 ? "serial" . count($this->dataAllList[$incSection][$incId]) : $serial);



					if($transactionCategory == "Creditor Invoices" and $incSection == 'assets')
					{
						$toDecrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Receivable Credit Note")
					{
						$toDecrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Loans Expense" and $incSection == 'assets')
					{
						$toDecrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Bank Interest Earned")
					{
						$toDecrease = false;
						$debcred = "debit";
					}

					else if($transactionCategory == "Journal Credit")
					{
						$toDecrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Journal Credit" and $incSection == 'expenses')
					{
						$toDecrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Journal Credit"  AND $incSection == 'incomes'){
						$toDecrease = true;
						$debcred = "debit";
					}
					

					$this->dataAllList[$incSection][$incId][$serial] = array("patient" => "xyz",
						"desc" => $transactionName,
						"cat" => $transactionCategory,
						"date" => $transactionDate,
						"amount" => ($amount * ($toDecrease ? 1:-1)),
						"inc" => $incSection,
						"dec" => $decSection,
						"added_at" => "P4",
						"debcred"=>$debcred,
						"link"=>$link,
						"referenceId"=>$referenceId);
				}
				else
				{

					$this->data[$incSection][$incId]["amount"] += ($amount * ($toDecrease ? -1:1));
					$serial = (strlen($serial) == 0 ? "serial" . count($this->dataAllList[$incSection][$incId]) : $serial);

					if($transactionCategory == "Provider Invoices Payments" OR $transactionCategory == "Creditor Payment" OR $transactionCategory == "Payroll Payment"){
						$toDecrease = ($decSection != "liabilities");
						$debcred = "credit";
					}
					// else if($transactionCategory == "Receivable Invoice")
					// {
					// 	$toDecrease = false;
					// 	$debcred = "debit";
					// }
					else if($transactionCategory == "Receivable Refund"){
						$toDecrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Creditor Invoices")
					{
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Provider Invoices")
					{
						$toDecrease = false;
						$debcred = "debit";
					}
					

					else if($transactionCategory == "Payroll Invoice"){
						$toDecrease = false;
						$debcred = "debit";
					}
					
					else if($transactionCategory == "Journal Credit" and $accountsclassfication >= 3){
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Transfer" AND $incSection == "equities"){
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Account Opening Balance" and ($incSection == "equities" or $incSection == "equity" or $incSection == "incomes" or $incSection == "liabilities")){
						$toDecrease = false;
						$debcred = "credit";
					}
					else if ($transactionCategory == "Account Opening Balance" and ($incSection == "assets" or $incSection == "expenses" or $incSection == "accounts receivables"))
					{
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Patients Journals"){
						$toDecrease = false;
						$debcred = "credit";
					}
					
					else if($transactionCategory == "Journal Credit" AND $incSection == 'equities' AND  $decSection == 'assets'){
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Journal Credit" AND $incSection == 'equities' AND $decSection == 'equities'){
						$toDecrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Direct Payments" AND ($decSection == 'equities' OR $decSection == 'assets')){
						$toDecrease = false;
						$debcred = "debit";
					}
					
					
					

					


					$this->dataAllList[$incSection][$incId][$serial] = array("patient" => "xyz",
						"desc" => $transactionName,
						"cat" => $transactionCategory,
						"date" => $transactionDate,
						"amount" => ($amount * ($toDecrease ? -1:1)),
						"inc" => $incSection,
						"dec" => $decSection,
						"added_at" => "P3",
						"debcred" => $debcred,
						"link"=>$link,
						"referenceId"=>$referenceId);
				}

				// $this->data[$incSection][$incId]["amount"] += ($amount * (($incSection == "assets" and $decSection == "liabilities" and $decId == $this->accounts_payable_id)?-1:1));
			else
				echo "<br>Inc Account not found: $incSection > $incId Cat: $transactionCategory Serial: $serial amounts: $amount $transactionName";
		}



		$toIncrease = (($incSection == "expenses" and $decId == $this->accounts_payable_id) // Accounts Payable Invoices
						or ($incSection == "incomes" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
						or ($incSection == "incomes" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
						or ($incSection == "assets" and $decId == $this->receivables_prepayment_id ) // Batch prepayments Invoices
						// or ($incSection == "assets" and $decSection == "equities") //  Shareholder increasing Capital
						or ($incSection == "expenses" and $decId == $this->payroll_liability_id ) // Payroll Invoices
						or ($incSection == "expenses" and $decId == $this->providers_liability_id ) // Payroll Invoices
						// or ($incSection == "expenses" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
						or ($incSection == "assets" and $incId == $this->accounts_receivable_id)
						or ($incSection == "expenses" and $decId == $this->other_payable_id) // Accounts Payable Invoices
						// or ($incSection == "assets" and $decId == $this->payroll_liability_id ) // Payroll Invoices
						// OR ($incSection == "expenses" and $decSection == "expenses" and $transactionCategory == 'Inventory Deductions')
						or ($incSection == "liabilities" and $incId == $this->other_payable_id) // Accounts Payable Invoices
						OR ($incSection == "assets" and $decSection == "equities")
						OR ($incSection == "expenses" and $decSection == "equities")
						OR ($incSection == "assets" and $decSection == "equities" AND $decId == $this->retained_earnings_id)
						OR ($incSection == "incomes" and $decSection == "assets")
						OR ($incSection == "expenses" and $decSection == "equities")
						// OR ($incSection == "expenses" and $decSection == "incomes" AND $credits === 1)
						// OR ($incSection == "expenses" and $decSection == "assets"  AND $transactionCategory === "Journal Credit")
						// OR ($incSection == "assets" and $decSection == "incomes")
						// OR ($incSection == "expenses" and $decSection == "assets")
						// or ($incSection == "assets" and $decId == $this->accounts_payable_id)



						 );


		if($decSection != "none")
		{
			if(array_key_exists($decId, $this->data[$decSection])){
				if($credits){
					$this->data[$decSection][$decId]["amount"] += ($amount * ($toIncrease ? -1:1));
					$serial = (strlen($serial) == 0 ? "serial" . count($this->dataAllList[$decSection][$decId]) : $serial);


					if($transactionCategory == "Payroll Deductions"){
						$toIncrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Patients Journals" and $incSection == 'assets'){
						$toIncrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Patients Journals" and $incSection == 'expenses'){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Receivable Credit Note")
					{
						$toIncrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Creditor Credit Note")
					{
						$toIncrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Creditor Invoices" and $incSection == 'assets')
					{
						$toDecrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Loans Expense" and $incSection == 'assets')
					{
						$toDecrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Bank Interest Earned")
					{
						$toDecrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Journal Credit" and $incSection == 'expenses')
					{
						$toIncrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Journal Credit" AND $incSection == 'incomes'){
						$toIncrease = false;
						$debcred = "credit";
					}


					$this->dataAllList[$decSection][$decId][$serial] = array("patient" => "xyz",
						"desc" => $transactionName,
						"cat" => $transactionCategory,
						"date" => $transactionDate,
						"amount" => ($amount * ($toIncrease ? -1:1)),
						"inc" => $incSection,
						"dec" => $decSection,
						"added_at" => "P2",
						"debcred"=>$debcred,
						"link"=>$link,
						"referenceId"=>$referenceId);
				}
				else{
					$this->data[$decSection][$decId]["amount"] += ($amount * ($toIncrease ? 1:-1));
					$serial = (strlen($serial) == 0 ? "serial" . count($this->dataAllList[$decSection][$decId]) : $serial);

					//To cater for bank transfers, the relieving bank will have the transaction as a credit
					if($transactionCategory == "Transfer"){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Provider Invoices Payments" OR $transactionCategory == "Creditor Payment" OR $transactionCategory == "Payroll Payment"){
						$toIncrease = ($decSection == "liabilities");
						$debcred = "debit";
					}
					else if($transactionCategory == "Prepayments"){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Petty Cash"){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Receivable Refund"){
						$toIncrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Receivable Payment")
					{
						$toIncrease = true;
						$debcred = "credit";
					}

					else if($transactionCategory == "Receivable Invoice")
					{
						$toIncrease = true;
						$debcred = "debit";
					}

					else if($transactionCategory == "Payroll Deductions")
					{
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Payroll Deductions Payment")
					{
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Inventory Deductions")
					{
						$toIncrease = true;
						$debcred = "credit";
					}
					
					else if($transactionCategory == "Journal Credit" and $accountsclassfication > 0 AND $accountsclassfication <= 3){
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Journal Credit" AND $incSection == 'expenses'){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Journal Credit" AND $incSection == 'equities' AND $decSection != 'equities'){
						$toIncrease = true;
						$debcred = "credit";
					}
					
					else if($transactionCategory == "Journal Credit" AND $incSection == 'assets'){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Creditor Invoices")
					{
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Provider Invoices")
					{
						$toIncrease = true;
						$debcred = "credit";
					}
					

					else if($transactionCategory == "Payroll Invoice"){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Loan Payment")
					{
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Bank Charges Expense")
					{
						$toIncrease = true;
						$debcred = "credit";
					}
					
					else if($transactionCategory == "Journal Credit" AND $incSection == 'equities' AND $decSection == 'equities'){
						$toIncrease = true;
						$debcred = "debit";
					}

					else if($transactionCategory == "Direct Payments" AND ($decSection == 'equities' OR $decSection == 'assets')){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Loan Opening Balance"){
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Account Opening Balance"){
						$toIncrease = true;
						$debcred = "credit";
					}



					
					
					

					

					$this->dataAllList[$decSection][$decId][$serial]= array("patient" => "xyz",
						"desc" => $transactionName,
						"cat" => $transactionCategory,
						"date" => $transactionDate,
						"amount" => ($amount * ($toIncrease ? 1:-1)),
						"inc" => $incSection,
						"dec" => $decSection,
						"added_at" => "P1",
						"debcred"=>$debcred,
						"link"=>$link,
						"referenceId"=>$referenceId);
				}


			}else
				echo "<br>Dec Account not found: $decSection > $decId - $amount - Cat: $transactionCategory--- Inc Account Found $incSection > $incId Cat: $transactionCategory Serial: $serial";
		}
		// echo "<br>Incoming - $amount - $incSection - $incId [".$this->data[$incSection][$incId]["name"]."] - $decSection - $decId ($this->accounts_payable_id): " . $this->data[$decSection][$this->accounts_payable_id]["amount"];

		if($incSection == "expenses" and $decSection == "liabilities" and $decId == $this->accounts_payable_id){
			if(!array_key_exists($incId, $this->data[$decSection][$decId]["subs"]))
				$this->data[$decSection][$decId]["subs"][$incId] = array("name" => $this->data["expenses"][$incId]["name"],
					"amount" => 0);

			$this->data[$decSection][$decId]["subs"][$incId]["amount"] += $amount;
		}
		else if($incSection == "assets" and $decSection == "liabilities" and $decId == $this->accounts_payable_id){
			if(!array_key_exists($subacc, $this->data[$decSection][$decId]["subs"]))
				$this->data[$decSection][$decId]["subs"][$subacc] = array("name" => $this->data["expenses"][$subacc]["name"],
					"amount" => 0);

			$this->data[$decSection][$decId]["subs"][$subacc]["amount"] -= $amount;
		}
		else if($incSection == "incomes" and $decSection == "assets" and $decId == $this->accounts_receivable_id){
			if(!array_key_exists($incId, $this->data[$decSection][$decId]["subs"]))
				$this->data[$decSection][$decId]["subs"][$incId] = array("name" => $this->data["incomes"][$incId]["name"],
					"amount" => 0);

			$this->data[$decSection][$decId]["subs"][$incId]["amount"] += $amount;
		}
		else if($incSection == "subacc" and $decSection == "assets" and $decId == $this->accounts_receivable_id){
			if(!array_key_exists($incId, $this->data[$decSection][$decId]["subs"]))
				$this->data[$decSection][$decId]["subs"][$subacc] = array("name" => $this->data["incomes"][$incId]["name"],
					"amount" => 0);

			$this->data[$decSection][$decId]["subs"][$subacc]["amount"] -= $amount;
		}







	}
	function registerTransaction_old_values($amount, $incSection, $incId, $decSection, $decId, $subacc = 0,$credits=0,$transaction_detail){

		// $this->registerTransactionDetail($amount, $incSection, $incId, $decSection, $decId, $subacc,$credits,$transaction_detail);


		$transactionId = $transaction_detail->transactionId;
		$transactionCategory = $transaction_detail->transactionCategory;
		$accountsclassfication = $transaction_detail->accountsclassfication;
		$transactionDate = $transaction_detail->transactionDate;
		$transactionDescription = $transaction_detail->transactionDescription;
		$amount = $transaction_detail->amount;

		$accountFromId = $transaction_detail->accountFromId;
		$accountId = $transaction_detail->accountId;
		$month = $transaction_detail->month_start;
		$year = $transaction_detail->year_start;
		$referenceCode = $transaction_detail->referenceCode;
		$transactionName = $transaction_detail->transactionName;
		$link = $transaction_detail->link;
		$referenceId = $transaction_detail->referenceId;

		$count = $count + 1;
		$date = $transactionDate;
		$patient = "";
		$serial = $referenceCode.'*'.$transactionId.'-'.$incId;

		// var_dump($amount);die();
		if($transactionCategory == "Receivable Payment")
		{
			$this->data['Patients Payments'][$incId]["amount"] += $amount;
		}


		$debcred = null;
		if($incSection == "assets" or $incSection == "expenses" or $incSection == "accounts receivables")
			$debcred = "debit";
		else if($incSection == "equity" or $incSection == "incomes" or $incSection == "liabilities")
			$debcred = "credit";


			$toDecrease = (($incSection == "assets" and $decSection == "liabilities" and $decId == $this->accounts_payable_id)
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->payroll_liability_id )
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->loans_payable_id)
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->providers_liability_id)
						OR ($incSection == "assets" and $decSection == "assets" and $incId == $this->accounts_receivable_id)
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->other_payable_id)
						OR ($incSection == "liabilities" and $decSection == "liabilities" and $decId == $this->payroll_liability_id)
						OR ($incSection == "equities" and $decSection == "assets")
						// OR ($incSection == "assets" and $decSection == "expenses")
						// OR ($incSection == "equities" and $decSection == "assets")
						OR ($incSection == "liabilities" and $decSection == "assets" ));
		if($incSection != "none")
		{
			if(array_key_exists($incId, $this->data[$incSection]))
				if($credits){
					$this->data[$incSection][$incId]["amount"] += ($amount * ($toDecrease ? 1:-1));
					$serial = (strlen($serial) == 0 ? "serial" . count($this->dataAllList[$incSection][$incId]) : $serial);



					if($transactionCategory == "Creditor Invoices" and $incSection == 'assets')
					{
						$toDecrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Receivable Credit Note")
					{
						$toDecrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Loans Expense" and $incSection == 'assets')
					{
						$toDecrease = true;
						$debcred = "debit";
					}


					$this->dataAllList[$incSection][$incId][$serial] = array("patient" => "xyz",
						"desc" => $transactionName,
						"cat" => $transactionCategory,
						"date" => $transactionDate,
						"amount" => ($amount * ($toDecrease ? 1:-1)),
						"inc" => $incSection,
						"dec" => $decSection,
						"added_at" => "P4",
						"debcred"=>$debcred,
						"link"=>$link,
						"referenceId"=>$referenceId);
				}
				else
				{

					$this->data[$incSection][$incId]["amount"] += ($amount * ($toDecrease ? -1:1));
					$serial = (strlen($serial) == 0 ? "serial" . count($this->dataAllList[$incSection][$incId]) : $serial);

					if($transactionCategory == "Provider Invoices Payments" OR $transactionCategory == "Creditor Payment" OR $transactionCategory == "Payroll Payment"){
						$toDecrease = ($decSection != "liabilities");
						$debcred = "credit";
					}
					// else if($transactionCategory == "Receivable Invoice")
					// {
					// 	$toDecrease = false;
					// 	$debcred = "debit";
					// }
					else if($transactionCategory == "Receivable Refund"){
						$toDecrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Creditor Invoices")
					{
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Provider Invoices")
					{
						$toDecrease = false;
						$debcred = "debit";
					}
					

					else if($transactionCategory == "Payroll Invoice"){
						$toDecrease = false;
						$debcred = "debit";
					}
					
					else if($transactionCategory == "Journal Credit" and $accountsclassfication >= 3){
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Transfer" AND $incSection == "equities"){
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Account Opening Balance" and ($incSection == "equities" or $incSection == "equity" or $incSection == "incomes" or $incSection == "liabilities")){
						$toDecrease = false;
						$debcred = "credit";
					}
					else if ($transactionCategory == "Account Opening Balance" and ($incSection == "assets" or $incSection == "expenses" or $incSection == "accounts receivables"))
					{
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Patients Journals"){
						$toDecrease = false;
						$debcred = "credit";
					}
					
					

					


					$this->dataAllList[$incSection][$incId][$serial] = array("patient" => "xyz",
						"desc" => $transactionName,
						"cat" => $transactionCategory,
						"date" => $transactionDate,
						"amount" => ($amount * ($toDecrease ? -1:1)),
						"inc" => $incSection,
						"dec" => $decSection,
						"added_at" => "P3",
						"debcred" => $debcred,
						"link"=>$link,
						"referenceId"=>$referenceId);
				}

				// $this->data[$incSection][$incId]["amount"] += ($amount * (($incSection == "assets" and $decSection == "liabilities" and $decId == $this->accounts_payable_id)?-1:1));
			else
				echo "<br>Inc Account not found: $incSection > $incId Cat: $transactionCategory Serial: $serial amount: $amount $transactionName";
		}



		$toIncrease = (($incSection == "expenses" and $decId == $this->accounts_payable_id) // Accounts Payable Invoices
						or ($incSection == "incomes" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
						or ($incSection == "incomes" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
						or ($incSection == "assets" and $decId == $this->receivables_prepayment_id ) // Batch prepayments Invoices
						// or ($incSection == "assets" and $decSection == "equities") //  Shareholder increasing Capital
						or ($incSection == "expenses" and $decId == $this->payroll_liability_id ) // Payroll Invoices
						or ($incSection == "expenses" and $decId == $this->providers_liability_id ) // Payroll Invoices
						// or ($incSection == "expenses" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
						or ($incSection == "assets" and $incId == $this->accounts_receivable_id)
						or ($incSection == "expenses" and $decId == $this->other_payable_id) // Accounts Payable Invoices
						// or ($incSection == "assets" and $decId == $this->payroll_liability_id ) // Payroll Invoices
						// OR ($incSection == "expenses" and $decSection == "expenses" and $transactionCategory == 'Inventory Deductions')
						or ($incSection == "liabilities" and $incId == $this->other_payable_id) // Accounts Payable Invoices
						OR ($incSection == "assets" and $decSection == "equities")
						// OR ($incSection == "equities" and $decSection == "assets")
						// OR ($incSection == "expenses" and $decSection == "assets")
						// or ($incSection == "assets" and $decId == $this->accounts_payable_id)



						 );


		if($decSection != "none")
		{
			if(array_key_exists($decId, $this->data[$decSection])){
				if($credits){
					$this->data[$decSection][$decId]["amount"] += ($amount * ($toIncrease ? -1:1));
					$serial = (strlen($serial) == 0 ? "serial" . count($this->dataAllList[$decSection][$decId]) : $serial);


					if($transactionCategory == "Payroll Deductions"){
						$toIncrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Patients Journals" and $incSection == 'assets'){
						$toIncrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Patients Journals" and $incSection == 'expenses'){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Receivable Credit Note")
					{
						$toIncrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Creditor Credit Note")
					{
						$toIncrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Creditor Invoices" and $incSection == 'assets')
					{
						$toDecrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Loans Expense" and $incSection == 'assets')
					{
						$toDecrease = true;
						$debcred = "credit";
					}



					$this->dataAllList[$decSection][$decId][$serial] = array("patient" => "xyz",
						"desc" => $transactionName,
						"cat" => $transactionCategory,
						"date" => $transactionDate,
						"amount" => ($amount * ($toIncrease ? -1:1)),
						"inc" => $incSection,
						"dec" => $decSection,
						"added_at" => "P2",
						"debcred"=>$debcred,
						"link"=>$link,
						"referenceId"=>$referenceId);
				}
				else{
					$this->data[$decSection][$decId]["amount"] += ($amount * ($toIncrease ? 1:-1));
					$serial = (strlen($serial) == 0 ? "serial" . count($this->dataAllList[$decSection][$decId]) : $serial);

					//To cater for bank transfers, the relieving bank will have the transaction as a credit
					if($transactionCategory == "Transfer"){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Provider Invoices Payments" OR $transactionCategory == "Creditor Payment" OR $transactionCategory == "Payroll Payment"){
						$toIncrease = ($decSection == "liabilities");
						$debcred = "debit";
					}
					else if($transactionCategory == "Prepayments"){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Petty Cash"){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Receivable Refund"){
						$toIncrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Receivable Payment")
					{
						$toIncrease = true;
						$debcred = "credit";
					}

					else if($transactionCategory == "Receivable Invoice")
					{
						$toIncrease = true;
						$debcred = "debit";
					}

					else if($transactionCategory == "Payroll Deductions")
					{
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Payroll Deductions Payment")
					{
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Inventory Deductions")
					{
						$toIncrease = true;
						$debcred = "credit";
					}
					
					else if($transactionCategory == "Journal Credit" and $accountsclassfication > 0 AND $accountsclassfication <= 3){
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Journal Credit" AND $incSection == 'expenses'){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Journal Credit" AND $incSection == 'equities'){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Journal Credit" AND $incSection == 'assets'){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Creditor Invoices")
					{
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Provider Invoices")
					{
						$toIncrease = true;
						$debcred = "credit";
					}
					

					else if($transactionCategory == "Payroll Invoice"){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Loan Payment")
					{
						$toIncrease = true;
						$debcred = "debit";
					}


					
					
					

					

					$this->dataAllList[$decSection][$decId][$serial]= array("patient" => "xyz",
						"desc" => $transactionName,
						"cat" => $transactionCategory,
						"date" => $transactionDate,
						"amount" => ($amount * ($toIncrease ? 1:-1)),
						"inc" => $incSection,
						"dec" => $decSection,
						"added_at" => "P1",
						"debcred"=>$debcred,
						"link"=>$link,
						"referenceId"=>$referenceId);
				}


			}else
				echo "<br>Dec Account not found: $decSection > $decId - $amount - Cat: $transactionCategory--- Inc Account Found $incSection > $incId Cat: $transactionCategory Serial: $serial";
		}
		// echo "<br>Incoming - $amount - $incSection - $incId [".$this->data[$incSection][$incId]["name"]."] - $decSection - $decId ($this->accounts_payable_id): " . $this->data[$decSection][$this->accounts_payable_id]["amount"];

		if($incSection == "expenses" and $decSection == "liabilities" and $decId == $this->accounts_payable_id){
			if(!array_key_exists($incId, $this->data[$decSection][$decId]["subs"]))
				$this->data[$decSection][$decId]["subs"][$incId] = array("name" => $this->data["expenses"][$incId]["name"],
					"amount" => 0);

			$this->data[$decSection][$decId]["subs"][$incId]["amount"] += $amount;
		}
		else if($incSection == "assets" and $decSection == "liabilities" and $decId == $this->accounts_payable_id){
			if(!array_key_exists($subacc, $this->data[$decSection][$decId]["subs"]))
				$this->data[$decSection][$decId]["subs"][$subacc] = array("name" => $this->data["expenses"][$subacc]["name"],
					"amount" => 0);

			$this->data[$decSection][$decId]["subs"][$subacc]["amount"] -= $amount;
		}
		else if($incSection == "incomes" and $decSection == "assets" and $decId == $this->accounts_receivable_id){
			if(!array_key_exists($incId, $this->data[$decSection][$decId]["subs"]))
				$this->data[$decSection][$decId]["subs"][$incId] = array("name" => $this->data["incomes"][$incId]["name"],
					"amount" => 0);

			$this->data[$decSection][$decId]["subs"][$incId]["amount"] += $amount;
		}
		else if($incSection == "subacc" and $decSection == "assets" and $decId == $this->accounts_receivable_id){
			if(!array_key_exists($incId, $this->data[$decSection][$decId]["subs"]))
				$this->data[$decSection][$decId]["subs"][$subacc] = array("name" => $this->data["incomes"][$incId]["name"],
					"amount" => 0);

			$this->data[$decSection][$decId]["subs"][$subacc]["amount"] -= $amount;
		}



	}

	public function arrData(){
		return $this->data;
	}

	public function arrAssetTypes(){
		return $this->arrAssetTypes;
	}

	public function arrdataAllList(){
		return $this->dataAllList;
	}

	public function printData(){
		echo "<pre>";
		print_r($this->data);
		echo "</pre>";
	}

	public function getProfit(){
		return $this->profit;
	}




	public function preloadAssetsOpeningBalance()
	{

		$this->dataAssets = array(
							"equities" => array(),
							"assets" => array($this->accounts_receivable_id => array("name" => "Accounts Receivables",
								"amount" => 0,
								"position" => "debit",
								"subs" => array())),
							"liabilities" => array($this->accounts_payable_id => array("name" => "Accounts Payable",
								"amount" => 0,
								"position" => "credit",
								"subs" => array()),
								$this->receivables_prepayment_id => array("name" => "Receivables Prepayments",
								"amount" => 0,
								"position" => "credit",
								"subs" => array()),
								$this->providers_liability_id => array("name" => "Providers Liability",
								"amount" => 0,
								"position" => "credit",
								"subs" => array()),
								$this->loans_payable_id => array("name" => "Loans Liability",
								"amount" => 0,
								"position" => "credit",
								"subs" => array()),
								$this->payroll_liability_id => array("name" => "Payroll Liability",
								"amount" => 0,
								"position" => "credit",
								"subs" => array())),
							"incomes" => array(),
							"expenses" => array()
						  );



		$all_accounts_rs = $this->company_financial_model->get_accounts_list();
		// echo "<br>Loading accounts";
		if($all_accounts_rs->num_rows() > 0)
		{
			$this->arrAssetTypes = array("c" => array($this->accounts_receivable_id), 'f' => array());
			foreach ($all_accounts_rs->result() as $key => $value) {
				// code...
				$account_id = $value->account_id;
				$account_type_name = strtolower($value->account_type_name);
				$section = ($account_type_name == "bank" ? "assets" : ($account_type_name == "expense account" ? "expenses" : ($account_type_name == "asset" ? "assets" : ($account_type_name == "depreciation" ? "expenses" : ($account_type_name == "equity" ? "equities" : ($account_type_name == "income account" ? "incomes" : "skip"))))));

				if($account_type_name == "bank")
					array_push($this->arrAssetTypes["c"], $account_id);
				else if($account_type_name == "asset")
					array_push($this->arrAssetTypes["f"], $account_id);


				$debcred = null;
				if($section == "assets" or $section == "expenses" or $section == "accounts receivables")
					$debcred = "debit";
				else if($section == "equity" or $section == "incomes" or $section == "liabilities")
					$debcred = "credit";


				if($section != "skip")
					$this->dataAssets[$section][$value->account_id] = array("name" => $value->account_name,
						"amount" => 0,
						"position" => $debcred);
			}
		}

		$all_transacted_rs = $this->company_financial_model->calculateAccountOpeningBalances(array("dateFrom"=>$this->dateFrom->format("Y-m-d"),
			"dateTo"=>$this->dateTo->format("Y-m-d")));
		// echo "<pre>";
		// print_r($all_transacted_rs->result());
		// echo "</pre>";
		$profit = 0;
		$curr_profit = 0;
		if($all_transacted_rs->num_rows() > 0)
		{
			foreach ($all_transacted_rs->result() as $key => $value_transaction_two) {
				// code...
				$transactionId = $value_transaction_two->transactionId;
				$transactionCategory = $value_transaction_two->transactionCategory;
				$transactionClassification = $value_transaction_two->transactionClassification;
				$amount = $value_transaction_two->amount;

				$accountFromId = $value_transaction_two->accountFromId;
				$accountId = $value_transaction_two->accountId;

				// echo "<br>Printing final $transactionCategory : $accountId - $accountFromId : $amount";
				if($transactionCategory == 'Transfer'){

					if(array_key_exists($accountFromId, $this->dataAssets["equities"]) and array_key_exists($accountId, $this->dataAssets["assets"]))
						$this->registerAssetsTransaction($amount, "assets", $accountId, "equities", $accountFromId);
					else if(array_key_exists($accountFromId, $this->dataAssets["assets"]) and array_key_exists($accountId, $this->dataAssets["assets"]))
						$this->registerAssetsTransaction($amount, "assets", $accountId, "assets", $accountFromId);
					else
						$this->registerAssetsTransaction($amount, "assets", $accountId, "assets", $accountFromId);

				}
				else if($transactionCategory == 'Petty Cash'){

					if(array_key_exists($accountFromId, $this->dataAssets["assets"]) and array_key_exists($accountId, $this->dataAssets["assets"]))

						$this->registerAssetsTransaction($amount, "assets", $accountId, "assets", $accountFromId);
					else{
						$this->registerAssetsTransaction($amount, "expenses", $accountId, "assets", $accountFromId);

					}
				}


				else if($transactionCategory == 'Creditor Invoices Payments'){
					$this->registerAssetsTransaction($amount, "assets", $accountId, "liabilities", $this->accounts_payable_id);
					array_push($this->arrPayblesPayments,$transactionId);
				}
				else if($transactionCategory == 'Provider Invoices Payments'){
					$this->registerAssetsTransaction($amount, "assets", $accountId, "liabilities", $this->providers_liability_id);
					// array_push($this->arrPayblesPayments,$transactionId);
				}
				else if($transactionCategory == 'Creditor Payment'){
					if(array_key_exists($accountId, $this->dataAssets["equities"]))
						$this->registerAssetsTransaction($amount, "equities", $accountId, "liabilities", $this->accounts_payable_id);
					else
						$this->registerAssetsTransaction($amount, "assets", $accountId, "liabilities", $this->accounts_payable_id);

					array_push($this->arrPayblesPayments,$transactionId);
				}

				else if($transactionCategory == 'Receivable Payment'){
					$this->registerAssetsTransaction($amount,"assets", $accountId, "assets", $this->accounts_receivable_id);
					array_push($this->arrReceivablePayment,$transactionId);
				}
				else if($transactionCategory == 'Receivable Refund'){
					$this->registerAssetsTransaction($amount, "assets", $this->accounts_receivable_id,"assets", $accountId,0,0,$value_transaction);
					array_push($this->arrReceivablePayment,$transactionId);
				}
				else if($transactionCategory == 'Payroll Payment'){
					$this->registerAssetsTransaction($amount, "assets", $accountId, "liabilities", $this->payroll_liability_id);
					array_push($this->arrPayblesPayments,$transactionId);
				}

				else if($transactionCategory == 'Prepayments')
					$this->registerAssetsTransaction($amount, "assets", $accountId, "liabilities", $this->receivables_prepayment_id);
				else if($transactionCategory == 'Journal Credit'){

					if(array_key_exists($accountFromId, $this->dataAssets["assets"]) and array_key_exists($accountId, $this->dataAssets["expenses"]))
					{

						// pay for the expense from the bank
						$this->registerAssetsTransaction($amount, "assets", $accountFromId, "liabilities", $this->accounts_payable_id);
					}
					// if(array_key_exists($accountFromId, $this->dataAssets["equities"]) and array_key_exists($accountId, $this->dataAssets["assets"]))
					// 	$this->registerAssetsTransaction($amount, "assets", $accountId, "equities", $accountFromId);
					if(array_key_exists($accountFromId, $this->dataAssets["equities"]) and array_key_exists($accountId, $this->dataAssets["assets"]))
					{
						$this->registerAssetsTransaction($amount, "assets", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountId, $this->dataAssets["equities"]) and array_key_exists($accountFromId, $this->dataAssets["assets"]))
					{
						$this->registerAssetsTransaction($amount, "assets", $accountFromId, "equities", $accountId,0,0,$value_transaction);
					}
				}
				else if($transactionCategory == 'Loans Expense')
				{
					// $profit -= $amount;
					$this->registerAssetsTransaction($amount, "assets", $accountId, "liabilities", $this->loans_payable_id,0,1);
				}

				else if($transactionCategory == 'Loan Payment')
					if($transactionClassification == 'Loans Invoices Payments')
						$this->registerAssetsTransaction($amount, "assets", $accountId, "liabilities", $this->loans_payable_id);
					else if($transactionClassification == 'Interest Expense'){
						$this->registerAssetsTransaction($amount, "expenses", $accountId, "assets", $accountFromId);

					}
				else
					echo "<bR>  account: $accountId : $transactionCategory";

			}

		}




	}

	function registerAssetsTransaction($amount, $incSection, $incId, $decSection, $decId, $subacc = 0,$credits=0){




			$toDecrease = (($incSection == "assets" and $decSection == "liabilities" and $decId == $this->accounts_payable_id)
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->payroll_liability_id)
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->loans_payable_id)
						// OR ($incSection == "assets" and $decSection == "assets")
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->providers_liability_id));
			if($incSection != "none")
			{
				// echo "<br>Startin transfer : $incSection > $incId";
				if(array_key_exists($incId, $this->dataAssets[$incSection]))
					if($credits)
						$this->dataAssets[$incSection][$incId]["amount"] += ($amount * ($toDecrease ? 1:-1));
					else
						$this->dataAssets[$incSection][$incId]["amount"] += ($amount * ($toDecrease ? -1:1));

				else
					echo "<br>Inc Account not found: $incSection > $incId";
			}



		$toIncrease = (($incSection == "expenses" and $decId == $this->accounts_payable_id) // Accounts Payable Invoices
						or ($incSection == "incomes" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
						or ($incSection == "incomes" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
						or ($incSection == "assets" and $decId == $this->receivables_prepayment_id ) // Batch prepayments Invoices
						or ($incSection == "assets" and $decSection == "equities") //  Shareholder increasing Capital
						or ($incSection == "expenses" and $decId == $this->payroll_liability_id ) // Payroll Invoices
						or ($incSection == "expenses" and $decId == $this->providers_liability_id ) // Payroll Invoices



						 );


		if($decSection != "none")
		{
			if(array_key_exists($decId, $this->dataAssets[$decSection])){
				if($credits)
					$this->dataAssets[$decSection][$decId]["amount"] += ($amount * ($toIncrease ? -1:1));
				else
					$this->dataAssets[$decSection][$decId]["amount"] += ($amount * ($toIncrease ? 1:-1));


			}else
				echo "<br>Dec Account not found: $decSection > $decId - $amount";
		}


		// echo "<pre> ";
		// print_r($this->dataAssets[$incSection]);
		// 	echo "</pre> ";


	}

	public function arrAssetsData(){
		return $this->dataAssets;
	}



	public function calculatePayableOpeningBal()
	{
		$startYear = $this->startYear;
		$currentYear = date('Y');

		$financial_period_month = $this->financialEndMonth;


		$this->company_financial_model->calculatePayableOpeningBal(array("payable_id"=>0,"financialEndMonth"=>$financial_period_month,"startYear"=>$startYear,"currentYear"=>$currentYear,"type"=>"all"));


		$this->calculateRetainedEarnings();

	}

	public function calculateRetainedEarnings()
	{
		$startYear = $this->startYear;
		$currentYear = date('Y');

		$financial_period_month = $this->financialEndMonth;

		$this->company_financial_model->calculateRetainedEarnings(array("accountId"=>$this->retained_earnings_id,"financialEndMonth"=>$financial_period_month,"startYear"=>$startYear,"currentYear"=>$currentYear));

		$this->calculateBalances();

	}


	public function calculateBalances()
	{


		$startYear = $this->startYear;
		$currentYear = date('Y');

		$financial_period_month = $this->financialEndMonth;


		$this->company_financial_model->calculateBalances(array("financialEndMonth"=>$financial_period_month,"startYear"=>$startYear,"currentYear"=>$currentYear));

		$this->calculateReceivables();

	}

	public function calculateReceivables()
	{
		$startYear = $this->startYear;
		$currentYear = date('Y');

		$financial_period_month = $this->financialEndMonth;


		$this->company_financial_model->calculateReceivablesOpeningBal(array("financialEndMonth"=>$financial_period_month,"startYear"=>$startYear,"currentYear"=>$currentYear));

	}
	function preloadDataDetail()
	{

		$this->dataDetailed = array(
							"equities" => array(),
							"assets" => array($this->accounts_receivable_id => array("name" => "Accounts Receivables",
								"amount" => 0,
								"position" => "debit",
								"subs" => array())),
							"liabilities" => array($this->accounts_payable_id => array("name" => "Accounts Payable",
								"amount" => 0,
								"position" => "credit",
								"subs" => array()),
								$this->receivables_prepayment_id => array("name" => "Receivables Prepayments",
								"amount" => 0,
								"position" => "credit",
								"subs" => array()),
								$this->providers_liability_id => array("name" => "Providers Liability",
								"amount" => 0,
								"position" => "credit",
								"subs" => array()),
								$this->loans_payable_id => array("name" => "Loans Liability",
								"amount" => 0,
								"position" => "credit",
								"subs" => array()),
								$this->payroll_liability_id => array("name" => "Payroll Liability",
								"amount" => 0,
								"position" => "credit",
								"subs" => array())),
							"incomes" => array(),
							"expenses" => array()
						  );



		$all_accounts_rs = $this->company_financial_model->get_accounts_list();
		// echo "<br>Loading accounts";
		if($all_accounts_rs->num_rows() > 0)
		{
			$this->arrAssetTypes = array("c" => array($this->accounts_receivable_id), 'f' => array());
			foreach ($all_accounts_rs->result() as $key => $value) {
				// code...
				$account_id = $value->account_id;
				$account_type_name = strtolower($value->account_type_name);
				$section = ($account_type_name == "bank" ? "assets" : ($account_type_name == "liability" ? "liabilities" : ($account_type_name == "cost of goods" ? "cost of goods" : ($account_type_name == "expense account" ? "expenses" : ($account_type_name == "asset" ? "assets" : ($account_type_name == "depreciation" ? "expenses" : ($account_type_name == "equity" ? "equities" : ($account_type_name == "income account" ? "incomes" : ($account_type_name == "liabilities" ? "liabilities" :"skip" )))))))));

				if($account_type_name == "bank")
					array_push($this->arrAssetTypes["c"], $account_id);
				else if($account_type_name == "asset")
					array_push($this->arrAssetTypes["f"], $account_id);


				$debcred = null;
				if($section == "assets" or $section == "expenses" or $section == "cost of goods" or $section == "accounts receivables")
					$debcred = "debit";
				else if($section == "equity" or $section == "incomes" or $section == "liabilities")
					$debcred = "credit";


				if($section != "skip")
					$this->dataDetailed[$section][$value->account_id] = array("name" => $value->account_name,
						"amount" => 0,
						"position" => $debcred);
			}
		}

		$all_transacted_rs_data = $this->company_financial_model->get_account_transactions_ledgers_new(array("for_pnl" => $this->for_pnl,
			"forBalanceSheet" => $this->forBalanceSheet,
			"forOpeningTB" => $this->forOpeningTB,
			"forTrialBalance"=> $this->forTrialBalance,
			"forDetailedPnl"=> $this->detailedPnl,
			"forCashFlowPnl"=> $this->for_cashflow_pnl,
			"reportingPeriod"=>$this->reportingPeriod,
			"dateFrom"=>$this->dateFrom->format("Y-m-d"),
			"dateTo"=>$this->dateTo->format("Y-m-d")));

		// var_dump($all_transacted_rs_data);die();


		// echo "<br>Getting ready to load the payment splits";
		// echo "<pre>";
		// print_r($all_transacted_rs_data->result());
		// echo "</pre>";
		$profit = 0;
		$curr_profit = 0;
		if($all_transacted_rs_data->num_rows() > 0)
		{
			foreach ($all_transacted_rs_data->result() as $key => $value_transaction) {
				// code...
				$transactionId = $value_transaction->transactionId;
				$transactionCategory = $value_transaction->transactionCategory;
				$transactionClassification = $value_transaction->transactionClassification;
				$amount = $value_transaction->amount;

				$accountFromId = $value_transaction->accountFromId;
				$accountId = $value_transaction->accountId;


				$accountFromId = $value_transaction->accountFromId;
				$accountId = $value_transaction->accountId;
				$link = $value_transaction->link;


				if($transactionCategory == 'Transfer'){

					if(array_key_exists($accountFromId, $this->dataDetailed["equities"]) and array_key_exists($accountId, $this->dataDetailed["assets"]))
						$this->registerTransactionDetail($amount, "assets", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->dataDetailed["equities"]) and array_key_exists($accountFromId, $this->dataDetailed["assets"]))
						$this->registerTransactionDetail($amount, "equities", $accountId, "assets", $accountFromId,0,0,$value_transaction);
					else if(array_key_exists($accountFromId, $this->dataDetailed["assets"]) and array_key_exists($accountId, $this->dataDetailed["assets"]))
						$this->registerTransactionDetail($amount, "assets", $accountId, "assets", $accountFromId,0,0,$value_transaction);
					else
						$this->registerTransactionDetail($amount, "assets", $accountId, "assets", $accountFromId,0,0,$value_transaction);

				}
				else if($transactionCategory == 'Petty Cash'){

					if(array_key_exists($accountFromId, $this->dataDetailed["assets"]) and array_key_exists($accountId, $this->dataDetailed["assets"]))

						$this->registerTransactionDetail($amount, "assets", $accountId, "assets", $accountFromId,0,0,$value_transaction);
					else{
						$this->registerTransactionDetail($amount, "expenses", $accountId, "assets", $accountFromId,0,0,$value_transaction);
						$profit -= $amount;
					}
				}

				else if($transactionCategory == 'Creditor Invoices'){

					if(array_key_exists($accountId, $this->dataDetailed["assets"])) // when the creditor invoices the company an asset
						$this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->accounts_payable_id,0,1,$value_transaction);
					else
					{
						if(array_key_exists($accountId, $this->dataDetailed["expenses"])) // when the creditor invoices the company an asset
						{
							$this->registerTransactionDetail($amount, "expenses", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
							$profit -= $amount;
						}
						else if(array_key_exists($accountId, $this->dataDetailed["assets"])) // when the creditor invoices the company an asset
						{
							$this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
						}
						else if ($show_steps and $accountId == 80)
							echo "<br>Asset invoice not captured [$accountId]";
						
					}
				}
				else if($transactionCategory == 'Provider Invoices'){

					if(array_key_exists($accountId, $this->dataDetailed["assets"])) // when the creditor invoices the company an asset
						$this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->providers_liability_id,0,1,$value_transaction);
					else
					{
						$this->registerTransactionDetail($amount, "expenses", $accountId, "liabilities", $this->providers_liability_id,0,0,$value_transaction);
						$profit -= $amount;
					}
				}
				else if($transactionCategory == 'Payroll Invoice')
				{
					
						$profit -= $amount;
						$this->registerTransactionDetail($amount, "expenses", $accountId, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);

					
					
				}
				else if($transactionCategory == 'Payroll Deductions')
				{
					
						$this->registerTransactionDetail($amount, "liabilities", $this->other_payable_id, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);
					
					
				}
				else if($transactionCategory == 'Inventory Deductions')
				{	

						// $profit -= $amount;
						$this->registerTransactionDetail($amount, "expenses", $accountId, "expenses", $accountFromId,0,0,$value_transaction);
					
					
				}
				else if($transactionCategory == 'Creditor Invoices Payments'){
					// $this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
					// array_push($this->arrPayblesPayments,$transactionId);

					if(array_key_exists($accountId, $this->dataDetailed["assets"])) // when the creditor invoices the company an asset
						$this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->dataDetailed["equities"])) // when the creditor invoices the company an equity
						$this->registerTransactionDetail($amount, "equities", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);

					array_push($this->arrPayblesPayments,$transactionId);
				}
				else if($transactionCategory == 'Provider Invoices Payments'){

					if(array_key_exists($accountId, $this->dataDetailed["assets"])) // when the provider invoices the company an asset
						$this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->providers_liability_id,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->dataDetailed["equities"])) // when the provider invoices the company an asset
						$this->registerTransactionDetail($amount, "equities", $accountId, "liabilities", $this->providers_liability_id,0,1,$value_transaction);
					// array_push($this->arrPayblesPayments,$transactionId);
				}

				else if($transactionCategory == 'Creditor Opening Balance')
					//$this->registerTransactionDetail($amount, "expenses", $accountId, "liabilities", $this->accounts_payable_id);
					$this->registerTransactionDetail($amount, "liabilities", $this->accounts_payable_id, "none", 0,0,0,$value_transaction);
				else if($transactionCategory == 'Provider Opening Balance'){
					// $this->registerTransactionDetail($amount, "expenses", $accountId, "liabilities", $this->providers_liability_id);
					$this->registerTransactionDetail($amount, "liabilities", $this->providers_liability_id, "none", 0,0,0,$value_transaction);

				}


				
				else if($transactionCategory == 'Account Opening Balance')
					if(array_key_exists($accountId, $this->dataDetailed["assets"]) AND $accountFromId == 0) // when the creditor invoices the company an asset
						$this->registerTransactionDetail($amount, "assets", $accountId, "none", 0,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->dataDetailed["equities"])  AND $accountFromId == 0) // when the creditor invoices the company an asset
						$this->registerTransactionDetail($amount, "equities", $accountId, "none", 0,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->dataDetailed["liabilities"]) AND $accountFromId == 0) // when the creditor invoices the company an asset
						$this->registerTransactionDetail($amount, "liabilities", $accountId, "none", 0,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->dataDetailed["assets"]) AND $accountFromId == $this->retained_earnings_id){

						$this->registerTransactionDetail($amount, "assets", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					}
					else
						echo "<br>Account misplaced $accountId";

				else if($transactionCategory == 'Payroll Opening Balance')
					// $this->registerTransactionDetail($amount, "expenses", $accountId, "liabilities", $this->providers_liability_id);
					$this->registerTransactionDetail($amount, "liabilities", $this->payroll_liability_id, "none", 0,0,0,$value_transaction);

				else if($transactionCategory == 'Creditor Payment'){
					if(array_key_exists($accountId, $this->dataDetailed["equities"]))
						$this->registerTransactionDetail($amount, "equities", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
					else if(array_key_exists($accountId, $this->dataDetailed["assets"]))
						$this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
					else
						echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";

					array_push($this->arrPayblesPayments,$transactionId);
				}
				else if($transactionCategory == 'Receivable Invoice'){
					$profit += $amount;
					$this->registerTransactionDetail($amount,"incomes", $accountId, "assets", $this->accounts_receivable_id,0,0,$value_transaction);
				}
				else if($transactionCategory == 'Receivable Payment'){
					$this->registerTransactionDetail($amount,"assets", $accountId, "assets", $this->accounts_receivable_id,0,0,$value_transaction);
					array_push($this->arrReceivablePayment,$transactionId);
				}
				else if($transactionCategory == 'Receivable Refund'){
					// $profit += $amount;
					$this->registerTransactionDetail($amount, "assets", $this->accounts_receivable_id,"assets", $accountId,0,0,$value_transaction);
					array_push($this->arrReceivablePayment,$transactionId);
				}
				else if($transactionCategory == 'Receivable Credit Note')
				{
					$profit -= $amount;
					$this->registerTransactionDetail($amount,"incomes", $accountId, "assets", $this->accounts_receivable_id,0,1,$value_transaction);
				}
				else if($transactionCategory == 'Payroll Payment'){
					// $this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);
					// array_push($this->arrPayblesPayments,$transactionId);

					if(array_key_exists($accountId, $this->dataDetailed["assets"])) // when the creditor invoices the company an asset
						$this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);
					else  if(array_key_exists($accountId, $this->dataDetailed["equities"])) // when the creditor invoices the company an asset
						$this->registerTransactionDetail($amount, "equities", $accountId, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);
					else
						echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
						
					array_push($this->arrPayblesPayments,$transactionId);
				}
				else if($transactionCategory == 'Payroll Deductions Payment'){
					$this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->other_payable_id,0,0,$value_transaction);
					array_push($this->arrPayblesPayments,$transactionId);
				}
				else if($transactionCategory == 'Creditor Credit Note')
				{
					$profit += $amount;
					$this->registerTransactionDetail($amount, "expenses", $accountId, "liabilities", $this->accounts_payable_id,0,1,$value_transaction);
				}
				// else if($transactionCategory == 'Payroll Credit Note')
				// {
					// $profit += $amount;
					// $this->registerTransactionDetail($amount, "expenses", $accountId, "liabilities", $this->payroll_liability_id,0,1,$value_transaction);
				// }
				else if($transactionCategory == 'Provider Credit Note')
				{
					$profit += $amount;
					$this->registerTransactionDetail($amount, "expenses", $accountId, "liabilities", $this->providers_liability_id,0,1,$value_transaction);
				}
				else if($transactionCategory == 'Prepayments')
					$this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->receivables_prepayment_id,0,0,$value_transaction);
				else if($transactionCategory == 'Journal Credit'){
				
					// echo "<pre>";
					// print_r($value_transaction);
					// echo "</pre>";
					if(array_key_exists($accountFromId, $this->dataDetailed["assets"]) and array_key_exists($accountId, $this->dataDetailed["expenses"])){
						// var_dump($this->other_payable_id);die();
						// record the invoice expense inc
						// $this->registerTransactionDetail($amount, "expenses", $accountId, "liabilities", $this->other_payable_id,0,0,$value_transaction);
						// $profit -= $amount;
						// pay for the expense from the bank
						// $this->registerTransactionDetail($amount, "assets", $accountFromId, "liabilities", $this->other_payable_id,0,0,$value_transaction);


						if(array_key_exists($accountFromId, $this->dataDetailed["assets"]) and array_key_exists($accountId, $this->dataDetailed["assets"]))
							$this->registerTransactionDetail($amount, "assets", $accountId, "assets", $accountFromId,0,0,$value_transaction);

						else if(array_key_exists($accountId, $this->dataDetailed["expenses"]) and array_key_exists($accountFromId, $this->dataDetailed["assets"])){
							$this->registerTransactionDetail($amount, "expenses", $accountId, "assets", $accountFromId,0,0,$value_transaction);
							$profit -= $amount;
						}
						else
							echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
					}
					



					// if(array_key_exists($accountFromId, $this->dataDetailed["equities"]) and array_key_exists($accountId, $this->dataDetailed["assets"]))
					// 	$this->registerTransactionDetail($amount, "assets", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					else if(array_key_exists($accountFromId, $this->dataDetailed["equities"]) and array_key_exists($accountId, $this->dataDetailed["assets"]))
					{
						$this->registerTransactionDetail($amount, "equities", $accountFromId, "assets", $accountId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountFromId, $this->dataDetailed["expenses"]) and array_key_exists($accountId, $this->dataDetailed["assets"])){
						$this->registerTransactionDetail($amount, "expenses", $accountFromId, "assets", $accountId,0,0,$value_transaction);
						$profit -= $amount;
					}
					else if(array_key_exists($accountFromId, $this->dataDetailed["expenses"]) and array_key_exists($accountId, $this->dataDetailed["incomes"])){
						$this->registerTransactionDetail($amount, "expenses", $accountFromId, "incomes", $accountId,0,1,$value_transaction);
						// $profit -= $amount;
					}
					else if(array_key_exists($accountFromId, $this->dataDetailed["assets"]) and array_key_exists($accountId, $this->dataDetailed["incomes"])){
						$this->registerTransactionDetail($amount, "assets", $accountFromId, "incomes", $accountId,0,1,$value_transaction);
						$profit += $amount;
					}
					else if(array_key_exists($accountFromId, $this->dataDetailed["assets"]) and array_key_exists($accountId, $this->dataDetailed["assets"]))
					{

						$this->registerTransactionDetail($amount, "assets", $accountFromId, "assets", $accountId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountFromId, $this->dataDetailed["assets"]) and array_key_exists($accountId, $this->dataDetailed["liabilities"]))
					{
					
						$this->registerTransactionDetail($amount, "assets", $accountFromId, "liabilities", $accountId,0,1,$value_transaction);
					}

					else if(array_key_exists($accountId, $this->dataDetailed["equities"]) and array_key_exists($accountFromId, $this->dataDetailed["assets"])) 
					{
						$this->registerTransactionDetail($amount, "assets", $accountFromId, "equities", $accountId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountId, $this->dataDetailed["equities"]) and array_key_exists($accountFromId, $this->dataDetailed["expenses"])) 
					{
						$this->registerTransactionDetail($amount, "expenses", $accountFromId, "equities", $accountId,0,0,$value_transaction);
						$profit -= $amount;
					}
					else if(array_key_exists($accountId, $this->dataDetailed["equities"]) and array_key_exists($accountFromId, $this->dataDetailed["equities"])) 
					{
						$this->registerTransactionDetail($amount, "equities", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					}
					// else if(array_key_exists($accountId, $this->dataDetailed["equities"]) and array_key_exists($accountFromId, $this->dataDetailed["assets"])) 
					// {
					// 	$this->registerTransactionDetail($amount, "equities", $accountFromId, "equities", $accountId,0,0,$value_transaction);
					// }
					else if(array_key_exists($accountFromId, $this->dataDetailed["liabilities"])){

						//***** Mar 23, 2023: When moving values from one payable to another. ...e.g. when ...
						if($accountsclassfication == 1) // ** add the creditors journals
							$this->registerTransactionDetail($amount, "liabilities", $accountFromId, "liabilities", $this->accounts_payable_id,0,0,$value_transaction);
						else if($accountsclassfication == 2) // ** add the providers journals
							$this->registerTransactionDetail($amount, "liabilities", $accountFromId, "liabilities", $this->providers_liability_id,0,0,$value_transaction);
						else if($accountsclassfication == 3) // ** add the payroll journals
							$this->registerTransactionDetail($amount, "liabilities", $accountFromId, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);
						//***** May 1, 2023: When moving values from one payable from an asset. ...e.g. when ... when paying off a particular liability
						else if($accountsclassfication == 5) // ** add the providers liabilities
							$this->registerTransactionDetail($amount, "liabilities", $accountFromId, "assets", $accountId,0,0,$value_transaction);
						else if(array_key_exists($accountId, $this->dataDetailed["assets"]) AND array_key_exists($accountFromId, $this->dataDetailed["liabilities"])){
							//***** April 30, 2023: When moving values from one payable to an asset. ...e.g. when ... recovering money from a staff salary
						
							if($accountsclassfication != 3) // ** add the payroll journals
								$this->registerTransactionDetail($amount, "liabilities", $accountFromId, "assets", $accountId,0,0,$value_transaction);
							else
								echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
						}
						else
							echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";

					}
					else if(array_key_exists($accountFromId, $this->dataDetailed["assets"]) AND array_key_exists($accountId, $this->dataDetailed["liabilities"])){
						//***** April 30, 2023: When moving values from one payable to an asset. ...e.g. when ... recovering money from a staff salary
						if($accountsclassfication == 3) // ** add the payroll journals
							$this->registerTransactionDetail($amount, "assets", $accountFromId, "liabilities", $this->payroll_liability_id,0,0,$value_transaction);
						else
							echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
					}
					else if(array_key_exists($accountId, $this->dataDetailed["assets"]) AND array_key_exists($accountFromId, $this->dataDetailed["liabilities"])){

						echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
						
					}
					else
						echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";

						
				}
				else if($transactionCategory == "Patients Journals")
				{
					$profit += $amount;
					$this->registerTransactionDetail($amount, "expenses", $accountFromId,"assets", $this->accounts_receivable_id,0,1,$value_transaction);
				}
				else if($transactionCategory == "Direct Payments")
				{
					// $profit += $amount;
					if(array_key_exists($accountId, $this->dataDetailed["assets"]) and array_key_exists($accountFromId, $this->dataDetailed["equities"])) 
					{
						$this->registerTransactionDetail($amount, "assets", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountId, $this->dataDetailed["expenses"]) and array_key_exists($accountFromId, $this->dataDetailed["equities"])) 
					{
						$profit -= $amount;
						$this->registerTransactionDetail($amount, "expenses", $accountId, "equities", $accountFromId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountId, $this->dataDetailed["expenses"]) and array_key_exists($accountFromId, $this->dataDetailed["assets"])) 
					{
						$profit -= $amount;
						$this->registerTransactionDetail($amount, "expenses", $accountId, "assets", $accountFromId,0,0,$value_transaction);
					}
					else if(array_key_exists($accountId, $this->dataDetailed["assets"]) and array_key_exists($accountFromId, $this->dataDetailed["assets"])) 
					{
						$this->registerTransactionDetail($amount, "assets", $accountId, "assets", $accountFromId,0,0,$value_transaction);
					}
					
					else
						echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : <a href='".site_url().$link."'> $transactionId </a>";
				}

				// else if($transactionCategory == "Balances Brought Forward"){
				// 	// echo "<br>Adding Bal. B/f: $accountId";
				// 	if(array_key_exists($accountId, $this->dataDetailed["assets"]))
				// 		$this->registerTransactionDetail($amount, "assets", $accountId, "none", 0);
				// 	else if(array_key_exists($accountId, $this->dataDetailed["equities"]))
				// 		$this->registerTransactionDetail($amount, "equities", $accountId, "none", 0);
				// }

				else if($transactionCategory == 'Loans Expense')
				{
					// $profit -= $amount;
					$this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->loans_payable_id,0,1,$value_transaction);
				}
				else if($transactionCategory == 'Loan Opening Balance'){
					// $this->registerTransactionDetail($amount, "expenses", $accountId, "liabilities", $this->providers_liability_id);
					$this->registerTransactionDetail($amount, "liabilities", $this->loans_payable_id, "equities", $this->retained_earnings_id,0,0,$value_transaction);

				}

				else if($transactionCategory == 'Loan Payment'){


					if(array_key_exists($accountId, $this->dataDetailed["assets"])) {
						// var_dump($this->loans_payable_id);die();
						$this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->loans_payable_id,0,0,$value_transaction);
						// $profit += $amount;
					}
					else if(array_key_exists($accountId, $this->dataDetailed["equities"])) {
						// var_dump($this->loans_payable_id);die();
						$this->registerTransactionDetail($amount, "assets", $accountId, "liabilities", $this->loans_payable_id,0,0,$value_transaction);
						// $profit += $amount;
					}
					else {
						var_dump($accountId);die();
						echo "<bR>  accounts: $accountId : $transactionCategory : $amount trans id : $transactionId";
					}
				}else if($transactionCategory == 'Loan Interest Payment'){
						$this->registerTransactionDetail($amount, "expenses", $accountId, "assets", $accountFromId,0,0,$value_transaction);
						$profit -= $amount;
			
				}else if($transactionCategory == 'Bank Charges Expense'){
						$this->registerTransactionDetail($amount, "expenses", $accountId, "assets", $accountFromId,0,0,$value_transaction);
						$profit -= $amount;
			
				}else if($transactionCategory == 'Bank Interest Earned'){
						$this->registerTransactionDetail($amount, "assets", $accountFromId, "incomes", $accountId,0,1,$value_transaction);
						$profit += $amount;
			
				}else
					echo "<bR>  accounts: $accountId : $transactionCategory";


				// if($show_steps and $curr_profit <> $profit){
				// 	//echo "<br>Changed at $transactionCategory [$profit]: " . ($profit - $curr_profit);

				// 	$account_name = $this->company_financial_model->get_account_name($accountId);
				// 	$curr_profit = $profit;
				// 	if(!array_key_exists($transactionCategory, $arrSummary))
				// 		$arrSummary[$transactionCategory] = array("t" => 0, "s" => array());
				// 	if(!array_key_exists($accountId, $arrSummary[$transactionCategory]["s"]))
				// 		$arrSummary[$transactionCategory]["s"][$accountId] = array("n" => $account_name, "s" => 0);
				// 	$arrSummary[$transactionCategory]["t"] += $amount;
				// 	$arrSummary[$transactionCategory]["s"][$accountId]["s"] += $amount;

				// 	if($accountId == 80){
				// 		echo "<br>Sth: ".$value_transaction->referenceTable." Invoice id ".$value_transaction->referenceId;
				// 	}
				// }


			



			}
		}


	}

	function registerTransactionDetail($amount, $incSection, $incId, $decSection, $decId, $subacc = 0,$credits=0,$transaction_detail)
	{

		$transactionId = $transaction_detail->transactionId;
		$transactionCategory = $transaction_detail->transactionCategory;
		$accountsclassfication = $transaction_detail->accountsclassfication;
		$transactionDate = $transaction_detail->transactionDate;
		$transactionDescription = $transaction_detail->transactionDescription;
		$amount = $transaction_detail->amount;

		$accountFromId = $transaction_detail->accountFromId;
		$accountId = $transaction_detail->accountId;
		$month = $transaction_detail->month_start;
		$year = $transaction_detail->year_start;
		$referenceCode = $transaction_detail->referenceCode;
		$transactionName = $transaction_detail->transactionName;
		$link = $transaction_detail->link;
		$referenceId = $transaction_detail->referenceId;

		$count = $count + 1;
		$date = $transactionDate;
		$patient = "";
		$serial = $referenceCode.'*'.$transactionId.'-'.$incId;

		// var_dump($amount);die();
		if($transactionCategory == "Receivable Payment")
		{
			$this->dataList['Patients Payments'][$incId]["amount"] += $amount;
		}



		$date = $transactionDate;
		$patient = "";
		$serial = $referenceCode.'*'.$transactionId;

		// echo "<bR>Received: $date - $serial";
		if(!array_key_exists($date, $this->dataList[$incSection][$incId][$year][$month]))
				$this->dataList[$incSection][$incId][$year][$month][$date] = array();

		$debcred = null;
		if($incSection == "assets" or $incSection == "expenses" or $incSection == "accounts receivables")
			$debcred = "debit";
		else if($incSection == "equity" or $incSection == "incomes" or $incSection == "liabilities")
			$debcred = "credit";


			$toDecrease = (($incSection == "assets" and $decSection == "liabilities" and $decId == $this->accounts_payable_id)
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->payroll_liability_id )
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->loans_payable_id)
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->providers_liability_id)
						OR ($incSection == "assets" and $decSection == "assets" and $incId == $this->accounts_receivable_id)
						OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->other_payable_id)
						OR ($incSection == "liabilities" and $decSection == "liabilities" and $decId == $this->payroll_liability_id)
						OR ($incSection == "equities" and $decSection == "assets")
						OR ($incSection == "assets" and $decSection == "incomes")

						OR ($incSection == "assets" and $decSection == "liabilities" AND $credits === 1)
						OR ($incSection == "expenses" and $decSection == "incomes" AND $credits === 1) // income paying off an expense
						OR ($incSection == "liabilities" and $decSection == "assets" ));
		if($incSection != "none")
		{
			if(array_key_exists($incId, $this->dataDetailed[$incSection]))
				if($credits){
					$this->dataDetailed[$incSection][$incId][$year][$month]["amount"] += ($amount * ($toDecrease ? 1:-1));
					$serial = (strlen($serial) == 0 ? "serial" . count($this->dataList[$incSection][$incId][$year][$month][$date]) : $serial);
					

					if($transactionCategory == "Creditor Invoices" and $incSection == 'assets')
					{
						$toDecrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Receivable Credit Note")
					{
						$toDecrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Loans Expense" and $incSection == 'assets')
					{
						$toDecrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Bank Interest Earned")
					{
						$toDecrease = false;
						$debcred = "debit";
					}

					else if($transactionCategory == "Journal Credit")
					{
						$toDecrease = true;
						$debcred = "debit";
					}
					

					// $this->dataAllList[$incSection][$incId][$serial] = array("patient" => "xyz",
					// 	"desc" => $transactionName,
					// 	"cat" => $transactionCategory,
					// 	"date" => $transactionDate,
					// 	"amount" => ($amount * ($toDecrease ? 1:-1)),
					// 	"inc" => $incSection,
					// 	"dec" => $decSection,
					// 	"added_at" => "P4",
					// 	"debcred"=>$debcred,
					// 	"link"=>$link,
					// 	"referenceId"=>$referenceId);


					$this->dataList[$incSection][$incId][$year][$month][$date][$serial] = array("patient" => "xyz",
						"desc" => $transactionName,
						"cat" => $transactionCategory,
						"amount" => ($amount * ($toDecrease ? 1:-1)),
						"inc" => $incSection,
						"dec" => $decSection);
				}
				else
				{
					$this->dataDetailed[$incSection][$incId][$year][$month]["amount"] += ($amount * ($toDecrease ? -1:1));
					$serial = (strlen($serial) == 0 ? "serial" . count($this->dataList[$incSection][$incId][$year][$month][$date]) : $serial);
					

					if($transactionCategory == "Provider Invoices Payments" OR $transactionCategory == "Creditor Payment" OR $transactionCategory == "Payroll Payment"){
						$toDecrease = ($decSection != "liabilities");
						$debcred = "credit";
					}
					// else if($transactionCategory == "Receivable Invoice")
					// {
					// 	$toDecrease = false;
					// 	$debcred = "debit";
					// }
					else if($transactionCategory == "Receivable Refund"){
						$toDecrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Creditor Invoices")
					{
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Provider Invoices")
					{
						$toDecrease = false;
						$debcred = "debit";
					}
					

					else if($transactionCategory == "Payroll Invoice"){
						$toDecrease = false;
						$debcred = "debit";
					}
					
					else if($transactionCategory == "Journal Credit" and $accountsclassfication >= 3){
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Transfer" AND $incSection == "equities"){
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Account Opening Balance" and ($incSection == "equities" or $incSection == "equity" or $incSection == "incomes" or $incSection == "liabilities")){
						$toDecrease = false;
						$debcred = "credit";
					}
					else if ($transactionCategory == "Account Opening Balance" and ($incSection == "assets" or $incSection == "expenses" or $incSection == "accounts receivables"))
					{
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Patients Journals"){
						$toDecrease = false;
						$debcred = "credit";
					}
					
					else if($transactionCategory == "Journal Credit" AND $incSection == 'equities' AND  $decSection == 'assets'){
						$toDecrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Journal Credit" AND $incSection == 'equities' AND $decSection == 'equities'){
						$toDecrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Direct Payments" AND ($decSection == 'equities' OR $decSection == 'assets')){
						$toDecrease = false;
						$debcred = "debit";
					}
					
					
					


					$this->dataList[$incSection][$incId][$year][$month][$date][$serial] = array("patient" => "xyz",
						"desc" => $transactionName,
						"cat" => $transactionCategory,
						"amount" => ($amount * ($toDecrease ? -1:1)),
						"inc" => $incSection,
						"dec" => $decSection);


					// $this->dataAllList[$incSection][$incId][$serial] = array("patient" => "xyz",
					// 	"desc" => $transactionName,
					// 	"cat" => $transactionCategory,
					// 	"date" => $transactionDate,
					// 	"amount" => ($amount * ($toDecrease ? -1:1)),
					// 	"inc" => $incSection,
					// 	"dec" => $decSection,
					// 	"added_at" => "P3",
					// 	"debcred" => $debcred,
					// 	"link"=>$link,
					// 	"referenceId"=>$referenceId);
				}

				// $this->data[$incSection][$incId]["amount"] += ($amount * (($incSection == "assets" and $decSection == "liabilities" and $decId == $this->accounts_payable_id)?-1:1));
			else
				echo "<br>Inc Account not found: $incSection > $incId Cat: $transactionCategory Serial: $serial amounts: $amount $transactionName";
		}



		$toIncrease = (($incSection == "expenses" and $decId == $this->accounts_payable_id) // Accounts Payable Invoices
						or ($incSection == "incomes" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
						or ($incSection == "incomes" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
						or ($incSection == "assets" and $decId == $this->receivables_prepayment_id ) // Batch prepayments Invoices
						// or ($incSection == "assets" and $decSection == "equities") //  Shareholder increasing Capital
						or ($incSection == "expenses" and $decId == $this->payroll_liability_id ) // Payroll Invoices
						or ($incSection == "expenses" and $decId == $this->providers_liability_id ) // Payroll Invoices
						// or ($incSection == "expenses" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
						or ($incSection == "assets" and $incId == $this->accounts_receivable_id)
						or ($incSection == "expenses" and $decId == $this->other_payable_id) // Accounts Payable Invoices
						// or ($incSection == "assets" and $decId == $this->payroll_liability_id ) // Payroll Invoices
						// OR ($incSection == "expenses" and $decSection == "expenses" and $transactionCategory == 'Inventory Deductions')
						or ($incSection == "liabilities" and $incId == $this->other_payable_id) // Accounts Payable Invoices
						OR ($incSection == "assets" and $decSection == "equities")
						OR ($incSection == "expenses" and $decSection == "equities")
						OR ($incSection == "assets" and $decSection == "equities" AND $decId == $this->retained_earnings_id)
						// OR ($incSection == "expenses" and $decSection == "incomes" AND $credits === 1)
						// OR ($incSection == "expenses" and $decSection == "assets"  AND $transactionCategory === "Journal Credit")
						// OR ($incSection == "assets" and $decSection == "incomes")
						// OR ($incSection == "expenses" and $decSection == "assets")
						// or ($incSection == "assets" and $decId == $this->accounts_payable_id)



						 );


		if($decSection != "none")
		{
			if(!array_key_exists($date, $this->dataList[$decSection][$decId][$year][$month]))
				$this->dataList[$decSection][$decId][$year][$month][$date] = array();


			if(array_key_exists($decId, $this->dataDetailed[$decSection])){

				if($credits)
				{

					$this->dataDetailed[$decSection][$decId][$year][$month]["amount"] += ($amount * ($toIncrease ? -1:1));
					$serial = (strlen($serial) == 0 ? "serial" . count($this->dataList[$decSection][$decId][$year][$month][$date]) : $serial);


					if($transactionCategory == "Payroll Deductions"){
						$toIncrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Patients Journals" and $incSection == 'assets'){
						$toIncrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Patients Journals" and $incSection == 'expenses'){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Receivable Credit Note")
					{
						$toIncrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Creditor Credit Note")
					{
						$toIncrease = false;
						$debcred = "debit";
					}
					else if($transactionCategory == "Creditor Invoices" and $incSection == 'assets')
					{
						$toDecrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Loans Expense" and $incSection == 'assets')
					{
						$toDecrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Bank Interest Earned")
					{
						$toDecrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Journal Credit")
					{
						$toDecrease = false;
						$debcred = "credit";
					}
					


					$this->dataAllList[$decSection][$decId][$serial] = array("patient" => "xyz",
						"desc" => $transactionName,
						"cat" => $transactionCategory,
						"date" => $transactionDate,
						"amount" => ($amount * ($toIncrease ? -1:1)),
						"inc" => $incSection,
						"dec" => $decSection,
						"added_at" => "P2",
						"debcred"=>$debcred,
						"link"=>$link,
						"referenceId"=>$referenceId);

					$this->dataList[$decSection][$decId][$year][$month][$date][$serial] = array("patient" => "xyz",
							"desc" => $transactionName,
							"cat" => $transactionCategory,
							"amount" => ($amount * ($toIncrease ? -1:1)),
							"inc" => $incSection,
							"dec" => $decSection);
				}
				else
				{
					$this->dataDetailed[$decSection][$decId][$year][$month]["amount"] += ($amount * ($toIncrease ? 1:-1));
					$serial = (strlen($serial) == 0 ? "serial" . count($this->dataList[$decSection][$decId][$year][$month][$date]) : $serial);
				

					//To cater for bank transfers, the relieving bank will have the transaction as a credit
					if($transactionCategory == "Transfer"){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Provider Invoices Payments" OR $transactionCategory == "Creditor Payment" OR $transactionCategory == "Payroll Payment"){
						$toIncrease = ($decSection == "liabilities");
						$debcred = "debit";
					}
					else if($transactionCategory == "Prepayments"){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Petty Cash"){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Receivable Refund"){
						$toIncrease = false;
						$debcred = "credit";
					}
					else if($transactionCategory == "Receivable Payment")
					{
						$toIncrease = true;
						$debcred = "credit";
					}

					else if($transactionCategory == "Receivable Invoice")
					{
						$toIncrease = true;
						$debcred = "debit";
					}

					else if($transactionCategory == "Payroll Deductions")
					{
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Payroll Deductions Payment")
					{
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Inventory Deductions")
					{
						$toIncrease = true;
						$debcred = "credit";
					}
					
					else if($transactionCategory == "Journal Credit" and $accountsclassfication > 0 AND $accountsclassfication <= 3){
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Journal Credit" AND $incSection == 'expenses'){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Journal Credit" AND $incSection == 'equities' AND $decSection != 'equities'){
						$toIncrease = true;
						$debcred = "credit";
					}
					
					else if($transactionCategory == "Journal Credit" AND $incSection == 'assets'){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Creditor Invoices")
					{
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Provider Invoices")
					{
						$toIncrease = true;
						$debcred = "credit";
					}
					

					else if($transactionCategory == "Payroll Invoice"){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Loan Payment")
					{
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Bank Charges Expense")
					{
						$toIncrease = true;
						$debcred = "credit";
					}
					
					else if($transactionCategory == "Journal Credit" AND $incSection == 'equities' AND $decSection == 'equities'){
						$toIncrease = true;
						$debcred = "debit";
					}

					else if($transactionCategory == "Direct Payments" AND ($decSection == 'equities' OR $decSection == 'assets')){
						$toIncrease = true;
						$debcred = "credit";
					}
					else if($transactionCategory == "Loan Opening Balance"){
						$toIncrease = true;
						$debcred = "debit";
					}
					else if($transactionCategory == "Account Opening Balance"){
						$toIncrease = true;
						$debcred = "credit";
					}



					
					
					

					

					// $this->dataAllList[$decSection][$decId][$serial]= array("patient" => "xyz",
					// 	"desc" => $transactionName,
					// 	"cat" => $transactionCategory,
					// 	"date" => $transactionDate,
					// 	"amount" => ($amount * ($toIncrease ? 1:-1)),
					// 	"inc" => $incSection,
					// 	"dec" => $decSection,
					// 	"added_at" => "P1",
					// 	"debcred"=>$debcred,
					// 	"link"=>$link,
					// 	"referenceId"=>$referenceId);

					$this->dataList[$decSection][$decId][$year][$month][$date][$serial] = array("patient" => "xyz",
						"desc" => $transactionName,
						"cat" => $transactionCategory,
						"amount" => ($amount * ($toIncrease ? 1:-1)),
						"inc" => $incSection,
						"dec" => $decSection);
				}


			}else
				echo "<br>Dec Account not found: $decSection > $decId - $amount - Cat: $transactionCategory--- Inc Account Found $incSection > $incId Cat: $transactionCategory Serial: $serial";
		}

		
		// start to remove from here


		// $transactionId = $transaction_detail->transactionId;
		// $transactionCategory = $transaction_detail->transactionCategory;
		// $transactionClassification = $transaction_detail->transactionClassification;
		// $transactionDate = $transaction_detail->transactionDate;
		// $transactionDescription = $transaction_detail->transactionDescription;
		// $amount = $transaction_detail->amount;

		// $accountFromId = $transaction_detail->accountFromId;
		// $accountId = $transaction_detail->accountId;
		// $month = $transaction_detail->month_start;
		// $year = $transaction_detail->year_start;
		// $referenceCode = $transaction_detail->referenceCode;
		// $transactionName = $transaction_detail->transactionName;


		// $date = $transactionDate;
		// $patient = "";
		// $serial = $referenceCode.'*'.$transactionId;

		// // echo "<bR>Received: $date - $serial";
		// if(!array_key_exists($date, $this->dataList[$incSection][$incId][$year][$month]))
		// 		$this->dataList[$incSection][$incId][$year][$month][$date] = array();


		// $toDecrease = (($incSection == "assets" and $decSection == "liabilities" and $decId == $this->accounts_payable_id)
		// 				OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->payroll_liability_id )
		// 				OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->loans_payable_id)
		// 				OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->providers_liability_id)
		// 				OR ($incSection == "assets" and $decSection == "assets" and $incId == $this->accounts_receivable_id)
		// 				OR ($incSection == "assets" and $decSection == "liabilities" and $decId == $this->other_payable_id)
		// 				OR ($incSection == "liabilities" and $decSection == "liabilities" and $decId == $this->payroll_liability_id)
		// 				OR ($incSection == "equities" and $decSection == "assets")
		// 				// OR ($incSection == "assets" and $decSection == "expenses")
		// 				// OR ($incSection == "equities" and $decSection == "assets")
		// 				OR ($incSection == "liabilities" and $decSection == "assets" ));

		// if($incSection != "none")
		// {
		// 	if(array_key_exists($incId, $this->dataDetailed[$incSection])){
		// 		if($credits){
		// 			$this->dataDetailed[$incSection][$incId][$year][$month]["amount"] += ($amount * ($toDecrease ? 1:-1));
		// 			$serial = (strlen($serial) == 0 ? "serial" . count($this->dataList[$incSection][$incId][$year][$month][$date]) : $serial);
		// 			$this->dataList[$incSection][$incId][$year][$month][$date][$serial] = array("patient" => "xyz",
		// 				"desc" => $transactionName,
		// 				"cat" => $transactionCategory,
		// 				"amount" => ($amount * ($toDecrease ? 1:-1)),
		// 				"inc" => $incSection,
		// 				"dec" => $decSection);

		// 		}
		// 		else{
		// 			$this->dataDetailed[$incSection][$incId][$year][$month]["amount"] += ($amount * ($toDecrease ? -1:1));
		// 			$serial = (strlen($serial) == 0 ? "serial" . count($this->dataList[$incSection][$incId][$year][$month][$date]) : $serial);
		// 			$this->dataList[$incSection][$incId][$year][$month][$date][$serial] = array("patient" => "xyz",
		// 				"desc" => $transactionName,
		// 				"cat" => $transactionCategory,
		// 				"amount" => ($amount * ($toDecrease ? -1:1)),
		// 				"inc" => $incSection,
		// 				"dec" => $decSection);
		// 		}

		// 		// $this->dataDetailed[$incSection][$incId]["amount"] += ($amount * (($incSection == "assets" and $decSection == "liabilities" and $decId == $this->accounts_payable_id)?-1:1));
		// 	}else
		// 		echo "<br>Inc Account not found: $incSection > $incId";
		// }



		// $toIncrease = (($incSection == "expenses" and $decId == $this->accounts_payable_id) // Accounts Payable Invoices
		// 				or ($incSection == "incomes" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
		// 				or ($incSection == "incomes" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
		// 				or ($incSection == "assets" and $decId == $this->receivables_prepayment_id ) // Batch prepayments Invoices
		// 				// or ($incSection == "assets" and $decSection == "equities") //  Shareholder increasing Capital
		// 				or ($incSection == "expenses" and $decId == $this->payroll_liability_id ) // Payroll Invoices
		// 				or ($incSection == "expenses" and $decId == $this->providers_liability_id ) // Payroll Invoices
		// 				// or ($incSection == "expenses" and $decId == $this->accounts_receivable_id) // Accounts Receivables Invoices
		// 				or ($incSection == "assets" and $incId == $this->accounts_receivable_id)
		// 				or ($incSection == "expenses" and $decId == $this->other_payable_id) // Accounts Payable Invoices
		// 				// or ($incSection == "assets" and $decId == $this->payroll_liability_id ) // Payroll Invoices
		// 				// OR ($incSection == "expenses" and $decSection == "expenses" and $transactionCategory == 'Inventory Deductions')
		// 				or ($incSection == "liabilities" and $incId == $this->other_payable_id) // Accounts Payable Invoices
		// 				OR ($incSection == "assets" and $decSection == "equities")
		// 				// OR ($incSection == "equities" and $decSection == "assets")
		// 				// OR ($incSection == "expenses" and $decSection == "assets")
		// 				// or ($incSection == "assets" and $decId == $this->accounts_payable_id)



		// 				 );



		// if($decSection != "none")
		// {


		// 	if(!array_key_exists($date, $this->dataList[$decSection][$decId][$year][$month]))
		// 		$this->dataList[$decSection][$decId][$year][$month][$date] = array();


		// 	if(array_key_exists($decId, $this->dataDetailed[$decSection])){

		// 		if($credits){

		// 			$this->dataDetailed[$decSection][$decId][$year][$month]["amount"] += ($amount * ($toIncrease ? -1:1));

		// 			$serial = (strlen($serial) == 0 ? "serial" . count($this->dataList[$decSection][$decId][$year][$month][$date]) : $serial);
		// 			$this->dataList[$decSection][$decId][$year][$month][$date][$serial] = array("patient" => "xyz",
		// 				"desc" => $transactionName,
		// 				"cat" => $transactionCategory,
		// 				"amount" => ($amount * ($toIncrease ? -1:1)),
		// 				"inc" => $incSection,
		// 				"dec" => $decSection);
		// 		}
		// 		else{
		// 			$this->dataDetailed[$decSection][$decId][$year][$month]["amount"] += ($amount * ($toIncrease ? 1:-1));
		// 			$serial = (strlen($serial) == 0 ? "serial" . count($this->dataList[$decSection][$decId][$year][$month][$date]) : $serial);
		// 			$this->dataList[$decSection][$decId][$year][$month][$date][$serial] = array("patient" => "xyz",
		// 				"desc" => $transactionName,
		// 				"cat" => $transactionCategory,
		// 				"amount" => ($amount * ($toIncrease ? 1:-1)),
		// 				"inc" => $incSection,
		// 				"dec" => $decSection);
		// 		}


		// 	}else
		// 		echo "<br>Dec Account not found: $decSection > $decId - $amount ---- Inc Account Found $incSection > $incId";
		// }





	}

	public function arrdataDetailed(){
		return $this->dataDetailed;
	}

	public function arrdataList(){
		return $this->dataList;
	}

}

?>
