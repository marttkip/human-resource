//Reference: http://jsfiddle.net/qxLn3h86/48/
//https://wiki.scn.sap.com/wiki/display/Snippets/XML+TAGS+simplifies+Excel+Download?original_fqdn=wiki.sdn.sap.com

var tablesToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">' +
            '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>' +
            '<Styles>' +
            '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>' +
            '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>' +
            '<Style ss:ID="center_normal"><Alignment ss:Horizontal="Center"></Alignment></Style>' +
            '<Style ss:ID="right_normal"><Alignment ss:Horizontal="Right"></Alignment></Style>' +
            '<Style ss:ID="center_bold"><Alignment ss:Horizontal="Center"></Alignment><Font ss:Bold="1"/></Style>' +
            '<Style ss:ID="right_bold"><Alignment ss:Horizontal="Right"></Alignment><Font ss:Bold="1"/></Style>' +
            '</Styles>' +
            '{worksheets}</Workbook>',
            tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>',
            tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>',
            base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            },
            format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            }
    return function (tables, wsnames, wbname, appname) {
        var ctx = "";
        var workbookXML = "";
        var worksheetsXML = "";
        var rowsXML = "";

        for (var i = 0; i < tables.length; i++) {
            if (!tables[i].nodeType)
                tables[i] = document.getElementById(tables[i]);
            //Find column descriptions
            var arrCols = Array(tables[i].rows[0].cells[0].style.width);
            var width = 0;
            var hasDefinition = false;
            for (var r = 0; r < tables[i].rows.length; r++) {
                for (var c = 0; c < tables[i].rows[r].cells.length; c++) {
                    width = parseInt(tables[i].rows[r].cells[c].style.width);
                    if (!isNaN(width)) {
                        arrCols[c] = width;
                        hasDefinition = true;
                    } else if (!(c in arrCols))
                        arrCols[c] = 0;
                }
            }

            var option = "";
            if (hasDefinition)
                for (var c = 0; c < arrCols.length; c++) {
                    option = arrCols[c] == 0 ? ' ss:AutoFitWidth="1"' : ' ss:AutoFitWidth="0" ss:Width="' + arrCols[c] + '"';
                    rowsXML += '<Column ' + option + '/>';
                    console.log("Col. " + c + ": " + arrCols[c]);
                }

            for (var j = 0; j < tables[i].rows.length; j++) {
                rowsXML += '<Row>'
                for (var k = 0; k < tables[i].rows[j].cells.length; k++) {
                    var dataType = tables[i].rows[j].cells[k].getAttribute("data-type");
                    var dataStyle = tables[i].rows[j].cells[k].getAttribute("data-style");
                    var dataValue = tables[i].rows[j].cells[k].getAttribute("data-value");
                    dataValue = (dataValue) ? dataValue : tables[i].rows[j].cells[k].innerHTML;
                    var dataFormula = tables[i].rows[j].cells[k].getAttribute("data-formula");
                    var textAlign = tables[i].rows[j].cells[k].style.textAlign;
                    var colspan = parseInt(tables[i].rows[j].cells[k].colSpan);
                    var width = parseInt(tables[i].rows[j].cells[k].style.width);
                    var fontweight = tables[i].rows[j].cells[k].style.fontWeight;
                    console.log(j + "-" + k + ": " + tables[i].rows[j].cells[k].style.textAlign + " (" + colspan + " - " + width + " -  " + fontweight + ") - " + dataValue);
                    dataFormula = (dataFormula) ? dataFormula : (appname == 'Calc' && dataType == 'DateTime') ? dataValue : null;
                    //(textAlign == "center") ? ' ss:Horizontal="'+ textAlign+'"' :
                    var cellStyle = (dataStyle == 'Currency' || dataStyle == 'Date') ? ' ss:StyleID="' + dataStyle + '"' : "";
                    if (textAlign == "center" || textAlign == "right") {
                        if (fontweight == "bold")
                            textAlign += "_bold";
                        else
                            textAlign += "_normal";
                        cellStyle += ' ss:StyleID="' + textAlign + '"';
                    }
                    if (colspan > 1) {
                        colspan -= 1;
                        cellStyle += ' ss:MergeAcross="' + colspan + '"';
                    }
                    ctx = {
                        attributeStyleID: cellStyle,
                        nameType: (dataType == 'Number' || dataType == 'DateTime' || dataType == 'Boolean' || dataType == 'Error') ? dataType : 'String',
                        data: (dataFormula) ? '' : dataValue,
                        attributeFormula: (dataFormula) ? ' ss:Formula="' + dataFormula + '"' : ''
                    };
                    rowsXML += format(tmplCellXML, ctx);
                }
                rowsXML += '</Row>'
            }
            ctx = {
                rows: rowsXML,
                nameWS: wsnames[i] || 'Sheet' + i
            };
            worksheetsXML += format(tmplWorksheetXML, ctx);
            rowsXML = "";
        }

        ctx = {
            created: (new Date()).getTime(),
            worksheets: worksheetsXML
        };
        workbookXML = format(tmplWorkbookXML, ctx);

        console.log(workbookXML);

        var link = document.createElement("A");
        link.href = uri + base64(workbookXML);
        link.download = wbname || 'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
})();
